﻿using System.Windows.Controls;

namespace NormalizationTool.DataGridCommonControl
{
    /// <summary>
    /// Interaction logic for CommonDataGrid.xaml
    /// </summary>
    public partial class CommonDataGrid : UserControl
    {
        public CommonDataGrid()
        {
            InitializeComponent();
        }
    }
}
