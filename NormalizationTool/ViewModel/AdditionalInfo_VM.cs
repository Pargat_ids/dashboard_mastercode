﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using EntityClass;
using System.Windows.Forms;
using EntityFramework.Utilities;
using VersionControlSystem.Model.ViewModel;
using System.Windows;
using System.Threading.Tasks;
using CommonGenericUIView.ViewModel;
using DataGridFilterLibrary.Querying;
using DataGridFilterLibrary.Support;
using GalaSoft.MvvmLight.Messaging;
using System.Linq.Dynamic;

namespace NormalizationTool.ViewModel
{
    public partial class MainControl_VM : Base_ViewModel
    {
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        public ICommand ClearAllFilter { get; set; }
        public List<ListCheckBoxType_ViewModel> lstInternalOnly { get; set; } = new List<ListCheckBoxType_ViewModel>();
        public ICommand BtnSaveAddInfo { get; set; }
        private Dictionary<string, char> SpecialCharacters = new Dictionary<string, char>();
        public List<GenericCriteria_VM> listGenericCriteria { get; set; } = new List<GenericCriteria_VM>();
        public List<GenericCriteria_VM> myListOriginal { get; set; } = new List<GenericCriteria_VM>();
        public List<GenericCriteria_VM> myList { get; set; } = new List<GenericCriteria_VM>();
        private List<CRA_DataAccess.ListCheckBoxType_ViewModel> listCheckBox = new List<CRA_DataAccess.ListCheckBoxType_ViewModel>();

        DataTable AllDicData = new DataTable();
        List<(string, int)> GetAllColumnNamesFromDataAddInfo = new List<(string, int)>();
        List<(string, int)> getAllDataColumns = new List<(string, int)>();
        private string IdentityColumn = string.Empty;
        public static List<(string, string)> CombineFieldName { get; set; } = new List<(string, string)>();
        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public Visibility listAllColVisible { get; set; } = Visibility.Collapsed;
        public ICommand CommandOnColSelectedChange { get; set; }
        public ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM> ListAllColumnDG { get; set; } = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            if (listAllColVisible == Visibility.Collapsed)
            {
                listAllColVisible = Visibility.Visible;
            }
            else
            {
                listAllColVisible = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllColVisible");
        }
        public void BindListAllColumn()
        {

            var list = new List<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();

            foreach (DataColumn dt in defaultDataTableColumns)
            {
                list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { IsSelected = dt.ColumnName == "Header" || dt.ColumnName == "Explanation" ? false : true, ColumnName = dt.ColumnName });
            }

            ListAllColumnDG = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>(list);
            NotifyPropertyChanged("ListAllColumnDG");
        }

        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDG.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            switch (selectCol.ColumnName)
            {
                case "ColumnName":
                    ColumnNameVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("ColumnNameVisible");
                    break;
                case "Header":
                    HeaderVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("HeaderVisible");
                    break;
                case "Explanation":
                    ExplanationVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("ExplanationVisible");
                    break;
                case "Is_Internal":
                    Is_InternalVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Hidden;
                    NotifyPropertyChanged("Is_InternalVisible");
                    break;
                case "Field_Type":
                    Field_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Field_TypeVisible");
                    break;
                case "Threshold_Type":
                    Threshold_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Threshold_TypeVisible");
                    break;
                case "Unit_Type":
                    Unit_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Unit_TypeVisible");
                    break;
                case "Operator_Type":
                    Operator_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Operator_TypeVisible");
                    break;
                case "Manual_Unit":
                    Manual_UnitVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Manual_UnitVisible");
                    break;
                case "Manual_Operator":
                    Manual_OperatorVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Manual_OperatorVisible");
                    break;
                case "Unit_Field":
                    Min_Max_Unit_FieldVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Min_Max_Unit_FieldVisible");
                    break;
                case "Operator_Field":
                    Operator_FieldVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Operator_FieldVisible");
                    break;
                case "Category":
                    CategoryVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("CategoryVisible");
                    break;
                case "Language":
                    LanguageVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("LanguageVisible");
                    break;
                case "Delimiter":
                    DelimiterVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("DelimiterVisible");
                    break;
                case "DateFormat":
                    DateFormatVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("DateFormatVisible");
                    break;
                case "Allowed_True_False":
                    Allowed_True_FalseVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Allowed_True_FalseVisible");
                    break;
                case "Consider_Single_Value":
                    Consider_Single_ValueVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Consider_Single_ValueVisible");
                    break;
                case "Consider_Null_Value":
                    Consider_Null_ValueVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Consider_Null_ValueVisible");
                    break;
                case "ValueField_Contain_Operator":
                    ValueField_Contain_OperatorVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("ValueField_Contain_OperatorVisible");
                    break;
            }
        }

        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        public string lblTotRecords { get; set; }
        public string lblContent { get; set; }
        int pageIndex = 1;
        private int numberOfRecPerPage = 25;
        private enum PagingMode { First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public DataColumnCollection defaultDataTableColumns { get; set; }
        public ObservableCollection<selectedListItemCombo> ListComboxItem { get; set; } = new ObservableCollection<selectedListItemCombo>();
        public selectedListItemCombo _selectedItemCombo { get; set; }
        public selectedListItemCombo selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    numberOfRecPerPage = (int)value.id;
                    NotifyPropertyChanged("numberOfRecPerPage");
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }

        public Visibility ColumnNameVisible { get; set; }
        public Visibility HeaderVisible { get; set; } = Visibility.Hidden;
        public Visibility ExplanationVisible { get; set; } = Visibility.Hidden;
        public Visibility Is_InternalVisible { get; set; }
        public Visibility Field_TypeVisible { get; set; }
        public Visibility Threshold_TypeVisible { get; set; }
        public Visibility Unit_TypeVisible { get; set; }
        public Visibility Operator_TypeVisible { get; set; }
        public Visibility Manual_UnitVisible { get; set; }
        public Visibility Manual_OperatorVisible { get; set; }
        public Visibility Min_Max_Unit_FieldVisible { get; set; }
        public Visibility Operator_FieldVisible { get; set; }
        public Visibility CategoryVisible { get; set; }
        public Visibility LanguageVisible { get; set; }
        public Visibility DelimiterVisible { get; set; }
        public Visibility DateFormatVisible { get; set; }
        public Visibility Allowed_True_FalseVisible { get; set; }
        public Visibility Consider_Single_ValueVisible { get; set; }
        public Visibility Consider_Null_ValueVisible { get; set; }
        public Visibility ValueField_Contain_OperatorVisible { get; set; }

        private void ExecuteFunction(List<GenericCriteria_VM> lstData)
        {
            var dtTemp = new DataTable();
            dtTemp.Columns.Add("ColumnName");
            dtTemp.Columns.Add("Header");
            dtTemp.Columns.Add("Explanation");
            dtTemp.Columns.Add("Is_Internal");
            dtTemp.Columns.Add("Field_Type");
            dtTemp.Columns.Add("Threshold_Type");
            dtTemp.Columns.Add("Unit_Type");
            dtTemp.Columns.Add("Operator_Type");
            dtTemp.Columns.Add("Manual_Unit");
            dtTemp.Columns.Add("Min/Max/Unit_Field");
            dtTemp.Columns.Add("Category");
            dtTemp.Columns.Add("Language");
            dtTemp.Columns.Add("Delimiter");
            dtTemp.Columns.Add("DateFormat");
            dtTemp.Columns.Add("Allowed_True_False");
            dtTemp.Columns.Add("Consider_Single_Value");
            dtTemp.Columns.Add("Consider_Null_Value");
            dtTemp.Columns.Add("ValueField_Contain_Operator");
            defaultDataTableColumns = dtTemp.Columns;
            BindListAllColumn();
            LoaderAddInfo = Visibility.Visible;
            NotifyPropertyChanged("LoaderAddInfo");
            Task.Run(() =>
            {
                myListOriginal = lstData;
                myList = lstData;
                //selectedItemCombo = ListComboxItem.Where(x => x.id == 25).FirstOrDefault();
                LoaderAddInfo = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderAddInfo");
                Navigate((int)PagingMode.First);
                //NotifyPropertyChanged("selectedItemCombo");
            });
        }
        private List<selectedListItemCombo> AddList()
        {
            List<selectedListItemCombo> listSelectedCombo =

                new List<selectedListItemCombo>() {
                new selectedListItemCombo() { id = 25, text = "25" },
                new selectedListItemCombo() { id = 50, text = "50" },
                new selectedListItemCombo() { id = 100, text = "100" },
                new selectedListItemCombo() { id = 500, text = "All" },
            };
            return listSelectedCombo;

        }
        private bool IsNavigateProgress { get; set; }
        private void Navigate(int mode)
        {
            IsNavigateProgress = true;
            lblContent = "";
            lblTotRecords = "";
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("lblContent");
            var result = _objLibraryFunction_Service.NavigateListNew<GenericCriteria_VM>(mode, ref pageIndex, numberOfRecPerPage, myList, selectedItemCombo == null ? "25" : selectedItemCombo.text, lblContent, lblTotRecords, listGenericCriteria);
            lblContent = result.Item1;
            lblTotRecords = result.Item2;
            listGenericCriteria = result.Item3;
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("listGenericCriteria");
            NotifyPropertyChanged("lblContent");
            IsNavigateProgress = false;
        }
        public void GetDicColumns()
        {
            AllDicData = new DataTable();
            GetAllColumnNamesFromDataAddInfo = new List<(string, int)>();
            getAllDataColumns = new List<(string, int)>();
            IdentityColumn = string.Empty;
            AllDicData = this.objCommon.DbaseQueryReturnTable("select * from DATA_Dictionary", CommonFilepath);
            var AllListData = this.objCommon.DbaseQueryReturnTable("select top 1 * from List_Dictionary", CommonFilepath);
            GetColumnNames("DATA", CommonFilepath);
            getAllDataColumns = GetAllColumnNamesFromDataAddInfo.ToList().OrderBy(y => y.Item2).ToList();
            if (AllListData.Rows.Count > 0)
            {
                var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(AllListData.Rows[0]["QSID"].ToString());
                var newList = false;
                if (QsidID == 0)
                {
                    var lst = new List<string>();
                    var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(AllListData.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                    if (resultQsidInsert != "Pass")
                    {
                        _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                        return;
                    }
                    newList = true;
                }
                var sessTypeId = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select SessionTypeId from Library_SessionTypes where sessionType ='Normalization - AdditionalInfo'", CommonListConn));
                var newSession = _objLibraryFunction_Service.CreateNewSession(QsidID, CommonFilepath, sessTypeId);

                var sessID = newSession;
                QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(AllListData.Rows[0]["QSID"].ToString());
                Qsid_ID = QsidID;

                listGenericCriteria = new List<GenericCriteria_VM>();
                OverrideQueryController = new QueryController();
                query = new Query();
                filtersForColumns = new Dictionary<string, FilterData>();
                OverrideQueryController.FilteringStarted -= DataGridFilteringStarted;
                OverrideQueryController.FilteringStarted += DataGridFilteringStarted;
                using (var context = new CRAModel())
                {
                    var allFieldNames = (from d1 in context.Library_FieldNames.AsNoTracking().ToList()
                                         join d2 in getAllDataColumns
                                         on d1.FieldName equals d2.Item1
                                         select new { d1, d2.Item2 }).OrderBy(z => z.Item2).Select(y => y.d1).ToList();
                    List<Map_QSID_FieldName> chkExists = context.Map_QSID_FieldName.Where(x => x.QSID_ID == QsidID).ToList();
                    if (!chkExists.Any())
                    {
                        DefaultValues();
                        DefaultInternal(AllDicData);
                        CheckCommonValues(AllDicData, AllListData.Rows[0]["QSID"].ToString());
                        InsertValuesToMapQSIDFromDefault(QsidID, sessID);
                    }
                    else
                    {
                        DefaultValues();
                        chkBothMdbAndMapTableShouldSame(context, QsidID, sessID);
                        CheckExistingValues(QsidID);
                        for (int i = 0; i < listGenericCriteria.Count; i++)
                        {
                            overrideMDBvalue(i, AllDicData);
                        }
                    }
                    var chkMapQsid = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == Qsid_ID && x.UnitID != null && x.UnitFieldNameID != null).Select(y => y.UnitFieldNameID).Distinct().ToList();
                    var chkMapQsid1 = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == Qsid_ID && x.OperatorID != null && x.OperatorFieldNameID != null).Select(y => y.OperatorFieldNameID).Distinct().ToList();
                    chkMapQsid.AddRange(chkMapQsid1);
                    List<Map_QSID_FieldName> mapvirtualFields = context.Map_QSID_FieldName.Where(x => x.QSID_ID == QsidID && chkMapQsid.Contains(x.FieldNameID)).ToList();
                    chkExists = chkExists.Except(mapvirtualFields).ToList();
                    if (allFieldNames.Count() < chkExists.Count())
                    {
                        List<int?> fieldsToDelete = (from d1 in chkExists
                                                     join d2 in allFieldNames
                                                     on d1.FieldNameID equals d2.FieldNameID
                                                     into tg
                                                     from tcheck in tg.DefaultIfEmpty()
                                                     where tcheck == null
                                                     select d1.FieldNameID).ToList();

                        if (fieldsToDelete.Count() > 0)
                        {
                            List<Map_QSID_FieldName> lstMap = context.Map_QSID_FieldName.Where(x => (x.QSID_ID == QsidID) && fieldsToDelete.Contains(x.FieldNameID)).ToList();

                            List<Log_Map_QSID_FieldName> lstLog = new List<Log_Map_QSID_FieldName>();
                            foreach (Map_QSID_FieldName ct in lstMap)
                            {
                                Log_Map_QSID_FieldName lg = new Log_Map_QSID_FieldName
                                {
                                    MapQSIDFieldName_ID = ct.MapQSIDFieldName_ID,
                                    FieldNameID = ct.FieldNameID,
                                    IsInternalOnly = ct.IsInternalOnly,
                                    IsCASField = ct.IsCASField,
                                    IsChemicalName = ct.IsChemicalName,
                                    IsIdentifierField = ct.IsIdentifierField,
                                    IsNoteCodeField = ct.IsNoteCodeField,
                                    IsPhraseField = ct.IsPhraseField,
                                    IsValueOther = ct.IsValueOther,
                                    IsTrueFalseField = ct.IsTrueFalseField,
                                    PhraseCategoryID = ct.PhraseCategoryID,
                                    IdentifierTypeID = ct.IdentifierTypeID,
                                    SubstanceNameCategoryId = ct.SubstanceNameCategoryId,
                                    LanguageID = ct.LanguageID,
                                    IsMinValue = ct.IsMinValue,
                                    IsMaxValue = ct.IsMaxValue,
                                    IsMinValOp = ct.IsMinValOp,
                                    IsMaxValOp = ct.IsMaxValOp,
                                    IsUnit = ct.IsUnit,
                                    UnitID = ct.UnitID,
                                    UnitFieldNameID = ct.UnitFieldNameID,
                                    Operator_ValueFieldID = ct.Operator_ValueFieldID,
                                    RangeDelimiterID = ct.RangeDelimiterID,
                                    ValueFieldContainsOperators = ct.ValueFieldContainsOperators,
                                    UnitDelimiterID = ct.UnitDelimiterID,
                                    ConsiderSingleValueAs = ct.ConsiderSingleValueAs,
                                    ConsiderNullValueAs = ct.ConsiderNullValueAs,
                                    AllowedTrueFalseValues = ct.AllowedTrueFalseValues,
                                    MultiValuePhrase = ct.MultiValuePhrase,
                                    MultiValuePhraseDelimiterID = ct.MultiValuePhraseDelimiterID,
                                    MultiValueIdentifier = ct.MultiValueIdentifier,
                                    MultiValueIdentifierDelimiterID = ct.MultiValueIdentifierDelimiterID,
                                    ChemicalNameDelimiterID = ct.ChemicalNameDelimiterID,
                                    ValueFieldContainsRanges = ct.ValueFieldContainsRanges,
                                    ValueFieldContainsUnits = ct.ValueFieldContainsUnits,
                                    QSID_ID = QsidID,
                                    Status = "D",
                                    TimeStamp = objCommon.ESTTime(),
                                    SessionID = sessID,
                                    ThresholdCategory = ct.ThresholdCategory,
                                    DisplayField2Client = ct.DisplayField2Client,
                                    SourceFieldDisplayOrder = ct.SourceFieldDisplayOrder,
                                    FieldSortOrder = ct.FieldSortOrder,
                                    DateFormatID = ct.DateFormatID,
                                };
                                lstLog.Add(lg);
                            }

                            _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLog);
                            EFBatchOperation.For(context, context.Map_QSID_FieldName).Where(x => (x.QSID_ID == QsidID) && fieldsToDelete.Contains(x.FieldNameID)).Delete();
                            context.SaveChanges();
                        }
                    }

                    ExecuteFunction(listGenericCriteria.ToList());
                    //listGenericCriteria = new ObservableCollection<GenericCriteria_VM>(listGenericCriteria);
                    //NotifyPropertyChanged("listGenericCriteria");
                }
                objCommon.DbaseQueryReturnStringSQL("Update Log_Sessions set SessionEnd_TimeStamp ='" + objCommon.ESTTime() + "' where QSID_ID ='" + QsidID + "' and UserName ='" + Environment.UserName + "' and sessiontypeid = " + sessTypeId + " and SessionEnd_TimeStamp is null and QSID_ID is not null", CommonListConn);
            }
        }
        private void CheckCommonValues(DataTable AllDicData, string currQsid)
        {
            using (CRAModel context = new CRAModel())
            {
                this.ChkCommonValuesForNotesUnitAndTrueFalse(AllDicData);
                var AllLangNol = new List<string>();
                string[] languageCodes = null;
                string getLanguageCode = objCommon.DbaseQueryReturnString("select FLD_NAME from " + currQsid + " where  FLD_NAME like 'nol%'", CommonFilepath);
                if (getLanguageCode != null)
                {
                    languageCodes = getLanguageCode.Replace("nol:", string.Empty).Split(';');
                }

                if (string.IsNullOrWhiteSpace(getLanguageCode) == false)
                {
                    for (int i = 0; i < languageCodes.Count(); i++)
                    {
                        AllLangNol.Add(languageCodes[i].Trim().ToUpper());
                    }
                }

                this.ChkCommonValuesPrefAndPrefEng(context, AllLangNol);
            }
        }
        private void ChkCommonValuesPrefAndPrefEng(CRAModel context, List<string> AllLangNol)
        {
            for (int j = 0; j < listGenericCriteria.Count; j++)
            {
                string chkColName = AllLangNol.FirstOrDefault(x => x.Contains(listGenericCriteria[j].ColumnName.Trim().ToUpper()));
                if ((chkColName != null) && (listGenericCriteria[j].ColumnName.ToUpper() != "PREF"))
                {
                    string[] splLang = chkColName.Split('|');
                    if ((splLang != null) && (splLang.Count() > 1))
                    {
                        Library_Languages getLangCode = context.Library_Languages.FirstOrDefault(x => x.LanguageCode.ToUpper().Trim() == splLang[1].Trim().ToUpper());
                        if (getLangCode != null)
                        {
                            listGenericCriteria[j].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                            NotifyPropertyChanged("IsInternalOnly");
                            listGenericCriteria[j].SelectedFieldType = listGenericCriteria[j].ListFieldType.Where(x => x.CboValue == "Substance Name").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            listGenericCriteria[j].SelectedLanguage = listGenericCriteria[j].ListLanguage.Where(x => x.LanguageName == getLangCode.LanguageName).FirstOrDefault();
                            NotifyPropertyChanged("SelectedLanguage");
                        }
                    }
                }
            }

            for (int i = 0; i < listGenericCriteria.Count; i++)
            {
                if (listGenericCriteria[i].IsInternalOnly.Value == true && (listGenericCriteria[i].ColumnName.ToUpper() == "PREF_ENGL" || listGenericCriteria[i].ColumnName.ToUpper().Contains("_SOURCE")))
                {
                    listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == true).FirstOrDefault();
                    NotifyPropertyChanged("IsInternalOnly");
                }
            }
        }
        private void ChkCommonValuesForNotesUnitAndTrueFalse(DataTable onlyDataColumns)
        {
            List<string> lstNotecode = onlyDataColumns.AsEnumerable().Where(x => x.Field<dynamic>("Data_Type") == 6).Select(y => y.Field<string>("Field_Name")).ToList();
            List<string> lstUnit = onlyDataColumns.AsEnumerable().Where(x => x.Field<dynamic>("Data_Type") == 7).Select(y => y.Field<string>("Field_Name")).ToList();
            List<string> lstTrueFalse = onlyDataColumns.AsEnumerable().Where(x => x.Field<dynamic>("Data_Type") == 5).Select(y => y.Field<string>("Field_Name")).ToList();

            foreach (string t in lstNotecode)
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (t.ToUpper() == listGenericCriteria[i].ColumnName.ToUpper())
                    {
                        listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                        NotifyPropertyChanged("IsInternalOnly");
                        listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "NoteCode").FirstOrDefault();
                        NotifyPropertyChanged("SelectedFieldType");
                        listGenericCriteria[i].SelectedLanguage = listGenericCriteria[i].ListLanguage.Where(x => x.LanguageName == "English").FirstOrDefault();
                        listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.Delimiter == ",").FirstOrDefault();
                        NotifyPropertyChanged("SelectedLanguage");
                        NotifyPropertyChanged("SelectedDelimiter");
                    }
                }
            }

            foreach (string t in lstUnit)
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (t.ToUpper() == listGenericCriteria[i].ColumnName.ToUpper())
                    {
                        listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                        NotifyPropertyChanged("IsInternalOnly");
                        listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Threshold").FirstOrDefault();
                        NotifyPropertyChanged("SelectedFieldType");
                        listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit").FirstOrDefault();
                        NotifyPropertyChanged("SelectedThresholdType");
                    }
                }
            }

            for (int ii = 0; ii < lstTrueFalse.Count; ii++)
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (lstTrueFalse[ii].ToUpper() == listGenericCriteria[i].ColumnName.ToUpper())
                    {
                        listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                        NotifyPropertyChanged("IsInternalOnly");
                        listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "True False").FirstOrDefault();
                        NotifyPropertyChanged("SelectedFieldType");
                    }
                }
            }
        }
        private void InsertValuesToMapQSIDFromDefault(Int32 QsidID, Int32 sID)
        {
            AddDataInMapQSIDLibrary(QsidID, sID);
        }
        private void chkBothMdbAndMapTableShouldSame(CRAModel context, Int32 qsidID, Int32 sessID)
        {
            var AllDicData = this.objCommon.DbaseQueryReturnTable("select * from Data_Dictionary", CommonFilepath);
            var additionalTotalRows = (from d1 in context.Map_QSID_FieldName.Where(x => (x.QSID_ID == qsidID)).Select(x => new { x.FieldNameID, x.DisplayField2Client, x.SourceFieldDisplayOrder, x.FieldSortOrder, x.IsInternalOnly }).ToList()
                                       join d2 in context.Library_FieldNames
                                       on d1.FieldNameID equals d2.FieldNameID
                                       select new { d1.FieldNameID, d1.DisplayField2Client, d1.SourceFieldDisplayOrder, d2.FieldName, d1.FieldSortOrder, d1.IsInternalOnly }).ToList();

            var joinMapWithMdb = (from d1 in AllDicData.AsEnumerable()
                                  join d2 in additionalTotalRows
                                  on d1.Field<string>("FIELD_NAME").ToUpper().Trim() equals d2.FieldName.ToUpper().Trim()
                                  select new
                                  {
                                      d2.FieldNameID,
                                      d2.DisplayField2Client,
                                      d2.SourceFieldDisplayOrder,
                                      d2.FieldSortOrder,
                                      d2.IsInternalOnly,
                                      RN = Convert.ToInt32(d1.Field<dynamic>("RN")),
                                      NewDisplayField2Client = AllDicData.Columns.Contains("DisplayField2Clients") ? d1.Field<dynamic>("DisplayField2Clients") : true,
                                      NewSourceFieldDisplayOrder = AllDicData.Columns.Contains("SourceFieldDisplayOrder") ? d1.Field<dynamic>("SourceFieldDisplayOrder") : null,
                                  }).ToList();
            joinMapWithMdb = joinMapWithMdb.OrderBy(x => x.RN).ToList();
            List<Log_Map_QSID_FieldName> lstLog = new List<Log_Map_QSID_FieldName>();
            int cnter = 1;
            for (int i = 0; i < joinMapWithMdb.Count; i++)
            {
                int newFieldSortOrder = joinMapWithMdb[i].IsInternalOnly ? 999 : cnter;
                if ((joinMapWithMdb[i].DisplayField2Client != joinMapWithMdb[i].NewDisplayField2Client)
                    || (joinMapWithMdb[i].SourceFieldDisplayOrder != joinMapWithMdb[i].NewSourceFieldDisplayOrder || joinMapWithMdb[i].FieldSortOrder != newFieldSortOrder))
                {
                    int? fieldId = joinMapWithMdb[i].FieldNameID;
                    Map_QSID_FieldName ct = context.Map_QSID_FieldName.FirstOrDefault(x => (x.QSID_ID == qsidID) && (x.FieldNameID == fieldId));
                    Log_Map_QSID_FieldName lg = new Log_Map_QSID_FieldName
                    {
                        MapQSIDFieldName_ID = ct.MapQSIDFieldName_ID,
                        FieldNameID = ct.FieldNameID,
                        IsInternalOnly = ct.IsInternalOnly,
                        IsCASField = ct.IsCASField,
                        IsChemicalName = ct.IsChemicalName,
                        IsIdentifierField = ct.IsIdentifierField,
                        IsNoteCodeField = ct.IsNoteCodeField,
                        IsPhraseField = ct.IsPhraseField,
                        IsValueOther = ct.IsValueOther,
                        IsTrueFalseField = ct.IsTrueFalseField,
                        PhraseCategoryID = ct.PhraseCategoryID,
                        IdentifierTypeID = ct.IdentifierTypeID,
                        SubstanceNameCategoryId = ct.SubstanceNameCategoryId,
                        LanguageID = ct.LanguageID,
                        IsMinValue = ct.IsMinValue,
                        IsMaxValue = ct.IsMaxValue,
                        IsMinValOp = ct.IsMinValOp,
                        IsMaxValOp = ct.IsMaxValOp,
                        IsUnit = ct.IsUnit,
                        UnitID = ct.UnitID,
                        UnitFieldNameID = ct.UnitFieldNameID,
                        Operator_ValueFieldID = ct.Operator_ValueFieldID,
                        RangeDelimiterID = ct.RangeDelimiterID,
                        ValueFieldContainsOperators = ct.ValueFieldContainsOperators,
                        UnitDelimiterID = ct.UnitDelimiterID,
                        ConsiderSingleValueAs = ct.ConsiderSingleValueAs,
                        ConsiderNullValueAs = ct.ConsiderNullValueAs,
                        AllowedTrueFalseValues = ct.AllowedTrueFalseValues,
                        MultiValuePhrase = ct.MultiValuePhrase,
                        MultiValuePhraseDelimiterID = ct.MultiValuePhraseDelimiterID,
                        MultiValueIdentifier = ct.MultiValueIdentifier,
                        MultiValueIdentifierDelimiterID = ct.MultiValueIdentifierDelimiterID,
                        ChemicalNameDelimiterID = ct.ChemicalNameDelimiterID,
                        ValueFieldContainsRanges = ct.ValueFieldContainsRanges,
                        ValueFieldContainsUnits = ct.ValueFieldContainsUnits,
                        QSID_ID = qsidID,
                        Status = "C",
                        TimeStamp = objCommon.ESTTime(),
                        SessionID = sessID,
                        ThresholdCategory = ct.ThresholdCategory,
                        DisplayField2Client = (bool?)joinMapWithMdb[i].NewDisplayField2Client,
                        SourceFieldDisplayOrder = (int?)joinMapWithMdb[i].NewSourceFieldDisplayOrder,
                        FieldSortOrder = newFieldSortOrder,
                        ParentId = ct.ParentId,
                        ChildSortOrder = ct.ChildSortOrder,
                        DateFormatID = ct.DateFormatID,
                    };
                    lstLog.Add(lg);
                    ct.DisplayField2Client = (bool?)joinMapWithMdb[i].NewDisplayField2Client;
                    ct.SourceFieldDisplayOrder = (int?)joinMapWithMdb[i].NewSourceFieldDisplayOrder;
                    ct.FieldSortOrder = newFieldSortOrder;
                    context.SaveChanges();
                }

                if (newFieldSortOrder != 999)
                {
                    cnter = cnter + 1;
                }
            }

            _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLog);
        }
        private void DefaultValues()
        {
            for (int i = 0; i < getAllDataColumns.Count(); i++)
            {
                var dicDet = AllDicData.AsEnumerable().Where(x => x.Field<string>("Field_Name").ToUpper() == getAllDataColumns[i].Item1.ToUpper()).FirstOrDefault();
                listGenericCriteria.Add(new GenericCriteria_VM()
                {
                    ColumnName = getAllDataColumns[i].Item1.ToUpper(),
                    Header = dicDet == null ? string.Empty : dicDet.Field<string>("Head_CD"),
                    Explanation = dicDet == null ? string.Empty : dicDet.Field<string>("Explain")

                });
            }
        }
        private void DefaultInternal(DataTable dt)
        {
            using (CRAModel context = new CRAModel())
            {
                var chkInMapQSIDInternalOnly = (from d1 in context.Map_QSID_FieldName_Defaults.AsNoTracking().ToList()
                                                join d2 in context.Library_FieldNames.AsNoTracking().ToList()
                                                on d1.FieldNameID equals d2.FieldNameID
                                                select new { d1, d2.FieldName }).ToList();
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    Map_QSID_FieldName_Defaults fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    listGenericCriteria[i].SelectedFieldType = null;
                    listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == true).FirstOrDefault();
                    listGenericCriteria[i].ValueFieldContainOperator = false;
                    listGenericCriteria[i].SourceFieldDisplay = 0;
                    listGenericCriteria[i].DisplayFieldToClient = false;
                    if (fieldDet != null)
                    {
                        if (fieldDet.IsCASField || fieldDet.IsChemicalName || fieldDet.IsNoteCodeField == true || fieldDet.IsPhraseField || fieldDet.IsIdentifierField || fieldDet.IsValueOther || fieldDet.IsTrueFalseField == true || fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.IsMinValOp || fieldDet.IsMaxValOp || fieldDet.IsUnit || fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                            NotifyPropertyChanged("IsInternalOnly");
                        }
                        if (fieldDet.IsCASField)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "CAS").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsChemicalName)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Substance Name").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            var CatId = fieldDet.SubstanceNameCategoryId == 0 || fieldDet.SubstanceNameCategoryId == null ? 0 : fieldDet.SubstanceNameCategoryId;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            var lngId = fieldDet.LanguageID == 0 || fieldDet.LanguageID == null ? 0 : fieldDet.LanguageID;
                            listGenericCriteria[i].SelectedLanguage = listGenericCriteria[i].ListLanguage.Where(x => x.LanguageID == lngId).FirstOrDefault();
                            var delId = fieldDet.ChemicalNameDelimiterID == 0 || fieldDet.ChemicalNameDelimiterID == null ? 0 : fieldDet.ChemicalNameDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedLanguage");
                            NotifyPropertyChanged("SelectedDelimiter");
                        }
                        if (fieldDet.IsPhraseField == true || fieldDet.IsNoteCodeField == true)
                        {
                            if (fieldDet.IsNoteCodeField == true)
                            {
                                listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "NoteCode").FirstOrDefault();
                            }
                            if (fieldDet.IsPhraseField == true)
                            {
                                listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Phrase").FirstOrDefault();
                            }
                            NotifyPropertyChanged("SelectedFieldType");
                            var phrCatId = fieldDet.PhraseCategoryID == 0 || fieldDet.PhraseCategoryID == null ? 0 : fieldDet.PhraseCategoryID;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == phrCatId).FirstOrDefault();
                            var lngId = fieldDet.LanguageID == 0 || fieldDet.LanguageID == null ? 0 : fieldDet.LanguageID;
                            listGenericCriteria[i].SelectedLanguage = listGenericCriteria[i].ListLanguage.Where(x => x.LanguageID == lngId).FirstOrDefault();
                            var delId = fieldDet.MultiValuePhraseDelimiterID == 0 || fieldDet.MultiValuePhraseDelimiterID == null ? 0 : fieldDet.MultiValuePhraseDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedLanguage");
                            NotifyPropertyChanged("SelectedDelimiter");
                        }
                        if (fieldDet.IsIdentifierField == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Identifier").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            var CatId = fieldDet.IdentifierTypeID == 0 || fieldDet.IdentifierTypeID == null ? 0 : fieldDet.IdentifierTypeID;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            var delId = fieldDet.MultiValueIdentifierDelimiterID == 0 || fieldDet.MultiValueIdentifierDelimiterID == null ? 0 : fieldDet.MultiValueIdentifierDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedDelimiter");
                        }
                        if (fieldDet.IsValueOther == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Value Other").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsTrueFalseField == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "True False").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");

                            string[] trueFalsChar = string.IsNullOrWhiteSpace(fieldDet.AllowedTrueFalseValues) ? new string[] { } : fieldDet.AllowedTrueFalseValues.Split(',');
                            foreach (string t in trueFalsChar)
                            {
                                listGenericCriteria[i].ListAllowedTrueFalse.Where(x => x.Character == t.Trim()).FirstOrDefault().IsSelected = true;
                            }
                            listGenericCriteria[i].SelectedTrueFalse = fieldDet.AllowedTrueFalseValues;
                            NotifyPropertyChanged("SelectedTrueFalse");
                            NotifyPropertyChanged("ListAllowedTrueFalse");
                        }
                        if (fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.IsMinValOp || fieldDet.IsMaxValOp || fieldDet.IsUnit || fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Threshold").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsMinValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.IsMaxValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("SelectedCatgory");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");
                            NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.IsMinValOp)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value Operator").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");

                        }
                        if (fieldDet.IsMaxValOp)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value Operator").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.IsUnit)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Value Range").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            var rangeDel = fieldDet.RangeDelimiterID == 0 || fieldDet.RangeDelimiterID == null ? 0 : fieldDet.RangeDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == rangeDel).FirstOrDefault();
                            NotifyPropertyChanged("SelectedDelimiter");
                            listGenericCriteria[i].SelectedConsiderSingleValues = listGenericCriteria[i].ListConsiderSingleValues.Where(x => x.CboValue == fieldDet.ConsiderSingleValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderSingleValues");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");
                        }
                        if (fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.ValueFieldContainsRanges)
                        {
                            if ((fieldDet.UnitID != 0) && (fieldDet.UnitID != null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Manual Unit Definition").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedUnit = listGenericCriteria[i].ListUnit.Where(x => x.UnitID == fieldDet.UnitID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedUnit");
                            }

                            if ((fieldDet.UnitFieldNameID != 0) && (fieldDet.UnitFieldNameID != null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.ID == fieldDet.UnitFieldNameID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedMaxFieldName");
                            }

                            if (fieldDet.UnitDelimiterID != null)
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit Delimitor").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.Delimiter == " ").FirstOrDefault();
                                NotifyPropertyChanged("SelectedDelimiter");
                            }
                            if ((fieldDet.UnitID == null) && (fieldDet.UnitFieldNameID == null) && (fieldDet.UnitDelimiterID == null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "No Unit").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                            }
                            if (fieldDet.ValueFieldContainsOperators)
                            {
                                listGenericCriteria[i].ValueFieldContainOperator = fieldDet.ValueFieldContainsOperators;
                                NotifyPropertyChanged("ValueFieldContainOperator");
                            }
                        }
                        //else
                        //{
                        //    if ((fieldDet.Operator_ValueFieldID != 0) && (fieldDet.Operator_ValueFieldID != null))
                        //    {
                        //        listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.ID == fieldDet.Operator_ValueFieldID).FirstOrDefault();
                        //        NotifyPropertyChanged("SelectedMaxFieldName");
                        //    }
                        //}

                    }
                    overrideMDBvalue(i, dt);
                }
            }
        }

        private void overrideMDBvalue(int i, DataTable AllDicData)
        {
            string fieldName = listGenericCriteria[i].ColumnName.ToUpper();
            dynamic displayField2Client = false;
            if (AllDicData.Columns.Contains("DisplayField2Clients"))
            {
                displayField2Client = AllDicData.AsEnumerable().Where(x => x.Field<string>("FIELD_NAME").ToUpper() == fieldName).Select(y => y.Field<dynamic>("DisplayField2Clients")).FirstOrDefault();
            }

            dynamic sourceFieldDisplayOrder = null;
            if (AllDicData.Columns.Contains("SourceFieldDisplayOrder"))
            {
                sourceFieldDisplayOrder = AllDicData.AsEnumerable().Where(x => x.Field<string>("FIELD_NAME").ToUpper() == fieldName).Select(y => y.Field<dynamic>("SourceFieldDisplayOrder")).FirstOrDefault();
            }


            if (displayField2Client == null)
            {
                listGenericCriteria[i].DisplayFieldToClient = false;
            }
            else
            {
                if (displayField2Client is Boolean)
                {
                    listGenericCriteria[i].DisplayFieldToClient = displayField2Client == true ? true : false;
                }
                if (displayField2Client is int || displayField2Client is double)
                {
                    listGenericCriteria[i].DisplayFieldToClient = displayField2Client == 1 ? true : false;
                }
            }
            listGenericCriteria[i].SourceFieldDisplay = sourceFieldDisplayOrder == null ? (int?)null : Convert.ToInt32(sourceFieldDisplayOrder);
        }

        private void CheckExistingValues(Int32 qsidID)
        {
            using (CRAModel context = new CRAModel())
            {
                var chkInMapQSIDInternalOnly = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).ToList()
                                                join d2 in context.Library_FieldNames.AsNoTracking().ToList()
                                                on d1.FieldNameID equals d2.FieldNameID
                                                select new { d1, d2.FieldName }).ToList();
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    var fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    listGenericCriteria[i].SelectedFieldType = null;
                    listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == true).FirstOrDefault();
                    listGenericCriteria[i].ValueFieldContainOperator = false;
                    listGenericCriteria[i].SourceFieldDisplay = 0;
                    listGenericCriteria[i].DisplayFieldToClient = false;
                    if (fieldDet != null)
                    {
                        if (fieldDet.IsCASField || fieldDet.IsChemicalName || fieldDet.IsNoteCodeField == true || fieldDet.IsPhraseField || fieldDet.IsIdentifierField || fieldDet.IsValueOther || fieldDet.IsTrueFalseField == true || fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.IsMinValOp || fieldDet.IsMaxValOp || fieldDet.IsUnit || fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].IsInternalOnly = lstInternalOnly.Where(y => y.Value == false).FirstOrDefault();
                            NotifyPropertyChanged("IsInternalOnly");
                        }
                        if (fieldDet.IsCASField)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "CAS").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsChemicalName)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Substance Name").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            var CatId = fieldDet.SubstanceNameCategoryId == 0 || fieldDet.SubstanceNameCategoryId == null ? 0 : fieldDet.SubstanceNameCategoryId;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            var lngId = fieldDet.LanguageID == 0 || fieldDet.LanguageID == null ? 0 : fieldDet.LanguageID;
                            listGenericCriteria[i].SelectedLanguage = listGenericCriteria[i].ListLanguage.Where(x => x.LanguageID == lngId).FirstOrDefault();
                            var delId = fieldDet.ChemicalNameDelimiterID == 0 || fieldDet.ChemicalNameDelimiterID == null ? 0 : fieldDet.ChemicalNameDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedLanguage");
                            NotifyPropertyChanged("SelectedDelimiter");
                        }
                        if (fieldDet.IsPhraseField == true || fieldDet.IsNoteCodeField == true)
                        {
                            if (fieldDet.IsNoteCodeField == true)
                            {
                                listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "NoteCode").FirstOrDefault();
                            }
                            if (fieldDet.IsPhraseField == true)
                            {
                                listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Phrase").FirstOrDefault();
                            }
                            NotifyPropertyChanged("SelectedFieldType");
                            var phrCatId = fieldDet.PhraseCategoryID == 0 || fieldDet.PhraseCategoryID == null ? 0 : fieldDet.PhraseCategoryID;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == phrCatId).FirstOrDefault();
                            var lngId = fieldDet.LanguageID == 0 || fieldDet.LanguageID == null ? 0 : fieldDet.LanguageID;
                            listGenericCriteria[i].SelectedLanguage = listGenericCriteria[i].ListLanguage.Where(x => x.LanguageID == lngId).FirstOrDefault();
                            var delId = fieldDet.MultiValuePhraseDelimiterID == 0 || fieldDet.MultiValuePhraseDelimiterID == null ? 0 : fieldDet.MultiValuePhraseDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            var dateForId = fieldDet.DateFormatID == 0 || fieldDet.DateFormatID == null ? 0 : fieldDet.DateFormatID;
                            listGenericCriteria[i].SelectedDateFormat = listGenericCriteria[i].ListDateFormat.Where(x => x.DateFormatID == dateForId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedLanguage");
                            NotifyPropertyChanged("SelectedDelimiter");
                            NotifyPropertyChanged("SelectedDateFormat");
                        }
                        if (fieldDet.IsIdentifierField == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Identifier").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            var CatId = fieldDet.IdentifierTypeID == 0 || fieldDet.IdentifierTypeID == null ? 0 : fieldDet.IdentifierTypeID;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            var delId = fieldDet.MultiValueIdentifierDelimiterID == 0 || fieldDet.MultiValueIdentifierDelimiterID == null ? 0 : fieldDet.MultiValueIdentifierDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == delId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            NotifyPropertyChanged("SelectedDelimiter");
                        }
                        if (fieldDet.IsValueOther == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Value Other").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsTrueFalseField == true)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "True False").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                            string[] trueFalsChar = string.IsNullOrWhiteSpace(fieldDet.AllowedTrueFalseValues) ? new string[] { } : fieldDet.AllowedTrueFalseValues.Split(',');
                            foreach (string t in trueFalsChar)
                            {
                                listGenericCriteria[i].ListAllowedTrueFalse.Where(x => x.Character == t.Trim()).FirstOrDefault().IsSelected = true;
                            }
                            listGenericCriteria[i].SelectedTrueFalse = fieldDet.AllowedTrueFalseValues;
                            NotifyPropertyChanged("SelectedTrueFalse");
                            NotifyPropertyChanged("ListAllowedTrueFalse");
                        }
                        if (fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.IsMinValOp || fieldDet.IsMaxValOp || fieldDet.IsUnit || fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].SelectedFieldType = listGenericCriteria[i].ListFieldType.Where(x => x.CboValue == "Threshold").FirstOrDefault();
                            NotifyPropertyChanged("SelectedFieldType");
                        }
                        if (fieldDet.IsMinValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");
                            NotifyPropertyChanged("SelectedCatgory");

                        }
                        if (fieldDet.IsMaxValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");
                            NotifyPropertyChanged("SelectedCatgory");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");

                        }
                        if (fieldDet.IsUnit)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Value Range").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            var rangeDel = fieldDet.RangeDelimiterID == 0 || fieldDet.RangeDelimiterID == null ? 0 : fieldDet.RangeDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == rangeDel).FirstOrDefault();
                            NotifyPropertyChanged("SelectedDelimiter");
                            listGenericCriteria[i].SelectedConsiderSingleValues = listGenericCriteria[i].ListConsiderSingleValues.Where(x => x.CboValue == fieldDet.ConsiderSingleValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderSingleValues");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");
                        }
                        if (listGenericCriteria[i].SelectedFieldType.CboValue == "Threshold")
                        {
                            if (fieldDet.IsMinValOp)
                            {
                                listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdType");
                            }
                            if (fieldDet.IsMaxValOp)
                            {
                                listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdType");
                            }
                        }

                    }
                }
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    var fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    if (fieldDet != null && listGenericCriteria[i].SelectedFieldType != null)
                    {
                        if (listGenericCriteria[i].SelectedFieldType.CboValue == "Threshold")
                        {
                            if (fieldDet.IsMinValue || fieldDet.IsMaxValue || fieldDet.ValueFieldContainsRanges)
                            {
                                if ((fieldDet.UnitID != 0) && (fieldDet.UnitID != null))
                                {
                                    listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Manual Unit Definition").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdUnitType");
                                    listGenericCriteria[i].SelectedUnit = listGenericCriteria[i].ListUnit.Where(x => x.UnitID == fieldDet.UnitID).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedUnit");
                                }
                                //if ((fieldDet.Operator_ValueFieldID != 0) && (fieldDet.Operator_ValueFieldID != null))
                                //{
                                //    listGenericCriteria[i].SelectedUnit = listGenericCriteria[i].ListUnit.Where(x => x.UnitID == fieldDet.UnitID).FirstOrDefault();
                                //    NotifyPropertyChanged("SelectedUnit");
                                //}
                                if ((fieldDet.UnitFieldNameID != 0) && (fieldDet.UnitFieldNameID != null) && ((fieldDet.UnitID == 0) || (fieldDet.UnitID == null)))
                                {
                                    listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit FieldName").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdUnitType");
                                    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.UnitFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                                    listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedMaxFieldName");
                                }

                                if (fieldDet.UnitDelimiterID != null)
                                {
                                    listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit Delimitor").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdUnitType");
                                    listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.Delimiter == " ").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedDelimiter");
                                }
                                if ((fieldDet.UnitID == null) && (fieldDet.UnitFieldNameID == null) && (fieldDet.UnitDelimiterID == null))
                                {
                                    listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "No Unit").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdUnitType");
                                }
                                if (fieldDet.ValueFieldContainsOperators)
                                {
                                    listGenericCriteria[i].ValueFieldContainOperator = fieldDet.ValueFieldContainsOperators;
                                    NotifyPropertyChanged("ValueFieldContainOperator");
                                }
                                if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMinValue == true)
                                {
                                    listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdOperatorType");
                                    listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedOperator");
                                }
                                if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMaxValue == true)
                                {
                                    listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdOperatorType");
                                    listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedOperator");
                                }
                                if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMinValue == true)
                                {
                                    listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdOperatorType");
                                    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                    listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedOperatorFieldName");
                                }
                                if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMaxValue == true)
                                {
                                    listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                    NotifyPropertyChanged("SelectedThresholdOperatorType");
                                    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                    listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                    NotifyPropertyChanged("SelectedOperatorFieldName");
                                }
                            }
                            //else
                            //{
                            //    //if ((fieldDet.Operator_ValueFieldID != 0) && (fieldDet.Operator_ValueFieldID != null))
                            //    //{
                            //    //    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.Operator_ValueFieldID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                            //    //    listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                            //    //    NotifyPropertyChanged("SelectedMaxFieldName");
                            //    //}
                            //}
                        }
                    }
                }
            }
        }
        public List<string> GetColumnNames(string tableName, string dbaseConnString)
        {
            List<string> colList = new List<string>();
            if (string.IsNullOrEmpty(dbaseConnString) == false)
            {
                string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source = " + dbaseConnString;
                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM DATA", conStr);
                DataTable dtSchema = adapter.FillSchema(new DataTable(), SchemaType.Source);
                if (dtSchema != null)
                {
                    int cnt = 0;
                    for (int i = 0; i < dtSchema.Columns.Count; i++)
                    {
                        if (dtSchema.Columns[i].AutoIncrement == false)
                        {
                            cnt += 1;
                            colList.Add(dtSchema.Columns[i].ColumnName.ToUpper().Trim().Replace("\n", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty));
                            GetAllColumnNamesFromDataAddInfo.Add((dtSchema.Columns[i].ColumnName.ToUpper().Trim().Replace("\n", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty), cnt));
                        }
                        else
                        {
                            IdentityColumn = dtSchema.Columns[i].ColumnName.ToUpper().Trim().Replace("\n", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty);
                        }
                    }
                }
            }
            return colList;
        }

        public QcCheckUC_VM qcCheckUC_Context { get; set; }

        public void BtnSaveAddInfoExecute(object o)
        {
            selectedItemCombo = ListComboxItem.Where(x => x.text == "All").FirstOrDefault();
            NotifyPropertyChanged("selectedItemCombo");
            string msgFlag = string.Empty;
            for (int i = 0; i < listGenericCriteria.Count(); i++)
            {
                if (listGenericCriteria[i].SelectedFieldType == null && listGenericCriteria[i].IsInternalOnly.Value == false)
                {
                    msgFlag += listGenericCriteria[i].ColumnName + " - choose its fieldType or Internalonly" + Environment.NewLine;
                }
                if (listGenericCriteria[i].SelectedFieldType != null)
                {
                    switch (listGenericCriteria[i].SelectedFieldType.CboValue)
                    {
                        case "NoteCode":
                        case "Phrase":
                            if (listGenericCriteria[i].SelectedCatgory == null || listGenericCriteria[i].SelectedLanguage == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Category & Language" + Environment.NewLine;
                            }
                            else
                            {
                                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                                {
                                    msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Category" + Environment.NewLine;
                                }
                                if (listGenericCriteria[i].SelectedCatgory.Category != "--Select--" && (listGenericCriteria[i].SelectedCatgory.IsDateType == true) && listGenericCriteria[i].SelectedDateFormat == null)
                                {
                                    msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Date Format" + Environment.NewLine;
                                }
                            }
                            break;

                        case "Identifier":
                            if (listGenericCriteria[i].SelectedCatgory == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Category" + Environment.NewLine;
                            }
                            else
                            {
                                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                                {
                                    msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Category" + Environment.NewLine;
                                }
                            }
                            break;

                        case "Substance Name":
                            if (listGenericCriteria[i].SelectedLanguage == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - Need to Select Language" + Environment.NewLine;
                            }
                            break;
                        case "True False":
                            if (string.IsNullOrEmpty(listGenericCriteria[i].SelectedTrueFalse))
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to select value in Allowed TrueFalse" + Environment.NewLine;
                            }
                            else
                            {
                                string[] str = listGenericCriteria[i].SelectedTrueFalse.Split(',');
                                if (str.Length > 2)
                                {
                                    msgFlag += listGenericCriteria[i].ColumnName + " - only select two value in Allowed True False" + Environment.NewLine;
                                }
                            }
                            break;
                        case "Threshold":
                            if (listGenericCriteria[i].SelectedThresholdType == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to select only one out of Minval/MaxVal/Range/Unit/Operator" + Environment.NewLine;
                            }
                            if (listGenericCriteria[i].SelectedThresholdType != null)
                            {
                                if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range") && listGenericCriteria[i].SelectedThresholdUnitType != null && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != "No Unit" && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != "Unit Delimitor")
                                {
                                    int chkCountNew = listGenericCriteria[i].SelectedUnit == null ? 0 : 1;
                                    chkCountNew += listGenericCriteria[i].SelectedMaxFieldName == null ? 0 : 1;
                                    if (chkCountNew == 0)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - need to select Unit ID/UnitFieldName" + Environment.NewLine;
                                    }
                                }
                                //if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
                                //{
                                //    if (listGenericCriteria[i].SelectedMaxFieldName == null)
                                //    {
                                //        msgFlag += listGenericCriteria[i].ColumnName + " - No MinValue/MaxValue associate with Operator" + Environment.NewLine;
                                //    }
                                //}

                                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range")
                                {
                                    if (listGenericCriteria[i].SelectedDelimiter == null)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - need to choose Delimiter " + Environment.NewLine;
                                    }
                                    else
                                    {
                                        if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--" || listGenericCriteria[i].SelectedDelimiter.Delimiter == null)
                                        {
                                            msgFlag += listGenericCriteria[i].ColumnName + " - need to choose Delimiter" + Environment.NewLine;
                                        }
                                    }

                                    if (listGenericCriteria[i].SelectedConsiderSingleValues == null)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - need to choose consider single value as" + Environment.NewLine;
                                    }
                                    else
                                    {
                                        if (listGenericCriteria[i].SelectedConsiderSingleValues.CboValue == "--Select--" || listGenericCriteria[i].SelectedConsiderSingleValues.CboValue == null)
                                        {
                                            msgFlag += listGenericCriteria[i].ColumnName + " - need to choose consider single value as" + Environment.NewLine;
                                        }
                                    }
                                }

                                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value")
                                {
                                    if (listGenericCriteria[i].SelectedConsiderNullValues == null)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - Choose ConsiderNullValue As, its mandatory" + Environment.NewLine;
                                    }
                                    else
                                    {
                                        if (listGenericCriteria[i].SelectedConsiderNullValues.CboValue == "--Select--" || listGenericCriteria[i].SelectedConsiderNullValues.CboValue == null)
                                        {
                                            msgFlag += listGenericCriteria[i].ColumnName + " - Choose ConsiderNullValue As, its mandatory" + Environment.NewLine;
                                        }
                                    }
                                }
                                if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range") && listGenericCriteria[i].SelectedCatgory == null)
                                {
                                    msgFlag += listGenericCriteria[i].ColumnName + " - should choose Threshold Category" + Environment.NewLine;
                                }

                                int assciatewithMaxMin = 0;
                                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Unit")
                                {
                                    var unitColumnName = listGenericCriteria[i].ColumnName;
                                    for (int j = 0; j < listGenericCriteria.Count(); j++)
                                    {
                                        if (listGenericCriteria[j].SelectedMaxFieldName != null && listGenericCriteria[j].SelectedMaxFieldName.CboValue == unitColumnName.Trim())
                                        {
                                            assciatewithMaxMin += 1;
                                        }
                                    }

                                    if (assciatewithMaxMin == 0)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - Unit Field is not associate with any Min/Max/Value Range Field" + Environment.NewLine;
                                    }
                                    if (assciatewithMaxMin > 2)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - Unit Field associate more than 1" + Environment.NewLine;
                                    }
                                }
                                int OpwithMaxMin = 0;
                                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
                                {
                                    var OpColumnName = listGenericCriteria[i].ColumnName;
                                    for (int j = 0; j < listGenericCriteria.Count(); j++)
                                    {
                                        if (listGenericCriteria[j].SelectedOperatorFieldName != null && listGenericCriteria[j].SelectedOperatorFieldName.CboValue == OpColumnName.Trim())
                                        {
                                            OpwithMaxMin += 1;
                                        }
                                    }

                                    if (OpwithMaxMin == 0)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - Min/Max Operator Field is not associate with any Min/Max Field" + Environment.NewLine;
                                    }
                                    if (OpwithMaxMin > 1)
                                    {
                                        msgFlag += listGenericCriteria[i].ColumnName + " - Operator Field is associate more than 1" + Environment.NewLine;
                                    }
                                }
                            }
                            break;

                    }
                }
            }
            if (msgFlag != string.Empty)
            {
                var res = System.Windows.Forms.MessageBox.Show("There are still some errors, do you still want to save" + Environment.NewLine + Environment.NewLine + msgFlag, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.No)
                {
                    return;
                }
            }
            var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
            var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var newList = false;
            if (QsidID == 0)
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(dtRawChk1.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return;
                }
                newList = true;
            }
            QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var chkAlreadyDeleted = objCommon.DbaseQueryReturnStringSQL("select top 1 b.QSID from Log_QSID_Changes a, Library_QSIDs b where status ='D' and a.QSID_ID=b.QSID_ID and upper(b.QSID) ='" + dtRawChk1.Rows[0]["QSID"].ToString().ToUpper() + "'", CommonListConn);
            if (!string.IsNullOrEmpty(chkAlreadyDeleted))
            {
                _notifier.ShowError("Deleted list are not allowed to normalized");
                return;
            }
            Log_Sessions chkExist = objCommon.CheckIfAnyoneNormalizing(QsidID);
            if (chkExist != null)
            {
                using (var context = new CRAModel())
                {
                    var userName = context.Library_Users.AsNoTracking().Where(x => x.VeriskID == chkExist.UserName).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Normalizing by " + userName);
                    return;
                }
            }
            var chkExist2 = _objLibraryFunction_Service.CheckActiveCheckOutButton(QsidID);
            if (chkExist2 != string.Empty && chkExist2 != Environment.UserName)
            {
                using (var context = new CRAModel())
                {
                    var userName = context.Library_Users.Where(x => x.VeriskID.ToUpper() == chkExist2.ToUpper()).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Check-Out " + userName);
                    return;
                }
            }
            var sessTypeId = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select SessionTypeId from Library_SessionTypes where sessionType ='Normalization - AdditionalInfo'", CommonListConn));
            var newSession = _objLibraryFunction_Service.CreateNewSession(QsidID, CommonFilepath, sessTypeId);

            AddDataInMapQSIDLibrary(QsidID, newSession);

            //if any manualunit&unitfieldNameid both exits at one row
            _objLibraryFunction_Service.AddDeleteVirtualFieldInDataDictionary(QsidID, newSession);
            _objLibraryFunction_Service.AddDeleteVirtualFieldInMapFieldDataType(QsidID, newSession);
            _objLibraryFunction_Service.AddDeleteVirtualFieldInMapProductFields(QsidID, newSession);

            objCommon.DbaseQueryReturnStringSQL("Update Log_Sessions set SessionEnd_TimeStamp ='" + objCommon.ESTTime() + "' where QSID_ID ='" + QsidID + "' and UserName ='" + Environment.UserName + "' and sessiontypeid = " + sessTypeId + " and SessionEnd_TimeStamp is null and QSID_ID is not null", CommonListConn);

            using (CRAModel context = new CRAModel())
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("Data Added Successfully");
                });
                var LtMapQSIDFieldNames = context.Map_QSID_FieldName.Where(x => x.QSID_ID == QsidID).ToList();
                LoaderAddInfo = Visibility.Visible;
                NotifyPropertyChanged("LoaderAddInfo");
                Task.Run(() =>
                {
                    CheckExistingValues(QsidID);
                    LoaderAddInfo = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderAddInfo");
                });
            }

        }
        private void insertIntoLibPhrases(string PhraseValue)
        {
            List<Library_Phrases> lstMain = new List<Library_Phrases>();
            var lib = new Library_Phrases
            {
                InternalOnly = false,
                Phrase = PhraseValue,
                PhraseHash = objCommon.GenerateSHA256String(PhraseValue),
                IsActive = true,
                LanguageID = 1,
                TimeStamp = objCommon.ESTTime(),
            };
            lstMain.Add(lib);
            _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
        }
        private bool flagDisplay2ClientOne;
        public void AddDataInMapQSIDLibrary(int qsidID, Int32 sId)
        {
            using (CRAModel context = new CRAModel())
            {
                int mapQSIDFieldNameId = this.objCommon.GetMaxAutoField("Map_QSID_FieldName");
                List<Map_QSID_FieldName> lstFinal = new List<Map_QSID_FieldName>();
                List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();
                List<Map_QSID_FieldName> mapQSIDFieldNames = context.Map_QSID_FieldName.Where(x => x.QSID_ID == qsidID).ToList();
                List<ILibUnits> libraryUnits = context.Library_Units.ToList().Select(x => new ILibUnits { UnitID = x.UnitID, Unit = ConvertSpecialCharacter(this.objCommon.UnitReplace(x.Unit)) }).ToList();
                var lngLst = context.Library_Languages.AsNoTracking().ToList();
                var libIdentifierCategories = context.Library_IdentifierCategories.AsNoTracking().ToList();
                var libPhraseCategories = context.Library_PhraseCategories.AsNoTracking().ToList();
                var libDelimiters = context.Library_Delimiters.AsNoTracking().ToList();
                var libDateFormat = context.Library_DateFormat.AsNoTracking().ToList();
                var libSubstanceNameCategories = context.Library_SubstanceNameCategories.AsNoTracking().ToList();
                var libThresholdCategories = context.Library_ThresholdCategories.AsNoTracking().ToList();
                var libStripCharacters = context.Library_StripCharacters.AsNoTracking().ToList();
                var libOperators = context.Library_Operators.AsNoTracking().ToList();
                SpecialCharacters = new Dictionary<string, char>();
                var result1 = context.Library_SpecialCharacters.ToList().Select(t => new { spch = @"\u" + t.SpecialCharacter.Trim(), t.ConversionCharacter }).ToList();
                result1.ForEach(x =>
                {
                    int p = int.Parse(x.spch.Substring(2), System.Globalization.NumberStyles.HexNumber);
                    string strvalue = Char.ConvertFromUtf32(p);
                    SpecialCharacters.Add(strvalue, Convert.ToChar(x.ConversionCharacter));
                });
                int counter = 0;
                int srtOrder = 0;
                flagDisplay2ClientOne = false;
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].DisplayFieldToClient != null && (bool)listGenericCriteria[i].DisplayFieldToClient && listGenericCriteria[i].SourceFieldDisplay != null)
                    {
                        flagDisplay2ClientOne = true;
                        break;
                    }
                }
                var sessID = sId;
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].IsInternalOnly.Value == false)
                    {
                        counter += 1;
                        srtOrder = counter;
                    }
                    else
                    {
                        srtOrder = 999;
                    }

                    string trueFalsValue = string.IsNullOrWhiteSpace(listGenericCriteria[i].SelectedTrueFalse) ? null : listGenericCriteria[i].SelectedTrueFalse;
                    string actualFieldName = listGenericCriteria[i].ColumnName.ToString().Trim().ToUpper();
                    string fieldName = string.Empty;
                    int childSortOrd = 0;
                    int parentId = 0;
                    fieldName = actualFieldName;
                    int fieldId = Convert.ToInt32(context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.Trim().ToUpper() == fieldName).Select(y => y.FieldNameID).FirstOrDefault());
                    if (fieldId == 0)
                    {
                        AddFieldNamesToLibrary(actualFieldName);
                        fieldId = Convert.ToInt32(context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.Trim().ToUpper() == fieldName).Select(y => y.FieldNameID).FirstOrDefault());
                    }
                    Map_QSID_FieldName itemToUpdate = mapQSIDFieldNames.FirstOrDefault(x => x.FieldNameID == fieldId);
                    int? lnID = listGenericCriteria[i].SelectedLanguage == null || listGenericCriteria[i].SelectedLanguage.LanguageName == null ? (int?)null : Convert.ToInt32(lngLst.Where(x => x.LanguageName == listGenericCriteria[i].SelectedLanguage.LanguageName.ToString().Trim()).Select(y => y.LanguageID).FirstOrDefault());

                    if (itemToUpdate == null)
                    {
                        if (listGenericCriteria[i].SelectedOperator != null && listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value")
                        {
                            var MaxOpFieldNameID = OpFieldNameId(listGenericCriteria, i);
                            var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == MaxOpFieldNameID);
                            if (chkExt == 0)
                            {
                                AddMaxOpVirtualField(qsidID, srtOrder, sessID, (int)MaxOpFieldNameID, mapQSIDFieldNameId);
                                counter += 1;
                                srtOrder = counter;
                                mapQSIDFieldNameId += 1;
                            }
                            else
                            {
                                var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == MaxOpFieldNameID);
                                UpdateMaxOpVirtualField(qsidID, srtOrder, sessID, (int)MaxOpFieldNameID, needToUpdate, context);
                                counter += 1;
                                srtOrder = counter;
                            }
                        }
                        if (listGenericCriteria[i].SelectedOperator != null && listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value")
                        {
                            var MinOpFieldNameID = UnitFieldNameId(listGenericCriteria, i);
                            var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == MinOpFieldNameID);
                            if (chkExt == 0)
                            {
                                AddMinOpVirtualField(qsidID, srtOrder, sessID, (int)MinOpFieldNameID, mapQSIDFieldNameId);
                                counter += 1;
                                srtOrder = counter;
                                mapQSIDFieldNameId += 1;
                            }
                            else
                            {
                                var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == MinOpFieldNameID);
                                UpdateMinOpVirtualField(qsidID, srtOrder, sessID, (int)MinOpFieldNameID, needToUpdate, context);
                                counter += 1;
                                srtOrder = counter;
                            }
                        }

                        Log_Map_QSID_FieldName mapLogQSIDFieldName = this.AddInLogMapQsidFieldName(listGenericCriteria, mapQSIDFieldNameId, fieldId, i, libPhraseCategories, libIdentifierCategories, libSubstanceNameCategories, lnID, libraryUnits, libDelimiters, trueFalsValue, libThresholdCategories, srtOrder, childSortOrd, parentId, qsidID, sessID, libDateFormat, libOperators, context);
                        Map_QSID_FieldName mapQSIDFieldName = this.AddInMapQsidFieldName1(listGenericCriteria, mapQSIDFieldNameId, fieldId, i, null, libPhraseCategories, libIdentifierCategories, libSubstanceNameCategories, lnID, libraryUnits, libDelimiters, trueFalsValue, libThresholdCategories, srtOrder, childSortOrd, parentId, qsidID, libDateFormat, libOperators, context);

                        lstLogFinal.Add(mapLogQSIDFieldName);
                        lstFinal.Add(mapQSIDFieldName);
                        if (listGenericCriteria[i].SelectedUnit != null && listGenericCriteria[i].SelectedMaxFieldName != null && listGenericCriteria[i].SelectedMaxFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null)
                        {
                            bool oneMoreTime = false;
                            var UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i);
                            var unitFieldName = listGenericCriteria[i].SelectedMaxFieldName.CboValue;
                            for (int k = i + 1; k < listGenericCriteria.Count; k++)
                            {
                                if (listGenericCriteria[k].SelectedMaxFieldName != null && listGenericCriteria[k].SelectedMaxFieldName.CboValue == unitFieldName)
                                {
                                    oneMoreTime = true;
                                    break;
                                }
                            }
                            if (oneMoreTime == false)
                            {
                                var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == (int)UnitFieldNameID);
                                if (chkExt == 0)
                                {
                                    counter += 1; srtOrder = counter;
                                    mapQSIDFieldNameId += 1;
                                    AddVirtualField(qsidID, srtOrder, sessID, (int)UnitFieldNameID, mapQSIDFieldNameId);
                                }
                                else
                                {
                                    counter += 1;
                                    srtOrder = counter;
                                    var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == (int)UnitFieldNameID);
                                    UpdateVirtualField(qsidID, srtOrder, sessID, (int)UnitFieldNameID, needToUpdate, context);
                                }
                            }
                        }
                        mapQSIDFieldNameId += 1;
                    }
                    else
                    {
                        if (listGenericCriteria[i].SelectedOperator != null && listGenericCriteria[i].SelectedOperator.Operator != null && listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value")
                        {
                            var MaxOpFieldNameID = OpFieldNameId(listGenericCriteria, i);
                            var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == MaxOpFieldNameID);
                            if (chkExt == 0)
                            {
                                AddMaxOpVirtualField(qsidID, srtOrder, sessID, (int)MaxOpFieldNameID, mapQSIDFieldNameId);
                                counter += 1;
                                srtOrder = counter;
                                mapQSIDFieldNameId += 1;
                            }
                            else
                            {
                                var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == MaxOpFieldNameID);
                                UpdateMaxOpVirtualField(qsidID, srtOrder, sessID, (int)MaxOpFieldNameID, needToUpdate, context);
                                counter += 1;
                                srtOrder = counter;
                            }
                        }
                        if (listGenericCriteria[i].SelectedOperator != null && listGenericCriteria[i].SelectedOperator.Operator != null && listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value")
                        {
                            var MinOpFieldNameID = OpFieldNameId(listGenericCriteria, i);
                            var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == MinOpFieldNameID);
                            if (chkExt == 0)
                            {
                                AddMinOpVirtualField(qsidID, srtOrder, sessID, (int)MinOpFieldNameID, mapQSIDFieldNameId);
                                counter += 1;
                                srtOrder = counter;
                                mapQSIDFieldNameId += 1;
                            }
                            else
                            {
                                var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == MinOpFieldNameID);
                                UpdateMinOpVirtualField(qsidID, srtOrder, sessID, (int)MinOpFieldNameID, needToUpdate, context);
                                counter += 1;
                                srtOrder = counter;
                            }
                        }
                        
                        Log_Map_QSID_FieldName mapLogQSIDFieldName = this.UpdateLogMapQsidFieldName(listGenericCriteria, itemToUpdate, i, libPhraseCategories, libIdentifierCategories, libSubstanceNameCategories, lnID, libraryUnits, libDelimiters, trueFalsValue, libThresholdCategories, srtOrder, childSortOrd, parentId, qsidID, sessID, libDateFormat, libOperators, context);
                        UPdateCaseMapQsidFieldName(listGenericCriteria, itemToUpdate, mapLogQSIDFieldName, lstLogFinal, i, libPhraseCategories, libIdentifierCategories, libSubstanceNameCategories, lnID, libraryUnits, libDelimiters, trueFalsValue, libThresholdCategories, context, srtOrder, childSortOrd, parentId, qsidID, sessID, libDateFormat, libOperators);
                        
                        if (listGenericCriteria[i].SelectedUnit != null && listGenericCriteria[i].SelectedUnit.Unit != null && listGenericCriteria[i].SelectedMaxFieldName != null && listGenericCriteria[i].SelectedMaxFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null)
                        {
                            bool oneMoreTime = false;
                            var UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i);
                            var unitFieldName = listGenericCriteria[i].SelectedMaxFieldName.CboValue;
                            for (int k = i + 1; k < listGenericCriteria.Count; k++)
                            {
                                if (listGenericCriteria[k].SelectedMaxFieldName != null && listGenericCriteria[k].SelectedMaxFieldName.CboValue == unitFieldName)
                                {
                                    oneMoreTime = true;
                                    break;
                                }
                            }
                            if (oneMoreTime == false)
                            {
                                var chkExt = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).Count(x => x.FieldNameID == (int)UnitFieldNameID);
                                if (chkExt == 0)
                                {
                                    counter += 1; srtOrder = counter;
                                    mapQSIDFieldNameId += 1;
                                    AddVirtualField(qsidID, srtOrder, sessID, (int)UnitFieldNameID, mapQSIDFieldNameId);
                                }
                                else
                                {
                                    counter += 1;
                                    srtOrder = counter;
                                    var needToUpdate = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID).FirstOrDefault(x => x.FieldNameID == (int)UnitFieldNameID);
                                    UpdateVirtualField(qsidID, srtOrder, sessID, (int)UnitFieldNameID, needToUpdate, context);
                                }
                            }
                        }
                    }
                }

                lstFinal = lstFinal.Distinct().ToList();
                _objLibraryFunction_Service.BulkIns<Map_QSID_FieldName>(lstFinal);
                _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
                _objLibraryFunction_Service.AddInLogSessionDetail("Additional-Info", "Saved", sessID);
            }
        }

        private void AddVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, int mapQSIDFieldNameId)
        {
            List<Map_QSID_FieldName> lstFinal = new List<Map_QSID_FieldName>();
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();

            var mapQSIDFieldName = new Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = false,
                IsUnit = true,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
            };
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = false,
                IsUnit = true,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
                Status = "A",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            lstLogFinal.Add(mapLogQSIDFieldName);
            lstFinal.Add(mapQSIDFieldName);

            _objLibraryFunction_Service.BulkIns<Map_QSID_FieldName>(lstFinal);
            _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
        }
        private void AddMaxOpVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, int mapQSIDFieldNameId)
        {
            List<Map_QSID_FieldName> lstFinal = new List<Map_QSID_FieldName>();
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();

            var mapQSIDFieldName = new Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = true,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
            };
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = true,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
                Status = "A",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            lstLogFinal.Add(mapLogQSIDFieldName);
            lstFinal.Add(mapQSIDFieldName);

            _objLibraryFunction_Service.BulkIns<Map_QSID_FieldName>(lstFinal);
            _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
        }
        private void AddMinOpVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, int mapQSIDFieldNameId)
        {
            List<Map_QSID_FieldName> lstFinal = new List<Map_QSID_FieldName>();
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();

            var mapQSIDFieldName = new Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = true,
                IsMaxValOp = false,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
            };
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = true,
                IsMaxValOp = false,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 25,
                Status = "A",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            lstLogFinal.Add(mapLogQSIDFieldName);
            lstFinal.Add(mapQSIDFieldName);

            _objLibraryFunction_Service.BulkIns<Map_QSID_FieldName>(lstFinal);
            _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
        }

        private void UpdateVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, Map_QSID_FieldName prevMapFields, CRAModel context)
        {
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = prevMapFields.MapQSIDFieldName_ID,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = false,
                IsUnit = true,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 100,
                Status = "C",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            if (prevMapFields.FieldSortOrder != maxSortOrd)
            {
                prevMapFields.FieldSortOrder = maxSortOrd;
                List<Map_QSID_FieldName> nMap = new List<Map_QSID_FieldName>();
                nMap.Add(prevMapFields);
                EFBatchOperation.For(context, context.Map_QSID_FieldName).UpdateAll(nMap, x => x.ColumnsToUpdate(c => c.FieldSortOrder));
                context.SaveChanges();
                lstLogFinal.Add(mapLogQSIDFieldName);
                _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
            }
        }
        private void UpdateMaxOpVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, Map_QSID_FieldName prevMapFields, CRAModel context)
        {
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = prevMapFields.MapQSIDFieldName_ID,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = false,
                IsMaxValOp = true,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 100,
                Status = "C",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            if (prevMapFields.FieldSortOrder != maxSortOrd)
            {
                prevMapFields.FieldSortOrder = maxSortOrd;
                List<Map_QSID_FieldName> nMap = new List<Map_QSID_FieldName>();
                nMap.Add(prevMapFields);
                EFBatchOperation.For(context, context.Map_QSID_FieldName).UpdateAll(nMap, x => x.ColumnsToUpdate(c => c.FieldSortOrder));
                context.SaveChanges();
                lstLogFinal.Add(mapLogQSIDFieldName);
                _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
            }
        }
        private void UpdateMinOpVirtualField(int QsidID, int maxSortOrd, int newSession, int virtualField, Map_QSID_FieldName prevMapFields, CRAModel context)
        {
            List<Log_Map_QSID_FieldName> lstLogFinal = new List<Log_Map_QSID_FieldName>();
            var mapLogQSIDFieldName = new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = prevMapFields.MapQSIDFieldName_ID,
                FieldNameID = virtualField,
                IsInternalOnly = false,
                IsCASField = false,
                IsChemicalName = false,
                IsIdentifierField = false,
                IsNoteCodeField = false,
                IsPhraseField = false,
                IsValueOther = false,
                IsTrueFalseField = false,
                IsMinValue = false,
                IsMaxValue = false,
                IsMinValOp = true,
                IsMaxValOp = false,
                IsUnit = false,
                ValueFieldContainsOperators = false,
                MultiValuePhrase = false,
                MultiValueIdentifier = false,
                ValueFieldContainsRanges = false,
                ValueFieldContainsUnits = false,
                QSID_ID = QsidID,
                FieldSortOrder = maxSortOrd,
                DisplayField2Client = false,
                SourceFieldDisplayOrder = 100,
                Status = "C",
                SessionID = newSession,
                TimeStamp = objCommon.ESTTime(),
            };

            if (prevMapFields.FieldSortOrder != maxSortOrd)
            {
                prevMapFields.FieldSortOrder = maxSortOrd;
                List<Map_QSID_FieldName> nMap = new List<Map_QSID_FieldName>();
                nMap.Add(prevMapFields);
                EFBatchOperation.For(context, context.Map_QSID_FieldName).UpdateAll(nMap, x => x.ColumnsToUpdate(c => c.FieldSortOrder));
                context.SaveChanges();
                lstLogFinal.Add(mapLogQSIDFieldName);
                _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstLogFinal);
            }
        }


        private Map_QSID_FieldName AddInMapQsidFieldName1(List<GenericCriteria_VM> listGenericCriteria, int mapQSIDFieldNameId, int fieldId, int i, string defaultComboValue, List<Library_PhraseCategories> libraryPhraseCategories, List<Library_IdentifierCategories> libraryIdentifierCategories, List<Library_SubstanceNameCategories> librarySubstanceNameCategories, int? lnID, List<ILibUnits> libraryUnits, List<Library_Delimiters> libraryDelimiters, string trueFalsValue, List<Library_ThresholdCategories> libraryThresholdCategories, int srtOrder, int? childSortOrder, int? parentId, int qsidID, List<Library_DateFormat> libDateFormat, List<Library_Operators> libOp, CRAModel context)
        {
            return new Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = fieldId,
                IsInternalOnly = listGenericCriteria[i].IsInternalOnly.Value,
                IsCASField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "CAS",
                IsChemicalName = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name",
                IsIdentifierField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier",
                IsNoteCodeField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode",
                IsPhraseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase",
                IsValueOther = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Value Other",
                IsTrueFalseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "True False",
                PhraseCategoryID = PhraseCategoryId(listGenericCriteria, i, libraryPhraseCategories),
                IdentifierTypeID = IdentifierTypeId(listGenericCriteria, i, libraryIdentifierCategories),
                SubstanceNameCategoryId = SubstanceNameCategoryId(listGenericCriteria, i, librarySubstanceNameCategories),
                LanguageID = lnID,
                IsMinValue = IsMinValue(listGenericCriteria, i),
                IsMaxValue = IsMaxValue(listGenericCriteria, i),
                IsMinValOp = IsMinValOp(listGenericCriteria, i),
                IsMaxValOp = IsMaxValOp(listGenericCriteria, i),
                IsUnit = IsUnit(listGenericCriteria, i),
                UnitID = UnitId(listGenericCriteria, i, libraryUnits),
                UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i),
                Operator_ValueFieldID = OperatorValueFieldId(listGenericCriteria, i),
                RangeDelimiterID = RangeDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsOperators = ValueFieldContainsOperators(listGenericCriteria, i),
                UnitDelimiterID = UnitDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ConsiderSingleValueAs = ConsiderSingleValueAs(listGenericCriteria, i),
                ConsiderNullValueAs = ConsiderNullValueAs(listGenericCriteria, i),
                AllowedTrueFalseValues = AllowedTrueFalseValues(listGenericCriteria, i),
                MultiValuePhrase = MultiValuePhrase(listGenericCriteria, i),
                MultiValuePhraseDelimiterID = MultiValuePhraseDelimiterId(listGenericCriteria, i, libraryDelimiters),
                MultiValueIdentifier = MultiValueIdentifier(listGenericCriteria, i),
                MultiValueIdentifierDelimiterID = MultiValueIdentifierDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ChemicalNameDelimiterID = ChemicalNameDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsRanges = ValueFieldContainsRanges(listGenericCriteria, i),
                ValueFieldContainsUnits = ValueFieldContainsUnits(listGenericCriteria, i),
                QSID_ID = qsidID,
                ThresholdCategory = ThresholdCategory(listGenericCriteria, i, libraryThresholdCategories),
                FieldSortOrder = srtOrder,
                DisplayField2Client = displayField2ClientVal(listGenericCriteria, i),
                SourceFieldDisplayOrder = sourceFieldDisplayOrderVal(listGenericCriteria, i),
                ParentId = parentId == 0 ? null : parentId,
                DateFormatID = DateFormatId(listGenericCriteria, i, libDateFormat),
                OperatorID = OperatorId(listGenericCriteria, i, libOp),
                OperatorFieldNameID = OperatorFieldId(listGenericCriteria, i, context),
            };
        }
        private Log_Map_QSID_FieldName AddInLogMapQsidFieldName(List<GenericCriteria_VM> listGenericCriteria, int mapQSIDFieldNameId, int fieldId, int i, List<Library_PhraseCategories> libraryPhraseCategories, List<Library_IdentifierCategories> libraryIdentifierCategories, List<Library_SubstanceNameCategories> librarySubstanceNameCategories, int? lnID, List<ILibUnits> libraryUnits, List<Library_Delimiters> libraryDelimiters, string trueFalsValue, List<Library_ThresholdCategories> libraryThresholdCategories, int srtOrder, int? childSortOrder, int? parentId, int qsidID, int sessID, List<Library_DateFormat> libDateFormat, List<Library_Operators> libOperators, CRAModel context)
        {
            return new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = mapQSIDFieldNameId,
                FieldNameID = fieldId,
                IsInternalOnly = listGenericCriteria[i].IsInternalOnly.Value,
                IsCASField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "CAS",
                IsChemicalName = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name",
                IsIdentifierField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier",
                IsNoteCodeField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode",
                IsPhraseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase",
                IsValueOther = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Value Other",
                IsTrueFalseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "True False",
                PhraseCategoryID = PhraseCategoryId(listGenericCriteria, i, libraryPhraseCategories),
                IdentifierTypeID = IdentifierTypeId(listGenericCriteria, i, libraryIdentifierCategories),
                SubstanceNameCategoryId = SubstanceNameCategoryId(listGenericCriteria, i, librarySubstanceNameCategories),
                LanguageID = lnID,
                IsMinValue = IsMinValue(listGenericCriteria, i),
                IsMaxValue = IsMaxValue(listGenericCriteria, i),
                IsMinValOp = IsMinValOp(listGenericCriteria, i),
                IsMaxValOp = IsMaxValOp(listGenericCriteria, i),
                IsUnit = IsUnit(listGenericCriteria, i),
                UnitID = UnitId(listGenericCriteria, i, libraryUnits),
                UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i),
                Operator_ValueFieldID = OperatorValueFieldId(listGenericCriteria, i),
                RangeDelimiterID = RangeDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsOperators = ValueFieldContainsOperators(listGenericCriteria, i),
                UnitDelimiterID = UnitDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ConsiderSingleValueAs = ConsiderSingleValueAs(listGenericCriteria, i),
                ConsiderNullValueAs = ConsiderNullValueAs(listGenericCriteria, i),
                AllowedTrueFalseValues = AllowedTrueFalseValues(listGenericCriteria, i),
                MultiValuePhrase = MultiValuePhrase(listGenericCriteria, i),
                MultiValuePhraseDelimiterID = MultiValuePhraseDelimiterId(listGenericCriteria, i, libraryDelimiters),
                MultiValueIdentifier = MultiValueIdentifier(listGenericCriteria, i),
                MultiValueIdentifierDelimiterID = MultiValueIdentifierDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ChemicalNameDelimiterID = ChemicalNameDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsRanges = ValueFieldContainsRanges(listGenericCriteria, i),
                ValueFieldContainsUnits = ValueFieldContainsUnits(listGenericCriteria, i),
                QSID_ID = qsidID,
                Status = "A",
                TimeStamp = objCommon.ESTTime(),
                SessionID = sessID,
                ThresholdCategory = ThresholdCategory(listGenericCriteria, i, libraryThresholdCategories),
                FieldSortOrder = srtOrder,
                DisplayField2Client = displayField2ClientVal(listGenericCriteria, i),
                SourceFieldDisplayOrder = sourceFieldDisplayOrderVal(listGenericCriteria, i),
                ParentId = parentId == 0 ? null : parentId,
                ChildSortOrder = childSortOrder == 0 ? null : childSortOrder,
                DateFormatID = DateFormatId(listGenericCriteria, i, libDateFormat),
                OperatorID = OperatorId(listGenericCriteria, i, libOperators),
                OperatorFieldNameID = OperatorFieldId(listGenericCriteria, i, context),
            };
        }

        private Log_Map_QSID_FieldName UpdateLogMapQsidFieldName(List<GenericCriteria_VM> listGenericCriteria, Map_QSID_FieldName itemToUpdate, int i, List<Library_PhraseCategories> libraryPhraseCategories, List<Library_IdentifierCategories> libraryIdentifierCategories, List<Library_SubstanceNameCategories> librarySubstanceNameCategories, int? lnID, List<ILibUnits> libraryUnits, List<Library_Delimiters> libraryDelimiters, string trueFalsValue, List<Library_ThresholdCategories> libraryThresholdCategories, int srtOrder, int? childSortOrder, int? parentId, int qsidID, int sessID, List<Library_DateFormat> libDateFormat, List<Library_Operators> libOperators, CRAModel context)
        {
            return new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = itemToUpdate.MapQSIDFieldName_ID,
                FieldNameID = itemToUpdate.FieldNameID,
                IsInternalOnly = listGenericCriteria[i].IsInternalOnly.Value,
                IsCASField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "CAS",
                IsChemicalName = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name",
                IsIdentifierField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier",
                IsNoteCodeField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode",
                IsPhraseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase",
                IsValueOther = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Value Other",
                IsTrueFalseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "True False",
                PhraseCategoryID = PhraseCategoryId(listGenericCriteria, i, libraryPhraseCategories),
                IdentifierTypeID = IdentifierTypeId(listGenericCriteria, i, libraryIdentifierCategories),
                SubstanceNameCategoryId = SubstanceNameCategoryId(listGenericCriteria, i, librarySubstanceNameCategories),
                LanguageID = lnID,
                IsMinValue = IsMinValue(listGenericCriteria, i),
                IsMaxValue = IsMaxValue(listGenericCriteria, i),
                IsMinValOp = IsMinValOp(listGenericCriteria, i),
                IsMaxValOp = IsMaxValOp(listGenericCriteria, i),
                IsUnit = IsUnit(listGenericCriteria, i),
                UnitID = UnitId(listGenericCriteria, i, libraryUnits),
                UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i),
                Operator_ValueFieldID = OperatorValueFieldId(listGenericCriteria, i),
                RangeDelimiterID = RangeDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsOperators = ValueFieldContainsOperators(listGenericCriteria, i),
                UnitDelimiterID = UnitDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ConsiderSingleValueAs = ConsiderSingleValueAs(listGenericCriteria, i),
                ConsiderNullValueAs = ConsiderNullValueAs(listGenericCriteria, i),
                AllowedTrueFalseValues = AllowedTrueFalseValues(listGenericCriteria, i),
                MultiValuePhrase = MultiValuePhrase(listGenericCriteria, i),
                MultiValuePhraseDelimiterID = MultiValuePhraseDelimiterId(listGenericCriteria, i, libraryDelimiters),
                MultiValueIdentifier = MultiValueIdentifier(listGenericCriteria, i),
                MultiValueIdentifierDelimiterID = MultiValueIdentifierDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ChemicalNameDelimiterID = ChemicalNameDelimiterId(listGenericCriteria, i, libraryDelimiters),
                ValueFieldContainsRanges = ValueFieldContainsRanges(listGenericCriteria, i),
                ValueFieldContainsUnits = ValueFieldContainsUnits(listGenericCriteria, i),
                QSID_ID = qsidID,
                Status = "C",
                TimeStamp = objCommon.ESTTime(),
                SessionID = sessID,
                ThresholdCategory = ThresholdCategory(listGenericCriteria, i, libraryThresholdCategories),
                FieldSortOrder = srtOrder,
                DisplayField2Client = displayField2ClientVal(listGenericCriteria, i),
                SourceFieldDisplayOrder = sourceFieldDisplayOrderVal(listGenericCriteria, i),
                ParentId = parentId == 0 ? null : parentId,
                ChildSortOrder = childSortOrder == 0 ? null : childSortOrder,
                DateFormatID = DateFormatId(listGenericCriteria, i, libDateFormat),
                OperatorID = OperatorId(listGenericCriteria, i, libOperators),
                OperatorFieldNameID = OperatorFieldId(listGenericCriteria, i, context),
            };
        }

        private void UPdateCaseMapQsidFieldName(List<GenericCriteria_VM> listGenericCriteria, Map_QSID_FieldName itemToUpdate, Log_Map_QSID_FieldName mapLogQSIDFieldName, List<Log_Map_QSID_FieldName> lstLogFinal, int i, List<Library_PhraseCategories> libraryPhraseCategories, List<Library_IdentifierCategories> libraryIdentifierCategories, List<Library_SubstanceNameCategories> librarySubstanceNameCategories, int? lnID, List<ILibUnits> libraryUnits, List<Library_Delimiters> libraryDelimiters, string trueFalsValue, List<Library_ThresholdCategories> libraryThresholdCategories, CRAModel context, int srtOrder, int? childSortOrder, int? parentId, int qsidID, int sessID, List<Library_DateFormat> libDateFormat, List<Library_Operators> libOperators)
        {
            if ((itemToUpdate.IsInternalOnly == mapLogQSIDFieldName.IsInternalOnly) &&
                (itemToUpdate.IsCASField == mapLogQSIDFieldName.IsCASField) &&
                (itemToUpdate.IsChemicalName == mapLogQSIDFieldName.IsChemicalName) &&
                (itemToUpdate.IsIdentifierField == mapLogQSIDFieldName.IsIdentifierField) &&
                (itemToUpdate.IsNoteCodeField == mapLogQSIDFieldName.IsNoteCodeField) &&
                (itemToUpdate.IsPhraseField == mapLogQSIDFieldName.IsPhraseField) &&
                (itemToUpdate.IsValueOther == mapLogQSIDFieldName.IsValueOther) &&
                (itemToUpdate.IsTrueFalseField == mapLogQSIDFieldName.IsTrueFalseField) &&
                (itemToUpdate.PhraseCategoryID == mapLogQSIDFieldName.PhraseCategoryID) &&
                (itemToUpdate.IdentifierTypeID == mapLogQSIDFieldName.IdentifierTypeID) &&
                (itemToUpdate.SubstanceNameCategoryId == mapLogQSIDFieldName.SubstanceNameCategoryId) &&
                (itemToUpdate.LanguageID == mapLogQSIDFieldName.LanguageID) &&
                (itemToUpdate.IsMinValue == mapLogQSIDFieldName.IsMinValue) &&
                (itemToUpdate.IsMaxValue == mapLogQSIDFieldName.IsMaxValue) &&
                (itemToUpdate.IsMinValOp == mapLogQSIDFieldName.IsMinValOp) &&
                (itemToUpdate.IsMaxValOp == mapLogQSIDFieldName.IsMaxValOp) &&
                (itemToUpdate.IsUnit == mapLogQSIDFieldName.IsUnit) &&
                (itemToUpdate.UnitFieldNameID == mapLogQSIDFieldName.UnitFieldNameID) &&
                (itemToUpdate.Operator_ValueFieldID == mapLogQSIDFieldName.Operator_ValueFieldID) &&
                (itemToUpdate.UnitID == mapLogQSIDFieldName.UnitID) &&
                (itemToUpdate.RangeDelimiterID == mapLogQSIDFieldName.RangeDelimiterID) &&
                (itemToUpdate.ValueFieldContainsOperators == mapLogQSIDFieldName.ValueFieldContainsOperators) &&
                (itemToUpdate.UnitDelimiterID == mapLogQSIDFieldName.UnitDelimiterID) &&
                (itemToUpdate.ConsiderSingleValueAs == mapLogQSIDFieldName.ConsiderSingleValueAs) &&
                (itemToUpdate.ConsiderNullValueAs == mapLogQSIDFieldName.ConsiderNullValueAs) &&
                (itemToUpdate.AllowedTrueFalseValues == mapLogQSIDFieldName.AllowedTrueFalseValues) &&
                (itemToUpdate.MultiValuePhrase == mapLogQSIDFieldName.MultiValuePhrase) &&
                (itemToUpdate.MultiValuePhraseDelimiterID == mapLogQSIDFieldName.MultiValuePhraseDelimiterID) &&
                (itemToUpdate.MultiValueIdentifier == mapLogQSIDFieldName.MultiValueIdentifier) &&
                (itemToUpdate.MultiValueIdentifierDelimiterID == mapLogQSIDFieldName.MultiValueIdentifierDelimiterID) &&
                (itemToUpdate.ChemicalNameDelimiterID == mapLogQSIDFieldName.ChemicalNameDelimiterID) &&
                (itemToUpdate.ValueFieldContainsRanges == mapLogQSIDFieldName.ValueFieldContainsRanges) &&
                (itemToUpdate.ValueFieldContainsUnits == mapLogQSIDFieldName.ValueFieldContainsUnits) &&
                (itemToUpdate.ThresholdCategory == mapLogQSIDFieldName.ThresholdCategory) &&
                (itemToUpdate.FieldSortOrder == mapLogQSIDFieldName.FieldSortOrder) &&
                (itemToUpdate.DisplayField2Client == mapLogQSIDFieldName.DisplayField2Client) &&
                (itemToUpdate.SourceFieldDisplayOrder == mapLogQSIDFieldName.SourceFieldDisplayOrder) &&
                (itemToUpdate.OperatorID == mapLogQSIDFieldName.OperatorID) &&
                (itemToUpdate.OperatorFieldNameID == mapLogQSIDFieldName.OperatorFieldNameID) &&
                (itemToUpdate.ParentId == mapLogQSIDFieldName.ParentId) &&
                (itemToUpdate.ChildSortOrder == mapLogQSIDFieldName.ChildSortOrder) && (itemToUpdate.DateFormatID == mapLogQSIDFieldName.DateFormatID))
            {
            }
            else
            {
                UpdateMapQsidFieldName(listGenericCriteria, lstLogFinal, mapLogQSIDFieldName, itemToUpdate, i, libraryPhraseCategories, libraryIdentifierCategories, librarySubstanceNameCategories, lnID, libraryUnits, libraryDelimiters, trueFalsValue, libraryThresholdCategories, context, srtOrder, childSortOrder, parentId, qsidID, sessID, libDateFormat, libOperators);
            }
        }
        private void UpdateMapQsidFieldName(List<GenericCriteria_VM> listGenericCriteria, List<Log_Map_QSID_FieldName> lstLogFinal, Log_Map_QSID_FieldName mapLogQSIDFieldName, Map_QSID_FieldName itemToUpdate, int i, List<Library_PhraseCategories> libraryPhraseCategories, List<Library_IdentifierCategories> libraryIdentifierCategories, List<Library_SubstanceNameCategories> librarySubstanceNameCategories, int? lnID, List<ILibUnits> libraryUnits, List<Library_Delimiters> libraryDelimiters, string trueFalsValue, List<Library_ThresholdCategories> libraryThresholdCategories, CRAModel context, int srtOrder, int? childSortOrder, int? parentId, int qsidID, int sessID, List<Library_DateFormat> libDateFormat, List<Library_Operators> libOperators)
        {
            lstLogFinal.Add(mapLogQSIDFieldName);
            itemToUpdate.IsInternalOnly = listGenericCriteria[i].IsInternalOnly.Value;
            itemToUpdate.IsCASField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "CAS";
            itemToUpdate.IsChemicalName = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name";
            itemToUpdate.IsIdentifierField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier";
            itemToUpdate.IsNoteCodeField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode";
            itemToUpdate.IsPhraseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase";
            itemToUpdate.IsValueOther = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "Value Other";
            itemToUpdate.IsTrueFalseField = listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedFieldType.CboValue == "True False";
            itemToUpdate.PhraseCategoryID = PhraseCategoryId(listGenericCriteria, i, libraryPhraseCategories);
            itemToUpdate.IdentifierTypeID = IdentifierTypeId(listGenericCriteria, i, libraryIdentifierCategories);
            itemToUpdate.SubstanceNameCategoryId = SubstanceNameCategoryId(listGenericCriteria, i, librarySubstanceNameCategories);
            itemToUpdate.LanguageID = lnID;
            itemToUpdate.IsMinValue = IsMinValue(listGenericCriteria, i);
            itemToUpdate.IsMaxValue = IsMaxValue(listGenericCriteria, i);
            itemToUpdate.IsMinValOp = IsMinValOp(listGenericCriteria, i);
            itemToUpdate.IsMaxValOp = IsMaxValOp(listGenericCriteria, i);
            itemToUpdate.IsUnit = IsUnit(listGenericCriteria, i);
            itemToUpdate.UnitID = UnitId(listGenericCriteria, i, libraryUnits);
            itemToUpdate.UnitFieldNameID = UnitFieldNameId(listGenericCriteria, i);
            itemToUpdate.Operator_ValueFieldID = OperatorValueFieldId(listGenericCriteria, i);
            itemToUpdate.RangeDelimiterID = RangeDelimiterId(listGenericCriteria, i, libraryDelimiters);
            itemToUpdate.ValueFieldContainsOperators = ValueFieldContainsOperators(listGenericCriteria, i);
            itemToUpdate.UnitDelimiterID = UnitDelimiterId(listGenericCriteria, i, libraryDelimiters);
            itemToUpdate.ConsiderSingleValueAs = ConsiderSingleValueAs(listGenericCriteria, i);
            itemToUpdate.ConsiderNullValueAs = ConsiderNullValueAs(listGenericCriteria, i);
            itemToUpdate.AllowedTrueFalseValues = AllowedTrueFalseValues(listGenericCriteria, i);
            itemToUpdate.MultiValuePhrase = MultiValuePhrase(listGenericCriteria, i);
            itemToUpdate.MultiValuePhraseDelimiterID = MultiValuePhraseDelimiterId(listGenericCriteria, i, libraryDelimiters);
            itemToUpdate.MultiValueIdentifier = MultiValueIdentifier(listGenericCriteria, i);
            itemToUpdate.MultiValueIdentifierDelimiterID = MultiValueIdentifierDelimiterId(listGenericCriteria, i, libraryDelimiters);
            itemToUpdate.ChemicalNameDelimiterID = ChemicalNameDelimiterId(listGenericCriteria, i, libraryDelimiters);
            itemToUpdate.ValueFieldContainsRanges = ValueFieldContainsRanges(listGenericCriteria, i);
            itemToUpdate.ValueFieldContainsUnits = ValueFieldContainsUnits(listGenericCriteria, i);
            itemToUpdate.ThresholdCategory = ThresholdCategory(listGenericCriteria, i, libraryThresholdCategories);
            itemToUpdate.FieldSortOrder = srtOrder;
            itemToUpdate.DisplayField2Client = displayField2ClientVal(listGenericCriteria, i);
            itemToUpdate.SourceFieldDisplayOrder = sourceFieldDisplayOrderVal(listGenericCriteria, i);
            itemToUpdate.ParentId = parentId == 0 ? null : parentId;
            itemToUpdate.ChildSortOrder = childSortOrder == 0 ? null : childSortOrder;
            itemToUpdate.DateFormatID = DateFormatId(listGenericCriteria, i, libDateFormat);
            itemToUpdate.OperatorID = OperatorId(listGenericCriteria, i, libOperators);
            itemToUpdate.OperatorFieldNameID = OperatorFieldId(listGenericCriteria, i, context);
            context.SaveChanges();
        }
        private bool ValueFieldContainsUnits(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdUnitType != null && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Unit Delimitor";
            }
            return false;
        }
        private int? UnitFieldNameId(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdUnitType != null && listGenericCriteria[i].SelectedMaxFieldName != null && listGenericCriteria[i].SelectedMaxFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null)
            {
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "--Select--" || listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "--Select--")
                {
                    return null;
                }
                else
                {
                    using (var context = new CRAModel())
                    {
                        var flName = listGenericCriteria[i].SelectedMaxFieldName.CboValue.ToUpper().Trim();
                        AddFieldNamesToLibrary(flName);
                        int? res = (int?)null;
                        //listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Unit FieldName" ||
                        if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range"))
                        {
                            res = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == flName).Select(y => y.FieldNameID).FirstOrDefault();
                        }
                        return res;
                    }

                }
            }
            return null;
        }

        private int? OpFieldNameId(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdOperatorType != null && listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null)
            {
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "--Select--" || listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "--Select--")
                {
                    return null;
                }
                else
                {
                    using (var context = new CRAModel())
                    {
                        var flName = listGenericCriteria[i].SelectedOperatorFieldName.CboValue.ToUpper().Trim();
                        AddFieldNamesToLibrary(flName);
                        int? res = (int?)null;
                        if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value") )
                        {
                            res = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == flName).Select(y => y.FieldNameID).FirstOrDefault();
                        }
                        return res;
                    }

                }
            }
            return null;
        }
        public void AddFieldNamesToLibrary(string allColumnNamesFromData)
        {
            using (CRAModel context = new CRAModel())
            {
                List<Library_FieldNames> lstFinal = new List<Library_FieldNames>();
                int maxFieldId = this.objCommon.GetMaxAutoField("Library_FieldNames");
                string fieldName = objCommon.GetFirstRecordMatchedWhereFieldNameRemSpecialFromStaticVariable(allColumnNamesFromData);
                if (fieldName == null)
                {
                    Library_FieldNames fieldNameLibrary = new Library_FieldNames
                    {
                        FieldName = allColumnNamesFromData.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty),
                        FieldNameID = maxFieldId,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstFinal.Add(fieldNameLibrary);
                }
                List<Library_FieldNames> nLibrary_FieldName = this.LibraryFieldNamesValidations(lstFinal);
                _objLibraryFunction_Service.BulkIns<Library_FieldNames>(nLibrary_FieldName);
            }
        }
        private int? UnitId(List<GenericCriteria_VM> listGenericCriteria, int i, List<ILibUnits> libraryUnits)
        {
            if (listGenericCriteria[i].SelectedMaxFieldName != null && listGenericCriteria[i].SelectedMaxFieldName.CboValue != null && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdUnitType != null)
            {
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "--Select--" || listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "--Select--")
                {
                    return null;
                }
                else
                {
                    return (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range") && listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Manual Unit Definition" ?
                  Convert.ToInt32(libraryUnits.Where(x => x.Unit.ToLower() == listGenericCriteria[i].SelectedUnit.Unit.Trim().ToLower()).Select(y => y.UnitID).FirstOrDefault()) : (int?)null;
                }
            }
            return null;
        }
        private int? RangeDelimiterId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Delimiters> libraryDelimiters)
        {
            if (listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null && listGenericCriteria[i].SelectedThresholdType != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--" || listGenericCriteria[i].SelectedThresholdType.CboValue == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range" ? libraryDelimiters.Where(x => x.Delimiter != null && x.Delimiter.Trim() == listGenericCriteria[i].SelectedDelimiter.Delimiter.Trim()).Select(y => y.DelimiterID).FirstOrDefault() : (int?)null;
                }
            }
            return null;
        }
        private int? DateFormatId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_DateFormat> LibraryDateFormat)
        {
            if (listGenericCriteria[i].SelectedDateFormat != null && listGenericCriteria[i].SelectedDateFormat.DateFormat != null)
            {
                return listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase" ?
                      LibraryDateFormat.Where(x => x.DateFormat != null && x.DateFormat.Trim() == listGenericCriteria[i].SelectedDateFormat.DateFormat.Trim()).Select(y => y.DateFormatID).FirstOrDefault() : (int?)null;
            }
            return null;
        }
        
        private int? OperatorId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Operators> LibOp)
        {
            if (listGenericCriteria[i].SelectedOperator != null && listGenericCriteria[i].SelectedOperator.Operator != null)
            {
                return LibOp.Where(x => x.Operator.Trim() == listGenericCriteria[i].SelectedOperator.Operator.Trim()).Select(y => y.OperatorID).FirstOrDefault();
            }
            return null;
        }
        private int? OperatorFieldId(List<GenericCriteria_VM> listGenericCriteria, int i, CRAModel context)
        {
            if (listGenericCriteria[i].SelectedOperatorFieldName != null && listGenericCriteria[i].SelectedOperatorFieldName.CboValue != null)
            {
                var fieldName = listGenericCriteria[i].SelectedOperatorFieldName.CboValue;
                int? fieldId = Convert.ToInt32(context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.Trim().ToUpper() == fieldName).Select(y => y.FieldNameID).FirstOrDefault());
                if (fieldId == 0)
                {
                    AddFieldNamesToLibrary(fieldName);
                    fieldId = Convert.ToInt32(context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.Trim().ToUpper() == fieldName).Select(y => y.FieldNameID).FirstOrDefault());
                }
                return fieldId;
            }
            return null;
        }
        private int? UnitDelimiterId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Delimiters> libraryDelimiters)
        {
            if (listGenericCriteria[i].SelectedThresholdUnitType != null && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != null)
            {
                if (listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Unit Delimitor" ? libraryDelimiters.Where(x => x.Delimiter != null && x.Delimiter.Trim() == " ").Select(y => y.DelimiterID).FirstOrDefault() : (int?)null;
                }
            }
            return null;
        }
        private int? MultiValueIdentifierDelimiterId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Delimiters> libraryDelimiters)
        {
            if (listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier" ?
                   libraryDelimiters.Where(x => x.Delimiter != null && x.Delimiter.Trim() == listGenericCriteria[i].SelectedDelimiter.Delimiter.Trim()).Select(y => y.DelimiterID).FirstOrDefault() : (int?)null;
                }
            }
            return null;
        }
        private int? ChemicalNameDelimiterId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Delimiters> libraryDelimiters)
        {
            if (listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name" ? libraryDelimiters.Where(x => x.Delimiter != null && x.Delimiter.Trim() == listGenericCriteria[i].SelectedDelimiter.Delimiter.Trim()).Select(y => y.DelimiterID).FirstOrDefault() : (int?)null;
                }
            }
            return null;
        }
        private bool MultiValuePhrase(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                {
                    return false;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase" && listGenericCriteria[i].SelectedDelimiter != null && string.IsNullOrWhiteSpace(listGenericCriteria[i].SelectedDelimiter.Delimiter) == false;
                }
            }
            return false;
        }
        private int? OperatorValueFieldId(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            //if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedMaxFieldName != null)
            //{
            //    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "--Select--")
            //    {
            //        return null;
            //    }
            //    else
            //    {
            //        using (var context = new CRAModel())
            //        {
            //            if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
            //            {
            //                var fieldName = listGenericCriteria[i].SelectedMaxFieldName.CboValue.ToUpper().Trim();
            //                var chk = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.ToUpper() == fieldName).Select(y => y.FieldNameID).FirstOrDefault();
            //                if (chk != 0)
            //                {
            //                    return Convert.ToInt32(chk);
            //                }
            //                else
            //                {
            //                    return (int?)null;
            //                }
            //            }
            //        }
            //    }
            //}
            return null;
        }
        private bool MultiValueIdentifier(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                {
                    return false;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier" && listGenericCriteria[i].SelectedDelimiter != null && string.IsNullOrWhiteSpace(listGenericCriteria[i].SelectedDelimiter.Delimiter) == false;
                }
            }
            return false;
        }
        private int? MultiValuePhraseDelimiterId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_Delimiters> libraryDelimiters)
        {
            if (listGenericCriteria[i].SelectedDelimiter != null && listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedDelimiter.Delimiter != null)
            {
                if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase" || listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode" ? libraryDelimiters.Where(x => (x.Delimiter != null) && (x.Delimiter.Trim() == listGenericCriteria[i].SelectedDelimiter.Delimiter.Trim())).Select(y => y.DelimiterID).FirstOrDefault() : (int?)null;
                }
            }
            return null;
        }
        private bool ValueFieldContainsRanges(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range";
            }
            return false;
        }
        private bool displayField2ClientVal(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (flagDisplay2ClientOne && (listGenericCriteria[i].SourceFieldDisplay != null))
            {
                return true;
            }

            return (listGenericCriteria[i].DisplayFieldToClient != null) && (bool)listGenericCriteria[i].DisplayFieldToClient;
        }
        private int? sourceFieldDisplayOrderVal(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            return listGenericCriteria[i].SourceFieldDisplay == null ? (int?)null : Convert.ToInt32(listGenericCriteria[i].SourceFieldDisplay);
        }
        private int? ThresholdCategory(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_ThresholdCategories> libraryThresholdCategories)
        {
            if (listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedFieldType != null && listGenericCriteria[i].SelectedCatgory.Category != null)
            {
                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Threshold" ? Convert.ToInt32(libraryThresholdCategories.Where(x => x.ThresholdCategory == listGenericCriteria[i].SelectedCatgory.Category.Trim()).Select(y => y.ThresholdCategoryID).FirstOrDefault()) :
                        (int?)null;
                }
            }
            return null;
        }
        private string ConsiderNullValueAs(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedConsiderNullValues != null && listGenericCriteria[i].SelectedConsiderNullValues.CboValue != null)
            {
                return listGenericCriteria[i].SelectedConsiderNullValues.CboValue == "--Select--" ? null : listGenericCriteria[i].SelectedConsiderNullValues.CboValue;
            }
            return null;
        }

        private string ConsiderSingleValueAs(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedConsiderSingleValues != null && listGenericCriteria[i].SelectedConsiderSingleValues.CboValue != null)
            {
                return listGenericCriteria[i].SelectedConsiderSingleValues.CboValue == "--Select--" ? null : listGenericCriteria[i].SelectedConsiderSingleValues.CboValue;
            }
            return null;
        }
        private bool ValueFieldContainsOperators(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            return listGenericCriteria[i].ValueFieldContainOperator != null && listGenericCriteria[i].ValueFieldContainOperator == true;

        }
        private bool IsUnit(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Unit";
            }
            return false;
        }
        private bool IsMaxValOp(List<GenericCriteria_VM> grdAdditionalInformation, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator";
            }
            return false;
        }
        private bool IsMinValOp(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator";
            }
            return false;
        }

        private bool IsMaxValue(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value";
            }
            return false;
        }

        private bool IsMinValue(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue != null)
            {
                return listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value";
            }
            return false;
        }

        private int? PhraseCategoryId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_PhraseCategories> libraryPhraseCategories)
        {
            if (listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedCatgory.Category != null && listGenericCriteria[i].SelectedFieldType != null)
            {
                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Phrase" || listGenericCriteria[i].SelectedFieldType.CboValue == "NoteCode" ? Convert.ToInt32(libraryPhraseCategories.Where(x => x.PhraseCategory.Trim() == listGenericCriteria[i].SelectedCatgory.Category.Trim()).Select(y => y.PhraseCategoryID).FirstOrDefault()) : (int?)null;
                }
            }
            return null;
        }
        private int? IdentifierTypeId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_IdentifierCategories> libraryIdentifierCategories)
        {
            if (listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedCatgory.Category != null && listGenericCriteria[i].SelectedFieldType != null)
            {
                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Identifier" ? Convert.ToInt32(libraryIdentifierCategories.Where(x => x.IdentifierTypeName.Trim() == listGenericCriteria[i].SelectedCatgory.Category.Trim()).Select(y => y.IdentifierTypeID).FirstOrDefault()) : (int?)null;
                }
            }
            return null;

        }
        private int? SubstanceNameCategoryId(List<GenericCriteria_VM> listGenericCriteria, int i, List<Library_SubstanceNameCategories> librarySubstanceNameCategories)
        {
            if (listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedCatgory.Category != null && listGenericCriteria[i].SelectedFieldType != null)
            {
                if (listGenericCriteria[i].SelectedCatgory.Category == "--Select--")
                {
                    return null;
                }
                else
                {
                    return listGenericCriteria[i].SelectedFieldType.CboValue == "Substance Name" ? Convert.ToInt32(librarySubstanceNameCategories.Where(x => x.SubstanceNameCategoryName.Trim() == listGenericCriteria[i].SelectedCatgory.Category.Trim()).Select(y => y.SubstanceNameCategoryID).FirstOrDefault()) : (int?)null;
                }
            }
            return null;
        }

        private string AllowedTrueFalseValues(List<GenericCriteria_VM> listGenericCriteria, int i)
        {
            if (listGenericCriteria[i].SelectedTrueFalse != null && listGenericCriteria[i].SelectedFieldType != null)
            {
                return listGenericCriteria[i].SelectedFieldType.CboValue == "True False" ? listGenericCriteria[i].SelectedTrueFalse : null;
            }
            return null;
        }
        public string ConvertSpecialCharacter(string str)
        {
            string strNew = string.Concat(str.ToCharArray().Select(c => SpecialCharacters.ContainsKey(c.ToString()) ? SpecialCharacters[c.ToString()] : c).ToArray());
            return objCommon.ChkGreekSmallLetter(strNew);
        }
        public void BindQcCheckList()
        {
            if (!string.IsNullOrEmpty(CommonFilepath))
            {
                string filePath = CommonFilepath.Split("\\").LastOrDefault();
                filePath = filePath.Substring(0, filePath.IndexOf("."));
                string version = filePath.Split("_").LastOrDefault();
                int versionControlID = _objLibraryFunction_Service.GetVersionControlID(Qsid_ID, version);
                qcCheckUC_Context._versionControlID = versionControlID;
                qcCheckUC_Context.BindQcCheckList();
                NotifyPropertyChanged("qcCheckUC_Context");
            }
        }


        private QueryController _overrideQueryController { get; set; }
        public QueryController OverrideQueryController
        {
            get { return _overrideQueryController; }
            set
            {
                _overrideQueryController = value;
                NotifyPropertyChanged("OverrideQueryController");
                //dgColFilter.query_FilteringStarted += DataGridFilteringStarted;
            }
        }
        private DataGridFilterLibrary.Support.FilterData _filterCurrentData { get; set; }
        public FilterData FilterCurrentData
        {
            get { return _filterCurrentData; }
            set
            {
                _filterCurrentData = value;
                FilterCurrentData.FilterChangedEvent -= new EventHandler<EventArgs>(filterCurrentData_FilterChangedEvent);
                FilterCurrentData.FilterChangedEvent += new EventHandler<EventArgs>(filterCurrentData_FilterChangedEvent);
                NotifyPropertyChanged("FilterCurrentData");
            }
        }
        void filterCurrentData_FilterChangedEvent(object sender, EventArgs e)
        {
            addFilterStateHandlers(OverrideQueryController);
        }
        private void addFilterStateHandlers(QueryController query)
        {
            query.FilteringStarted -= new EventHandler<EventArgs>(query_FilteringStarted);
            query.FilteringFinished -= new EventHandler<EventArgs>(query_FilteringFinished);

            query.FilteringStarted += new EventHandler<EventArgs>(query_FilteringStarted);
            query.FilteringFinished += new EventHandler<EventArgs>(query_FilteringFinished);
        }
        private void query_FilteringFinished(object sender, EventArgs e)
        {
            if (FilterCurrentData != null && FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
            {
                if (FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
                {
                    // FilterCurrentData.IsFilteringInProgress = false;
                }
            }
        }
        private void query_FilteringStarted(object sender, EventArgs e)
        {
            if (FilterCurrentData != null && FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
            {
                if (FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
                {
                    //data.IsFilteringInProgress = true;
                }
            }
        }
        //public List<GenericCriteria_VM> defaultList { get; set; }
        private string _searchCriteria { get; set; }
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (value != _searchCriteria)
                {
                    _searchCriteria = value;
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(_searchCriteria, _searchCriteria, "SearchCriteria_" + typeof(GenericCriteria_VM).Name));
                }
            }
        }
        private Dictionary<string, FilterData> filtersForColumns;
        private string queryStringForNUll { get; set; }
        private List<string> queryNull { get; set; } = new List<string>();
        private Query query;
        private void DataGridFilteringStarted(object sender, EventArgs e)
        {
            if (!IsNavigateProgress)
            {
                pageIndex = 1;
                FilterCurrentData = OverrideQueryController.ColumnFilterData;
                //if (OverrideQueryController.ColumnFilterData.Operator == FilterOperator.Undefined)
                //{
                //    filtersForColumns = new Dictionary<string, FilterData>();
                //    OverrideQueryController.ClearFilter();
                //}

                if (OverrideQueryController.ColumnFilterData != null && OverrideQueryController.ColumnFilterData.Operator != FilterOperator.Undefined)
                {
                    if (!filtersForColumns.ContainsKey(OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath))
                    {
                        filtersForColumns.Add(OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath, OverrideQueryController.ColumnFilterData);
                    }
                    else
                    {
                        filtersForColumns[OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath] = OverrideQueryController.ColumnFilterData;
                    }
                }
                var customizeFilter = filtersForColumns.Where(x => x.Key == "AlternateCAS" || x.Key == "PrimaryCAS" || x.Key == "Anhydrous_CAS" || x.Key == "Hydrous_CAS" || x.Key == "Cas.CasOrignal" || x.Key == "ParentCas" || x.Key == "ChildCas").FirstOrDefault();
                if (!string.IsNullOrEmpty(customizeFilter.Key))
                {
                    customizeFilter.Value.QueryString = customizeFilter.Value.QueryString.Replace("-", "");
                }

                QueryCreator queryCreator = new QueryCreator(filtersForColumns);
                queryCreator.CreateFilter(ref query);

                if (query.FilterString != String.Empty)
                {
                    myList = myListOriginal.AsQueryable().Where(query.FilterString, query.QueryParameters.ToArray<object>()).ToList();
                    Navigate(0);
                    CreateFilterQuery();
                }
                else
                {
                    if (string.IsNullOrEmpty(queryStringForNUll))
                    {
                        myList = myListOriginal.ToList();
                        Navigate(0);
                        SearchCriteria = "";
                    }
                    else
                    {
                        var result = string.Join("&&", queryNull.ToList());
                        if (result != string.Empty)
                        {
                            myList = myListOriginal.AsQueryable().Where(result.Trim()).ToList();
                        }
                        else
                        {
                            myList = myListOriginal.ToList();
                        }
                        listGenericCriteria = new List<GenericCriteria_VM>(myList);
                        SearchCriteria = queryStringForNUll;
                        Navigate(0);
                    }
                }
            }
        }
        public void CreateFilterQuery()
        {
            string queryString = string.Empty;
            foreach (var item in filtersForColumns)
            {
                if (item.Key != "" && item.Value.QueryString != "")
                    queryString += " " + item.Key + " Like '" + item.Value.QueryString + (!string.IsNullOrEmpty(item.Value.QueryStringTo) ? " And " + item.Value.QueryStringTo : "") + "',";
            }
            if (string.IsNullOrEmpty(queryStringForNUll))
            {
                SearchCriteria = queryString;
            }
            else
            {
                SearchCriteria = queryStringForNUll + ", " + queryString;
            }
        }
    }
    public class SectionProfile
    {
        public string SectionID { get; set; }
        public string SectionTitle { get; set; }
    }
    public class ILibUnits
    {
        public int UnitID
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }
    }
}
