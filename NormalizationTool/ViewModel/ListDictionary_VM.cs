﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace NormalizationTool.ViewModel
{
    public partial class MainControl_VM : Base_ViewModel
    {
        public ICommand RunListDictionary { get; set; }
        public Visibility LoaderListDictionary { get; set; } = Visibility.Collapsed;
        private RowIdentifier objDataDictionary = new RowIdentifier();
        public ICommand CheckListDictionaryFile { get; set; }
        public string NormalizationMsg { get; set; }

        public CommonFunctions objCommon;
        List<IStripChar> StripCharacters = new List<IStripChar>();

        public ObservableCollection<MapChecksVM> listChkTableParentOne { get; set; }
        public bool _IsSelectedAllChecks { get; set; }
        public bool IsSelectedAllChecks
        {
            get => _IsSelectedAllChecks;
            set
            {
                if (Equals(_IsSelectedAllChecks, value))
                {
                    return;
                }

                _IsSelectedAllChecks = value;
                listChkTableParentOne.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });

                NotifyPropertyChanged("listChkTableParentOne");
            }
        }       
        private void BindChecks()
        {
            //Tables check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field PUB_DATE in mm/dd/yyyy format", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Two QSID shouldn't linked one List_FieldName", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Two List_FieldName shouldn't linked one QSID", ParentID = 1 });

            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Short Name Length greater than 70 Chracter", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "List Phrase Length greater than 200 Chracter", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "List Phrase and List Explain not matched", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "QC check Country not present in lookup Table", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Pub-date is greater that today's date", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Validate QSID", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "NonEnglish_ListPhrase Equals to List_Phrase", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Legislation Id not exist in Library", ParentID = 1 });

            listChkTableParentOne = new ObservableCollection<MapChecksVM>(listChkTb.Where(y => y.ParentID == 1).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            NotifyPropertyChanged("listChkTableParentOne");
        }
        public void RunListDictionaryExecute(object o)
        {
            NormalizationMsg = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }

            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from LIST_DICTIONARY", CommonFilepath);
            if (dtRawChk == null || dtRawChk.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            if (string.IsNullOrEmpty(dtRawChk.Rows[0]["QSID"].ToString()))
            {
                _notifier.ShowError("Qsid field is Empty");
                return;
            }

            var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk.Rows[0]["QSID"].ToString());
            var newList = false;
            if (QsidID == 0)
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(dtRawChk.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return;
                }
                newList = true;
            }
            QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk.Rows[0]["QSID"].ToString());
            var chkAlreadyDeleted = objCommon.DbaseQueryReturnStringSQL("select top 1 b.QSID from Log_QSID_Changes a, Library_QSIDs b where status ='D' and a.QSID_ID=b.QSID_ID and upper(b.QSID) ='" + dtRawChk.Rows[0]["QSID"].ToString().ToUpper() + "'", CommonListConn);
            if (!string.IsNullOrEmpty(chkAlreadyDeleted))
            {
                _notifier.ShowError("Deleted list are not allowed to normalized");
                return;
            }
            IsSelectedAllChecks = false;
            NotifyPropertyChanged("IsSelectedAllChecks");
            IsSelectedAllChecks = true;
            NotifyPropertyChanged("IsSelectedAllChecks");
            LoaderListDictionary = Visibility.Visible;
            NotifyPropertyChanged("LoaderListDictionary");

            DataTable result = new DataTable();
            Task.Run(() =>
            {
                result = RunChecks();
                if (result == null)
                {
                    LoaderListDictionary = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderListDictionary");
                    _notifier.ShowError("can't find SHORT_NAME, LIST_PHRASE, LIST_EXPLAIN, PUB_DATE, Country, QSID fields in file, row count is zero");
                    return;
                }
                Log_Sessions chkExist = objCommon.CheckIfAnyoneNormalizing(QsidID);
                if (chkExist != null)
                {
                    using (var context = new CRAModel())
                    {
                        var userName = context.Library_Users.AsNoTracking().Where(x => x.VeriskID == chkExist.UserName).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                        _notifier.ShowError("This QSID is already Normalizing by " + userName);
                        return;
                    }
                }
                if (result.AsEnumerable().Count(x => x.Field<string>("Status") == "Fail") == 0)
                {
                    var qsidId = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk.Rows[0]["QSID"].ToString());
                    result = new DataTable();
                    var resultPrev = objDataDictionary.GetListDictionaryVerticalData(qsidId);
                    
                    using (var context = new CRAModel())
                    {
                        var StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                    }
                    bool lngNonEng = false;
                    bool nonEng = false;
                    bool noLeg = false;
                    bool logChg = false;
                    List<IQsxxxListDictionary> lstDic = new List<IQsxxxListDictionary>();
                    List<IQsxxxListDictionary> lstDicNew = new List<IQsxxxListDictionary>();
                    var sessTypeId = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select SessionTypeId from Library_SessionTypes where sessionType ='Normalization - List-Dictionary'", CommonListConn));
                    var newSession = _objLibraryFunction_Service.CreateNewSession(QsidID, CommonFilepath, sessTypeId);
                    string resultPhraseCat = _objLibraryFunction_Service.AddLibraryPhraseCategories(ref lngNonEng, ref nonEng, ref noLeg,
                        qsidId, objCommon.ESTTime(), StripCharacters, newSession, CommonFilepath, ref logChg, ref lstDic, ref lstDicNew);
                    var lstQsxxxListDic = lstDic;
                    objCommon.DbaseQueryReturnStringSQL("Update Log_Sessions set SessionEnd_TimeStamp ='" + objCommon.ESTTime() + "' where QSID_ID ='" + QsidID + "' and UserName ='" + Environment.UserName + "' and sessiontypeid = " + sessTypeId + " and SessionEnd_TimeStamp is null and QSID_ID is not null", CommonListConn);

                    var addRec = lstQsxxxListDic.Where(x => x.Status == "A").Select(y => y.PhraseCategoryID).Distinct().Count();
                    var delRec = lstQsxxxListDic.Where(x => x.Status == "D").Select(y => y.PhraseCategoryID).Distinct().Count();
                    var chgRec = lstQsxxxListDic.Where(x => x.Status == "C").Select(y => y.PhraseCategoryID).Distinct().Count();
                    if ((addRec > 0) || (delRec > 0) || (chgRec > 0))
                    {
                        if (addRec > 0 && newList == true)
                        {
                            _objLibraryFunction_Service.AddInLogQsidChanges(QsidID, "A", null, objCommon.ESTTime(), newSession);
                            newList = false;
                        }
                        else
                        {
                            _objLibraryFunction_Service.AddInLogQsidChanges(QsidID, "C", 3, objCommon.ESTTime(), newSession);
                        }
                    }
                    NormalizationMsg = "Pass - Added - " + addRec + ", Deleted - " + delRec + ", Changed - " + chgRec;
                    var resultNew = objDataDictionary.GetListDictionaryVerticalData(qsidId);

                    var totCat = resultPrev.AsEnumerable().Select(y => y.Field<string>("Phrase_Category")).Distinct().ToList();
                    var totCatNew = resultNew.AsEnumerable().Select(y => y.Field<string>("Phrase_Category")).Distinct().ToList();
                    totCat.AddRange(totCatNew);
                    totCat = totCat.Distinct().ToList();

                    result.Columns.Add("Phrase_Category");
                    result.Columns.Add("Prev_Value");
                    result.Columns.Add("New_Value");
                    result.Columns.Add("Status");
                    for (int i = 0; i < totCat.Count(); i++)
                    {
                        var phr = totCat[i];
                        var prevValu = resultPrev.AsEnumerable().Where(x => x.Field<string>("Phrase_Category") == phr).Select(y => y.Field<String>("Phrase")).FirstOrDefault();
                        var newValu = resultNew.AsEnumerable().Where(x => x.Field<string>("Phrase_Category") == phr).Select(y => y.Field<String>("Phrase")).FirstOrDefault();
                        result.Rows.Add(phr, prevValu, newValu, prevValu != newValu ? "NotMatched": "Same");
                    }
                }
                comonListDictionary_VM = new StandardDataGrid_VM("ListDictionary", result);
                NotifyPropertyChanged("comonListDictionary_VM");
                NotifyPropertyChanged("NormalizationMsg");
                LoaderListDictionary = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderListDictionary");
            });

        }

        private void CheckListDictionaryFileExecute(object o)
        {

            if (string.IsNullOrEmpty(CommonFilepath))
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            LoaderListDictionary = Visibility.Visible;
            NotifyPropertyChanged("LoaderListDictionary");
            DataTable result = new DataTable();
            Task.Run(() =>
            {
                result = RunChecks();
                if (result == null)
                {
                    LoaderListDictionary = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderListDictionary");
                    _notifier.ShowError("can't find SHORT_NAME, LIST_PHRASE, LIST_EXPLAIN, PUB_DATE, Country, QSID fields in file, row count is zero");
                    return;
                }

                comonListDictionary_VM = new StandardDataGrid_VM("ListDictionary", result);
                NotifyPropertyChanged("comonListDictionary_VM");
                NotifyPropertyChanged("NormalizationMsg");
                LoaderListDictionary = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderListDictionary");
            });
        }

        private DataTable RunChecks()
        {
            DataTable lstFinalPassed = new DataTable();

            lstFinalPassed.Columns.Add("Description");
            lstFinalPassed.Columns.Add("Status");
            lstFinalPassed.Columns.Add("Remarks");
            List<string> chkedItems = new List<string>();
            chkedItems.AddRange(listChkTableParentOne.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
            var listFieldCateid = objCommon.DbaseQueryReturnStringSQL("select top 1 PhraseCategoryID  from Library_PhraseCategories where phrasecategory ='List Field Name'", CommonListConn);
            DataTable result = new DataTable();

            result = objCommon.DbaseQueryReturnTable("select top 1 SHORT_NAME, LIST_PHRASE, LIST_EXPLAIN, PUB_DATE, Country, QSID, LIST_FIELD_NAME from LIST_DICTIONARY", CommonFilepath);
            if (result.Rows.Count == 0)
            {
                return null;
            }

           for (int i = 0; i < chkedItems.Count(); i++)
            {
                switch (chkedItems[i].ToString())
                {
                    case "Short Name Length greater than 70 Chracter":
                        lstFinalPassed.Rows.Add("Short Name Length greater than 70 Chracter", result.Rows[0][0].ToString().Trim().Length > 70 ? "Fail" : "Pass", "");
                        break;
                    case "List Phrase Length greater than 200 Chracter":
                        lstFinalPassed.Rows.Add("List Phrase Length greater than 200 Chracter", result.Rows[0][1].ToString().Trim().Length > 200 ? "Fail" : "Pass", "");
                        break;
                    case "List Phrase and List Explain not matched":
                        lstFinalPassed.Rows.Add("List Phrase and List Explain not matched", result.Rows[0][1].ToString().Trim() != result.Rows[0][2].ToString().Trim() ? "Fail" : "Pass", "");
                        break;
                    case "Field PUB_DATE in mm/dd/yyyy format":
                        lstFinalPassed.Rows.Add("Date format in Table List_Dictionary", objCommon.CheckingInvalidDateFormatInListDictionary(result.Rows[0][3].ToString().Trim()), "");
                        break;
                    case "QC check Country not present in lookup Table":
                        lstFinalPassed.Rows.Add("QC check Country not present in lookup Table", objCommon.CheckCountryCode(result), "");
                        break;
                    case "Pub-date is greater that today's date":
                        lstFinalPassed.Rows.Add("Pub-date is greater that today's date", objCommon.CheckingDateNotGreaterThanToday(result.Rows[0][3].ToString().Trim()), "");
                        break;
                    case "Validate QSID":
                        lstFinalPassed.Rows.Add("Qsid not populated in Library due to validation fail", result.AsEnumerable().Count(x => string.IsNullOrWhiteSpace(x.Field<string>("QSID").Trim()) || (x.Field<string>("QSID").Length > 15)) > 0 ? "Fail" : "Pass", "");
                        break;
                    case "NonEnglish_ListPhrase Equals to List_Phrase":
                        var resPh = objCommon.DbaseQueryReturnTableSql("select top 1 NonEnglish_ListPhrase, LIST_PHRASE from LIST_DICTIONARY", CommonFilepath);
                        if (resPh == null || resPh.Rows.Count == 0)
                        {
                            lstFinalPassed.Rows.Add("NonEnglish_ListPhrase Equals to List_Phrase", "Pass", "");
                        }
                        else
                        {
                            if ((resPh.Rows[0]["NonEnglish_ListPhrase"] == resPh.Rows[0]["LIST_PHRASE"]) && (resPh.Rows[0]["NonEnglish_ListPhrase"] != null) && (resPh.Rows[0]["LIST_PHRASE"].ToString() != string.Empty))
                            {
                                lstFinalPassed.Rows.Add("NonEnglish_ListPhrase Equals to List_Phrase", "Fail", "");
                            }
                            else
                            {
                                lstFinalPassed.Rows.Add("NonEnglish_ListPhrase Equals to List_Phrase", "Pass", "");
                            }
                        }
                        break;
                    case "Legislation Id not exist in Library":
                        var res = objCommon.DbaseQueryReturnString("select top 1 LegislationID from LIST_DICTIONARY", CommonFilepath);
                        string chkLib = string.Empty;
                        if (res != string.Empty)
                        {
                            chkLib = objCommon.DbaseQueryReturnStringSQL("select top 1 LG_ID from Library_Legislations where LegislationID = '" + res + "'", CommonListConn);
                        }
                        lstFinalPassed.Rows.Add("Legislation Id not exist in Library", res == string.Empty || res == null ? "Pass" : chkLib == string.Empty ? "Fail" : "Pass", "");
                        break;
                    case "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION":
                        string EmptyFields1 = string.Empty;
                        for (int ii = 0; ii < result.Columns.Count; ii++)
                        {
                            if (string.IsNullOrEmpty(result.Rows[0][ii].ToString()))
                            {
                                EmptyFields1 += result.Rows[0][ii].ToString() + ",";
                            }
                        }
                        if (EmptyFields1 != string.Empty)
                        {
                            EmptyFields1 = EmptyFields1.TrimEnd(',');
                            lstFinalPassed.Rows.Add("Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION", "Fail",
                                "Empty Field Found in LIST_DICTIONARY - " + EmptyFields1);
                        }
                        else
                        {
                            lstFinalPassed.Rows.Add("Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION",
                                result.Columns.Count == 0 ? "Fail" : "Pass",
                                result.Columns.Count == 0 ? "All compulsory fields are not found" : ""
                            );
                        }
                        break;
                    case "Two QSID shouldn't linked one List_FieldName":
                        var lstFldName = result.AsEnumerable().Select(y => y.Field<string>("LIST_FIELD_NAME")).FirstOrDefault();
                        var lstPhrID = objCommon.DbaseQueryReturnStringSQL("select PhraseId from Library_Phrases where languageid = 1 and phrase ='" + lstFldName + "' COLLATE SQL_Latin1_General_CP1_CS_AS ", CommonListConn);
                        if (lstPhrID == "")
                        {
                            lstPhrID = "0";
                        }
                        var resultNew = objCommon.DbaseQueryReturnTableSql("select distinct qsid_ID from QSxxx_ListDictionary where phrasecategoryID =" + Convert.ToInt32(listFieldCateid) + " and phraseid =" + Convert.ToInt32(lstPhrID), CommonListConn);
                        var st1 = lstPhrID == "0" ? "Warning" : resultNew.Rows.Count <= 1 ? "Pass" : "Fail";
                        var rem1 = lstPhrID == "0" ? "Its New LIST_FIELD_NAME,  Not Normalized Yet" : resultNew.Rows.Count <= 1 ? "" : "Found in these Qsid_IDs -" + string.Join(",", resultNew.AsEnumerable().Select(y => y.Field<string>("QSID_ID")));
                        lstFinalPassed.Rows.Add("Two QSID shouldn't linked one List_FieldName", st1, rem1);
                        break;
                    case "Two List_FieldName shouldn't linked one QSID":
                        var lstqsidid = objCommon.DbaseQueryReturnStringSQL("select top 1 Qsid_ID from Library_Qsids where upper(qsid) ='" + result.Rows[0]["QSID"].ToString().ToUpper() + "'", CommonListConn);
                        if (lstqsidid == "")
                        {
                            lstqsidid = "0";
                        }
                        var resultnew = objCommon.DbaseQueryReturnTableSql("select distinct phrase from QSxxx_ListDictionary a, library_phrases b where phrasecategoryID =" + Convert.ToInt32(listFieldCateid) + " and qsid_id =" + Convert.ToInt32(lstqsidid) + " and a.phraseid = b.phraseid ", CommonListConn);
                        var st = lstqsidid == "0" ? "Warning" : resultnew.Rows.Count <= 1 ? "Pass" : "Fail";
                        var rem = lstqsidid == "0" ? "Its New Qsid,  Not Normalized Yet" : resultnew.Rows.Count <= 1 ? "" : "Found in these List_Field_Name -" + string.Join(",", resultnew.AsEnumerable().Select(y => y.Field<string>("phrase")));
                        lstFinalPassed.Rows.Add("Two List_FieldName shouldn't linked one QSID", st, rem);
                        break;
                }
            }
            return lstFinalPassed;
        }
    }
    public class IListDicError
    {
        public String ErrorDesc { get; set; }
    }
    public class IChkName
    {
        public bool IsSelected { get; set; }
        public string ChkDescription { get; set; }
        public int ParentID { get; set; }
    }
}
