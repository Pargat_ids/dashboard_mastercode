﻿using NormalizationTool.Library;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using System.Windows.Media;

namespace NormalizationTool.ViewModel
{
    public class GenericCriteria_VM : Base_ViewModel
    {
        public GenericCriteria_VM()
        {
            CommandTrueFalse = new RelayCommand(CommandTrueFalseExecute);
            CheckedCommand = new RelayCommand(CheckedCommandExecute);
        }
        public SolidColorBrush Back_Category { get; set; }
        public SolidColorBrush Back_Language { get; set; }
        public SolidColorBrush Back_Delimiter { get; set; }
        public SolidColorBrush Back_DateFormat { get; set; }
        public SolidColorBrush Back_TrueFalse { get; set; }
        public SolidColorBrush Back_ThresholdType { get; set; }
        public SolidColorBrush Back_ThresholdUnitType { get; set; }
        public SolidColorBrush Back_ThresholdOperatorType { get; set; }
        public SolidColorBrush Back_NullValue { get; set; }
        public SolidColorBrush Back_SingleValue { get; set; }
        public SolidColorBrush Back_UnitID { get; set; }
        public SolidColorBrush Back_OperatorID { get; set; }
        public SolidColorBrush Back_FieldName { get; set; }
        public SolidColorBrush Back_OperatorFieldName { get; set; }
        public string ColumnName { get; set; }
        public string Header { get; set; }
        public string Explanation { get; set; }
        private VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel _IsInternalOnly { get; set; } = Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault();
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsInternalOnly
        {
            get { return _IsInternalOnly; }
            set
            {
                _IsInternalOnly = value;
                NotifyPropertyChanged("IsInternalOnly");
                ListFieldType = value.Value ? new ObservableCollection<IComboValue>(new List<IComboValue>()) : new ObservableCollection<IComboValue>(MainControl_VM._lstFieldType);
                NotifyPropertyChanged("ListFieldType");
                MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                UpdateMaxFields();
                if(_IsInternalOnly.Value == true)
                {
                    SelectedOperator = new ILibraryOperator();
                    SelectedMaxFieldName = new IComboValue();
                    SelectedOperatorFieldName = new IComboValue();
                    SelectedDateFormat = new ILibDateFormat();
                    SelectedCatgory = new ILibraryCategories();
                    SelectedConsiderNullValues = new IComboValue();
                    SelectedConsiderSingleValues = new IComboValue();
                    SelectedDelimiter = new ILibraryDelimiters();
                    SelectedFieldType = new IComboValue();
                    SelectedLanguage = new ILibraryLanguages();
                    SelectedThresholdOperatorType = new IComboValue();
                    SelectedThresholdType = new IComboValue();
                    SelectedThresholdUnitType = new IComboValue();
                    SelectedTrueFalse = "";
                    SelectedUnit = new ILibraryUnits();
                }
            }
        }


        public ObservableCollection<IComboValue> ListFieldType { get; set; } = new ObservableCollection<IComboValue>(MainControl_VM._lstFieldType);
        private IComboValue _SelectedFieldType { get; set; }
        public IComboValue SelectedFieldType
        {
            get { return _SelectedFieldType; }
            set
            {
                _SelectedFieldType = value;
                NotifyPropertyChanged("SelectedFieldType");

                ListCategory = new ObservableCollection<ILibraryCategories>(new List<ILibraryCategories>());
                ListLanguage = new ObservableCollection<ILibraryLanguages>(new List<ILibraryLanguages>());
                ListDelimiter = new ObservableCollection<ILibraryDelimiters>(new List<ILibraryDelimiters>());
                ListDateFormat = new ObservableCollection<ILibDateFormat>(new List<ILibDateFormat>());
                ListThresholdType = new ObservableCollection<IComboValue>(new List<IComboValue>());
                ListAllowedTrueFalse = new ObservableCollection<ILibrarySubfieldCharacters>(new List<ILibrarySubfieldCharacters>());
                ListUnit = new ObservableCollection<ILibraryUnits>(new List<ILibraryUnits>());
                ListOperator = new ObservableCollection<ILibraryOperator>(new List<ILibraryOperator>());
                MaxFieldName = new ObservableCollection<IComboValue>(new List<IComboValue>());


                Back_Category = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_Language = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_Delimiter = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_DateFormat = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_TrueFalse = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdUnitType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_NullValue = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_UnitID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_FieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorFieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                SelectedTrueFalse = string.Empty;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Threshold":
                            Back_ThresholdType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            ListThresholdType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdType);
                            break;
                        case "Phrase":
                        case "NoteCode":
                            SelectedCatgory = new ILibraryCategories();
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibraryPhraseCategories);
                            ListLanguage = new ObservableCollection<ILibraryLanguages>(MainControl_VM._lstLibraryLanguages);
                            ListDelimiter = new ObservableCollection<ILibraryDelimiters>(MainControl_VM._lstLibraryDelimiters);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_Language = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_Delimiter = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                            //UpdateMaxFields();
                            //NotifyPropertyChanged("SelectedCatgory");
                            break;
                        case "Substance Name":
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibrarySubstanceCategories);
                            ListLanguage = new ObservableCollection<ILibraryLanguages>(MainControl_VM._lstLibraryLanguages);
                            ListDelimiter = new ObservableCollection<ILibraryDelimiters>(MainControl_VM._lstLibraryDelimiters);
                            Back_Category = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            Back_Language = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_Delimiter = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                            //UpdateMaxFields();
                            break;
                        case "Identifier":
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibraryIdentifierCategories);
                            ListDelimiter = new ObservableCollection<ILibraryDelimiters>(MainControl_VM._lstLibraryDelimiters);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_Delimiter = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                            //UpdateMaxFields();
                            break;
                        case "True False":
                            var lst = GenericList.Clone<ILibrarySubfieldCharacters>(MainControl_VM._lstLibrarySubfieldCharacters);
                            ListAllowedTrueFalse = new ObservableCollection<ILibrarySubfieldCharacters>(lst);

                            Back_TrueFalse = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                            //UpdateMaxFields();
                            break;
                        case "Value Other":
                        case "CAS":
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName && x.Item1 != ColumnName + "_Unit").ToList();
                            //UpdateMaxFields();
                            break;

                    }
                }
                //NotifyPropertyChanged("SelectedTrueFalse");
                NotifyPropertyChanged("ListCategory");
                NotifyPropertyChanged("ListLanguage");
                NotifyPropertyChanged("ListDelimiter");
                NotifyPropertyChanged("ListThresholdType");
                NotifyPropertyChanged("ListAllowedTrueFalse");
                NotifyPropertyChanged("ListDateFormat");
                NotifyPropertyChanged("ListUnit");
                NotifyPropertyChanged("ListOperator");
                NotifyPropertyChanged("MinFieldName");
                NotifyPropertyChanged("UnitFieldName");

                NotifyPropertyChanged("Back_Category");
                NotifyPropertyChanged("Back_Delimiter");
                NotifyPropertyChanged("Back_Language");
                NotifyPropertyChanged("Back_TrueFalse");
                NotifyPropertyChanged("Back_ThresholdType");
                NotifyPropertyChanged("Back_ThresholdUnitType");
                NotifyPropertyChanged("Back_ThresholdOperatorType");
                NotifyPropertyChanged("Back_NullValue");
                NotifyPropertyChanged("Back_UnitID");
                NotifyPropertyChanged("Back_OperatorID");
                NotifyPropertyChanged("Back_FieldName");
                NotifyPropertyChanged("Back_OperatorFieldName");
                NotifyPropertyChanged("Back_DateFormat");
            }
        }

        public ObservableCollection<ILibraryCategories> ListCategory { get; set; }
        private ILibraryCategories _SelectedCatgory { get; set; }
        public ILibraryCategories SelectedCatgory
        {
            get { return _SelectedCatgory; }
            set
            {
                _SelectedCatgory = value;
                NotifyPropertyChanged("SelectedCatgory");
                Back_Category = value == null && ListCategory.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_Category");
                ListDateFormat = new ObservableCollection<ILibDateFormat>(new List<ILibDateFormat>());
                if (SelectedCatgory != null && SelectedCatgory.Category != null && SelectedFieldType.CboValue == "Phrase" && SelectedCatgory.IsDateType == true)
                {
                    Back_DateFormat = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                    ListDateFormat = new ObservableCollection<ILibDateFormat>(MainControl_VM._lstDateFormat);
                    NotifyPropertyChanged("ListDateFormat");
                    NotifyPropertyChanged("Back_DateFormat");
                }
                else
                {
                    SelectedDateFormat = new ILibDateFormat();
                    Back_DateFormat = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("ListDateFormat");
                    NotifyPropertyChanged("Back_DateFormat");
                }

                CommonFunctionToSelectMaxField();
            }
        }


        public ObservableCollection<ILibraryLanguages> ListLanguage { get; set; }
        private ILibraryLanguages _SelectedLanguage { get; set; }
        public ILibraryLanguages SelectedLanguage
        {
            get { return _SelectedLanguage; }
            set
            {
                _SelectedLanguage = value;
                NotifyPropertyChanged("SelectedLanguage");
                Back_Language = value == null && ListLanguage.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_Language");
            }
        }


        public ObservableCollection<ILibraryDelimiters> ListDelimiter { get; set; }
        private ILibraryDelimiters _SelectedDelimiter { get; set; }
        public ILibraryDelimiters SelectedDelimiter
        {
            get { return _SelectedDelimiter; }
            set
            {
                _SelectedDelimiter = value;
                NotifyPropertyChanged("SelectedDelimiter");
                Back_Delimiter = value == null && ListDelimiter.Count > 0 ? new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_Delimiter");
            }
        }

        public ObservableCollection<ILibDateFormat> ListDateFormat { get; set; }
        private ILibDateFormat _SelectedDateFormat { get; set; }
        public ILibDateFormat SelectedDateFormat
        {
            get { return _SelectedDateFormat; }
            set
            {
                _SelectedDateFormat = value;
                NotifyPropertyChanged("SelectedDateFormat");
                Back_DateFormat = value == null && ListDateFormat.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_DateFormat");
            }
        }



        public ObservableCollection<IComboValue> ListThresholdType { get; set; }
        private IComboValue _SelectedThresholdType { get; set; }
        public IComboValue SelectedThresholdType
        {
            get { return _SelectedThresholdType; }
            set
            {
                _SelectedThresholdType = value;
                NotifyPropertyChanged("SelectedThresholdType");
                ListCategory = new ObservableCollection<ILibraryCategories>(new List<ILibraryCategories>());
                ListConsiderNullValues = new ObservableCollection<IComboValue>(new List<IComboValue>());
                ListConsiderSingleValues = new ObservableCollection<IComboValue>(new List<IComboValue>());
                ThresholdUnitType = new ObservableCollection<IComboValue>(new List<IComboValue>());
                ThresholdOperatorType = new ObservableCollection<IComboValue>(new List<IComboValue>());
                ListDelimiter = new ObservableCollection<ILibraryDelimiters>(new List<ILibraryDelimiters>());
                ListUnit = new ObservableCollection<ILibraryUnits>(new List<ILibraryUnits>());
                ListOperator = new ObservableCollection<ILibraryOperator>(new List<ILibraryOperator>());
                MaxFieldName = new ObservableCollection<IComboValue>(new List<IComboValue>());

                Back_Category = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdUnitType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_NullValue = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_UnitID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_Delimiter = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_FieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorFieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Max Value":
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibraryThresholdCategories);
                            ListConsiderNullValues = new ObservableCollection<IComboValue>(MainControl_VM._lstConsiderNullValue);
                            ThresholdUnitType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            Back_NullValue = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //if (MainControl_VM.CombineFieldName.Where(x => x.Item1.ToUpper() == ColumnName.ToUpper()).Count() == 0)
                            //{
                            //    MainControl_VM.CombineFieldName.Add((ColumnName, "Max"));
                            //}
                            break;
                        case "Min Value":
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibraryThresholdCategories);
                            ThresholdUnitType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //if (MainControl_VM.CombineFieldName.Where(x => x.Item1.ToUpper() == ColumnName.ToUpper()).Count() == 0)
                            //{
                            //    MainControl_VM.CombineFieldName.Add((ColumnName, "Min"));
                            //}
                            break;
                        case "Value Range":
                            ListCategory = new ObservableCollection<ILibraryCategories>(MainControl_VM._lstLibraryThresholdCategories);
                            ThresholdUnitType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new ObservableCollection<IComboValue>(MainControl_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_NullValue = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            ListConsiderSingleValues = new ObservableCollection<IComboValue>(MainControl_VM._lstSingleValueConsiderAs);
                            ListConsiderNullValues = new ObservableCollection<IComboValue>(MainControl_VM._lstConsiderNullValue);
                            Back_SingleValue = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            ListDelimiter = new ObservableCollection<ILibraryDelimiters>(MainControl_VM._lstLibraryDelimiters);
                            Back_Delimiter = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            break;
                        case "Max Value Operator":
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName).ToList();
                            //var result = MainControl_VM.CombineFieldName.Where(x => x.Item2 == "Max").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            //Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //MaxFieldName = new ObservableCollection<IComboValue>(result);
                            //if (MainControl_VM.CombineFieldName.Where(x => x.Item1.ToUpper() == ColumnName.ToUpper()).Count() == 0)
                            //{
                            //    MainControl_VM.CombineFieldName.Add((ColumnName, "Min"));
                            //}
                            break; ;
                        case "Min Value Operator":
                            //MainControl_VM.CombineFieldName = MainControl_VM.CombineFieldName.Where(x => x.Item1 != ColumnName).ToList();
                            //var resultMin = MainControl_VM.CombineFieldName.Where(x => x.Item2 == "Min").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            //MaxFieldName = new ObservableCollection<IComboValue>(resultMin);
                            //Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            break;
                        case "Unit":
                            UpdateMaxFields();
                            break;

                    }
                }
                Back_ThresholdType = value == null && ListThresholdType.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;

                NotifyPropertyChanged("ListCategory");
                NotifyPropertyChanged("ThresholdUnitType");
                NotifyPropertyChanged("ThresholdOperatorType");
                NotifyPropertyChanged("ListConsiderNullValues");
                NotifyPropertyChanged("ListDelimiter");
                NotifyPropertyChanged("ListConsiderSingleValues");
                NotifyPropertyChanged("ListUnit");
                NotifyPropertyChanged("ListOperator");
                NotifyPropertyChanged("ListDelimiter");

                NotifyPropertyChanged("Back_UnitID");
                NotifyPropertyChanged("Back_OperatorID");
                NotifyPropertyChanged("Back_ThresholdType");
                NotifyPropertyChanged("Back_Category");
                NotifyPropertyChanged("Back_ThresholdUnitType");
                NotifyPropertyChanged("Back_ThresholdOperatorType");
                NotifyPropertyChanged("Back_NullValue");
                NotifyPropertyChanged("Back_Delimiter");
                NotifyPropertyChanged("Back_SingleValue");
                NotifyPropertyChanged("Back_Delimiter");
                NotifyPropertyChanged("Back_FieldName");
                NotifyPropertyChanged("Back_OperatorFieldName");
            }
        }


        public ObservableCollection<IComboValue> ListConsiderNullValues { get; set; }
        private IComboValue _SelectedConsiderNullValues { get; set; }
        public IComboValue SelectedConsiderNullValues
        {
            get { return _SelectedConsiderNullValues; }
            set
            {
                _SelectedConsiderNullValues = value;
                NotifyPropertyChanged("SelectedConsiderNullValues");
                if (value != null)
                {
                    Back_NullValue = value == null ? (SelectedThresholdType.CboValue != null && SelectedThresholdType.CboValue == "Max Value" ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush) : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_NullValue");
                }
            }
        }


        public ObservableCollection<IComboValue> ListConsiderSingleValues { get; set; }
        private IComboValue _SelectedConsiderSingleValues { get; set; }
        public IComboValue SelectedConsiderSingleValues
        {
            get { return _SelectedConsiderSingleValues; }
            set
            {
                _SelectedConsiderSingleValues = value;
                NotifyPropertyChanged("SelectedConsiderSingleValues");
                if (ListConsiderSingleValues != null)
                {
                    Back_SingleValue = value == null && ListConsiderSingleValues.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_SingleValue");
                }
            }
        }


        public ObservableCollection<ILibraryUnits> ListUnit { get; set; }
        public ObservableCollection<ILibraryOperator> ListOperator { get; set; }

        private ILibraryUnits _SelectedUnit { get; set; }
        public ILibraryUnits SelectedUnit
        {
            get
            {
                return _SelectedUnit;
            }
            set
            {
                _SelectedUnit = value;
                NotifyPropertyChanged("SelectedUnit");
                Back_UnitID = value == null && ListUnit.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_UnitID");
                CommonFunctionToSelectMaxField();
            }
        }
        private ILibraryOperator _SelectedOperator { get; set; }
        public ILibraryOperator SelectedOperator
        {
            get
            {
                return _SelectedOperator;
            }
            set
            {
                _SelectedOperator = value;
                NotifyPropertyChanged("SelectedOperator");
                Back_OperatorID = value == null && ListOperator.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_OperatorID");
                CommonFunctionToSelectMaxField();
            }
        }
        public ICommand CommandTrueFalse { get; set; }
        public void CommandTrueFalseExecute(object o)
        {
            SelectedTrueFalse = string.Join(",", ListAllowedTrueFalse.Where(x => x.IsSelected).Select(y => y.Character).ToList());
            NotifyPropertyChanged("SelectedTrueFalse");
            Back_TrueFalse = string.IsNullOrEmpty(SelectedTrueFalse) && ListAllowedTrueFalse.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
            NotifyPropertyChanged("Back_TrueFalse");
        }
        public ICommand CheckedCommand { get; set; }
        public void CheckedCommandExecute(object o)
        {
            if (IsInternalOnly.Value)
            {
                IsInternalOnly = Engine._listCheckboxType.Where(x => x.Value == false).FirstOrDefault();
            }
            else
            {
                IsInternalOnly = Engine._listCheckboxType.Where(x => x.Value).FirstOrDefault();
            }

        }


        public ObservableCollection<ILibrarySubfieldCharacters> ListAllowedTrueFalse { get; set; }

        private string _SelectedTrueFalse { get; set; }
        public string SelectedTrueFalse
        {
            get
            {
                return _SelectedTrueFalse;
            }
            set
            {
                _SelectedTrueFalse = value;
                NotifyPropertyChanged("SelectedTrueFalse");
                Back_TrueFalse = value == null && ListAllowedTrueFalse.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_TrueFalse");
            }
        }

        public ObservableCollection<IComboValue> ThresholdUnitType { get; set; }
        public ObservableCollection<IComboValue> ThresholdOperatorType { get; set; }
        private IComboValue _SelectedThresholdUnitType { get; set; }
        public IComboValue SelectedThresholdUnitType
        {
            get
            {
                return _SelectedThresholdUnitType;
            }
            set
            {
                _SelectedThresholdUnitType = value;
                NotifyPropertyChanged("SelectedThresholdUnitType");
                ListUnit = new ObservableCollection<ILibraryUnits>(new List<ILibraryUnits>());
                MaxFieldName = new ObservableCollection<IComboValue>(new List<IComboValue>());
                Back_UnitID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_FieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdUnitType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Manual Unit Definition":
                            ListUnit = new ObservableCollection<ILibraryUnits>(MainControl_VM._lstLibraryUnits);
                            Back_UnitID = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;
                        case "Unit FieldName":

                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;

                    }
                }
                Back_ThresholdUnitType = value == null && ThresholdUnitType.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;

                NotifyPropertyChanged("ListUnit");
                NotifyPropertyChanged("Back_UnitID");
                NotifyPropertyChanged("Back_ThresholdUnitType");
                NotifyPropertyChanged("Back_FieldName");
            }
        }
        private IComboValue _SelectedThresholdOperatorType { get; set; }
        public IComboValue SelectedThresholdOperatorType
        {
            get
            {
                return _SelectedThresholdOperatorType;
            }
            set
            {
                _SelectedThresholdOperatorType = value;
                NotifyPropertyChanged("SelectedThresholdOperatorType");
                ListOperator = new ObservableCollection<ILibraryOperator>(new List<ILibraryOperator>());
                OperatorFieldName = new ObservableCollection<IComboValue>(new List<IComboValue>());
                Back_OperatorID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorFieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Manual Operator":
                            
                            if(SelectedThresholdType.CboValue == "Max Value")
                            {
                                ListOperator = new ObservableCollection<ILibraryOperator>(MainControl_VM._lstLibraryOperators.Where(x=> x.Operator == "<" || x.Operator == "<=").ToList());
                            }
                            else
                            {
                                ListOperator = new ObservableCollection<ILibraryOperator>(MainControl_VM._lstLibraryOperators.Where(x => x.Operator == ">" || x.Operator == ">=").ToList());
                            }
                            Back_OperatorID = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;   
                        case "Operator FieldName":
                            Back_OperatorFieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;

                    }
                }
                if (ThresholdOperatorType != null)
                {
                    Back_ThresholdOperatorType = value == null && ThresholdOperatorType.Count > 0 ? new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                }
                NotifyPropertyChanged("ListOperator");
                NotifyPropertyChanged("OperatorFieldName");
                NotifyPropertyChanged("Back_OperatorID");
                NotifyPropertyChanged("Back_ThresholdOperatorType");
                NotifyPropertyChanged("Back_OperatorFieldName");
            }
        }

        private ObservableCollection<IComboValue> _MaxFieldName { get; set; }

        public ObservableCollection<IComboValue> MaxFieldName
        {
            get
            {
                return _MaxFieldName;
            }
            set
            {
                if (value != null)
                {
                    _MaxFieldName = value;
                    NotifyPropertyChanged("MaxFieldName");
                }
            }
        }
        private IComboValue _SelectedMaxFieldName { get; set; }
        public IComboValue SelectedMaxFieldName
        {
            get
            {
                return _SelectedMaxFieldName;
            }
            set
            {
                if (value != null)
                {
                    _SelectedMaxFieldName = value;
                    NotifyPropertyChanged("SelectedMaxFieldName");
                    Back_FieldName = value == null && MaxFieldName.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_FieldName");
                }
            }
        }

        private ObservableCollection<IComboValue> _OperatorFieldName { get; set; }

        public ObservableCollection<IComboValue> OperatorFieldName
        {
            get
            {
                return _OperatorFieldName;
            }
            set
            {
                if (value != null)
                {
                    _OperatorFieldName = value;
                    NotifyPropertyChanged("OperatorFieldName");
                }
            }
        }
        private IComboValue _SelectedOperatorFieldName { get; set; }
        public IComboValue SelectedOperatorFieldName
        {
            get
            {
                return _SelectedOperatorFieldName;
            }
            set
            {
                if (value != null)
                {
                    _SelectedOperatorFieldName = value;
                    NotifyPropertyChanged("SelectedOperatorFieldName");
                    Back_OperatorFieldName = value == null && OperatorFieldName.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_OperatorFieldName");
                }
            }
        }

        public bool? ValueFieldContainOperator { get; set; }
        public bool? DisplayFieldToClient { get; set; }
        public int? SourceFieldDisplay { get; set; }

        private void CommonFunctionToSelectMaxField()
        {
            if (SelectedCatgory != null && SelectedCatgory.Category != null && SelectedFieldType != null && SelectedFieldType.CboValue == "Threshold" && SelectedThresholdUnitType != null && SelectedThresholdUnitType.CboValue == "Manual Unit Definition" && SelectedUnit != null)
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "abc", SelectedThresholdType.CboValue + ";" + SelectedCatgory.Category + ";" + SelectedUnit.Unit + ";" + ColumnName), typeof(GenericCriteria_VM));
                var selColumn = string.Empty;
                if (MainControl_VM.columnNameFound != string.Empty)
                {
                    selColumn = MainControl_VM.columnNameFound + "_Unit";
                }
                else
                {
                    selColumn = ColumnName + "_Unit";
                }
                if (MainControl_VM.CombineFieldName.Count(x => x.Item1 == selColumn) == 0)
                {
                    MainControl_VM.CombineFieldName.Add((selColumn, "VirtualUnit"));
                }
                var resultUnit2 = MainControl_VM.CombineFieldName.Where(x => x.Item2 == "VirtualUnit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                MaxFieldName = new ObservableCollection<IComboValue>(resultUnit2);
                SelectedMaxFieldName = MaxFieldName.Where(x => x.CboValue == selColumn).FirstOrDefault();
            }
        }
        private void UpdateMaxFields()
        {
            if ((SelectedThresholdType != null && SelectedThresholdType.CboValue == "Unit") || (SelectedThresholdUnitType != null && SelectedThresholdUnitType.CboValue == "Unit FieldName"))
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddUnit", ColumnName), typeof(GenericCriteria_VM));
            }
            else
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "abc", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Max Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Operator FieldName")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMax", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Min Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Operator FieldName")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMin", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Max Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Manual Operator")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMaxManual", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Min Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Manual Operator")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMinManual", ColumnName), typeof(GenericCriteria_VM));
            }
        }
    }
    public static class GenericList
    {
        public static List<T> Clone<T>(this List<T> list) where T : ICloneable
        {
            return list.Select(i => (T)i.Clone()).ToList();
        }
    }
}
