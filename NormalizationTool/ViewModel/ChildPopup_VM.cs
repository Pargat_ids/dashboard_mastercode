﻿using System.Data;

namespace NormalizationTool.ViewModel
{
    public class ChildPopup_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstpopup { get; set; }

        public ChildPopup_VM(DataTable dt, string des)
        {
            lstpopup = new StandardDataGrid_VM(des, dt);
            NotifyPropertyChanged("lstpopup");
        }
    }
}
