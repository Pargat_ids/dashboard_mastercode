﻿using NormalizationTool.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using ListCheckBoxType_ViewModel = VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel;
using System.Diagnostics;
using System.Windows.Forms;
using CommonGenericUIView.ViewModel;
using System.IO;

namespace NormalizationTool.ViewModel
{
    public partial class MainControl_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        // for Additional-info Screen
        public static List<IComboValue> _lstSingleValueConsiderAs { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstConsiderNullValue { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstFieldType { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstThresholdType { get; set; } = new List<IComboValue>();
        public static List<ILibDateFormat> _lstDateFormat { get; set; } = new List<ILibDateFormat>();
        public static List<IComboValue> _lstThresholdCategoryinCombo { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstThresholdOperatorinCombo { get; set; } = new List<IComboValue>();
        public static List<ILibraryLanguages> _lstLibraryLanguages { get; set; } = new List<ILibraryLanguages>();
        public static List<ILibraryUnits> _lstLibraryUnits { get; set; } = new List<ILibraryUnits>();
        public static List<ILibraryOperator> _lstLibraryOperators { get; set; } = new List<ILibraryOperator>();
        public static List<ILibraryDelimiters> _lstLibraryDelimiters { get; set; } = new List<ILibraryDelimiters>();
        public static List<ILibrarySubfieldCharacters> _lstLibrarySubfieldCharacters { get; set; } = new List<ILibrarySubfieldCharacters>();
        public static List<ILibraryCategories> _lstLibraryThresholdCategories { get; set; } = new List<ILibraryCategories>();
        public static List<ILibraryCategories> _lstLibraryPhraseCategories { get; set; } = new List<ILibraryCategories>();
        public static List<ILibraryCategories> _lstLibraryIdentifierCategories { get; set; } = new List<ILibraryCategories>();
        public static List<ILibraryCategories> _lstLibrarySubstanceCategories { get; set; } = new List<ILibraryCategories>();

        public ICommand BtnInitiateNormal { get; set; }
        public Visibility LoaderAddInfo { get; set; }
        public static List<IChkName> _lstabc { get; set; }
        private List<IChkName> listChkTb = new List<IChkName>();
        public ICommand UploadMdbCommon { get; set; }
        public ICommand BtnResetFilePath { get; set; }
        public string _CommonFilepath;
        public ICommand commandSelectedChanges { get; set; }
        public string CommonFilepath
        {
            get { return _CommonFilepath; }
            set
            {
                if (_CommonFilepath != value)
                {
                    _CommonFilepath = value;
                    NotifyPropertyChanged("CommonFilepath");
                }
            }
        }
        public bool IsSelectedTab { get; set; }
        public bool IsSelectedTabData { get; set; }
        public bool IsSelectedAddInfo { get; set; }
        public bool IsSelectedUniqueCriteria { get; set; }

        public StandardDataGrid_VM comonListDictionary_VM { get; set; }
        public StandardDataGrid_VM comonDataDictionary_VM { get; set; }
        //public ICommand CellEditEndingCommand { get; set; }
        public string CommonListConn;
        public int Qsid_ID { get; set; }

        public ICommand CommGenerateExcelFile { get; set; }
        private void CommandExcelExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                var dtDefault = ConverAddInfoIntoTable();
                RowIdentifier.ExportExcelFile(dtDefault, folderDlg.SelectedPath + "\\AdditionalInfo.Xlsx");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommGenerateAccessFile { get; set; }
        private RowIdentifier objCurrentView = new RowIdentifier();
        private void CommandAccessExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var dtDefault = ConverAddInfoIntoTable();
                objCurrentView.ExportAccessFileLocal(folderDlg.SelectedPath + "\\AdditionalInfo.mdb", dtDefault, "AdditionalInfo");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public DataTable ConverAddInfoIntoTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ColumnName");
            dt.Columns.Add("Is_Internal", typeof(bool));
            dt.Columns.Add("Field_Type");
            dt.Columns.Add("Threshold_Type");
            dt.Columns.Add("Unit_Type");
            dt.Columns.Add("Manual_Unit");
            dt.Columns.Add("Unit_Field");
            dt.Columns.Add("Category");
            dt.Columns.Add("Operator_Type");
            dt.Columns.Add("Manual_Operator");
            dt.Columns.Add("Operator_Field");
            dt.Columns.Add("Language");
            dt.Columns.Add("Delimiter");
            dt.Columns.Add("Allowed_TrueFalse");
            dt.Columns.Add("Consider_Single_Value");
            dt.Columns.Add("Consider_Null_Value");
            dt.Columns.Add("ValueField_ContainOperator", typeof(bool));
            dt.Columns.Add("DisplayFieldToClient");
            for (int i = 0; i < listGenericCriteria.Count; i++)
            {
                dt.Rows.Add
                    (
                    listGenericCriteria[i].ColumnName,
                    listGenericCriteria[i].IsInternalOnly == null || listGenericCriteria[i].IsInternalOnly.Value == false ? false : true,
                    listGenericCriteria[i].SelectedFieldType == null ? string.Empty : listGenericCriteria[i].SelectedFieldType.CboValue,
                    listGenericCriteria[i].SelectedThresholdType == null ? string.Empty : listGenericCriteria[i].SelectedThresholdType.CboValue,
                    listGenericCriteria[i].SelectedThresholdUnitType == null ? string.Empty : listGenericCriteria[i].SelectedThresholdUnitType.CboValue,
                    listGenericCriteria[i].SelectedUnit == null ? string.Empty : listGenericCriteria[i].SelectedUnit.Unit,
                    listGenericCriteria[i].SelectedMaxFieldName == null ? string.Empty : listGenericCriteria[i].SelectedMaxFieldName.CboValue,
                    listGenericCriteria[i].SelectedCatgory == null || listGenericCriteria[i].SelectedCatgory.Category == "--Select--" ? string.Empty : listGenericCriteria[i].SelectedCatgory.Category,
                    listGenericCriteria[i].SelectedThresholdOperatorType == null ? string.Empty : listGenericCriteria[i].SelectedThresholdOperatorType.CboValue,
                    listGenericCriteria[i].SelectedOperator == null ? string.Empty : listGenericCriteria[i].SelectedOperator.Operator,
                    listGenericCriteria[i].SelectedOperatorFieldName == null ? string.Empty : listGenericCriteria[i].SelectedOperatorFieldName.CboValue,
                    listGenericCriteria[i].SelectedLanguage == null ? string.Empty : listGenericCriteria[i].SelectedLanguage.LanguageName,
                    listGenericCriteria[i].SelectedDelimiter == null || listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--" ? string.Empty : listGenericCriteria[i].SelectedDelimiter.Delimiter,
                    listGenericCriteria[i].SelectedTrueFalse == null ? string.Empty : listGenericCriteria[i].SelectedTrueFalse,
                    listGenericCriteria[i].SelectedConsiderSingleValues == null ? string.Empty : listGenericCriteria[i].SelectedConsiderSingleValues.CboValue,
                    listGenericCriteria[i].SelectedConsiderNullValues == null ? string.Empty : listGenericCriteria[i].SelectedConsiderNullValues.CboValue,
                    listGenericCriteria[i].ValueFieldContainOperator == true ? true : false,
                    listGenericCriteria[i].DisplayFieldToClient
                    );

            }
            return dt;
        }
        public MainControl_VM()
        {
            qcCheckUC_Context = new QcCheckUC_VM();
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(GenericCriteria_VM), NotifyRefreshGrid);
            CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute, CommandCasCanExecute);
            CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute, CommandCasCanExecute);
            MapAdditionalInfoFields();
            objCommon = new CommonFunctions();
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            var listCheckBox = new List<ListCheckBoxType_ViewModel>();
            listCheckBox.Add(new ListCheckBoxType_ViewModel { Content = "Yes", Value = true });
            listCheckBox.Add(new ListCheckBoxType_ViewModel { Content = "No", Value = false });
            lstInternalOnly = listCheckBox;
            ListComboxItem = new ObservableCollection<selectedListItemCombo>(AddList());
            NotifyPropertyChanged("ListComboxItem");
            UploadMdbCommon = new RelayCommand(UploadMdbCommonExecute, CommandCasCanExecute);
            BtnResetFilePath = new RelayCommand(BtnResetFilePathExecute, CommandCasCanExecute);
            using (var context = new CRAModel())
            {
                StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
            }
            var resultCase = new List<iTableName>();
            resultCase.Add(new iTableName { TableName = "Upper" });
            resultCase.Add(new iTableName { TableName = "Lower" });
            resultCase.Add(new iTableName { TableName = "Keep Original" });
            listCaseOmit = new ObservableCollection<iTableName>(resultCase);
            NotifyPropertyChanged("listCaseOmit");
            SelectedCaseOmit = listCaseOmit.Where(x => x.TableName == "Lower").FirstOrDefault();
            NotifyPropertyChanged("SelectedCaseOmit");
            CheckDuplicates = new RelayCommand(CheckDuplicatesExecute, CommandCasCanExecute);
            RunListDictionary = new RelayCommand(RunListDictionaryExecute, CommandCasCanExecute);
            RunDataDictionary = new RelayCommand(RunDataDictionaryExecute, CommandCasCanExecute);
            commandFirst = new RelayCommand(CommandFirstExecute, CommandCasCanExecute);
            commandPrev = new RelayCommand(CommandPrevExecute, CommandCasCanExecute);
            commandNext = new RelayCommand(CommandNextExecute, CommandCasCanExecute);
            commandLast = new RelayCommand(CommandLastExecute, CommandCasCanExecute);
            BtnInitiateNormal = new RelayCommand(BtnInitiateNormalExecute, CommandCasCanExecute);
            //ClearAllFilter = new RelayCommand(CommandClearAllFilter, CommandCasCanExecute);

            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenDataDictionaryFromVersionHis)
            {
                CommonFilepath = VersionControlSystem.Model.ApplicationEngine.Engine.DataDictionaryListDicPath;
                NotifyPropertyChanged("CommonFilepath");
                IsSelectedTab = false;
                NotifyPropertyChanged("IsSelectedTab");
                IsSelectedAddInfo = false;
                NotifyPropertyChanged("IsSelectedAddInfo");
                IsSelectedUniqueCriteria = false;
                NotifyPropertyChanged("IsSelectedUniqueCriteria");
                IsSelectedTabData = true;
                NotifyPropertyChanged("IsSelectedTabData");
                VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenDataDictionaryFromVersionHis = false;
            }
            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenListDictionaryFromVersionHis)
            {
                CommonFilepath = VersionControlSystem.Model.ApplicationEngine.Engine.DataDictionaryListDicPath;
                NotifyPropertyChanged("CommonFilepath");
                IsSelectedTab = true;
                NotifyPropertyChanged("IsSelectedTab");
                IsSelectedTabData = false;
                IsSelectedAddInfo = false;
                NotifyPropertyChanged("IsSelectedAddInfo");
                IsSelectedUniqueCriteria = false;
                NotifyPropertyChanged("IsSelectedUniqueCriteria");
                NotifyPropertyChanged("IsSelectedTabData");
                VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenListDictionaryFromVersionHis = false;
            }
            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenAdditionalInfoFromVersionHis)
            {
                CommonFilepath = VersionControlSystem.Model.ApplicationEngine.Engine.DataDictionaryListDicPath;
                NotifyPropertyChanged("CommonFilepath");
                IsSelectedTab = false;
                NotifyPropertyChanged("IsSelectedTab");
                IsSelectedTabData = false;
                IsSelectedAddInfo = true;
                NotifyPropertyChanged("IsSelectedAddInfo");
                IsSelectedUniqueCriteria = false;
                NotifyPropertyChanged("IsSelectedUniqueCriteria");
                NotifyPropertyChanged("IsSelectedTabData");
                VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenAdditionalInfoFromVersionHis = false;
            }
            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenUniquenessCriteriaFromVersionHis)
            {
                CommonFilepath = VersionControlSystem.Model.ApplicationEngine.Engine.DataDictionaryListDicPath;
                NotifyPropertyChanged("CommonFilepath");
                IsSelectedTab = false;
                NotifyPropertyChanged("IsSelectedTab");
                IsSelectedTabData = false;
                NotifyPropertyChanged("IsSelectedTabData");
                IsSelectedAddInfo = false;
                NotifyPropertyChanged("IsSelectedAddInfo");
                IsSelectedUniqueCriteria = true;
                NotifyPropertyChanged("IsSelectedUniqueCriteria");
                VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenUniquenessCriteriaFromVersionHis = false;
            }
            CheckListDictionaryFile = new RelayCommand(CheckListDictionaryFileExecute, CommandCasCanExecute);
            CheckDataDictionaryFile = new RelayCommand(CheckDataDictionaryFileExecute, CommandCasCanExecute);
            RunUniquenessCriteria = new RelayCommand(RunUniquenessCriteriaExecute, CommandCasCanExecute);
            commandSelectedChanges = new RelayCommand(commandSelectedChangesExecute, CommandCasCanExecute);
            BindChecks();
            BindDataChecks();
            IsSelectedAllChecks = true;
            NotifyPropertyChanged("IsSelectedAllChecks");
            IsSelectedAllDataChecks = true;
            NotifyPropertyChanged("IsSelectedAllDataChecks");

            CommGenerateAccessFile = new RelayCommand(CommandAccessExport, CommandCasCanExecute);
            CommGenerateExcelFile = new RelayCommand(CommandExcelExport, CommandCasCanExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            //CellEditEndingCommand = new RelayCommand(DoSomething, CommandCasCanExecute);
            BtnSaveAddInfo = new RelayCommand(BtnSaveAddInfoExecute, CommandCasCanExecute);
            if (CommonFilepath != string.Empty)
            {
                LoaderAddInfo = Visibility.Visible;
                NotifyPropertyChanged("LoaderAddInfo");
                Task.Run(() => RunComm());
            }
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        public void BtnInitiateNormalExecute(object o)
        {
            var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
            var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var newList = false;
            if (QsidID == 0)
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(dtRawChk1.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return;
                }
                newList = true;
            }
            QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var chkAlreadyDeleted = objCommon.DbaseQueryReturnStringSQL("select top 1 b.QSID from Log_QSID_Changes a, Library_QSIDs b where status ='D' and a.QSID_ID=b.QSID_ID and upper(b.QSID) ='" + dtRawChk1.Rows[0]["QSID"].ToString().ToUpper() + "'", CommonListConn);
            if (!string.IsNullOrEmpty(chkAlreadyDeleted))
            {
                _notifier.ShowError("Deleted list are not allowed to normalized");
                return;
            }
            Log_Sessions chkExist = objCommon.CheckIfAnyoneNormalizing(QsidID);
            if (chkExist != null)
            {
                using (var context = new CRAModel())
                {
                    var userName = context.Library_Users.AsNoTracking().Where(x => x.VeriskID == chkExist.UserName).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Normalizing by " + userName);
                    return;
                }
            }
            var chkExist2 = _objLibraryFunction_Service.CheckActiveCheckOutButton(QsidID);
            if (chkExist2 != string.Empty && chkExist2 != Environment.UserName)
            {
                using (var context = new CRAModel())
                {
                    var userName = context.Library_Users.Where(x => x.VeriskID.ToUpper() == chkExist2.ToUpper()).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Check-Out " + userName);
                    return;
                }
            }

            if (File.Exists(@"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe"))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.FileName = @"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe";
                startInfo.Arguments = CommonFilepath;
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + @"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe");
            }
        }
        public static string columnNameFound { get; set; }
        private void NotifyRefreshGrid(PropertyChangedMessage<string> obj)
        {
            if (obj != null && obj.NewValue == "abc")
            {
                columnNameFound = string.Empty;
                var splVal = obj.PropertyName.Split(';');
                if (splVal.Count() > 2)
                {
                    var visaVerss = splVal[0].ToString() == "Max Value" ? "Min Value" : "Max Value";
                    columnNameFound = splVal[3];
                    for (int i = 0; i < listGenericCriteria.Count; i++)
                    {
                        if ((splVal[0] == "Max Value" || splVal[0] == "Min Value") && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == visaVerss && listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedCatgory.Category == splVal[1].ToString() && listGenericCriteria[i].SelectedUnit != null && listGenericCriteria[i].SelectedUnit.Unit == splVal[2].ToString())
                        {
                            columnNameFound = splVal[0] == "Min Value" ? listGenericCriteria[i].ColumnName : splVal[3].ToString();
                            if (splVal[0] == "Max Value")
                            {
                                if (CombineFieldName.Count(x => x.Item1 == columnNameFound + "_Unit") == 0)
                                {
                                    CombineFieldName.Add((columnNameFound + "_Unit", "VirtualUnit"));
                                }
                                var resultUnit2 = MainControl_VM.CombineFieldName.Where(x => x.Item1 == columnNameFound + "_Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                                listGenericCriteria[i].MaxFieldName = new ObservableCollection<IComboValue>(resultUnit2);
                                listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == columnNameFound + "_Unit").FirstOrDefault();
                            }
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < listGenericCriteria.Count; i++)
                    {
                        if (listGenericCriteria[i].SelectedMaxFieldName != null)
                        {
                            if (listGenericCriteria[i].SelectedMaxFieldName.CboValue == splVal[0].ToString() + "_Unit" && listGenericCriteria[i].ColumnName != splVal[0].ToString())
                            {
                                if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_Unit") == 0)
                                {
                                    CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_Unit", "VirtualUnit"));
                                }
                                var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                                listGenericCriteria[i].MaxFieldName = new ObservableCollection<IComboValue>(resultUnit2);
                                var result = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_Unit").FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedMaxFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddUnit")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdUnitType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Unit FieldName")
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Unit")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Unit").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Unit"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].MaxFieldName = new ObservableCollection<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedMaxFieldName != null)
                            {
                                var result = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedMaxFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedMaxFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMax")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Max Value Operator")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Max Value Operator").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Max Value Operator"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Max Value Operator").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].OperatorFieldName = new ObservableCollection<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedOperatorFieldName != null)
                            {
                                var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedOperatorFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedOperatorFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMin")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Min Value Operator")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Min Value Operator").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Min Value Operator"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Min Value Operator").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].OperatorFieldName = new ObservableCollection<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedOperatorFieldName != null)
                            {
                                var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedOperatorFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedOperatorFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMaxManual")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                    {
                        //if (listGenericCriteria[i].SelectedOperatorFieldName.CboValue == obj.PropertyName + "_MaxOp" && listGenericCriteria[i].ColumnName != obj.PropertyName)
                        //{
                        if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MaxOp") == 0)
                        {
                            CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_MaxOp", "VirtualOperator"));
                        }
                        var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MaxOp").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                        listGenericCriteria[i].OperatorFieldName = new ObservableCollection<IComboValue>(resultUnit2);
                        var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_MaxOp").FirstOrDefault();
                        if (result != null)
                        {
                            listGenericCriteria[i].SelectedOperatorFieldName = result;
                        }
                        //}
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMinManual")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                    {
                        //if (listGenericCriteria[i].SelectedOperatorFieldName.CboValue == obj.PropertyName + "_MinOp" && listGenericCriteria[i].ColumnName != obj.PropertyName)
                        //{
                        if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MinOp") == 0)
                        {
                            CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_MinOp", "VirtualOperator"));
                        }
                        var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MinOp").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                        listGenericCriteria[i].OperatorFieldName = new ObservableCollection<IComboValue>(resultUnit2);
                        var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_MinOp").FirstOrDefault();
                        if (result != null)
                        {
                            listGenericCriteria[i].SelectedOperatorFieldName = result;
                        }
                        //}
                    }
                }
            }
        }

        private void MapAdditionalInfoFields()
        {
            Task.Run(() =>
            {
                _lstFieldType.AddRange(_objLibraryFunction_Service.AddFieldTypeInCombo().ToList());
                _lstLibraryThresholdCategories.AddRange(_objLibraryFunction_Service.AllLibraryThresholdCategories().ToList());
                _lstLibraryPhraseCategories.AddRange(_objLibraryFunction_Service.AllLibraryPhraseCategories().ToList());
                _lstLibraryIdentifierCategories.AddRange(_objLibraryFunction_Service.AllLibraryIdentifierCategories().ToList());
                _lstLibrarySubstanceCategories.AddRange(_objLibraryFunction_Service.AllLibrarySubstanceCategories().ToList());
                _lstLibraryLanguages.AddRange(_objLibraryFunction_Service.AllLibraryLanguages().ToList());
                _lstLibraryDelimiters.AddRange(_objLibraryFunction_Service.AllLibraryDelimiters().ToList());
                _lstThresholdType.AddRange(_objLibraryFunction_Service.AddThresholdTypeinCombo().ToList());
                _lstDateFormat.AddRange(_objLibraryFunction_Service.AllLibraryDateFormat().ToList());
                _lstThresholdCategoryinCombo.AddRange(_objLibraryFunction_Service.AddThresholdCategoryinCombo().ToList());
                _lstThresholdOperatorinCombo.AddRange(_objLibraryFunction_Service.AddThresholdOperatorinCombo().ToList());
                _lstSingleValueConsiderAs.AddRange(_objLibraryFunction_Service.AddSingleValueConsiderinComboBox().ToList());
                _lstConsiderNullValue.AddRange(_objLibraryFunction_Service.AddConsiderNullValueinComboBox().ToList());
                _lstLibraryUnits.AddRange(_objLibraryFunction_Service.AllLibraryUnits().ToList());
                _lstLibraryOperators.AddRange(_objLibraryFunction_Service.AllLibraryOperators().ToList());
                _lstLibrarySubfieldCharacters.AddRange(_objLibraryFunction_Service.AllLibrarySubFieldCharacters().ToList());
            });
        }
        private bool CommandCasCanExecute(object o)
        {
            return true;
        }

        private void UploadMdbCommonExecute(object o)
        {

            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.DefaultExt = ".mdb";
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                CommonFilepath = SelectedPath[0];
                NotifyPropertyChanged("CommonFilepath");
            }
            AccessFileStartUpTime = System.IO.File.GetLastWriteTime(CommonFilepath);
            NoneedToCheckFileAgain = false;
            IsEnableRunDataDictionary = true;
            NotifyPropertyChanged("IsEnableRunDataDictionary");
            //IsSelectedAddInfo = true;
            //NotifyPropertyChanged("IsSelectedAddInfo");
            if (string.IsNullOrEmpty(CommonFilepath) == false)
            {
                LoaderAddInfo = Visibility.Visible;
                NotifyPropertyChanged("LoaderAddInfo");
                Task.Run(() => RunComm());
            }
        }

        private void BtnResetFilePathExecute(object o)
        {
            LoaderAddInfo = Visibility.Visible;
            NotifyPropertyChanged("LoaderAddInfo");
            CommonFilepath = string.Empty;
            NotifyPropertyChanged("CommonFilepath");
            IsHideInternalOnly = false;
            _IsSelectedAllColumns = false;
            NotifyPropertyChanged("IsSelectedAllColumns");
            NotifyPropertyChanged("IsHideInternalOnly");
            //blank all grids
            NormalizationMsg = string.Empty;
            NormalizationDataMsg = string.Empty;
            NotifyPropertyChanged("NormalizationMsg");
            NotifyPropertyChanged("NormalizationDataMsg");
            comonUniquenessCriteria_VM = null;
            listGenericCriteria = new List<GenericCriteria_VM>();
            comonDataDictionary_VM = null;
            comonListDictionary_VM = null;
            NotifyPropertyChanged("listGenericCriteria");
            NotifyPropertyChanged("comonUniquenessCriteria_VM");
            NotifyPropertyChanged("comonListDictionary_VM");
            NotifyPropertyChanged("comonDataDictionary_VM");
            DataChkTableColumns = null;
            NotifyPropertyChanged("DataChkTableColumns");
            qcCheckUC_Context = new QcCheckUC_VM();
            NotifyPropertyChanged("qcCheckUC_Context");
            LoaderAddInfo = Visibility.Collapsed;
            NotifyPropertyChanged("LoaderAddInfo");
            LoaderUniquenessCriteria = Visibility.Collapsed;
            NotifyPropertyChanged("LoaderUniquenessCriteria");

        }
        private void RunComm()
        {
            IsHideInternalOnly = false;
            _IsSelectedAllColumns = false;
            NotifyPropertyChanged("IsSelectedAllColumns");
            NotifyPropertyChanged("IsHideInternalOnly");
            //blank all grids
            NormalizationMsg = string.Empty;
            NormalizationDataMsg = string.Empty;
            var result = new DataTable();
            comonListDictionary_VM = null;
            comonDataDictionary_VM = null;
            NotifyPropertyChanged("comonListDictionary_VM");
            NotifyPropertyChanged("comonDataDictionary_VM");
            NotifyPropertyChanged("NormalizationMsg");
            NotifyPropertyChanged("NormalizationDataMsg");
            BindColumns();
            GetDicColumns();
            BindQcCheckList();
            LoaderAddInfo = Visibility.Collapsed;
            NotifyPropertyChanged("LoaderAddInfo");

        }

    }
}
