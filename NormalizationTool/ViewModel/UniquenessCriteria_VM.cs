﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using ToastNotifications.Messages;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System;
using System.Windows.Forms;
using EntityClass;
using System.Linq.Dynamic;
using EntityFramework.Utilities;
using System.Text;

namespace NormalizationTool.ViewModel
{
    public partial class MainControl_VM : Base_ViewModel
    {
        public bool ChkRemoveStrip { get; set; }
        public bool ChkSquashDup { get; set; }
        public bool ChkAlphaOrderOmit { get; set; } = false;
        public ObservableCollection<iTableName> listCaseOmit { get; set; }
        private iTableName _SelectedCaseOmit { get; set; }
        public iTableName SelectedCaseOmit
        {
            get => _SelectedCaseOmit;
            set
            {
                if (Equals(_SelectedCaseOmit, value))
                {
                    return;
                }

                _SelectedCaseOmit = value;
            }
        }
        public ICommand CheckDuplicates { get; set; }

        public ICommand RunUniquenessCriteria { get; set; }
        public string UniquenessMsg { get; set; }
        public StandardDataGrid_VM comonUniquenessCriteria_VM { get; set; }
        public Visibility LoaderUniquenessCriteria { get; set; } = Visibility.Collapsed;
        public ObservableCollection<MapColumnsVM> DataChkTableColumns { get; set; }
        public bool _IsSelectedAllColumns { get; set; }
        public bool IsSelectedAllColumns
        {
            get => _IsSelectedAllColumns;
            set
            {
                if (Equals(_IsSelectedAllColumns, value))
                {
                    return;
                }

                _IsSelectedAllColumns = value;
                DataChkTableColumns.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                UniquenessMsg = string.Join(",", DataChkTableColumns.Where(x => x.IsSelected).Select(y => y.ActualColumnName).ToList());
                NotifyPropertyChanged("UniquenessMsg");
                NotifyPropertyChanged("DataChkTableColumns");
            }
        }
        public bool _IsHideInternalOnly { get; set; }
        public bool IsHideInternalOnly
        {
            get => _IsHideInternalOnly;
            set
            {
                if (Equals(_IsHideInternalOnly, value))
                {
                    return;
                }

                _IsHideInternalOnly = value;
                DataChkTableColumns.Where(x => x.IsInternalOnly).ToList().ForEach(x =>
                 {
                     x.IsVisible = value == true ? Visibility.Collapsed : Visibility.Visible;
                 });
                NotifyPropertyChanged("DataChkTableColumns");
            }
        }
        private void commandSelectedChangesExecute(object o)
        {
            if (DataChkTableColumns.Count(x => x.IsSelected) > 0)
            {
                UniquenessMsg = string.Join(",", DataChkTableColumns.Where(x => x.IsSelected).Select(y => y.ActualColumnName).ToList());
                NotifyPropertyChanged("UniquenessMsg");
            }
        }
        public void RunUniquenessCriteriaExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from DATA", CommonFilepath);
            if (dtRawChk == null || dtRawChk.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            string message = string.Empty;
            string messageEd = string.Empty;
            string lenMessage = ",len(CAS1 & ";
            string messageWithoutBrac = string.Empty;
            string ExistingUniqueCriteria = string.Empty;
            var lstFlName = DataChkTableColumns.Where(x => x.IsSelected).Select(y => y.ActualColumnName.ToUpper().Trim()).ToList();
            foreach (var node in lstFlName)
            {
                message += "[" + node.Trim() + "],";
                messageWithoutBrac += node.Trim() + ",";
                if (node.Trim() == "CAS")
                {
                    messageEd += "[Editorial],";
                }
                else
                {
                    messageEd += "[" + node.Trim() + "],";
                    lenMessage += "" + node.Trim() + " & ";
                }
            }

            messageWithoutBrac = messageWithoutBrac.Substring(0, messageWithoutBrac.Length - 1);
            message = message.Substring(0, message.Length - 1);
            messageEd = messageEd.Substring(0, messageEd.Length - 1);
            lenMessage = lenMessage.Substring(0, lenMessage.Length - 2);
            lenMessage += ")";
            if (message.IndexOf("CAS", StringComparison.Ordinal) < 0)
            {
                _notifier.ShowError("CAS should be the part of the selection, Kindly select CAS");
                return;
            }
            if (message.IndexOf("EDITORIAL", StringComparison.Ordinal) > 0)
            {
                _notifier.ShowError("Editorial should not be the part of the selection, Kindly deselect Editorial");
                return;
            }

            if (message.IndexOf("PREF", StringComparison.Ordinal) > 0)
            {
                DialogResult resultMsg = System.Windows.Forms.MessageBox.Show("It is recommended not to choose PREF ! want to continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (resultMsg == DialogResult.No)
                {
                    return;
                }
            }

            var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
            var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var newList = false;
            if (QsidID == 0)
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(dtRawChk1.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return;
                }
                newList = true;
            }
            QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            string result = AddFieldNameToLibraryLocal();
            if (result == "Fail")
            {
                _notifier.ShowError("Can't insert into fieldnames to Library");
                return;
            }
            using (var context = new CRAModel())
            {
                var chkExst = context.Map_Unique_Criteria.AsNoTracking().Count(x => x.QSID_ID == QsidID);
                if (chkExst > 0)
                {
                    DialogResult resultMsg = System.Windows.Forms.MessageBox.Show("Unique Criteria Already Exists, Do you want override it", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (resultMsg == DialogResult.No)
                    {
                        return;
                    }
                }
                Log_Sessions chkExist = objCommon.CheckIfAnyoneNormalizing(QsidID);
                if (chkExist != null)
                {
                    var userName = context.Library_Users.AsNoTracking().Where(x => x.VeriskID == chkExist.UserName).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Normalizing by " + userName);
                    return;
                }

                List<string> chooseField = messageWithoutBrac.Split(',').ToList();
                List<string> chkInMapQSIDInternalOnly =
                    (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.IsInternalOnly).ToList()
                     join d2 in context.Library_FieldNames.AsNoTracking()
                     on d1.FieldNameID equals d2.FieldNameID
                     select d2.FieldName).ToList();
                IEnumerable<string> chkCount = chooseField.Intersect(chkInMapQSIDInternalOnly);
                if (chkCount.Any())
                {
                    _notifier.ShowError("FieldName  " + string.Join(",", chkCount.Select(x => x)) + " are marked as internalonly in AdditionalInfo, First unmarked them");
                    return;
                }

                Task.Run(() =>
                {
                    LoaderUniquenessCriteria = Visibility.Visible;
                    NotifyPropertyChanged("LoaderUniquenessCriteria");
                    ExistingUniqueCriteria = message;
                    ExistingUniqueCriteria = message.Replace("[CAS],", "");
                    messageEd = messageEd.Replace("[Editorial],", "");
                    messageEd = messageEd.Replace("[EDITORIAL],", "");
                    ExistingUniqueCriteria = ExistingUniqueCriteria.Replace("[CAS]", "");
                    messageEd = messageEd.Replace("[Editorial]", "");
                    messageEd = messageEd.Replace("[EDITORIAL]", "");
                    messageEd = messageEd.TrimEnd(',');
                    ExistingUniqueCriteria = ExistingUniqueCriteria.TrimEnd(',');
                    DataTable DtUniqueCriteria = null;
                    var commaReq = ExistingUniqueCriteria == "" ? "" : ",";
                    var listType = typeof(System.String);
                    var getAllRows = this.objCommon.DbaseQueryReturnTable("select * from DATA Where omit is null or omit ='' order by cdbl(RN)", CommonFilepath);
                    var list = _objLibraryFunction_Service.BindListFromTable(getAllRows, listType);
                    var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + messageWithoutBrac + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
                    if (chkExistingCriteria.Count() > 0)
                    {
                        DtUniqueCriteria = objCommon.DbaseQueryReturnTable(
                            "SELECT CAS1  " + commaReq + ExistingUniqueCriteria + " , COUNT(*) AS RowsCount FROM( select  replace(CAS,'-','')  AS CAS1 " + commaReq +
                            ExistingUniqueCriteria + " from DATA where omit is null or omit = '' " +
                            " union all select  replace(Editorial,'-','') as CAS1 " + commaReq + messageEd + " from DATA " +
                            " where (omit is null or omit = '' ) and (editorial is not null and editorial <> '')) GROUP BY CAS1 " + commaReq +
                            ExistingUniqueCriteria + lenMessage + " HAVING COUNT(*) > 1 ", CommonFilepath);
                        if ((DtUniqueCriteria != null) && (DtUniqueCriteria.Rows.Count > 1))
                        {
                            if (DtUniqueCriteria.AsEnumerable().Count(x => x.Field<Int32>("RowsCount") > 1) > 0)
                            {
                                DtUniqueCriteria = DtUniqueCriteria.AsEnumerable().Where(x => x.Field<Int32>("RowsCount") > 1).CopyToDataTable();
                            }
                        }
                        DtUniqueCriteria.Columns["CAS1"].ColumnName = "CAS";
                        comonUniquenessCriteria_VM = new StandardDataGrid_VM("UniquenessCriteria", DtUniqueCriteria);
                        NotifyPropertyChanged("comonUniquenessCriteria_VM");
                        LoaderUniquenessCriteria = Visibility.Collapsed;
                        NotifyPropertyChanged("LoaderUniquenessCriteria");
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            _notifier.ShowError("Choosed Criteria is not unique, Check Duplicate Rows Tab");
                        });
                        return;
                    }

                    comonUniquenessCriteria_VM = null;
                    NotifyPropertyChanged("comonUniquenessCriteria_VM");
                    var sessTypeId = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select SessionTypeId from Library_SessionTypes where sessionType ='Uniqueness Criteria'", CommonListConn));
                    var newSession = _objLibraryFunction_Service.CreateNewSession(QsidID, CommonFilepath, sessTypeId);
                    AddInUniqueCriteria(lstFlName, QsidID, newSession);
                    _objLibraryFunction_Service.AddInLogSessionDetail("Unique-Criteria", "Saved", newSession);
                    objCommon.DbaseQueryReturnStringSQL("Update Log_Sessions set SessionEnd_TimeStamp ='" + objCommon.ESTTime() + "' where QSID_ID ='" + QsidID + "' and UserName ='" + Environment.UserName + "' and sessiontypeid = " + sessTypeId + " and SessionEnd_TimeStamp is null and QSID_ID is not null", CommonListConn);
                    LoaderUniquenessCriteria = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderUniquenessCriteria");
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("unique criteria successfully added");
                    });

                });
            }
        }
        private void BindColumns()
        {
            List<IUniqueCriteria> DataChkCol = new List<IUniqueCriteria>();
            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from DATA", CommonFilepath);
            var AllDicData = objCommon.DbaseQueryReturnTable("select Field_Name, Explain from DATA_Dictionary", CommonFilepath);
            var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
            if (dtRawChk1.Rows.Count > 0)
            {
                var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
                var AllMapField = objCommon.DbaseQueryReturnTableSql("select a.FieldNameID, Upper(FieldName) as FieldName from Map_QSID_FieldName a, Library_FieldNames b where a.FieldNameID = b.FieldNameID and Qsid_ID = " + QsidID + " and IsInternalOnly = 1", CommonListConn);
                var AllUniqueCr = objCommon.DbaseQueryReturnTableSql("select a.FieldNameID, Upper(FieldName) as FieldName from Map_Unique_Criteria a, Library_FieldNames b where a.FieldNameID = b.FieldNameID and Qsid_ID = " + QsidID, CommonListConn);
                for (int i = 0; i < dtRawChk.Columns.Count; i++)
                {
                    var fieldName = dtRawChk.Columns[i].ColumnName.ToUpper();
                    var chkExst = AllMapField.AsEnumerable().Count(x => x.Field<string>("FieldName") == fieldName);
                    var chkAlready = AllUniqueCr.AsEnumerable().Count(x => x.Field<string>("FieldName") == fieldName);
                    string explain = AllDicData.AsEnumerable().Where(z => z.Field<string>("Field_Name").ToUpper() == fieldName).Select(d => d.Field<string>("Explain")).FirstOrDefault();
                    //Tables check
                    DataChkCol.Add(new IUniqueCriteria { IsSelected = chkAlready == 0 ? false : true, ActualColumnName = fieldName, IsInternalOnly = chkExst == 0 ? false : true, ColumnName = explain == null ? string.Empty : " [" + explain + "]", IsVisible = Visibility.Visible, IsPrevCriteria = chkAlready > 0 ? true : false });
                }
                DataChkTableColumns = new ObservableCollection<MapColumnsVM>(DataChkCol.Select(z => new MapColumnsVM { ColumnName = z.ColumnName, IsInternalOnly = z.IsInternalOnly, ActualColumnName = z.ActualColumnName, IsSelected = z.IsSelected, IsVisible = z.IsVisible, IsPrevCriteria = z.IsPrevCriteria }).ToList());
                NotifyPropertyChanged("DataChkTableColumns");
                commandSelectedChangesExecute(null);
            }
        }
        public string AddFieldNameToLibraryLocal()
        {
            try
            {
                using (CRAModel context = new CRAModel())
                {
                    List<string> getAllColumnNamesFromLibrary = objCommon.TbLibrary_FieldNamesSelectAllFieldNames();
                    var lstFlName = DataChkTableColumns.Where(x => x.IsSelected).Select(y => y.ActualColumnName.ToUpper().Trim()).ToList();
                    List<string> colsNotInLibrary = lstFlName.Except(getAllColumnNamesFromLibrary).ToList();
                    if (colsNotInLibrary.Any())
                    {
                        AddFieldNamesToLibrary(colsNotInLibrary);
                    }

                    colsNotInLibrary = null;
                }

                return "Pass";
            }
            catch
            {
                return "Fail";
            }
        }
        public void AddFieldNamesToLibrary(List<string> allColumnNamesFromData)
        {
            using (CRAModel context = new CRAModel())
            {
                List<Library_FieldNames> lstFinal = new List<Library_FieldNames>();
                int maxFieldId = this.objCommon.GetMaxAutoField("Library_FieldNames");
                foreach (string item in allColumnNamesFromData)
                {
                    string fieldName = objCommon.GetFirstRecordMatchedWhereFieldNameRemSpecialFromStaticVariable(item);
                    if (fieldName == null)
                    {
                        Library_FieldNames fieldNameLibrary = new Library_FieldNames
                        {
                            FieldName = item.Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty),
                            FieldNameID = maxFieldId,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstFinal.Add(fieldNameLibrary);
                        maxFieldId += 1;
                    }
                }

                lstFinal = lstFinal.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x.FieldName) == false).GroupBy(y => y.FieldName).Select(z => z.FirstOrDefault()).ToList();
                List<Library_FieldNames> nLibrary_FieldName = this.LibraryFieldNamesValidations(lstFinal);
                _objLibraryFunction_Service.BulkIns<Library_FieldNames>(nLibrary_FieldName);
                lstFinal = null;
            }
        }
        public void AddInUniqueCriteria(List<string> value, Int32 QsidID, Int32 sessId)
        {
        executeAgain:
            using (CRAModel context = new CRAModel())
            {
                List<Dictionary<object, object>> prevVal = objCommon.EntityListtoDynamicObject<Map_Unique_Criteria>(context.Map_Unique_Criteria.AsNoTracking().Where(x => x != null).Where(y => y.QSID_ID == QsidID).ToList());

                List<Library_FieldNames> newValues = (from d1 in context.Library_FieldNames.AsNoTracking()
                                                      join d2 in value
                                                      on d1.FieldName.ToUpper().Trim() equals d2
                                                      select d1).ToList();

                List<Dictionary<object, object>> newValuesFinal = objCommon.ForLibFieldsEntityListtoDynamicObject<Library_FieldNames>(newValues);
                Tuple<List<Dictionary<object, object>>, List<Dictionary<object, object>>> finalList = objCommon.GetCommonRecExpando(newValuesFinal, prevVal, "FIELDNAMEID", "FIELDNAMEID");
                List<Dictionary<object, object>> addRec = finalList.Item2;
                List<Dictionary<object, object>> delRec = objCommon.GetDeletionsDynamic(newValuesFinal, prevVal, "FIELDNAMEID", "FIELDNAMEID");
                List<Log_Unique_Criteria> lstFinal = new List<Log_Unique_Criteria>();
                List<Map_Unique_Criteria> lstDicFinal = new List<Map_Unique_Criteria>();
                List<int?> lstfieldNameId = new List<int?>();
                int maxUniqueCriteriaID = objCommon.GetMaxByColumnNameSQLIncrementOne("UniqueCriteriaID", "Map_Unique_Criteria", CommonListConn);
                int maxIPrev = maxUniqueCriteriaID;
                int maxUniqueCriteriaDeltaID = this.objCommon.GetMaxAutoField("Log_Unique_Criteria");
                for (int i = 0; i < delRec.Count(); i++)
                {
                    int fieldNameId = Convert.ToInt32(delRec.ElementAt(i)["FIELDNAMEID"]);
                    Map_Unique_Criteria itemToRemove = context.Map_Unique_Criteria.AsNoTracking().SingleOrDefault(x => (x.QSID_ID == QsidID) && (x.FieldNameID == fieldNameId));
                    if (itemToRemove != null)
                    {
                        Log_Unique_Criteria logUniqueCriteria = new Log_Unique_Criteria { UniqueCriteriaDeltaID = maxUniqueCriteriaDeltaID, UniqueCriteriaID = itemToRemove.UniqueCriteriaID, QSID_ID = QsidID, Status = "D", UserID = Environment.UserName.Length > 6 ? Environment.UserName.Substring(0, 6) : Environment.UserName, UpdateTimeStamp = DateTime.Now, TimeStamp = DateTime.Now, SessionID = sessId, FieldNameID = fieldNameId };
                        lstfieldNameId.Add(fieldNameId);
                        lstFinal.Add(logUniqueCriteria);
                        maxUniqueCriteriaDeltaID += 1;
                    }
                }

                for (int i = 0; i < addRec.Count(); i++)
                {
                    int fieldNameId = Convert.ToInt32(addRec.ElementAt(i)["FIELDNAMEID"]);
                    Log_Unique_Criteria logUniqueCriteria = new Log_Unique_Criteria { UniqueCriteriaDeltaID = maxUniqueCriteriaDeltaID, UniqueCriteriaID = maxUniqueCriteriaID, FieldNameID = fieldNameId, QSID_ID = QsidID, Status = "A", UserID = Environment.UserName.Length > 6 ? Environment.UserName.Substring(0, 6) : Environment.UserName, UpdateTimeStamp = DateTime.Now, TimeStamp = DateTime.Now, SessionID = sessId };
                    Map_Unique_Criteria libraryDataDic = new Map_Unique_Criteria { UniqueCriteriaID = maxUniqueCriteriaID, QSID_ID = QsidID, FieldNameID = fieldNameId, };
                    lstFinal.Add(logUniqueCriteria);
                    lstDicFinal.Add(libraryDataDic);
                    maxUniqueCriteriaID += 1;
                    maxUniqueCriteriaDeltaID += 1;
                }

                int maxNew = objCommon.GetMaxByColumnNameSQLIncrementOne("UniqueCriteriaID", "Map_Unique_Criteria", CommonListConn);
                if (maxNew == maxIPrev)
                {
                    _objLibraryFunction_Service.BulkIns<Log_Unique_Criteria>(lstFinal);
                    _objLibraryFunction_Service.BulkIns<Map_Unique_Criteria>(lstDicFinal);
                    if (lstfieldNameId.Count() > 0)
                    {
                        EFBatchOperation.For(context, context.Map_Unique_Criteria).Where(x => (x.QSID_ID == QsidID) && lstfieldNameId.Contains(x.FieldNameID)).Delete();
                    }
                }
                else
                {
                    goto executeAgain;
                }
            }
        }
        public List<Library_FieldNames> LibraryFieldNamesValidations(List<Library_FieldNames> nLibrary_FieldName)
        {
            nLibrary_FieldName = nLibrary_FieldName.AsEnumerable().Where(x => (string.IsNullOrWhiteSpace(x.FieldName.Trim()) == false) && (x.FieldName.Length <= 255)).ToList();
            return nLibrary_FieldName;
        }
        //public IList BindListFromTable(DataTable dt, Type listType)
        //{
        //    var obj = MyTypeBuilder.CreateNewObject(dt.Columns);
        //    listType = typeof(List<>).MakeGenericType(new[] { obj });
        //    System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
        //    var fields = obj.GetProperties();
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        var ob = Activator.CreateInstance(obj);

        //        foreach (var fieldInfo in fields)
        //        {
        //            foreach (DataColumn dc in dt.Columns)
        //            {
        //                if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
        //                {
        //                    object value = (dr[dc.ColumnName] == DBNull.Value) ? null : dr[dc.ColumnName];
        //                    if (value != null)
        //                    {

        //                        var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
        //                        var result = converter.ConvertFrom(value.ToString());
        //                        fieldInfo.SetValue(ob, result);

        //                    }
        //                    else
        //                    {
        //                        if (fieldInfo.PropertyType == typeof(DateTime))
        //                        {
        //                            fieldInfo.SetValue(ob, (DateTime?)null);
        //                        }
        //                        else
        //                        {
        //                            fieldInfo.SetValue(ob, value);
        //                        }

        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //        lst.Add(ob);
        //    }

        //    return lst;
        //}

        public void CheckDuplicatesExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from DATA", CommonFilepath);
            if (dtRawChk == null || dtRawChk.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            string messageWithoutBrac = string.Empty;
            var lstFlName = DataChkTableColumns.Where(x => x.IsSelected).Select(y => y.ActualColumnName.ToUpper().Trim()).ToList();
            foreach (var node in lstFlName)
            {
                messageWithoutBrac += node.Trim() + ",";
            }

            messageWithoutBrac = messageWithoutBrac.Substring(0, messageWithoutBrac.Length - 1);
            if (messageWithoutBrac.IndexOf("CAS", StringComparison.Ordinal) < 0)
            {
                _notifier.ShowError("CAS should be the part of the selection, Kindly select CAS");
                return;
            }
            if (messageWithoutBrac.IndexOf("EDITORIAL", StringComparison.Ordinal) > 0)
            {
                _notifier.ShowError("Editorial should not be the part of the selection, Kindly deselect Editorial");
                return;
            }

            Task.Run(() =>
            {
                LoaderUniquenessCriteria = Visibility.Visible;
                NotifyPropertyChanged("LoaderUniquenessCriteria");
                var chkDup = objCommon.DbaseQueryReturnTable("select " + messageWithoutBrac + " from DATA where omit is null or omit =''", CommonFilepath);

                chkDup = ApplyChecksonDataTable(chkDup);
                var listType = typeof(System.String);
                var list = _objLibraryFunction_Service.BindListFromTable(chkDup, listType);
                var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + messageWithoutBrac + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
                if (chkExistingCriteria.Any())
                {
                    var dt = CreateDataTableFromDynamicQuery(messageWithoutBrac, chkExistingCriteria, true);
                    comonUniquenessCriteria_VM = new StandardDataGrid_VM("Omit Duplicates", dt);
                    NotifyPropertyChanged("comonUniquenessCriteria_VM");
                    LoaderUniquenessCriteria = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderUniquenessCriteria");
                }
                else
                {
                    comonUniquenessCriteria_VM = null;
                    NotifyPropertyChanged("comonUniquenessCriteria_VM");
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("No Duplicate Found");
                        LoaderUniquenessCriteria = Visibility.Collapsed;
                        NotifyPropertyChanged("LoaderUniquenessCriteria");
                    });
                    return;
                }
            });
        }
        private DataTable ApplyChecksonDataTable(DataTable chkDup)
        {
            List<IStripChar> stripChar = new List<IStripChar>();
            List<string> selectedSquashChar = new List<string>();
            using (var context = new CRAModel())
            {
                stripChar = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                selectedSquashChar = context.Library_SquashCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.Character.Trim()) == false).Select(x => x.Character.Trim()).ToList();
            }

            if (SelectedCaseOmit.TableName == "Upper")
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        chkDup.Rows[i][chkDup.Columns[j].ColumnName] = chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString().ToUpper();
                    }
                }
            }
            if (SelectedCaseOmit.TableName == "Lower")
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        chkDup.Rows[i][chkDup.Columns[j].ColumnName] = chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString().ToLower();
                    }
                }
            }
            if (ChkRemoveStrip == true)
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        var res = objCommon.RemoveWhitespaceCharacter(chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString(), stripChar);
                        if (res != chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString())
                        {
                            chkDup.Rows[i][chkDup.Columns[j].ColumnName] = res;
                        }
                    }
                }
            }
            if (ChkSquashDup == true)
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        var res = SquashChar(chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString(), selectedSquashChar, ChkAlphaOrderOmit);
                        if (res != chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString())
                        {
                            chkDup.Rows[i][chkDup.Columns[j].ColumnName] = res;
                        }
                    }
                }
            }

            return chkDup;
        }
        private DataTable CreateDataTableFromDynamicQuery(string combineField, IQueryable chkExistingCriteria, bool rowCount)
        {
            var colName = combineField.Split(",").ToList();
            var chkDup = new DataTable();
            for (int i = 0; i < colName.Count; i++)
            {
                chkDup.Columns.Add(colName[i].ToString());
            }
            if (rowCount)
            {
                chkDup.Columns.Add("RowCount");
                chkDup.Columns.Add("DuplicateID");
            }
            int dupId = 1;
            foreach (var ct in chkExistingCriteria)
            {
                var nM = ct.GetType().GetProperty("Name").GetValue(ct, null);
                DataRow dr = chkDup.NewRow();
                for (int i = 0; i < colName.Count; i++)
                {
                    var colVal = nM.GetType().GetProperty(colName[i].ToString()).GetValue(nM, null);
                    dr[colName[i].ToString()] = colVal;
                }
                if (rowCount)
                {
                    var tot = ct.GetType().GetProperty("Total").GetValue(ct, null);
                    dr["RowCount"] = tot;
                    dr["DuplicateID"] = dupId;
                    dupId += 1;
                }
                chkDup.Rows.Add(dr);
            }
            return chkDup;
        }

        public string SquashChar(string str, List<string> selectedSquashChar, bool ChkAlphaOrder)
        {
            StringBuilder strToconvert = new StringBuilder(str);
            selectedSquashChar.ForEach(x =>
            {
                strToconvert.Replace(x, string.Empty);
            });
            if (ChkAlphaOrder)
            {
                var splitWord = strToconvert.ToString().Split(' ');
                var sortArray = splitWord.OrderBy(d => d.Trim()).ToArray();
                return string.Join(string.Empty, sortArray).Trim();
            }
            else
            {
                return strToconvert.Replace(" ", "").ToString();
            }
        }
    }
    public class IUniqueCriteria
    {
        public bool IsSelected { get; set; }
        public string ColumnName { get; set; }

        public string ActualColumnName { get; set; }
        public bool IsInternalOnly { get; set; }
        public bool IsPrevCriteria { get; set; }
        public Visibility IsVisible { get; set; }

    }
    public class iTableName
    {
        public string TableName { get; set; }
    }
}
