﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using ToastNotifications.Messages;

namespace NormalizationTool.ViewModel
{
    public partial class MainControl_VM : Base_ViewModel
    {
        public bool IsEnableRunDataDictionary { get; set; } = true;
        public DateTime AccessFileStartUpTime { get; set; }
        public DateTime CheckAccessFileTimeAgain { get; set; }
        public bool NoneedToCheckFileAgain { get; set; }
        public string NormalizationDataMsg { get; set; }
        public ICommand RunDataDictionary { get; set; }
        public Visibility LoaderDataDictionary { get; set; } = Visibility.Collapsed;
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CheckDataDictionaryFile { get; set; }

        private List<IChkNameData> DataChkTb = new List<IChkNameData>();
        public ObservableCollection<MapChecksVM> DataChkTableParentOne { get; set; }
        public bool _IsSelectedAllDataChecks { get; set; }
        public bool IsSelectedAllDataChecks
        {
            get => _IsSelectedAllDataChecks;
            set
            {
                if (Equals(_IsSelectedAllDataChecks, value))
                {
                    return;
                }

                _IsSelectedAllDataChecks = value;
                DataChkTableParentOne.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });

                NotifyPropertyChanged("DataChkTableParentOne");
            }
        }
        private void BindDataChecks()
        {
            //Tables check
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Fields listed in DATA_Dictionary are present in DATA", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "RN Should be number in Data-Dictionary", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Field RN cannot contain Null entries in Data-Dictionary", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Field RN cannot contain duplicate entries in Data-Dictionary", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "All fields in DATA Should present in DATA_Dictionary", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Field sizes in DATA should same define in DATA_DICTIONARY", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Normalized FieldName not in Data-Dictionary", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Field Names cannot contain Space", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "FieldNames cannot start with a number", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Field Names cannot contain any characters other than [a-z][0-9]_", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Max length of a note text should be less than 2000 characters", ParentID = 1 });
            DataChkTb.Add(new IChkNameData { IsSelected = false, ChkDescription = "Not allow duplicate data in HEAD_CD", ParentID = 1 });

            DataChkTableParentOne = new ObservableCollection<MapChecksVM>(DataChkTb.Where(y => y.ParentID == 1).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            NotifyPropertyChanged("DataChkTableParentOne");
        }
        public void RunDataDictionaryExecute(object o)
        {
            NormalizationDataMsg = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }

            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from Data_DICTIONARY", CommonFilepath);
            if (dtRawChk == null || dtRawChk.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
            var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var newList = false;
            if (QsidID == 0)
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(dtRawChk1.Rows[0]["QSID"].ToString(), Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return;
                }
                newList = true;
            }
            QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
            var chkAlreadyDeleted = objCommon.DbaseQueryReturnStringSQL("select top 1 b.QSID from Log_QSID_Changes a, Library_QSIDs b where status ='D' and a.QSID_ID=b.QSID_ID and upper(b.QSID) ='" + dtRawChk1.Rows[0]["QSID"].ToString().ToUpper() + "'", CommonListConn);
            if (!string.IsNullOrEmpty(chkAlreadyDeleted))
            {
                _notifier.ShowError("Deleted list are not allowed to normalized");
                return;
            }
            Log_Sessions chkExist = objCommon.CheckIfAnyoneNormalizing(QsidID);
            if (chkExist != null)
            {
                using (var context = new CRAModel())
                {
                    var userName = context.Library_Users.AsNoTracking().Where(x => x.VeriskID == chkExist.UserName).Select(y => y.FirstName + " " + y.LastName).FirstOrDefault();
                    _notifier.ShowError("This QSID is already Normalizing by " + userName);
                    return;
                }
            }

            IsSelectedAllDataChecks = false;
            NotifyPropertyChanged("IsSelectedAllDataChecks");
            IsSelectedAllDataChecks = true;
            NotifyPropertyChanged("IsSelectedAllDataChecks");
            LoaderDataDictionary = Visibility.Visible;
            NotifyPropertyChanged("LoaderDataDictionary");
            IsEnableRunDataDictionary = false;
            NotifyPropertyChanged("IsEnableRunDataDictionary");
            DataTable result = new DataTable();
            CheckAccessFileTimeAgain = System.IO.File.GetLastWriteTime(CommonFilepath);
            Task.Run(() =>
            {
                if (NoneedToCheckFileAgain == false || AccessFileStartUpTime != CheckAccessFileTimeAgain)
                {
                    result = RunDataChecks();

                    if (result == null)
                    {
                        LoaderDataDictionary = Visibility.Collapsed;
                        NotifyPropertyChanged("LoaderDataDictionary");
                        IsEnableRunDataDictionary = true;
                        NotifyPropertyChanged("IsEnableRunDataDictionary");
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            _notifier.ShowError("can't find FIELD_NAME, DF1_REQUIRED, Head_CD, EXPLAIN,Phrase fields in file, row count is zero");
                        });
                        return;
                    }
                    NoneedToCheckFileAgain = true;
                }
                var resultPrev = rowIdentifier.GetBindDataDictionary(QsidID, CommonListConn);
                if (result.AsEnumerable().Count(x => x.Field<string>("Status") == "Fail") == 0)
                {
                    var DataDictionaryInfoEmpty = new List<(string, string)>();
                    var lstQsxxxDataDictionaryChanges = new List<IQsxxxDataDic>();
                    var lstQsxxxDataDictionaryChangesNew = new List<IQsxxxDataDic>();
                    var logQsidDataDicChg = false;
                    var sessTypeId = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select top 1 SessionTypeId from Library_SessionTypes where sessionType ='Normalization - Data-Dictionary'", CommonListConn));
                    var newSession = _objLibraryFunction_Service.CreateNewSession(QsidID, CommonFilepath, sessTypeId);
                    using (var context = new CRAModel())
                    {
                        var countDDColumns = objCommon.DbaseQueryReturnTable("select FIELD_NAME from Data_DICTIONARY", CommonFilepath); ;
                        var countMapColumns = context.Map_QSID_FieldName.AsNoTracking().Count(x => x.QSID_ID == QsidID);
                        if (countDDColumns.Rows.Count != countMapColumns)
                        {
                            AddFromMapQSIDFieldNameDefaults(QsidID, newSession);
                        }
                    }

                    var staticVariablesMapVirtualFields = new List<IVirtualFields>();
                    var staticVariablesMapVirtualOperator = new List<IVirtualOperator>();
                    var combineFields = string.Empty;
                    using (var context = new CRAModel())
                    {
                        var mapVirtualField = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.UnitID != null && x.UnitFieldNameID != null).Select(y => new { y.UnitFieldNameID, y.UnitID }).Distinct().ToList();
                        var allUnitFieldNameIds = mapVirtualField.Select(y => y.UnitFieldNameID).Distinct().ToList();
                        var allUnitIDs = mapVirtualField.Select(y => y.UnitID).Distinct().ToList();
                        var libFldNames = context.Library_FieldNames.AsNoTracking().Where(x => allUnitFieldNameIds.Contains(x.FieldNameID)).ToList();
                        var libUnitNames = context.Library_Units.AsNoTracking().Where(x => allUnitIDs.Contains(x.UnitID)).ToList();
                        for (int i = 0; i < mapVirtualField.Count; i++)
                        {
                            var fldName = Convert.ToInt32(mapVirtualField[i].UnitFieldNameID);
                            var uFldName = Convert.ToInt32(mapVirtualField[i].UnitID);
                            var fieldName = libFldNames.Where(x => x.FieldNameID == fldName).Select(y => y.FieldName).FirstOrDefault();
                            var untDesc = libUnitNames.Where(x => x.UnitID == uFldName).Select(y => y.Unit).FirstOrDefault();
                            staticVariablesMapVirtualFields.Add(new IVirtualFields { VirtualFieldNameID = (int)mapVirtualField[i].UnitFieldNameID, VirtualFieldName = fieldName, UnitID = (int)mapVirtualField[i].UnitID, Unit = untDesc });
                        }

                        var mapVirtualOperator = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.OperatorID != null && x.OperatorFieldNameID != null).Select(y => new { y.OperatorFieldNameID, y.OperatorID, y.FieldNameID }).Distinct().ToList();
                        mapVirtualOperator = mapVirtualOperator.GroupBy(y => y.OperatorFieldNameID).Select(y => y.FirstOrDefault()).ToList();
                        for (int i = 0; i < mapVirtualOperator.Count; i++)
                        {
                            var parent = Convert.ToInt32(mapVirtualOperator[i].FieldNameID);
                            var fldName = Convert.ToInt32(mapVirtualOperator[i].OperatorFieldNameID);
                            var uFldName = Convert.ToInt32(mapVirtualOperator[i].OperatorID);
                            var fieldName = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fldName).Select(y => y.FieldName).FirstOrDefault();
                            var untDesc = context.Library_Operators.AsNoTracking().Where(x => x.OperatorID == uFldName).Select(y => y.Operator).FirstOrDefault();
                            staticVariablesMapVirtualOperator.Add(new IVirtualOperator { ParentThreshold = parent, VirtualFieldNameID = (int)mapVirtualOperator[i].OperatorFieldNameID, VirtualFieldName = fieldName, OperatorID = (int)mapVirtualOperator[i].OperatorID, Operator = untDesc });
                        }
                    }
                    var resultAll = _objLibraryFunction_Service.AddDataIntoQsxxxDataDictionary(QsidID, newSession, CommonFilepath, staticVariablesMapVirtualFields, staticVariablesMapVirtualOperator);
                    objCommon.DbaseQueryReturnStringSQL("Update Log_Sessions set SessionEnd_TimeStamp ='" + objCommon.ESTTime() + "' where QSID_ID ='" + QsidID + "' and UserName ='" + Environment.UserName + "' and sessiontypeid = " + sessTypeId + " and SessionEnd_TimeStamp is null and QSID_ID is not null", CommonListConn);

                    DataDictionaryInfoEmpty = resultAll.Item1;
                    lstQsxxxDataDictionaryChanges = resultAll.Item2;
                    lstQsxxxDataDictionaryChangesNew = resultAll.Item3;
                    logQsidDataDicChg = resultAll.Item4;
                    var resultFinal = resultAll.Item5;
                    if ((resultFinal == "Failed due to columns not found") || (resultFinal == "Data Dictionary Info not found for some fields") || (resultFinal == "Additional info of this qsid is missing, can't continue"))
                    {
                        result = new DataTable();
                        result.Columns.Add("Description");
                        result.Columns.Add("Status");
                        result.Columns.Add("Remarks");
                        result.Rows.Add(resultFinal, "Fail", "");
                    }
                    else
                    {
                        _objLibraryFunction_Service.UpdateProducteInMapTable(QsidID, newSession, CommonFilepath, dtRawChk1.Rows[0]["QSID"].ToString());
                        _objLibraryFunction_Service.UpdateMapFieldDataType(QsidID, CommonFilepath, newSession);
                        var addRec = lstQsxxxDataDictionaryChanges.Where(x => x.Status == "A").Select(y => y.FieldNameID).Distinct().Count();
                        var delRec = lstQsxxxDataDictionaryChanges.Where(x => x.Status == "D").Select(y => y.FieldNameID).Distinct().Count();
                        var chgRec = lstQsxxxDataDictionaryChanges.Where(x => x.Status == "C").Select(y => y.FieldNameID).Distinct().Count();
                        if ((addRec > 0) || (delRec > 0) || (chgRec > 0))
                        {
                            if (addRec > 0 && newList == true)
                            {
                                _objLibraryFunction_Service.AddInLogQsidChanges(QsidID, "A", null, objCommon.ESTTime(), newSession);
                                newList = false;
                            }
                            else
                            {
                                _objLibraryFunction_Service.AddInLogQsidChanges(QsidID, "C", 3, objCommon.ESTTime(), newSession);
                            }
                        }
                        NormalizationDataMsg = "Added - " + addRec + ", Deleted - " + delRec + ", Changed - " + chgRec;

                        var resultNew = objDataDictionary.GetBindDataDictionary(QsidID, CommonListConn);
                        result = new DataTable();
                        foreach (DataColumn column in resultPrev.Columns)
                        {
                            string colname = column.ColumnName;
                            if (colname.ToUpper() != "FIELDNAME")
                            {
                                resultPrev.Columns[colname].ColumnName = "Old_" + colname;
                            }
                        }
                        foreach (DataColumn column in resultNew.Columns)
                        {
                            string colname = column.ColumnName;
                            if (colname.ToUpper() != "FIELDNAME")
                            {
                                resultNew.Columns[colname].ColumnName = "New_" + colname;
                            }
                        }

                        var totField = resultPrev.AsEnumerable().Select(y => y.Field<string>("Fieldname")).Distinct().ToList();
                        var totFieldNew = resultNew.AsEnumerable().Select(y => y.Field<string>("Fieldname")).Distinct().ToList();
                        totField.AddRange(totFieldNew);
                        totField = totField.Distinct().ToList();
                        for (int i = 0; i < totField.Count(); i++)
                        {
                            var fldName = totField[i];
                            var oldValues = resultPrev.AsEnumerable().Where(x => x.Field<string>("FieldName") == fldName);
                            if (oldValues.Any())
                            {
                                result.Merge(oldValues.CopyToDataTable());
                            }
                            var NewVal = resultNew.AsEnumerable().Where(x => x.Field<string>("FieldName") == fldName);
                            if (NewVal.Any())
                            {
                                if (oldValues.Any())
                                {
                                    foreach (DataColumn column in resultNew.Columns)
                                    {
                                        string colname = column.ColumnName;
                                        if (colname.ToUpper() != "FIELDNAME" && !result.Columns.Contains(colname))
                                        {
                                            result.Columns.Add(colname);
                                        }
                                    }

                                    var newdt = NewVal.CopyToDataTable();
                                    result.AsEnumerable().Where(x => x.Field<string>("Fieldname") == fldName).ToList().ForEach(x =>
                                    {
                                        for (int j = 0; j < newdt.Columns.Count; j++)
                                        {
                                            if (newdt.Columns[j].DataType == typeof(System.Int32))
                                            {
                                                x.SetField(newdt.Columns[j].ColumnName, newdt.Rows[0].Field<Int32>(newdt.Columns[j].ColumnName));
                                            }
                                            else
                                            {
                                                x.SetField(newdt.Columns[j].ColumnName, newdt.Rows[0].Field<string>(newdt.Columns[j].ColumnName));
                                            }
                                        }
                                    });
                                    result.AcceptChanges();
                                }
                                else
                                {
                                    result.Merge(NewVal.CopyToDataTable());
                                }
                            }
                        }
                    }
                }
                comonDataDictionary_VM = new StandardDataGrid_VM("DataDictionary", result);
                NotifyPropertyChanged("comonDataDictionary_VM");
                NotifyPropertyChanged("NormalizationDataMsg");
                LoaderDataDictionary = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderDataDictionary");
                IsEnableRunDataDictionary = true;
                NotifyPropertyChanged("IsEnableRunDataDictionary");
                //postCheck(QsidID);
            });

        }
        private void CheckDataDictionaryFileExecute(object o)
        {

            if (string.IsNullOrEmpty(CommonFilepath))
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            LoaderDataDictionary = Visibility.Visible;
            NotifyPropertyChanged("LoaderDataDictionary");
            DataTable result = new DataTable();
            Task.Run(() =>
            {
                result = RunDataChecks();
                if (result == null)
                {
                    LoaderDataDictionary = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderDataDictionary");
                    _notifier.ShowError("can't find  FIELD_NAME, DF1_REQUIRED, Head_CD, EXPLAIN,Phrase in file, row count is zero");
                    return;
                }
                if (result != null)
                {
                    NoneedToCheckFileAgain = result.AsEnumerable().Count(x => x.Field<string>("Status") == "Fail") > 0 ? false : true;
                }
                comonDataDictionary_VM = new StandardDataGrid_VM("DataDictionary", result);
                NotifyPropertyChanged("comonDataDictionary_VM");
                NotifyPropertyChanged("NormalizationDataMsg");
                LoaderDataDictionary = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderDataDictionary");
            });
        }
        private DataTable RunDataChecks()
        {
            DataTable lstFinalPassed = new DataTable();

            lstFinalPassed.Columns.Add("Description");
            lstFinalPassed.Columns.Add("Status");
            lstFinalPassed.Columns.Add("Remarks");
            List<string> chkedItems = new List<string>();
            chkedItems.AddRange(DataChkTableParentOne.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
            var DataFieldCateid = objCommon.DbaseQueryReturnStringSQL("select top 1 PhraseCategoryID  from Library_PhraseCategories where phrasecategory ='List Field Name'", CommonListConn);
            DataTable result = new DataTable();

            var allFieldNameofDD = objCommon.DbaseQueryReturnTable("select FIELD_NAME, DF1_REQUIRED, Head_CD, EXPLAIN,Phrase from DATA_Dictionary", CommonFilepath);
            if (allFieldNameofDD.Rows.Count == 0)
            {
                return null;
            }

            var lstDate1 = objCommon.DbaseQueryReturnTable("select top 1 LIST_FIELD_NAME, QSID, country,[module],  LIST_PHRASE, SHORT_NAME,  PUB_DATE,LIST_EXPLAIN,  SHORT_DESCRIPTION  from LIST_DICTIONARY", CommonFilepath);
            if (lstDate1.Rows.Count == 0)
            {
                _notifier.ShowError("Unable to find columns LIST_FIELD_NAME, QSID, country,[module],  LIST_PHRASE, SHORT_NAME,  PUB_DATE,LIST_EXPLAIN,  SHORT_DESCRIPTION, contact administrator");
                return null;
            }
            var allColumnName = objCommon.DbaseQueryReturnTable("select top 1  *  from DATA", CommonFilepath);
            var lstQsid = lstDate1.AsEnumerable().Select(y => y.Field<string>("QSID")).FirstOrDefault();
            var lstqsidid = objCommon.DbaseQueryReturnStringSQL("select top 1 Qsid_ID from Library_Qsids where upper(qsid) ='" + lstQsid.ToUpper() + "'", CommonListConn);
            if (lstqsidid == "")
            {
                var lst = new List<string>();
                var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(lstQsid, Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                if (resultQsidInsert != "Pass")
                {
                    _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                    return null;
                }
            }
            lstqsidid = objCommon.DbaseQueryReturnStringSQL("select top 1 Qsid_ID from Library_Qsids where upper(qsid) ='" + lstQsid.ToUpper() + "'", CommonListConn);

            var noteDataTypeId = objCommon.DbaseQueryReturnStringSQL("select top 1 DataTypeID from Library_DataType where TypeDescription ='Notes'", CommonListConn);
            List<string> DataDicField = new List<string>();
            allFieldNameofDD.AsEnumerable().ToList().ForEach(x =>
            {
                DataDicField.Add(x.Field<string>("FIELD_NAME").ToUpper());
            });
            var qsidid = Convert.ToInt32(lstqsidid);
            List<Map_QSID_FieldName> mpQsidFld = new List<Map_QSID_FieldName>();
            using (var context = new CRAModel())
            {
                mpQsidFld = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid).ToList();
            }
            for (int i = 0; i < chkedItems.Count(); i++)
            {
                switch (chkedItems[i].ToString())
                {
                    case "Fields listed in DATA_Dictionary are present in DATA":
                        string notfoundFieldsinData = string.Empty;
                        for (int ii = 0; ii < DataDicField.Count; ii++)
                        {
                            if (!allColumnName.Columns.Contains(DataDicField[ii].ToString().ToUpper()))
                            {

                                notfoundFieldsinData += DataDicField[ii].ToString() + ",";
                            }
                        }
                        if (notfoundFieldsinData != string.Empty)
                        {
                            notfoundFieldsinData = notfoundFieldsinData.TrimEnd(',');
                            lstFinalPassed.Rows.Add(

                                "Fields listed in DATA_Dictionary are present in DATA",
                                "Fail",
                                 notfoundFieldsinData
                            );
                        }
                        else
                        {
                            lstFinalPassed.Rows.Add
                             (
                                "Fields listed in DATA_Dictionary are present in DATA",
                                "Pass",
                                ""
                            );
                        }
                        break;
                    case "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE":
                        var EmptyFields = string.Empty;
                        string notfoundFieldsinDataDictionary = string.Empty;
                        var chkFrommapQsid = objCommon.DbaseQueryReturnTableSql("select FieldName from Library_FieldNames a, Map_QSID_FieldName b " +
                        " where a.FieldNameID = b.FieldNameID and b.IsInternalOnly = 0 and b.QSID_ID = " + lstqsidid + " and b.isunit != 1", CommonListConn);
                        chkFrommapQsid.AsEnumerable().ToList().ForEach(x =>
                        {
                            var fldName = x.Field<string>("FieldName").ToUpper();
                            var chkDFREquiredTrue = allFieldNameofDD.AsEnumerable().FirstOrDefault(xx => xx.Field<string>("FIELD_NAME").ToUpper() == fldName);
                            if (chkDFREquiredTrue != null)
                            {
                                if (chkDFREquiredTrue[1].GetType() == typeof(System.Double))
                                {
                                    if (chkDFREquiredTrue.Field<double>("DF1_REQUIRED") == 1 && (string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("Head_CD")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("EXPLAIN")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("PHRASE"))))
                                    {
                                        EmptyFields += fldName + ",";
                                    }
                                }
                                if (chkDFREquiredTrue[1].GetType() == typeof(System.Boolean))
                                {
                                    if (chkDFREquiredTrue.Field<bool>("DF1_REQUIRED") && (string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("Head_CD")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("EXPLAIN")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("PHRASE"))))
                                    {
                                        EmptyFields += fldName + ",";
                                    }
                                }
                            }
                            else
                            {
                                notfoundFieldsinDataDictionary += fldName + ",";
                            }
                        });
                        if (EmptyFields != string.Empty)
                        {
                            EmptyFields = EmptyFields.TrimEnd(',');
                            lstFinalPassed.Rows.Add(
                                "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE",
                                "Fail",
                                 EmptyFields

                            );
                        }
                        else
                        {
                            notfoundFieldsinDataDictionary = notfoundFieldsinDataDictionary.TrimEnd(',');
                            lstFinalPassed.Rows.Add(
                                 "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE",
                                 notfoundFieldsinDataDictionary != string.Empty ? "Fail" : "Pass",
                                 notfoundFieldsinDataDictionary != string.Empty ? notfoundFieldsinDataDictionary : ""
                            );
                        }
                        break;
                    case "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE":
                        var chkcolDDExs = objCommon.DbaseQueryReturnTable("select top 1  RN,  FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE from Data_Dictionary", CommonFilepath);
                        lstFinalPassed.Rows.Add(
                         "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE in Data_Dictionary",
                            chkcolDDExs.Columns.Count == 0 ? "Fail" : "Pass",
                            chkcolDDExs.Columns.Count == 0 ? "Not Found RN/FIELD_NAME/DF1_REQUIRED/UNITS_FLD/HEAD_CD/EXPLAIN/PHRASE in Data_Dictionary" : ""
                        );
                        break;

                    case "RN Should be number in Data-Dictionary":
                        var chkRNInteger = objCommon.DbaseQueryReturnTable("select top 1 RN from Data_Dictionary where isnumeric(RN) = 0", CommonFilepath);
                        var chkRNDataTypeDD = objCommon.GetAccessTableColumnDataType("Data_Dictionary", "RN", CommonFilepath);
                        lstFinalPassed.Rows.Add(

                             "RN Should be number in Data-Dictionary",
                            chkRNInteger.Rows.Count == 0 && (chkRNDataTypeDD.ToUpper().Contains("INT") || chkRNDataTypeDD.ToUpper().Contains("DOUBLE") || chkRNDataTypeDD.ToUpper().Contains("DECIMAL") || chkRNDataTypeDD.ToUpper().Contains("FLOAT")) ? "Pass" : "Fail",
                            chkRNInteger.Columns.Count == 0 ? "RN Field Not found in Data-Dictionary" : chkRNInteger.Rows.Count > 0 ? "RN is Non-Numeric in Data-Dictionary -" + chkRNInteger.Rows[0][0].ToString() : chkRNDataTypeDD.ToUpper().Contains("INT") || chkRNDataTypeDD.ToUpper().Contains("DOUBLE") || chkRNDataTypeDD.ToUpper().Contains("DECIMAL") || chkRNDataTypeDD.ToUpper().Contains("FLOAT") ? "" : "RN DataType is Non-Numeric"
                        );
                        break;
                    case "Not allow duplicate data in HEAD_CD":
                        var chkDupHC = objCommon.DbaseQueryReturnTable("select ltrim(rtrim(ucase(HEAD_CD))) from Data_Dictionary where Head_CD is not null and Head_cd <> '' group by ucase(Head_CD) having count(*) > 1", CommonFilepath);
                        lstFinalPassed.Rows.Add(

                            "Not allow duplicate data in HEAD_CD",
                            chkDupHC.Rows.Count > 0 ? "Fail" : "Pass",
                            chkDupHC.Rows.Count > 0 ? "Found duplicate data in HEAD_CD -" + chkDupHC.Rows[0][0].ToString() : ""

                        );
                        break;
                    case "Field RN cannot contain Null entries in Data-Dictionary":
                        var chkRnDD = objCommon.DbaseQueryReturnTable("select top 1  RN from Data_Dictionary where RN is null or RN =''", CommonFilepath);
                        lstFinalPassed.Rows.Add(

                            "Field RN cannot contain Null entries in Data-Dictionary",
                            chkRnDD.Rows.Count > 0 ? "Fail" : "Pass",
                            chkRnDD.Rows.Count > 0 ? "Found Null RN in Data-Dictionary" : ""

                        );
                        break;
                    case "Field RN cannot contain duplicate entries in Data-Dictionary":
                        var chkRDD = objCommon.DbaseQueryReturnTable("select RN from Data_Dictionary group by RN having count(*) > 1", CommonFilepath);
                        lstFinalPassed.Rows.Add(

                            "Field RN cannot contain duplicate entries in Data-Dictionary",
                             chkRDD.Rows.Count > 0 ? "Fail" : "Pass",
                             chkRDD.Rows.Count > 0 ? string.Join(",", chkRDD.AsEnumerable().Select(y => y.Field<dynamic>("RN")).ToList()) : ""
                        );
                        break;
                    case "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY":
                        int mismatch = 0;
                        var chkdataCol = objCommon.DbaseQueryReturnTable("select top 1  * from DATA", CommonFilepath);
                        var dtdata = new DataTable();
                        dtdata.Columns.Add("FieldName");
                        dtdata.Columns.Add("RN");

                        for (int ii = 0; ii < chkdataCol.Columns.Count; ii++)
                        {
                            dtdata.Rows.Add(chkdataCol.Columns[ii].ToString().ToUpper().Trim(), (ii + 1));
                        }
                        var chkdatadic = objCommon.DbaseQueryReturnTable("select ltrim(rtrim(ucase(Field_Name))) as FieldName, RN from DATA_DICTIONARY", CommonFilepath);
                        var diff = (from d1 in chkdatadic.AsEnumerable()
                                    join d2 in dtdata.AsEnumerable()
                                    on new
                                    {
                                        a = d1.Field<string>("FieldName"),
                                        b = Convert.ToInt32(d1.Field<dynamic>("RN"))
                                    }
                                    equals new
                                    {
                                        a = d2.Field<string>("FieldName"),
                                        b = Convert.ToInt32(d2.Field<dynamic>("RN"))
                                    }
                                    into tg
                                    from tcheck in tg.DefaultIfEmpty()
                                    where tcheck == null
                                    select d1);

                        lstFinalPassed.Rows.Add(

                            "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY",
                            diff.Any() ? "Fail" : "Pass",
                            diff.Any() ? "Order of Fields in table DATA should not match the order of fields in table DATA_DICTIONARY" : ""
                        );
                        break;
                    case "All fields in DATA Should present in DATA_Dictionary":
                        string notfoundFields = string.Empty;
                        for (int ii = 0; ii < allColumnName.Columns.Count; ii++)
                        {
                            if (!DataDicField.Contains(allColumnName.Columns[ii].ColumnName.ToString().ToUpper()))
                            {

                                notfoundFields += allColumnName.Columns[ii].ColumnName.ToString() + ",";
                            }
                        }
                        if (notfoundFields != string.Empty)
                        {
                            notfoundFields = notfoundFields.TrimEnd(',');
                            lstFinalPassed.Rows.Add(

                                "All fields in DATA Should present in DATA_Dictionary",
                                "Fail",
                                 notfoundFields
                            );
                        }
                        else
                        {
                            lstFinalPassed.Rows.Add(

                                "All fields in DATA Should present in DATA_Dictionary",
                                "Pass",
                                ""
                            );
                        }
                        break;
                    case "Field sizes in DATA should same define in DATA_DICTIONARY":
                        var res = _objLibraryFunction_Service.GetSchemaBtn_Click(CommonFilepath);
                        lstFinalPassed.Rows.Add(

                             "Field sizes in DATA should same define in DATA_DICTIONARY - " + res,
                             res == string.Empty ? "Pass" : "Fail",
                            res == string.Empty ? "" : res
                        );
                        break;
                    case "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY":
                        var NonNotesFields = string.Empty;
                        var chkNoteCodeField = objCommon.DbaseQueryReturnTable("select distinct NOTES_FIELD from notes a, Data_Dictionary b where a.notes_field = b.FIELD_NAME where Data_Type <> " + noteDataTypeId, CommonFilepath);
                        for (int ii = 0; ii < chkNoteCodeField.Rows.Count; ii++)
                        {
                            NonNotesFields += chkNoteCodeField.Rows[ii][0].ToString() + ",";
                        }
                        if (NonNotesFields != string.Empty)
                        {
                            NonNotesFields = NonNotesFields.TrimEnd(',');
                            lstFinalPassed.Rows.Add(

                                "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY",
                                "Fail",
                                NonNotesFields
                            );
                        }
                        else
                        {
                            lstFinalPassed.Rows.Add(

                                "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY",
                                "Pass",
                                ""
                            );
                        }
                        break;
                    case "Normalized FieldName not in Data-Dictionary":
                        using (var context = new CRAModel())
                        {
                            List<string> notExistinDD = new List<string>();
                            var fieldNameInDataDic = objCommon.DbaseQueryReturnTable("select FIELD_NAME from Data_Dictionary", CommonFilepath);
                            var chkMapQsid = mpQsidFld.Where(x => x.UnitID != null && x.UnitFieldNameID != null).ToList();
                            var virtualFields = chkMapQsid.Select(y => y.UnitFieldNameID).ToList();
                            var fieldNames = (from d1 in mpQsidFld.Where(x => x.IsInternalOnly == false && x.ParentId == null).ToList()
                                              join d2 in context.Library_FieldNames.AsNoTracking()
                                                  on d1.FieldNameID equals d2.FieldNameID
                                              select new
                                              {
                                                  FieldName = d2.FieldName.Replace("\n", string.Empty).Replace("\r", string.Empty).ToUpper(),
                                                  d2.FieldNameID,
                                              }).ToList();
                            if (virtualFields.Any())
                            {
                                fieldNames = fieldNames.Where(x => !virtualFields.Contains(x.FieldNameID)).ToList();
                            }
                            fieldNames.ToList().ForEach(x =>
                            {
                                if (fieldNameInDataDic.AsEnumerable().Count(xx => xx.Field<string>("FIELD_NAME").ToUpper() == x.FieldName) == 0)
                                {
                                    notExistinDD.Add(x.FieldName);
                                }
                            });
                            lstFinalPassed.Rows.Add(

                                "Normalized FieldName not in Data - Dictionary",
                                notExistinDD.Count() > 0 ? "Fail" : "Pass",
                                string.Join(",", notExistinDD)
                            );
                        }
                        break;
                    case "Field Names cannot contain Space":
                        var chkSpace = allFieldNameofDD.AsEnumerable().Where(x => x.Field<string>("FIELD_NAME").Contains(" ")).Select(y => y.Field<string>("FIELD_NAME")).ToList();
                        lstFinalPassed.Rows.Add(
                            "Field Names cannot contain Space",
                            chkSpace.Count == 0 ? "Pass" : "Fail",
                            string.Join(",", chkSpace)
                        );
                        break;
                    case "FieldNames cannot start with a number":
                        var numErr = new List<string>();
                        var chkNumber = allFieldNameofDD.AsEnumerable().Select(y => y.Field<string>("FIELD_NAME")).ToList();
                        foreach (var ct in chkNumber)
                        {
                            char[] charArr = ct.ToCharArray();
                            if (Char.IsDigit(charArr[0]))
                            {
                                numErr.Add(ct);
                            }
                        }
                        lstFinalPassed.Rows.Add(
                            "FieldNames cannot start with a number",
                                        numErr.Count == 0 ? "Pass" : "Fail",
                                        string.Join(",", numErr)
                                    );
                        break;
                    case "Field Names cannot contain any characters other than [a-z][0-9]_":
                        var numDig = new List<string>();
                        var chkDig = allFieldNameofDD.AsEnumerable().Select(y => y.Field<string>("FIELD_NAME")).ToList();
                        foreach (var ct in chkDig)
                        {
                            if (ct.All(c => c >= 48 && c <= 57 || c >= 65 && c <= 90 || c >= 97 && c <= 122 || c == 95))
                            {
                            }
                            else
                            {
                                numDig.Add(ct);
                            }
                        }
                        lstFinalPassed.Rows.Add(
                            "Field Names cannot contain any characters other than [a-z][0-9]_",
                                        numDig.Count == 0 ? "Pass" : "Fail",
                                        string.Join(",", numDig)
                                    );
                        break;
                    case "Max length of a note text should be less than 2000 characters":
                        var fieldNams = objCommon.DbaseQueryReturnTable("select FIELD_NAME as FieldName from DATA_Dictionary where DATA_TYPE =6", CommonFilepath);
                        var chknoteCodeField = objCommon.DbaseQueryReturnTableSql("select FieldName as FieldName from Library_FieldNames a, Map_QSID_FieldName b " +
                        " where a.FieldNameID = b.FieldNameID and b.isNoteCodeField = 1 and b.QSID_ID = " + lstqsidid, CommonListConn);
                        if (chknoteCodeField.Rows.Count > 0)
                        {
                            fieldNams.Merge(chknoteCodeField);
                        }
                        string extraQuery = string.Empty;
                        if (fieldNams.Rows.Count > 0)
                        {
                            var uniqueFields = fieldNams.AsEnumerable().Select(x => "'" + x.Field<string>("FieldName").ToUpper() + "'").Distinct().ToList();
                            var joinAll = string.Join(",", uniqueFields);
                            extraQuery = " and ucase(NOTES_FIELD) in (" + joinAll + ")";
                        }
                        var notesField = objCommon.DbaseQueryReturnTable("select NOTES_FIELD & '(' & NOTE_CODE & ')' as NOTES_FIELD_CODE from Notes where (len(NOTE_TEXT) > 2000 or len(NOTE_ENGL) > 2000 ) " + extraQuery, CommonFilepath);
                        lstFinalPassed.Rows.Add(
                             "Max length of a note text should be less than 2000 characters",
                                         notesField.Rows.Count == 0 ? "Pass" : "Fail",
                                         string.Join(",", notesField.AsEnumerable().Select(y => y.Field<string>("NOTES_FIELD_CODE")).ToList())
                                     );
                        break;
                }
            }
            return lstFinalPassed;
        }

        private void AddFromMapQSIDFieldNameDefaults(int qsidId, int sessID)
        {
            var AllDicData = objCommon.DbaseQueryReturnTable("select FIELD_NAME,DisplayField2Clients,SourceFieldDisplayOrder from DATA_DICTIONARY", CommonFilepath);
            CheckNewFieldNames(AllDicData);
            using (var context = new CRAModel())
            {
                var onlyFieldName = AllDicData.AsEnumerable().Select(x => x.Field<string>("FIELD_NAME").Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty).Trim().ToUpper()).ToList();
                var allDicDataLst = AllDicData.AsEnumerable().Select(x => new { FIELD_NAME = x.Field<string>("FIELD_NAME").Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty).Trim(), DisplayField2Client = x.Field<bool?>("DisplayField2Clients"), SourceFieldDisplayOrder = x.Field<dynamic>("SourceFieldDisplayOrder") == null ? (int?)null : Convert.ToInt32(x.Field<dynamic>("SourceFieldDisplayOrder")) }).Distinct().ToList();
                var fromLib = context.Library_FieldNames.AsNoTracking().Where(x => onlyFieldName.Contains(x.FieldName.ToUpper())).Select(y => new { y.FieldName, y.FieldNameID }).Distinct().ToList();

                var joinWithLibr = (from d1 in allDicDataLst
                                    join d2 in fromLib
                                    on d1.FIELD_NAME.ToUpper() equals d2.FieldName.ToUpper()
                                    select new { d1.FIELD_NAME, d1.DisplayField2Client, d1.SourceFieldDisplayOrder, d2.FieldNameID }).ToList();

                var joinWithDD = (from d1 in joinWithLibr
                                  join d2 in context.Map_QSID_FieldName_Defaults.AsNoTracking()
                                  on d1.FieldNameID equals d2.FieldNameID into tg
                                  from tcheck in tg.DefaultIfEmpty()
                                  select new IMapFields { FieldNameID = d1.FieldNameID, DisplayField2Client = d1.DisplayField2Client, SourceFieldDisplayOrder = d1.SourceFieldDisplayOrder, lst = tcheck == null ? null : tcheck }).ToList();

                var currmapQsid = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidId).ToList();
                List<Map_QSID_FieldName> lstfinal = new List<Map_QSID_FieldName>();
                List<Log_Map_QSID_FieldName> lstlogfinal = new List<Log_Map_QSID_FieldName>();
                int counter = 0;
                int srtOrder = 0;
                int maxFieldId = this.objCommon.GetMaxAutoField("Map_QSID_FieldName");
                for (int i = 0; i < joinWithDD.Count; i++)
                {
                    var fieldId = joinWithDD[i].FieldNameID;
                    var chkExst = currmapQsid.FirstOrDefault(x => x.FieldNameID == fieldId);
                    if (chkExst == null)
                    {
                        if (joinWithDD[i].lst != null && joinWithDD[i].lst.IsInternalOnly == false)
                        {
                            counter += 1;
                            srtOrder = counter;
                        }
                        else
                        {
                            srtOrder = 999;
                        }
                        lstfinal.Add(AddInMapQsidFieldName(joinWithDD[i].lst, qsidId, srtOrder, joinWithDD[i].DisplayField2Client, joinWithDD[i].SourceFieldDisplayOrder, joinWithDD[i].FieldNameID));
                        lstlogfinal.Add(AddInLogMapQsidFieldName(joinWithDD[i].lst, qsidId, srtOrder, joinWithDD[i].DisplayField2Client, joinWithDD[i].SourceFieldDisplayOrder, sessID, joinWithDD[i].FieldNameID, maxFieldId));
                        maxFieldId += 1;
                    }
                }
                _objLibraryFunction_Service.BulkIns<Map_QSID_FieldName>(lstfinal);
                _objLibraryFunction_Service.BulkIns<Log_Map_QSID_FieldName>(lstlogfinal);

            }
        }
        private Map_QSID_FieldName AddInMapQsidFieldName(Map_QSID_FieldName_Defaults lst, int qsidId, int srtOrder, bool? displayField2Client, int? sortfield, int FieldNameID)
        {
            return new Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = 0,
                FieldNameID = FieldNameID,
                IsInternalOnly = lst == null ? true : lst.IsInternalOnly,
                IsCASField = lst == null ? false : lst.IsCASField,
                IsChemicalName = lst == null ? false : lst.IsChemicalName,
                IsIdentifierField = lst == null ? false : lst.IsIdentifierField,
                IsNoteCodeField = lst == null ? false : lst.IsNoteCodeField,
                IsPhraseField = lst == null ? false : lst.IsPhraseField,
                IsValueOther = lst == null ? false : lst.IsValueOther,
                IsTrueFalseField = lst == null ? false : lst.IsTrueFalseField,
                PhraseCategoryID = lst == null ? null : lst.PhraseCategoryID,
                IdentifierTypeID = lst == null ? null : lst.IdentifierTypeID,
                SubstanceNameCategoryId = lst == null ? null : lst.SubstanceNameCategoryId,
                LanguageID = lst == null ? null : lst.LanguageID,
                IsMinValue = lst == null ? false : lst.IsMinValue,
                IsMaxValue = lst == null ? false : lst.IsMaxValue,
                IsMinValOp = lst == null ? false : lst.IsMinValOp,
                IsMaxValOp = lst == null ? false : lst.IsMaxValOp,
                IsUnit = lst == null ? false : lst.IsUnit,
                UnitID = lst == null ? null : lst.UnitID,
                UnitFieldNameID = lst == null ? null : lst.UnitFieldNameID,
                Operator_ValueFieldID = lst == null ? null : lst.Operator_ValueFieldID,
                RangeDelimiterID = lst == null ? null : lst.RangeDelimiterID,
                ValueFieldContainsOperators = lst == null ? false : lst.ValueFieldContainsOperators,
                UnitDelimiterID = lst == null ? null : lst.UnitDelimiterID,
                ConsiderSingleValueAs = lst == null ? null : lst.ConsiderSingleValueAs,
                ConsiderNullValueAs = lst == null ? null : lst.ConsiderNullValueAs,
                AllowedTrueFalseValues = lst == null ? null : lst.AllowedTrueFalseValues,
                MultiValuePhrase = lst == null ? false : lst.MultiValuePhrase,
                MultiValuePhraseDelimiterID = lst == null ? null : lst.MultiValuePhraseDelimiterID,
                MultiValueIdentifier = lst == null ? null : lst.MultiValueIdentifier,
                MultiValueIdentifierDelimiterID = lst == null ? null : lst.MultiValueIdentifierDelimiterID,
                ChemicalNameDelimiterID = lst == null ? null : lst.ChemicalNameDelimiterID,
                ValueFieldContainsRanges = lst == null ? false : lst.ValueFieldContainsRanges,
                ValueFieldContainsUnits = lst == null ? false : lst.ValueFieldContainsUnits,
                QSID_ID = qsidId,
                ThresholdCategory = lst == null ? null : lst.ThresholdCategory,
                FieldSortOrder = srtOrder,
                DisplayField2Client = displayField2Client,
                SourceFieldDisplayOrder = sortfield,
                ParentId = null,
                ChildSortOrder = null,
                DateFormatID = null,
            };
        }
        private Log_Map_QSID_FieldName AddInLogMapQsidFieldName(Map_QSID_FieldName_Defaults lst, int qsidId, int srtOrder, bool? displayField2Client, int? sortfield, int sessID, int FieldNameID, int maxField)
        {
            return new Log_Map_QSID_FieldName
            {
                MapQSIDFieldName_ID = maxField,
                FieldNameID = FieldNameID,
                IsInternalOnly = lst == null ? true : lst.IsInternalOnly,
                IsCASField = lst == null ? false : lst.IsCASField,
                IsChemicalName = lst == null ? false : lst.IsChemicalName,
                IsIdentifierField = lst == null ? false : lst.IsIdentifierField,
                IsNoteCodeField = lst == null ? false : lst.IsNoteCodeField,
                IsPhraseField = lst == null ? false : lst.IsPhraseField,
                IsValueOther = lst == null ? false : lst.IsValueOther,
                IsTrueFalseField = lst == null ? false : lst.IsTrueFalseField,
                PhraseCategoryID = lst == null ? null : lst.PhraseCategoryID,
                IdentifierTypeID = lst == null ? null : lst.IdentifierTypeID,
                SubstanceNameCategoryId = lst == null ? null : lst.SubstanceNameCategoryId,
                LanguageID = lst == null ? null : lst.LanguageID,
                IsMinValue = lst == null ? false : lst.IsMinValue,
                IsMaxValue = lst == null ? false : lst.IsMaxValue,
                IsMinValOp = lst == null ? false : lst.IsMinValOp,
                IsMaxValOp = lst == null ? false : lst.IsMaxValOp,
                IsUnit = lst == null ? false : lst.IsUnit,
                UnitID = lst == null ? null : lst.UnitID,
                UnitFieldNameID = lst == null ? null : lst.UnitFieldNameID,
                Operator_ValueFieldID = lst == null ? null : lst.Operator_ValueFieldID,
                RangeDelimiterID = lst == null ? null : lst.RangeDelimiterID,
                ValueFieldContainsOperators = lst == null ? false : lst.ValueFieldContainsOperators,
                UnitDelimiterID = lst == null ? null : lst.UnitDelimiterID,
                ConsiderSingleValueAs = lst == null ? null : lst.ConsiderSingleValueAs,
                ConsiderNullValueAs = lst == null ? null : lst.ConsiderNullValueAs,
                AllowedTrueFalseValues = lst == null ? null : lst.AllowedTrueFalseValues,
                MultiValuePhrase = lst == null ? false : lst.MultiValuePhrase,
                MultiValuePhraseDelimiterID = lst == null ? null : lst.MultiValuePhraseDelimiterID,
                MultiValueIdentifier = lst == null ? null : lst.MultiValueIdentifier,
                MultiValueIdentifierDelimiterID = lst == null ? null : lst.MultiValueIdentifierDelimiterID,
                ChemicalNameDelimiterID = lst == null ? null : lst.ChemicalNameDelimiterID,
                ValueFieldContainsRanges = lst == null ? false : lst.ValueFieldContainsRanges,
                ValueFieldContainsUnits = lst == null ? false : lst.ValueFieldContainsUnits,
                QSID_ID = qsidId,
                ThresholdCategory = lst == null ? null : lst.ThresholdCategory,
                FieldSortOrder = srtOrder,
                DisplayField2Client = displayField2Client,
                SourceFieldDisplayOrder = sortfield,
                ParentId = null,
                ChildSortOrder = null,
                DateFormatID = null,
                Status = "A",
                TimeStamp = objCommon.ESTTime(),
                SessionID = sessID,
            };
        }

        public void CheckNewFieldNames(DataTable AllDicData)
        {
            List<Library_FieldNames> lstFinal = new List<Library_FieldNames>();
            int maxFieldId = this.objCommon.GetMaxAutoField("Library_FieldNames");
            for (int i = 0; i < AllDicData.Rows.Count; i++)
            {
                string fieldName = objCommon.GetFirstRecordMatchedWhereFieldNameRemSpecialFromStaticVariable(AllDicData.Rows[i].Field<string>("FIELD_NAME"));
                if (fieldName == null)
                {
                    Library_FieldNames fieldNameLibrary = new Library_FieldNames
                    {
                        FieldName = AllDicData.Rows[i].Field<string>("FIELD_NAME").Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\t", string.Empty),
                        FieldNameID = maxFieldId,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstFinal.Add(fieldNameLibrary);
                    maxFieldId += 1;
                }
            }

            lstFinal = lstFinal.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x.FieldName) == false).GroupBy(y => y.FieldName).Select(z => z.FirstOrDefault()).ToList();
            lstFinal = lstFinal.AsEnumerable().Where(x => (string.IsNullOrWhiteSpace(x.FieldName.Trim()) == false) && (x.FieldName.Length <= 255)).ToList();
            _objLibraryFunction_Service.BulkIns<Library_FieldNames>(lstFinal);
        }

        private void postCheck(int qsidid)
        {
            var resultInternal = objCommon.DbaseQueryReturnTableSql("select distinct c.FieldNameID,c.FieldName from (select a.FieldNameId, FieldName, QSID_ID from Map_QSID_FieldName a, library_fieldnames d where a.IsInternalOnly = 0 and a.fieldNameid = d.fieldnameid and  a.QSID_ID = " + qsidid + ") c " +
              " left join QSxxx_DataDictionary b " +
              " on b.QSID_ID = c.QSID_ID and c.FieldNameID = b.FieldNameID " +
              " where  b.FieldNameID is null", CommonListConn);
            if (resultInternal.Rows.Count > 0)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowError("All non-internal fields in not in QSxxx_DataDictionary " + string.Join(",", resultInternal.AsEnumerable().Select(x => x.Field<string>("FieldName")).ToList()));
                });
            }
        }
    }
    public class IChkNameData
    {
        public bool IsSelected { get; set; }
        public string ChkDescription { get; set; }
        public int ParentID { get; set; }
    }
    public class IMapFields
    {
        public Map_QSID_FieldName_Defaults lst { get; set; }
        public int FieldNameID { get; set; }
        public bool? DisplayField2Client { get; set; }
        public int? SourceFieldDisplayOrder { get; set; }
    }

}
