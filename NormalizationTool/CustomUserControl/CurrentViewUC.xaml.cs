﻿using System;
using System.Windows.Controls;

namespace NormalizationTool.CustomUserControl
{
    /// <summary>
    /// Interaction logic for CurrentViewUC.xaml
    /// </summary>
    public partial class CurrentViewUC : UserControl
    {
        public CurrentViewUC()
        {
            InitializeComponent();
        }
        private void dataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                var dg = sender as DataGrid;
                if (dg != null && e.AddedCells != null && e.AddedCells.Count > 0)
                {
                    var cell = e.AddedCells[0];
                    if (!cell.IsValid)
                        return;
                    var generator = dg.ItemContainerGenerator;
                    //int columnIndex = cell.Column.DisplayIndex; //OK as long user can't reorder
                    int rowIndex = generator.IndexFromContainer(generator.ContainerFromItem(cell.Item));
                    lblCurrentRow.Content = "Current Row - " + (Convert.ToInt32(rowIndex) + 1);
                }
            }
            catch { }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
