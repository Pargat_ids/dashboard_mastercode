﻿using System.Windows;

namespace NormalizationTool.CustomUserControl
{
    /// <summary>
    /// Interaction logic for ChildPopup.xaml
    /// </summary>
    public partial class ChildPopup : Window
    {
        public ChildPopup()
        {
            InitializeComponent();
        }
    }
}
