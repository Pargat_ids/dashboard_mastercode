﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using CRA_DataAccess;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;

namespace CRA_CasAssignment.Resource.Library
{
    public class Base_ViewModel : INotifyPropertyChanged
    {
        public IAccess_Version_BL _objAccessVersion_Service { get; private set; }
        public ILogUserSessionBL _objLogUserSession_Service { get; private set; }
        public ICasAssignmentBL _objCasAssignment_Service { get; private set; }
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        private IMessenger _messengerInstance { get; set; }
        protected IMessenger MessengerInstance
        {
            get
            {
                return this._messengerInstance ?? Messenger.Default;
            }
            set
            {
                this._messengerInstance = value;
            }
        }
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public ICommand CommandGenerateExcelCasResult { get; set; }
        public Notifier _notifier { get; set; }

       

        public Base_ViewModel()
        {
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (_objAccessVersion_Service == null)
            {
                _objAccessVersion_Service = new Access_Version_BL(_objLibraryFunction_Service);
            }
            if (_objCasAssignment_Service == null)
            {
                _objCasAssignment_Service = new CasAssignmentBL(_objLibraryFunction_Service);
            }
            if (_objLogUserSession_Service == null)
            {
                _objLogUserSession_Service = new LogUserSessionBL(_objLibraryFunction_Service);
            }

            _notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new WindowPositionProvider(
                    parentWindow: Application.Current.MainWindow,
                    corner: Corner.BottomRight,
                    offsetX: 10,
                    offsetY: 10);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(10),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(5));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });

        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        public string ExportExcelFile(DataTable table, string destination)
        {
            try
            {
                foreach (DataColumn lCol in table.Columns)
                {
                    if (lCol.ColumnName.ToString().ToLower().Equals("_notifier"))
                    {
                        table.Columns.Remove(lCol);
                        break;
                    }
                }
                table.AcceptChanges();
                var workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook);
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();
                    //foreach (System.Data.DataTable table in ds.Tables)
                    //{
                    var sheetPart = workbook.WorkbookPart.AddNewPart<DocumentFormat.OpenXml.Packaging.WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }
                    //}
                }
                workbook.WorkbookPart.Workbook.Save();
                workbook.Close();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExportAccessFile(DataTable _objDataTable, string fileName, IAccess_Version_BL _objAccessVersion_Service)
        {
            try
            {
                foreach (DataColumn lCol in _objDataTable.Columns)
                {
                    if (lCol.ColumnName.ToString().ToLower().Equals("_notifier"))
                    {
                        _objDataTable.Columns.Remove(lCol);
                        break;
                    }
                }
                _objDataTable.AcceptChanges();
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                //IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable, fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public static DataTable ConvertDataTableFromListDetail<T>(List<T> listDetails)
        {
            DataTable table = new DataTable();
            PropertyDescriptorCollection properties =
                  TypeDescriptor.GetProperties(typeof(T));

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, typeof(string));
            }
            foreach (T item in listDetails)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.PropertyType.FullName == "VersionControlSystem.Model.ViewModel.SubstanceType_VM")
                    {
                        var obj = prop.GetValue(item);
                        row[prop.Name] = obj != null ? (obj.GetType().GetProperty("SubstanceTypeName").GetValue(obj) != null ? obj.GetType().GetProperty("SubstanceTypeName").GetValue(obj).ToString() : "") : "";
                    }
                    else if (prop.PropertyType.FullName == "VersionControlSystem.Model.ViewModel.SubstanceCategories_VM")
                    {
                        var obj = prop.GetValue(item);
                        row[prop.Name] = obj != null ? (obj.GetType().GetProperty("SubstanceCategoryName").GetValue(obj) != null ? obj.GetType().GetProperty("SubstanceCategoryName").GetValue(obj).ToString() : "") : "";
                    }
                    else if (prop.Name == "listSubstanceType_VM")
                    {
                        var obj = prop.GetValue(item);
                        row[prop.Name] = row["SelectedSubstanceType"].ToString();
                        //row[prop.Name] = obj != null ? (obj.GetType().GetProperty("SubstanceType_VM.SubstanceTypeName").GetValue(obj) != null ? obj.GetType().GetProperty("SubstanceType_VM.SubstanceTypeName").GetValue(obj).ToString() : "") : "";
                    }
                    else if (prop.Name == "listSubstanceCategories_VM")
                    {
                        var obj = prop.GetValue(item);
                        row[prop.Name] = row["SelectedSubstanceCate"].ToString();
                        //row[prop.Name] = obj != null ? (obj.GetType().GetProperty("SubstanceCategories_VM.SubstanceCategoryName").GetValue(obj) != null ? obj.GetType().GetProperty("SubstanceCategories_VM.SubstanceCategoryName").GetValue(obj).ToString() : "") : "";
                    }
                    else
                    {
                        row[prop.Name] = prop.GetValue(item) != null ? prop.GetValue(item).ToString() : "";
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
