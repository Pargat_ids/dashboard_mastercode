﻿using CRA_CasAssignment.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CRA_CasAssignment.Common
{
    public static class CommonGeneralFunction
    {
        public static List<CasAssignChemicalNamesVM> ConvertCasAssignmentToViewModel(List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> listCasAssignChemNameVM, List<VersionControlSystem.Model.ViewModel.SubstanceType_VM> listSubstanceType, List<VersionControlSystem.Model.ViewModel.SubstanceCategories_VM> listSubstanceCate)
        {
            return listCasAssignChemNameVM.Select(x => new CasAssignChemicalNamesVM { OtherCasCount = string.IsNullOrEmpty( x.OtherCasCount)?0:Convert.ToInt32( x.OtherCasCount), OtherQsidCount =  string.IsNullOrEmpty(x.OtherQsidCount) ? 0 : Convert.ToInt32(x.OtherQsidCount), ChemicalNameCount =  string.IsNullOrEmpty(x.ChemicalNameCount) ? 0 : Convert.ToInt32(x.ChemicalNameCount), ChemicalNameID = x.ChemicalNameID, CasDetailList = x.CasDetailList != null ? x.CasDetailList.Select(y => new VersionControlSystem.Model.ViewModel.CasDetailVM { Cas = y.Cas, QsidCount = y.QsidCount, CasID = y.CasID, IsValid = y.IsValid, SubstanceTypeID = y.SubstanceTypeID, SubstanceCategoryID = y.SubstanceCategoryID, IsInCorrect_Cas = y.IsInCorrect_Cas }).ToList() : (List<VersionControlSystem.Model.ViewModel.CasDetailVM>)null, IsInCorrect = x.IsInCorrect, CasAssigned = x.CasAssigned, CasCount = x.CasDetailList != null ? x.CasDetailList.Count : 0, ChemicalName = x.ChemicalName, OriginalChemicalName = x.OriginalChemicalName, Frequency = x.Frequency, RefernceRN = x.RefernceRN, Type = x.Type, SubCasType = x.SubCasType, Qsid = x.Qsid, OverrideCasComments = x.OverrideCasComments, IsOverWriteCasAssigned = x.IsOverWriteCasAssigned, listSubstanceType_VM = new ObservableCollection<VersionControlSystem.Model.ViewModel.SubstanceType_VM>(listSubstanceType), listSubstanceCategories_VM = new ObservableCollection<VersionControlSystem.Model.ViewModel.SubstanceCategories_VM>(listSubstanceCate), SubstanceTypeIDSelected = x.SubstanceTypeIDSelected, SubstanceCategoryIDSelected = x.SubstanceCategoryIDSelected, AlternateCasCount = x.AlternateCasCount, ChildrenCount = x.ChildrenCount }).OrderBy(x => x.ChemicalName).ToList();
        }
        public static List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> ConvertCasAssignmentToViewModel(List<CasAssignChemicalNamesVM> listCasAssignChemNameVM)
        {
            return listCasAssignChemNameVM.Select(x => new CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM { ChemicalNameID = x.ChemicalNameID, CasDetailList = x.CasDetailList != null ? x.CasDetailList.Select(y => new CRA_DataAccess.ViewModel.CasDetailVM { Cas = y.Cas, QsidCount = y.QsidCount, CasID = y.CasID, IsValid = y.IsValid }).ToList() : (List<CRA_DataAccess.ViewModel.CasDetailVM>)null, CasAssigned = x.CasAssigned, IsInCorrect = x.IsInCorrect, CasCount = x.CasCount, OriginalChemicalName = x.OriginalChemicalName, Frequency = x.Frequency, RefernceRN = x.RefernceRN, ChemicalName = x.ChemicalName, Type = x.Type, SubCasType = x.SubCasType, Qsid = x.Qsid, OverrideCasComments = x.OverrideCasComments, IsOverWriteCasAssigned = x.IsOverWriteCasAssigned, SubstanceTypeIDSelected = x.SubstanceTypeIDSelected, SubstanceCategoryIDSelected = x.SubstanceCategoryIDSelected }).OrderBy(x => x.ChemicalName).ToList();
        }
        public static List<VersionControlSystem.Model.ViewModel.SubstanceType_VM> ConvertSubstanceTypeToViewModel(List<EntityClass.Library_SubstanceType> listSubstanceType)
        {
            return listSubstanceType.Select(x => new VersionControlSystem.Model.ViewModel.SubstanceType_VM { SubstanceTypeID = x.SubstanceTypeID, SubstanceTypeName = x.SubstanceTypeName }).ToList();
        }
        public static List<VersionControlSystem.Model.ViewModel.SubstanceCategories_VM> ConvertSubstanceCategoryToViewModel(List<EntityClass.Library_SubstanceCategories> listSubstanceCate)
        {
            return listSubstanceCate.Select(x => new VersionControlSystem.Model.ViewModel.SubstanceCategories_VM { SubstanceCategoryID = x.SubstanceCategoryID, SubstanceCategoryName = x.SubstanceCategoryName }).ToList();
        }

        public static List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> ConvertIncorrectCasChemical(List<CRA_DataAccess.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> list)
        {
            return list.Select(x => new VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM { Cas = x.Cas, ActiveListsCount = x.ActiveListsCount, CasID = x.CasID, ChemicalName = x.ChemicalName, ChemicalNameId = x.ChemicalNameId, LanguageName = x.LanguageName }).ToList();
        }
    }
}
