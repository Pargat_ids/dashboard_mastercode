﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;
using VersionControlSystem.Model.ViewModel;

namespace CRA_CasAssignment.ViewModel
{
    public class CasAssignChemicalNamesVM : INotifyPropertyChanged
    {
        public Notifier _notifier { get; set; } =
         new Notifier(cfg =>
         {
             cfg.PositionProvider = new WindowPositionProvider(
                 parentWindow: Application.Current.MainWindow,
                 corner: Corner.BottomRight,
                 offsetX: 10,
                 offsetY: 10);

             cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                 notificationLifetime: TimeSpan.FromSeconds(10),
                 maximumNotificationCount: MaximumNotificationCount.FromCount(5));

             cfg.Dispatcher = Application.Current.Dispatcher;
         });
        public string ChemicalName { get; set; }
        public int ChemicalNameID { get; set; }
        public bool IsUpdateAuto { get; set; }
        private bool _IsOverWriteCasAssigned { get; set; }
        public bool IsOverWriteCasAssigned
        {
            get { return _IsOverWriteCasAssigned; }
            set
            {
                if (value != _IsOverWriteCasAssigned)
                {
                    _IsOverWriteCasAssigned = value;
                    //if (_IsOverWriteCasAssigned)
                    //{
                    //    InCorrectEnable = false;
                    //}
                    //else
                    //{
                    //    InCorrectEnable = true;
                    //}
                }
            }
        }
        public string OverrideCasComments { get; set; }
        private string _overWriteCasAssigned { get; set; }
        public string OverWriteCasAssigned
        {
            get { return _overWriteCasAssigned; }
            set
            {

                if (value != _overWriteCasAssigned)
                {
                    if (!string.IsNullOrEmpty(_overWriteCasAssigned) && IsUpdateAuto)
                    {
                        IsUpdateAuto = false;
                    }
                    _overWriteCasAssigned = value;
                    if (!string.IsNullOrEmpty(_overWriteCasAssigned))
                    {
                        IsOverWriteCasAssigned = true;
                        checkPrimaryNameForCas(_overWriteCasAssigned);
                    }
                    else
                    {
                        IsOverWriteCasAssigned = false;
                        IsUpdateAuto = false;
                    }
                    System.Threading.Tasks.Task.Run(() => checkCasExistInCorrectCasName());
                    OnPropertyChanged("OverWriteCasAssigned");
                }
            }
        }
        public List<CasDetailVM> CasDetailList { get; set; }
        private CasDetailVM _selectedCasDetail { get; set; }
        public CasDetailVM SelectedCasDetail
        {
            get { return _selectedCasDetail; }
            set
            {

                if (value != _selectedCasDetail)
                {
                    _selectedCasDetail = value;
                    if (_selectedCasDetail != null)
                    {
                        CasAssigned = _selectedCasDetail.Cas;
                        if (CasDetailList != null)
                        {
                            CasDetailList.ForEach(x => x.listQsidInMultiCas = null);
                        }
                        if (!IsOverWriteCasAssigned)
                        {
                            SubCasType = "Same Name Multiple Cas - Manually Update";
                        }
                        if (_selectedCasDetail.SubstanceTypeID != null)
                        {
                            SelectedSubstanceType = listSubstanceType_VM.Where(x => x.SubstanceTypeID == _selectedCasDetail.SubstanceTypeID).FirstOrDefault();
                        }
                        else
                        {
                            SelectedSubstanceType = null;
                        }
                        if (_selectedCasDetail.SubstanceCategoryID != null)
                        {
                            SelectedSubstanceCate = listSubstanceCategories_VM.Where(x => x.SubstanceCategoryID == _selectedCasDetail.SubstanceCategoryID).FirstOrDefault();
                        }
                        else
                        {
                            SelectedSubstanceCate = null;
                        }
                    }
                    else
                    {
                        if (!IsOverWriteCasAssigned)
                        {
                            SubCasType = string.Empty;
                        }
                    }
                    OnPropertyChanged("SelectedCasDetail");
                }

            }
        }
        public string CasAssigned
        {
            get { return _casAssigned; }
            set
            {
                if (value != _casAssigned)
                {
                    _casAssigned = value;
                    OnPropertyChanged("CasAssigned");
                    checkPrimaryNameForCas(_casAssigned);
                }
            }
        }
        private string _casAssigned { get; set; }
        public int CasCount { get; set; }
        public string Type { get; set; }
        public string Qsid { get; set; }
        public string SubCasType { get; set; }
        public string OriginalChemicalName { get; set; }
        public int? Frequency { get; set; }
        public string RefernceRN { get; set; }
        private ObservableCollection<Cas_MIC_ING_Seed> _listdetail_MIC_ING { get; set; }
        public ObservableCollection<Cas_MIC_ING_Seed> listdetail_MIC_ING
        {
            get { return _listdetail_MIC_ING; }
            set
            {
                if (value != _listdetail_MIC_ING)
                {
                    _listdetail_MIC_ING = value;
                    OnPropertyChanged("listdetail_MIC_ING");
                }
            }
        }
        private Cas_MIC_ING_Seed _selected_MIC_ING { get; set; }
        public Cas_MIC_ING_Seed Selected_MIC_ING
        {
            get { return _selected_MIC_ING; }
            set
            {
                if (_selected_MIC_ING != value)
                {
                    _selected_MIC_ING = value;
                    if (_selected_MIC_ING != null)
                    {
                        IsUpdateAuto = false;

                        OverWriteCasAssigned = _selected_MIC_ING.Prefix + FormatSeedValue(_selected_MIC_ING.NextSeed);
                        SubCasType = SubCasType + " - Assign " + (Is_MIC ? "MIC" : "ING") + " Identifier Update";
                    }
                    else
                    {
                        OverWriteCasAssigned = "";

                    }
                }
            }
        }
        public bool Is_MIC { get; set; }
        public bool Is_ING { get; set; }
        private Visibility _comboING_MIC { get; set; } = Visibility.Collapsed;
        public Visibility ComboING_MIC
        {
            get { return _comboING_MIC; }
            set
            {
                if (_comboING_MIC != value)
                {
                    _comboING_MIC = value;
                    OnPropertyChanged("ComboING_MIC");
                }
            }
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private string FormatSeedValue(int? seedValue)
        {
            return seedValue.ToString().PadLeft(7, '0'); ;
        }
        public int? SubstanceCategoryIDSelected { get; set; }
        public int? SubstanceTypeIDSelected { get; set; }

        private bool _InCorrectEnable { get; set; } = true;
        public bool InCorrectEnable
        {
            get { return _InCorrectEnable; }
            set
            {

                if (value != _InCorrectEnable)
                {
                    _InCorrectEnable = value;
                    OnPropertyChanged("InCorrectEnable");
                }
            }
        }
        private bool _isInCorrect { get; set; }
        public bool IsInCorrect
        {
            get { return _isInCorrect; }
            set
            {
                if (value != _isInCorrect)
                {
                    _isInCorrect = value;
                    //InsertUpdateIsIncorrect();
                    OnPropertyChanged("IsInCorrect");
                }
            }
        }
        //public bool enableRowControl { get; set; } = true;

        private SubstanceType_VM _selectedSubstanceType { get; set; }
        public SubstanceType_VM SelectedSubstanceType
        {
            get { return _selectedSubstanceType; }
            set
            {
                if (value != _selectedSubstanceType)
                {
                    _selectedSubstanceType = value;

                    if (SelectedCasDetail != null)
                    {
                        SelectedCasDetail.SubstanceTypeID = _selectedSubstanceType != null ? _selectedSubstanceType.SubstanceTypeID : (int?)null;
                    }
                    else
                    {
                        if (SubCasType != null)
                        {
                            if (SubCasType.Contains("Same Name One Cas"))
                            {
                                CasDetailList.FirstOrDefault().SubstanceTypeID = _selectedSubstanceType != null ? _selectedSubstanceType.SubstanceTypeID : (int?)null;
                            }
                        }
                    }

                    SubstanceTypeIDSelected = _selectedSubstanceType != null ? _selectedSubstanceType.SubstanceTypeID : (int?)null;

                    if (_selectedSubstanceType != null && _selectedSubstanceType.SubstanceTypeID == 1)
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            if (Type == "No Match")
                            {
                                if (string.IsNullOrEmpty(OverWriteCasAssigned) && string.IsNullOrWhiteSpace(OverWriteCasAssigned))
                                {
                                    _notifier.ShowWarning("Cas Assigned is empty and does not validate substance type in flat!");
                                }
                                else if (ValidateSubstanceTypeInFlat(OverWriteCasAssigned))
                                {
                                    _notifier.ShowError("Substance Type can not change to Single because this cas (" + OverWriteCasAssigned + ") active in Flat or Hydrate");
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(CasAssigned) && string.IsNullOrWhiteSpace(CasAssigned))
                                {
                                    _notifier.ShowWarning("Cas Assigned is empty and does not validate substance type in flat!");
                                }
                                else if (ValidateSubstanceTypeInFlat(CasAssigned))
                                {
                                    _notifier.ShowError("Substance Type can not change to Single because this cas (" + CasAssigned + ") active in Flat or Hydrate");
                                }
                            }
                        });
                    }
                }
            }

        }
        private SubstanceCategories_VM _selectedSubstanceCate { get; set; }
        public SubstanceCategories_VM SelectedSubstanceCate
        {
            get { return _selectedSubstanceCate; }
            set
            {
                if (value != _selectedSubstanceCate)
                {
                    _selectedSubstanceCate = value;
                    if (SelectedCasDetail != null)
                    {
                        SelectedCasDetail.SubstanceCategoryID = _selectedSubstanceCate != null ? _selectedSubstanceCate.SubstanceCategoryID : (int?)null;
                    }
                    else
                    {
                        if (SubCasType != null)
                        {
                            if (SubCasType.Contains("Same Name One Cas"))
                            {
                                CasDetailList.FirstOrDefault().SubstanceCategoryID = _selectedSubstanceCate != null ? _selectedSubstanceCate.SubstanceCategoryID : (int?)null;
                            }
                        }
                    }
                    SubstanceCategoryIDSelected = _selectedSubstanceCate != null ? _selectedSubstanceCate.SubstanceCategoryID : (int?)null;
                    OnPropertyChanged("SelectedSubstanceCate");
                }
            }
        }
        private ObservableCollection<SubstanceType_VM> _listSubstanceType_VM = new ObservableCollection<SubstanceType_VM>();
        private ObservableCollection<SubstanceCategories_VM> _listSubstanceCategories_VM = new ObservableCollection<SubstanceCategories_VM>();
        public ObservableCollection<SubstanceType_VM> listSubstanceType_VM
        {
            get { return _listSubstanceType_VM; }
            set
            {
                if (value != _listSubstanceType_VM)
                {
                    _listSubstanceType_VM = value;
                    OnPropertyChanged("listSubstanceType_VM");
                }
            }
        }
        public ObservableCollection<SubstanceCategories_VM> listSubstanceCategories_VM
        {
            get { return _listSubstanceCategories_VM; }
            set
            {
                if (value != _listSubstanceCategories_VM)
                {
                    _listSubstanceCategories_VM = value;
                    OnPropertyChanged("listSubstanceCategories_VM");
                }
            }
        }
        public Visibility visibleNotRecomended { get; set; } = Visibility.Collapsed;
        public bool NotRecomendedOverWriteCas { get; set; }
        private void checkCasExistInCorrectCasName()
        {

            CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service = CRA_DataAccess.LibraryFunction.GetInstance;
            NotRecomendedOverWriteCas = _objLibraryFunction_Service.CheckCasExistInMap_Incorrect_Cas_ChemicalName(ChemicalName, OverWriteCasAssigned, VersionControlSystem.Model.ApplicationEngine.Engine.DelimiterForQsidCasAssignmentTool);
            OnPropertyChanged("NotRecomendedOverWriteCas");
            visibleNotRecomended = NotRecomendedOverWriteCas ? Visibility.Visible : Visibility.Collapsed;
            OnPropertyChanged("visibleNotRecomended");
        }
        //private void InsertUpdateIsIncorrect()
        //{
        //    if (!string.IsNullOrEmpty(ChemicalName) && !string.IsNullOrEmpty(CasAssigned))
        //    {
        //        CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service = CRA_DataAccess.LibraryFunction.GetInstance;
        //        if (IsInCorrect)
        //        {
        //            _objLibraryFunction_Service.InsertUpdateIsIncorrectCasName(ChemicalName, ApplicationEngine.Engine.DelimiterForQsidCasAssignmentTool,CasAssigned, 0, ApplicationEngine.Engine.CurrentUserSessionID);
        //        }
        //        else if(IsInCorrect==false)
        //        {
        //            _objLibraryFunction_Service.DeleteMap_Incorrect_Cas_ChemicalName(ChemicalName, ApplicationEngine.Engine.DelimiterForQsidCasAssignmentTool, CasAssigned, ApplicationEngine.Engine.CurrentUserSessionID);
        //        }
        //    }



        //}
        private void checkPrimaryNameForCas(string cas)
        {
            if (!string.IsNullOrEmpty(cas) && !string.IsNullOrWhiteSpace(cas))
            {
                CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service = CRA_DataAccess.LibraryFunction.GetInstance;
                CasPrimaryName = _objLibraryFunction_Service.GetPrimaryNameByCas(cas);
            }
        }
        public int? ChemicalNameCount { get; set; }
        public int? OtherCasCount { get; set; }
        public int? OtherQsidCount { get; set; }
        public int? ChildrenCount { get; set; }
        public int? AlternateCasCount { get; set; }

        private bool _listOtherChemicalNameVisibility { get; set; } = false;
        private bool _listOtherQsidVisibility { get; set; } = false;
        private bool _listOtherCasVisibility { get; set; } = false;
        private bool _listChildrenVisibility { get; set; } = false;
        private bool _listAlternateVisibility { get; set; } = false;
        public bool ListOtherChemicalNameVisibility
        {
            get { return _listOtherChemicalNameVisibility; }
            set
            {
                if (_listOtherChemicalNameVisibility != value)
                {
                    _listOtherChemicalNameVisibility = value;
                    OnPropertyChanged("ListOtherChemicalNameVisibility");
                }
            }
        }
        public bool ListOtherQsidVisibility
        {
            get { return _listOtherQsidVisibility; }
            set
            {
                if (_listOtherQsidVisibility != value)
                {
                    _listOtherQsidVisibility = value;
                    OnPropertyChanged("ListOtherQsidVisibility");
                }
            }
        }
        public bool ListOtherCasVisibility
        {
            get { return _listOtherCasVisibility; }
            set
            {
                if (_listOtherCasVisibility != value)
                {
                    _listOtherCasVisibility = value;
                    OnPropertyChanged("ListOtherCasVisibility");
                }
            }
        }
        public bool ListChildrenVisibility
        {
            get { return _listChildrenVisibility; }
            set
            {
                if (_listChildrenVisibility != value)
                {
                    _listChildrenVisibility = value;
                    OnPropertyChanged("ListChildrenVisibility");
                }
            }
        }
        public bool ListAlternateVisibility
        {
            get { return _listAlternateVisibility; }
            set
            {
                if (_listAlternateVisibility != value)
                {
                    _listAlternateVisibility = value;
                    OnPropertyChanged("ListAlternateVisibility");
                }
            }
        }

        private string _casPrimaryName { get; set; }
        public string CasPrimaryName
        {
            get { return _casPrimaryName; }
            set
            {
                _casPrimaryName = value;
                OnPropertyChanged("CasPrimaryName");
            }
        }
        private bool ValidateSubstanceTypeInFlat(string cas)
        {
            CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service = CRA_DataAccess.LibraryFunction.GetInstance;
            return _objLibraryFunction_Service.ValidateSubstanceTypeInFlatAndHydrate(cas);
        }

    }
}
