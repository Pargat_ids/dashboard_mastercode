﻿
using CRA_CasAssignment.Resource.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using System.Diagnostics;
using GalaSoft.MvvmLight.Messaging;
using I4FTab.ViewModel;
using CommonGenericUIView.ViewModel;
using FastMember;
using System.ComponentModel;
using VersionControlSystem.Model.ViewModel;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Reflection;

namespace CRA_CasAssignment.ViewModel
{
    public class CasAssignment_ViewModel : Base_ViewModel  
    {
        public ICommand commandUploadMdbFile { get; set; }

        public ICommand CommandUnloadMdbFile { get; set; }
        public ICommand commandProceedMdbFile { get; set; }
        public ICommand commandUpdateMdbFile { get; set; }
        public ICommand commandCancelCasAssign { get; set; }
        public ICommand commandCasDetailQsid { get; set; }
        public ICommand commandAssignSeqIdentifier { get; set; }

        public ICommand commandAssignSeqSingleMultiCas { get; set; }
        public ICommand commandGoogleSearchChemName { get; set; }

        public ICommand commandAssignINGSeed { get; set; }
        public ICommand commandAssignMICSeed { get; set; }
        public ICommand commandRemoveMIC_ING_Seed { get; set; }
        public ICommand CommandNavigateTabBySummary { get; set; }
        public ICommand CommandRemoveCasName { get; set; }
        public ICommand CommandOtherCasDetail { get; set; }
        public ICommand CommandOtherQsidDetail { get; set; }
        public ICommand CommandOtherChemNameDetail { get; set; }
        public ICommand CommandIsInCorrectMultipleCas { get; set; }

        public ICommand CommandActiveListsDetail { get; set; }
        public ICommand CommandChildrenCasDetail { get; set; }
        public ICommand CommandAlternateCasDetail { get; set; }
        public ICommand CommandCopyCellDataToClipboard { get; set; }
        public ICommand CommandCancelAnalysis { get; set; }

        public ICommand CommandGenerateAccessCasInitialAssesment { get; set; }
        public ICommand CommandGenerateExcelCasInitialAssement { get; set; }

        public ICommand CommandGenerateAccessSameNameOneCas { get; set; }
        public ICommand CommandGenerateExcelSameNameOneCas { get; set; }

        public ICommand CommandGenerateAccessSameNameMultipleCas { get; set; }
        public ICommand CommandGenerateExcelSameNameMultipleCas { get; set; }

        public ICommand CommandGenerateAccessNoCas { get; set; }
        public ICommand CommandGenerateExcelNoCas { get; set; }

        public ICommand CommandGenerateAccessExistingCas { get; set; }
        public ICommand CommandGenerateExcelExistingCas { get; set; }

        public ICommand CommandGenerateAccessPastHistory { get; set; }
        public ICommand CommandGenerateExcelPastHistory { get; set; }

        public string SelectedMdbQSID { get; set; }
        public string SelectedMdbPath { get; set; }
        public string SelectedMdbListFieldName { get; set; }
        public string SelectedMdbModule { get; set; }
        public string SelectedDefaultAccessTable { get; set; }
        public int listRowCount { get; set; }

        public string defaultTitleContent { get; set; }
        public ObservableCollection<CasAssignment_ViewModel> defaultListColumnGrid { get; set; }
        private DataGridCellInfo _currentCellData { get; set; }

        public DataGridCellInfo CurrentCellData
        {
            get { return _currentCellData; }
            set
            {
                if (value != _currentCellData)
                {
                    _currentCellData = value;
                }
            }
        }
        public Visibility SelectMdbVisible { get; set; }
        public Visibility AnalysisMdbVisible { get; set; } = Visibility.Collapsed;
        public Visibility TableListVisible { get; set; }
        public Visibility IsLoaderVisible { get; set; }
        public Visibility AssignSeqBtnVisibility { get; set; }
        public Visibility AssignCasCheckVisibility { get; set; }

        private bool _isFoodData { get; set; }
        public bool IsFoodData
        {
            get { return _isFoodData; }
            set
            {
                if (value != _isFoodData)
                {
                    _isFoodData = value;
                    if (_isFoodData)
                    {
                        IsNonFoodData = false;
                        //FetchChemicalName();
                    }
                    else if (!IsNonFoodData)
                    {
                        //  FetchChemicalName();
                    }
                }
                NotifyPropertyChanged("IsFoodData");
            }
        }
        private bool _isNonFoodData { get; set; }
        public bool IsNonFoodData
        {
            get
            {
                return _isNonFoodData;

            }
            set
            {
                if (value != _isNonFoodData)
                {
                    _isNonFoodData = value;
                    if (_isNonFoodData)
                    {
                        IsFoodData = false;
                        //   FetchChemicalName();
                    }
                    else if (!IsFoodData)
                    {
                        // FetchChemicalName();
                    }
                }
                NotifyPropertyChanged("IsNonFoodData");
            }
        }


        public bool _isAssignCasHighestQsidCount { get; set; }
        public bool IsAssignCasHighestQsidCount
        {
            get { return _isAssignCasHighestQsidCount; }
            set
            {
                if (value != _isAssignCasHighestQsidCount)
                {
                    _isAssignCasHighestQsidCount = value;
                    UpdateCasByHighestQsidCount();
                }
            }
        }



        public List<VersionControlSystem.Model.ViewModel.SubstanceType_VM> listSubstanceType = new List<VersionControlSystem.Model.ViewModel.SubstanceType_VM>();

        public List<VersionControlSystem.Model.ViewModel.SubstanceCategories_VM> listSubstanceCategories = new List<VersionControlSystem.Model.ViewModel.SubstanceCategories_VM>();



        public ObservableCollection<string> listQsidInMultiCas { get; set; }

        public VersionControlSystem.Model.ViewModel.SelectListItem _selectedAccessTable { get; set; } = new VersionControlSystem.Model.ViewModel.SelectListItem();
        public VersionControlSystem.Model.ViewModel.SelectListItem SelectedAccessTable
        {
            get { return _selectedAccessTable; }
            set
            {
                if (value != _selectedAccessTable)
                {
                    _selectedAccessTable = value;
                    if (_selectedAccessTable != null)
                    {
                        if (ValidateDefaultAccessTable(_selectedAccessTable.Text))
                        {
                            if (!string.IsNullOrEmpty(SelectedDefaultAccessTable))
                            {
                                SelectedDefaultAccessTable = _selectedAccessTable.Text;
                                //FetchChemicalName();
                            }
                            else
                            {
                                SelectedDefaultAccessTable = _selectedAccessTable.Text;
                            }
                        }
                    }
                    NotifyPropertyChanged("SelectedAccessTable");
                }
            }
        }
        public ObservableCollection<VersionControlSystem.Model.ViewModel.SelectListItem> listAccessTable { get; set; }
        public ObservableCollection<CasAssignmentHistory> ListHistoryCasAssignment { get; set; }

        private List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> _listMap_Incorrect_Cas { get; set; } = new List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>();
        public List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> listMap_Incorrect_Cas
        {
            get
            {
                return _listMap_Incorrect_Cas;
            }
            set
            {
                _listMap_Incorrect_Cas = value;
                MessengerInstance.Send<PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>>>(new PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>>(null, _listMap_Incorrect_Cas, "Default List"));

            }
        }
        private List<Log_Map_Incorrect_Cas_ChemicalName_VM> _listHistory_Incorrect_Cas { get; set; } = new();
        public List<Log_Map_Incorrect_Cas_ChemicalName_VM> listHistory_Incorrect_Cas
        {
            get
            {
                return _listHistory_Incorrect_Cas;
            }
            set
            {
                _listHistory_Incorrect_Cas = value;
                MessengerInstance.Send<PropertyChangedMessage<List<Log_Map_Incorrect_Cas_ChemicalName_VM>>>(new PropertyChangedMessage<List<Log_Map_Incorrect_Cas_ChemicalName_VM>>(null, _listHistory_Incorrect_Cas, "Default List"));
            }
        }
        public VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM SelectedMapIncorrectCas { get; set; }
        public ObservableCollection<CasAssignChemicalNamesVM> listChemNameExistingCas { get; set; } = new ObservableCollection<CasAssignChemicalNamesVM>();
        public ObservableCollection<VersionControlSystem.Model.ViewModel.CasAssignmentSummary> listCasAssignmentSummary { get; set; }
        public ObservableCollection<CasAssignChemicalNamesVM> listChemNameNoCas { get; set; }
        public ObservableCollection<CasAssignChemicalNamesVM> listChemNameSingleCasAssign { get; set; }
        public ObservableCollection<CasAssignChemicalNamesVM> listChemNameMultiCasAssign { get; set; } = new ObservableCollection<CasAssignChemicalNamesVM>();
        public List<CasAssignChemicalNamesVM> listChemNameCasAssign { get; set; } = new List<CasAssignChemicalNamesVM>();
        private CasAssignChemicalNamesVM _selectedChemNameMultiCasAssign { get; set; }
        private CasAssignChemicalNamesVM _selectedChemNameOneCasAssign { get; set; }
        public CasAssignChemicalNamesVM SelectedChemNameMultiCasAssign
        {
            get
            {
                return _selectedChemNameMultiCasAssign;
            }
            set
            {
                _selectedChemNameMultiCasAssign = value;
                if (_selectedChemNameMultiCasAssign != null)
                {
                    objCommentDC_SameNameMultipleCas = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM(_selectedChemNameMultiCasAssign.CasAssigned, "", 0, 0, 550, false, "CAS assignment"));
                    NotifyPropertyChanged("objCommentDC_SameNameMultipleCas");
                }
                NotifyPropertyChanged("SelectedChemNameMultiCasAssign");
            }
        }
        public CasAssignChemicalNamesVM SelectedChemNameOneCasAssign
        {
            get
            {
                return _selectedChemNameOneCasAssign;
            }
            set
            {
                _selectedChemNameOneCasAssign = value;
                if (_selectedChemNameOneCasAssign != null)
                {
                    objCommentDC_SameNameOneCas = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM( _selectedChemNameOneCasAssign.CasAssigned, "", 0, 0, 550, false, "CAS assignment"));
                    NotifyPropertyChanged("objCommentDC_SameNameOneCas");
                }
                NotifyPropertyChanged("SelectedChemNameOneCasAssign");
            }
        }
        public CasAssignChemicalNamesVM SelectedChemNameNoCas { get; set; }
        private CasAssignChemicalNamesVM _selectedChemNameExistingCas { get; set; }
        public CasAssignChemicalNamesVM SelectedChemNameExistingCas
        {
            get { return _selectedChemNameExistingCas; }
            set
            {
                _selectedChemNameExistingCas = value;
                if (_selectedChemNameExistingCas != null)
                {
                    objCommentDC_SameNameExistingCas = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM(_selectedChemNameExistingCas.CasAssigned, "", 0, 0, 550, false, "CAS assignment"));
                    NotifyPropertyChanged("objCommentDC_SameNameExistingCas");
                }
                NotifyPropertyChanged("SelectedChemNameExistingCas");
            }
        }
        public CasAssignmentHistory _selectedChemNamePastHistory { get; set; }
        public CasAssignmentHistory SelectedChemNamePastHistory
        {
            get { return _selectedChemNamePastHistory; }
            set
            {
                _selectedChemNamePastHistory = value;
                if (_selectedChemNamePastHistory != null)
                {
                    objCommentDC_SameNamePastHistory = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM(_selectedChemNamePastHistory.CasAssigned, "", 0, 0, 550, false, "CAS assignment"));
                    NotifyPropertyChanged("objCommentDC_SameNamePastHistory");
                }
                NotifyPropertyChanged("SelectedChemNamePastHistory");
            }
        }
        public VersionControlSystem.Model.ViewModel.CasAssignmentSummary _selectedCasAssignmentSummary { get; set; }
        public VersionControlSystem.Model.ViewModel.CasAssignmentSummary SelectedCasAssignmentSummary
        {
            get { return _selectedCasAssignmentSummary; }
            set
            {
                if (value != _selectedCasAssignmentSummary)
                {
                    _selectedCasAssignmentSummary = value;
                }
            }
        }

        public ObservableCollection<string> ListOtherQsid { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> ListOtherChemicalName { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> ListOtherCas { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> ListChildren { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> ListAlternateCas { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> ActiveListsQsidDetail { get; set; } = new ObservableCollection<string>();


        public Dictionary<string, int> dicNextSeqIdentifier { get; set; }
        public int selectedIndexTab { get; set; }
        private TabItem _selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return _selectedTabItem;
            }
            set
            {
                if (_selectedTabItem != value)
                {
                    _selectedTabItem = value;
                    if (_selectedTabItem != null)
                    {
                        ExecuteFunctionalityOnTabChanged();
                    }
                }
            }
        }

        public TabItem _selectedTabHeaderItem { get; set; }
        public TabItem SelectedTabHeaderItem
        {
            get
            {
                return _selectedTabHeaderItem;
            }
            set
            {
                if (_selectedTabHeaderItem != value)
                {
                    _selectedTabHeaderItem = value;
                    if (_selectedTabHeaderItem != null)
                    {
                        ExecuteFunctionalityOnTabHeaderChanged();
                    }
                }
            }
        }

        public bool IsSelectedInitialAssessment { get; set; }

        private void ExecuteFunctionalityOnTabHeaderChanged()
        {

            switch (SelectedTabHeaderItem.Header)
            {
                case "Incorrect Combination Cas+Name":
                    GetListMapIncorrectChemicalName();
                    GetLogMapIncorrectChemicalName();
                    break;

            }

        }

        public DataGridFilterLibrary.Querying.QueryController CustomQueryController
        {
            get; set;
        } = new DataGridFilterLibrary.Querying.QueryController();
        public DataGridFilterLibrary.Querying.QueryController CustomQueryControllerMultipleCas
        {
            get; set;
        } = new DataGridFilterLibrary.Querying.QueryController();
        public DataGridFilterLibrary.Querying.QueryController CustomQueryControllerNoCas
        {
            get; set;
        } = new DataGridFilterLibrary.Querying.QueryController();
        public DataGridFilterLibrary.Querying.QueryController CustomQueryControllerExistingCas
        {
            get; set;
        } = new DataGridFilterLibrary.Querying.QueryController();
        public DataGridFilterLibrary.Querying.QueryController CustomQueryControllerHistory
        {
            get; set;
        } = new DataGridFilterLibrary.Querying.QueryController();
        public CancellationTokenSource tokenSource { get; set; } = new CancellationTokenSource();

        public Visibility cancelAnalysisVisibility { get; set; } = Visibility.Collapsed;

        #region MapInCorrectCasName Popup
        public bool IsShowPopUp { get; set; } = false;
        public string IncorrectCombination_Cas { get; set; }
        public string IncorrectCombination_ChemicalName { get; set; }
        public string IncorrectCombination_Comments { get; set; }

        public ICommand CommandShowPopUpMapInCorrect { get; set; }
        public ICommand CommandClosePopUpInCorrectCasName { get; set; }

        public ICommand CommandAddInCorrectCasName { get; set; }
        public ICommand CommandAddMoreInCorrectCasName { get; set; }

        public CommonDataGrid_ViewModel<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> objCommonGrid { get; set; }
        public VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM SelectedMapIncorrectCasChemicalName
        {
            get
            {
                return _selectedMapIncorrectCasChemicalName;
            }
            set
            {
                if (value != null)
                {
                    _selectedMapIncorrectCasChemicalName = value;
                    if (_selectedMapIncorrectCasChemicalName != null)
                    {
                        headerCommentMapSection = _selectedMapIncorrectCasChemicalName.Cas + " ( " + _selectedMapIncorrectCasChemicalName.ChemicalName + " )";
                        NotifyPropertyChanged("headerCommentMapSection");
                        BindListComment((int)_selectedMapIncorrectCasChemicalName.ChemicalNameId, (int)_selectedMapIncorrectCasChemicalName.CasID, "MapInCorrect");
                    }
                    NotifyPropertyChanged("SelectedMapIncorrectCasChemicalName");
                }
            }
        }
        private VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM _selectedMapIncorrectCasChemicalName { get; set; }

        public CommonDataGrid_ViewModel<Log_Map_Incorrect_Cas_ChemicalName_VM> objHistoryCommonGrid { get; set; }

        private Log_Map_Incorrect_Cas_ChemicalName_VM _selectedItemHistoryIncorrect { get; set; }
        public Log_Map_Incorrect_Cas_ChemicalName_VM selectedItemHistoryIncorrect
        {
            get { return _selectedItemHistoryIncorrect; }
            set
            {
                _selectedItemHistoryIncorrect = value;
                if (_selectedItemHistoryIncorrect != null)
                {
                    headerCommentHistorySection = _selectedItemHistoryIncorrect.Cas + " ( " + _selectedItemHistoryIncorrect.ChemicalName + " )";
                    NotifyPropertyChanged("headerCommentHistorySection");
                    BindListComment(_selectedItemHistoryIncorrect.ChemicalNameID, _selectedItemHistoryIncorrect.CasID, "History");
                }
                NotifyPropertyChanged("selectedItemHistoryIncorrect");
            }
        }

        public bool ActiveListsQsidVisibility { get; set; }

        public Visibility IsIncorrectCasChemicalNameVisibility { get; set; } = Visibility.Collapsed;
        public Visibility MarkValidCorrectVisibility { get; set; } = Visibility.Collapsed;

        public ICommand CommandMarkCorrectCasName { get; set; }

        public Visibility visibiltiyMapIncorrectCasName { get; set; } = Visibility.Collapsed;

        public ObservableCollection<VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM> listIncorrectCasName_Comments { get; set; } = new();
        public ObservableCollection<VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM> listHistoryIncorrectCasName_Comments { get; set; } = new();

        public string IncorrectCombination_AdditionalComments { get; set; }
        public ICommand CommandAddAdditionalComment { get; set; }
        public Visibility LoadingHistoryCasNameVisibility { get; set; } = Visibility.Collapsed;

        public string headerCommentMapSection { get; set; }
        public string headerCommentHistorySection { get; set; }

        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }

        public ICommand commandOpenCurrentDataViewNewAddList { get; set; }
        public ICommand commandGoToSubstanceIdentity { get; set; }
        public ICommand commandAddEditPrimaryName { get; set; }
        public string CasAssign { get; set; }
        private string _parentName { get; set; }
        public string ParentName
        {
            get { return _parentName; }
            set
            {
                _parentName = value;
                FilterChemicalName(_parentName);
            }
        }
        public string PrimaryNameType { get; set; }
        public int? ParentNameID { get; set; }
        public bool IsParentNameShowPopUp { get; set; }
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        private GenericChemicalName_VM _selected_Chemical_Name { get; set; }
        public GenericChemicalName_VM Selected_Chemical_Name
        {
            get { return _selected_Chemical_Name; }
            set
            {
                if (_selected_Chemical_Name != value)
                {
                    _selected_Chemical_Name = value;
                    if (_selected_Chemical_Name != null)
                    {
                        ParentName = _selected_Chemical_Name.ChemicalName;
                        ParentNameID = _selected_Chemical_Name.ChemicalNameID;
                        NotifyPropertyChanged("ParentName");
                    }
                    NotifyPropertyChanged("Selected_Chemical_Name");
                }
            }
        }
        public GenericParentChildCasName_VM objGenericParentChildCasName { get; set; }
        public ICommand commandClosePopUp { get; set; }
        public ICommand commandSaveAddEditCasName { get; set; }
        public CommentsUC_VM objCommentDC_SameNameOneCas { get; set; } = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM("", "", 0, 0, 550, false, "CAS assignment"));
        public CommentsUC_VM objCommentDC_SameNameMultipleCas { get; set; } = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM("", "", 0, 0, 550, false, "CAS assignment"));
        public CommentsUC_VM objCommentDC_SameNameExistingCas { get; set; } = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM("", "", 0, 0, 550, false,"CAS assignment"));
        public CommentsUC_VM objCommentDC_SameNamePastHistory { get; set; } = new CommentsUC_VM(new VersionControlSystem.Model.ViewModel.GenericComment_VM("", "", 0, 0, 550, false,"CAS assignment"));
        #endregion
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        public CasAssignment_ViewModel()
        {
            objCommonGrid = new CommonDataGrid_ViewModel<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>(GetListColumnInCorrectCasChemicalName(), "Cas", "");
            objHistoryCommonGrid = new CommonDataGrid_ViewModel<Log_Map_Incorrect_Cas_ChemicalName_VM>(GetListColumnHistoryCasChemicalName(), "Timestamps", "");
            IsSelectedInitialAssessment = true;
            NotifyPropertyChanged("IsSelectedInitialAssessment");
            if (Engine.IsOpenCasAssignmentFromAccess)
            {
                TableListVisible = Visibility.Collapsed;
                SelectMdbVisible = Visibility.Collapsed;
                IsLoaderVisible = Visibility.Collapsed;
                SelectedMdbPath = Engine.CasAssignmentAccessDBFilePath;
                NotifyPropertyChanged("SelectedMdbPath");
                FetchChemicalName();

            }
            else
            {
                TableListVisible = Visibility.Collapsed;
                SelectMdbVisible = Visibility.Collapsed;
                IsLoaderVisible = Visibility.Collapsed;
            }
            GetListMapIncorrectChemicalName();
            GetLogMapIncorrectChemicalName();
            listSubstanceType = Common.CommonGeneralFunction.ConvertSubstanceTypeToViewModel(_objCasAssignment_Service.GetLibrarySubstanceType());
            listSubstanceCategories = Common.CommonGeneralFunction.ConvertSubstanceCategoryToViewModel(_objCasAssignment_Service.GetLibrarySubstanceCategory());
            commandGoogleSearchChemName = new RelayCommand(commandGoogleSearchChemNameExecute, defaultCanExecute);
            commandUploadMdbFile = new RelayCommand(commandUploadMdbFileExecute, commandUploadMdbFileCanExecute);
            commandProceedMdbFile = new RelayCommand(commandProceedMdbFileExecute, commandProceedMdbFileCanExecute);
            commandUpdateMdbFile = new RelayCommand(commandUpdateMdbFileExecute, commandUpdateMdbFileCanExecute);
            commandCancelCasAssign = new RelayCommand(commandCancelCasAssignExecute, commandCancelCasAssignCanExecute);
            commandCasDetailQsid = new RelayCommand(commandCasDetailQsidExecute, defaultCanExecute);
            commandAssignSeqIdentifier = new RelayCommand(commandAssignSeqIdentifierExecute, defaultCanExecute);
            commandAssignSeqSingleMultiCas = new RelayCommand(commandAssignSeqSingleMultiCasExecute, defaultCanExecute);
            commandAssignINGSeed = new RelayCommand(commandAssignINGSeedExecute, defaultCanExecute);
            commandAssignMICSeed = new RelayCommand(commandAssignMICSeedExecute, defaultCanExecute);
            commandRemoveMIC_ING_Seed = new RelayCommand(commandRemoveMIC_ING_SeedExecute, defaultCanExecute);
            CommandNavigateTabBySummary = new RelayCommand(commandNavigateTabBySummaryExecute, defaultCanExecute);
            CommandRemoveCasName = new RelayCommand(commandRemoveCasNameExecute, defaultCanExecute);
            CommandOtherCasDetail = new RelayCommand(CommandOtherCasDetailExecute, defaultCanExecute);
            CommandOtherChemNameDetail = new RelayCommand(CommandOtherChemNameDetailExecute, defaultCanExecute);
            CommandOtherQsidDetail = new RelayCommand(CommandOtherQsidDetailExecute, defaultCanExecute);
            CommandIsInCorrectMultipleCas = new RelayCommand(CommandIsInCorrectMultipleCasExecute, defaultCanExecute);
            CommandActiveListsDetail = new RelayCommand(CommandActiveListsDetailExecute, defaultCanExecute);
            CommandChildrenCasDetail = new RelayCommand(CommandChildrenCasDetailExecute, defaultCanExecute);
            CommandAlternateCasDetail = new RelayCommand(CommandAlternateCasDetailExecute, defaultCanExecute);
            CommandCopyCellDataToClipboard = new RelayCommand(CommandCopyCellDataToClipboardExecute, CommandCopyCellDataToClipboardCanExecute);
            CommandCancelAnalysis = new RelayCommand(CommandCancelAnalysisExecute, CommandCancelAnalysisCanExecute);
            CommandUnloadMdbFile = new RelayCommand(CommandUnloadMdbFileExecute, defaultCanExecute);
            CommandShowPopUpMapInCorrect = new RelayCommand(CommandShowPopUpMapInCorrectExecute);
            CommandAddInCorrectCasName = new RelayCommand(CommandAddInCorrectCasNameExecute, CommandAddInCorrectCasNameCanExecute);
            CommandAddMoreInCorrectCasName = new RelayCommand(CommandAddMoreInCorrectCasNameExecute, CommandAddInCorrectCasNameCanExecute);
            CommandClosePopUpInCorrectCasName = new RelayCommand(CommandClosePopUpInCorrectCasNameExecute);
            CommandMarkCorrectCasName = new RelayCommand(CommandMarkCorrectCasNameExecute, CommandMarkCorrectCasNameCanExecute);
            CommandAddAdditionalComment = new RelayCommand(CommandAddAdditionalCommentExecute, CommandAddAdditionalCommentCanExecute);
            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExecute);
            commandGoToSubstanceIdentity = new RelayCommand(commandGoToSubstanceIdentityExecute);
            commandAddEditPrimaryName = new RelayCommand(commandAddEditPrimaryNameExecuted);
            commandSaveAddEditCasName = new RelayCommand(CommandSaveAddEditCasNameExecute, CommandSaveAddEditCasNameCanExecute);
            commandClosePopUp = new RelayCommand(CommandClosePopUPExecute);
            CommandGenerateExcelCasInitialAssement = new RelayCommand(CommandGenerateExcelCasInitialAssementExecute);
            CommandGenerateAccessCasInitialAssesment = new RelayCommand(CommandGenerateAccessCasInitialAssesmentExecute);
            CommandGenerateAccessSameNameOneCas = new RelayCommand(CommandGenerateAccessSameNameOneCasExecute);
            CommandGenerateExcelSameNameOneCas = new RelayCommand(CommandGenerateExcelSameNameOneCasExecute);
            CommandGenerateAccessSameNameMultipleCas = new RelayCommand(CommandGenerateAccessSameNameMultipleCasExecute);
            CommandGenerateExcelSameNameMultipleCas = new RelayCommand(CommandGenerateExcelSameNameMultipleCasExecute);
            CommandGenerateAccessNoCas = new RelayCommand(CommandGenerateAccessNoCasExecute);
            CommandGenerateExcelNoCas = new RelayCommand(CommandGenerateExcelNoCasExecute);
            CommandGenerateAccessExistingCas = new RelayCommand(CommandGenerateAccessExistingCasExecute);
            CommandGenerateExcelExistingCas = new RelayCommand(CommandGenerateExcelExistingCasExecute);
            CommandGenerateAccessPastHistory = new RelayCommand(CommandGenerateAccessPastHistoryExecute);
            CommandGenerateExcelPastHistory = new RelayCommand(CommandGenerateExcelPastHistoryExecute);
            MessengerInstance.Register<PropertyChangedMessage<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<Log_Map_Incorrect_Cas_ChemicalName_VM>>(this, NotifyMe);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM), CallBackMapIncorrectCasChemicalNameExecute);
            MessengerInstance.Register<NotificationMessageAction<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM>>(this, CallBackRefreshGridExecute);
            MessengerInstance.Register<NotificationMessageAction<Log_Map_Incorrect_Cas_ChemicalName_VM>>(this, CallBackRefreshGridExecute);
        }

     

        private void CommandClosePopUPExecute(object obj)
        {
            IsParentNameShowPopUp = false;
            NotifyPropertyChanged("IsParentNameShowPopUp");
        }
        private void commandAddEditPrimaryNameExecuted(object obj)
        {
            PrimaryNameType = (string)obj;
            if (PrimaryNameType == "NoCas")
            {
                CasAssign = SelectedChemNameNoCas.OverWriteCasAssigned;
                ParentName = SelectedChemNameNoCas.CasPrimaryName;
            }
            else if (PrimaryNameType == "SameNameOneCas")
            {
                CasAssign = SelectedChemNameOneCasAssign.CasAssigned;
                ParentName = SelectedChemNameOneCasAssign.CasPrimaryName;
            }
            else if (PrimaryNameType == "SameNameMultipleCas")
            {
                CasAssign = SelectedChemNameMultiCasAssign.CasAssigned;
                ParentName = SelectedChemNameMultiCasAssign.CasPrimaryName;
            }
            IsParentNameShowPopUp = true;
            NotifyPropertyChanged("CasAssign");
            NotifyPropertyChanged("ParentName");
            NotifyPropertyChanged("IsParentNameShowPopUp");
        }
        private bool CommandSaveAddEditCasNameCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ParentName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandSaveAddEditCasNameExecute(object obj)
        {
            _notifier.ShowInformation("Add Primary Name InProgress...");
            Task.Run(() =>
            {
                var casID = _objLibraryFunction_Service.CheckCasIDIfNotThenAdd(CasAssign);
                objGenericParentChildCasName = new();
                objGenericParentChildCasName.CasID = casID;
                objGenericParentChildCasName.ParentName = ParentName;
                objGenericParentChildCasName.ParentNameID = ParentNameID;
                objGenericParentChildCasName.SessionID = Engine.CurrentUserSessionID;
                objGenericParentChildCasName.CasType = "Parent";
                _objLibraryFunction_Service.SaveParentChildCasName(objGenericParentChildCasName);
                _notifier.ShowSuccess("Add Primary Name Successfully");
                if (PrimaryNameType == "NoCas")
                {
                    SelectedChemNameNoCas.CasPrimaryName = ParentName;
                }
                else if (PrimaryNameType == "SameNameOneCas")
                {
                    SelectedChemNameOneCasAssign.CasPrimaryName = ParentName;
                }
                else if (PrimaryNameType == "SameNameMultipleCas")
                {
                    SelectedChemNameMultiCasAssign.CasPrimaryName = ParentName;
                }

            });
            IsParentNameShowPopUp = false;
            NotifyPropertyChanged("IsParentNameShowPopUp");

        }
        private void FilterChemicalName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                Task.Run(() =>
                {
                    listAllChemicalName = new ObservableCollection<GenericChemicalName_VM>(_objLibraryFunction_Service.FilterChemicalNameForGeneric(name.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName");

                });
            }
            else
            {
                listAllChemicalName = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllChemicalName");
            }


        }

        private void commandGoToSubstanceIdentityExecute(object obj)
        {
            string cas = (string)obj;
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", cas, "ActiveSubstanceIdentityWithCas"), typeof(GroupThresholdUC_VM));
            //MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", cas, "SearchCasInSubstanceIdentityTab"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
        }

        private void CommandUnloadMdbFileExecute(object obj)
        {
            ClearVariableSelectedMbFile();
            ClearVariable();
        }

        private bool CommandCancelAnalysisCanExecute(object obj)
        {
            return true;
        }

        private void CommandCancelAnalysisExecute(object obj)
        {
            try
            {
                tokenSource.Cancel();
            }
            catch (AggregateException ex)
            {

            }

        }

        private bool CommandCopyCellDataToClipboardCanExecute(object obj)
        {
            return true;
        }

        private void CommandCopyCellDataToClipboardExecute(object obj)
        {
            if (_currentCellData.Column != null)
            {
                if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                {
                    var text = string.Empty;
                    text = ((System.Windows.Controls.TextBlock)_currentCellData.Column.GetCellContent(_currentCellData.Item)).Text.ToString();
                    Clipboard.SetDataObject(text);
                }
            }
        }

        private void CommandAlternateCasDetailExecute(object obj)
        {
            if (!SelectedChemNameExistingCas.ListAlternateVisibility)
            {
                ListAlternateCas = new ObservableCollection<string>(_objCasAssignment_Service.GetCasAlternateList(SelectedChemNameExistingCas.CasAssigned));
                SelectedChemNameExistingCas.ListAlternateVisibility = true;
                NotifyPropertyChanged("ListAlternateCas");
            }
            else
            {
                SelectedChemNameExistingCas.ListAlternateVisibility = false;
            }
        }

        private void CommandChildrenCasDetailExecute(object obj)
        {
            if (!SelectedChemNameExistingCas.ListChildrenVisibility)
            {
                ListChildren = new ObservableCollection<string>(_objCasAssignment_Service.GetCasChildrenList(SelectedChemNameExistingCas.CasAssigned, IsFoodData, IsNonFoodData));
                SelectedChemNameExistingCas.ListChildrenVisibility = true;
                NotifyPropertyChanged("ListChildren");
            }
            else
            {
                SelectedChemNameExistingCas.ListChildrenVisibility = false;
            }
        }

        private void CommandActiveListsDetailExecute(object obj)
        {
            if (!SelectedMapIncorrectCas.ActiveListsQsidVisibility)
            {
                ActiveListsQsidDetail = new ObservableCollection<string>(_objCasAssignment_Service.GetActiveListQsid(SelectedMapIncorrectCas.ChemicalNameId, SelectedMapIncorrectCas.CasID));
                SelectedMapIncorrectCas.ActiveListsQsidVisibility = true;
                NotifyPropertyChanged("ActiveListsQsidDetail");
            }
            else
            {
                SelectedMapIncorrectCas.ActiveListsQsidVisibility = false;
            }
        }

        private void CommandIsInCorrectMultipleCasExecute(object values)
        {
            object[] arryValues = (object[])values;
            string chemicalName = string.Empty;
            switch (SelectedTabItem.Header)
            {
                case "Same Name One Cas":
                    chemicalName = SelectedChemNameOneCasAssign.ChemicalName;
                    break;
                case "Same Name Multiple Cas":
                    chemicalName = SelectedChemNameMultiCasAssign.ChemicalName;
                    break;
                case "Existing Cas":
                    chemicalName = SelectedChemNameExistingCas.ChemicalName;
                    break;
            }
            bool reultFlag = InsertUpdateIsIncorrect(chemicalName, arryValues[0].ToString(), Convert.ToBoolean(arryValues[1]));
            if (!reultFlag)
            {
                _notifier.ShowError("IsIncorrect Mark/Unmark not successfull due to internal server error");
            }
        }
        private bool InsertUpdateIsIncorrect(string ChemicalName, string Cas, bool IsInCorrect)
        {
            bool reultFlag = false;
            if (IsInCorrect)
            {
                reultFlag = _objCasAssignment_Service.InsertUpdateIsIncorrectCasName(ChemicalName, Engine.DelimiterForQsidCasAssignmentTool, Cas, 0, Engine.CurrentUserSessionID);
            }
            else if (IsInCorrect == false)
            {
                reultFlag = _objCasAssignment_Service.DeleteMap_Incorrect_Cas_ChemicalName(ChemicalName, Engine.DelimiterForQsidCasAssignmentTool, Cas, Engine.CurrentUserSessionID);
            }
            return reultFlag;
        }

        private void CommandOtherCasDetailExecute(object obj)
        {
            if (!SelectedChemNameExistingCas.ListOtherCasVisibility)
            {
                ListOtherCas = new ObservableCollection<string>(_objCasAssignment_Service.FetchOtherCasByChemicalName(SelectedChemNameExistingCas.ChemicalName, SelectedChemNameExistingCas.CasAssigned));
                SelectedChemNameExistingCas.ListOtherCasVisibility = true;
                NotifyPropertyChanged("ListOtherCas");
            }
            else
            {
                SelectedChemNameExistingCas.ListOtherCasVisibility = false;
            }
        }

        private void CommandOtherChemNameDetailExecute(object obj)
        {
            if (!SelectedChemNameExistingCas.ListOtherChemicalNameVisibility)
            {
                ListOtherChemicalName = new ObservableCollection<string>(_objCasAssignment_Service.FetchOtherChemicalNameByCas(SelectedChemNameExistingCas.ChemicalName, SelectedChemNameExistingCas.CasAssigned));
                SelectedChemNameExistingCas.ListOtherChemicalNameVisibility = true;
                NotifyPropertyChanged("ListOtherChemicalName");
            }
            else
            {
                SelectedChemNameExistingCas.ListOtherChemicalNameVisibility = false;
            }
        }

        private void CommandOtherQsidDetailExecute(object obj)
        {
            if (!SelectedChemNameExistingCas.ListOtherQsidVisibility)
            {
                ListOtherQsid = new ObservableCollection<string>(_objCasAssignment_Service.FetchOtherQsidByChemicalNameAndCas(SelectedChemNameExistingCas.ChemicalName, SelectedChemNameExistingCas.CasAssigned));
                SelectedChemNameExistingCas.ListOtherQsidVisibility = true;
                NotifyPropertyChanged("ListOtherQsid");
            }
            else
            {
                SelectedChemNameExistingCas.ListOtherQsidVisibility = false;
            }
        }

        private void commandRemoveCasNameExecute(object obj)
        {
            int mapCasNameId = (int)obj;
            //bool IsDelete = _objCasAssignment_Service.DeleteMap_Incorrect_Cas_ChemicalName(mapCasNameId);
            //if (IsDelete)
            //{
            //    _notifier.ShowSuccess("Map incorrect Cas+Name is successfully deleted.");
            //}
            //else
            //{
            //    _notifier.ShowError("Delete Map incorrect Cas+Name failed.");
            //}
            GetListMapIncorrectChemicalName();
            GetLogMapIncorrectChemicalName();

        }

        public void GetListMapIncorrectChemicalName()
        {
            visibiltiyMapIncorrectCasName = Visibility.Visible;
            NotifyPropertyChanged("visibiltiyMapIncorrectCasName");
            Task.Run(() =>
            {
                var resultlist = _objCasAssignment_Service.GetMap_Incorrect_Cas_ChemicalName();

                listMap_Incorrect_Cas = resultlist.GroupBy(x => new { x.ChemicalName, x.ChemicalNameId, x.Cas, x.CasID, x.LanguageName }).Select(x => new VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM
                {
                    ChemicalName = x.Key.ChemicalName,
                    LanguageName = x.Key.LanguageName,
                    ChemicalNameId = x.Key.ChemicalNameId,
                    CasID = x.Key.CasID,
                    ActiveListsCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Count(),
                    Cas = x.Key.Cas,
                    ListQsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Select(y => new VersionControlSystem.Model.ViewModel.HyperLinkDataValue() { DisplayValue = y.Qsid + " ( " + y.ListFieldName + " )", NavigateData = y.Qsid_Id }).Distinct().ToList(),
                    Qsid = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => y.Qsid).ToList())
                }).ToList();
                NotifyPropertyChanged("listMap_Incorrect_Cas");
                visibiltiyMapIncorrectCasName = Visibility.Collapsed;
                NotifyPropertyChanged("visibiltiyMapIncorrectCasName");
            });
        }

        public void GetLogMapIncorrectChemicalName()
        {
            LoadingHistoryCasNameVisibility = Visibility.Visible;
            NotifyPropertyChanged("LoadingHistoryCasNameVisibility");
            Task.Run(() =>
            {
                listHistory_Incorrect_Cas = _objCasAssignment_Service.GetLog_Map_Incorrect_Cas_ChemicalName();
                LoadingHistoryCasNameVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("LoadingHistoryCasNameVisibility");
            });
        }

        private void commandNavigateTabBySummaryExecute(object obj)
        {
            ActiveTabOnSummaryClick();
        }

        private void commandGoogleSearchChemNameExecute(object obj)
        {
            string ChemName = obj.ToString();
            string dataToBeSeacrh = string.Empty;
            switch (SelectedTabItem.Header)
            {
                case "Same Name One Cas":
                    dataToBeSeacrh = ChemName.Trim().Replace(" ", "+") + "+ Cas +" + listChemNameSingleCasAssign.Where(x => x.ChemicalName == ChemName).Select(x => x.CasAssigned).FirstOrDefault();
                    break;
                case "Same Name Multiple Cas":
                    dataToBeSeacrh = ChemName.Trim().Replace(" ", "+") + "+ Cas +" + listChemNameMultiCasAssign.Where(x => x.ChemicalName == ChemName).Select(x => x.CasAssigned).FirstOrDefault();
                    break;
                case "No Cas":
                    dataToBeSeacrh = ChemName.Trim().Replace(" ", "+") + "+ Cas";
                    break;
            }


            GoogleSearchChemicalName(dataToBeSeacrh);
        }

        private void commandAssignSeqIdentifierExecute(object obj)
        {
        AssignNextSeq:
            var count = listChemNameNoCas.Where(x => string.IsNullOrEmpty(x.OverWriteCasAssigned)).Count();
            var listExistSeqCas = 0;
            dicNextSeqIdentifier = _objCasAssignment_Service.GetNextSeqCasByModule(SelectedMdbModule, count);
            if (dicNextSeqIdentifier.Count > 0)
            {
                int nextSeq = dicNextSeqIdentifier.FirstOrDefault().Value;
                string preFixSeq = dicNextSeqIdentifier.FirstOrDefault().Key;
                foreach (var innerItem in listChemNameNoCas.Where(x => string.IsNullOrEmpty(x.OverWriteCasAssigned)))
                {
                    if (!_objCasAssignment_Service.ValidateNextSeq(preFixSeq + nextSeq))
                    {
                        innerItem.IsUpdateAuto = true;
                        innerItem.OverWriteCasAssigned = preFixSeq + nextSeq;
                        nextSeq = nextSeq + 1;
                        innerItem.SubCasType = "No Cas - Assign Seq Identifier Update";
                    }
                }
                if (listChemNameNoCas.Any(x => string.IsNullOrEmpty(x.OverWriteCasAssigned)))
                {
                    goto AssignNextSeq;
                }

                NotifyPropertyChanged("listChemNameNoCas");
            }
            else
            {
                _notifier.ShowError("Next Seq not available for module '" + SelectedMdbModule + "' in data table, Please insert it and try again!");
            }
        }



        private void commandAssignSeqSingleMultiCasExecute(object obj)
        {
        AssignNextSeq:
            dicNextSeqIdentifier = _objCasAssignment_Service.GetNextSeqCasByModule(SelectedMdbModule, 1);
            if (dicNextSeqIdentifier.Count > 0)
            {
                int nextSeq = dicNextSeqIdentifier.FirstOrDefault().Value;
                string preFixSeq = dicNextSeqIdentifier.FirstOrDefault().Key;
                if (!_objCasAssignment_Service.ValidateNextSeq(preFixSeq + nextSeq))
                {
                    switch (SelectedTabItem.Header)
                    {

                        case "Same Name One Cas":
                            Assign_Seq_NextSeed_SingleCas(preFixSeq, nextSeq);
                            break;
                        case "Same Name Multiple Cas":
                            Assign_Seq_NextSeed_MultiCas(preFixSeq, nextSeq);
                            break;
                        case "No Cas":
                            Assign_Seq_NextSeed_NoCas(preFixSeq, nextSeq);
                            break;
                        case "Existing Cas":
                            Assign_Seq_NextSeed_ExistingCas(preFixSeq, nextSeq);
                            break;
                    }
                }
                else
                {
                    goto AssignNextSeq;
                }
            }
            else
            {
                _notifier.ShowError("Next Seq not available for module '" + SelectedMdbModule + "' in data table, Please insert it and try again!");
            }
        }
        private void Assign_Seq_NextSeed_SingleCas(string preFixSeq, int nextSeq)
        {
            if (SelectedChemNameOneCasAssign != null)
            {
                SelectedChemNameOneCasAssign.IsUpdateAuto = true;
                SelectedChemNameOneCasAssign.OverWriteCasAssigned = preFixSeq + nextSeq;
                SelectedChemNameOneCasAssign.SubCasType = "Same Name One Cas - Assign SEQ Identifier Update";
            }
        }
        private void Assign_Seq_NextSeed_MultiCas(string preFixSeq, int nextSeq)
        {
            if (SelectedChemNameMultiCasAssign != null)
            {
                SelectedChemNameMultiCasAssign.IsUpdateAuto = true;
                SelectedChemNameMultiCasAssign.OverWriteCasAssigned = preFixSeq + nextSeq;
                SelectedChemNameMultiCasAssign.SubCasType = "Same Name Multiple Cas - Assign SEQ Identifier Update";
            }
        }
        private void Assign_Seq_NextSeed_NoCas(string preFixSeq, int nextSeq)
        {
            if (SelectedChemNameNoCas != null)
            {
                SelectedChemNameNoCas.IsUpdateAuto = true;
                SelectedChemNameNoCas.OverWriteCasAssigned = preFixSeq + nextSeq;
                SelectedChemNameNoCas.SubCasType = "No Cas - Assign SEQ Identifier Update";
            }
        }
        private void Assign_Seq_NextSeed_ExistingCas(string preFixSeq, int nextSeq)
        {
            if (SelectedChemNameExistingCas != null)
            {
                SelectedChemNameExistingCas.OverWriteCasAssigned = preFixSeq + nextSeq;
                SelectedChemNameExistingCas.SubCasType = "Existing Cas";
                NotifyPropertyChanged("SelectedChemNameExistingCas");

            }
        }

        private void commandCasDetailQsidExecute(object obj)
        {
            int casId = Convert.ToInt32(obj);
            var objMultiQsidDetail = SelectedChemNameMultiCasAssign.CasDetailList.Where(x => x.CasID == casId).FirstOrDefault();
            if (objMultiQsidDetail.ListOpenVisibility == Visibility.Collapsed)
            {
                var listQsidInMultiCas = new ObservableCollection<string>(_objCasAssignment_Service.GetQsidListByCasID(casId, IsFoodData, IsNonFoodData, SelectedChemNameMultiCasAssign.ChemicalName));
                objMultiQsidDetail.listQsidInMultiCas = listQsidInMultiCas;
            }
            else
            {
                objMultiQsidDetail.listQsidInMultiCas = null;
            }
            NotifyPropertyChanged("SelectedChemNameMultiCasAssign");
        }
        private bool defaultCanExecute(object obj)
        {
            return true;
        }
        private bool commandUploadMdbFileCanExecute(object obj)
        {
            bool isEnable = false;
            //if (Engine.IsOpenCasAssignmentFromAccess)
            //{
            //    isEnable = false;
            //}
            //else
            //{
            if (!string.IsNullOrEmpty(SelectedMdbPath) && !string.IsNullOrEmpty(SelectedMdbQSID))
            {
                isEnable = false;
            }
            else
            {
                isEnable = true;
            }
            //}
            return isEnable;
        }
        private bool commandProceedMdbFileCanExecute(object obj)
        {
            return true;
        }

        private void commandProceedMdbFileExecute(object obj)
        {
            ProcessMdbFile();

        }
        public void ProcessMdbFile()
        {
            tokenSource = new CancellationTokenSource();
            IsLoaderVisible = Visibility.Visible;
            NotifyPropertyChanged("IsLoaderVisible");
            cancelAnalysisVisibility = Visibility.Visible;
            NotifyPropertyChanged("cancelAnalysisVisibility");
            Task.Factory.StartNew(() =>
            { FetchChemicalNameCasFromAccess(SelectedMdbPath); }, tokenSource.Token).ContinueWith((t) =>
            {
                t.Exception.Handle((e) => true);
            }, TaskContinuationOptions.OnlyOnCanceled);

        }

        private void commandUploadMdbFileExecute(object obj)
        {
            IsLoaderVisible = Visibility.Visible;
            NotifyPropertyChanged("IsLoaderVisible");
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.DefaultExt = ".mdb";
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                SelectedMdbPath = SelectedPath[0];
                NotifyPropertyChanged("SelectedMdbPath");
                UploadFileBeforeAnalyse();
                // Task.Run(() => FetchChemicalNameCasFromAccess(SelectedMdbPath));

            }
            else
            {
                IsLoaderVisible = Visibility.Collapsed;
                NotifyPropertyChanged("IsLoaderVisible");
                ClearVariable();
            }
        }
        private void UploadFileBeforeAnalyse()
        {

            VersionControlSystem.Model.ViewModel.List_Dictionary_VM obj_MetaData = VersionControlSystem.Business.Common.CommonGeneralFunction.GetMeta_List_Dictionary_FromAccessFile("Select [LIST_FIELD_NAME], [QSID],[Module] from [List_Dictionary]", SelectedMdbPath);
            if (obj_MetaData == null)
            {
                StopFetchChemicalFromAccess("List_Dictionary table is not found in selected file!");
                return;
            }
            else if (string.IsNullOrEmpty(obj_MetaData.QSID))
            {
                StopFetchChemicalFromAccess("List_Dictionary table is not found the QSID in selected file!");
                return;
            }

            SelectedMdbListFieldName = obj_MetaData.List_Field_Name;
            SelectedMdbQSID = obj_MetaData.QSID;
            SelectedMdbModule = obj_MetaData.Module;
            NotifyPropertyChanged("SelectedMdbListFieldName");
            NotifyPropertyChanged("SelectedMdbQSID");
            if (string.IsNullOrEmpty(SelectedDefaultAccessTable))
            {
                GetTableNameFromAccess("data");
            }
            string filename = System.IO.Path.GetFileName(SelectedMdbPath);
            int? qsid_Id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(SelectedMdbQSID);
            if (qsid_Id == null || qsid_Id == 0)
            {
                List<string> staticVariable = new List<string>();
                _objLibraryFunction_Service.AddQSIDInLibrary(SelectedMdbQSID, Engine.CurrentUserSessionID, VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime(), out staticVariable);
                qsid_Id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(SelectedMdbQSID);
            }
            IsLoaderVisible = Visibility.Collapsed;
            NotifyPropertyChanged("IsLoaderVisible");
            SelectMdbVisible = Visibility.Visible;
            NotifyPropertyChanged("SelectMdbVisible");
            TableListVisible = Visibility.Visible;
            NotifyPropertyChanged("TableListVisible");
        }

        private bool commandUpdateMdbFileCanExecute(object obj)
        {
            bool IsUpdate = false;
            if (!string.IsNullOrEmpty(SelectedMdbQSID))//&& listChemNameCasAssign != null && listChemNameCasAssign.Count > 0
            {
                IsUpdate = true;
            }
            return IsUpdate;
        }

        private void commandUpdateMdbFileExecute(object obj)
        {
            IsLoaderVisible = Visibility.Visible;
            NotifyPropertyChanged("IsLoaderVisible");
            Task.Run(() => UpdateCasAssignmentMDBFile());
        }

        public void CommandGenerateExcelCasInitialAssementExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listCasAssignmentSummary.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessCasInitialAssesmentExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listCasAssignmentSummary.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }

        public void CommandGenerateExcelSameNameOneCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameSingleCasAssign.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessSameNameOneCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameSingleCasAssign.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }


        public void CommandGenerateExcelSameNameMultipleCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameMultiCasAssign.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessSameNameMultipleCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameMultiCasAssign.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }

        public void CommandGenerateExcelNoCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameNoCas.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessNoCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameNoCas.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }

        public void CommandGenerateExcelExistingCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameExistingCas.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessExistingCasExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listChemNameExistingCas.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }

        public void CommandGenerateExcelPastHistoryExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(ListHistoryCasAssignment.ToList());
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }

        public void CommandGenerateAccessPastHistoryExecute(object obj)
        {
            defaultTitleContent = string.IsNullOrEmpty(defaultTitleContent) ? "ExportedGrid" : defaultTitleContent;
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(ListHistoryCasAssignment.ToList());
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }


        public async Task OpenFileInAccessAsync(string resultCopyPath)
        {
            try
            {
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(resultCopyPath)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            catch (Exception ex)
            {
                _notifier.ShowError("System not open the file in access due to below error:" + Environment.NewLine + ex.Message);
            }
        }


        private void ShowMessageInThreadDispatcher(bool isError, string message)
        {
            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                if (isError)
                {
                    _notifier.ShowError(message);
                }
                else
                {
                    _notifier.ShowSuccess(message);
                }
            });
        }

        private void UpdateCasAssignmentMDBFile()
        {
            int? qsid_Id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(SelectedMdbQSID);
            DateTime startTime_ExistingCas = DateTime.Now;
            int ProcessId_ExistCas = _objCasAssignment_Service.AddProcessTimeTaken_CasAssignment("Cas Assignment", "Update Mdb File Commit Cas back to MDB", startTime_ExistingCas, (DateTime?)null, "", 0, 0, (int)qsid_Id, Engine.CurrentUserSessionID);
            List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> listUpdateCas = new List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM>();
            listChemNameNoCas.Where(x => x.IsUpdateAuto != true && x.IsOverWriteCasAssigned && x.Selected_MIC_ING == null).ToList().ForEach(x => x.SubCasType = "No Cas - Manually Update");
            listChemNameExistingCas.Where(x => x.IsOverWriteCasAssigned && !string.IsNullOrEmpty(x.OverWriteCasAssigned) && x.Selected_MIC_ING == null).ToList().ForEach(x => x.SubCasType = "Existing Cas - Manually Update");
            listUpdateCas.AddRange(MergeCasAssignChemicalName(listChemNameSingleCasAssign.ToList()));
            listUpdateCas.AddRange(MergeCasAssignChemicalName(listChemNameMultiCasAssign.Where(x => !string.IsNullOrEmpty(x.CasAssigned) || !string.IsNullOrEmpty(x.OverWriteCasAssigned)).ToList()));
            listUpdateCas.AddRange(MergeCasAssignChemicalName(listChemNameNoCas.Where(x => !string.IsNullOrEmpty(x.OverWriteCasAssigned)).ToList()));
            listUpdateCas.AddRange(MergeCasAssignChemicalName(listChemNameExistingCas.Where(x => x.IsOverWriteCasAssigned && !string.IsNullOrEmpty(x.OverWriteCasAssigned)).ToList()));
            bool resultUpdate = _objCasAssignment_Service.UpdateAccessData(listUpdateCas, SelectedMdbPath, Environment.UserName, SelectedMdbQSID, SelectedDefaultAccessTable);
            if (resultUpdate)
            {
                UpdateINGNextSeed();
                UpdateMICNextSeed();
                ShowMessageInThreadDispatcher(false, "Assigned Cas Updated Successfully");
                string path = SelectedMdbPath;
                bool IsFood = IsFoodData;
                bool IsNonFood = IsNonFoodData;
                string defaultTable = SelectedDefaultAccessTable;
                ClearVariable();
                SelectedMdbPath = path;
                IsFoodData = IsFood;
                IsNonFoodData = IsNonFood;
                //SelectedDefaultAccessTable = defaultTable;
                NotifyPropertyChanged("SelectedMdbPath");
                NotifyPropertyChanged("IsFoodData");
                NotifyPropertyChanged("IsNonFoodData");
                GetTableNameFromAccess(defaultTable);
                //Task.Run(() => FetchChemicalNameCasFromAccess(path));
                ProcessMdbFile();
            }
            else
            {
                ShowMessageInThreadDispatcher(true, "Assigned cas update failed due to internal server error.");
                IsLoaderVisible = Visibility.Collapsed;
                NotifyPropertyChanged("IsLoaderVisible");
                // _notifier.ShowError("Assigned cas update failed due to internal server error.");
            }
            DateTime endTime_ExistingCas = DateTime.Now;
            TimeSpan timeTake = endTime_ExistingCas - startTime_ExistingCas;
            _objCasAssignment_Service.UpdateProcessTimeTaken_CasAssignment(ProcessId_ExistCas, endTime_ExistingCas, timeTake.TotalSeconds.ToString());

        }

        public List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> MergeCasAssignChemicalName(List<CasAssignChemicalNamesVM> listCasAssignChemicalName)
        {
            var listCasAssignOverride = listCasAssignChemicalName.Where(x => x.IsOverWriteCasAssigned == true).ToList();
            listCasAssignOverride.ForEach(x => x.CasAssigned = x.OverWriteCasAssigned);
            return Common.CommonGeneralFunction.ConvertCasAssignmentToViewModel(listCasAssignChemicalName);
        }

        private bool commandCancelCasAssignCanExecute(object obj)
        {
            bool IsUpdate = false;
            if (!string.IsNullOrEmpty(SelectedMdbQSID))
            {
                IsUpdate = true;
            }
            return IsUpdate;
        }

        private void commandCancelCasAssignExecute(object obj)
        {
            ClearVariable();
        }
        public void ClearVariableSelectedMbFile()
        {
            SelectedMdbListFieldName = string.Empty;
            SelectedMdbQSID = string.Empty;
            SelectedMdbPath = string.Empty;
            SelectedMdbModule = string.Empty;
            SelectedDefaultAccessTable = string.Empty;
            NotifyPropertyChanged("SelectedMdbListFieldName");
            NotifyPropertyChanged("SelectedMdbQSID");
            NotifyPropertyChanged("SelectedMdbPath");
            NotifyPropertyChanged("SelectedMdbModule");
            SelectMdbVisible = Visibility.Collapsed;
            NotifyPropertyChanged("SelectMdbVisible");
            TableListVisible = Visibility.Collapsed;
            NotifyPropertyChanged("TableListVisible");
            IsFoodData = false;
            NotifyPropertyChanged("IsFoodData");
            IsNonFoodData = false;
            NotifyPropertyChanged("IsNonFoodData");
            SelectedAccessTable = null;
            NotifyPropertyChanged("SelectedAccessTable");
            listAccessTable = null;
            NotifyPropertyChanged("listAccessTable");
        }
        public void ClearVariable()
        {
            listChemNameCasAssign = null;
            AnalysisMdbVisible = Visibility.Collapsed;
            NotifyPropertyChanged("AnalysisMdbVisible");
            ListHistoryCasAssignment = null;
            IsSelectedInitialAssessment = true;
            NotifyPropertyChanged("IsSelectedInitialAssessment");
            Engine.DelimiterForQsidCasAssignmentTool = string.Empty;

        }

        public Task<string> FetchChemicalNameCasFromAccess(string path)
        {

            using (tokenSource.Token.Register(() => CancelCurrentThread()))
            {
                VersionControlSystem.Model.ViewModel.List_Dictionary_VM obj_MetaData = VersionControlSystem.Business.Common.CommonGeneralFunction.GetMeta_List_Dictionary_FromAccessFile("Select [LIST_FIELD_NAME], [QSID],[Module] from [List_Dictionary]", path);
                if (obj_MetaData == null)
                {
                    StopFetchChemicalFromAccess("List_Dictionary table is not found in selected file!");
                    return Task.FromResult("");
                }
                else if (string.IsNullOrEmpty(obj_MetaData.QSID))
                {
                    StopFetchChemicalFromAccess("List_Dictionary table is not found the QSID in selected file!");
                    return Task.FromResult("");
                }

                SelectedMdbListFieldName = obj_MetaData.List_Field_Name;
                SelectedMdbQSID = obj_MetaData.QSID;
                SelectedMdbModule = obj_MetaData.Module;
                NotifyPropertyChanged("SelectedMdbListFieldName");
                NotifyPropertyChanged("SelectedMdbQSID");
                if (string.IsNullOrEmpty(SelectedDefaultAccessTable))
                {
                    GetTableNameFromAccess("data");
                }
                string filename = System.IO.Path.GetFileName(path);
                int? qsid_Id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(SelectedMdbQSID);
                if (!tokenSource.IsCancellationRequested)
                {

                    if (qsid_Id == null || qsid_Id == 0)
                    {
                        List<string> staticVariable = new List<string>();
                        _objLibraryFunction_Service.AddQSIDInLibrary(SelectedMdbQSID, Engine.CurrentUserSessionID, VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime(), out staticVariable);
                        qsid_Id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(SelectedMdbQSID);
                    }

                }
                DateTime startTime_ExistingCas = DateTime.Now;
                DateTime endTime_ExistingCas;
                TimeSpan timeTake;
                int ProcessId_ExistCas = _objCasAssignment_Service.AddProcessTimeTaken_CasAssignment("Cas Assignment", "Upload Mdb File Fetch Existing Cas Tab Data From SQL", startTime_ExistingCas, (DateTime?)null, "", 0, 0, (int)qsid_Id, Engine.CurrentUserSessionID);
                if (!tokenSource.IsCancellationRequested)
                {
                    var chemicalNameExistingCas = _objCasAssignment_Service.GetChemicalNameExistingCasFromAccess(path, SelectedDefaultAccessTable, IsFoodData, IsNonFoodData);
                    if (chemicalNameExistingCas.Keys.FirstOrDefault() != "Success")
                    {
                        StopFetchChemicalFromAccess(chemicalNameExistingCas.Keys.FirstOrDefault());
                        return Task.FromResult("");
                    }

                    var existingCasAssignedList = Common.CommonGeneralFunction.ConvertCasAssignmentToViewModel(chemicalNameExistingCas.Values.FirstOrDefault(), listSubstanceType, listSubstanceCategories);
                    listChemNameExistingCas = new ObservableCollection<CasAssignChemicalNamesVM>(MakeSelectionSubstanceTypeAndCategoryExisting(existingCasAssignedList));
                    NotifyPropertyChanged("listChemNameExistingCas");
                    endTime_ExistingCas = DateTime.Now;
                    timeTake = endTime_ExistingCas - startTime_ExistingCas;
                    _objCasAssignment_Service.UpdateProcessTimeTaken_CasAssignment(ProcessId_ExistCas, endTime_ExistingCas, timeTake.TotalSeconds.ToString());
                }

                startTime_ExistingCas = DateTime.Now;
                int ProcessId_NoCas = _objCasAssignment_Service.AddProcessTimeTaken_CasAssignment("Cas Assignment", "Upload Mdb File Fetch Same Name One Cas, Multiple Cas and No Cas Data From SQL", startTime_ExistingCas, (DateTime?)null, "", 0, 0, (int)qsid_Id, Engine.CurrentUserSessionID);
                if (!tokenSource.IsCancellationRequested)
                {
                    var dicResultChemicalNameNoCas = _objCasAssignment_Service.GetChemicalNameNoCasFromAccess(path, obj_MetaData.QSID, IsFoodData, IsNonFoodData, SelectedDefaultAccessTable);
                    if (dicResultChemicalNameNoCas.Keys.FirstOrDefault() != "Success" && dicResultChemicalNameNoCas.Keys.FirstOrDefault() != "WarningChemicalName")
                    {
                        StopFetchChemicalFromAccess(dicResultChemicalNameNoCas.Keys.FirstOrDefault());
                        return Task.FromResult("");
                    }
                    else
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            _notifier.ShowWarning("Some entries in selected table have no Name, please make sure all entries in table have names before assigning CAS");
                        });
                    }
                    listChemNameCasAssign = Common.CommonGeneralFunction.ConvertCasAssignmentToViewModel(dicResultChemicalNameNoCas.Values.FirstOrDefault(), listSubstanceType, listSubstanceCategories);
                }
                if (!tokenSource.IsCancellationRequested)
                {
                    Engine.DelimiterForQsidCasAssignmentTool = _objCasAssignment_Service.GetDelimiterByQsid(SelectedMdbQSID);
                    if (listChemNameSingleCasAssign != null && listChemNameSingleCasAssign.Count > 0)
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            listChemNameSingleCasAssign.Clear();
                        });
                    }
                    var listCasAssignSingle = listChemNameCasAssign.Where(x => x.CasCount == 1).Select(x => new CasAssignChemicalNamesVM { ChemicalName = x.ChemicalName, CasAssigned = x.CasDetailList.FirstOrDefault().Cas, OriginalChemicalName = x.OriginalChemicalName, Type = x.Type, SubCasType = "Same Name One Cas", Qsid = x.Qsid, RefernceRN = x.RefernceRN, Frequency = x.Frequency, ChemicalNameID = x.ChemicalNameID, CasDetailList = x.CasDetailList, listSubstanceType_VM = x.listSubstanceType_VM, listSubstanceCategories_VM = x.listSubstanceCategories_VM }).ToList();

                    listChemNameSingleCasAssign = new ObservableCollection<CasAssignChemicalNamesVM>(MakeSelectionSubstanceTypeAndCategory(listCasAssignSingle));
                    NotifyPropertyChanged("listChemNameSingleCasAssign");
                    listChemNameMultiCasAssign = new ObservableCollection<CasAssignChemicalNamesVM>(listChemNameCasAssign.Where(x => x.CasCount > 1));
                    NotifyPropertyChanged("listChemNameMultiCasAssign");
                    listChemNameNoCas = new ObservableCollection<CasAssignChemicalNamesVM>(listChemNameCasAssign.Where(x => x.CasCount == 0));
                    NotifyPropertyChanged("listChemNameNoCas");
                }
                if (!tokenSource.IsCancellationRequested)
                {
                    cancelAnalysisVisibility = Visibility.Collapsed;
                    NotifyPropertyChanged("cancelAnalysisVisibility");
                    AnalysisMdbVisible = Visibility.Visible;
                    NotifyPropertyChanged("AnalysisMdbVisible");

                    IsLoaderVisible = Visibility.Collapsed;
                    NotifyPropertyChanged("IsLoaderVisible");
                    GetCasAssignmentSummary();
                    endTime_ExistingCas = DateTime.Now;
                    timeTake = endTime_ExistingCas - startTime_ExistingCas;
                    _objCasAssignment_Service.UpdateProcessTimeTaken_CasAssignment(ProcessId_NoCas, endTime_ExistingCas, timeTake.TotalSeconds.ToString());
                    Task.Run(() => GetPastHistoryCasAssignment());
                    ExecuteFunctionalityOnTabChanged();
                }
                ClearFilterData();
                return Task.FromResult("");
            }
        }

        private void CancelCurrentThread()
        {
            cancelAnalysisVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("cancelAnalysisVisibility");
            AnalysisMdbVisible = Visibility.Collapsed;
            NotifyPropertyChanged("AnalysisMdbVisible");
            IsLoaderVisible = Visibility.Collapsed;
            NotifyPropertyChanged("IsLoaderVisible");
            ClearVariable();
            throw new Exception("Canceled by User");
            //Thread.CurrentThread.Abort();
        }

        private void ClearFilterData()
        {
            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                if (CustomQueryController.ColumnFilterData != null)
                {
                    CustomQueryController.ClearFilter();
                }
                if (CustomQueryControllerMultipleCas.ColumnFilterData != null)
                {
                    CustomQueryControllerMultipleCas.ClearFilter();
                }
                if (CustomQueryControllerNoCas.ColumnFilterData != null)
                {
                    CustomQueryControllerNoCas.ClearFilter();
                }
                if (CustomQueryControllerExistingCas.ColumnFilterData != null)
                {
                    CustomQueryControllerExistingCas.ClearFilter();
                }
                if (CustomQueryControllerHistory.ColumnFilterData != null)
                {
                    CustomQueryControllerHistory.ClearFilter();
                }
                selectedIndexTab = 0;
                NotifyPropertyChanged("selectedIndexTab");
            });
        }
        private List<CasAssignChemicalNamesVM> MakeSelectionSubstanceTypeAndCategory(List<CasAssignChemicalNamesVM> CasAssignmentList)
        {
            var listSelectedType = CasAssignmentList.Where(x => x.CasDetailList != null && x.CasDetailList.Any(y => y.SubstanceTypeID != null || y.SubstanceCategoryID != null)).ToList();
            foreach (var itemSelected in listSelectedType)
            {
                var casDetailVM = itemSelected.CasDetailList.FirstOrDefault();

                if (casDetailVM != null)
                {
                    int? subTypeID = casDetailVM.SubstanceTypeID;
                    int? subCatID = casDetailVM.SubstanceCategoryID;
                    if (subTypeID != null)
                    {
                        itemSelected.SelectedSubstanceType = itemSelected.listSubstanceType_VM.Where(x => x.SubstanceTypeID == subTypeID).FirstOrDefault();
                    }
                    if (subCatID != null)
                    {
                        itemSelected.SelectedSubstanceCate = itemSelected.listSubstanceCategories_VM.Where(x => x.SubstanceCategoryID == subCatID).FirstOrDefault();
                    }
                    itemSelected.IsInCorrect = casDetailVM.IsInCorrect_Cas;
                }
            }
            return CasAssignmentList;
        }
        private List<CasAssignChemicalNamesVM> MakeSelectionSubstanceTypeAndCategoryExisting(List<CasAssignChemicalNamesVM> CasAssignmentList)
        {
            var listSelectedType = CasAssignmentList.Where(x => x.SubstanceCategoryIDSelected != null || x.SubstanceTypeIDSelected != null).ToList();
            foreach (var itemSelected in listSelectedType)
            {
                if (itemSelected.SubstanceTypeIDSelected != null)
                {
                    itemSelected.SelectedSubstanceType = itemSelected.listSubstanceType_VM.Where(x => x.SubstanceTypeID == itemSelected.SubstanceTypeIDSelected).FirstOrDefault();
                }
                if (itemSelected.SubstanceCategoryIDSelected != null)
                {
                    itemSelected.SelectedSubstanceCate = itemSelected.listSubstanceCategories_VM.Where(x => x.SubstanceCategoryID == itemSelected.SubstanceCategoryIDSelected).FirstOrDefault();
                }
            }
            return CasAssignmentList;
        }
        private void StopFetchChemicalFromAccess(string ErrorMessage)
        {
            ShowMessageInThreadDispatcher(true, ErrorMessage);
            IsLoaderVisible = Visibility.Collapsed;
            NotifyPropertyChanged("IsLoaderVisible");
            ClearVariable();

        }

        private void UpdateCasByHighestQsidCount()
        {
            if (IsAssignCasHighestQsidCount)
            {
                foreach (var item in listChemNameMultiCasAssign.Where(x => string.IsNullOrEmpty(x.CasAssigned)))
                {
                    var selectedCas = item.CasDetailList.OrderByDescending(x => x.QsidCount).FirstOrDefault();
                    item.SelectedCasDetail = selectedCas;
                    item.IsUpdateAuto = true;
                    item.SubCasType = "Same Name Multiple Cas - Assign Auto Highest Qsid Count";
                }
            }
            else
            {
                foreach (var item in listChemNameMultiCasAssign.Where(x => x.IsUpdateAuto))
                {
                    item.SelectedCasDetail = null;
                    item.CasAssigned = string.Empty;
                    item.IsUpdateAuto = false;
                    item.SubCasType = string.Empty;
                }
            }
            NotifyPropertyChanged("listChemNameMultiCasAssign");
        }
        public void ExecuteFunctionalityOnTabChanged()
        {
            AssignCasCheckVisibility = Visibility.Collapsed;
            AssignSeqBtnVisibility = Visibility.Collapsed;

            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                switch (SelectedTabItem.Header)
                {
                    case "Initial assessment summary":
                        listRowCount = listCasAssignmentSummary != null ? listCasAssignmentSummary.Count : 0;

                        break;
                    case "Same Name One Cas":
                        listRowCount = listChemNameSingleCasAssign != null ? listChemNameSingleCasAssign.Count : 0;

                        break;
                    case "Same Name Multiple Cas":
                        listRowCount = listChemNameMultiCasAssign != null ? listChemNameMultiCasAssign.Count : 0;
                        AssignCasCheckVisibility = Visibility.Visible;
                        break;
                    case "No Cas":
                        listRowCount = listChemNameNoCas != null ? listChemNameNoCas.Count : 0;
                        AssignSeqBtnVisibility = Visibility.Visible;
                        break;
                    case "Existing Cas":
                        listRowCount = listChemNameExistingCas != null ? listChemNameExistingCas.Count : 0;
                        break;
                    case "Past History":
                        listRowCount = ListHistoryCasAssignment != null ? ListHistoryCasAssignment.Count : 0;

                        break;
                }
            });
            NotifyPropertyChanged("listRowCount");
            NotifyPropertyChanged("AssignCasCheckVisibility");
            NotifyPropertyChanged("AssignSeqBtnVisibility");

        }

        public void FetchChemicalName()
        {
            if (!string.IsNullOrEmpty(SelectedMdbPath))
            {
                UploadFileBeforeAnalyse();
                ProcessMdbFile();
            }
        }

        public void GetCasAssignmentSummary()
        {

            List<VersionControlSystem.Model.ViewModel.CasAssignmentSummary> listCasSmry = new List<VersionControlSystem.Model.ViewModel.CasAssignmentSummary>();
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No of unique names requiring CAS",
                count = listChemNameCasAssign != null ? listChemNameCasAssign.Count : 0,
                SummaryCountDetail = " - ",
                ForegroundCol = listChemNameCasAssign.Count() == 0 ? "Green" : "Orange"
            }); ;
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No of unique names with existing CAS",
                count = listChemNameExistingCas != null ? listChemNameExistingCas.Count : 0,
                SummaryCountDetail = " - ",
                ForegroundCol = listChemNameExistingCas.Count() == 0 ? "Green" : "Orange"
            });

            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No of unique names with Exact Match",
                count = listChemNameCasAssign.Where(x => x.Type == "Exact Match" && x.CasCount > 0).Count(),
                SummaryCountDetail = " Same Name One Cas = " + listChemNameCasAssign.Where(x => x.Type.Contains("Exact Match") && x.CasCount == 1).Count() + " \r\n Same Name Multiple Cas = " + listChemNameCasAssign.Where(x => x.Type.Contains("Exact Match") && x.CasCount > 1).Count(),
                ForegroundCol = listChemNameCasAssign.Where(x => x.Type == "Exact Match").Count() == 0 ? "Green" : "Orange"
            });
            //+ "\r\n No Cas = " + listChemNameCasAssign.Where(x => x.Type == "Exact Match" && x.CasCount == 0).Count(),
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No of unique names with Squash Match",
                count = listChemNameCasAssign.Where(x => x.Type == "Squash Match").Count(),
                SummaryCountDetail = " Same Name One Cas = " + listChemNameCasAssign.Where(x => x.Type.Contains("Squash Match") && x.CasCount == 1).Count() + "\r\n Same Name Multiple Cas = " + listChemNameCasAssign.Where(x => x.Type.Contains("Squash Match") && x.CasCount > 1).Count() + "\r\n No Cas = " + listChemNameCasAssign.Where(x => x.Type.Contains("Squash Match") && x.CasCount == 0).Count(),
                ForegroundCol = listChemNameCasAssign.Where(x => x.Type == "Squash Match").Count() == 0 ? "Green" : "Orange"
            });
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No of unique names with No Match",
                count = listChemNameCasAssign.Where(x => x.Type == "No Match").Count(),
                SummaryCountDetail = " No Cas = " + listChemNameCasAssign.Where(x => x.Type == "No Match").Count(),
                ForegroundCol = listChemNameCasAssign.Where(x => x.Type == "No Match").Count() == 0 ? "Green" : "Orange"
            });
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No. of No cas with Exact Match",
                count = listChemNameCasAssign.Where(x => x.Type.Contains("Exact Match") && x.CasCount == 0).Count(),
                SummaryCountDetail = " - ",
                ForegroundCol = listChemNameCasAssign.Where(x => x.Type.Contains("Exact Match") && x.CasCount == 0).Count() == 0 ? "Green" : "Orange"
            });
            listCasSmry.Add(new VersionControlSystem.Model.ViewModel.CasAssignmentSummary
            {
                CasSummary = "No. of incorrect existing CAS+Name",
                count = listChemNameExistingCas.Where(x => x.IsInCorrect == true).Count(),
                SummaryCountDetail = " - ",
                ForegroundCol = listChemNameExistingCas.Where(x => x.IsInCorrect == true).Count() == 0 ? "Green" : "Orange"
            });

            listCasAssignmentSummary = new ObservableCollection<VersionControlSystem.Model.ViewModel.CasAssignmentSummary>(listCasSmry);
            NotifyPropertyChanged("listCasAssignmentSummary");

        }

        public void GetPastHistoryCasAssignment()
        {
            if (!string.IsNullOrEmpty(SelectedMdbQSID))
            {
                ListHistoryCasAssignment = new ObservableCollection<CasAssignmentHistory>(_objCasAssignment_Service.GetCasAssignmentHistory(SelectedMdbQSID));
                NotifyPropertyChanged("ListHistoryCasAssignment");
            }
        }

        public void GetTableNameFromAccess(string tableName)
        {
            if (!string.IsNullOrEmpty(SelectedMdbPath))
            {
                var listTableName = _objCasAssignment_Service.GetTableNameFromAccess(SelectedMdbPath).Select(x => new VersionControlSystem.Model.ViewModel.SelectListItem { Text = x });
                listAccessTable = new ObservableCollection<VersionControlSystem.Model.ViewModel.SelectListItem>(listTableName);
                var objDefaultTable = listAccessTable.Where(x => x.Text.ToLower() == tableName.ToLower()).FirstOrDefault();
                if (objDefaultTable != null)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        SelectedAccessTable = objDefaultTable;
                    });
                }
                NotifyPropertyChanged("listAccessTable");
            }
        }
        private bool ValidateDefaultAccessTable(string tblName)
        {
            bool isValid = false;
            if (!_objCasAssignment_Service.ValidateDefaultAccessTable(tblName, SelectedMdbPath))
            {
                AnalysisMdbVisible = Visibility.Collapsed;
                _notifier.ShowError("Selected table '" + tblName + "' missing pref or cas columns!");
            }
            else
            {
                isValid = true;
                //SelectMdbVisible = Visibility.Visible;
            }
            NotifyPropertyChanged("AnalysisMdbVisible");
            return isValid;
        }

        private void GoogleSearchChemicalName(string dataToBeSeacrh)
        {
            var p = new Process();
            p.StartInfo = new ProcessStartInfo("https://www.google.com/search?q=" + dataToBeSeacrh)
            {
                UseShellExecute = true
            };
            p.Start();
            //            System.Diagnostics.Process.Start();
        }
        private void ActiveTabOnSummaryClick()
        {
            if (SelectedCasAssignmentSummary != null)
            {
                switch (SelectedCasAssignmentSummary.CasSummary)
                {
                    case "No of unique names with Exact Match":
                        selectedIndexTab = 1;
                        break;
                    //case "No of names with squash match and a single CAS":
                    //    selectedIndexTab = 1;
                    //    break;
                    case "No of unique names with Squash Match":
                        selectedIndexTab = 2;
                        break;
                    case "No of unique names with No Match":
                        selectedIndexTab = 3;
                        break;
                    case "No of unique names with existing CAS":
                        selectedIndexTab = 4;
                        break;
                    case "No. of incorrect existing CAS+Name":
                        selectedIndexTab = 4;
                        break;
                }
                NotifyPropertyChanged("selectedIndexTab");
            }
        }
        private void commandAssignMICSeedExecute(object obj)
        {
            switch (SelectedTabItem.Header)
            {

                case "Same Name One Cas":
                    Get_ING_MIC_NextSeed_SingleCas(GetMICNextSeedList(), false, true, Visibility.Visible);
                    break;
                case "Same Name Multiple Cas":
                    Get_ING_MIC_NextSeed_MultipleCas(GetMICNextSeedList(), false, true, Visibility.Visible);
                    break;
                case "No Cas":
                    Get_ING_MIC_NextSeed_NoCas(GetMICNextSeedList(), false, true, Visibility.Visible);
                    break;
                case "Existing Cas":
                    Get_ING_MIC_NextSeed_ExistingCas(GetMICNextSeedList(), false, true, Visibility.Visible);
                    break;

            }

        }

        private void commandAssignINGSeedExecute(object obj)
        {
            switch (SelectedTabItem.Header)
            {

                case "Same Name One Cas":
                    Get_ING_MIC_NextSeed_SingleCas(GetINGNextSeedList(), true, false, Visibility.Visible);
                    break;
                case "Same Name Multiple Cas":
                    Get_ING_MIC_NextSeed_MultipleCas(GetINGNextSeedList(), true, false, Visibility.Visible);
                    break;
                case "No Cas":
                    Get_ING_MIC_NextSeed_NoCas(GetINGNextSeedList(), true, false, Visibility.Visible);
                    break;
                case "Existing Cas":
                    Get_ING_MIC_NextSeed_ExistingCas(GetINGNextSeedList(), true, false, Visibility.Visible);
                    break;

            }
        }
        private void Get_ING_MIC_NextSeed_SingleCas(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> list_CAS_ING_MIC_Seed, bool Is_Ing, bool Is_Mic, Visibility ComboING_MIC)
        {
            if (SelectedChemNameOneCasAssign != null)
            {
                SelectedChemNameOneCasAssign.listdetail_MIC_ING = new ObservableCollection<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>(list_CAS_ING_MIC_Seed);
                SelectedChemNameOneCasAssign.Is_ING = Is_Ing;
                SelectedChemNameOneCasAssign.Is_MIC = Is_Mic;
                SelectedChemNameOneCasAssign.ComboING_MIC = ComboING_MIC;
                SelectedChemNameOneCasAssign.SubCasType = "Same Name One Cas";
                NotifyPropertyChanged("SelectedChemNameOneCasAssign");

            }
        }
        private void Get_ING_MIC_NextSeed_MultipleCas(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> list_CAS_ING_MIC_Seed, bool Is_Ing, bool Is_Mic, Visibility ComboING_MIC)
        {
            if (SelectedChemNameMultiCasAssign != null)
            {
                SelectedChemNameMultiCasAssign.listdetail_MIC_ING = new ObservableCollection<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>(list_CAS_ING_MIC_Seed);
                SelectedChemNameMultiCasAssign.Is_ING = Is_Ing;
                SelectedChemNameMultiCasAssign.Is_MIC = Is_Mic;
                SelectedChemNameMultiCasAssign.ComboING_MIC = ComboING_MIC;
                SelectedChemNameMultiCasAssign.SubCasType = "Same Name Multiple Cas";
                NotifyPropertyChanged("SelectedChemNameMultiCasAssign");

            }
        }
        private void Get_ING_MIC_NextSeed_NoCas(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> list_CAS_ING_MIC_Seed, bool Is_Ing, bool Is_Mic, Visibility ComboING_MIC)
        {
            if (SelectedChemNameNoCas != null)
            {
                SelectedChemNameNoCas.listdetail_MIC_ING = new ObservableCollection<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>(list_CAS_ING_MIC_Seed);
                SelectedChemNameNoCas.Is_ING = Is_Ing;
                SelectedChemNameNoCas.Is_MIC = Is_Mic;
                SelectedChemNameNoCas.ComboING_MIC = ComboING_MIC;
                SelectedChemNameNoCas.SubCasType = "No Cas";
                NotifyPropertyChanged("SelectedChemNameNoCas");

            }
        }
        private void Get_ING_MIC_NextSeed_ExistingCas(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> list_CAS_ING_MIC_Seed, bool Is_Ing, bool Is_Mic, Visibility ComboING_MIC)
        {
            if (SelectedChemNameExistingCas != null)
            {
                SelectedChemNameExistingCas.listdetail_MIC_ING = new ObservableCollection<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>(list_CAS_ING_MIC_Seed);
                SelectedChemNameExistingCas.Is_ING = Is_Ing;
                SelectedChemNameExistingCas.Is_MIC = Is_Mic;
                SelectedChemNameExistingCas.ComboING_MIC = ComboING_MIC;
                SelectedChemNameExistingCas.SubCasType = "Existing Cas";
                NotifyPropertyChanged("SelectedChemNameExistingCas");

            }
        }

        private void commandRemoveMIC_ING_SeedExecute(object obj)
        {
            switch (SelectedTabItem.Header)
            {

                case "Same Name One Cas":
                    Remove_ING_MIC_NextSeed_SingleCas();
                    break;
                case "Same Name Multiple Cas":
                    Remove_ING_MIC_NextSeed_MultiCas();
                    break;
                case "No Cas":
                    Remove_ING_MIC_NextSeed_NoCas();
                    break;
                case "Existing Cas":
                    Remove_ING_MIC_NextSeed_ExistingCas();
                    break;

            }
        }
        private void Remove_ING_MIC_NextSeed_SingleCas()
        {
            if (SelectedChemNameOneCasAssign != null)
            {
                SelectedChemNameOneCasAssign.Is_ING = false;
                SelectedChemNameOneCasAssign.Is_MIC = false;
                SelectedChemNameOneCasAssign.Selected_MIC_ING = null;
                SelectedChemNameOneCasAssign.OverWriteCasAssigned = string.Empty;
                SelectedChemNameOneCasAssign.SubCasType = string.Empty;
                SelectedChemNameOneCasAssign.ComboING_MIC = Visibility.Collapsed;
                NotifyPropertyChanged("SelectedChemNameOneCasAssign");
            }
        }
        private void Remove_ING_MIC_NextSeed_MultiCas()
        {
            if (SelectedChemNameMultiCasAssign != null)
            {
                SelectedChemNameMultiCasAssign.Is_ING = false;
                SelectedChemNameMultiCasAssign.Is_MIC = false;
                SelectedChemNameMultiCasAssign.Selected_MIC_ING = null;
                SelectedChemNameMultiCasAssign.OverWriteCasAssigned = string.Empty;
                SelectedChemNameMultiCasAssign.SubCasType = string.Empty;
                SelectedChemNameMultiCasAssign.ComboING_MIC = Visibility.Collapsed;
                NotifyPropertyChanged("SelectedChemNameMultiCasAssign");
            }
        }
        private void Remove_ING_MIC_NextSeed_NoCas()
        {
            if (SelectedChemNameNoCas != null)
            {
                SelectedChemNameNoCas.Is_ING = false;
                SelectedChemNameNoCas.Is_MIC = false;
                SelectedChemNameNoCas.Selected_MIC_ING = null;
                SelectedChemNameNoCas.OverWriteCasAssigned = string.Empty;
                SelectedChemNameNoCas.SubCasType = string.Empty;
                SelectedChemNameNoCas.ComboING_MIC = Visibility.Collapsed;
                NotifyPropertyChanged("SelectedChemNameNoCas");
            }
        }
        private void Remove_ING_MIC_NextSeed_ExistingCas()
        {
            if (SelectedChemNameExistingCas != null)
            {
                SelectedChemNameExistingCas.Is_ING = false;
                SelectedChemNameExistingCas.Is_MIC = false;
                SelectedChemNameExistingCas.Selected_MIC_ING = null;
                SelectedChemNameExistingCas.OverWriteCasAssigned = string.Empty;
                SelectedChemNameExistingCas.SubCasType = string.Empty;
                SelectedChemNameExistingCas.ComboING_MIC = Visibility.Collapsed;
                NotifyPropertyChanged("SelectedChemNameExistingCas");
            }
        }

        private List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetMICNextSeedList()
        {
            List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectMIC = new List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>();
            listSelectMIC.AddRange(listChemNameSingleCasAssign.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectMIC.AddRange(listChemNameMultiCasAssign.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectMIC.AddRange(listChemNameNoCas.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectMIC.AddRange(listChemNameExistingCas.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            var listSelectedMIC = listSelectMIC.OrderByDescending(x => x.NextSeed).GroupBy(x => new { x.CatCode, x.CatName }).Select(x => x.First()).ToList();
            return _objCasAssignment_Service.GetMICNextSeedList(listSelectedMIC);
        }
        private List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetINGNextSeedList()
        {
            List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectING = new List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>();
            listSelectING.AddRange(listChemNameSingleCasAssign.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameMultiCasAssign.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameNoCas.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameExistingCas.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            var listSelectedING = listSelectING.OrderByDescending(x => x.NextSeed).GroupBy(x => new { x.CatCode, x.CatName }).Select(x => x.First()).ToList();
            return _objCasAssignment_Service.GetINGNextSeedList(listSelectedING);
        }
        private void UpdateINGNextSeed()
        {
            List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectING = new List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>();
            listSelectING.AddRange(listChemNameSingleCasAssign.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameMultiCasAssign.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameNoCas.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listSelectING.AddRange(listChemNameExistingCas.Where(x => x.Is_ING && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            var listSelectedING = listSelectING.OrderByDescending(x => x.NextSeed).GroupBy(x => new { x.CatCode, x.CatName }).Select(x => x.First()).ToList();

            if (listSelectedING.Count > 0)
            {
                _objCasAssignment_Service.UpdateINGNextSeed(listSelectedING);
            }
        }
        private void UpdateMICNextSeed()
        {
            List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listAllSelectedMIC = new List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed>();
            listAllSelectedMIC.AddRange(listChemNameSingleCasAssign.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listAllSelectedMIC.AddRange(listChemNameMultiCasAssign.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listAllSelectedMIC.AddRange(listChemNameNoCas.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            listAllSelectedMIC.AddRange(listChemNameExistingCas.Where(x => x.Is_MIC && x.Selected_MIC_ING != null).Select(x => x.Selected_MIC_ING).ToList());
            var listSelectedMIC = listAllSelectedMIC.OrderByDescending(x => x.NextSeed).GroupBy(x => new { x.CatCode, x.CatName }).Select(x => x.First()).ToList();
            if (listSelectedMIC.Count > 0)
            {
                _objCasAssignment_Service.UpdateMICNextSeed(listSelectedMIC);
            }
        }
        public void CommandShowPopUpMapInCorrectExecute(object sender)
        {
            ShowPopUpMapInCorrectCasName();
        }
        public void ShowPopUpMapInCorrectCasName()
        {
            ClearVisibilitySectionPopUp();
            IsIncorrectCasChemicalNameVisibility = Visibility.Visible;
            NotifyPropertyChanged("IsIncorrectCasChemicalNameVisibility");
            IsShowPopUp = true;
            NotifyPropertyChanged("IsShowPopUp");

        }

        private void CommandClosePopUpInCorrectCasNameExecute(object obj)
        {
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }

        private void CommandAddMoreInCorrectCasNameExecute(object obj)
        {
            SaveMapAddIncorrectCasChemicalName();
        }

        private List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> GetListColumnInCorrectCasChemicalName()
        {
            List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> listColumn = new List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn>();
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "ListQsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "Qsid" });

            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Chemical Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "250" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Language Name", ColBindingName = "LanguageName", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Active Lists Count", ColBindingName = "ActiveListsCount", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Cas", ContentName = "flag as valid CAS+Name", ColType = "Button", CommandParam = "FlagAsValid", BackgroundColor = "#71EFA3", ColumnWidth = "250" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Substance Identity", ColBindingName = "Cas", ContentName = "Go to Substance Identity for CAS", ColType = "Button", CommandParam = "GoToSubstanceIdentity", BackgroundColor = "#6E85B2", ColumnWidth = "250" });
            return listColumn;
        }
        private List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> GetListColumnHistoryCasChemicalName()
        {
            List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> listColumn = new List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn>();


            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Chemical Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "250" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "User Name", ColBindingName = "UserName", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Timestamps", ColBindingName = "Timestamps", ColType = "Textbox", ColumnWidth = "250" });
            return listColumn;
        }
        private bool CommandAddInCorrectCasNameCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(IncorrectCombination_ChemicalName) || string.IsNullOrWhiteSpace(IncorrectCombination_ChemicalName) || string.IsNullOrEmpty(IncorrectCombination_Cas) || string.IsNullOrWhiteSpace(IncorrectCombination_Cas))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void ClearMapIncorrectCasNameVariable()
        {
            IncorrectCombination_ChemicalName = string.Empty;
            IncorrectCombination_Cas = string.Empty;
            IncorrectCombination_Comments = string.Empty;
            NotifyPropertyChanged("IncorrectCombination_ChemicalName");
            NotifyPropertyChanged("IncorrectCombination_Cas");
            NotifyPropertyChanged("IncorrectCombination_Comments");
        }
        private void CommandAddInCorrectCasNameExecute(object obj)
        {
            SaveMapAddIncorrectCasChemicalName();
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }

        public void SaveMapAddIncorrectCasChemicalName()
        {
            Task.Run(() =>
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    var response = _objCasAssignment_Service.CreateMapIncorrectCasChemicalNameWithComments(IncorrectCombination_ChemicalName, IncorrectCombination_Cas, IncorrectCombination_Comments, Engine.CurrentUserSessionID);
                    ClearMapIncorrectCasNameVariable();
                    if (response.Status == true)
                    {
                        _notifier.ShowSuccess(response.Description);
                    }
                    else { _notifier.ShowError(response.Description); }
                    GetListMapIncorrectChemicalName();
                    GetLogMapIncorrectChemicalName();
                });
            });
        }

        private void CallBackMapIncorrectCasChemicalNameExecute(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "ShowDetail")
            {
                ActiveListsQsidDetail = new ObservableCollection<string>(_objCasAssignment_Service.GetActiveListQsid(SelectedMapIncorrectCasChemicalName.ChemicalNameId, SelectedMapIncorrectCasChemicalName.CasID));
                ActiveListsQsidVisibility = true;
                NotifyPropertyChanged("ActiveListsQsidDetail");
                NotifyPropertyChanged("ActiveListsQsidVisibility");
            }
            else if (obj.Notification == "FlagAsValid")
            {
                ClearVisibilitySectionPopUp();
                MarkValidCorrectVisibility = Visibility.Visible;
                NotifyPropertyChanged("MarkValidCorrectVisibility");
                ClearMapIncorrectCasNameVariable();
                IsShowPopUp = true;
                NotifyPropertyChanged("IsShowPopUp");
                GetListMapIncorrectChemicalName();
                GetLogMapIncorrectChemicalName();
            }
            else if (obj.Notification == "GoToSubstanceIdentity")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedMapIncorrectCasChemicalName.Cas, "ActiveSubstanceIdentityWithCas"), typeof(GroupThresholdUC_VM));
                //MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedMapIncorrectCasChemicalName.Cas, "SearchCasInSubstanceIdentityTab"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
        private void CallBackRefreshGridExecute(NotificationMessageAction<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> obj)
        {
            GetListMapIncorrectChemicalName();
        }
        private void CallBackRefreshGridExecute(NotificationMessageAction<Log_Map_Incorrect_Cas_ChemicalName_VM> obj)
        {
            GetLogMapIncorrectChemicalName();
        }
        public void ClearVisibilitySectionPopUp()
        {
            IsIncorrectCasChemicalNameVisibility = Visibility.Collapsed;
            MarkValidCorrectVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("IsIncorrectCasChemicalNameVisibility");
            NotifyPropertyChanged("MarkValidCorrectVisibility");
        }
        private bool CommandMarkCorrectCasNameCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(IncorrectCombination_Comments) && !string.IsNullOrWhiteSpace(IncorrectCombination_Comments))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandMarkCorrectCasNameExecute(object obj)
        {
            var isMoveToValid = _objCasAssignment_Service.DeleteMap_Incorrect_Cas_ChemicalName((int)SelectedMapIncorrectCasChemicalName.ChemicalNameId, (int)SelectedMapIncorrectCasChemicalName.CasID, Engine.CurrentUserSessionID, IncorrectCombination_Comments);
            if (isMoveToValid)
            {
                _notifier.ShowSuccess("Chemical + Cas mark valid successfully!");
                IsShowPopUp = false;
                NotifyPropertyChanged("IsShowPopUp");
                GetListMapIncorrectChemicalName();
                GetLogMapIncorrectChemicalName();
            }
            else
            {
                _notifier.ShowError("Chemical + Cas mark valid failed due to internal server error!");

            }
        }

        private void NotifyMe(PropertyChangedMessage<VersionControlSystem.Model.ViewModel.Map_Incorrect_Cas_ChemicalName_VM> obj)
        {
            SelectedMapIncorrectCasChemicalName = obj.NewValue;
        }
        private void NotifyMe(PropertyChangedMessage<Log_Map_Incorrect_Cas_ChemicalName_VM> obj)
        {
            selectedItemHistoryIncorrect = obj.NewValue;
        }
        public void BindListComment(int chemcalID, int casID, string type)
        {
            var listcomments = _objLibraryFunction_Service.GetListCommentsMapIncorrectCasName(chemcalID, casID).Select(x => new VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM
            {
                Action = x.Action,
                CommentBy = x.CommentBy,
                CommentID = x.CommentID,
                Comments = x.Comments,
                Timestamp = x.Timestamp
            }).ToList();

            if (type == "History")
            {
                listHistoryIncorrectCasName_Comments = new ObservableCollection<VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM>(listcomments);
                NotifyPropertyChanged("listHistoryIncorrectCasName_Comments");
            }
            else
            {
                listIncorrectCasName_Comments = new ObservableCollection<VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM>(listcomments);
                NotifyPropertyChanged("listIncorrectCasName_Comments");
            }
        }

        private bool CommandAddAdditionalCommentCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(IncorrectCombination_AdditionalComments) && !string.IsNullOrWhiteSpace(IncorrectCombination_AdditionalComments))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandAddAdditionalCommentExecute(object obj)
        {
            if ((string)obj == "HistoryTab")
            {
                _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName(selectedItemHistoryIncorrect.ChemicalNameID, selectedItemHistoryIncorrect.CasID, Engine.CurrentUserSessionID, IncorrectCombination_AdditionalComments);
                BindListComment(selectedItemHistoryIncorrect.ChemicalNameID, selectedItemHistoryIncorrect.CasID, "Histroy");
            }
            else
            {
                _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName((int)SelectedMapIncorrectCasChemicalName.ChemicalNameId, (int)SelectedMapIncorrectCasChemicalName.CasID, Engine.CurrentUserSessionID, IncorrectCombination_AdditionalComments);
                BindListComment((int)SelectedMapIncorrectCasChemicalName.ChemicalNameId, (int)SelectedMapIncorrectCasChemicalName.CasID, "Map");
            }
            IncorrectCombination_AdditionalComments = string.Empty;
            NotifyPropertyChanged("IncorrectCombination_AdditionalComments");
        }

        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
        public void commandOpenCurrentDataViewNewAddListExecute(object obj)
        {
            visibiltiyMapIncorrectCasName = Visibility.Visible;
            NotifyPropertyChanged("visibiltiyMapIncorrectCasName");
            Task.Run(() =>
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    try
                    {

                        Engine.IsOpenCurrentDataViewFromVersionHis = true;
                        Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                        Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                        MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

                    }
                    catch (Exception ex)
                    {

                        _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);

                    }
                    visibiltiyMapIncorrectCasName = Visibility.Collapsed;
                    NotifyPropertyChanged("visibiltiyMapIncorrectCasName");
                });
            });
        }

        private void NotifyMe(string obj)
        {

        }
    }
}
