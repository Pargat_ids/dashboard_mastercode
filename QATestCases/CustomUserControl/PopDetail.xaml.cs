﻿using QATestCases.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QATestCases.CustomUserControl
{
    /// <summary>
    /// Interaction logic for PopDetail.xaml
    /// </summary>
    public partial class PopDetail : Window
    {
        public PopDetail(DataTable matchdt, DataTable unMatchdt)
        {
            InitializeComponent();
            var vm = new PopDetail_VM(this, matchdt, unMatchdt);
            DataContext = vm;
        }
    }
}
