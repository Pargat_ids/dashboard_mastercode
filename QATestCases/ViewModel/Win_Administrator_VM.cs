﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using QATestCases.Library;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using System;
using System.Collections.ObjectModel;

namespace QATestCases.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public static string CommonListConn;
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public List<EntityClass.Library_QSIDs> listQsids { get; set; }
        public List<ITestCaseType> listType { get; set; }
        public List<SelectListItem> listOperator { get; set; }
        SelectListItem _selectedOperator { get; set; }
        public SelectListItem SelectedOperator
        {
            get
            {
                return _selectedOperator;
            }
            set
            {
                if (value != null)
                {
                    _selectedOperator = value;
                    NotifyPropertyChanged("SelectedOperator");
                }
            }
        }
        public List<IRunQsid> listQsidsAll { get; set; }
        public List<IlibTestCase> listTestCasesByQsid { get; set; }
        public ICommand CommandHyperlinkClick { get; private set; }
        public ICommand commandCloseTestCaseDetail { get; set; }
        public ICommand CommandAddCriteria { get; set; }
        public ObservableCollection<GenericCriteria_VM> listGenericCriteria { get; set; } = new ObservableCollection<GenericCriteria_VM>();
        public Win_Administrator_VM()
        {
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            comonLibTestCaseMap_VM = new CommonDataGrid_ViewModel<MapLibTestCaseMap>(GetListGridColumnLibTestCaseMap(), "Map TestCase", "Map_TestCase");
            comonLibRunTestCase_VM = new CommonDataGrid_ViewModel<MapRunTestCase>(GetListGridColumnRunTestCase(), "Run TestCase", "Run_TestCase");
            CommandShowPopUpAddLibTestCaseMap = new RelayCommand(CommandShowPopUpAddLibTestCaseMapExecute);
            CommandClosePopUpLibTestCaseMap = new RelayCommand(CommandClosePopUpExecuteLibTestCaseMap);
            CommandAddLibTestCaseMap = new RelayCommand(CommandAddLibTestCaseMapExecute, CommandAddLibTestCaseMapCanExecute);
            CommandAddMoreLibTestCaseMap = new RelayCommand(CommandAddMoreLibTestCaseMapExecute, CommandAddLibTestCaseMapCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<MapLibTestCaseMap>>(this, NotifyMeLibTestCaseMap);
            MessengerInstance.Register<PropertyChangedMessage<MapRunTestCase>>(this, NotifyMeLibTestCaseRun);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibTestCaseMap), CommandButtonExecutedLibTestCaseMap);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapRunTestCase), CommandButtonExecutedSeeDetailTestCase);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibTestCaseMap), RefreshGridLibTestCaseMap);
            CommandHyperlinkClick = new RelayCommand(CommandHyperlinkClickExecuted);
            commandCloseTestCaseDetail = new RelayCommand(commandCloseTestCaseDetailExecute);
            CommandAddCriteria = new RelayCommand(CommandAddCriteriaExecute);
            CommandDeleteCriteria = new RelayCommand(CommandDeleteCriteriaExecute);
            BtnRunShowResult = new RelayCommand(BtnRunShowResultExecute);
            commandSelectedChanges = new RelayCommand(commandSelectedChangesExecute);
            CommandAddWorkOrder = new RelayCommand(CommandAddWorkOrderExecute);
            CommandAddCSCTicket = new RelayCommand(CommandAddCSCTicketExecute);
            CommandDeleteWorkOrder = new RelayCommand(CommandDeleteWorkOrderExecute);
            CommandDeleteCSCTicket = new RelayCommand(CommandDeleteCSCTicketExecute);
            BindAllLibraryQsid();
            BindAllTestcaseTypes();
            BindOperator();
            BindConditions();
        }
        private void CommandDeleteCriteriaExecute(object obj)
        {
            int crietr = (int)obj;
            DeleteItemInCriteriaList(crietr);
        }
        public ObservableCollection<SelectListItem> listCriteria { get; set; }
        //public ObservableCollection<SelectListItem> listAdditionalCheck { get; set; }
        public ObservableCollection<SelectListItem> listCondition { get; set; }
        public ObservableCollection<LibraryFieldName_Generic> listFieldNames { get; set; }

        public void BindOperator()
        {
            var listCrit = new List<SelectListItem>();
            listCrit.Add(new SelectListItem() { Text = ">" });
            listCrit.Add(new SelectListItem() { Text = ">=" });
            listCrit.Add(new SelectListItem() { Text = "<" });
            listCrit.Add(new SelectListItem() { Text = "<=" });
            listCrit.Add(new SelectListItem() { Text = "=" });
            listOperator = listCrit;
            NotifyPropertyChanged("listOperator");
        }
        public void BindCriteriaCombo()
        {
            var listCrit = new List<SelectListItem>();
            listCrit.Add(new SelectListItem() { Text = "Equal" });
            listCrit.Add(new SelectListItem() { Text = "Contains" });
            listCrit.Add(new SelectListItem() { Text = "Begins with" });
            listCrit.Add(new SelectListItem() { Text = "Ends with" });
            listCrit.Add(new SelectListItem() { Text = "Greater than" });
            listCrit.Add(new SelectListItem() { Text = "Greater than equal to" });
            listCrit.Add(new SelectListItem() { Text = "Less than" });
            listCrit.Add(new SelectListItem() { Text = "Less than equal to" });
            listCriteria = new ObservableCollection<SelectListItem>(listCrit);
            NotifyPropertyChanged("listCriteria");
        }
        public void BindConditionCombo()
        {
            var listCond = new List<SelectListItem>();
            listCond.Add(new SelectListItem() { Text = "And" });
            listCond.Add(new SelectListItem() { Text = "Or" });
            listCondition = new ObservableCollection<SelectListItem>(listCond);
            NotifyPropertyChanged("listCondition");
        }
        public void ExistingRec()
        {
            BindCriteriaCombo();
            BindConditionCombo();
            listFieldNames = new ObservableCollection<LibraryFieldName_Generic>(_objLibraryFunction_Service.GetAllNormalizedFieldName(SelectedLibTestCaseMap.QSID_ID));
            using (var context = new CRAModel())
            {
                var existingRec = context.Library_TestCases_Detail.AsNoTracking().Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).OrderBy(y => y.TestCaseID).ToList();
                listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();

                existingRec.ForEach(x =>
                {
                    var cId = (int)x.DetailID;
                    if (listGenericCriteria.Where(xx => xx.CriteriaNumber == cId).Count() == 0)
                    {
                        listGenericCriteria.Add(new GenericCriteria_VM()
                        {
                            CriteriaNumber = cId,
                            CriteriaType = listCriteria.Where(xx => xx.Text == x.SelectCriteria).Select(y => y.Text).FirstOrDefault(),
                            CriteriaValue = x.CriteriaValue,
                            FieldNameID = listFieldNames.Where(xx => xx.FieldNameID == x.FieldNameID).Select(Y => Y.FieldNameID).FirstOrDefault(),
                            GroupCriteriaNo = x.GroupingCriteria,
                            NextCondition = listCondition.Where(xx => xx.Text == x.NextCondition).Select(Y => Y.Text).FirstOrDefault(),
                            SelectFieldName = listFieldNames.Where(xx => xx.FieldNameID == x.FieldNameID).FirstOrDefault(),
                            SelectedCriteria_Generics = listCriteria.Where(xx => xx.Text == x.SelectCriteria).FirstOrDefault(),
                            SelectedNextCondition_Generics = listCondition.Where(xx => xx.Text == x.NextCondition).FirstOrDefault(),
                        });
                    }
                });
                NotifyPropertyChanged("listGenericCriteria");
            }
        }
        public void CommandAddCriteriaExecute(object obj)
        {
            if (SelectedQsid != null)
            {
                var flds = _objLibraryFunction_Service.GetAllNormalizedFieldName(SelectedQsid.QSID_ID);
                var mxTestID = mxIDs;
                var lstMaxCreteria = listGenericCriteria.Select(y => y.CriteriaNumber).OrderByDescending(z => z).FirstOrDefault();
                if (lstMaxCreteria >= mxTestID)
                {
                    mxIDs += 1;
                    mxTestID = mxIDs;
                }
                listGenericCriteria.Add(new GenericCriteria_VM()
                {
                    CriteriaNumber = mxTestID,
                });
                NotifyPropertyChanged("listGenericCriteria");
            }
        }
        public IPopup QsidDetailInfo { get; set; }
        public void CommandHyperlinkClickExecuted(object obj)
        {
            if (obj != null)
            {
                var tid = Convert.ToInt32(obj);
                var queryCombine = CombineTestCaseDet(tid);
                QsidDetailInfo = new IPopup { Condition = queryCombine[0] };
                NotifyPropertyChanged("QsidDetailInfo");
                QsidInfoPopUpIsOpen = true;
                NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            }
        }
        private string[] CombineTestCaseDet(Int32 tid)
        {
            string queryCombine = string.Empty;
            string OppqueryCombine = string.Empty;
            using (var context = new CRAModel())
            {
                var chklst = (from d1 in context.Library_TestCases_Detail.AsNoTracking().Where(x => x.TestCaseID == tid).ToList()
                              join d2 in context.Library_FieldNames.AsNoTracking()
                              on d1.FieldNameID equals d2.FieldNameID
                              select new
                              {
                                  d1.DetailID,
                                  d1.TestCaseID,
                                  d2.FieldName,
                                  d1.SelectCriteria,
                                  d1.CriteriaValue,
                                  d1.NextCondition,
                                  d1.GroupingCriteria,
                              }).OrderBy(y => y.DetailID).ToList();
                for (int i = 0; i < chklst.Count; i++)
                {
                    var condition = string.Empty;
                    var Oppcondition = string.Empty;
                    switch (chklst[i].SelectCriteria)
                    {
                        case "Equal":
                            condition = " like '" + chklst[i].CriteriaValue + "'";
                            Oppcondition = " not like '" + chklst[i].CriteriaValue + "'";
                            break;
                        case "Contains":
                            condition = " Like '%" + chklst[i].CriteriaValue + "%'";
                            Oppcondition = " not Like '%" + chklst[i].CriteriaValue + "%'";
                            break;
                        case "Begins with":
                            condition = " Like '" + chklst[i].CriteriaValue + "%'";
                            Oppcondition = " not Like '" + chklst[i].CriteriaValue + "%'";
                            break;
                        case "Ends with":
                            condition = " Like '%" + chklst[i].CriteriaValue + "'";
                            Oppcondition = " not Like '%" + chklst[i].CriteriaValue + "'";
                            break;
                        case "Greater than":
                            condition = " > " + chklst[i].CriteriaValue;
                            Oppcondition = " < " + chklst[i].CriteriaValue;
                            break;
                        case "Greater than equal to":
                            condition = " >= " + chklst[i].CriteriaValue;
                            Oppcondition = " <= " + chklst[i].CriteriaValue;
                            break;
                        case "Less than":
                            condition = " < " + chklst[i].CriteriaValue;
                            Oppcondition = " > " + chklst[i].CriteriaValue;
                            break;
                        case "Less than equal to":
                            condition = " <= " + chklst[i].CriteriaValue;
                            Oppcondition = " >= " + chklst[i].CriteriaValue;
                            break;
                    }
                    if (i == 0)
                    {
                        queryCombine += "(";
                        OppqueryCombine += "(";
                    }
                    else
                    {
                        if (chklst[i - 1].GroupingCriteria == chklst[i].GroupingCriteria && chklst[i].GroupingCriteria != null)
                        {
                            queryCombine += " " + chklst[i].NextCondition + " ";
                            OppqueryCombine += " " + (chklst[i].NextCondition == "Or" ? "And" : chklst[i].NextCondition) + " ";
                        }
                        else
                        {
                            queryCombine += ") " + chklst[i - 1].NextCondition + " (";
                            OppqueryCombine += ") " + (chklst[i - 1].NextCondition == "Or" ? "And" : chklst[i - 1].NextCondition) + " (";
                        }
                    }
                    //queryCombine += chklst[i].FieldName + " " + chklst[i].SelectCriteria + " " + chklst[i].CriteriaValue;
                    queryCombine += chklst[i].FieldName + condition;
                    OppqueryCombine += chklst[i].FieldName + Oppcondition;
                }
                queryCombine += ")";
                OppqueryCombine += ")";
            }
            return new string[] { queryCombine, OppqueryCombine };
        }
        public void commandCloseTestCaseDetailExecute(object obj)
        {
            QsidInfoPopUpIsOpen = false;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
        }

        private void CallbackExecute(object obj)
        {

        }
        public int Qsid_ID { get; set; }
        public int FieldNameID { get; set; }

        Library_QSIDs _selectedQsid { get; set; }
        public Library_QSIDs SelectedQsid
        {
            get
            {
                return _selectedQsid;
            }
            set
            {
                if (value != null)
                {
                    _selectedQsid = value;
                    NotifyPropertyChanged("SelectedQsid");
                    listFieldNames = new ObservableCollection<LibraryFieldName_Generic>(_objLibraryFunction_Service.GetAllNormalizedFieldName(SelectedQsid.QSID_ID));
                    NotifyPropertyChanged("listFieldNames");
                }
            }
        }

        ITestCaseType _selectedType { get; set; }
        public ITestCaseType SelectedType
        {
            get
            {
                return _selectedType;
            }
            set
            {
                if (value != null)
                {
                    _selectedType = value;
                    NotifyPropertyChanged("SelectedType");
                }
            }
        }

        IRunQsid _SelectedQsidAll { get; set; }
        public IRunQsid SelectedQsidAll
        {
            get
            {
                return _SelectedQsidAll;
            }
            set
            {
                if (value != null)
                {
                    _SelectedQsidAll = value;
                    NotifyPropertyChanged("SelectedQsidAll");
                    GetAllTestCases((int)_SelectedQsidAll.QSID_ID);
                }

            }
        }

        public bool ChkIsSelected { get; set; } = true;

        ICondition _SelectedCondition { get; set; }
        public ICondition SelectedCondition
        {
            get
            {
                return _SelectedCondition;
            }
            set
            {
                if (value != null)
                {
                    _SelectedCondition = value;
                    NotifyPropertyChanged("SelectedCondition");
                }

            }
        }

        public LibraryFieldName_Generic SelectFieldName
        {
            get
            {
                return _selectFieldNames;
            }
            set
            {
                if (value != null)
                {
                    _selectFieldNames = value;
                    FieldNameID = _selectFieldNames.FieldNameID;
                    NotifyPropertyChanged("SelectFieldName");
                }
            }
        }
        LibraryFieldName_Generic _selectFieldNames { get; set; }

        public ObservableCollection<ICondition> listConditions { get; set; } = new ObservableCollection<ICondition>();

        private void GetAllTestCases(int qsid_Id)
        {
            using (var context = new CRAModel())
            {
                listTestCasesByQsid = context.Library_TestCases.AsNoTracking().Where(x => x.QSID_ID == qsid_Id
                && x.IsActive == true).Select(y => new IlibTestCase
                {
                    IsSelected = false,
                    IsActive = y.IsActive,
                    Description = y.Description,
                    QSID_ID = y.QSID_ID,
                    TestCaseID = y.TestCaseID,
                    TestCaseTypeID = y.TestCaseTypeID,
                    ExpectedCountMatch = (int)y.ExpectedCountMatch,
                    ExpectedCountOperator = y.ExpectedCountOperator,
                }).ToList();

                NotifyPropertyChanged("listTestCasesByQsid");
            }
        }

        public void BindAllLibraryQsid()
        {
            listQsids = _objLibraryFunction_Service.GetLibrary_QsidList();
            NotifyPropertyChanged("listQsids");
        }
        public void BindAllTestcaseTypes()
        {
            var result = new List<ITestCaseType>();
            result.Add(new ITestCaseType { TypeID = 1, TypeName = "abc" });
            listType = result;
            NotifyPropertyChanged("listType");
        }
        public void BindQsidForRunTestCases()
        {
            using (var context = new CRAModel())
            {
                listQsidsAll = (from d1 in context.Library_TestCases.AsNoTracking().Select(y => y.QSID_ID).Distinct().ToList()
                                join d2 in context.Library_QSIDs.AsNoTracking()
                                on d1 equals d2.QSID_ID
                                select new IRunQsid
                                {
                                    QSID_ID = d2.QSID_ID,
                                    QSID = d2.QSID,
                                }).ToList();
                NotifyPropertyChanged("listQsidsAll");
                listTestCasesByQsid = new List<IlibTestCase>();
                NotifyPropertyChanged("listTestCasesByQsid");
            }
        }
        public void BindConditions()
        {
            listConditions.Add(new ICondition { Condition = "Less than(<)" });
            listConditions.Add(new ICondition { Condition = "Less than Equal to(<=)" });
            listConditions.Add(new ICondition { Condition = "greater than Equal to(>=)" });
            listConditions.Add(new ICondition { Condition = "greater than(>)" });
            listConditions.Add(new ICondition { Condition = "Equal To(>)" });
            listConditions.Add(new ICondition { Condition = "Start with" });
            listConditions.Add(new ICondition { Condition = "End with" });
            listConditions.Add(new ICondition { Condition = "Contains" });
            listConditions.Add(new ICondition { Condition = "Total Counts equal to" });
            NotifyPropertyChanged("listConditions");
        }
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }
        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {
                case "Map New TestCase":
                    RunLibTestCaseMap();
                    break;
                case "Run TestCase":
                    BindQsidForRunTestCases();
                    break;
            }
        }
    }
    public class ICondition
    {
        public string Condition { get; set; }
    }
    public class IRunQsid
    {
        public Int32 QSID_ID { get; set; }
        public string QSID { get; set; }
    }
    public class SelectListItem
    {
        public string Text { get; set; }
    }
    public class IPopup
    {
        public string Condition { get; set; }
    }
    public class ITestCaseType
    {
        public Int32 TypeID { get; set; }
        public string TypeName { get; set; }
    }
}