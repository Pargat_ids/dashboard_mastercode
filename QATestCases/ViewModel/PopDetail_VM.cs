﻿using QATestCases.CustomUserControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QATestCases.ViewModel
{
    public class PopDetail_VM
    {
        public StandardDataGrid_VM listMatched { get; set; }
        public StandardDataGrid_VM listUnMatched { get; set; }
        public PopDetail_VM(PopDetail _popWindow, DataTable matchdt, DataTable unMatchdt)
        {
            listMatched = new StandardDataGrid_VM("Match Records", matchdt, Visibility.Visible);
            listUnMatched = new StandardDataGrid_VM("Match Records", unMatchdt, Visibility.Visible);
        }
    }
}
