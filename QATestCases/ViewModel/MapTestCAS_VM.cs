﻿using System.Windows.Input;
using System.Windows.Controls;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using QATestCases.CustomUserControl;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using EntityFramework.Utilities;

namespace QATestCases.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand CommandAddWorkOrder { get; private set; }
        public ICommand CommandAddCSCTicket { get; private set; }
        public ICommand CommandDeleteWorkOrder { get; private set; }
        public ICommand CommandDeleteCSCTicket { get; private set; }
        public CommonDataGrid_ViewModel<MapLibTestCaseMap> comonLibTestCaseMap_VM { get; set; }
        public Visibility VisibilityLibTestCaseMapDashboard_Loader { get; set; }
        public ICommand CommandClosePopUpLibTestCaseMap { get; private set; }
        public ICommand CommandShowPopUpAddLibTestCaseMap { get; private set; }
        public bool IsEditLibTestCaseMap { get; set; } = true;
        public Visibility PopUpContentAddLibTestCaseMapVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<MapLibTestCaseMap> _lstLibTestCaseMap { get; set; } = new ObservableCollection<MapLibTestCaseMap>();
        private bool _IsShowPopUpLibTestCaseMap { get; set; }
        public ICommand CommandAddLibTestCaseMap { get; private set; }
        public ICommand CommandAddMoreLibTestCaseMap { get; private set; }
        private MapLibTestCaseMap _SelectedLibTestCaseMap { get; set; }
        public string newTestCaseMapDesc { get; set; } = string.Empty;
        public Int32? newExpectedCountMatch { get; set; } = 0;
        private DataGridCellInfo _cellInfo_AddNewTestCaseMap { get; set; }
        public ObservableCollection<MapLibTestCaseMap> lstLibTestCaseMap
        {
            get { return _lstLibTestCaseMap; }
            set
            {
                if (_lstLibTestCaseMap != value)
                {
                    _lstLibTestCaseMap = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibTestCaseMap>>>(new PropertyChangedMessage<List<MapLibTestCaseMap>>(null, _lstLibTestCaseMap.ToList(), "Default List"));
                }
            }
        }
        private void BindLibTestCaseMap()
        {
            VisibilityLibTestCaseMapDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibTestCaseMapDashboard_Loader");
            Task.Run(() =>
            {
                List<MapLibTestCaseMap> lstFinal = new List<MapLibTestCaseMap>();
                var SAPLibTestCaseMapTable = objCommon.DbaseQueryReturnTableSql(
                "select a.TestCaseID,a.TestCaseTypeID, 'abc' as TestCaseType, a.Description, a.IsActive, a.QSID_ID, e.QSID, a.ExpectedCountMatch, a.ExpectedCountOperator from QA.Library_TestCases a left join Library_QSIDs e on a.Qsid_id = e.Qsid_id order by TestCaseID desc", Win_Administrator_VM.CommonListConn);
                using (var context = new CRAModel())
                {
                    var totTestCasIDs = (from d1 in context.Log_Library_TestCases.AsNoTracking().Where(x => x.Status == "A").OrderBy(z => z.TestCaseID).GroupBy(y => y.TestCaseID).Select(Y => Y.FirstOrDefault()).ToList()
                                         join d2 in context.Log_Sessions.AsNoTracking()
                                         on d1.SessionID equals d2.SessionID
                                         join d3 in context.Library_Users.AsNoTracking()
                                         on d2.UserName equals d3.VeriskID
                                         select new { d1.TestCaseID, d2.SessionID, UserName = d3.FirstName + " " + d3.LastName, d1.TimeStamp }).ToList();

                    var totTestCasIDChgs = (from d1 in context.Log_Library_TestCases.AsNoTracking().Where(x => x.Status == "C").OrderByDescending(z => z.TestCaseID).GroupBy(y => y.TestCaseID).Select(Y => Y.FirstOrDefault()).ToList()
                                            join d2 in context.Log_Sessions.AsNoTracking()
                                            on d1.SessionID equals d2.SessionID
                                            join d3 in context.Library_Users.AsNoTracking()
                                            on d2.UserName equals d3.VeriskID
                                            select new { d1.TestCaseID, d2.SessionID, UserName = d3.FirstName + " " + d3.LastName, d1.TimeStamp }).ToList();

                    SAPLibTestCaseMapTable.AsEnumerable().ToList().ForEach(x =>
                    {
                        var tId = Convert.ToInt32(x.Field<Int32>("TestCaseID"));
                        var crdDet = totTestCasIDs.Where(xx => xx.TestCaseID == tId);
                        var deactBy = String.Empty;
                        DateTime? deactTime = null;
                        if (x.Field<bool>("ISActive") == false)
                        {
                            var chgDet = totTestCasIDChgs.Where(xx => xx.TestCaseID == tId);
                            deactBy = chgDet.Select(y => y.UserName).FirstOrDefault();
                            deactTime = chgDet.Select(y => y.TimeStamp).FirstOrDefault();
                        }
                        lstFinal.Add(new MapLibTestCaseMap
                        {
                            QSID = x.Field<string>("QSID"),
                            QSID_ID = x.Field<Int32>("QSID_ID"),
                            TestCaseTypeID = x.Field<dynamic>("TestCaseTypeID")== null ? null : x.Field<Int32>("TestCaseTypeID"),
                            TestCaseType = x.Field<string>("TestCaseType"),
                            IsActive = x.Field<bool>("ISActive"),
                            Description = x.Field<string>("Description"),
                            CreatedBy = crdDet.Select(y => y.UserName).FirstOrDefault(),
                            CreatedTimeStamp = crdDet.Select(y => y.TimeStamp).FirstOrDefault(),
                            DeactivateBy = deactBy,
                            DeactivateTimeStamp = deactTime,
                            TestCaseID = tId,
                            ExpectedCountMatch = x.Field<Int32?>("ExpectedCountMatch"),
                            ExpectedCountOperator = x.Field<string>("ExpectedCountOperator"),
                        });
                    });

                }
                lstLibTestCaseMap = new ObservableCollection<MapLibTestCaseMap>(lstFinal);
            });
            NotifyPropertyChanged("comonLibTestCaseMap_VM");
            VisibilityLibTestCaseMapDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibTestCaseMapDashboard_Loader");
        }
        public bool IsShowPopUpLibTestCaseMap
        {
            get { return _IsShowPopUpLibTestCaseMap; }
            set
            {
                if (_IsShowPopUpLibTestCaseMap != value)
                {
                    _IsShowPopUpLibTestCaseMap = value;
                    NotifyPropertyChanged("IsShowPopUpLibTestCaseMap");
                }
            }
        }
        private void RunLibTestCaseMap()
        {
            BindLibTestCaseMap();
        }
        private void RefreshGridLibTestCaseMap(PropertyChangedMessage<string> flag)
        {
            BindLibTestCaseMap();
            if (IsShowPopUpLibTestCaseMap)
            {
                NotifyPropertyChanged("IsShowPopUpLibTestCaseMap");
            }
        }
        private void CommandAddLibTestCaseMapExecute(object obj)
        {
            SaveLibTestCaseMap();
            IsShowPopUpLibTestCaseMap = false;
            IsEditLibTestCaseMap = true;
            NotifyPropertyChanged("IsEditLibTestCaseMap");
            ClearPopUpVariableLibTestCaseMap();
            PopUpContentAddLibTestCaseMapVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibTestCaseMapVisibility");
        }
        public bool CommandAddLibTestCaseMapCanExecute(object obj)
        {
            return true;
        }
        public void SaveLibTestCaseMap()
        {
            Library_TestCases obj = new Library_TestCases();
            List<Library_TestCases_Detail> lstAdd = new List<Library_TestCases_Detail>();
            List<Library_TestCases_Detail> lstUpd = new List<Library_TestCases_Detail>();
            List<Int32> lstDel = new List<Int32>();
            List<Log_Library_TestCases_Detail> lstLog = new List<Log_Library_TestCases_Detail>();
            using (var context = new CRAModel())
            {
                if (!IsEditLibTestCaseMap)
                {
                    obj = context.Library_TestCases.Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).FirstOrDefault();
                }
                else
                {
                    var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('QA.Library_TestCases')", Win_Administrator_VM.CommonListConn));
                    obj.TestCaseID = mx + 1;
                }
                obj.Description = newTestCaseMapDesc;
                obj.QSID_ID = SelectedQsid.QSID_ID;
                obj.IsActive = ChkIsSelected;
                obj.TestCaseTypeID = 1;
                obj.ExpectedCountMatch = newExpectedCountMatch;
                obj.ExpectedCountOperator = SelectedOperator.Text;

                if (!IsEditLibTestCaseMap)
                {
                    LogEntry(SelectedLibTestCaseMap.TestCaseID);
                    context.SaveChanges();
                    _notifier.ShowSuccess("Existing TestCase Saved Successfully.");
                }
                else
                {
                    var chkAlreadyExist = context.Library_TestCases.Count(x => x.Description.ToLower() == obj.Description.ToLower());
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("TestCaseMap already exists");
                    }
                    else
                    {
                        context.Library_TestCases.Add(obj);
                        context.SaveChanges();
                        LogEntry(obj.TestCaseID);
                        _notifier.ShowSuccess("New TestCase Saved Successfully.");
                    }
                }


                //Save Detail


                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var detId = listGenericCriteria[i].CriteriaNumber;
                    var chkExt = context.Library_TestCases_Detail.FirstOrDefault(x => x.DetailID == detId);
                    if (chkExt == null)
                    {
                        // New Entry
                        var lstF = new Library_TestCases_Detail
                        {
                            CriteriaValue = listGenericCriteria[i].CriteriaValue,
                            GroupingCriteria = listGenericCriteria[i].GroupCriteriaNo,
                            FieldNameID = listGenericCriteria[i].FieldNameID,
                            NextCondition = listGenericCriteria[i].NextCondition,
                            TestCaseID = obj.TestCaseID,
                            SelectCriteria = listGenericCriteria[i].CriteriaType,
                            DetailID = listGenericCriteria[i].CriteriaNumber,
                        };
                        context.Library_TestCases_Detail.Add(lstF);
                        context.SaveChanges();
                        var lstLogF = new Log_Library_TestCases_Detail
                        {
                            CriteriaValue = listGenericCriteria[i].CriteriaValue,
                            GroupingCriteria = listGenericCriteria[i].GroupCriteriaNo,
                            FieldNameID = listGenericCriteria[i].FieldNameID,
                            NextCondition = listGenericCriteria[i].NextCondition,
                            TestCaseID = obj.TestCaseID,
                            SelectCriteria = listGenericCriteria[i].CriteriaType,
                            DetailID = lstF.DetailID,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lstLogF);
                    }
                    else
                    {
                        if (listGenericCriteria[i].CriteriaValue != chkExt.CriteriaValue || listGenericCriteria[i].GroupCriteriaNo != chkExt.GroupingCriteria || listGenericCriteria[i].FieldNameID != chkExt.FieldNameID
                            || listGenericCriteria[i].NextCondition != chkExt.NextCondition || listGenericCriteria[i].CriteriaType != chkExt.SelectCriteria)
                        {

                            chkExt.CriteriaValue = listGenericCriteria[i].CriteriaValue;
                            chkExt.GroupingCriteria = listGenericCriteria[i].GroupCriteriaNo;
                            chkExt.FieldNameID = listGenericCriteria[i].FieldNameID;
                            chkExt.NextCondition = listGenericCriteria[i].NextCondition;
                            chkExt.TestCaseID = obj.TestCaseID;
                            chkExt.SelectCriteria = listGenericCriteria[i].CriteriaType;
                            chkExt.DetailID = listGenericCriteria[i].CriteriaNumber;
                            context.SaveChanges();

                            var lstLogF = new Log_Library_TestCases_Detail
                            {
                                CriteriaValue = listGenericCriteria[i].CriteriaValue,
                                GroupingCriteria = listGenericCriteria[i].GroupCriteriaNo,
                                FieldNameID = listGenericCriteria[i].FieldNameID,
                                NextCondition = listGenericCriteria[i].NextCondition,
                                TestCaseID = obj.TestCaseID,
                                SelectCriteria = listGenericCriteria[i].CriteriaType,
                                DetailID = chkExt.DetailID,
                                Status = "C",
                                SessionID = Engine.CurrentUserSessionID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lstLogF);
                        }
                    }
                }

                if (SelectedLibTestCaseMap != null)
                {
                    var DelRec = (from d1 in context.Library_TestCases_Detail.AsNoTracking().Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).ToList()
                                  join d2 in listGenericCriteria.ToList()
                                  on d1.DetailID equals d2.CriteriaNumber
                                  into tg
                                  from tcheck in tg.DefaultIfEmpty()
                                  where tcheck == null
                                  select (int)d1.DetailID).ToList();
                    DelRec.ForEach(x =>
                    {
                        var delExt = context.Library_TestCases_Detail.AsNoTracking().Where(xx => xx.DetailID == x).FirstOrDefault();
                        var lstLogF = new Log_Library_TestCases_Detail
                        {
                            CriteriaValue = delExt.CriteriaValue,
                            GroupingCriteria = delExt.GroupingCriteria == null ? null : delExt.GroupingCriteria,
                            FieldNameID = delExt.FieldNameID,
                            NextCondition = delExt.NextCondition,
                            TestCaseID = (int)delExt.TestCaseID,
                            SelectCriteria = delExt.SelectCriteria,
                            DetailID = delExt.DetailID,
                            Status = "D",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lstLogF);
                        lstDel.AddRange(DelRec);
                    });
                    //bulk del
                    if (lstDel.Any())
                    {
                        EFBatchOperation.For(context, context.Library_TestCases_Detail).Where(x => lstDel.Contains(x.DetailID)).Delete();
                    }
                }
                //log Bulk ins
                EFBatchOperation.For(context, context.Log_Library_TestCases_Detail).InsertAll(lstLog.Cast<Log_Library_TestCases_Detail>());
                context.SaveChanges();


                // save workorder
                List<Log_TestCase_WorkOrder> listWorkOrderVM = new List<Log_TestCase_WorkOrder>();
                listWorkOrderVM = listWorkOrder.Where(x => x.WorkOrderNumber != null).Select(y => new Log_TestCase_WorkOrder
                {
                    WorkOrderNumber = y.WorkOrderNumber,
                    TestCaseID = SelectedLibTestCaseMap.TestCaseID,
                }).ToList();

                var existingWorkOrderChanges = context.Log_TestCase_WorkOrder.Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).ToList();
                if (existingWorkOrderChanges != null && existingWorkOrderChanges.Count > 0)
                {
                    context.Log_TestCase_WorkOrder.RemoveRange(existingWorkOrderChanges);
                    context.SaveChanges();
                }
                EFBatchOperation.For(context, context.Log_TestCase_WorkOrder).InsertAll(listWorkOrderVM);

                //save csc Ticket
                List<Log_TestCase_CSCTicket> listCSCVM = new List<Log_TestCase_CSCTicket>();
                listCSCVM = listCSCTicket.Where(x => x.CSCTicket_Number != null).Select(y => new Log_TestCase_CSCTicket
                {
                    CSCTicket_Number = y.CSCTicket_Number,
                    CSCTicket_Url = y.CSCTicket_Url,
                    TestCaseID = SelectedLibTestCaseMap.TestCaseID,
                }).ToList();

                var existingCSCChanges = context.Log_TestCase_CSCTicket.Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).ToList();
                if (existingCSCChanges != null && existingCSCChanges.Count > 0)
                {
                    context.Log_TestCase_CSCTicket.RemoveRange(existingCSCChanges);
                    context.SaveChanges();
                }
                EFBatchOperation.For(context, context.Log_TestCase_CSCTicket).InsertAll(listCSCVM);
            }




            ClearPopUpVariableLibTestCaseMap();
            BindLibTestCaseMap();
        }
        public void DeleteItemInCriteriaList(int criteriaNo)
        {
            var delCri = listGenericCriteria.Where(x => x.CriteriaNumber == criteriaNo).FirstOrDefault();
            listGenericCriteria.Remove(delCri);
            NotifyPropertyChanged("listGenericCriteria");

        }
        public ICommand CommandDeleteCriteria { get; set; }
        private void LogEntry(Int32 tID)
        {
            using (var context = new CRAModel())
            {
                var Olog = new Log_Library_TestCases();
                var objLog = context.Library_TestCases.AsNoTracking().Where(x => x.TestCaseID == tID).FirstOrDefault();
                Olog.Description = objLog.Description;
                Olog.QSID_ID = objLog.QSID_ID;
                Olog.TestCaseTypeID = objLog.TestCaseTypeID;
                Olog.IsActive = objLog.IsActive;
                Olog.TimeStamp = objCommon.ESTTime();
                Olog.SessionID = Engine.CurrentUserSessionID;
                Olog.TestCaseID = objLog.TestCaseID;
                Olog.Status = !IsEditLibTestCaseMap ? "C" : "A";
                Olog.ExpectedCountMatch = objLog.ExpectedCountMatch;
                Olog.ExpectedCountOperator = objLog.ExpectedCountOperator;
                context.Log_Library_TestCases.Add(Olog);
                context.SaveChanges();
            }
        }
        private void CommandAddMoreLibTestCaseMapExecute(object obj)
        {
            SaveLibTestCaseMap();
        }
        private void CommandClosePopUpExecuteLibTestCaseMap(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpLibTestCaseMap = false;
            listGenericCriteria = new ObservableCollection<GenericCriteria_VM>(listGenericCriteria.Where(x => x.FieldNameID > 0).ToList());
            NotifyPropertyChanged("listGenericCriteria");
        }
        private void CommandShowPopUpAddLibTestCaseMapExecute(object obj)
        {
            BindCriteriaCombo();
            BindConditionCombo();
            ClearPopUpVariableLibTestCaseMap();
            IsEditLibTestCaseMap = true;
            NotifyPropertyChanged("IsEditLibTestCaseMap");
            IsShowPopUpLibTestCaseMap = true;
            PopUpContentAddLibTestCaseMapVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibTestCaseMapVisibility");
        }
        public MapLibTestCaseMap SelectedLibTestCaseMap
        {
            get => (MapLibTestCaseMap)_SelectedLibTestCaseMap;
            set
            {
                if (Equals(_SelectedLibTestCaseMap, value))
                {
                    return;
                }
                _SelectedLibTestCaseMap = value;
            }
        }
        private void NotifyMeLibTestCaseMap(PropertyChangedMessage<MapLibTestCaseMap> obj)
        {
            SelectedLibTestCaseMap = obj.NewValue;
        }
        public void ClearPopUpVariableLibTestCaseMap()
        {
            newTestCaseMapDesc = "";
            NotifyPropertyChanged("newTestCaseMapDesc");
            newExpectedCountMatch = 0;
            NotifyPropertyChanged("newExpectedCountMatch");
        }
        public Int32 mxIDs { get; set; }
        private void CommandButtonExecutedLibTestCaseMap(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "EditTestCases")
            {
                IsEditLibTestCaseMap = false;
                NotifyPropertyChanged("IsEditLibTestCaseMap");
                IsShowPopUpLibTestCaseMap = true;
                NotifyPropertyChanged("IsShowPopUpLibTestCaseMap");
                PopUpContentAddLibTestCaseMapVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddLibTestCaseMapVisibility");
                newTestCaseMapDesc = SelectedLibTestCaseMap.Description;
                NotifyPropertyChanged("newTestCaseMapDesc");
                newExpectedCountMatch = SelectedLibTestCaseMap.ExpectedCountMatch;
                NotifyPropertyChanged("newExpectedCountMatch");
                SelectedQsid = listQsids.Where(x => x.QSID_ID == SelectedLibTestCaseMap.QSID_ID).FirstOrDefault();
                NotifyPropertyChanged("SelectedQsid");
                SelectedType = listType.Where(x => x.TypeID == SelectedLibTestCaseMap.TestCaseTypeID).FirstOrDefault();
                NotifyPropertyChanged("SelectedType");
                SelectedOperator = listOperator.Where(x => x.Text == SelectedLibTestCaseMap.ExpectedCountOperator).FirstOrDefault();
                NotifyPropertyChanged("SelectedOperator");
                ChkIsSelected = SelectedLibTestCaseMap.IsActive == null ? false : true;
                NotifyPropertyChanged("ChkIsSelected");
                ExistingRec();
                using (var context = new CRAModel())
                {
                    mxIDs = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('QA.Library_TestCases_Detail')", Win_Administrator_VM.CommonListConn));
                    mxIDs += 1;
                    var wo = context.Log_TestCase_WorkOrder.AsNoTracking().Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).ToList();
                    listWorkOrder = new ObservableCollection<Log_TestCase_WorkOrder>(wo.OrderByDescending(x => x.Log_WorkOrderID));
                    NotifyPropertyChanged("listWorkOrder");
                    var csTick = context.Log_TestCase_CSCTicket.AsNoTracking().Where(x => x.TestCaseID == SelectedLibTestCaseMap.TestCaseID).ToList();
                    listCSCTicket = new ObservableCollection<Log_TestCase_CSCTicket>(csTick.OrderByDescending(x => x.Log_CSCTicketID));
                    NotifyPropertyChanged("listCSCTicket");
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnLibTestCaseMap()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Description", ColBindingName = "Description", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "TestCaseType", ColType = "Textbox"});
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsActive", ColBindingName = "IsActive", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Expected Count Operator", ColBindingName = "ExpectedCountOperator", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Expected Count Match", ColBindingName = "ExpectedCountMatch", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CreatedBy", ColBindingName = "CreatedBy", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Created TimeStamp", ColBindingName = "CreatedTimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DeactivateBy", ColBindingName = "DeactivateBy", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Deactivate TimeStamp", ColBindingName = "DeactivateTimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "View/Edit", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "View/Edit", BackgroundColor = "#07689f", CommandParam = "EditTestCases" });
            return listColumnQsidDetail;
        }
        public DataGridCellInfo CellInfo_AddNewTestCaseMap
        {
            get { return _cellInfo_AddNewTestCaseMap; }
            set
            {
                _cellInfo_AddNewTestCaseMap = value;
            }
        }
        private void UpdateSourceTriggerCurrentCellTestCaseMap(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "MapLibTestCaseMap")
            {
                CellInfo_AddNewTestCaseMap = obj.NewValue;
            }
        }

        public void BindWorkOrderList()
        {
            if (listWorkOrder.Count == 0)
            {
                listWorkOrder.Add(new Log_TestCase_WorkOrder { WorkOrderNumber = null, Log_WorkOrderID= listWorkOrder.Count() });
            }
            listWorkOrder = new ObservableCollection<Log_TestCase_WorkOrder>(listWorkOrder.OrderByDescending(x => x.Log_WorkOrderID));
            NotifyPropertyChanged("listWorkOrder");
        }
        public void BindCSCTicketList()
        {
            if (listCSCTicket.Count == 0)
            {
                listCSCTicket.Add(new Log_TestCase_CSCTicket { CSCTicket_Number = null, Log_CSCTicketID = listCSCTicket.Count() });
            }
            listCSCTicket = new ObservableCollection<Log_TestCase_CSCTicket>(listCSCTicket.OrderByDescending(x => x.Log_CSCTicketID));
            NotifyPropertyChanged("listCSCTicket");
        }
        private void CommandDeleteWorkOrderExecute(object obj)
        {
            if (obj != null)
            {
                int? woNum = (int?)obj;
                listWorkOrder = new ObservableCollection<Log_TestCase_WorkOrder>(listWorkOrder.Where(x => x.WorkOrderNumber != woNum).ToList());
                BindWorkOrderList();
            }
        }
        private void CommandDeleteCSCTicketExecute(object obj)
        {
            if (obj != null)
            {
                string tNum = obj.ToString();
                listCSCTicket = new ObservableCollection<Log_TestCase_CSCTicket>(listCSCTicket.Where(x => x.CSCTicket_Number != tNum).ToList());
                BindCSCTicketList();
            }
        }
        public ObservableCollection<Log_TestCase_WorkOrder> listWorkOrder { get; set; } = new ObservableCollection<Log_TestCase_WorkOrder>();
        public ObservableCollection<Log_TestCase_CSCTicket> listCSCTicket { get; set; } = new ObservableCollection<Log_TestCase_CSCTicket>();
        private void CommandAddWorkOrderExecute(object obj)
        {
            if (!listWorkOrder.Any(x => x.WorkOrderNumber == null))
            {
                if (!listWorkOrder.GroupBy(x => x.WorkOrderNumber).Any(grp => grp.Count() > 1))
                {
                    listWorkOrder.Add(new Log_TestCase_WorkOrder { WorkOrderNumber = null, Log_WorkOrderID = listWorkOrder.Count() });
                    BindWorkOrderList();
                }
                else
                {
                    _notifier.ShowWarning("New work order not added due existing work orders is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New work order not added due existing work order is empty or null!");
            }
        }
        private void CommandAddCSCTicketExecute(object obj)
        {
            if (!listCSCTicket.Any(x => x.CSCTicket_Number == null))
            {
                if (!listCSCTicket.GroupBy(x => x.CSCTicket_Number).Any(grp => grp.Count() > 1))
                {
                    listCSCTicket.Add(new Log_TestCase_CSCTicket { CSCTicket_Number = null, Log_CSCTicketID = listCSCTicket.Count() });
                    BindCSCTicketList();
                }
                else
                {
                    _notifier.ShowWarning("New CSC Ticket not added due existing CSC Ticket is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New CSC Ticket not added due existing CSC Ticket is empty or null!");
            }
        }
    }
    public class MapLibTestCaseMap
    {
        public int TestCaseID { get; set; }
        public int? TestCaseTypeID { get; set; }
        public string TestCaseType { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public string QSID { get; set; }
        public int QSID_ID { get; set; }
        public string ExpectedCountOperator { get; set; }
        public int? ExpectedCountMatch { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string DeactivateBy { get; set; }
        public DateTime? DeactivateTimeStamp { get; set; }
    }
    public class GenericCriteria_VM : Base_ViewModel
    {
        int _criteriaNumber { get; set; }
        public int CriteriaNumber
        {
            get { return _criteriaNumber; }
            set
            {
                _criteriaNumber = value;
                NotifyPropertyChanged("CriteriaNumber");
            }
        }
        public int FieldNameID { get; set; }
        public LibraryFieldName_Generic SelectFieldName
        {
            get
            {
                return _selectFieldNames;
            }
            set
            {
                if (value != null)
                {
                    _selectFieldNames = value;
                    FieldNameID = _selectFieldNames.FieldNameID;
                    NotifyPropertyChanged("SelectFieldName");
                }
            }
        }
        LibraryFieldName_Generic _selectFieldNames { get; set; }
        SelectListItem _selectedCriteria_Generics { get; set; }
        public SelectListItem SelectedCriteria_Generics
        {
            get
            {
                return _selectedCriteria_Generics;
            }
            set
            {
                if (value != null)
                {
                    _selectedCriteria_Generics = value;
                    CriteriaType = _selectedCriteria_Generics.Text;
                    NotifyPropertyChanged("SelectedCriteria_Generics");
                }
            }
        }
        SelectListItem _selectedNextCondition_Generics { get; set; }
        public SelectListItem SelectedNextCondition_Generics
        {
            get
            {
                return _selectedNextCondition_Generics;
            }
            set
            {
                if (value != null)
                {
                    _selectedNextCondition_Generics = value;
                    NextCondition = _selectedNextCondition_Generics.Text;
                    NotifyPropertyChanged("SelectedNextCondition_Generics");
                }
            }
        }
        public string CriteriaType { get; set; }
        public string CriteriaValue { get; set; }
        public string NextCondition { get; set; }
        public int? GroupCriteriaNo { get; set; }
    }
}
