﻿using CRA_DataAccess;
using GalaSoft.MvvmLight.Messaging;
using QATestCases.CustomUserControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using VersionControlSystem.Model.ViewModel;

namespace QATestCases.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand commandSelectedChanges { get; set; }
        public ICommand BtnRunShowResult { get; set; }
        public Visibility VisibilityLibRunTestCase_Loader { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<MapRunTestCase> comonLibRunTestCase_VM { get; set; }
        private List<CommonDataGridColumn> GetListGridColumnRunTestCase()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Description", ColBindingName = "Description", ColType = "Textbox", ColumnWidth = "400" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "MatchRecords", ColBindingName = "MatchRecords", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ExpectedCountOperator", ColBindingName = "ExpectedCountOperator", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ExpectedCountMatch", ColBindingName = "ExpectedCountMatch", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SeeDetail", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "See Detail", BackgroundColor = "#07689f", CommandParam = "SeeDetailTestCases" });
            return listColumnQsidDetail;
        }
        private List<MapRunTestCase> _listRunTestCase { get; set; } = new List<MapRunTestCase>();
        public List<MapRunTestCase> listRunTestCase
        {
            get { return _listRunTestCase; }
            set
            {
                if (_listRunTestCase != value)
                {
                    _listRunTestCase = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapRunTestCase>>>(new PropertyChangedMessage<List<MapRunTestCase>>(null, _listRunTestCase, "Default List"));
                }
            }
        }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        private DataTable matchRec = new DataTable();
        private List<MapRunTestCase> result = new List<MapRunTestCase>();
        private void BtnRunShowResultExecute(object o)
        {
            VisibilityLibRunTestCase_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibRunTestCase_Loader");
            Task.Run(() =>
            {
                var CurrentDataTable = new DataTable();
                var tempData = rowIdentifier.MakeOldDataListDharmesh(SelectedQsidAll.QSID_ID, true, true, null);
                CurrentDataTable = rowIdentifier.AddExtraFieldInCurrentData(SelectedQsidAll.QSID_ID, CurrentDataTable, tempData);
                result = new List<MapRunTestCase>();
                for (int i = 0; i < listTestCasesByQsid.Count; i++)
                {
                    if (listTestCasesByQsid[i].IsSelected)
                    {
                        var queryCombine = CombineTestCaseDet(listTestCasesByQsid[i].TestCaseID);
                        var tempmatchRec = CurrentDataTable.Select(queryCombine[0]);
                        matchRec = new DataTable();
                        if (tempmatchRec.Any())
                        {
                            matchRec = tempmatchRec.CopyToDataTable();
                        }
                        string sts = string.Empty;
                        switch (listTestCasesByQsid[i].ExpectedCountOperator)
                        {
                            case "<":
                                sts = tempmatchRec.Count() < listTestCasesByQsid[i].ExpectedCountMatch ? "Pass" : "Fail";
                                break;
                            case "<=":
                                sts = tempmatchRec.Count() <= listTestCasesByQsid[i].ExpectedCountMatch ? "Pass" : "Fail";
                                break;
                            case ">":
                                sts = tempmatchRec.Count() > listTestCasesByQsid[i].ExpectedCountMatch ? "Pass" : "Fail";
                                break;
                            case ">=":
                                sts = tempmatchRec.Count() >= listTestCasesByQsid[i].ExpectedCountMatch ? "Pass" : "Fail";
                                break;
                            case "=":
                                sts = tempmatchRec.Count() == listTestCasesByQsid[i].ExpectedCountMatch ? "Pass" : "Fail";
                                break;
                        }
                        result.Add(new MapRunTestCase
                        {
                            TestCaseID = listTestCasesByQsid[i].TestCaseID,
                            Description = listTestCasesByQsid[i].Description,
                            Status = sts,
                            MatchRecords = tempmatchRec.Count(),
                            ExpectedCountOperator = listTestCasesByQsid[i].ExpectedCountOperator,
                            ExpectedCountMatch = listTestCasesByQsid[i].ExpectedCountMatch,
                            matchRecords = matchRec.DefaultView.ToTable(),
                        });
                    }
                }
                listRunTestCase = new List<MapRunTestCase>(result);
                NotifyPropertyChanged("comonLibRunTestCase_VM");
                VisibilityLibRunTestCase_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibRunTestCase_Loader");
            });
        }

        List<Int32> lstSelTestCaseID { get; set; } = new List<int>();
        private void commandSelectedChangesExecute(object obj)
        {
        }
        private MapRunTestCase _SelectedLibTestCaseRun { get; set; }
        public MapRunTestCase SelectedLibTestCaseRun
        {
            get => (MapRunTestCase)_SelectedLibTestCaseRun;
            set
            {
                if (Equals(_SelectedLibTestCaseRun, value))
                {
                    return;
                }
                _SelectedLibTestCaseRun = value;
            }
        }
        private void NotifyMeLibTestCaseRun(PropertyChangedMessage<MapRunTestCase> obj)
        {
            SelectedLibTestCaseRun = obj.NewValue;
        }
        public void CommandButtonExecutedSeeDetailTestCase(object ob)
        {
            var dtRec = result.Where(x => x.Description == SelectedLibTestCaseRun.Description).Select(y => y.matchRecords).FirstOrDefault();
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                PopDetail objChangesAdd = new PopDetail(dtRec, null);
                objChangesAdd.ShowDialog();
            });
        }
    }
    public class MapRunTestCase
    {
        public Int32 TestCaseID { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public Int32 MatchRecords { get; set; }
        public Int32 ExpectedCountMatch { get; set; }
        public String ExpectedCountOperator { get; set; }
        public DataTable matchRecords { get; set; }
    }
    public class IlibTestCase : Base_ViewModel
    {
        bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                NotifyPropertyChanged("IsSelected");

            }
        }
        public int TestCaseID { get; set; }
        public int? TestCaseTypeID { get; set; }
        public string Description { get; set; }

        public Int32 ExpectedCountMatch { get; set; }
        public string ExpectedCountOperator { get; set; }
        public bool? IsActive { get; set; }
        public int? QSID_ID { get; set; }
    }
}
