﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToastNotifications.Messages;
using UpdateFlatGenerics.Library;

namespace UpdateFlatGenerics.ViewModel
{
    public class RefreshGenericsFlats:Base_ViewModel
    {
        public int totalUniqueParentCas { get; set; } = 0;
        public int currentProcessingCas { get; set; } = 0;
        public int totalProcessedRecord { get; set; } = 0;
        public int processingValue { get; set; } = 0;
        public List<int> listUniqueCas { get; set; } 
        public ICommand CommandRefreshFlatGenerics { get; set; }
        public ICommand CommandApplyTempToMainTables { get; set; }
        public string processingNotification { get; set; }=string.Empty;
       
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public RefreshGenericsFlats()
        {
            CommandRefreshFlatGenerics = new RelayCommand(CommandRefreshFlatGenericsExecute, CommandRefreshFlatGenericsCanExecute);
            CommandApplyTempToMainTables = new RelayCommand(CommandApplyTempToMainTablesExecute, CommandRefreshFlatGenericsCanExecute);
            GetTotalUniqueParentCas();
        }

        private void CommandApplyTempToMainTablesExecute(object obj)
        {
            Task.Run(() =>
            {
                processingValue = 10;
                NotifyPropertyChanged("processingValue");

                processingNotification = "Processing ADC for Flat and Combine generics";
                NotifyPropertyChanged("processingNotification");
                _objLibraryFunction_Service.CompareFlatWithTempTable(Environment.UserName);
                processingValue = 60;
                NotifyPropertyChanged("processingValue");

                _objLibraryFunction_Service.CompareCombineGenericsWithTempTable(Environment.UserName);

                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowSuccess("Flat and Combine Generics refresh successfully!");
                });
                processingValue = 100;
                NotifyPropertyChanged("processingValue");

            });
        }

        public void GetTotalUniqueParentCas() {
            listUniqueCas = _objLibraryFunction_Service.GetUniqueParentLabelNode();
            totalUniqueParentCas = listUniqueCas.Count;
            NotifyPropertyChanged("totalUniqueParentCas");
        }

        private void CommandRefreshFlatGenericsExecute(object obj)
        {
            
            _objLibraryFunction_Service.DeleteFlatTempTabel("Delete from Map_Generics_Flat_Temp");
            
            Task.Run(() =>
            {
                try
                {
                    for (var i = 0; i < listUniqueCas.Count; i++)
                    {
                        _objLibraryFunction_Service.CreateNewGenericsFlatTemp(listUniqueCas[i], Environment.UserName);
                        totalProcessedRecord = i + 1;
                        processingNotification = "Completed " + totalProcessedRecord + " out of " + listUniqueCas.Count;
                        NotifyPropertyChanged("processingNotification");
                        BindProgressValue(totalProcessedRecord, totalUniqueParentCas);
                    }
                    processingNotification = "Processing Combine Generics basis on flat, alternate cas and Hydrate";
                    NotifyPropertyChanged("processingNotification");
                    _objLibraryFunction_Service.InsertDeleteCombineGenericsToDBTemp_New(Environment.UserName);
                   
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowSuccess("Flat and Combine Generics added to temp tables successfully!");
                    });
                    processingNotification = "Completed";
                    NotifyPropertyChanged("processingNotification");
                    processingValue = 100;
                    NotifyPropertyChanged("processingValue");                   
                }
                catch (Exception ex) {
                    _objLogUserSession_Service.ErrorLogging(ex);
                    _notifier.ShowError("Internal Server Error: "+ System.Environment.NewLine +ex.Message);
                }
            });
        }
        public void BindProgressValue(int currentProcessRecord,int totalRecords)
        {
            processingValue=(currentProcessRecord*100)/totalRecords;
            NotifyPropertyChanged("processingValue");
        }
        private bool CommandRefreshFlatGenericsCanExecute(object obj)
        {
            if (totalUniqueParentCas > 0 && (processingValue == 0 || processingValue == 100))
            {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
