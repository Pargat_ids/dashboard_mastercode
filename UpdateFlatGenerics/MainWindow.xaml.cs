﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UpdateFlatGenerics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            FetchLastCompileDate();
        }
        private void FetchLastCompileDate()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            DateTime buildDate = new DateTime(2000, 1, 1)
                .AddDays(version.Build)
                .AddSeconds(version.Revision * 2);
            string tmp = " ";
            double tmpWidth = 0;
            for (; ((tmpWidth + 1) < 50);)
            {
                tmp += " ";
                tmpWidth = (tmpWidth + 1);
            }
            this.Title = "Update Generics Flats    " + tmp + "Tool Version: " + buildDate.ToString("MM/dd/yyyy") + "          ( " + GetToolDbType() + " )";

        }
        public string GetToolDbType()
        {
            string ToolDbType=string.Empty;
            string dbContext = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            string[] arrayContext = dbContext.Split(";");
            var dbName = arrayContext.Where(x => x.Contains("initial catalog")).FirstOrDefault();
            string initialCatalog = dbName.Split("=")[1];
            switch (initialCatalog)
            {
                case "CRA_Architecture_Dev":
                   ToolDbType = "UAT";
                    break;
                case "CRA_Architecture":
                   ToolDbType = "Production";
                    break;
                case "CRA_Dev":
                  ToolDbType = "Local";
                    break;
            }
            return ToolDbType;
        }
    }
}
