﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AccessVersionControlSystem.ViewModel
{
    public class TypeChange_VM : INotifyPropertyChanged
    {
        public string TypeHeader { get; set; }
        public string typeChange { get; set; }
        public string TypeChange
        {
            get { return typeChange; }
            set
            {
                typeChange = value;
                OnPropertyChanged();
            }
        }
        public string workOrder { get; set; }
        public string WorkOrder
        {
            get { return workOrder; }
            set
            {
                workOrder = value;
                OnPropertyChanged();
            }
        }

        private bool isSelected;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
    public class TypeChangeHeader_VM
    {
        public string TypeHeader { get; set; }
    }
}
