﻿using AccessVersionControlSystem.Library;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class ListLevelMetaData_VM : Library.Base_ViewModel
    {
        public List<LibPhraseCategory> listPhraseCategory { get; set; }
        public ListLevelMetaData_VM()
        {
            objCommon = new CommonFunctions();
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            listPhraseCategory = objCommon.DbaseQueryReturnSqlList<LibPhraseCategory>("select b.PhraseCategoryID, b.PhraseCategory " +
                " from Library_ListLevelMetaData a, Library_PhraseCategories b " +
                " where a.ListLevelMetaData_PhraseCategoryID = b.PhraseCategoryID and IsMaintainedInDashboardListLevelMetaData = 1 ", CommonListConn);
            comonListLevelMetaData_VM = new CommonDataGrid_ViewModel<MapListLevelMetaData>(GetListGridColumnListLevelMetaData(), "", "");
            CommandShowPopUpAddListLevelMetaData = new RelayCommand(CommandShowPopUpAddListLevelMetaDataExecute);
            CommandAddListLevelMetaData = new RelayCommand(CommandAddListLevelMetaDataExecute, CommandAddLibSubrootsCanExecute);
            CommandAddMoreListLevelMetaData = new RelayCommand(CommandAddMoreListLevelMetaDataExecute);
            CommandClosePopUpListLevelMetaData = new RelayCommand(CommandClosePopUpExecuteListLevelMetaData);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapListLevelMetaData), refreshListLevelMetaData);
            MessengerInstance.Register<PropertyChangedMessage<MapListLevelMetaData>>(this, NotifyMeListLevelMetaData);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapListLevelMetaData), CommandButtonExecutedListLevelMetaData);
            BindListLevelMetaData();
        }
        public MapListLevelMetaData SelectedListLevelMetaData { get; set; }
        private void NotifyMeListLevelMetaData(PropertyChangedMessage<MapListLevelMetaData> obj)
        {
            SelectedListLevelMetaData = obj.NewValue;
        }
        private bool CommandAddLibSubrootsCanExecute(object obj)
        {
            if (SelectedPhraseCategory == null || string.IsNullOrEmpty(newPhraseValue.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private LibPhraseCategory _SelectedPhraseCategory { get; set; }
        public LibPhraseCategory SelectedPhraseCategory
        {
            get => _SelectedPhraseCategory;
            set
            {
                if (Equals(_SelectedPhraseCategory, value))
                {
                    return;
                }

                _SelectedPhraseCategory = value;
            }
        }

        private void refreshListLevelMetaData(PropertyChangedMessage<string> obj)
        {
            if (obj.NewValue == "ListMetaData")
            {
                BindListLevelMetaData();
            }
        }
        public CommonFunctions objCommon;
        public string CommonListConn;
        public CommonDataGrid_ViewModel<MapListLevelMetaData> comonListLevelMetaData_VM { get; set; }
        public string newPhraseValue { get; set; } = string.Empty;
        public Visibility VisibilityListLevelMetaDataDashboard_Loader { get; set; }
        public Visibility PopUpContentAddListLevelMetaDataVisibility { get; set; }
        public ICommand CommandClosePopUpListLevelMetaData { get; private set; }
        public ICommand CommandShowPopUpAddListLevelMetaData { get; private set; }
        public ICommand CommandAddListLevelMetaData { get; private set; }
        public ICommand CommandAddMoreListLevelMetaData { get; private set; }
        private bool _IsShowPopUpListLevelMetaData { get; set; }
        public bool IsShowPopUpListLevelMetaData
        {
            get { return _IsShowPopUpListLevelMetaData; }
            set
            {
                if (_IsShowPopUpListLevelMetaData != value)
                {
                    _IsShowPopUpListLevelMetaData = value;
                    NotifyPropertyChanged("_IsShowPopUpListLevelMetaData");
                }
            }
        }
        private ObservableCollection<MapListLevelMetaData> _lstListLevelMetaData { get; set; } = new ObservableCollection<MapListLevelMetaData>();
        public ObservableCollection<MapListLevelMetaData> lstListLevelMetaData
        {
            get { return _lstListLevelMetaData; }
            set
            {
                if (_lstListLevelMetaData != value)
                {
                    _lstListLevelMetaData = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapListLevelMetaData>>>(new PropertyChangedMessage<List<MapListLevelMetaData>>(null, _lstListLevelMetaData.ToList(), "Default List"));
                }
            }
        }

        private void BindListLevelMetaData()
        {
            VisibilityListLevelMetaDataDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListLevelMetaDashboard_Loader");
            Task.Run(() =>
            {
                var SAPListLevelMetaDataTable = objCommon.DbaseQueryReturnSqlList<MapListLevelMetaData>(
                "select a.Qsxxx_listDictionary_Id, a.PhraseCategoryId, a.PhraseId, phrasecategory,phrase, isnull(IsMaintainedInDashboardListLevelMetaData,'False')  as IsButtonVisible from (select a.Qsxxx_listDictionary_Id, a.PhraseCategoryId, a.PhraseId, phrasecategory,phrase from Qsxxx_ListDictionary a, library_phraseCategories b, library_phrases c where a.qsid_id =" + Engine.CurrentDataViewQsidID + " and a.phrasecategoryid = b.phrasecategoryid and a.phraseid = c.phraseid )  as a left join Library_ListLevelMetaData b on a.PhraseCategoryId = b.ListLevelMetaData_PhraseCategoryID and b.IsMaintainedInDashboardListLevelMetaData = 1", CommonListConn);
                lstListLevelMetaData = new ObservableCollection<MapListLevelMetaData>(SAPListLevelMetaDataTable);
            });
            NotifyPropertyChanged("comonListLevelMetaData_VM");
            VisibilityListLevelMetaDataDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityListLevelMetaDataDashboard_Loader");
        }

        private void CommandAddListLevelMetaDataExecute(object obj)
        {
            SaveListLevelMetaData();
            IsEditListLevelMetaData = true;
            NotifyPropertyChanged("IsEditListLevelMetaData");
            IsShowPopUpListLevelMetaData = false;
            NotifyPropertyChanged("IsShowPopUpListLevelMetaData");
            ClearPopUpVariableListLevelMetaData();
            PopUpContentAddListLevelMetaDataVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddListLevelMetaDataVisibility");
        }
        public void SaveListLevelMetaData()
        {
            using (var context = new CRAModel())
            {
                QSxxx_ListDictionary obj = new QSxxx_ListDictionary();
                var lstStripChar = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();

                newPhraseValue = this.objCommon.RemoveWhitespaceCharacter(newPhraseValue, lstStripChar);
                newPhraseValue = Regex.Replace(newPhraseValue, @"\s+", " ").Trim();
                if (!IsEditListLevelMetaData)
                {
                    
                    obj.PhraseCategoryID = SelectedPhraseCategory.PhraseCategoryID;
                    var phrId = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == newPhraseValue && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                    if (phrId == 0)
                    {
                        insertIntoLibPhrases(newPhraseValue);
                        phrId = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == newPhraseValue && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                    }
                    obj.PhraseID = phrId;
                    obj.QSID_ID = Engine.CurrentDataViewQsidID;
                    var chkAlreadyExist = context.QSxxx_ListDictionary.Count(x => x.PhraseCategoryID == obj.PhraseCategoryID && x.QSID_ID == Engine.CurrentDataViewQsidID);
                    if (chkAlreadyExist > 0)
                    {   
                        _notifier.ShowError("Phrase Category can't be duplicate");
                    }
                    else
                    {
                        var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('QSxxx_ListDictionary')", CommonListConn));
                        obj.QSxxx_ListDictionary_ID = mx + 1;
                        Log_QSxxx_ListDictionary libraryListDic = new Log_QSxxx_ListDictionary
                        {
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Status = "A",
                            QSxxx_ListDictionary_ID = mx,
                            ListDictionaryID = 1,
                            PhraseID = phrId,
                            PhraseCategoryID = SelectedPhraseCategory.PhraseCategoryID,
                            QSID_ID = Engine.CurrentDataViewQsidID,
                        };
                        context.Log_QSxxx_ListDictionary.Add(libraryListDic);
                        context.QSxxx_ListDictionary.Add(obj);
                        context.SaveChanges();
                        _notifier.ShowSuccess("New Phrase Category Saved Successfully.");
                    }
                }
                else
                {
                    obj = context.QSxxx_ListDictionary.Where(x => x.QSxxx_ListDictionary_ID == SelectedListLevelMetaData.QSxxx_ListDictionary_ID && x.QSID_ID == Engine.CurrentDataViewQsidID).FirstOrDefault();
                    var chkAlreadyExist = context.QSxxx_ListDictionary.Count(x => x.PhraseCategoryID == obj.PhraseCategoryID && x.QSxxx_ListDictionary_ID != SelectedListLevelMetaData.QSxxx_ListDictionary_ID && x.QSID_ID == Engine.CurrentDataViewQsidID);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("Phrase Category can't be duplicate");
                    }
                    else
                    {
                        obj.PhraseCategoryID = SelectedPhraseCategory.PhraseCategoryID;
                        var phrId = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == newPhraseValue && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                        if (phrId == 0)
                        {
                            insertIntoLibPhrases(newPhraseValue);
                            phrId = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == newPhraseValue && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                        }
                        obj.PhraseID = phrId;
                        obj.QSID_ID = Engine.CurrentDataViewQsidID;
                        context.SaveChanges();
                        Log_QSxxx_ListDictionary libraryListDic = new Log_QSxxx_ListDictionary
                        {
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Status = "C",
                            QSxxx_ListDictionary_ID = SelectedListLevelMetaData.QSxxx_ListDictionary_ID,
                            ListDictionaryID = 1,
                            PhraseID = phrId,
                            PhraseCategoryID = obj.PhraseCategoryID,
                            QSID_ID = Engine.CurrentDataViewQsidID,
                        };
                        context.Log_QSxxx_ListDictionary.Add(libraryListDic);
                        context.SaveChanges();
                        _notifier.ShowSuccess("Updated Successfully.");
                    }
                }
            }

            ClearPopUpVariableListLevelMetaData();
            BindListLevelMetaData();

        }

        private void insertIntoLibPhrases(string PhraseValue)
        {
            List<Library_Phrases> lstMain = new List<Library_Phrases>();
            var lib = new Library_Phrases
            {
                InternalOnly = false,
                Phrase = PhraseValue,
                PhraseHash = objCommon.GenerateSHA256String(PhraseValue),
                IsActive = true,
                LanguageID = 1,
                TimeStamp = objCommon.ESTTime(),
            };
            lstMain.Add(lib);
            _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
        }
        private void CommandAddMoreListLevelMetaDataExecute(object obj)
        {
            SaveListLevelMetaData();
        }
        private void CommandClosePopUpExecuteListLevelMetaData(object obj)
        {
            IsShowPopUpListLevelMetaData = false;
            NotifyPropertyChanged("IsShowPopUpListLevelMetaData");
        }
        private void CommandShowPopUpAddListLevelMetaDataExecute(object obj)
        {
            ClearPopUpVariableListLevelMetaData();
            PhraseCategoryComboRead = false;
            NotifyPropertyChanged("PhraseCategoryComboRead");
            IsEditListLevelMetaData = false;
            NotifyPropertyChanged("IsEditListLevelMetaData");
            IsShowPopUpListLevelMetaData = true;
            NotifyPropertyChanged("IsShowPopUpListLevelMetaData");
            PopUpContentAddListLevelMetaDataVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddListLevelMetaDataVisibility");
        }

        public void ClearPopUpVariableListLevelMetaData()
        {
            newPhraseValue = "";
            NotifyPropertyChanged("newPhraseValue");
        }
        private List<CommonDataGridColumn> GetListGridColumnListLevelMetaData()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseCategory", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox" , ColumnWidth = "1000" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteListLevelMetaData" , CustomStyleClass = "MetaDataButtons" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditListLevelMetaData", CustomStyleClass = "MetaDataButtons" });
            return listColumnQsidDetail;
        }
        public bool IsEditListLevelMetaData { get; set; } = true;
        public bool PhraseCategoryComboRead { get; set; } = false;
        private void CommandButtonExecutedListLevelMetaData(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteListLevelMetaData")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this List Dictionary Category?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    using (var context = new CRAModel())
                    {
                        var objdel = context.QSxxx_ListDictionary.Where(x => x.QSxxx_ListDictionary_ID == SelectedListLevelMetaData.QSxxx_ListDictionary_ID && x.QSID_ID == Engine.CurrentDataViewQsidID).FirstOrDefault();
                        Log_QSxxx_ListDictionary libraryListDic = new Log_QSxxx_ListDictionary
                        {
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Status = "D",
                            QSxxx_ListDictionary_ID = objdel.QSxxx_ListDictionary_ID,
                            ListDictionaryID = 0,
                            PhraseID = objdel.PhraseID,
                            PhraseCategoryID = objdel.PhraseCategoryID,
                            QSID_ID = objdel.QSID_ID,
                        };
                        context.Log_QSxxx_ListDictionary.Add(libraryListDic);
                        context.SaveChanges();
                    }
                    objCommon.DbaseQueryReturnStringSQL("Delete from QSxxx_ListDictionary where QSxxx_ListDictionary_ID =" + SelectedListLevelMetaData.QSxxx_ListDictionary_ID, CommonListConn);
                    BindListLevelMetaData();
                    _notifier.ShowSuccess("Selected  List Dictionary deleted successfully");

                }
            }
            else if (obj.Notification == "AddEditListLevelMetaData")
            {
                PhraseCategoryComboRead = true;
                NotifyPropertyChanged("IsEditListLevelMetaData");
                IsEditListLevelMetaData = true;
                NotifyPropertyChanged("IsEditListLevelMetaData");
                IsShowPopUpListLevelMetaData = true;
                PopUpContentAddListLevelMetaDataVisibility = Visibility.Visible;
                NotifyPropertyChanged("IsShowPopUpListLevelMetaData");
                NotifyPropertyChanged("PopUpContentAddListLevelMetaDataVisibility");

                SelectedPhraseCategory = listPhraseCategory.Where(x => x.PhraseCategoryID == SelectedListLevelMetaData.PhraseCategoryID).FirstOrDefault();
                newPhraseValue = SelectedListLevelMetaData.Phrase;
                NotifyPropertyChanged("newPhraseValue");
                NotifyPropertyChanged("SelectedPhraseCategory");


            }
        }
    }
    public class MapListLevelMetaData
    {
        public int QSxxx_ListDictionary_ID { get; set; }
        public int PhraseID { get; set; }
        public int PhraseCategoryID { get; set; }
        public string Phrase { get; set; }
        public string PhraseCategory { get; set; }
        public bool? IsButtonVisible { get; set; }
    }
    public class LibPhraseCategory
    {
        public int PhraseCategoryID { get; set; }
        public string PhraseCategory { get; set; }
    }
}
