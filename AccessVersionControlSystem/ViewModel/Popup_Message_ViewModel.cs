﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using System.Windows.Input;

namespace AccessVersionControlSystem.ViewModel
{
    public class Popup_Message_ViewModel
    {
        #region Property Member
        public string MessageHeader { get; set; }
        public string MessageContent { get; set; }
        public ICommand CommandCancel { get; private set; }
        public PopUp_Message popWindow { get; set; }
        #endregion

        #region
        public Popup_Message_ViewModel(PopUp_Message _popWindow, string _messageHeader, string _messageContent)
        {
            MessageHeader = _messageHeader;
            MessageContent = _messageContent;
            CommandCancel = new RelayCommand(CommandCancelExecute, CommandCancelCanExecute);
            popWindow = _popWindow;
        }
        private bool CommandCancelCanExecute(object o)
        {
            return true;
        }

        private void CommandCancelExecute(object o)
        {
            popWindow.Close();
        }
        #endregion
    }
}
