﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using CurrentDataView.ViewModel;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using DocumentFormat.OpenXml.Wordprocessing;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class Popup_ListChangesReport_ViewModel : Base_ViewModel
    {
        public StandardDataGrid_VM objSummaryAdded { get; set; }
        public StandardDataGrid_VM objSummaryDeleted { get; set; }
        public StandardDataGrid_VM objCASAdded { get; set; }
        public StandardDataGrid_VM objCASDeleted { get; set; }
        public StandardDataGrid_VM objSummaryChanged { get; set; }

        public Visibility LoadSummAdded { get; set; } = Visibility.Collapsed;
        public Visibility LoadSummDel { get; set; } = Visibility.Collapsed;
        public Visibility LoadSummChg { get; set; } = Visibility.Collapsed;
        public Visibility LoadSummCasAdded { get; set; } = Visibility.Collapsed;
        public Visibility LoadSummCasDeleted { get; set; } = Visibility.Collapsed;
        public Visibility ShowTabSummaryForExistingList { get; set; } = Visibility.Collapsed;

        public Visibility LoadQsidFieldAdded { get; set; } = Visibility.Collapsed;
        public Visibility LoadQsidFieldDeleted { get; set; } = Visibility.Collapsed;
        public Visibility LoadListDictChanges { get; set; } = Visibility.Collapsed;

        public Visibility LoadNOL { get; set; } = Visibility.Collapsed;
        public Visibility LoadFieldExplainChanges { get; set; } = Visibility.Collapsed;
        public string _gridType { get; set; }
        public string _activeTab { get; set; }

        public bool IsSelectedRowAdded { get; set; } = false;
        public bool IsSelectedRowDeleted { get; set; } = false;
        public bool IsSelectedRowChanged { get; set; } = false;
        public bool IsSelectedCasDeleted { get; set; } = false;
        public bool IsSelectedCasAdded { get; set; } = false;
        public bool IsQsidFieldAdded { get; set; } = false;
        public bool IsQsidFieldDeleted { get; set; } = false;
        public bool IsListDictionaryChanges { get; set; } = false;
        public bool IsListNOL { get; set; } = false;
        public bool IsListFieldExplainChanges { get; set; } = false;
        public bool IsSelectDataDict { get; set; } = false;
        public bool IsSelectedRowChangesMainTab { get; set; } = false;
        


        public TabItem _SelectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get { return _SelectedTabItem; }
            set
            {
                if (_SelectedTabItem != value)
                {
                    _SelectedTabItem = value;
                    OnTabChange();
                }

            }
        }
        public CommonDataGrid_ViewModel<ListChangesReportAddQsidFieldPopUP> comonDGChangesReportAddField_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportDeleteFieldPopUP> comonDGChangesReportDeleteField_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportListDictionaryPopUP> comonDGListDicChanges_VM { get; set; }
        public CommonDataGrid_ViewModel<Log_ListNOL_VM> comonDGListNOL_VM { get; set; }
        public CommonDataGrid_ViewModel<Log_ListFieldExplainChanges_VM> comonDGListFieldExplainChanges_VM { get; set; }

        public ObservableCollection<ListChangesReportAddQsidFieldPopUP> _listAddedFieldQsid { get; set; }
        public ObservableCollection<ListChangesReportAddQsidFieldPopUP> ListAddedFieldQsid
        {
            get { return _listAddedFieldQsid; }
            set
            {
                if (_listAddedFieldQsid != value)
                {
                    _listAddedFieldQsid = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportAddQsidFieldPopUP>>>(new PropertyChangedMessage<List<ListChangesReportAddQsidFieldPopUP>>(_listAddedFieldQsid.ToList(), _listAddedFieldQsid.ToList(), "Default List"));

                }
            }
        }
        public ObservableCollection<ListChangesReportDeleteFieldPopUP> _listDeletedFieldQsid { get; set; }
        public ObservableCollection<ListChangesReportDeleteFieldPopUP> ListDeletedFieldQsid
        {
            get { return _listDeletedFieldQsid; }
            set
            {
                if (_listDeletedFieldQsid != value)
                {
                    _listDeletedFieldQsid = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportDeleteFieldPopUP>>>(new PropertyChangedMessage<List<ListChangesReportDeleteFieldPopUP>>(_listDeletedFieldQsid.ToList(), _listDeletedFieldQsid.ToList(), "Default List"));

                }
            }
        }
        public ObservableCollection<ListChangesReportListDictionaryPopUP> _lisDicionaryChanges { get; set; }
        public ObservableCollection<ListChangesReportListDictionaryPopUP> ListDicionaryChanges
        {
            get { return _lisDicionaryChanges; }
            set
            {
                if (_lisDicionaryChanges != value)
                {
                    _lisDicionaryChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportListDictionaryPopUP>>>(new PropertyChangedMessage<List<ListChangesReportListDictionaryPopUP>>(_lisDicionaryChanges.ToList(), _lisDicionaryChanges.ToList(), "Default List"));

                }
            }
        }
        public ObservableCollection<Log_ListNOL_VM> _ListNOLChanges { get; set; }
        public ObservableCollection<Log_ListNOL_VM> ListNOLChanges
        {
            get { return _ListNOLChanges; }
            set
            {
                if (_ListNOLChanges != value)
                {
                    _ListNOLChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Log_ListNOL_VM>>>(new PropertyChangedMessage<List<Log_ListNOL_VM>>(_ListNOLChanges.ToList(), _ListNOLChanges.ToList(), "Default List"));

                }
            }
        }


        public ObservableCollection<Log_ListFieldExplainChanges_VM> _ListFieldExplainChanges { get; set; }
        public ObservableCollection<Log_ListFieldExplainChanges_VM> ListFieldExplainChanges
        {
            get { return _ListFieldExplainChanges; }
            set
            {
                if (_ListFieldExplainChanges != value)
                {
                    _ListFieldExplainChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Log_ListFieldExplainChanges_VM>>>(new PropertyChangedMessage<List<Log_ListFieldExplainChanges_VM>>(_ListFieldExplainChanges.ToList(), _ListFieldExplainChanges.ToList(), "Default List"));

                }
            }
        }

        public string TitlePopUp { get; set; }
        public Popup_ListChangesReport_ViewModel(int qsid_ID, DateTime? ProductDateMin, DateTime? ProductDateMax, string GridType, string ActiveTab)
        {
            _qsid_ID = qsid_ID;
            _ProductDateMin = ProductDateMin;
            _ProductDateMax = ProductDateMax;
            _gridType = GridType;
            _activeTab = ActiveTab;

            comonDGChangesReportAddField_VM = new CommonDataGrid_ViewModel<ListChangesReportAddQsidFieldPopUP>(GetListReportAddFieldGridCol(), "Qsid", "New fields added :");
            NotifyPropertyChanged("comonDGChangesReportAddField_VM");
            comonDGChangesReportDeleteField_VM = new CommonDataGrid_ViewModel<ListChangesReportDeleteFieldPopUP>(GetListReportChangeDeleteFieldGridCol(), "Qsid", "Deleted fields :");
            NotifyPropertyChanged("comonDGChangesReportDeleteField_VM");
            comonDGListDicChanges_VM = new CommonDataGrid_ViewModel<ListChangesReportListDictionaryPopUP>(GetListDictionaryChanges(), "PhraseCategory", "List Dictionary Changes :");
            NotifyPropertyChanged("comonDGListDicChanges_VM");
            comonDGListNOL_VM = new CommonDataGrid_ViewModel<Log_ListNOL_VM>(GetListNOL(), "FieldName", "NOL Fields :");
            NotifyPropertyChanged("comonDGListNOL_VM");
            comonDGListFieldExplainChanges_VM = new CommonDataGrid_ViewModel<Log_ListFieldExplainChanges_VM>(GetListFieldExplainChanges(), "FieldName", "Field Explain Changes :");
            NotifyPropertyChanged("comonDGListFieldExplainChanges_VM");

            BindGridOnInit();
            TitlePopUp = "Detail Changes For QSID: " + _objLibraryFunction_Service.GetQsidFromLibrary(qsid_ID);
        }
        private List<CommonDataGridColumn> GetListDictionaryChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Category", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "New Value", ColBindingName = "NewValue", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Old Value", ColBindingName = "OldValue", ColType = "Textbox" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListNOL()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListFieldExplainChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldExplain", ColBindingName = "OldExplain", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewExplain", ColBindingName = "NewExplain", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldHeadCD", ColBindingName = "OldHeadCD", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewHeadCD", ColBindingName = "NewHeadCD", ColType = "Textbox" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListReportAddFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAddedDeleted", ColType = "Textbox" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportChangeDeleteFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateDeleted", ColBindingName = "DateAddedDeleted", ColType = "Textbox" });
            return listColumn;
        }

        public void OnTabChange()
        {
            switch (SelectedTabItem.Header.ToString())
            {
                case "Rows Added":

                    UniqueRowAdded();
                    break;
                case "Rows Changed":

                    UniqueRowChanges();
                    break;
                case "Rows Deleted":

                    UniqueRowDeleted();
                    break;
                case "Cas Added":

                    SummaryCasAdded();
                    break;
                case "Cas Deleted":

                    SummaryCasDeleted();
                    break;
                case "Fields Added":
                    BindDataDictonaryAddQsidFields();
                    break;
                case "Fields Deleted":
                    BindDataDictonaryDeleteQsidFields();
                    break;
                case "List Dictionary Changes":
                    BindListDictonaryChanges();
                    break;
                case "Count NOL":
                    BindNOL();
                    break;
                case "Field Explain Changes":
                    BindFieldExplainChanges();
                    break;

            }
        }

        public DateTime? _ProductDateMin { get; set; }
        public DateTime? _ProductDateMax { get; set; }
        public int _qsid_ID { get; set; }
        public void BindGridOnInit()
        {
            if (_gridType == "NewAddedList")
            {
                ShowTabSummaryForExistingList = Visibility.Collapsed;
            }
            else { ShowTabSummaryForExistingList = Visibility.Visible; }
            switch (_activeTab)
            {
                case "Row Added":
                    IsSelectedRowChangesMainTab=true;
                    IsSelectedRowAdded = true;                    
                    break;
                case "Row Changed":
                    IsSelectedRowChangesMainTab = true;
                    IsSelectedRowChanged = true;                    
                    break;
                case "Row Deleted":
                    IsSelectedRowChangesMainTab = true;
                    IsSelectedRowDeleted = true;                   
                    break;
                case "Cas Added":
                    IsSelectedCasAdded = true;                   
                    break;
                case "Cas Deleted":
                    IsSelectedCasDeleted = true;                    
                    break;
                case "Fields Added":
                    IsSelectDataDict = true;
                    IsQsidFieldAdded = true;
                    break;
                case "Fields Deleted":
                    IsSelectDataDict = true;
                    IsQsidFieldDeleted = true;
                    break;
                case "List Dictionary Changes":
                    IsListDictionaryChanges = true;
                    break;
                case "Count NOL":
                    IsListNOL = true;
                    break;
                case "Field Explain Changes":
                    IsListFieldExplainChanges = true;
                    break;
            }
        }

        private void UniqueRowAdded()
        {
            LoadSummAdded = Visibility.Visible;
            NotifyPropertyChanged("LoadSummAdded");
            Task.Run(() =>
            {
                var addRows = _objLibraryFunction_Service.GetUniqueRow_ADC_Summary(_qsid_ID, "A", _ProductDateMin, _ProductDateMax);
                if (addRows != null && addRows.Rows.Count > 0)
                {
                    objSummaryAdded = new StandardDataGrid_VM("Summary Added", addRows, Visibility.Hidden);
                    NotifyPropertyChanged("objSummaryAdded");
                }
                LoadSummAdded = Visibility.Collapsed;
                NotifyPropertyChanged("LoadSummAdded");
            });
        }

        private void UniqueRowDeleted()
        {
            LoadSummDel = Visibility.Visible;
            NotifyPropertyChanged("LoadSummDel");
            Task.Run(() =>
            {
                var delRows = _objLibraryFunction_Service.GetUniqueRow_ADC_Summary(_qsid_ID, "D", _ProductDateMin, _ProductDateMax);
                if (delRows != null && delRows.Rows.Count > 0)
                {
                    objSummaryDeleted = new StandardDataGrid_VM("Summary Deleted", delRows, Visibility.Hidden);
                    NotifyPropertyChanged("objSummaryDeleted");
                }
                LoadSummDel = Visibility.Collapsed;
                NotifyPropertyChanged("LoadSummDel");
            });
        }
        private void UniqueRowChanges()
        {
            LoadSummChg = Visibility.Visible;
            NotifyPropertyChanged("LoadSummChg");
            Task.Run(() =>
            {
                var chgRows = _objLibraryFunction_Service.GetUniqueRow_Changes_Summary(_qsid_ID, "C", _ProductDateMin, _ProductDateMax);
                if (chgRows != null && chgRows.Rows.Count > 0)
                {
                    objSummaryChanged = new StandardDataGrid_VM("Summary Changed", chgRows, Visibility.Hidden);
                    NotifyPropertyChanged("objSummaryChanged");
                }
                LoadSummChg = Visibility.Collapsed;
                NotifyPropertyChanged("LoadSummChg");
            });
        }

        public void SummaryCasAdded()
        {
            LoadSummCasAdded = Visibility.Visible;
            NotifyPropertyChanged("LoadSummCasAdded");
            Task.Run(() =>
            {
                var newCAS = _objLibraryFunction_Service.GetCasSummary(_qsid_ID, _ProductDateMin, _ProductDateMax, "A");
                if (newCAS != null && newCAS.Rows.Count > 0)
                {
                    objCASAdded = new StandardDataGrid_VM("New CAS", newCAS, Visibility.Hidden);
                    NotifyPropertyChanged("objCASAdded");
                }
                LoadSummCasAdded = Visibility.Collapsed;
                NotifyPropertyChanged("LoadSummCasAdded");
            });
        }
        public void SummaryCasDeleted()
        {
            LoadSummCasDeleted = Visibility.Visible;
            NotifyPropertyChanged("LoadSummCasDeleted");
            Task.Run(() =>
            {
                var delCAS = _objLibraryFunction_Service.GetCasSummary(_qsid_ID, _ProductDateMin, _ProductDateMax, "D");
                if (delCAS != null && delCAS.Rows.Count > 0)
                {
                    objCASDeleted = new StandardDataGrid_VM("Deleted CAS", delCAS, Visibility.Hidden);
                    NotifyPropertyChanged("objCASDeleted");
                }
                LoadSummCasDeleted = Visibility.Collapsed;
                NotifyPropertyChanged("LoadSummCasDeleted");
            });
        }


        public void BindDataDictonaryAddQsidFields()
        {
            LoadQsidFieldAdded = Visibility.Visible;
            NotifyPropertyChanged("LoadQsidFieldAdded");
            Task.Run(() =>
            {
                ListAddedFieldQsid = new ObservableCollection<ListChangesReportAddQsidFieldPopUP>(Common.CommonFunctions.CovertAddQsidBusinessToViewModelProductDate(_objAccessVersion_Service.GetAddedQsidFieldbyProductDateTimestamp(_qsid_ID, _ProductDateMin, _ProductDateMax)));
                LoadQsidFieldAdded = Visibility.Collapsed;
                NotifyPropertyChanged("LoadQsidFieldAdded");
            });
        }
        public void BindDataDictonaryDeleteQsidFields()
        {
            LoadQsidFieldDeleted = Visibility.Visible;
            NotifyPropertyChanged("LoadQsidFieldDeleted");
            Task.Run(() =>
            {
                ListDeletedFieldQsid = new ObservableCollection<ListChangesReportDeleteFieldPopUP>(Common.CommonFunctions.CovertDeletedQsidBusinessToViewModelProductDate(_objAccessVersion_Service.GetDeletedQsidFieldbyProductDateTimestamp(_qsid_ID, _ProductDateMin, _ProductDateMax)));
                LoadQsidFieldDeleted = Visibility.Collapsed;
                NotifyPropertyChanged("LoadQsidFieldDeleted");
            });
        }
        public void BindListDictonaryChanges()
        {
            LoadListDictChanges = Visibility.Visible;
            NotifyPropertyChanged("LoadListDictChanges");
            Task.Run(() =>
            {
                ListDicionaryChanges = new ObservableCollection<ListChangesReportListDictionaryPopUP>(Common.CommonFunctions.CovertListChangesReportBusinessToViewModel(_objAccessVersion_Service.GetListDictionaryChangesbyProductDateTimestamp(_qsid_ID, _ProductDateMin, _ProductDateMax)));
                LoadListDictChanges = Visibility.Collapsed;
                NotifyPropertyChanged("LoadListDictChanges");
            });

        }
        public void BindNOL()
        {
            LoadNOL = Visibility.Visible;
            NotifyPropertyChanged("LoadNOL");
            Task.Run(() =>
            {
                ListNOLChanges = new ObservableCollection<Log_ListNOL_VM>(_objAccessVersion_Service.GetListNOL(_qsid_ID));
                LoadNOL = Visibility.Collapsed;
                NotifyPropertyChanged("LoadNOL");
            });

        }

        public void BindFieldExplainChanges()
        {
            LoadFieldExplainChanges = Visibility.Visible;
            NotifyPropertyChanged("LoadFieldExplainChanges");
            Task.Run(() =>
            {
                ListFieldExplainChanges = new ObservableCollection<Log_ListFieldExplainChanges_VM>(_objAccessVersion_Service.GetListFieldExplainChanges(_qsid_ID));
                LoadFieldExplainChanges = Visibility.Collapsed;
                NotifyPropertyChanged("LoadFieldExplainChanges");
            });

        }
    }
}
