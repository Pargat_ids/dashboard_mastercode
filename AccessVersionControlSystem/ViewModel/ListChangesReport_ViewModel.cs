﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using MaterialDesignThemes.Wpf;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class ListChangesReport_ViewModel : Base_ViewModel
    {
        public DateTime? ProductDateStart { get; set; }
        public DateTime? ProductDateEnd { get; set; }
        public ICommand commandGetListChangeReport { get; private set; }
        public ICommand CommandNewListAdded { get; private set; }
        public ICommand CommandExistingList { get; private set; }
        public ICommand CommandDeletedList { get; private set; }
        public ICommand CommandNewFieldsList { get; private set; }
        public ICommand CommandDeletedFieldsList { get; private set; }
        public ICommand CommandNewUnitList { get; private set; }
        public ICommand CommandAddPhraseList { get; private set; }
        public ICommand CommandDeletePhraseList { get; private set; }

        public ICommand CommandGenerateWordListChangesResult { get; private set; }
        public ICommand CommandGeneratePdfListChangesResult { get; private set; }
        public ICommand CommandGenerateAccessListChangesResult { get; private set; }
        public Visibility visibilityNewList_DataGrid { get; set; }
        public Visibility visibilityExistingList_DataGrid { get; set; }
        public Visibility visibilityDeletedList_DataGrid { get; set; }
        public Visibility visibilityNewfieldsList_DataGrid { get; set; }
        public Visibility visibilityDeletedfieldsList_DataGrid { get; set; }
        public Visibility visibilityNewUnitList_DataGrid { get; set; }
        public Visibility visibilityNewPhraseList_DataGrid { get; set; }
        public Visibility visibilityDeletePhraseList_DataGrid { get; set; }
        public Visibility visibilityExpandMore_NewList { get; set; }
        public Visibility visibilityExpandMore_ExistingList { get; set; }
        public Visibility visibilityExpandMore_DeletedList { get; set; }
        public Visibility visibilityExpandMore_NewfieldsList { get; set; }
        public Visibility visibilityExpandMore_DeletedfieldsList { get; set; }
        public Visibility visibilityExpandMore_NewUnitList { get; set; }
        public Visibility visibilityExpandMore_NewPhraseList { get; set; }
        public Visibility visibilityExpandMore_DeletePhraseList { get; set; }

        private Visibility _visibilityPhraseAdded_Loader { get; set; }
        private Visibility _visibilityPhraseDeleted_Loader { get; set; }
        public Visibility visibilityPhraseAdded_Loader
        {
            get { return _visibilityPhraseAdded_Loader; }
            set
            {
                _visibilityPhraseAdded_Loader = value;
                NotifyPropertyChanged("visibilityPhraseAdded_Loader");

            }
        }
        public Visibility visibilityPhraseDeleted_Loader
        {
            get { return _visibilityPhraseDeleted_Loader; }
            set
            {
                _visibilityPhraseDeleted_Loader = value;
                NotifyPropertyChanged("visibilityPhraseDeleted_Loader");
            }
        }
        public CommonDataGrid_ViewModel<ListChangesReportQsidAdd_VM> comonDGChangesReportAdd_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesResportQsidChanges_VM> comonDGChangesReport_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsidDelete_VM> comonDGChangesReportDelete_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsid_Field_VM> comonDGChangesReportAddField_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsid_DeleteField_VM> comonDGChangesReportDeleteField_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsid_Unit_VM> comonDGChangesReportUnit_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsid_AddPhrases_VM> comonDGChangesReportAddPhrase_VM { get; set; }
        public CommonDataGrid_ViewModel<ListChangesReportQsid_DeletePhrases_VM> comonDGChangesReportDeletePhrase_VM { get; set; }
        //public bool CheckedFoodData { get; set; }
        //public bool CheckedExcludeFoodData { get; set; }

        public Visibility VisibilityResultListChanges { get; set; }

        public ListChangesReport_VM ListChangesResult { get; set; }

        public ListChangesReport_Detail_VM ListChangesReportDetailInfromation { get; set; } //= new ListChangesReport_Detail_VM();
        private List<ListChangesReportQsidAdd_VM> _listChanges_AddNewList { get; set; } = new();

        private List<ListChangesResportQsidChanges_VM> _listChanges_ExistingList { get; set; } = new();

        private List<ListChangesReportQsidDelete_VM> _listChanges_DeletedList { get; set; } = new();

        private List<ListChangesReportQsid_Field_VM> _listChanges_AddNewFieldsList { get; set; } = new();

        private List<ListChangesReportQsid_DeleteField_VM> _listChanges_DeletedFieldsList { get; set; } = new();

        private List<ListChangesReportQsid_Unit_VM> _listChanges_AddNewUnitsList { get; set; } = new();
        public List<ListChangesReportQsidAdd_VM> ListChanges_AddNewList
        {
            get { return _listChanges_AddNewList; }
            set
            {
                if (value != _listChanges_AddNewList)
                {
                    _listChanges_AddNewList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsidAdd_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsidAdd_VM>>(null, _listChanges_AddNewList, "Default List"));
                }
            }
        }
        public List<ListChangesResportQsidChanges_VM> ListChanges_ExistingList
        {
            get { return _listChanges_ExistingList; }
            set
            {
                if (value != _listChanges_ExistingList)
                {
                    _listChanges_ExistingList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesResportQsidChanges_VM>>>(new PropertyChangedMessage<List<ListChangesResportQsidChanges_VM>>(null, _listChanges_ExistingList, "Default List"));
                }
            }
        }
        public List<ListChangesReportQsidDelete_VM> ListChanges_DeletedList
        {
            get { return _listChanges_DeletedList; }
            set
            {
                if (value != _listChanges_DeletedList)
                {
                    _listChanges_DeletedList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsidDelete_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsidDelete_VM>>(null, _listChanges_DeletedList, "Default List"));
                }
            }
        }
        public List<ListChangesReportQsid_Field_VM> ListChanges_AddNewFieldsList
        {
            get { return _listChanges_AddNewFieldsList; }
            set
            {
                if (value != _listChanges_AddNewFieldsList)
                {
                    _listChanges_AddNewFieldsList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsid_Field_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsid_Field_VM>>(null, _listChanges_AddNewFieldsList, "Default List"));

                }
            }
        }
        public List<ListChangesReportQsid_DeleteField_VM> ListChanges_DeletedFieldsList
        {
            get { return _listChanges_DeletedFieldsList; }
            set
            {
                if (value != _listChanges_DeletedFieldsList)
                {
                    _listChanges_DeletedFieldsList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsid_DeleteField_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsid_DeleteField_VM>>(null, _listChanges_DeletedFieldsList, "Default List"));
                }
            }
        }
        public List<ListChangesReportQsid_Unit_VM> ListChanges_AddNewUnitsList
        {
            get { return _listChanges_AddNewUnitsList; }
            set
            {
                if (value != _listChanges_AddNewUnitsList)
                {
                    _listChanges_AddNewUnitsList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsid_Unit_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsid_Unit_VM>>(null, _listChanges_AddNewUnitsList, "Default List"));
                }
            }
        }
        public List<ListChangesReportQsid_AddPhrases_VM> _listChanges_AddPhraseList { get; set; }
        public List<ListChangesReportQsid_AddPhrases_VM> ListChanges_AddPhraseList
        {
            get { return _listChanges_AddPhraseList; }
            set
            {
                if (value != _listChanges_AddPhraseList)
                {
                    _listChanges_AddPhraseList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsid_AddPhrases_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsid_AddPhrases_VM>>(null, _listChanges_AddPhraseList, "Default List"));
                }
            }
        }
        public List<ListChangesReportQsid_DeletePhrases_VM> _listChanges_DeletePhraseList { get; set; }
        public List<ListChangesReportQsid_DeletePhrases_VM> ListChanges_DeletePhraseList
        {
            get { return _listChanges_DeletePhraseList; }
            set
            {
                if (value != _listChanges_DeletePhraseList)
                {
                    _listChanges_DeletePhraseList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListChangesReportQsid_DeletePhrases_VM>>>(new PropertyChangedMessage<List<ListChangesReportQsid_DeletePhrases_VM>>(null, _listChanges_DeletePhraseList, "Default List"));
                }
            }
        }

        public ObservableCollection<ListVersionChangeTypes_VM> ListVersionChangeType_Combo { get; private set; }
        public ObservableCollection<MapProductListVM> listProductsToMap { get; set; }
        private DataGridCellInfo _cellInfo_AddNew { get; set; }
        public DataGridCellInfo CellInfo_AddNew
        {
            get { return _cellInfo_AddNew; }
            set
            {
                _cellInfo_AddNew = value;
            }
        }
        private DataGridCellInfo _cellInfo_Change { get; set; }
        public DataGridCellInfo CellInfo_Change
        {
            get { return _cellInfo_Change; }
            set
            {
                _cellInfo_Change = value;
            }
        }

        private ListChangesReportQsidAdd_VM _SelectedAddNewQsid { get; set; }
        public ListChangesReportQsidAdd_VM SelectedAddNewQsid
        {
            get => (ListChangesReportQsidAdd_VM)_SelectedAddNewQsid;
            set
            {
                if (Equals(_SelectedAddNewQsid, value))
                {
                    return;
                }
                _SelectedAddNewQsid = value;
                OpenPopupHistoryQsid_Add();
            }
        }
        private ListChangesResportQsidChanges_VM _SelectedChangedQsid { get; set; }
        public ListChangesResportQsidChanges_VM SelectedChangedQsid
        {
            get => (ListChangesResportQsidChanges_VM)_SelectedChangedQsid;
            set
            {
                if (Equals(_SelectedChangedQsid, value))
                {
                    return;
                }
                _SelectedChangedQsid = value;
                OpenPopupHistoryQsid_Changes();
            }
        }
        public Visibility VisibilityListChangesReportQSID_Loader { get; set; } = Visibility.Collapsed;
        public RelayCommand commandOpenCurrentDataViewNewAddList { get; set; }
        public RelayCommand commandOpenCurrentDataViewExistingList { get; set; }

        public string SelectedProductList { get; set; }
        public Visibility ProductVisibility { get; set; } = Visibility.Collapsed;

        public string countPhraseAdded
        {
            get { return _countPhraseAdded; }
            set
            {
                _countPhraseAdded = value;
                NotifyPropertyChanged("countPhraseAdded");
            }
        }
        public string _countPhraseAdded { get; set; } = "0";
        public string countPhraseDeleted
        {
            get { return _countPhraseDeleted; }
            set
            {
                _countPhraseDeleted = value;
                NotifyPropertyChanged("countPhraseDeleted");
            }
        }
        public string _countPhraseDeleted { get; set; }
        public ListChangesReport_ViewModel()
        {
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                InitModel();
            }
        }


        private void InitModel()
        {
            //CheckedFoodData = false;
            //CheckedExcludeFoodData = false;
            listProductsToMap = new ObservableCollection<MapProductListVM>(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapProductToViewModel(_objLibraryFunction_Service.GetAllLibraryProductLists()));
            Engine._listVersionChangeType = _objAccessVersion_Service.GetLibrary_ListVersionChangeTypes().Select(x => new ListVersionChangeTypes_VM { Id = x.ListVersionChangeTypeID, Name = x.ListVersionChangeType }).ToList();
            GetRefreshListDetailCommonControl();
            VisibilityResultListChanges = Visibility.Collapsed;


            visibilityNewList_DataGrid = Visibility.Collapsed;
            visibilityExistingList_DataGrid = Visibility.Collapsed;
            visibilityDeletedList_DataGrid = Visibility.Collapsed;
            visibilityNewfieldsList_DataGrid = Visibility.Collapsed;
            visibilityDeletedfieldsList_DataGrid = Visibility.Collapsed;
            visibilityNewUnitList_DataGrid = Visibility.Collapsed;
            visibilityNewPhraseList_DataGrid = Visibility.Collapsed;
            visibilityDeletePhraseList_DataGrid = Visibility.Collapsed;


            visibilityExpandMore_NewList = Visibility.Visible;
            visibilityExpandMore_ExistingList = Visibility.Visible;
            visibilityExpandMore_DeletedList = Visibility.Visible;
            visibilityExpandMore_NewfieldsList = Visibility.Visible;
            visibilityExpandMore_DeletedfieldsList = Visibility.Visible;
            visibilityExpandMore_NewUnitList = Visibility.Visible;
            visibilityExpandMore_NewPhraseList = Visibility.Visible;
            visibilityExpandMore_DeletePhraseList = Visibility.Visible;


            commandGetListChangeReport = new RelayCommand(commandGetListChangeReportExecuted, commandGetListChangeReportCanExecute);
            CommandNewListAdded = new RelayCommand(CommandNewListAddedExecuted, DefualtExecuteTrue);
            CommandExistingList = new RelayCommand(CommandExistingListExecuted, DefualtExecuteTrue);
            CommandDeletedList = new RelayCommand(CommandDeletedListExecuted, DefualtExecuteTrue);
            CommandNewFieldsList = new RelayCommand(CommandNewFieldsListExecuted, DefualtExecuteTrue);
            CommandDeletedFieldsList = new RelayCommand(CommandDeletedFieldsListExecuted, DefualtExecuteTrue);
            CommandNewUnitList = new RelayCommand(CommandNewUnitListExecuted, DefualtExecuteTrue);
            CommandAddPhraseList = new RelayCommand(CommandAddPhraseListExecuted);
            CommandDeletePhraseList = new RelayCommand(CommandDeletePhraseListExecuted);
            CommandGenerateWordListChangesResult = new RelayCommand(CommandGenerateWordListExecuted, DefualtExecuteTrue);
            CommandGeneratePdfListChangesResult = new RelayCommand(CommandGeneratePdfListExecuted, DefualtExecuteTrue);
            CommandGenerateAccessListChangesResult = new RelayCommand(CommandGenerateAccessListExecuted, DefualtExecuteTrue);
            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExected, commandOpenCurrentDataViewNewAddListCanExecte);
            commandOpenCurrentDataViewExistingList = new RelayCommand(commandOpenCurrentDataViewExistingListExecuted, commandOpenCurrentDataViewExistingListCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCell);
            MessengerInstance.Register<PropertyChangedMessage<ListChangesReportQsidAdd_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<ListChangesResportQsidChanges_VM>>(this, NotifyMe);
        }

        private bool commandOpenCurrentDataViewExistingListCanExecute(object obj)
        {
            if (SelectedChangedQsid == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool commandOpenCurrentDataViewNewAddListCanExecte(object obj)
        {
            if (SelectedAddNewQsid == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void commandOpenCurrentDataViewExistingListExecuted(object obj)
        {
            Engine.IsOpenCurrentDataViewFromVersionHis = true;
            Engine.CurrentDataViewQsid = SelectedChangedQsid.Qsid;
            Engine.CurrentDataViewQsidID = SelectedChangedQsid.QSID_ID;
            MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

        }
        public void NotifyMe(string callAction)
        {

        }


        private void commandOpenCurrentDataViewNewAddListExected(object obj)
        {
            Engine.IsOpenCurrentDataViewFromVersionHis = true;
            Engine.CurrentDataViewQsid = SelectedAddNewQsid.Qsid;
            Engine.CurrentDataViewQsidID = SelectedAddNewQsid.QSID_ID;
            MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));
        }

        private void NotifyMe(PropertyChangedMessage<ListChangesReportQsidAdd_VM> obj)
        {
            SelectedAddNewQsid = obj.NewValue;
        }
        private void NotifyMe(PropertyChangedMessage<ListChangesResportQsidChanges_VM> obj)
        {
            SelectedChangedQsid = obj.NewValue;
        }
        private void UpdateSourceTriggerCurrentCell(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "ListChangesReportQsidAdd_VM")
            {
                CellInfo_AddNew = obj.NewValue;
            }
            if (obj.Sender.ToString() == "ListChangesResportQsidChanges_VM")
            {
                CellInfo_Change = obj.NewValue;
            }
        }
        public void OpenPopupHistoryQsid_Add()
        {
            if (CellInfo_AddNew.Column == null)
            {
                return;
            }
            if (CellInfo_AddNew.Column.Header.ToString() == "Row Added" || CellInfo_AddNew.Column.Header.ToString() == "Cas Added")
            {
                PopUp_ListChangesReport objChangesAdd = new PopUp_ListChangesReport(SelectedAddNewQsid.QSID_ID, SelectedAddNewQsid.ProductDateMin, SelectedAddNewQsid.ProductDateMax, "NewAddedList", CellInfo_AddNew.Column.Header.ToString());
                objChangesAdd.ShowDialog();
            }
            if (CellInfo_AddNew.Column.Header.ToString() == "Count NOL")
            {
                PopUp_ListChangesReport objChangesAdd = new PopUp_ListChangesReport(SelectedAddNewQsid.QSID_ID, SelectedAddNewQsid.ProductDateMin, SelectedAddNewQsid.ProductDateMax, "NewAddedList", CellInfo_AddNew.Column.Header.ToString());
                objChangesAdd.ShowDialog();
            }
        }
        public void OpenPopupHistoryQsid_Changes()
        {
            if (CellInfo_Change.Column == null)
            {
                return;
            }
            if (CellInfo_Change.Column.Header.ToString() == "Row Added" || CellInfo_Change.Column.Header.ToString() == "Row Changed" || CellInfo_Change.Column.Header.ToString() == "Row Deleted" || CellInfo_Change.Column.Header.ToString() == "Cas Added" || CellInfo_Change.Column.Header.ToString() == "Cas Deleted" || CellInfo_Change.Column.Header.ToString() == "Fields Added" || CellInfo_Change.Column.Header.ToString() == "Fields Deleted" || CellInfo_Change.Column.Header.ToString() == "List Dictionary Changes" || CellInfo_Change.Column.Header.ToString() == "Count NOL" || CellInfo_Change.Column.Header.ToString() == "Field Explain Changes")
            {
                PopUp_ListChangesReport objChangesAdd = new PopUp_ListChangesReport(SelectedChangedQsid.QSID_ID, SelectedChangedQsid.ProductDateMin, SelectedChangedQsid.ProductDateMax, "ChangedList", CellInfo_Change.Column.Header.ToString());
                objChangesAdd.ShowDialog();
            }
        }


        public void BindListFilter_ColumnsData()
        {
            ListVersionChangeType_Combo = new ObservableCollection<ListVersionChangeTypes_VM>(Engine._listVersionChangeType);

        }
        public void GetRefreshListDetailCommonControl()
        {
            comonDGChangesReportAdd_VM = new CommonDataGrid_ViewModel<ListChangesReportQsidAdd_VM>(GetListReportAddQSIDGridCol(), "Qsid", "New List Added :");
            NotifyPropertyChanged("comonDGChangesReportAdd_VM");

            comonDGChangesReport_VM = new CommonDataGrid_ViewModel<ListChangesResportQsidChanges_VM>(GetListReportChangeQSIDGridCol(), "Qsid", "Existing List Updated :");
            NotifyPropertyChanged("comonDGChangesReport_VM");

            comonDGChangesReportDelete_VM = new CommonDataGrid_ViewModel<ListChangesReportQsidDelete_VM>(GetListReportDeletedQSIDGridCol(), "Qsid", "List Deleted :");
            NotifyPropertyChanged("comonDGChangesReportDelete_VM");
            comonDGChangesReportAddField_VM = new CommonDataGrid_ViewModel<ListChangesReportQsid_Field_VM>(GetListReportAddFieldGridCol(), "Qsid", "New fields added in existing list Excluding CAS Editorial PREF :");
            NotifyPropertyChanged("comonDGChangesReportAddField_VM");

            comonDGChangesReportDeleteField_VM = new CommonDataGrid_ViewModel<ListChangesReportQsid_DeleteField_VM>(GetListReportChangeDeleteFieldGridCol(), "Qsid", "Deleted fields in existing list :");
            NotifyPropertyChanged("comonDGChangesReportDeleteField_VM");

            comonDGChangesReportUnit_VM = new CommonDataGrid_ViewModel<ListChangesReportQsid_Unit_VM>(GetListReportUnitGridCol(), "Qsid", "New Units Added in existing list :");
            NotifyPropertyChanged("comonDGChangesReportUnit_VM");

            comonDGChangesReportAddPhrase_VM = new CommonDataGrid_ViewModel<ListChangesReportQsid_AddPhrases_VM>(GetListReportPhraseGridCol(), "Qsid", "New Phrase Added in existing list :");
            NotifyPropertyChanged("comonDGChangesReportAddPhrase_VM");

            comonDGChangesReportDeletePhrase_VM = new CommonDataGrid_ViewModel<ListChangesReportQsid_DeletePhrases_VM>(GetListReportPhraseGridCol(), "Qsid", "old Phrase Delete in existing list :");
            NotifyPropertyChanged("comonDGChangesReportDeletePhrase_VM");
        }
        private List<CommonDataGridColumn> GetListReportAddQSIDGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Short Name", ColBindingName = "ShortName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Work order Number", ColBindingName = "WoNumber", ColType = "MultipleHyperLinkCol" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Version Number", ColBindingName = "Versions", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Phrase", ColBindingName = "ListPhrase", ColType = "Textbox", ColumnWidth = "300" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Start Timestamp", ColBindingName = "ProductDateMin", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "End Timestamp", ColBindingName = "ProductDateMax", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Row Added", ColBindingName = "RowAdded", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Added", ColBindingName = "CasAdded", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Normalized Fields", ColBindingName = "NormalizedFields", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Count NOL", ColBindingName = "CountNOL", ColType = "Textbox" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportChangeQSIDGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Change Type", ColBindingName = "ListVersionChangeType", ColType = "Combobox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Work order Number", ColBindingName = "WoNumber", ColType = "MultipleHyperLinkCol" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Version Number", ColBindingName = "Versions", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "New List Phrase", ColBindingName = "NewListPhrase", ColType = "Textbox", ColumnWidth = "300" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Old List Phrase", ColBindingName = "OldListPhrase", ColType = "Textbox", ColumnWidth = "300" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Start Timestamp", ColBindingName = "ProductDateMin", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "End Timestamp", ColBindingName = "ProductDateMax", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Row Added", ColBindingName = "RowAdded", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Row Changed", ColBindingName = "RowChanged", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Row Deleted", ColBindingName = "RowDeleted", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Added", ColBindingName = "CasAdded", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Deleted", ColBindingName = "CasDeleted", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Fields Added", ColBindingName = "FieldsAdded", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Fields Deleted", ColBindingName = "FieldsDeleted", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explain Changes", ColBindingName = "FieldExplainChanges", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Dictionary Changes", ColBindingName = "ListDictionaryChanges", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Count NOL", ColBindingName = "CountNOL", ColType = "Textbox" });

            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportDeletedQSIDGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Short Name", ColBindingName = "ShortName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Work order Number", ColBindingName = "WoNumber", ColType = "MultipleHyperLinkCol" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Version Number", ColBindingName = "Versions", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Phrase", ColBindingName = "ListPhrase", ColType = "Textbox", ColumnWidth = "300" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Start Timestamp", ColBindingName = "ProductDateMin", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "End Timestamp", ColBindingName = "ProductDateMax", ColType = "Textbox" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportAddFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product Name", ColBindingName = "ProductName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAddedDeleted", ColType = "DateTime" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportChangeDeleteFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product Name", ColBindingName = "ProductName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateDeleted", ColBindingName = "DateAddedDeleted", ColType = "DateTime" });
            return listColumn;
        }



        private List<CommonDataGridColumn> GetListReportUnitGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List FieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Actual Unit", ColBindingName = "ActualUnit", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product Name", ColBindingName = "ProductName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAddedDeleted", ColType = "DateTime" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListReportPhraseGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = false, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Category", ColBindingName = "PhraseCategory", ColType = "Textbox" });           
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAddedDeleted", ColType = "DateTime" });
            return listColumn;
        }
        private bool DefualtExecuteTrue(object obj)
        {
            return true;
        }
        private bool commandGetListChangeReportCanExecute(object obj)
        {
            bool resultFlag = false;
            if (ProductDateEnd != null && ProductDateStart != null)
            {
                resultFlag = true;
            }
            return resultFlag;
        }

        private void CommandGeneratePdfListExecuted(object obj)
        {
            string exportPath = Engine.CommonAccessExportFolderPath + "ListChangesRepord_" + Guid.NewGuid() + ".pdf";
            Common.CommonFunctions.GeneratePdfDocument(exportPath, ProductDateStart, ProductDateEnd, string.Join(",", listProductsToMap.Where(x => x.IsSelected).Select(x => x.ProductName).ToArray()), ListChangesReportDetailInfromation);
            _notifier.ShowSuccess("Exported List Changes Pdf File on below :" + Environment.NewLine + exportPath);
        }



        private void CommandGenerateWordListExecuted(object obj)
        {
            string exportPath = Engine.CommonAccessExportFolderPath + "ListChangesRepord_" + Guid.NewGuid() + ".docx";
            Common.CommonFunctions.GenerateWordDocument(exportPath, ProductDateStart, ProductDateEnd, string.Join(",", listProductsToMap.Where(x => x.IsSelected).Select(x => x.ProductName).ToArray()), ListChangesReportDetailInfromation);
            _notifier.ShowSuccess("Exported List Changes Word File on below :" + Environment.NewLine + exportPath);
        }

        private void commandGetListChangeReportExecuted(object obj)
        {
            VisibilityListChangesReportQSID_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");
            visibilityPhraseAdded_Loader = Visibility.Visible;
            visibilityPhraseDeleted_Loader = Visibility.Visible;
            FetchListChangesReportDetail();

        }
        private void FetchListChangesReportDetail()
        {

            string productId = string.Join(",", listProductsToMap.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            if (string.IsNullOrEmpty(productId))
            {
                SelectedProductList = "None";
            }
            else
            {
                SelectedProductList = string.Join(", ", listProductsToMap.Where(x => x.IsSelected).Select(x => x.ProductName).ToArray()); ;
            }
            ProductVisibility = Visibility.Visible;
            NotifyPropertyChanged("ProductVisibility");
            NotifyPropertyChanged("SelectedProductList");
            System.Threading.Tasks.Task.Run(() =>
            {
                ListChangesReportDetailInfromation = _objLibraryFunction_Service.GetListChangesReportDetail(ProductDateStart, ProductDateEnd, productId);
                ListChangesResult = ListChangesReportDetailInfromation.ChangesReportSummary;
                VisibilityResultListChanges = Visibility.Visible;

                ListChanges_AddNewList = Common.CommonFunctions.ConvertMapQsidAddReportToViewModel(ListChangesReportDetailInfromation.ListChangesReport_Add);
                ListChanges_ExistingList = Common.CommonFunctions.ConvertMapQsidChangesReportToViewModel(ListChangesReportDetailInfromation.ListChangesReport_Change);
                ListChanges_DeletedList = Common.CommonFunctions.ConvertMapQsidDeleteReportToViewModel(ListChangesReportDetailInfromation.ListChangesReport_Delete);
                ListChanges_AddNewFieldsList = ListChangesReportDetailInfromation.ListChangesReport_Fields_Add;
                ListChanges_DeletedFieldsList = ListChangesReportDetailInfromation.ListChangesReport_Fields_Delete;
                ListChanges_AddNewUnitsList = ListChangesReportDetailInfromation.ListChangesReport_Unit_Add;

                VisibilityListChangesReportQSID_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("ListChanges_AddNewList");
                NotifyPropertyChanged("ListChanges_ExistingList");
                NotifyPropertyChanged("ListChanges_DeletedList");
                NotifyPropertyChanged("ListChanges_AddNewFieldsList");
                NotifyPropertyChanged("ListChanges_DeletedFieldsList");
                NotifyPropertyChanged("ListChanges_AddNewUnitsList");
                NotifyPropertyChanged("ListChangesResult");
                NotifyPropertyChanged("VisibilityResultListChanges");
                NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");

                System.Threading.Tasks.Task.Run(() =>
                {
                    ListChanges_AddPhraseList = _objLibraryFunction_Service.GetAddedPhraseDetail_ListChangesReport(ProductDateStart, ProductDateEnd, productId);
                    countPhraseAdded = ListChanges_AddPhraseList.Count.ToString();
                    visibilityPhraseAdded_Loader = Visibility.Collapsed;
                });
                System.Threading.Tasks.Task.Run(() =>
                {
                    ListChanges_DeletePhraseList = _objLibraryFunction_Service.GetDeletedPhraseDetail_ListChangesReport(ProductDateStart, ProductDateEnd, productId);
                    countPhraseDeleted = ListChanges_DeletePhraseList.Count.ToString();
                    visibilityPhraseDeleted_Loader = Visibility.Collapsed;
                });
            });
        }



        public Visibility GenericVisibleCollapsedFunction(Visibility currentVisibility)
        {

            visibilityNewList_DataGrid = Visibility.Collapsed;
            visibilityExistingList_DataGrid = Visibility.Collapsed;
            visibilityDeletedList_DataGrid = Visibility.Collapsed;
            visibilityNewfieldsList_DataGrid = Visibility.Collapsed;
            visibilityDeletedfieldsList_DataGrid = Visibility.Collapsed;
            visibilityNewUnitList_DataGrid = Visibility.Collapsed;
            visibilityNewPhraseList_DataGrid = Visibility.Collapsed;
            visibilityDeletePhraseList_DataGrid = Visibility.Collapsed;

            NotifyPropertyChanged("visibilityNewList_DataGrid");
            NotifyPropertyChanged("visibilityExistingList_DataGrid");
            NotifyPropertyChanged("visibilityDeletedList_DataGrid");
            NotifyPropertyChanged("visibilityNewfieldsList_DataGrid");
            NotifyPropertyChanged("visibilityDeletedfieldsList_DataGrid");
            NotifyPropertyChanged("visibilityNewUnitList_DataGrid");
            NotifyPropertyChanged("visibilityNewPhraseList_DataGrid");
            NotifyPropertyChanged("visibilityDeletePhraseList_DataGrid");
            if (currentVisibility == Visibility.Collapsed)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
        public Visibility GenericVisibleCollapsedFunction_Expand(Visibility currentVisibility)
        {

            visibilityExpandMore_NewList = Visibility.Visible;
            visibilityExpandMore_ExistingList = Visibility.Visible;
            visibilityExpandMore_DeletedList = Visibility.Visible;
            visibilityExpandMore_NewfieldsList = Visibility.Visible;
            visibilityExpandMore_DeletedfieldsList = Visibility.Visible;
            visibilityExpandMore_NewUnitList = Visibility.Visible;
            visibilityExpandMore_NewPhraseList = Visibility.Visible;
            visibilityExpandMore_DeletePhraseList = Visibility.Visible;

            NotifyPropertyChanged("visibilityExpandMore_NewList");
            NotifyPropertyChanged("visibilityExpandMore_ExistingList");
            NotifyPropertyChanged("visibilityExpandMore_DeletedList");
            NotifyPropertyChanged("visibilityExpandMore_NewfieldsList");
            NotifyPropertyChanged("visibilityExpandMore_DeletedfieldsList");
            NotifyPropertyChanged("visibilityExpandMore_NewUnitList");
            NotifyPropertyChanged("visibilityExpandMore_NewPhraseList");
            NotifyPropertyChanged("visibilityExpandMore_DeletePhraseList");
            if (currentVisibility == Visibility.Collapsed)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }


        private void CommandNewListAddedExecuted(object obj)
        {
            visibilityNewList_DataGrid = GenericVisibleCollapsedFunction(visibilityNewList_DataGrid);
            visibilityExpandMore_NewList = GenericVisibleCollapsedFunction_Expand(visibilityNewList_DataGrid);
            NotifyPropertyChanged("visibilityNewList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_NewList");
        }

        private void CommandExistingListExecuted(object obj)
        {
            visibilityExistingList_DataGrid = GenericVisibleCollapsedFunction(visibilityExistingList_DataGrid);
            visibilityExpandMore_ExistingList = GenericVisibleCollapsedFunction_Expand(visibilityExistingList_DataGrid);
            NotifyPropertyChanged("visibilityExistingList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_ExistingList");
        }

        private void CommandDeletedListExecuted(object obj)
        {
            visibilityDeletedList_DataGrid = GenericVisibleCollapsedFunction(visibilityDeletedList_DataGrid);
            visibilityExpandMore_DeletedList = GenericVisibleCollapsedFunction_Expand(visibilityDeletedList_DataGrid);
            NotifyPropertyChanged("visibilityDeletedList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_DeletedList");
        }

        private void CommandNewFieldsListExecuted(object obj)
        {
            visibilityNewfieldsList_DataGrid = GenericVisibleCollapsedFunction(visibilityNewfieldsList_DataGrid);
            visibilityExpandMore_NewfieldsList = GenericVisibleCollapsedFunction_Expand(visibilityNewfieldsList_DataGrid);
            NotifyPropertyChanged("visibilityNewfieldsList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_NewfieldsList");
        }
        private void CommandDeletedFieldsListExecuted(object obj)
        {
            visibilityDeletedfieldsList_DataGrid = GenericVisibleCollapsedFunction(visibilityDeletedfieldsList_DataGrid);
            visibilityExpandMore_DeletedfieldsList = GenericVisibleCollapsedFunction_Expand(visibilityDeletedfieldsList_DataGrid);
            NotifyPropertyChanged("visibilityDeletedfieldsList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_DeletedfieldsList");
        }
        private void CommandNewUnitListExecuted(object obj)
        {
            visibilityNewUnitList_DataGrid = GenericVisibleCollapsedFunction(visibilityNewUnitList_DataGrid);
            visibilityExpandMore_NewUnitList = GenericVisibleCollapsedFunction_Expand(visibilityNewUnitList_DataGrid);
            NotifyPropertyChanged("visibilityNewUnitList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_NewUnitList");
        }
        private void CommandAddPhraseListExecuted(object obj)
        {
            visibilityNewPhraseList_DataGrid = GenericVisibleCollapsedFunction(visibilityNewPhraseList_DataGrid);
            visibilityExpandMore_NewPhraseList = GenericVisibleCollapsedFunction_Expand(visibilityNewPhraseList_DataGrid);
            NotifyPropertyChanged("visibilityNewPhraseList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_NewPhraseList");
        }
        private void CommandDeletePhraseListExecuted(object obj)
        {
            visibilityDeletePhraseList_DataGrid = GenericVisibleCollapsedFunction(visibilityDeletePhraseList_DataGrid);
            visibilityExpandMore_DeletePhraseList = GenericVisibleCollapsedFunction_Expand(visibilityDeletePhraseList_DataGrid);
            NotifyPropertyChanged("visibilityDeletePhraseList_DataGrid");
            NotifyPropertyChanged("visibilityExpandMore_DeletePhraseList");
        }



        List<AccessFields> dt = new List<AccessFields>();
        private CommonFunctions objCommonFunc;
        private void CommandGenerateAccessListExecuted(object obj)
        {
            objCommonFunc = new CommonFunctions();
            string productId = string.Join(",", listProductsToMap.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            string exportPath = Engine.CommonAccessExportFolderPath + "ListChangesReport_" + System.DateTime.Now.ToString("HHmmss") + ".mdb";
            var stringSqlCon = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;

            var dtAdd = objCommonFunc.DbaseQueryReturnSqlList<ListChangesReportQsid_Add_VM>("Execute GetListChangesReport  " + "'GetQSIDListDetail', 'A', '" + productId + "', '" + ProductDateStart + "', '" + ProductDateEnd + "'", stringSqlCon);

            var dtChg = objCommonFunc.DbaseQueryReturnSqlList<ListChangesReportQsid_Change_VM>("Execute GetListChangesReport  " + "'GetQSIDListDetail_Change','C', '" + productId + "', '" + ProductDateStart + "', '" + ProductDateEnd + "'", stringSqlCon);

            var dtDel = objCommonFunc.DbaseQueryReturnSqlList<ListChangesReportQsid_Delete_VM>("Execute GetListChangesReport  " + "'GetQSIDListDetail_Delete','D', '" + productId + "', '" + ProductDateStart + "', '" + ProductDateEnd + "'", stringSqlCon);

            var dtAddN = dtAdd.Select(x => new { PDFNAME = x.Qsid, Explain = string.Empty, QSID_ID = x.QSID_ID, FLDNAME = x.ListFieldName == null ? string.Empty : x.ListFieldName, Change_type = "New List" }).ToList();
            var dtDelN = dtDel.Select(x => new { PDFNAME = x.Qsid, Explain = string.Empty, QSID_ID = x.QSID_ID, FLDNAME = x.ListFieldName == null ? string.Empty : x.ListFieldName, Change_type = "Deleted List" }).ToList();
            using (var context = new CRAModel())
            {
                var dtNew = (from d1 in dtChg
                             join d2 in context.Library_ListVersionChangeTypes.AsNoTracking()
                             on d1.ListVersionChangeTypeID equals d2.ListVersionChangeTypeID
                             select new
                             {
                                 PDFNAME = d1.Qsid,
                                 Explain = string.Empty,
                                 d1.QSID_ID,
                                 FLDNAME = d1.ListFieldName == null ? string.Empty : d1.ListFieldName,
                                 Change_type = d2.ListVersionChangeType,
                             }).ToList();
                dtNew.AddRange(dtAddN);
                dtNew.AddRange(dtDelN);
                var FieldExplain = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Field Explanation").Select(y => y.PhraseCategoryID).FirstOrDefault();
                var joinWithExplain = (from d1 in dtNew
                                       join d2 in context.QSxxx_DataDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == FieldExplain).ToList()
                                       on d1.QSID_ID equals d2.QSID_ID
                                       join d3 in context.Library_Phrases.AsNoTracking()
                                       on d2.PhraseID equals d3.PhraseID
                                       select new { d1.PDFNAME, d3.Phrase }).ToList();

                var dtFinal = dtNew.Select(x => new
                {
                    x.PDFNAME,
                    x.Change_type,
                    x.FLDNAME,
                    EXPLAIN = joinWithExplain.Count(y => y.PDFNAME == x.PDFNAME) == 0 ? string.Empty : joinWithExplain.Where(y => y.PDFNAME == x.PDFNAME).Select(z => z.Phrase).FirstOrDefault(),
                    //Action = string.Empty,
                    //Change_Table = string.Empty,
                    //Check_Notes = string.Empty,
                    //Completed = string.Empty,
                    //CRA = string.Empty,
                    //Data_Impact = string.Empty,
                    //DI = string.Empty,
                    //ERC_Rules = string.Empty,
                    //RegType = string.Empty,
                    //Test_Cas = string.Empty,
                    //Update_issue = string.Empty
                }).ToList();
                var dt = objCommonFunc.ConvertToDataTable(dtFinal);
                dt.TableName = "Optimize_update_action_list";
                var resultMessage = ExportAccessFile(dt, exportPath);
            }
            //var dt = objCommonFunc.DbaseQueryReturnTableSql("Select distinct vch.QSID_ID as PDFNAME,mp.ListVersionChangeType as Change_type, lp.Phrase as FLDNAME, isnull(lpt.Phrase,'') as EXPLAIN, '' as Action, '' as Change_Table, '' as Check_Notes, '' as Completed, '' as CRA, '' as Data_Impact, '' as DI, '' as ERC_Rules, '' as RegType, '' as Test_Cas, '' as Update_issue," +
            //" (Select distinct top 1 ProductTimeStamp from Log_VersionControl_History where QSID_ID = vch.QSID_ID " +
            //" and ProductTimeStamp < convert(datetime, '" + ProductDateStart + "') " +
            //" order by ProductTimeStamp desc) as ProductMinVal, " +
            //" (Select distinct top 1  ProductTimeStamp from Log_VersionControl_History " +
            //" where QSID_ID = vch.QSID_ID and ProductTimeStamp <= convert(datetime, '" + ProductDateEnd + "') " +
            //" order by ProductTimeStamp desc) as ProductMaxVal " +
            //" from Log_VersionControl_History vch " +
            //" inner " +
            //" join Library_ListVersionChangeTypes mp on " +
            //" vch.ListVersionChangeTypeID = mp.ListVersionChangeTypeID " +
            //" left " +
            //" join QSxxx_ListDictionary ld on ld.QSID_ID = vch.QSID_ID and ld.PhraseCategoryID = 20 " +
            //" left join Library_Phrases lp on ld.PhraseID = lp.phraseid " +
            //" left join QSxxx_ListDictionary ldt on ldt.QSID_ID = vch.QSID_ID and ldt.PhraseCategoryID = 82 " +
            //" left join Library_Phrases lpt on ldt.PhraseID = lpt.phraseid ", stringSqlCon);
            //dt.Columns.Remove("ProductMinVal");
            //dt.Columns.Remove("ProductMaxVal");

            _notifier.ShowSuccess("Exported List Changes Access File on below :" + Environment.NewLine + exportPath);
        }

        public System.Data.DataTable ConvertDataTableFromListDetail(List<AccessFields> listDetails)
        {
            System.Data.DataTable table = new System.Data.DataTable();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(AccessFields));


            foreach (PropertyDescriptor prop in properties)
            {
                //CommonDataGridColumn selectCol = new CommonDataGridColumn();
                var selectCol = dt.Select(x => x.GetType().GetProperty(prop.Name).GetValue(x, null)).FirstOrDefault();
                //defaultListColumnGrid.Where(x => x.ColBindingName.IndexOf(".") > 0 ? x.ColBindingName.Split('.').FirstOrDefault().Contains(prop.Name) : x.ColBindingName == prop.Name).FirstOrDefault();
                if (selectCol != null)
                {
                    //if (selectCol.ColType == "Checkbox")
                    //{
                    //    table.Columns.Add(prop.Name, typeof(bool));
                    //}
                    //else
                    //{
                    table.Columns.Add(prop.Name, typeof(string));
                    //}
                }

            }
            foreach (AccessFields item in listDetails)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    //CommonDataGridColumn selectCol = new CommonDataGridColumn();
                    var selectCol = dt.Select(x => x.GetType().GetProperty(prop.Name).GetValue(x, null)).FirstOrDefault();
                    //defaultListColumnGrid.Where(x => x.ColBindingName.IndexOf(".") > 0 ? x.ColBindingName.Split('.').FirstOrDefault().Contains(prop.Name) : x.ColBindingName == prop.Name).FirstOrDefault();
                    if (selectCol != null)
                    {
                        //if (selectCol.ColType == "Checkbox")
                        //{
                        //    var obj = prop.GetValue(item);
                        //    row[prop.Name] = obj != null ? (obj.GetType().GetProperty("Value").GetValue(obj).ToString()) : "";
                        //}
                        //else if (selectCol.ColType == "Combobox")
                        //{
                        //    var obj = prop.GetValue(item);
                        //    row[prop.Name] = obj != null ? (obj.GetType().GetProperty("Name").GetValue(obj) != null ? obj.GetType().GetProperty("Name").GetValue(obj).ToString() : "") : "";
                        //}
                        //else if (selectCol.ColType == "MultipleHyperLinkCol")
                        //{
                        //    List<HyperLinkDataValue> obj = (List<HyperLinkDataValue>)prop.GetValue(item);
                        //    row[prop.Name] = obj != null ? string.Join(", ", obj.Select(y => y.DisplayValue).ToArray()) : "";
                        //}
                        //else
                        //{
                        //if (prop.PropertyType.FullName == "CRA_DataAccess.ViewModel.CasType")
                        //{
                        //    var obj = prop.GetValue(item);
                        //    row[prop.Name] = obj != null ? (obj.GetType().GetProperty("CasFormatted").GetValue(obj) != null ? obj.GetType().GetProperty("CasFormatted").GetValue(obj).ToString() : "") : "";
                        //}
                        //else
                        //{
                        row[prop.Name] = prop.GetValue(item) != null ? prop.GetValue(item).ToString() : "";
                        //}
                        //}
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public string ExportAccessFile(System.Data.DataTable _objDataTable, string fileName)
        {
            try
            {
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                // IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable, fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public class AccessFields
        {
            public string PDFNAME { get; set; }
            public string FLDNAME { get; set; }

            public string EXPLAIN { get; set; }
            public string REGTYPE { get; set; }
            public string Change_type { get; set; }
            public string Change_table { get; set; }
            public string Check_notes { get; set; }
            public string Action { get; set; }
            public string Data_Impact { get; set; }
            public string Update_Issue { get; set; }
            public string Completed { get; set; }
            public string Cra { get; set; }
            public string Di { get; set; }
            public string Test_cas { get; set; }
            public string ERC_rules { get; set; }
        }

    }
}
