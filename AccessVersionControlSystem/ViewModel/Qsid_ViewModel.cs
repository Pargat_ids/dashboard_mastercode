﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using DataGridFilterLibrary;
using DataGridFilterLibrary.Querying;
using DocumentFormat.OpenXml.Office.CoverPageProps;
using GalaSoft.MvvmLight.Messaging;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Business.Common;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;
using DataGridFilterLibrary.Support;
using System.Windows.Data;
using System.Windows.Markup;

namespace AccessVersionControlSystem.ViewModel
{
    public class Qsid_ViewModel : Library.Base_ViewModel
    {

        #region PropertyMember

        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public bool listNomalizationDisable { get; set; } = true;
        public bool IsDisplayCheckIn { get; set; }
        public bool IsDisplayCheckOut { get; set; }
        public bool IsDisplayUnLock { get; set; }
        public bool IsSelectedCasAssigmentTab { get; set; }
        public bool IsSelectedCurrentDataView { get; set; }
        public bool IsSelectedNormalization { get; set; }
        public bool IsSelectedAdditionalInfoView { get; set; }
        public bool IsSelectedGenerics { get; set; }
        private bool _isSelectedSubstanceIdentityTab { get; set; }
        public bool IsSelectedSubstanceIdentityTab
        {
            get { return _isSelectedSubstanceIdentityTab; }
            set
            {
                _isSelectedSubstanceIdentityTab = value;
                if (_isSelectedSubstanceIdentityTab)
                {
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", "", "RefreshDetailGrids"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
                }
            }
        }

        public bool IsSelectedVersionHistoryTab
        {
            get; set;
        }


        public string IsSearchCriteriaVisible { get; set; }
        public string CurrentUser { get; set; }
        public string CurrentProgramVersion { get; set; }

        public string CheckedOutByUser { get; set; }
        public string NormalizedByUser { get; set; }
        public Visibility VisibilityCheckedOutByUser { get; set; }
        public Visibility VisibilityNormalizedByUser { get; set; } = Visibility.Collapsed;

      
        public ObservableCollection<EntityClass.Library_ListSpecificTools> listSpecificTools { get; set; } = new();
        public EntityClass.Library_ListSpecificTools SelectedTool { get; set; }

        public string _searchCriteria { get; set; }
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (_searchCriteria != value)
                {
                    _searchCriteria = value;
                    NotifyPropertyChanged("SearchCriteria");
                    if (String.IsNullOrEmpty(_searchCriteria))
                    {
                        IsSearchCriteriaVisible = "Hidden";
                    }
                    else
                    {
                        IsSearchCriteriaVisible = "Visible";
                    }
                    NotifyPropertyChanged("IsSearchCriteriaVisible");
                }
            }
        }
        public string SearchTypeFilter { get; set; }
        public string _selectedNormalization { get; set; }
        public string SelectedNormalization
        {
            get { return _selectedNormalization; }
            set
            {
                if (_selectedNormalization != value)
                {
                    _selectedNormalization = value;
                }
                if (value == "Data-Dictionary")
                {
                    CommandDataDictionaryExecuted();
                }
                if (value == "List-Dictionary")
                {
                    CommandListDictionaryExecuted();
                }
                if (value == "Additional-Info")
                {
                    CommandAddInfoExecuted();
                }
                if (value == "Uniqueness-Criteria")
                {
                    CommandUniqueCriteriaExecuted();
                }
            }
        }

        public SelectListItem _selectedSearchType { get; set; }
        public SelectListItem SelectedSearchType
        {
            get { return _selectedSearchType; }
            set
            {
                if (value == null)
                {
                    _selectedSearchType = value;
                    SearchTypeFilter = string.Empty;
                    return;
                }
                if (_selectedSearchType != value)
                {
                    _selectedSearchType = value;
                    SearchTypeFilter = value.Text;
                    FilterQSIDGrid();
                }
                if (!string.IsNullOrEmpty(SearchTypeFilter))
                {
                    if (SearchTypeFilter == "WorkOrder Number" || SearchTypeFilter == "Notes" || SearchTypeFilter == "CSCTicket Number")
                    {
                        TextFilterVisibility = Visibility.Visible;
                        DateRangeFilterVisibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TextFilterVisibility = Visibility.Collapsed;
                        DateRangeFilterVisibility = Visibility.Visible;
                    }
                    NotifyPropertyChanged("TextFilterVisibility");
                    NotifyPropertyChanged("DateRangeFilterVisibility");
                }
            }
        }
        private bool _IsSelectedListRelationship { get; set; }
        public bool IsSelectedListRelationship
        {
            get
            {
                return _IsSelectedListRelationship;
            }
            set
            {
                _IsSelectedListRelationship = value;
                if (_IsSelectedListRelationship)
                {
                    //comListRelationship = new MapListRelationShip_VM();
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "ListRel", "Default List"), typeof(MapListRelationShip_VM));
                }
            }
        }
        public ObservableCollection<SelectListItem> ListSearchType { get; set; }
        public ObservableCollection<string> ListNormalization { get; set; }

        public Visibility VisibilityListDetailDashboard_Loader { get; set; }

        public Visibility VisibilityNormalizedHis_Loader { get; set; }
        public Visibility VisibilityVersionHis_Loader { get; set; }


        public Visibility VisibilitySideNav { get; set; }
        public Visibility VisibilityMenuButton { get; set; }


        public QueryController _queryController { get; set; }

        public QueryController CustomQueryController
        {
            get { return _queryController; }
            set
            {
                if (_queryController != value)
                {
                    _queryController = value;
                }
            }
        }



        public List<Library_QSID_Information_VM> ListQsidDetailInfromation { get; set; }
        private ObservableCollection<Library_QSID_Information_VM> _listQSID_VM { get; set; } = new ObservableCollection<Library_QSID_Information_VM>();
        public ObservableCollection<Library_QSID_Information_VM> ListQSID_VM
        {
            get { return _listQSID_VM; }
            set
            {
                if (_listQSID_VM != value)
                {
                    _listQSID_VM = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Library_QSID_Information_VM>>>(new PropertyChangedMessage<List<Library_QSID_Information_VM>>(_listQSID_VM.ToList(), _listQSID_VM.ToList(), "Default List"));
                }
            }
        }


        public ObservableCollection<ListCountries_ViewModel> ListCountry { get; set; } = new ObservableCollection<ListCountries_ViewModel>();
        public ObservableCollection<ListModule_ViewModel> ListModule { get; set; } = new ObservableCollection<ListModule_ViewModel>();
        public ObservableCollection<ListTopic_ViewModel> ListTopic { get; set; } = new ObservableCollection<ListTopic_ViewModel>();
        public List<Access_QSID_Data_VM> _listQSID_VersionHistory { get; set; } = new List<Access_QSID_Data_VM>();
        public List<Access_QSID_Data_VM> ListQSID_VersionHistory
        {
            get { return _listQSID_VersionHistory; }
            set
            {
                if (_listQSID_VersionHistory != value)
                {
                    _listQSID_VersionHistory = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Access_QSID_Data_VM>>>(new PropertyChangedMessage<List<Access_QSID_Data_VM>>(null, _listQSID_VersionHistory, "Default List"));
                }
            }
        }
        private List<Log_Session_NormalizeHistory_VM> _listQsid_NormalizedHistory { get; set; } = new List<Log_Session_NormalizeHistory_VM>();
        public List<Log_Session_NormalizeHistory_VM> ListQsid_NormalizedHistory
        {
            get { return _listQsid_NormalizedHistory; }
            set
            {
                if (_listQsid_NormalizedHistory != value)
                {
                    _listQsid_NormalizedHistory = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Log_Session_NormalizeHistory_VM>>>(new PropertyChangedMessage<List<Log_Session_NormalizeHistory_VM>>(null, _listQsid_NormalizedHistory, "Default List"));
                }
            }
        }
        public Library_QSID_Information_VM _SelectedQSID_VM { get; set; }
        public Access_QSID_Data_VM _SelectedQSID_Version { get; set; }

        public Log_Session_NormalizeHistory_VM _SelectedNormalizedHistory { get; set; }
        public List<filterList> _listfilter { get; set; } = new List<filterList>();
        public DateTime? _productionDate_From { get; set; }
        public DateTime? ProductionDate_From
        {
            get { return this._productionDate_From; }
            set
            {
                if (this._productionDate_From != value)
                {
                    this._productionDate_From = value;

                    if (this._productionDate_From != null)
                    {
                        FilterQSIDGrid();
                    }
                }
            }
        }
        public DateTime? _productionDate_To { get; set; }
        public DateTime? ProductionDate_To
        {
            get { return this._productionDate_To; }
            set
            {
                if (this._productionDate_To != value)
                {
                    this._productionDate_To = value;
                    if (this._productionDate_To != null)
                    {
                        FilterQSIDGrid();
                    }
                }
            }
        }


        public string _searchText { get; set; }
        public string SerachText
        {
            get { return this._searchText; }
            set
            {
                if (this._searchText != value)
                {
                    this._searchText = value;
                    FilterQSIDGrid();
                }
            }
        }

        public Library_QSID_Information_VM CurrentSelectedQSID_VM { get; set; }

        public Library_QSID_Information_VM SelectedQSID_VM
        {
            get => (Library_QSID_Information_VM)_SelectedQSID_VM;
            set
            {
                if (value != null)
                {

                    if (Equals(_SelectedQSID_VM, value))
                    {
                        return;
                    }
                    _SelectedQSID_VM = value;
                    if (_SelectedQSID_VM != null)
                    {
                        Qsid = _SelectedQSID_VM.QSID + " ( " + _SelectedQSID_VM.ListFieldName + " )";
                    }
                    else
                    {
                        Qsid = " ";
                    }
                    NotifyPropertyChanged("Qsid");
                    SelectedNormalization = "--Select--";
                    NotifyPropertyChanged("SelectedNormalization");
                    // CurrentSelectedQSID_VM = _SelectedQSID_VM;
                    LoadVersionHistoryAccordingToQSID();
                    LoadNormalizedHistoryAccordingToQSID();
                    EnableDisableControl();
                    GetAlreadyCheckOutUser();
                    GetNormalizedByUser();
                    CheckActiveRDButton();
                    CheckActiveNormButton();

                }
            }
        }
        public void CheckActiveNormButton()
        {
            IsActiveNormButton = SelectedQSID_VM.IsDeleted.Value;
            tooltipNormButton = IsActiveNormButton ? "Deleted list should not be allowed to be normalized" : "";
            NotifyPropertyChanged("tooltipNormButton");
        }
        public void CheckActiveRDButton()
        {
            IsActiveRdButton = _objLibraryFunction_Service.CheckQsidForRDProduct(SelectedQSID_VM.QSID_ID);
            tooltipRDButton = !IsActiveRdButton ? "This is only available for list associated with RD & I4F product" : "";
            NotifyPropertyChanged("tooltipRDButton");
        }
        public void GetAlreadyCheckOutUser()
        {
            if (SelectedQSID_VM != null)
            {

                CheckedOutByUser = _objAccessVersion_Service.GetAlreadyCheckOutUserByQSID(SelectedQSID_VM.QSID_ID);
                if (string.IsNullOrEmpty(CheckedOutByUser))
                {
                    VisibilityCheckedOutByUser = Visibility.Collapsed;

                }
                else
                {
                    VisibilityCheckedOutByUser = Visibility.Visible;
                }
                NotifyPropertyChanged("VisibilityCheckedOutByUser");
                NotifyPropertyChanged("CheckedOutByUser");
            }
        }
        public void GetNormalizedByUser()
        {
            if (SelectedQSID_VM != null)
            {

                NormalizedByUser = _objLibraryFunction_Service.GetNormalizedUserByQSID(SelectedQSID_VM.QSID_ID);
                if (string.IsNullOrEmpty(NormalizedByUser))
                {
                    VisibilityNormalizedByUser = Visibility.Collapsed;

                }
                else
                {
                    VisibilityNormalizedByUser = Visibility.Visible;
                }
                NotifyPropertyChanged("VisibilityNormalizedByUser");
                NotifyPropertyChanged("NormalizedByUser");
            }
        }

        public Access_QSID_Data_VM SelectedQSID_Version
        {
            get => (Access_QSID_Data_VM)_SelectedQSID_Version;
            set
            {
                if (Equals(_SelectedQSID_Version, value))
                {
                    return;
                }

                _SelectedQSID_Version = value;

            }
        }
        public Log_Session_NormalizeHistory_VM SelectedNormalizedHistory
        {
            get => (Log_Session_NormalizeHistory_VM)_SelectedNormalizedHistory;
            set
            {
                if (Equals(_SelectedNormalizedHistory, value))
                {
                    return;
                }
                _SelectedNormalizedHistory = value;
                OpenPopupNormalizedHistory();
            }
        }
        public CRA_CasAssignment.ViewModel.CasAssignment_ViewModel casAssignmentDataContext { get; set; }
        public CurrentDataView.ViewModel.Win_Administrator_VM CurrentDataViewDataContext { get; set; }
        public NormalizationTool.ViewModel.MainControl_VM NormalizationToolContext { get; set; }
        public SAPDataView.ViewModel.Win_Administrator_VM SAPDataViewDataContext { get; set; }
        public QATestCases.ViewModel.Win_Administrator_VM QADataContext { get; set; }
        public Substane_Identity.ViewModel.Win_Administrator_VM SIDataViewDataContext { get; set; }
        public Libraries.ViewModel.Win_Administrator_VM LibDataViewDataContext { get; set; }

        public MapListRelationShip_VM comListRelationship { get; set; }

        public I4FTab.ViewModel.Win_Administrator_VM I4FDataViewDataContext { get; set; }
        public BulkNormalization.ViewModel.Win_Administrator_VM BulkDataViewDataContext { get; set; }

        public CurrentDataView.ViewModel.AdditionalInfoHistory_VM AdditionalInfoDataContext { get; set; }

        public ICommand CommandAddNote { get; private set; }
        public ICommand CommandAddProductTimestamp { get; private set; }
        public ICommand CommandAddNewList { get; private set; }
        public ICommand CommandGetCopyList { get; private set; }
        public ICommand CommandCheckInList { get; private set; }
        public ICommand CommandCheckOutList { get; private set; }
        public ICommand CommandUnLock { get; private set; }
        public ICommand CommandClearProductionDate { get; private set; }
        public ICommand CommandSearchCas { get; private set; }

        public ICommand CommandInitiateNormalization { get; private set; }
        public ICommand CommandGenerateExcelListDetail { get; private set; }
        public ICommand CommandClearProductTimeStamp { get; private set; }

        public ICommand CommandCloseMenu { get; private set; }
        public ICommand CommandOpenMenu { get; private set; }

        public ICommand CommandExportToOldPath { get; private set; }
        public ICommand CommandRefreshListDetail { get; private set; }
        public ICommand CommandUpdateCheckedInfo { get; private set; }

        public ICommand CommandRunCurrentDataView { get; private set; }
        // public ICommand CommandDataDictionary { get; private set; }
        //public ICommand CommandListDictionary { get; private set; }
        public ICommand CommandRunAdditionalInfo { get; private set; }
        public ICommand CommandRunCasAssignment { get; private set; }
        public ICommand CommandRunApplicationNameMapping { get; private set; }
        public ICommand CommandRunRdMapping { get; private set; }

        public DataGridCellInfo _cellInfo { get; set; }
        public DataGridCellInfo CellInfo
        {
            get { return _cellInfo; }
            set
            {
                _cellInfo = value;
            }
        }
        public TabItem _selectedTabItem { get; set; } = new TabItem();
        public TabItem SelectedTabItem
        {
            get
            {
                return _selectedTabItem;
            }
            set
            {
                if (_selectedTabItem != value)
                {
                    _selectedTabItem = value;
                    if (!IsFirstTime)
                    {
                        ExecuteFunctionalityOnTabChanged();
                    }
                    else
                    {
                        IsFirstTime = false;
                    }
                    if (_selectedTabItem.Header != null && _selectedTabItem.Header.ToString() == "Generics")
                    {
                        MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("GenericTabChanged", "GenericTabChanged", "TabChange"), typeof(GenericManagementTool.ViewModels.Generics_VM));
                    }

                }
            }
        }
        public int selectedIndexTab { get; set; }
        public DataGridFilterCommand _commandDataGridClearFilter { get; set; }
        public DataGridFilterCommand CommandDataGridClearFilter
        {
            get { return _commandDataGridClearFilter; }
            set
            {
                _commandDataGridClearFilter = new DataGridFilterCommand(new Action<object>(CommandDataGridClearFilterExecute));
            }
        }
        public ICommand CommandDataGridFilterFocus { get; private set; }
        public bool IsFirstTime { get; set; }
        public string ListDetailFilterQueryString { get; set; }
        public CommonDataGrid_ViewModel<Library_QSID_Information_VM> comonDG_VM { get; set; }
        public CommonDataGrid_ViewModel<Access_QSID_Data_VM> commonDGQsidVersion_VM { get; set; }
        public CommonDataGrid_ViewModel<Log_Session_NormalizeHistory_VM> commonDGQsidNormalizationHistory_VM { get; set; }
        public bool popUpAddNoteVisibility { get; set; } = false;
        private string _listNote { get; set; }
        public string ListNoteUrl
        {
            get { return _listNote; }
            set
            {
                if (_listNote != value)
                {
                    _listNote = value;
                    NotifyPropertyChanged("ListNoteUrl");
                }
            }
        }
        public ICommand CommandAddNoteURL { get; private set; }
        public ICommand CommandCancelNoteURL { get; private set; }
        public bool IsActiveRdButton { get; set; } = false;
        public string tooltipRDButton { get; set; } = string.Empty;
        public bool IsActiveNormButton { get; set; } = false;

        public string tooltipNormButton { get; set; } = string.Empty;
        public ObservableCollection<SelectListItem> ListSearchTypeCondition { get; set; }
        private SelectListItem _selectedSearchTypeCondition { get; set; }
        public SelectListItem SelectedSearchTypeCondition
        {
            get { return _selectedSearchTypeCondition; }
            set
            {
                if (value != _selectedSearchTypeCondition)
                {

                    _selectedSearchTypeCondition = value;
                    NotifyPropertyChanged("SelectedSearchTypeCondition");
                }
            }
        }
        public string _inputTextSearch { get; set; }
        public string InputTextSearch
        {
            get { return _inputTextSearch; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (SearchTypeFilter == "WorkOrder Number")
                    {
                        if (!value.All(char.IsDigit))
                        {
                            _notifier.ShowError("WorkOrder input should be numeric.");
                            return;
                        }
                    }

                    if (value != _inputTextSearch)
                    {
                        _inputTextSearch = value;
                    }
                }
                else
                {
                    _inputTextSearch = value;
                }
                FilterQSIDGrid();
                NotifyPropertyChanged("InputTextSearch");
            }
        }

        public Visibility TextFilterVisibility { get; set; } = Visibility.Collapsed;
        public Visibility DateRangeFilterVisibility { get; set; } = Visibility.Visible;

        public ICommand CommandRunExeOfListSpecificTools { get; set; }
        public ICommand CommondOpenUserCommentNotification { get; set; }
        public bool ShowPopUpUserNotificationDetail { get; set; }
        public ICommand CommandMarkReviewUserComments { get; set; }
        public ICommand CommandFilterInTreeView { get; set; }
        public ICommand CommandFilterInSubstanceIdentity { get; set; }
        public ICommand CommandFilterInCasAssignment { get; set; }
        public ObservableCollection<UserComments_VM> listUserComments { get; set; } = new ObservableCollection<UserComments_VM>();
        #endregion

        #region Member function
        private bool _IsSelectedListLevelMetaData { get; set; }
        public bool IsSelectedListLevelMetaData
        {
            get
            {
                return _IsSelectedListLevelMetaData;
            }
            set
            {
                _IsSelectedListLevelMetaData = value;
                if (_IsSelectedListLevelMetaData)
                {
                    Engine.CurrentDataViewQsidID = SelectedQSID_VM.QSID_ID;
                    //comListRelationship = new MapListRelationShip_VM();
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "ListMetaData", "Default List"), typeof(MapListLevelMetaData));
                }
            }
        }



        public Qsid_ViewModel()
        {
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                InitModel();
                if (_notifier == null)
                {
                    MessageBox.Show("Notifier null");
                }
            }
        }


        private void InitModel()
        {
            BindListFilterFromLibrary();
            casAssignmentDataContext = new CRA_CasAssignment.ViewModel.CasAssignment_ViewModel();
            CurrentDataViewDataContext = new CurrentDataView.ViewModel.Win_Administrator_VM();
            NormalizationToolContext = new NormalizationTool.ViewModel.MainControl_VM();
            SAPDataViewDataContext = new SAPDataView.ViewModel.Win_Administrator_VM();
            QADataContext = new QATestCases.ViewModel.Win_Administrator_VM();
            SIDataViewDataContext = new Substane_Identity.ViewModel.Win_Administrator_VM();
            LibDataViewDataContext = new Libraries.ViewModel.Win_Administrator_VM();
            I4FDataViewDataContext = new I4FTab.ViewModel.Win_Administrator_VM();
            BulkDataViewDataContext = new BulkNormalization.ViewModel.Win_Administrator_VM();

            AdditionalInfoDataContext = new CurrentDataView.ViewModel.AdditionalInfoHistory_VM();
            IsFirstTime = true;
            CurrentUser = Environment.UserName;
            IsSearchCriteriaVisible = "Hidden";            
            CustomQueryController = new QueryController();
            VisibilitySideNav = Visibility.Hidden;
            VisibilityMenuButton = Visibility.Visible;
            VisibilityCheckedOutByUser = Visibility.Hidden;
            VisibilityNormalizedHis_Loader = Visibility.Collapsed;
            VisibilityVersionHis_Loader = Visibility.Collapsed;

            BindListFilter_ColumnsData();
            BindQSIDGrid();
            BindFilterProductTypeCombo();
            BindNormalization();
            BindTextFilterWorkOrderAndNote();
            ShowActiveTabCasAssignment();
            GetRefreshListDetailCommonControl();
            GetLibraryListSpecificTools();
            BindMapUserCommentList();
            RefreshMapUserComments();
            CommandAddNewList = new RelayCommand(CommandAddNewListExecute, CommandAddNewListCanExecute);
            CommandGetCopyList = new RelayCommand(CommandGetCopyListExecute, CommandGetCopyListCanExecute);
            CommandCheckInList = new RelayCommand(CommandCheckInListExecute, CommandCheckInListCanExecute);
            CommandCheckOutList = new RelayCommand(CommandCheckOutListExecute, CommandCheckOutListCanExecute);
            CommandUnLock = new RelayCommand(CommandUnLockListExecute, CommandUnLockListCanExecute);
            CommandAddNote = new RelayCommand(CommandAddNoteExecute, CommandAddNoteCanExecute);
            CommandAddProductTimestamp = new RelayCommand(CommandAddProductTimestampExecute, CommandAddProductTimestampCanExecute);
            CommandClearProductionDate = new RelayCommand(CommandClearProductionDateExecute, CommandClearProductionDateCanExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            //  CommandDataGridFilterFocus = new RelayCommand(CommandDataGridFilterFocusExecute, CommandDataGridFilterCanExecute);
            CommandGenerateAccessCasResult = new RelayCommand(CommandGenerateAccessListDetailExecuted, CommandGenerateAccessListDetailCanExecute);
            CommandGenerateExcelCasResult = new RelayCommand(CommandGenerateExcelListDetailExecuted, CommandGenerateAccessListDetailCanExecute);
            CommandInitiateNormalization = new RelayCommand(CommandInitiateNormalizationExecuted, CommandInitiateNormalizationCanExecute);
            CommandClearProductTimeStamp = new RelayCommand(CommandClearProductTimeStampExecuted, CommandClearProductTimeStampCanExecute);
            CommandCloseMenu = new RelayCommand(CommandCloseMenuExecuted, CommandDefaultTrueCanExecute);
            CommandOpenMenu = new RelayCommand(CommandOpenMenuExecuted, CommandDefaultTrueCanExecute);
            CommandExportToOldPath = new RelayCommand(CommandExportToOldPathExecuted, CommandExportToOldPathCanExecute);
            CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;
            CommandRefreshListDetail = new RelayCommand(CommandRefreshListDetailExecuted, CommandDefaultTrueCanExecute);
            CommandUpdateCheckedInfo = new RelayCommand(CommandUpdateCheckedInfoExecuted, CommandUpdateCheckedInfoCanExecute);
            CommandRunCasAssignment = new RelayCommand(CommandRunCasAssignmentExecuted, CommandInitiateOtherCanExecute);
            CommandRunApplicationNameMapping = new RelayCommand(CommandRunApplicationNameMappingExecuted, CommandRDAndApplicationNameMappingCanExecute);
            CommandRunCurrentDataView = new RelayCommand(CommandRunCurrentDataViewExecuted, CommandInitiateOtherCanExecute);
            //CommandDataDictionary = new RelayCommand(CommandDataDictionaryExecuted, CommandInitiateOtherCanExecute);
            //CommandListDictionary = new RelayCommand(CommandListDictionaryExecuted, CommandInitiateOtherCanExecute);
            CommandRunAdditionalInfo = new RelayCommand(CommandRunAdditionalInfoviewExecuted, CommandInitiateOtherCanExecute);
            CommandRunRdMapping = new RelayCommand(CommandRunRdMappingExecuted, CommandRDAndApplicationNameMappingCanExecute);
            CommandAddNoteURL = new RelayCommand(CommandAddNoteURLExecuted, CommandAddNoteURLCanExecute);
            CommandCancelNoteURL = new RelayCommand(CommandCancelNoteURLExecuted, CommandDefaultTrueCanExecute);
            CommandRunExeOfListSpecificTools = new RelayCommand(CommandRunExeOfListSpecificToolsExecuted);
            CommondOpenUserCommentNotification = new RelayCommand(CommondOpenUserCommentNotificationExecute);
            CommandMarkReviewUserComments = new RelayCommand(CommandMarkReviewUserCommentsExecute);
            CommandFilterInTreeView = new RelayCommand(CommandFilterInTreeViewExecute);
            CommandFilterInSubstanceIdentity = new RelayCommand(CommandFilterInSubstanceIdentityExecute);
            CommandFilterInCasAssignment = new RelayCommand(CommandFilterInCasAssignmentExecute);


            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCell);
            MessengerInstance.Register<PropertyChangedMessage<Library_QSID_Information_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<Access_QSID_Data_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<Log_Session_NormalizeHistory_VM>>(this, NotifyMe);
            MessengerInstance.Register<NotificationMessageAction<Library_QSID_Information_VM>>(this, CallBackBindListDetailGridExecute);
            MessengerInstance.Register<NotificationMessageAction<Access_QSID_Data_VM>>(this, CallBackBindListVersionHisGridExecute);
            MessengerInstance.Register<NotificationMessageAction<Log_Session_NormalizeHistory_VM>>(this, CallBackBindListNormalizationHisGridExecute);

            MessengerInstance.Register<NotificationMessageAction<string>>(this, CallBackOpenCurrentDataView);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Access_QSID_Data_VM), HyperlinkClickExecuted);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, UpdateSearchCriteria);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(CurrentDataView.ViewModel.ParentChildDataUC_VM), TabChangedEvent);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(I4FTab.ViewModel.GroupThresholdUC_VM), TabChangedEvent);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(SubstanceSearchUserControl_VM), TabChangedEvent);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(GenericManagementTool.ViewModels.Generics_VM), TabChangedEvent);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(CommonGenericUIView.ViewModel.CommentsUC_VM), TabChangedEvent);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(WorkFeedUserControl_VM), TabChangedEvent);
        }




        public void CheckPermission()
        {
            var libraryTab = Engine.listAllPermissionDashboard.Any(x => x.PermissionName == "");

        }
        private void CommondOpenUserCommentNotificationExecute(object obj)
        {
            ShowPopUpUserNotificationDetail = true;
            NotifyPropertyChanged("ShowPopUpUserNotificationDetail");
        }

        private void CommandRunExeOfListSpecificToolsExecuted(object obj)
        {
            string exePath = (string)obj;
            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.FileName = exePath;
                //startInfo.Arguments = filePath;
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + exePath);
            }
        }

        public void GetLibraryListSpecificTools()
        {
            listSpecificTools = new ObservableCollection<EntityClass.Library_ListSpecificTools>(_objLibraryFunction_Service.GetListAllLibraryTools());
            NotifyPropertyChanged("listSpecificTools");
        }
        private void TabChangedEvent(PropertyChangedMessage<string> objParam)
        {
            if (objParam.PropertyName == "ActiveGenericTab")
            {
                IsSelectedSubstanceIdentityTab = false;
                NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
                IsSelectedGenerics = true;
                NotifyPropertyChanged("IsSelectedGenerics");
                IsSelectedCasAssigmentTab = false;
                IsSelectedCurrentDataView = false;
                IsSelectedNormalization = false;
                NotifyPropertyChanged("IsSelectedCasAssigmentTab");
                NotifyPropertyChanged("IsSelectedCurrentDataView");
                NotifyPropertyChanged("IsSelectedNormalization");
            }
            else if (objParam.PropertyName == "ActiveSubstanceIdentityWithCas")
            {
                IsSelectedSubstanceIdentityTab = true;
                NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
                IsSelectedGenerics = false;
                NotifyPropertyChanged("IsSelectedGenerics");
                IsSelectedCasAssigmentTab = false;
                IsSelectedCurrentDataView = false;
                IsSelectedNormalization = false;
                NotifyPropertyChanged("IsSelectedCasAssigmentTab");
                NotifyPropertyChanged("IsSelectedCurrentDataView");
                NotifyPropertyChanged("IsSelectedNormalization");
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", objParam.NewValue, "SearchCasInSubstanceIdentityTab"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
            }
            else if (objParam.PropertyName == "UpdateUserMentionComments")
            {
                BindMapUserCommentList();
            }
            else if (objParam.PropertyName == "FilterCheckOutUser")
            {
                IsFirstTime = true;
                IsSelectedVersionHistoryTab = true;
                NotifyPropertyChanged("IsSelectedVersionHistoryTab");
                IsSelectedSubstanceIdentityTab = false;
                NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
                IsSelectedGenerics = false;
                NotifyPropertyChanged("IsSelectedGenerics");
                IsSelectedCasAssigmentTab = false;
                IsSelectedCurrentDataView = false;
                IsSelectedNormalization = false;
                NotifyPropertyChanged("IsSelectedCasAssigmentTab");
                NotifyPropertyChanged("IsSelectedCurrentDataView");
                NotifyPropertyChanged("IsSelectedNormalization");

                FilterCheckOutUser(objParam.NewValue);
            }
            else if (objParam.PropertyName == "FilterAddSuggestionReview")
            {
                IsSelectedVersionHistoryTab = false;
                IsSelectedSubstanceIdentityTab = false;
                IsSelectedGenerics = true;
                IsSelectedCasAssigmentTab = false;
                IsSelectedCurrentDataView = false;
                IsSelectedNormalization = false;
                NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
                NotifyPropertyChanged("IsSelectedGenerics");
                NotifyPropertyChanged("IsSelectedVersionHistoryTab");
                NotifyPropertyChanged("IsSelectedCasAssigmentTab");
                NotifyPropertyChanged("IsSelectedCurrentDataView");
                NotifyPropertyChanged("IsSelectedNormalization");
                FilterAddSuggestion(objParam.NewValue);
            }
        }
        private void HyperlinkClickExecuted(NotificationMessageAction<object> notificationMessageAction)
        {
            string url = (string)notificationMessageAction.Notification;
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    //System.Diagnostics.Process.Start(url);
                    var p = new Process();
                    p.StartInfo = new ProcessStartInfo(url)
                    {
                        UseShellExecute = true
                    };
                    p.Start();
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void CommandCancelNoteURLExecuted(object obj)
        {
            popUpAddNoteVisibility = false;
            NotifyPropertyChanged("popUpAddNoteVisibility");
        }

        private void CallBackOpenCurrentDataView(NotificationMessageAction<string> notificationMessageAction)
        {
            if (notificationMessageAction.Notification == "AddNote")
            {
                ListNoteUrl = SelectedQSID_VM.NoteUrl;
                popUpAddNoteVisibility = true;
                NotifyPropertyChanged("popUpAddNoteVisibility");
            }
            else if (notificationMessageAction.Notification == "AddParentChildQsid")
            {
                PopUp_UpdateData_Version popUpDialog = new PopUp_UpdateData_Version(SelectedQSID_Version, "AddParentChildQsid", true);
                popUpDialog.ShowDialog();
                BindQSIDGrid();
            }
            else
            {

                CurrentDataViewDataContext = new CurrentDataView.ViewModel.Win_Administrator_VM();
                NotifyPropertyChanged("CurrentDataViewDataContext");
                IsSelectedCurrentDataView = true;
                NotifyPropertyChanged("IsSelectedCurrentDataView");
                IsSelectedCasAssigmentTab = false;
                NotifyPropertyChanged("IsSelectedCasAssigmentTab");
                IsSelectedNormalization = false;
                NotifyPropertyChanged("IsSelectedNormalization");
                if (notificationMessageAction.Notification == "OpenDataGenerics")
                {
                    MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenDataGenerics", NotifyMe), typeof(CurrentDataView.ViewModel.Win_Administrator_VM));
                }
            }

        }

        private void NotifyMe(string obj)
        {

        }

        private void UpdateSourceTriggerCurrentCell(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "Log_Session_NormalizeHistory_VM")
            {
                CellInfo = obj.NewValue;
            }
        }

        private void UpdateSearchCriteria(PropertyChangedMessage<string> objSearchCriteria)
        {
            if (objSearchCriteria.PropertyName == "SearchCriteria_Library_QSID_Information_VM")
            {
                ListDetailFilterQueryString = objSearchCriteria.NewValue;
                GetFilterQuery();
            }
        }
        private void CallBackBindListDetailGridExecute(NotificationMessageAction<Library_QSID_Information_VM> notificationMessageAction)
        {
            BindQSIDGrid();
        }
        private void CallBackBindListVersionHisGridExecute(NotificationMessageAction<Access_QSID_Data_VM> notificationMessageAction)
        {
            LoadVersionHistoryAccordingToQSID();
        }
        private void CallBackBindListNormalizationHisGridExecute(NotificationMessageAction<Log_Session_NormalizeHistory_VM> notificationMessageAction)
        {
            LoadNormalizedHistoryAccordingToQSID();
        }


        private void NotifyMe(PropertyChangedMessage<Access_QSID_Data_VM> obj)
        {
            SelectedQSID_Version = obj.NewValue;
        }
        private void NotifyMe(PropertyChangedMessage<Library_QSID_Information_VM> obj)
        {
            SelectedQSID_VM = obj.NewValue;
        }
        private void NotifyMe(PropertyChangedMessage<Log_Session_NormalizeHistory_VM> obj)
        {
            SelectedNormalizedHistory = obj.NewValue;
        }
        private bool CommandRunRdMappingCanExecute(object obj)
        {
            if (SelectedQSID_Version != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandRunRdMappingExecuted(object obj)
        {
            if (File.Exists(@"C:\Ariel\Common\COM Add-ins\IntegratedContentDataAnalysis\ICDataAnalysis.exe"))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.FileName = @"C:\Ariel\Common\COM Add-ins\IntegratedContentDataAnalysis\ICDataAnalysis.exe";
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + @"C:\Ariel\Common\COM Add-ins\IntegratedContentDataAnalysis\ICDataAnalysis.exe");
            }
        }
        private bool CommandAddNoteURLCanExecute(object obj)
        {
            if (SelectedQSID_VM != null && SelectedQSID_VM.IsDeleted.Value == false && !string.IsNullOrEmpty(ListNoteUrl))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandAddNoteURLExecuted(object obj)
        {
            _objAccessVersion_Service.AddQsidNoteUrl(SelectedQSID_VM.QSID_ID, ListNoteUrl);
            SelectedQSID_VM.NoteUrl = ListNoteUrl;
            popUpAddNoteVisibility = false;
            NotifyPropertyChanged("popUpAddNoteVisibility");
            BindQSIDGrid();
        }

        private bool CommandRunCurrentDataViewCanExecute(object obj)
        {
            if (SelectedQSID_Version != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandRunCurrentDataViewExecuted(object obj)
        {
            Engine.IsOpenCurrentDataViewFromVersionHis = true;
            Engine.CurrentDataViewQsid = SelectedQSID_VM.QSID;
            Engine.CurrentDataViewQsidID = SelectedQSID_VM.QSID_ID;
            CurrentDataViewDataContext = new CurrentDataView.ViewModel.Win_Administrator_VM();
            NotifyPropertyChanged("CurrentDataViewDataContext");
            IsSelectedCurrentDataView = true;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = false;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        private void CommandDataDictionaryExecuted()
        {
            Engine.IsOpenDataDictionaryFromVersionHis = true;
            Engine.DataDictionaryListDicPath = _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID) == string.Empty ? _objAccessVersion_Service.CheckInFileName(SelectedQSID_VM.QSID_ID) : _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID);
            NormalizationToolContext = new NormalizationTool.ViewModel.MainControl_VM();
            NotifyPropertyChanged("NormalizationToolContext");
            if (!File.Exists(Engine.DataDictionaryListDicPath))
            {
                MessageBox.Show("Error: Source File not exists on below path, so Choose Manually:" + Environment.NewLine + Engine.DataDictionaryListDicPath);
            }
            IsSelectedCurrentDataView = false;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = true;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        private void CommandListDictionaryExecuted()
        {
            Engine.IsOpenListDictionaryFromVersionHis = true;

            Engine.DataDictionaryListDicPath = _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID) == string.Empty ? _objAccessVersion_Service.CheckInFileName(SelectedQSID_VM.QSID_ID) : _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID);
            NormalizationToolContext = new NormalizationTool.ViewModel.MainControl_VM();
            //NormalizationToolContext.CommonFilepath = Engine.DataDictionaryListDicPath;
            NotifyPropertyChanged("NormalizationToolContext");
            if (!File.Exists(Engine.DataDictionaryListDicPath))
            {
                MessageBox.Show("Error: Source File not exists on below path, so Choose Manually:" + Environment.NewLine + Engine.DataDictionaryListDicPath);
            }
            IsSelectedCurrentDataView = false;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = true;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        private void CommandUniqueCriteriaExecuted()
        {
            Engine.IsOpenUniquenessCriteriaFromVersionHis = true;

            Engine.DataDictionaryListDicPath = _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID) == string.Empty ? _objAccessVersion_Service.CheckInFileName(SelectedQSID_VM.QSID_ID) : _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID);
            NormalizationToolContext = new NormalizationTool.ViewModel.MainControl_VM();
            NotifyPropertyChanged("NormalizationToolContext");
            if (!File.Exists(Engine.DataDictionaryListDicPath))
            {
                MessageBox.Show("Error: Source File not exists on below path, so Choose Manually:" + Environment.NewLine + Engine.DataDictionaryListDicPath);
            }
            IsSelectedCurrentDataView = false;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = true;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        private void CommandAddInfoExecuted()
        {
            Engine.IsOpenAdditionalInfoFromVersionHis = true;

            Engine.DataDictionaryListDicPath = _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID) == string.Empty ? _objAccessVersion_Service.CheckInFileName(SelectedQSID_VM.QSID_ID) : _objAccessVersion_Service.CheckOutFileName(SelectedQSID_VM.QSID_ID);
            NormalizationToolContext = new NormalizationTool.ViewModel.MainControl_VM();
            //NormalizationToolContext.CommonFilepath = Engine.DataDictionaryListDicPath;
            NotifyPropertyChanged("NormalizationToolContext");
            if (!File.Exists(Engine.DataDictionaryListDicPath))
            {
                MessageBox.Show("Error: Source File not exists on below path, so Choose Manually:" + Environment.NewLine + Engine.DataDictionaryListDicPath);
            }
            IsSelectedCurrentDataView = false;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = true;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        private void CommandRunAdditionalInfoviewExecuted(object obj)
        {
            Engine.IsOpenAdditionalInfoViewFromVersionHis = true;
            Engine.CurrentDataViewQsid = SelectedQSID_VM.QSID;
            Engine.CurrentDataViewQsidID = SelectedQSID_VM.QSID_ID;
            CurrentDataViewDataContext = new CurrentDataView.ViewModel.Win_Administrator_VM();
            NotifyPropertyChanged("CurrentDataViewDataContext");
            //AdditionalInfoDataContext = new CurrentDataView.ViewModel.AdditionalInfoHistory_VM();
            //NotifyPropertyChanged("AdditionalInfoDataContext");
            IsSelectedCurrentDataView = true;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedCasAssigmentTab = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedNormalization = false;
            NotifyPropertyChanged("IsSelectedNormalization");
        }

        private bool CommandRunApplicationNameMappingCanExecute(object obj)
        {
            if (SelectedQSID_Version != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandRunApplicationNameMappingExecuted(object obj)
        {
            string filePath = string.Empty;
            if (SelectedQSID_Version.CheckedOut == false)
            {

                filePath = SelectedQSID_Version.FilePath;
            }
            else
            {
                filePath = @"C:\crawork\versioncontrol\" + _objAccessVersion_Service.GetCurrentCheckedOutFilePath(SelectedQSID_Version.Version, SelectedQSID_Version.FileName);
                if (File.Exists(filePath))
                {

                }
                else
                {
                    _notifier.ShowError("File not exist on below path: " + System.Environment.NewLine + filePath);
                }
            }
            if (File.Exists(@"C:\Ariel\Common\COM Add-ins\ApplicationName2CategoryMapping\ApplicationMapping.exe"))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.Arguments = filePath;
                startInfo.FileName = @"C:\Ariel\Common\COM Add-ins\ApplicationName2CategoryMapping\ApplicationMapping.exe";
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + @"C:\Ariel\Common\COM Add-ins\ApplicationName2CategoryMapping\ApplicationMapping.exe");
            }


        }

        private bool CommandRunCasAssignmentCanExecute(object obj)
        {
            if (SelectedQSID_Version != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandRunCasAssignmentExecuted(object obj)
        {

            Engine.IsOpenCasAssignmentFromAccess = true;

            string filePath = string.Empty;
            if (SelectedQSID_Version.CheckedOut == false)
            {

                filePath = SelectedQSID_Version.FilePath;
            }
            else
            {
                if (Engine.CurrentUserName == CheckedOutByUser.Replace("Checked Out By: ", ""))
                {
                    filePath = @"C:\crawork\versioncontrol\" + _objAccessVersion_Service.GetCurrentCheckedOutFilePath(SelectedQSID_Version.Version, SelectedQSID_Version.FileName);
                    if (File.Exists(filePath))
                    {

                    }
                    else
                    {
                        _notifier.ShowError("File not exist on below path: " + System.Environment.NewLine + filePath);
                    }

                }
            }
            Engine.CasAssignmentAccessDBFilePath = filePath;
            casAssignmentDataContext = new CRA_CasAssignment.ViewModel.CasAssignment_ViewModel();
            NotifyPropertyChanged("casAssignmentDataContext");
            ShowActiveTabCasAssignment();


        }

        private void CommandRefreshListDetailExecuted(object obj)
        {
            BindQSIDGrid();
        }

        public void BindFilterProductTypeCombo()
        {
            var listItem = new List<SelectListItem>();
            listItem.Add(new SelectListItem { Text = "Production Date Timestamp" });
            listItem.Add(new SelectListItem { Text = "Normalization Date Timestamp" });
            listItem.Add(new SelectListItem { Text = "List Currently Checked Out" });
            listItem.Add(new SelectListItem { Text = "WorkOrder Number" });
            listItem.Add(new SelectListItem { Text = "Notes" });
            listItem.Add(new SelectListItem { Text = "CSCTicket Number" });
            ListSearchType = new ObservableCollection<SelectListItem>(listItem);
            NotifyPropertyChanged("ListSearchType");
        }
        public void BindNormalization()
        {
            var listItem = new List<string>();
            listItem.Add("--Select--");
            listItem.Add("Additional-Info");
            listItem.Add("Data-Dictionary");
            listItem.Add("List-Dictionary");
            listItem.Add("Uniqueness-Criteria");
            ListNormalization = new ObservableCollection<string>(listItem);
            NotifyPropertyChanged("ListNormalization");
        }
        public void BindTextFilterWorkOrderAndNote()
        {
            var listItem = new List<SelectListItem>();
            listItem.Add(new SelectListItem { Text = "Partial Match" });
            listItem.Add(new SelectListItem { Text = "Exact Match" });
            listItem.Add(new SelectListItem { Text = "Begin with" });
            listItem.Add(new SelectListItem { Text = "End with" });
            ListSearchTypeCondition = new ObservableCollection<SelectListItem>(listItem);


            SelectedSearchTypeCondition = ListSearchTypeCondition.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchTypeCondition");
            NotifyPropertyChanged("ListSearchTypeCondition");
        }


        private void CommandExportToOldPathExecuted(object obj)
        {
            if (SelectedQSID_Version != null)
            {
                string version = (SelectedQSID_Version.Version).Remove(0, 1);
                string FileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(SelectedQSID_Version.FileName, version);
                string destinationPath = Engine.CommonExportOldFolderPath + SelectedQSID_VM.Module.Name + "\\";
                _objLibraryFunction_Service.MoveFileOnePathToAnother(SelectedQSID_Version.FilePath, destinationPath, FileName);
                _notifier.ShowSuccess("File Exported on below path:" + Environment.NewLine + destinationPath + FileName);
            }


        }

        private bool CommandExportToOldPathCanExecute(object obj)
        {
            bool resultFlag = false;
            if ((SearchTypeFilter == "Production Date Timestamp") && (ProductionDate_From != null || ProductionDate_To != null))
            {
                string topVersion = ListQSID_VersionHistory.OrderByDescending(x => x.Version).Select(x => x.Version).FirstOrDefault();
                if (SelectedQSID_Version != null)
                {
                    if (SelectedQSID_Version.Version == topVersion)
                    {
                        resultFlag = true;
                    }
                }
            }
            return resultFlag;
        }

        private void CustomQueryController_FilteringStarted(object sender, EventArgs e)
        {
            if (CustomQueryController.ColumnFilterData != null)
            {
                string colName = CustomQueryController.ColumnFilterData.ValuePropertyBindingPath;
                string valueofCol = CustomQueryController.ColumnFilterData.QueryString;
                if (_listfilter.Any(x => x.ColName == colName))
                {
                    var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                    objFilter.ColValue = valueofCol;
                }
                else
                {
                    _listfilter.Add(new filterList { ColName = colName, ColValue = valueofCol });
                }
                GetFilterQuery();
            }
        }



        private bool CommandClearProductTimeStampCanExecute(object obj)
        {
            bool IsEnabled = false;
            if (SelectedQSID_Version != null)
            {
                if (SelectedQSID_Version.ProductTimeStamp != null && SelectedQSID_VM.IsDeleted.Value == false)
                {
                    IsEnabled = true;
                }
            }
            return IsEnabled;
        }
        private void CommandClearProductTimeStampExecuted(object obj)
        {
            var logProductTimeStamp = AccessVersionControlSystem.Common.CommonFunctions.BindProductTimeStampFromVersionHistory(SelectedQSID_Version, "D");
            SelectedQSID_Version.ProductTimeStamp = (DateTime?)null;
            _objAccessVersion_Service.UpdateAccessversionData(SelectedQSID_Version);
            _objLibraryFunction_Service.BulkIns(logProductTimeStamp);
            LoadVersionHistoryAccordingToQSID();
        }

        private bool CommandDefaultTrueCanExecute(object obj)
        {
            return true;
        }

        private void CommandCloseMenuExecuted(object obj)
        {
            VisibilitySideNav = Visibility.Hidden;
            VisibilityMenuButton = Visibility.Visible;
            NotifyPropertyChanged("VisibilitySideNav");
            NotifyPropertyChanged("VisibilityMenuButton");

        }
        private void CommandOpenMenuExecuted(object obj)
        {
            VisibilitySideNav = Visibility.Visible;
            VisibilityMenuButton = Visibility.Hidden;
            NotifyPropertyChanged("VisibilitySideNav");
            NotifyPropertyChanged("VisibilityMenuButton");
        }
        private bool CommandRDAndApplicationNameMappingCanExecute(object obj)
        {
            string topVersion = ListQSID_VersionHistory.OrderByDescending(x => x.Version).Select(x => x.Version).FirstOrDefault();
            if (SelectedQSID_Version != null)
            {
                if (SelectedQSID_Version.Version == topVersion && IsActiveRdButton)//&& IsDisplayCheckOut
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private bool CommandInitiateOtherCanExecute(object obj)
        {
            string topVersion = ListQSID_VersionHistory.OrderByDescending(x => x.Version).Select(x => x.Version).FirstOrDefault();
            if (SelectedQSID_Version != null)
            {
                if (SelectedQSID_Version.Version == topVersion)//&& IsDisplayCheckOut
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {

                return false;
            }
        }

        private bool CommandInitiateNormalizationCanExecute(object obj)
        {
            string topVersion = ListQSID_VersionHistory.OrderByDescending(x => x.Version).Select(x => x.Version).FirstOrDefault();
            if (SelectedQSID_Version != null)
            {
                if (SelectedQSID_Version.Version == topVersion && (SelectedQSID_VM.IsDeleted.Value == false))//&& IsDisplayCheckOut
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private void CommandInitiateNormalizationExecuted(object obj)
        {
            bool IsUserRunNormalization = false;
            string filePath = string.Empty;
            if (SelectedQSID_Version.CheckedOut == false)
            {
                IsUserRunNormalization = true;
                filePath = SelectedQSID_Version.FilePath;
            }
            else
            {
                if (Engine.CurrentUserName == CheckedOutByUser.Replace("Checked Out By: ", ""))
                {
                    filePath = @"C:\crawork\versioncontrol\" + _objAccessVersion_Service.GetCurrentCheckedOutFilePath(SelectedQSID_Version.Version, SelectedQSID_Version.FileName);
                    if (File.Exists(filePath))
                    {
                        IsUserRunNormalization = true;
                    }
                    else
                    {
                        _notifier.ShowError("File not exist on below path: " + System.Environment.NewLine + filePath);
                    }

                }
            }

            if (IsUserRunNormalization)
            {
                if (File.Exists(@"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe"))
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = true;
                    startInfo.FileName = @"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe";
                    startInfo.Arguments = filePath;
                    Process.Start(startInfo);
                }
                else
                {
                    _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + @"C:\Ariel\Common\COM Add-ins\UpStreamDataNormalization\UpstreamDataNormalization.exe");
                }
            }
            else
            {

                _notifier.ShowInformation("You can't run the Initiate Normalization due to this qsid version checked out!");
            }

        }



        private bool CommandGenerateAccessListDetailCanExecute(object obj)
        {
            if (ListQSID_VM.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CommandGenerateAccessListDetailExecuted(object obj)
        {
            string fileName = "ListDetailQSID_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = AccessVersionControlSystem.Common.CommonFunctions.ConvertDataTableFromListDetail(ListQSID_VM);
            dtListQsidDetail.TableName = "List Qsids";
            var resultMessage = AccessVersionControlSystem.Common.CommonFunctions.ExportAccessFile(dtListQsidDetail, fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }
        public void CommandGenerateExcelListDetailExecuted(object obj)
        {
            string fileName = "ListDetailQSID_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = AccessVersionControlSystem.Common.CommonFunctions.ConvertDataTableFromListDetail(ListQSID_VM);
            dtListQsidDetail.TableName = "List Qsids";
            var resultMessage = AccessVersionControlSystem.Common.CommonFunctions.ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }
        }






        private bool CommandSelectionChangedCanExecute(object obj)
        {
            return true;
        }


        private bool CommandDataGridFilterCanExecute(object obj)
        {
            return true;
        }

        private void CommandDataGridFilterFocusExecute(object obj)
        {
            string colName = CustomQueryController.ColumnFilterData.ValuePropertyBindingPath;
            string valueofCol = CustomQueryController.ColumnFilterData.QueryString;
            if (_listfilter.Any(x => x.ColName == colName))
            {
                var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                objFilter.ColValue = valueofCol;
            }
            else
            {
                _listfilter.Add(new filterList { ColName = colName, ColValue = valueofCol });
            }
            GetFilterQuery();
        }


        public void CommandDataGridClearFilterExecute(object sender)
        {
            CustomQueryController.ClearFilter();
            _listfilter.Clear();
            GetFilterQuery();
        }

        public void BindListFilterFromLibrary()
        {
            Engine._listModule = _objLibraryFunction_Service.GetAll_Library_Module().Select(x => new ListModule_ViewModel { Id = x.ModuleId, Name = x.ModuleName }).ToList();
            Engine._listCountry = _objLibraryFunction_Service.GetAll_Library_Countries().Select(x => new ListCountries_ViewModel { Id = x.CountryId, Name = x.CountryName }).ToList();
            Engine._listTopic = _objLibraryFunction_Service.GetAll_Library_Topic().Select(x => new ListTopic_ViewModel { Id = x.TopicId, Name = x.TopicName }).ToList();
            var listCheckBox = new List<ListCheckBoxType_ViewModel>();
            listCheckBox.Add(new ListCheckBoxType_ViewModel { Content = "Yes", Value = true });
            listCheckBox.Add(new ListCheckBoxType_ViewModel { Content = "No", Value = false });
            Engine._listCheckboxType = listCheckBox;
            Engine._listCheckedOutUser = _objLibraryFunction_Service.GetAll_Library_CheckedOut().Select(x => new ListCommonComboBox_ViewModel { Id = x.Id, Name = x.Name, Id_String = x.Id_String }).OrderBy(x => x.Name).ToList();
            Engine._listProducts = _objLibraryFunction_Service.GetAll_Library_Products().Select(x => new ListCommonComboBox_ViewModel { Id = x.Id, Name = x.Name }).ToList();
            Engine._listObligationType = _objLibraryFunction_Service.GetAll_Library_Obligation().Select(x => new ListCommonComboBox_ViewModel { Id = x.Id, Name = x.Name }).ToList();
            Engine._listTypeCategory = _objLibraryFunction_Service.GetAll_Library_TypeCategory().Select(x => new ListCommonComboBox_ViewModel { Id = x.Id, Name = x.Name }).ToList();
        }

        private bool CommandClearProductionDateCanExecute(object o)
        {
            return true;
        }
        private void CommandClearProductionDateExecute(object o)
        {
            ProductionDate_From = null;
            ProductionDate_To = null;
            NotifyPropertyChanged("ProductionDate_From");
            NotifyPropertyChanged("ProductionDate_To");
            SelectedSearchType = null;
            NotifyPropertyChanged("SelectedSearchType");
            InputTextSearch = string.Empty;
            DateRangeFilterVisibility = Visibility.Visible;
            TextFilterVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("TextFilterVisibility");
            NotifyPropertyChanged("DateRangeFilterVisibility");
            FilterQSIDGrid();
        }


        private bool CommandAddNewListCanExecute(object o)
        {
            return true;
        }

        private void CommandAddNewListExecute(object o)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = true;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                string[] fileName = openFileDlg.SafeFileNames;
                for (int count = 0; count < SelectedPath.Count(); count++)
                {
                    Dictionary<string, Log_VersionControl_History> resultQsidObj = _objAccessVersion_Service.AddNewQSIDList(SelectedPath[count], fileName[count]);
                    if (!string.IsNullOrEmpty(resultQsidObj.Keys.FirstOrDefault()))
                    {
                        if (resultQsidObj.Values.FirstOrDefault() != null)
                        {
                            Access_QSID_Data_VM objSelectQsid = AccessVersionControlSystem.Common.CommonFunctions.ConvertVersionHistoryTO_VM(resultQsidObj.Values.FirstOrDefault(), resultQsidObj.Keys.FirstOrDefault());
                            //PopUp_UpdateData_Version popUpdateWO
                            PopUp_UpdateData_Version popUpDialog = new PopUp_UpdateData_Version(objSelectQsid, "AddTypeOfChange_FirstTime", true);
                            popUpDialog.ShowDialog();
                            BindQSIDGrid();
                            _notifier.ShowSuccess("Added new qsid'" + resultQsidObj.Keys.FirstOrDefault() + "' list successfully!");
                        }
                        else
                        {
                            _notifier.ShowError("Error: Add new qsid '" + resultQsidObj.Keys.FirstOrDefault() + "' list failed due to already exist!");
                        }
                    }
                    else
                    {
                        _notifier.ShowError("Error: Add new qsid list failed due to missing meta data information!");
                    }
                }
                BindQSIDGrid();
                EnableDisableControl();
                NotifyPropertyChanged("ListQSID_VM");
            }
        }
        private bool CommandGetCopyListCanExecute(object o)
        {
           // bool IsViewButton = Engine.CurrentUserRolePermission.IsGetCopy != null ? Convert.ToBoolean(Engine.CurrentUserRolePermission.IsGetCopy) : false;
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && SelectedQSID_Version != null && SelectedQSID_Version.VersionControl_ID > 0;
        }

        private void CommandGetCopyListExecute(object o)
        {
            if (SelectedQSID_VM != null && SelectedQSID_Version != null)
            {
                string resultCopyPath = _objAccessVersion_Service.GetLatestVersionCopyOfQSID(SelectedQSID_VM.QSID_ID, SelectedQSID_Version.VersionControl_ID);
                if (!string.IsNullOrEmpty(resultCopyPath))
                {
                    _notifier.ShowInformation("File copied on below path: " + Environment.NewLine + resultCopyPath);
                    OpenFileInAccess(resultCopyPath);

                }
                else
                {
                    _notifier.ShowError("File copied failed due to internal server error!");
                }
            }
        }
        public void OpenFileInAccess(string resultCopyPath)
        {
            try
            {
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(resultCopyPath)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            catch (Exception ex)
            {
                _notifier.ShowError("System not open the file in access due to below error:" + Environment.NewLine + ex.Message);
            }
        }
        private bool CommandAddNoteCanExecute(object o)
        {
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && SelectedQSID_Version != null && SelectedQSID_Version.VersionControl_ID > 0 && SelectedQSID_VM.IsDeleted.Value == false; ;
        }

        private void CommandAddNoteExecute(object o)
        {
            if (SelectedQSID_VM != null && SelectedQSID_Version != null)
            {
                bool IsEnable = CheckEnableForEditing("AddNote");
                if (IsEnable)
                {
                    var popUpWindow = new PopUp_UpdateData_Version(SelectedQSID_Version, "AddNote", IsEnable);
                    popUpWindow.ShowDialog();
                    BindQSIDGrid();
                    //LoadVersionHistoryAccordingToQSID();
                }
                else
                {
                    _notifier.ShowWarning("Note disabled for updating due to current version already have note.");
                }
            }
        }

        private bool CommandUpdateCheckedInfoCanExecute(object obj)
        {
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && SelectedQSID_Version != null && SelectedQSID_Version.VersionControl_ID > 0 && SelectedQSID_VM.IsDeleted.Value == false; ;
        }

        private void CommandUpdateCheckedInfoExecuted(object obj)
        {
            if (SelectedQSID_VM != null && SelectedQSID_Version != null)
            {
                bool IsEnable = CheckEnableForEditing("UpdateTypeOfChange");
                if (IsEnable)
                {
                    PopUp_UpdateData_Version popUpDialog = new PopUp_UpdateData_Version(SelectedQSID_Version, "UpdateTypeOfChange", true);
                    popUpDialog.ShowDialog();
                    BindQSIDGrid();
                    //LoadVersionHistoryAccordingToQSID();
                }
                else
                {
                    _notifier.ShowWarning("Checked Information disabled for updating due to current version already have Checked Information.");
                }

            }
        }

        private bool CommandAddProductTimestampCanExecute(object o)
        {
           // bool IsViewButton = Engine.CurrentUserRolePermission.IsAddProductTimeStamp != null ? Convert.ToBoolean(Engine.CurrentUserRolePermission.IsAddProductTimeStamp) : false;
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && SelectedQSID_Version != null && SelectedQSID_Version.VersionControl_ID > 0  && SelectedQSID_VM.IsDeleted.Value == false; ;
        }

        private void CommandAddProductTimestampExecute(object o)
        {
            if (SelectedQSID_VM != null && SelectedQSID_Version != null)
            {
                bool IsEnable = CheckEnableForEditing("AddProductTimestamp");
                if (IsEnable)
                {
                    var popUpWindow = new PopUp_UpdateData_Version(SelectedQSID_Version, "AddProductTimestamp", IsEnable);
                    popUpWindow.ShowDialog();
                    BindQSIDGrid();
                    //LoadVersionHistoryAccordingToQSID();
                }
                else
                {
                    _notifier.ShowWarning("Product Timestamp disabled for updating due to current version already have product timestamp.");
                }
            }
        }
        private bool CheckEnableForEditing(string flagType)
        {
            bool resultFlag = false;
            resultFlag = _objAccessVersion_Service.CheckEnableForEditVersionHistoryInfo(SelectedQSID_VM.QSID_ID, SelectedQSID_Version.Version, flagType);
            return resultFlag;
        }

        private bool CommandCheckInListCanExecute(object o)
        {
            //bool IsViewButton = Engine.CurrentUserRolePermission.IsCheckOutOrIN != null ? Convert.ToBoolean(Engine.CurrentUserRolePermission.IsCheckOutOrIN) : false;
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && IsDisplayCheckIn && SelectedQSID_VM.IsDeleted.Value == false; ;
        }

        private void CommandCheckInListExecute(object o)
        {
            if (SelectedQSID_VM != null)
            {
                //  var CurrentQsidVersion = _objAccessVersion_Service.CheckIn_Functionality(SelectedQSID_VM.QSID_ID);

                //LoadVersionHistoryAccordingToQSID();
                EnableDisableControl();

                var popUpWindow = new PopUp_UpdateData_Version(SelectedQSID_Version, "AddTypeOfChange", true);
                popUpWindow.ShowDialog();
                BindQSIDGrid();
            }
        }



        private bool CommandCheckOutListCanExecute(object o)
        {
            bool flagDisplayCheckout = false;
            //bool IsViewButton = Engine.CurrentUserRolePermission.IsCheckOutOrIN != null ? Convert.ToBoolean(Engine.CurrentUserRolePermission.IsCheckOutOrIN) : false;
            if (SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && IsDisplayCheckOut &&  SelectedQSID_VM.IsDeleted.Value == false)
            {
                flagDisplayCheckout = true;
            }
            return flagDisplayCheckout;
            // return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && IsDisplayCheckOut && IsViewButton;
        }

        private void CommandCheckOutListExecute(object o)
        {
            if (SelectedQSID_VM != null)
            {
                if (NormalizedByUser != string.Empty && !NormalizedByUser.Contains(Engine.CurrentUserName))
                {
                    _notifier.ShowError(NormalizedByUser);
                    return;
                }
                if (_objAccessVersion_Service.CheckOutFunctionality(SelectedQSID_VM.QSID_ID))
                {

                    string incrementedVersion = _objAccessVersion_Service.IncrementVersion(SelectedQSID_Version.Version);
                    string FileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(SelectedQSID_Version.FileName, incrementedVersion);

                    string destinationPath = Engine.CommonCheckoutFolderPath + FileName;
                    SelectedQSID_VM.CurrentlyCheckedOut = Engine._listCheckedOutUser.Where(x => x.Name == Engine.CurrentUserName).FirstOrDefault();
                    if (_objAccessVersion_Service.CheckProductEchaByQsid(SelectedQSID_VM.QSID_ID))
                    {
                        _notifier.ShowWarning("This list is associated with ECHA/EUCLEF, please be sure all necessary QC checks are completed");
                    }
                    _notifier.ShowInformation("File Check Out Successfully.");
                    LoadVersionHistoryAccordingToQSID();
                    GetAlreadyCheckOutUser();
                    EnableDisableControl();
                    NotifyPropertyChanged("ListQSID_VersionHistory");
                    OpenFileInAccess(destinationPath);

                }
                else
                {
                    _notifier.ShowError("Check-out process failed.");
                }
            }
        }
        private bool CommandUnLockListCanExecute(object o)
        {
            return SelectedQSID_VM != null && SelectedQSID_VM.QSID_ID > 0 && IsDisplayUnLock && SelectedQSID_VM.IsDeleted.Value == false;
        }

        private void CommandUnLockListExecute(object o)
        {
            if (SelectedQSID_VM != null)
            {
                SelectedQSID_Version.CheckedOut = false;
                _objAccessVersion_Service.UpdateUnlockLastCheckOutQsidList(SelectedQSID_VM.QSID_ID);
                SelectedQSID_VM.CurrentlyCheckedOut = new ListCommonComboBox_ViewModel();
                EnableDisableControl();
                GetAlreadyCheckOutUser();
                _notifier.ShowInformation("List " + SelectedQSID_VM.ListFieldName + " successfully unlocked.");
            }
        }

        public void BindQSIDGrid()
        {
            VisibilityListDetailDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
            //MessengerInstance.Send<PropertyChangedMessage<bool>>(new PropertyChangedMessage<bool>(this, comonDG_VM, true, true, "Show Loader"));
            Task.Run(() => BindQsidDetailToView());
        }

        private void FetchListQsidInfo()
        {
            var listQsidDetail = _objLibraryFunction_Service.GetQSIDListDetail("Q");
            ListQsidDetailInfromation = AccessVersionControlSystem.Common.CommonFunctions.BindLibraryInformationModel(listQsidDetail);
        }
        private void BindQsidDetailToView()
        {
            if (SelectedQSID_VM != null)
            {
                CurrentSelectedQSID_VM = SelectedQSID_VM;
            }
            FetchListQsidInfo();
            if (SearchCriteria != null)
            {
                FilterQSIDGrid();
            }
            else
            {

                ListQSID_VM = new ObservableCollection<Library_QSID_Information_VM>(ListQsidDetailInfromation);

                if (CurrentSelectedQSID_VM != null)
                {
                    SelectedQSID_VM = ListQSID_VM.Where(x => x.QSID_ID == CurrentSelectedQSID_VM.QSID_ID).FirstOrDefault();
                    NotifyPropertyChanged("SelectedQSID_VM");
                }
                NotifyPropertyChanged("ListQSID_VM");
            }
            VisibilityListDetailDashboard_Loader = Visibility.Hidden;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");


        }

        public void FilterQSIDGrid()
        {
            var list_Library_Information_QSID_VM = ListQsidDetailInfromation;
            if (SerachText != null)
            {
                list_Library_Information_QSID_VM = ListQsidDetailInfromation.Where(x => (x.QSID != null ? x.QSID.ToLower().Contains(SerachText.ToLower()) : false) ||
                (x.ListFieldName != null ? x.ListFieldName.ToLower().Contains(SerachText.ToLower()) : false) || (x.ShortName != null ? x.ShortName.ToLower().Contains(SerachText.ToLower()) : false)
                || (x.ListPhrase != null ? x.ListPhrase.ToLower().Contains(SerachText.ToLower()) : false)).ToList();
            }
            if (SearchTypeFilter == "Production Date Timestamp")
            {
                if (ProductionDate_From != null || ProductionDate_To != null)
                {
                    list_Library_Information_QSID_VM = ApplyFilterDataProductionTimestamp(list_Library_Information_QSID_VM);
                }
            }
            if (SearchTypeFilter == "Normalization Date Timestamp")
            {
                if (ProductionDate_From != null || ProductionDate_To != null)
                {
                    var listQsid_id = _objLibraryFunction_Service.FilterNormalizedHistorySession(ProductionDate_From.Value, ProductionDate_To.Value);
                    list_Library_Information_QSID_VM = (from c in list_Library_Information_QSID_VM
                                                        join q in listQsid_id
                                                        on c.QSID_ID equals q.Value
                                                        select c).ToList();
                }
            }


            if (SearchTypeFilter == "List Currently Checked Out")
            {
                List<int> listCheckedOutQSID = _objAccessVersion_Service.GetOnlyCheckedOutQSIDList(ProductionDate_From, ProductionDate_To);
                list_Library_Information_QSID_VM = (from c in list_Library_Information_QSID_VM
                                                    join q in listCheckedOutQSID
                                                    on c.QSID_ID equals q
                                                    select c).ToList();

            }
            if (SearchTypeFilter == "WorkOrder Number" || SearchTypeFilter == "Notes" || SearchTypeFilter == "CSCTicket Number")
            {
                if (SelectedSearchTypeCondition != null && !string.IsNullOrEmpty(InputTextSearch))
                {
                    List<int> listCheckedOutQSID = _objAccessVersion_Service.GetSearchWorkOrderAndNote(SearchTypeFilter, SelectedSearchTypeCondition.Text, InputTextSearch);
                    list_Library_Information_QSID_VM = (from c in list_Library_Information_QSID_VM
                                                        join q in listCheckedOutQSID
                                                        on c.QSID_ID equals q
                                                        select c).ToList();
                }
            }

            ListQSID_VM = new ObservableCollection<Library_QSID_Information_VM>(list_Library_Information_QSID_VM);
            if (CurrentSelectedQSID_VM != null)
            {
                SelectedQSID_VM = ListQSID_VM.Where(x => x.QSID_ID == CurrentSelectedQSID_VM.QSID_ID).FirstOrDefault();
                NotifyPropertyChanged("SelectedQSID_VM");
            }
            GetFilterQuery();
            this.NotifyPropertyChanged("ListQSID_VM");
        }




        public void BindListFilter_ColumnsData()
        {
            ListCountry = new ObservableCollection<ListCountries_ViewModel>(Engine._listCountry);
            ListModule = new ObservableCollection<ListModule_ViewModel>(Engine._listModule);
            ListTopic = new ObservableCollection<ListTopic_ViewModel>(Engine._listTopic);
        }

        public List<Library_QSID_Information_VM> ApplyFilterDataProductionTimestamp(List<Library_QSID_Information_VM> list_Library_Information_QSID_VM)
        {
            var qsid_List = _objLibraryFunction_Service.GetListQsxxx_Cas_UniqueByQSID();


            var listHistory = _objAccessVersion_Service.GetAllVersionHistoryByProductionDate(ProductionDate_From, ProductionDate_To);
            return list_Library_Information_QSID_VM.Where(item => listHistory.Any(item2 => item2.QSID_ID == item.QSID_ID)).ToList();

        }


        public void LoadVersionHistoryAccordingToQSID()
        {
            if (SelectedQSID_VM != null)
            {
                //MessengerInstance.Send<PropertyChangedMessage<bool>>(new PropertyChangedMessage<bool>(this,commonDGQsidVersion_VM, true, true, "Show Loader"));
                VisibilityVersionHis_Loader = Visibility.Visible;
                NotifyPropertyChanged("VisibilityVersionHis_Loader");
                FetchVersionHistoryAccordingToQsid();
            }
            else
            {
                ListQSID_VersionHistory.Clear();
                NotifyPropertyChanged("ListQSID_VersionHistory");
            }
        }

        private void FetchVersionHistoryAccordingToQsid()
        {
            var listVersionHis = _objAccessVersion_Service.GetList_VersionHistory(SelectedQSID_VM.QSID_ID, SearchTypeFilter != "Normalization Date Timestamp" ? ProductionDate_From : (DateTime?)null, SearchTypeFilter != "Normalization Date Timestamp" ? ProductionDate_To : (DateTime?)null);
            //List<Access_QSID_Data_VM> _listQSID_VersionHistory = new ObservableCollection<Access_QSID_Data_VM>(listVersionHis);
            ListQSID_VersionHistory = listVersionHis;
            NotifyPropertyChanged("ListQSID_VersionHistory");
            EnableDisableControl();
            VisibilityVersionHis_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityVersionHis_Loader");
        }


        public void LoadNormalizedHistoryAccordingToQSID()
        {
            if (SelectedQSID_VM != null)
            {
                VisibilityNormalizedHis_Loader = Visibility.Visible;
                NotifyPropertyChanged("VisibilityNormalizedHis_Loader");
                Task.Run(() => FetchNormalizedHistory());
                // FetchNormalizedHistory();
            }
            else
            {

                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    ListQsid_NormalizedHistory.Clear();
                });

                NotifyPropertyChanged("ListQsid_NormalizedHistory");
            }
        }

        private void FetchNormalizedHistory()
        {
            List<Log_Session_NormalizeHistory_VM> _listQSID_NormalizedHistory = _objLibraryFunction_Service.GetNormalizedHistorySessionByQSID_Version(SelectedQSID_VM.QSID_ID);
            ListQsid_NormalizedHistory = _listQSID_NormalizedHistory;
            NotifyPropertyChanged("ListQsid_NormalizedHistory");
            VisibilityNormalizedHis_Loader = Visibility.Hidden;
            NotifyPropertyChanged("VisibilityNormalizedHis_Loader");
        }
        public void EnableDisableControl()
        {
            if (SelectedQSID_VM != null)
            {
                IsDisplayCheckOut = _objAccessVersion_Service.CheckActiveCheckOutButton(SelectedQSID_VM.QSID_ID);
                IsDisplayCheckIn = _objAccessVersion_Service.CheckActiveCheckInButton(SelectedQSID_VM.QSID_ID);
                IsDisplayUnLock = _objAccessVersion_Service.CheckUnlockForCurrentUser(SelectedQSID_VM.QSID_ID);
                NotifyPropertyChanged("IsDisplayCheckOut");
                NotifyPropertyChanged("IsDisplayCheckIn");
                NotifyPropertyChanged("IsDisplayUnLock");
            }
        }
        public void GetFilterQuery()
        {
            // string queryString = string.Empty;
            string staticFilterQuery = GetStaticFilterQuery();
            //foreach (var item in _listfilter)
            //{
            //    if (item.ColValue != "")
            //        queryString += " " + item.ColName + " Like '" + item.ColValue + "',";
            //}
            SearchCriteria = (staticFilterQuery != string.Empty ? staticFilterQuery + "," : "") + (!string.IsNullOrEmpty(ListDetailFilterQueryString) ? ListDetailFilterQueryString.Trim(',') : "");
        }
        public string GetStaticFilterQuery()
        {
            string queryString = string.Empty;
            if (SerachText != null)
            {
                if (SerachText.Trim(' ') != string.Empty)
                {
                    queryString += " Qsid/Phrase Like '" + SerachText + "',";
                }
            }
            if (SearchTypeFilter == "Production Date Timestamp")
            {
                if (ProductionDate_From != null)
                {
                    queryString += " ProductionDate From Like '" + ProductionDate_From + "',";
                }
                if (ProductionDate_To != null)
                {
                    queryString += " ProductionDate To Like '" + ProductionDate_To + "',";
                }
            }
            if (SearchTypeFilter == "Normalization Date Timestamp")
            {
                if (ProductionDate_From != null)
                {
                    queryString += " NormalizeDate From Like '" + ProductionDate_From + "',";
                }
                if (ProductionDate_To != null)
                {
                    queryString += " NormalizeDate To Like '" + ProductionDate_To + "',";
                }
            }

            if (SearchTypeFilter == "List Currently Checked Out")
            {
                if (ProductionDate_From == null && ProductionDate_To == null)
                {

                }
                if (ProductionDate_From != null)
                {
                    queryString += " CheckedOut Date From Like '" + ProductionDate_From + "',";
                }
                if (ProductionDate_To != null)
                {
                    queryString += " CheckedOut Date To Like '" + ProductionDate_To + "',";
                }

            }
            if (SearchTypeFilter == "WorkOrder Number" || SearchTypeFilter == "Notes" || SearchTypeFilter == "CSCTicket Number")
            {
                if (SelectedSearchTypeCondition != null && !string.IsNullOrEmpty(InputTextSearch))
                {
                    queryString += SearchTypeFilter + " " + SelectedSearchTypeCondition.Text + " '" + InputTextSearch + "',";
                }
            }

            return queryString.Trim(',');
        }
        public void OpenPopupNormalizedHistory()
        {
            if (SelectedNormalizedHistory != null)
            {
                PopUp_NormalizationHistory _popNormalization = new PopUp_NormalizationHistory(SelectedNormalizedHistory, CellInfo.Column.Header.ToString(), _SelectedQSID_VM.QSID_ID);
                _popNormalization.Show();
                BindQsidDetailToView();
            }
        }


        public void ExecuteFunctionalityOnTabChanged()
        {
            CurrentSelectedQSID_VM = null;
            switch (SelectedTabItem.Header)
            {
                case "List Version History":
                    BindQSIDGrid();
                    break;
            }

        }

        public void ShowActiveTabCasAssignment()
        {
            if (Engine.IsOpenCasAssignmentFromAccess)
            {
                IsSelectedCasAssigmentTab = true;
            }
            else
            {
                IsSelectedCasAssigmentTab = false;
            }
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            IsSelectedCurrentDataView = false;
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            IsSelectedNormalization = false;
            NotifyPropertyChanged("IsSelectedNormalization");
        }
        public string Qsid { get; set; }
        public void GetRefreshListDetailCommonControl()
        {
            comonDG_VM = new CommonDataGrid_ViewModel<Library_QSID_Information_VM>(GetListDetailGridCol(), "QSID", "List Details :");
            NotifyPropertyChanged("comonDG_VM");

            commonDGQsidVersion_VM = new CommonDataGrid_ViewModel<Access_QSID_Data_VM>(GetQsidVersionGridCol(), "Version", "", true);
            NotifyPropertyChanged("commonDGQsidVersion_VM");

            commonDGQsidNormalizationHistory_VM = new CommonDataGrid_ViewModel<Log_Session_NormalizeHistory_VM>(GetQsidNormalizationGridCol(), "VersionNumber", "");
            NotifyPropertyChanged("commonDGQsidNormalizationHistory_VM");
        }

        private List<CommonDataGridColumn> GetQsidNormalizationGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();

            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Version", ColBindingName = "VersionNumber", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Normalized By", ColBindingName = "NormalizedBy", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TimeStamp", ColBindingName = "NormalizedTimestamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Unique RowID Count", ColBindingName = "UniqueRowID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Errors", ColBindingName = "Error_Failed", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Warnings", ColBindingName = "Warning_Failed", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Rows Added", ColBindingName = "UniqueAdd", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Rows Changed", ColBindingName = "UniqueChange", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Rows Deleted", ColBindingName = "UniqueDelete", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Added", ColBindingName = "UniqueCasAdded", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Deleted", ColBindingName = "UniqueCasDeleted", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Fields Added", ColBindingName = "FieldsAdded", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Fields Deleted", ColBindingName = "FieldsDeleted", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explain Changes", ColBindingName = "FieldExplainChanges", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Normalization Status", ColBindingName = "NormalizationStatus", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SessionID", ColBindingName = "SessionId", ColType = "Textbox" });
            return listColumnQsidDetail;
        }

        public List<CommonDataGridColumn> GetQsidVersionGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "User Name", ColBindingName = "UserName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Version", ColBindingName = "Version", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Check In TimeStamp", ColBindingName = "Check_In_TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Check Out TimeStamp", ColBindingName = "Check_Out_TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Note", ColBindingName = "AddNote", ColType = "Textbox", ColumnWidth = "250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product TimeStamp", ColBindingName = "ProductTimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "WO Number", ColBindingName = "WorkOrderNumber", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Ticket Number", ColBindingName = "TicketNumber", ColumnWidth = "200", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Change Type Header", ColBindingName = "TypeChange", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Editorial Change Type", ColBindingName = "EditorialChangeType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Unique Substances", ColBindingName = "UniqueSubstance", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Total Records", ColBindingName = "TotalCount", ColType = "Textbox" });


            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetListDetailGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "QSID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Field Name", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EasiPro ListID(s)", ColBindingName = "ListId", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EHS ListCode(s)", ColBindingName = "EHS_ListCodes", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Phrase", ColBindingName = "ListPhrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Short Name", ColBindingName = "ShortName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Publication Date", ColBindingName = "ListPublicationDate", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product(s)", ColBindingName = "ProductName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Module", ColBindingName = "Module", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Topic", ColBindingName = "Topic", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "List Type / Category", ColBindingName = "ListType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "List Industry Vertical", ColBindingName = "ListIndustryVertical", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "List Obligation Type", ColBindingName = "ListObligationType", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Currently Checked Out", ColBindingName = "CurrentlyCheckedOut", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "IsDeleted", ColBindingName = "IsDeleted", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Confluence Note URL", ColBindingName = "NoteUrl", ColType = "Hyperlink", ColumnWidth = "250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Short Description", ColBindingName = "ShortDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Qsids", ColBindingName = "ParentQsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Qsids", ColBindingName = "ChildQsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Generics Count", ColBindingName = "GenericsCount", ColType = "Textbox" });
            return listColumnQsidDetail;
        }

        public void BindMapUserCommentList()
        {
            Task.Run(() =>
            {
                var listComment = _objLibraryFunction_Service.GetUserComments(Environment.UserName);
                listUserComments = new ObservableCollection<UserComments_VM>(listComment);
                NotifyPropertyChanged("listUserComments");
            });
        }

        public void RefreshMapUserComments()
        {
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(Callback_RefreshMapUserComments);
            dispatcherTimer.Interval = new TimeSpan(0, 5, 0);
            dispatcherTimer.Start();
        }
        private void Callback_RefreshMapUserComments(object sender, EventArgs e)
        {
            BindMapUserCommentList();
        }
        private void CommandFilterInCasAssignmentExecute(object obj)
        {
            UserComments_VM objUserCom = (UserComments_VM)obj;
            UpdateUserCommentReview(objUserCom.CommentUserID);

        }

        private void CommandFilterInSubstanceIdentityExecute(object obj)
        {
            UserComments_VM objUserCom = (UserComments_VM)obj;
            var filterCas = string.Empty;
            if (objUserCom.CommentType == "ParentChild")
            {
                filterCas = objUserCom.ChildCas;
            }
            else
            {
                filterCas = objUserCom.ParentCas;
            }
            IsSelectedSubstanceIdentityTab = true;
            NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
            IsSelectedGenerics = false;
            NotifyPropertyChanged("IsSelectedGenerics");
            IsSelectedCasAssigmentTab = false;
            IsSelectedCurrentDataView = false;
            IsSelectedNormalization = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            NotifyPropertyChanged("IsSelectedNormalization");
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", filterCas, "SearchCasInSubstanceIdentityTab"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
            UpdateUserCommentReview(objUserCom.CommentUserID);

        }

        private void CommandFilterInTreeViewExecute(object obj)
        {
            UserComments_VM objUserCom = (UserComments_VM)obj;
            var filterCas = string.Empty;
            if (objUserCom.CommentType == "ParentChild")
            {
                if (!string.IsNullOrEmpty(objUserCom.ChildCas))
                {
                    filterCas = objUserCom.ChildCas;
                }
                else
                {
                    filterCas = objUserCom.ParentCas;
                }
            }
            else
            {
                filterCas = objUserCom.ParentCas;
            }
            IsSelectedSubstanceIdentityTab = false;
            NotifyPropertyChanged("IsSelectedSubstanceIdentityTab");
            IsSelectedGenerics = true;
            NotifyPropertyChanged("IsSelectedGenerics");
            IsSelectedCasAssigmentTab = false;
            IsSelectedCurrentDataView = false;
            IsSelectedNormalization = false;
            NotifyPropertyChanged("IsSelectedCasAssigmentTab");
            NotifyPropertyChanged("IsSelectedCurrentDataView");
            NotifyPropertyChanged("IsSelectedNormalization");
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", filterCas, "SearchCasInTreeFromCurrentDataView"), typeof(GenericManagementTool.ViewModels.Generics_VM));
            UpdateUserCommentReview(objUserCom.CommentUserID);
        }

        private void CommandMarkReviewUserCommentsExecute(object obj)
        {
            UserComments_VM objUserCom = (UserComments_VM)obj;
            UpdateUserCommentReview(objUserCom.CommentUserID);
        }
        public void UpdateUserCommentReview(int? commentUserID)
        {
            if (commentUserID != null)
            {
                _objLibraryFunction_Service.UpdateReviewMapUserComments((int)commentUserID);
                BindMapUserCommentList();
            }
        }
        #endregion

        private void FilterCheckOutUser(string name)
        {
            var filterdata = new FilterData(FilterOperator.Equals, FilterType.List, "CurrentlyCheckedOut.Id", typeof(int), name.ToUpper().Replace("I", ""), "", true, true);
            comonDG_VM.OverrideQueryController.ColumnFilterData = filterdata;
            comonDG_VM.DataGridFilteringFinished(null, null);
        }
        private void FilterAddSuggestion(string filterData)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", filterData, "FilterAddSuggestionReview_Generic"), typeof(GenericManagementTool.ViewModels.Generics_VM));

        }

    }

    public abstract class BaseConverter : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class StringFormatConverter : BaseConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (Engine.listAllPermissionDashboard.Any(x => x.UniquePermissionName == parameter.ToString()))
            {
              return  Visibility.Visible;
            }
            else {
                return Visibility.Collapsed;
            }            
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                        System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
