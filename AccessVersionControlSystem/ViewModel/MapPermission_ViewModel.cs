﻿using AccessVersionControlSystem.Library;
using CRA_DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessVersionControlSystem.ViewModel
{
    public class MapPermission_ViewModel : Base_ViewModel
    {
        public string Name { get; set; }
        public int PermissionID { get; set; }
        public int ParentPermissionID { get; set; }
        public bool IsChecked { get; set; }
        private int? _ChildCount { get; set; }
        public int? ChildCount { get { return _ChildCount; } set { _ChildCount = value; NotifyPropertyChanged("ChildCount"); } }
        public int RoleID { get; set; }
        public ObservableCollection<MapPermission_ViewModel> SubGeneric { get; set; }
        private bool _IsExpanded { get; set; }
        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set
            {
                _IsExpanded = value;
                if (_IsExpanded)
                {
                    this.SubGeneric = new ObservableCollection<MapPermission_ViewModel>(GetChildren());
                }
                else
                {
                    if (this.ChildCount > 0)
                    {
                        this.SubGeneric = new ObservableCollection<MapPermission_ViewModel>(GetDummyChildren());
                    }
                }

                NotifyPropertyChanged("SubGeneric");
                NotifyPropertyChanged("IsExpanded");
            }
        }

        private List<MapPermission_ViewModel> GetChildren()
        {
            return _objLibraryFunction_Service.GetPermissionsByRole(RoleID, ParentPermissionID).Select(x => new MapPermission_ViewModel()
            {
                PermissionID = x.PermissionID,
                ParentPermissionID = x.ParentPermissionID,
                Name = x.Name,
                IsChecked = x.IsChecked,
                RoleID = x.RoleID,
                ChildCount=x.ChildCount,
                IsExpanded=false
            }).ToList();
        }
        private List<MapPermission_ViewModel> GetDummyChildren()
        {
            var list= new List<MapPermission_ViewModel>();
            list.Add(new MapPermission_ViewModel() {Name="test" });
            return list;
        }
    }


}
