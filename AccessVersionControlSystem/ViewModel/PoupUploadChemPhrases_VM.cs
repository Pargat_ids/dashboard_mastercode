﻿using AccessVersionControlSystem.Library;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class PoupUploadChemPhrases_VM : Base_ViewModel
    {
        public string lblHeading { get; set; }
        public CommonDataGrid_ViewModel<IpopValues> comonUploadPopup_VM { get; set; }
        public Visibility IsVisibleBtn { get; set; } = Visibility.Collapsed;
        public ICommand CommandSaveInput { get; set; }
        public CommonFunctions objCommon;
        private List<IpopValues> lstFinal { get; set; }

        private bool _ChkSelectAll { get; set; }
         public bool ChkSelectAll
        {
            get => _ChkSelectAll;
            set
            {
                if (Equals(_ChkSelectAll, value))
                {
                    return;
                }

                _ChkSelectAll = value;
                if (_ChkSelectAll == true)
                {
                    lstFinal.ForEach(x =>
                        {
                            x.IsSelected = true;
                        });
                   
                }
                else
                {
                    lstFinal.ForEach(x =>
                    {
                        x.IsSelected = false;
                    });
                }
                UniqueList(lstFinal);
            }
        }
        public PoupUploadChemPhrases_VM(string _messageHeader, List<IpopValues> lst)
        {
            if (_messageHeader == "Phrase Name not Exist in Library" || _messageHeader == "Chemical Name not Exist in Library")
            {
                IsVisibleBtn = Visibility.Visible;
            }
            else
            {
                IsVisibleBtn = Visibility.Collapsed;
            }
            lblHeading = _messageHeader;
            NotifyPropertyChanged("lblHeading");
            lstFinal = lst;
            CommandSaveInput = new RelayCommand(CommandSaveInputExecute, CommandInsertUpdateRolePermissionCanExecute);
            comonUploadPopup_VM = new CommonDataGrid_ViewModel<IpopValues>(GetView(), _messageHeader, _messageHeader);
            
            NotifyPropertyChanged("IsVisibleBtn");
            Init(_messageHeader);
            objCommon = new CommonFunctions();
            Task.Run(() => UniqueList(lstFinal));

        }
        private bool CommandInsertUpdateRolePermissionCanExecute(object obj)
        {
            return true;
        }
        private List<CommonDataGridColumn> GetView()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = lblHeading == "Phrase Name Exist in Library" || lblHeading == "Chemical Name Exist in Library", ColName = lblHeading == "Phrase Name Exist in Library" ? "PhraseID" : "ChemicalNameID", ColBindingName = "ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = lblHeading == "Phrase Name Exist in Library" || lblHeading == "Phrase Name not Exist in Library" || lblHeading == "Chemical Name Exist in Library" || lblHeading == "Chemical Name not Exist in Library", ColName = lblHeading == "Phrase Name Exist in Library" || lblHeading == "Phrase Name not Exist in Library" ? "Phrase" : "ChemicalName", ColBindingName = "Name", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageID", ColBindingName = "LanguageID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = lblHeading == "Phrase Name not Exist in Library" || lblHeading == "Chemical Name not Exist in Library", ColName = "IsSelected", ColBindingName = "IsSelected", ColType = "CheckboxEnable" });
            return listColumn;
        }
        public static string CommonListConn;
        private ObservableCollection<IpopValues> _lstUploadPopup { get; set; } = new ObservableCollection<IpopValues>();
        public ObservableCollection<IpopValues> lstUploadPopup
        {
            get { return _lstUploadPopup; }
            set
            {
                if (_lstUploadPopup != value)
                {
                    _lstUploadPopup = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IpopValues>>>(new PropertyChangedMessage<List<IpopValues>>(null, _lstUploadPopup.ToList(), "List"));
                }
            }
        }
        private void UniqueList(List<IpopValues> lst)
        {
            Task.Run(() =>
            {
                lstUploadPopup = new ObservableCollection<IpopValues>(lst);
                NotifyPropertyChanged("lstUploadPopup");
                NotifyPropertyChanged("comonUploadPopup_VM");
            });

        }
        public void Init(string _messageHeader)
        {
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            comonUploadPopup_VM = new CommonDataGrid_ViewModel<IpopValues>(GetView(), _messageHeader, _messageHeader);
            NotifyPropertyChanged("comonUploadPopup_VM");
        }
        public void CommandSaveInputExecute(object obj)
        {
            Engine.UploadChemPhrase = true;
            if (lblHeading == "Chemical Name not Exist in Library")
            {
                _objLibraryFunction_Service.saveChemicals(lstFinal);
            }
            else
            {
                _objLibraryFunction_Service.savePhrases(lstFinal);
            }
            _notifier.ShowSuccess("Data Saved Successfully");
        }

    }


}

