﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using GenericManagementTool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class SubstanceSearchUserControl_VM : Base_ViewModel
    {
        public DateTime? _Date_To { get; set; }
        public DateTime? Date_To 
        {
            get { return this._Date_To; }
            set
            {
                if (this._Date_To != value)
                {
                    this._Date_To = value;
                }
            }
        }
        public DateTime? _Date_From { get; set; }
        public DateTime? Date_From
        {
            get { return this._Date_From; }
            set
            {
                if (this._Date_From != value)
                {
                    this._Date_From = value;
                }
            }
        }
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public SubstanceSearchUserControl_VM()
        {
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                Init();
            }
        }

        public string CasSearch { get; set; }
        //public bool IsFoodDataCasSearch { get; set; }
        public string chemicalName { get; set; }
        // public bool IsFoodDataChemName { get; set; }
        public string SearchQsid { get; set; }
        public string casVariant { get; set; }
        public bool IsSuffix { get; set; }
        public bool IsPrefix { get; set; }
        //  public bool IsFoodDataMultipleCas { get; set; }
        public string txtSearchPhrase { get; set; }
        public string txtSearchIdentifier { get; set; }
        public bool IsPhrasExactMatch { get; set; }
        public bool IsIdentifierExactMatch { get; set; }
        public Visibility VisibilityDateSearch_Loader { get; set; }
        public string VisibilityPhraseSearch_Loader { get; set; }
        public string VisibilityIdentifierSearch_Loader { get; set; }
        public string VisibilityMultipleCasSearch_Loader { get; set; }
        public string VisibilityCasVarientSearch_Loader { get; set; }
        public string VisibilityCasHistorySearch_Loader { get; set; }
        public string VisibilitySubstanceNameSearch_Loader { get; set; }
        public string VisibilityCasSearch_Loader { get; set; }
        public Visibility VisibilityFieldNameSearch_Loader { get; set; } = Visibility.Collapsed;

        public bool IsExactMatch { get; set; }
        public bool IsPartialMatch { get; set; }
        public bool IsBeginWith { get; set; }
        public bool IsEndWith { get; set; }
        public CommonDataGrid_ViewModel<DateSearchTab_VM> dcDateSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<CasSearchSubstanceTab_VM> dcCasSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<SubstanceSearchTab_VM> dcSubstanceSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<CasHistorySubstanceSearch> dcSubstanceCasHistorySearch_VM { get; set; }
        public CommonDataGrid_ViewModel<SubstanceSearchCasVariant_VM> dcSubstanceCasVarientSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<SubstanceSearchIdentifier_VM> dcSubstanceIdentifierSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<SubstanceSearchPhrase_VM> dcSubstancePhraseSearch_VM { get; set; }
        public CommonDataGrid_ViewModel<SameNameMultipleCas_VM> dcSameNameUnAccepted { get; set; }
        public CommonDataGrid_ViewModel<SameNameMultipleCas_AcceptedReview_VM> dcSameNameAcceptedReview { get; set; }
        public CommonDataGrid_ViewModel<FieldNameSearch_VM> dcFieldNameSearch_VM { get; set; }
        public SelectListItem SelectedSearchType { get; set; }
        public SelectListItem SelectedSearchTypeForCas { get; set; }

        private string _SelectedCasSearch { get; set; }
        public string SelectedCasSearchUpd { get; set; }
        public string SelectedCasSearch
        {
            get { return _SelectedCasSearch; }
            set
            {
                if (_SelectedCasSearch != value)
                {
                    _SelectedCasSearch = value;
                    NotifyPropertyChanged("SelectedCasSearch");
                    if (value == "Search By CAS")
                    {
                        SelectedCasSearchUpd = "Enter CAS";
                    }
                    else
                    {
                        SelectedCasSearchUpd = "Enter CASID";
                    }
                    NotifyPropertyChanged("SelectedCasSearchUpd");
                }

            }
        }
        public SelectListItem SelectedSearchTypeForIdentifer { get; set; }
        public SelectListItem SelectedSearchTypeForPhrase { get; set; }
        public ObservableCollection<SelectListItem> ListSearchType { get; set; }
        public ObservableCollection<string> ListCASSearchType { get; set; }

        private TabItem _SelectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get { return _SelectedTabItem; }
            set
            {
                if (_SelectedTabItem != value)
                {
                    _SelectedTabItem = value;
                    OnTabChangedFunction();
                }

            }
        }

        private void OnTabChangedFunction()
        {
            switch (SelectedTabItem.Header)
            {
                case "Cas Search":
                    if (!string.IsNullOrEmpty(CasSearch))
                    {
                        SearchCas();
                    }
                    break;

            }
        }

        public TabItem SelectedMultipleCasTabItem { get; set; }
        public Library_IdentifierCategories SelectedIdentType { get; set; }
        public Library_PhraseCategories SelectedPhraseCategory { get; set; }

        public ICommand CommandDateSearch { get; set; }
        public ICommand CommandCasSearch { get; set; }
        public ICommand CommandSubstanceNameSearch { get; set; }
        public ICommand CommandCasHistory { get; set; }
        public ICommand CommandCasVariant { get; set; }
        public ICommand CommandMultipleCas { get; set; }
        public ICommand CommandIdentifierSearch { get; set; }
        public ICommand CommandPhraseSearch { get; set; }
        public ICommand CommandUpdateSameNameMultipleCas { get; set; }





        public ObservableCollection<DateSearchTab_VM> ListDateSearch { get; set; } = new ObservableCollection<DateSearchTab_VM>();
        public ObservableCollection<CasSearchSubstanceTab_VM> ListCasSearch { get; set; } = new ObservableCollection<CasSearchSubstanceTab_VM>();
        public List<SubstanceSearchTab_VM> ListSubstanceNameSearch { get; set; } = new List<SubstanceSearchTab_VM>();
        public SubstanceSearchTab_VM SelectedSubstanceNameSearch { get; set; } = new SubstanceSearchTab_VM();
        public ObservableCollection<CasHistorySubstanceSearch> _listCasHistory { get; set; } = new ObservableCollection<CasHistorySubstanceSearch>();
        public ObservableCollection<CasHistorySubstanceSearch> ListCasHistory
        {
            get { return _listCasHistory; }
            set
            {

                if (value != _listCasHistory)
                {
                    _listCasHistory = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<CasHistorySubstanceSearch>>>(new PropertyChangedMessage<List<CasHistorySubstanceSearch>>(_listCasHistory.ToList(), _listCasHistory.ToList(), "Default List"));
                    NotifyPropertyChanged("ListCasHistory");
                }
            }
        }
        public ObservableCollection<Library_IdentifierCategories> ListIdentCategories { get; set; }
        public ObservableCollection<Library_PhraseCategories> ListPhraseCategories { get; set; }
        public List<SubstanceSearchCasVariant_VM> _listCasVariantSearch { get; set; } = new List<SubstanceSearchCasVariant_VM>();
        public List<SubstanceSearchCasVariant_VM> ListCasVariantSearch
        {
            get { return _listCasVariantSearch; }
            set
            {
                if (_listCasVariantSearch != value)
                {
                    _listCasVariantSearch = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SubstanceSearchCasVariant_VM>>>(new PropertyChangedMessage<List<SubstanceSearchCasVariant_VM>>(null, _listCasVariantSearch, "Default List"));
                }

            }
        }
        private List<SameNameMultipleCas_VM> _listMultipleCasSearch { get; set; } = new List<SameNameMultipleCas_VM>();
        private List<SameNameMultipleCas_AcceptedReview_VM> _listMultipleCasSearch_Review { get; set; } = new List<SameNameMultipleCas_AcceptedReview_VM>();
        public List<SameNameMultipleCas_VM> ListMultipleCasSearch
        {
            get { return _listMultipleCasSearch; }
            set
            {
                if (value != _listMultipleCasSearch)
                {
                    _listMultipleCasSearch = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SameNameMultipleCas_VM>>>(new PropertyChangedMessage<List<SameNameMultipleCas_VM>>(null, _listMultipleCasSearch, "Default List"));
                }
            }
        }
        public List<SameNameMultipleCas_AcceptedReview_VM> ListMultipleCasSearch_Review
        {
            get { return _listMultipleCasSearch_Review; }
            set
            {
                if (value != _listMultipleCasSearch_Review)
                {

                    _listMultipleCasSearch_Review = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SameNameMultipleCas_AcceptedReview_VM>>>(new PropertyChangedMessage<List<SameNameMultipleCas_AcceptedReview_VM>>(null, _listMultipleCasSearch_Review, "Default List"));

                }
            }
        }
        public List<SubstanceSearchIdentifier_VM> _listIdentifierDetail { get; set; }
        public List<SubstanceSearchIdentifier_VM> ListIdentifierDetail
        {
            get { return _listIdentifierDetail; }
            set
            {
                if (_listIdentifierDetail != value)
                {
                    _listIdentifierDetail = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SubstanceSearchIdentifier_VM>>>(new PropertyChangedMessage<List<SubstanceSearchIdentifier_VM>>(null, _listIdentifierDetail, "Default List"));
                }
            }
        }
        private List<SubstanceSearchPhrase_VM> _listPhraseDetailSearch { get; set; } = new List<SubstanceSearchPhrase_VM>();
        public List<SubstanceSearchPhrase_VM> ListPhraseDetailSearch
        {
            get { return _listPhraseDetailSearch; }
            set
            {
                if (_listPhraseDetailSearch != value)
                {
                    _listPhraseDetailSearch = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SubstanceSearchPhrase_VM>>>(new PropertyChangedMessage<List<SubstanceSearchPhrase_VM>>(null, _listPhraseDetailSearch, "Default List"));
                }
            }
        }
        public List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM> _listFlatGenerics { get; set; }
        public List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM> ListFlatGenerics
        {
            get { return _listFlatGenerics; }
            set
            {
                if (_listFlatGenerics != value)
                {
                    _listFlatGenerics = value;
                    MessengerInstance.Send<GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>>>(new GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>>(null, _listFlatGenerics, "Default List"));
                }
            }
        }

        private List<FieldNameSearch_VM> _listFieldNameSearch { get; set; } = new List<FieldNameSearch_VM>();
        public List<FieldNameSearch_VM> ListFieldNameSearch
        {
            get { return _listFieldNameSearch; }
            set
            {
                _listFieldNameSearch = value;
                MessengerInstance.Send<PropertyChangedMessage<List<FieldNameSearch_VM>>>(new PropertyChangedMessage<List<FieldNameSearch_VM>>(null, _listFieldNameSearch, "Default List"));
            }
        }

        public DataGridCellInfo _cellInfo { get; set; }
        public DataGridCellInfo CellInfo
        {
            get { return _cellInfo; }
            set
            {
                _cellInfo = value;
            }
        }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public RelayCommand commandOpenCurrentDataViewNewAddList { get; set; }
        public ObservableCollection<MapProductListVM> listProductsCasSearch { get; set; }
        public ObservableCollection<MapProductListVM> listProductsSubstanceSearch { get; set; }
        public ObservableCollection<MapProductListVM> listProductsSameNameMultipleCas { get; set; }
        public ObservableCollection<MapProductListVM> listProductsIdentifierSearch { get; set; }
        public ObservableCollection<MapProductListVM> listProductsPhraseSearch { get; set; }
        public ObservableCollection<MapProductListVM> listProductsFieldNameSearch { get; set; }

        public ObservableCollection<SelectListItem> ListFieldNameSearchType { get; set; }
        private SelectListItem _selectedFieldNameType { get; set; }
        public SelectListItem SelectedFieldNameType
        {
            get
            {
                return _selectedFieldNameType;
            }
            set
            {
                _selectedFieldNameType = value;
                if (_selectedFieldNameType.Text != null)
                {
                    if (_selectedFieldNameType.Text == "Field Name")
                    {
                        txtFieldNameHint = "Enter Field Name";
                    }
                    else if (_selectedFieldNameType.Text == "Field Name ID")
                    {
                        txtFieldNameHint = "Enter Field Name ID";
                    }
                    else {
                        txtFieldNameHint = "";
                    }
                }
                else
                {
                    txtFieldNameHint = "";
                }
                NotifyPropertyChanged("txtFieldNameHint");
            }
        }
        public string txtFieldNameHint { get; set; }

        public SelectListItem SelectedSearchTypeForFieldName { get; set; }
        public RelayCommand CommandFieldNameSearch { get; set; }
        public string FieldNameSearch { get; set; }
        public PopUp_GenericsDetail objPopUp { get; set; }
        public CommonDataGrid_ViewModel<FlatGeneric_CurrentData_VM> commonDG_GenericFlat_VM { get; set; }
        public void Init()
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                commonDG_GenericFlat_VM = new CommonDataGrid_ViewModel<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>(GetListGridColumn_Flat(), "", "Flat Generic List:");
                NotifyPropertyChanged("commonDG_GenericFlat_VM");
                //commonDG_GenericCombined_VM = new DataGrid_Generic_VM<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM>(GetListGridColumn_Flat(true), "", "Combined Generic List:");
                //NotifyPropertyChanged("commonDG_GenericCombined_VM");
                objPopUp = new PopUp_GenericsDetail();
                objPopUp.DataContext = this;
            });
            VisibilityCasSearch_Loader = "Hidden";
            VisibilitySubstanceNameSearch_Loader = "Hidden";
            VisibilityCasHistorySearch_Loader = "Hidden";
            VisibilityCasVarientSearch_Loader = "Hidden";
            VisibilityMultipleCasSearch_Loader = "Hidden";
            VisibilityIdentifierSearch_Loader = "Hidden";
            VisibilityPhraseSearch_Loader = "Hidden";

            BindComboListIdentifier();
            BindComboListPhraseCategories();
            GetRefreshListDetailCommonControl();
            BindMapProductList();
            CommandDateSearch = new RelayCommand(CommandDateSearchExecute);
            CommandCasSearch = new RelayCommand(CommandCasSearchExecute, CommandCasSearchCanExecute);
            CommandSubstanceNameSearch = new RelayCommand(CommandSubstanceNameSearchExecute, CommandSubstanceNameSearchCanExecute);
            CommandCasHistory = new RelayCommand(CommandCasHistoryExecute, CommandCasHistoryCanExecute);
            CommandCasVariant = new RelayCommand(CommandCasVariantExecute, CommandCasVariantCanExecute);
            CommandMultipleCas = new RelayCommand(CommandMultipleCasExecute, CommandMultipleCasCanExecute);
            CommandIdentifierSearch = new RelayCommand(CommandIdentifierSearchExecute, CommandIdentifierSearchsCanExecute);
            CommandPhraseSearch = new RelayCommand(CommandPhraseSearchExecute, CommandPhraseSearchCanExecute);
            CommandGenerateAccessCasResult = new RelayCommand(CommandGenerateAccessCasResultExecuted, CommandGenerateAccessCasResultCanExecute);
            CommandGenerateExcelCasResult = new RelayCommand(CommandGenerateExcelCasResultExecuted, CommandGenerateAccessCasResultCanExecute);
            CommandUpdateSameNameMultipleCas = new RelayCommand(CommandUpdateSameNameMultipleCasExecuted, CommandUpdateSameNameMultipleCasCanExecute);
            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExected, commandOpenCurrentDataViewNewAddListCanExecte);
            CommandFieldNameSearch = new RelayCommand(CommandFieldNameSearchExecuted, CommandFieldNameSearchCanExecuted);
            var listItem = new List<SelectListItem>();
            listItem.Add(new SelectListItem { Text = "Partial Match" });
            listItem.Add(new SelectListItem { Text = "Exact Match" });
            listItem.Add(new SelectListItem { Text = "Begin with" });
            listItem.Add(new SelectListItem { Text = "End with" });


            ListSearchType = new ObservableCollection<SelectListItem>(listItem);

            var listItemCAS = new List<string>();
            listItemCAS.Add("Search By CAS");
            listItemCAS.Add("Search By CASID");
            ListCASSearchType = new ObservableCollection<string>(listItemCAS);

            SelectedCasSearch = ListCASSearchType.Where(x => x == "Search By CAS").FirstOrDefault();
            NotifyPropertyChanged("SelectedCasSearch");

            SelectedSearchTypeForCas = ListSearchType.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchTypeForCas");

            SelectedSearchType = ListSearchType.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchType");

            SelectedSearchTypeForIdentifer = ListSearchType.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchTypeForIdentifer");
            SelectedSearchTypeForPhrase = ListSearchType.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchTypeForPhrase");
            SelectedSearchTypeForFieldName = ListSearchType.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedSearchTypeForFieldName");
            NotifyPropertyChanged("ListSearchType");
            BindListFieldName();
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            MessengerInstance.Register<NotificationMessageAction<CasSearchSubstanceTab_VM>>(this, CallBackRefreshCasSeacrhExecute);
            MessengerInstance.Register<NotificationMessageAction<SubstanceSearchTab_VM>>(this, CallBackRefreshSubstanceSeacrhExecute);
            MessengerInstance.Register<NotificationMessageAction<SubstanceSearchIdentifier_VM>>(this, CallBackRefreshIdentifierSeacrhExecute);
            MessengerInstance.Register<NotificationMessageAction<SubstanceSearchPhrase_VM>>(this, CallBackRefreshPhraseSeacrhExecute);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceSearchTab_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(CasSearchSubstanceTab_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceSearchIdentifier_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceSearchPhrase_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(DateSearchTab_VM), CallbackHyperlink);



            MessengerInstance.Register<PropertyChangedMessage<SubstanceSearchTab_VM>>(this, NotifyMe);

        }
        public void BindFlatGenerics()
        {
            
            ListFlatGenerics = _objLibraryFunction_Service.GetAllFlatGenerics(SelectedSubstanceNameSearch.Cas, "ParentCasOnly").Select(x => new VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM
            {
                ParentCas = x.ParentCas,
                ChildCas = x.ChildCas,
                ParentChemicalName = x.ParentChemicalName,
                ChemicalName = x.ChemicalName

            }

            ).ToList();
            objPopUp.Show();
            objPopUp.Focus();
            
        }

        private void NotifyMe(PropertyChangedMessage<SubstanceSearchTab_VM> obj)
        {
            SelectedSubstanceNameSearch = obj.NewValue;
        }

        public void BindMapProductList()
        {
            Task.Run(() =>
            {
                var listProducts = AccessVersionControlSystem.Common.CommonFunctions.ConvertMapProductToViewModel(_objLibraryFunction_Service.GetAllLibraryProductLists());
                listProductsCasSearch = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                listProductsSubstanceSearch = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                listProductsSameNameMultipleCas = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                listProductsIdentifierSearch = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                listProductsPhraseSearch = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                listProductsFieldNameSearch = new ObservableCollection<MapProductListVM>(Common.CommonFunctions.Clone<MapProductListVM>(listProducts));
                NotifyPropertyChanged("listProductsCasSearch");
                NotifyPropertyChanged("listProductsSubstanceSearch");
                NotifyPropertyChanged("listProductsSameNameMultipleCas");
                NotifyPropertyChanged("listProductsIdentifierSearch");
                NotifyPropertyChanged("listProductsPhraseSearch");
                NotifyPropertyChanged("listProductsFieldNameSearch");
            });
        }
        public void BindListFieldName()
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Field Name" });
            list.Add(new SelectListItem { Text = "Field Name ID" });
            list.Add(new SelectListItem { Text = "All Field Name" });

            ListFieldNameSearchType = new ObservableCollection<SelectListItem>(list);
            SelectedFieldNameType = ListFieldNameSearchType.Where(x => x.Text == "Field Name").FirstOrDefault();
            NotifyPropertyChanged("SelectedFieldNameType");
            NotifyPropertyChanged("ListFieldNameSearchType");

        }
        private bool commandOpenCurrentDataViewNewAddListCanExecte(object obj)
        {
            return true;
        }

        private void commandOpenCurrentDataViewNewAddListExected(object obj)
        {

            //Task.Run(() =>
            //{
                try
                {
                    Engine.IsOpenCurrentDataViewFromVersionHis = true;
                    Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                    Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                    MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

                }
                catch (Exception ex)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);
                    });
                }
            //});
        }
        public void NotifyMe(string callAction)
        {

        }
        private void CallbackHyperlink(NotificationMessageAction<object> obj)
        {

            if (obj.Notification == "ViewSubstanceGenericTree")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedSubstanceNameSearch.Cas, "ActiveGenericTab"), typeof(SubstanceSearchUserControl_VM));
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedSubstanceNameSearch.Cas, "SearchCasInGenericTreeTab"), typeof(Generics_VM));
            }

            else if (obj.Notification == "ViewSubstanceIdentity")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedSubstanceNameSearch.Cas, "ActiveSubstanceIdentityWithCas"), typeof(SubstanceSearchUserControl_VM));
               // MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedSubstanceNameSearch.Cas, "SearchCasInSubstanceIdentityTab"), typeof(Substane_Identity.ViewModel.Win_Administrator_VM));
            }
            else if (obj.Notification == "ViewFlatGenericsDetail")
            {
                BindFlatGenerics();
            }
            else

            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
        private void CommandUpdateSameNameMultipleCasExecuted(object obj)
        {
            List<SameNameMultipleCas_VM> listMultipleCasUpdate = new List<SameNameMultipleCas_VM>();
            bool? IsInsertMultipleCas = null;
            if (SelectedMultipleCasTabItem.Header.ToString() == "Show Un-Accepted")
            {
                listMultipleCasUpdate = ListMultipleCasSearch.Where(x => x.Accepted == true).ToList();
                IsInsertMultipleCas = true;
            }
            else if (SelectedMultipleCasTabItem.Header.ToString() == "Review Accepted Pair")
            {
                listMultipleCasUpdate = ListMultipleCasSearch_Review.Where(x => x.Accepted == false).Select(x => new SameNameMultipleCas_VM() { Accepted = x.Accepted, Cas = x.Cas, CASID = x.CASID, ChemicalName = x.ChemicalName, ChemicalNameID = x.ChemicalNameID, ChemicalNameSquash = x.ChemicalNameSquash, Frequency = x.Frequency, ParentCAS = x.ParentCAS, Qsid = x.Qsid, SameNameMultipleCASID = x.SameNameMultipleCASID }).ToList();
                IsInsertMultipleCas = false;
            }
            if (IsInsertMultipleCas != null)
            {
                _objLibraryFunction_Service.InsertSameNameMultipleCas(listMultipleCasUpdate, IsInsertMultipleCas, Engine.CurrentUserSessionID);
                _notifier.ShowSuccess("Same name multiple cas" + (IsInsertMultipleCas == false ? " removed" : "") + " accepted successfully");
                CommandMultipleCasExecute(obj);
            }
        }

        private bool CommandUpdateSameNameMultipleCasCanExecute(object obj)
        {
            bool isUpdateMultipleCas = false;
            if (SelectedMultipleCasTabItem != null)
                if (SelectedMultipleCasTabItem.Header.ToString() == "Show Un-Accepted")
                {
                    isUpdateMultipleCas = ListMultipleCasSearch.Where(x => x.Accepted == true).Count() > 0 ? true : false;
                }
                else if (SelectedMultipleCasTabItem.Header.ToString() == "Review Accepted Pair")
                {
                    isUpdateMultipleCas = ListMultipleCasSearch_Review.Where(x => x.Accepted == false).Count() > 0 ? true : false;
                }
            return isUpdateMultipleCas;
        }

        private bool CommandGenerateAccessCasResultCanExecute(object obj)
        {
            bool IsEnableExportButton = false;
            if (SelectedTabItem != null)
            {
                switch (SelectedTabItem.Header)
                {
                    case "Cas Search":
                        IsEnableExportButton = ListCasSearch.Count > 0 ? true : false;
                        break;
                    case "Substance Name Search":
                        IsEnableExportButton = ListSubstanceNameSearch.Count > 0 ? true : false;
                        break;
                    case "Cas History":
                        IsEnableExportButton = ListCasHistory.Count > 0 ? true : false;
                        break;
                    case "Cas Variant":
                        IsEnableExportButton = ListCasVariantSearch.Count > 0 ? true : false;
                        break;
                    case "Same Name Multiple Cas":
                        if (SelectedMultipleCasTabItem.Header.ToString() == "Show Un-Accepted")
                        {
                            IsEnableExportButton = ListMultipleCasSearch.Count > 0 ? true : false;
                        }
                        else if (SelectedMultipleCasTabItem.Header.ToString() == "Review Accepted Pair")
                        {
                            IsEnableExportButton = ListMultipleCasSearch_Review.Count > 0 ? true : false;
                        }
                        break;
                    case "Identifier Search":
                        IsEnableExportButton = ListIdentifierDetail.Count > 0 ? true : false;
                        break;
                    case "Phrase Search":
                        IsEnableExportButton = ListPhraseDetailSearch.Count > 0 ? true : false;
                        break;
                }
            }
            return IsEnableExportButton;
        }
        public void CommandGenerateAccessCasResultExecuted(object obj)
        {
            try
            {
                DataTable dtExportResult = GetDataFromListForExporting();
                string fileName = dtExportResult.TableName.Replace(" ", "_") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
                AccessVersionControlSystem.Common.CommonFunctions.ExportAccessFile(dtExportResult, fileName, _objAccessVersion_Service);
                _notifier.ShowSuccess("Access File Export Successfully");
            }
            catch (Exception ex)
            {
                _notifier.ShowError(ex.Message);
            }
        }
        public void CommandGenerateExcelCasResultExecuted(object obj)
        {
            try
            {
                DataTable dtExportResult = GetDataFromListForExporting();
                string fileName = dtExportResult.TableName.Replace(" ", "_") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
                AccessVersionControlSystem.Common.CommonFunctions.ExportExcelFile(dtExportResult, Engine.CommonAccessExportFolderPath + fileName);
                _notifier.ShowSuccess("Excel File Export Successfully");
            }
            catch (Exception ex)
            {
                _notifier.ShowError(ex.Message);

            }
        }

        private DataTable GetDataFromListForExporting()
        {
            DataTable dtResult = new DataTable();
            if (SelectedTabItem != null)
            {
                switch (SelectedTabItem.Header)
                {
                    case "Cas Search":
                        var listCas = ListCasSearch.Select(x => new { Qsid = string.Join(", ", x.Qsid.Select(y => y.DisplayValue).ToArray()), Cas = x.Cas.CasFormatted, IdentifierValue = x.IdentifierValue_SubstanceName, IdentifierCategory = x.Type, SubstanceName_Language = x.SubstanceName_Language, ListCount = x.ListCount }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listCas);
                        break;
                    case "Substance Name Search":
                        var listSubstanceName = ListSubstanceNameSearch.Select(x => new { Qsid = string.Join(", ", x.Qsid.Select(y => y.DisplayValue).ToArray()), Cas = x.Cas, Type = x.Type, IdentifierValue_SubstanceName = x.IdentifierValue_SubstanceName, SubstanceName_Language = x.SubstanceName_Language }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listSubstanceName);
                        break;
                    case "Cas History":
                        var listCasHistory = ListCasHistory.Select(x => new { Cas = x.Cas, Status = x.Status, TimeStamp = x.TimeStamp, UserName = x.UserName, SesionId = x.SessionId }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listCasHistory);
                        break;
                    case "Cas Variant":
                        var listCasVarient = ListCasVariantSearch.Select(x => new { Qsid = x.Qsid, Cas = x.Cas }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listCasVarient);
                        break;
                    case "Same Name Multiple Cas":
                        if (SelectedMultipleCasTabItem.Header.ToString() == "Show Un-Accepted")
                        {
                            var listMultipleCas = ListMultipleCasSearch.Select(x => new { Qsid = x.Qsid, Cas = x.Cas, ChemicalName = x.ChemicalName, ChemicalNameSquash = x.ChemicalNameSquash, Frequency = x.Frequency, Accept = x.Accepted }).ToList();
                            dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listMultipleCas);
                        }
                        else if (SelectedMultipleCasTabItem.Header.ToString() == "Review Accepted Pair")
                        {
                            var listMultipleCas = ListMultipleCasSearch_Review.Select(x => new { Qsid = x.Qsid, Cas = x.Cas, ChemicalName = x.ChemicalName, ChemicalNameSquash = x.ChemicalNameSquash, Frequency = x.Frequency, Accept = x.Accepted }).ToList();
                            dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listMultipleCas);
                        }

                        break;
                    case "Identifier Search":

                        var listIdentSearch = ListIdentifierDetail.Select(x => new { Qsid = x.Qsid, Type = x.IdentifierType, IdentifierValue = x.IdentifierValue }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listIdentSearch);
                        break;
                    case "Phrase Search":

                        var listPhraseSearch = ListPhraseDetailSearch.Select(x => new { Qsid = x.Qsid, PhraseCategory = x.PhraseCategory, Phrase = x.Phrase }).ToList();
                        dtResult = AccessVersionControlSystem.Common.CommonFunctions.ConvertToDataTable(listPhraseSearch);
                        break;
                }
                dtResult.TableName = SelectedTabItem.Header.ToString();
            }
            return dtResult;
        }

        private bool CommandIdentifierSearchsCanExecute(object o)
        {
            if (String.IsNullOrEmpty(txtSearchIdentifier) && SelectedIdentType == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandIdentifierSearchExecute(object o)
        {
            BindIdentifierSearch();
        }
        private void BindIdentifierSearch()
        {
            VisibilityIdentifierSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityIdentifierSearch_Loader");
            Task.Run(() => FetchListIdentifierSearch());
        }
        private void FetchListIdentifierSearch()
        {
            string productIds = string.Join(",", listProductsIdentifierSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            int? identifierTypeId = SelectedIdentType != null ? SelectedIdentType.IdentifierTypeID : (int?)null;
            ListIdentifierDetail = _objAccessVersion_Service.GetListIdentifierDetail(identifierTypeId, txtSearchIdentifier, SelectedSearchTypeForIdentifer.Text, productIds);
            NotifyPropertyChanged("ListIdentifierDetail");
            VisibilityIdentifierSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityIdentifierSearch_Loader");
        }
        private bool CommandPhraseSearchCanExecute(object o)
        {
            if (String.IsNullOrEmpty(txtSearchPhrase) && SelectedPhraseCategory == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandPhraseSearchExecute(object o)
        {
            BindPhraseSearch();
        }
        private void BindPhraseSearch()
        {
            VisibilityPhraseSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityPhraseSearch_Loader");
            Task.Run(() => FetchListPhraseSearch());
        }
        private void FetchListPhraseSearch()
        {
            string productIds = string.Join(",", listProductsPhraseSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            int? phraseCateId = SelectedPhraseCategory != null ? SelectedPhraseCategory.PhraseCategoryID : (int?)null;
            ListPhraseDetailSearch = _objAccessVersion_Service.GetListPhraseDetail(phraseCateId, txtSearchPhrase, SelectedSearchTypeForPhrase.Text, productIds);
            NotifyPropertyChanged("ListPhraseDetailSearch");
            VisibilityPhraseSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityPhraseSearch_Loader");
        }

        private bool CommandCasSearchCanExecute(object o)
        {
            if (String.IsNullOrEmpty(CasSearch))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandCasSearchExecute(object o)
        {
            SearchCas();
        }

        private void CommandDateSearchExecute(object o)
        {
            //if(Date_From == null || Date_To == null)
            //{
            //    _notifier.ShowError("Choose DateFrom and DateTo to Continue");
            //    return;
            //}
            VisibilityDateSearch_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityDateSearch_Loader");
            Task.Run(() => FetchDateSearch());
            
        }

        private void FetchDateSearch()
        {

            ListDateSearch= new ObservableCollection<DateSearchTab_VM>(_objAccessVersion_Service.GetDateSearch(Date_From == null ? null : Date_From.Value, Date_To == null ? null : Date_To.Value));
            MessengerInstance.Send<PropertyChangedMessage<List<DateSearchTab_VM>>>(new PropertyChangedMessage<List<DateSearchTab_VM>>(ListDateSearch.ToList(), ListDateSearch.ToList(), "Default List"));
            NotifyPropertyChanged("ListDateSearch");
            VisibilityDateSearch_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityDateSearch_Loader");
        }
        private void SearchCas()
        {

            VisibilityCasSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityCasSearch_Loader");
            Task.Run(() => FetchListCas());
        }

        private void FetchListCas()
        {
            string productIds = string.Join(",", listProductsCasSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            if (SelectedCasSearch == "Search By CAS")
            {
                var casName = CasSearch.Replace("-", "").Replace(" ", "").Trim();
                ListCasSearch = new ObservableCollection<CasSearchSubstanceTab_VM>(_objAccessVersion_Service.GetCASDataSubstanceSearch(productIds, casName, SelectedSearchTypeForCas.Text));
            }
            else
            {
                using (var context = new CRAModel())
                {
                    var casid = Convert.ToInt32(CasSearch.Replace("-", "").Replace(" ", "").Trim());
                    var casName = context.Library_CAS.AsNoTracking().Where(x => x.CASID == casid).Select(y => y.CAS).FirstOrDefault();
                    if (casName != null)
                    {
                        ListCasSearch = new ObservableCollection<CasSearchSubstanceTab_VM>(_objAccessVersion_Service.GetCASDataSubstanceSearch(productIds, casName, SelectedSearchTypeForCas.Text));
                    }
                }
            }
            MessengerInstance.Send<PropertyChangedMessage<List<CasSearchSubstanceTab_VM>>>(new PropertyChangedMessage<List<CasSearchSubstanceTab_VM>>(ListCasSearch.ToList(), ListCasSearch.ToList(), "Default List"));
            NotifyPropertyChanged("ListCasSearch");
            VisibilityCasSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityCasSearch_Loader");
        }

        private bool CommandSubstanceNameSearchCanExecute(object o)
        {
            if (String.IsNullOrEmpty(chemicalName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandSubstanceNameSearchExecute(object o)
        {
            VisibilitySubstanceNameSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilitySubstanceNameSearch_Loader");
            Task.Run(() => FetchSubstanceNameSearch());
        }
        private void FetchSubstanceNameSearch()
        {
            string productIds = string.Join(",", listProductsSubstanceSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            ListSubstanceNameSearch = _objAccessVersion_Service.GetChemDataSubstanceSearch(productIds, chemicalName, SelectedSearchType.Text);
            MessengerInstance.Send<PropertyChangedMessage<List<SubstanceSearchTab_VM>>>(new PropertyChangedMessage<List<SubstanceSearchTab_VM>>(null, ListSubstanceNameSearch, "Default List"));
            NotifyPropertyChanged("ListSubstanceNameSearch");
            VisibilitySubstanceNameSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilitySubstanceNameSearch_Loader");
        }
        private bool CommandFieldNameSearchCanExecuted(object obj)
        {
            bool flag = false;
            if (SelectedFieldNameType.Text == "All Field Name") {
                flag = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(FieldNameSearch) && SelectedSearchTypeForFieldName != null && SelectedFieldNameType != null)
                {
                    flag = true;
                }
            }
            return flag;
        }

        private void CommandFieldNameSearchExecuted(object obj)
        {
            VisibilityFieldNameSearch_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityFieldNameSearch_Loader");
            Task.Run(() =>
            {
                FetchFieldNameSearch();
            });
        }
        private void FetchFieldNameSearch()
        {
            string productIds = string.Join(",", listProductsFieldNameSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            ListFieldNameSearch = _objAccessVersion_Service.SearchByFieldName(FieldNameSearch,SelectedFieldNameType.Text,SelectedSearchTypeForFieldName.Text, productIds);
            VisibilityFieldNameSearch_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityFieldNameSearch_Loader");
        }
        private bool CommandCasHistoryCanExecute(object o)
        {
            if (String.IsNullOrEmpty(SearchQsid))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandCasHistoryExecute(object o)
        {
            VisibilityCasHistorySearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityCasHistorySearch_Loader");
            Task.Run(() => FetchCasHistory());
        }

        private void FetchCasHistory()
        {
            ListCasHistory = new ObservableCollection<CasHistorySubstanceSearch>(_objLibraryFunction_Service.GetCASHistorySubstanceSearch(SearchQsid));
            NotifyPropertyChanged("ListCasHistory");
            VisibilityCasHistorySearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityCasHistorySearch_Loader");
        }
        private bool CommandCasVariantCanExecute(object o)
        {
            if (String.IsNullOrEmpty(casVariant))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandCasVariantExecute(object o)
        {
            BindCasVarientSearch();

        }
        private void BindCasVarientSearch()
        {
            VisibilityCasVarientSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityCasVarientSearch_Loader");
            Task.Run(() => FetchCasVarient());
        }
        private void FetchCasVarient()
        {
            //ListCasVariantSearch = new ObservableCollection<SubstanceSearchCasVariant_VM>(_objLibraryFunction_Service.GetCASVariantSubstanceSearch(casVariant).Select(x => new SubstanceSearchCasVariant_VM { Cas = x.Cas, Qsid = x.Qsid }).ToList());
            NotifyPropertyChanged("ListCasVariantSearch");
            VisibilityCasVarientSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityCasVarientSearch_Loader");
        }
        private bool CommandMultipleCasCanExecute(object o)
        {
            return true;
        }

        private void CommandMultipleCasExecute(object o)
        {
            VisibilityMultipleCasSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityMultipleCasSearch_Loader");
            Task.Run(() => FetchMultipleCas());

        }
        private void FetchMultipleCas()
        {
            string productIds = string.Join(",", listProductsSubstanceSearch.Where(x => x.IsSelected).Select(x => x.ProductID).ToArray());
            ListMultipleCasSearch = _objLibraryFunction_Service.GetSameNameMultipleCAS(productIds);
            NotifyPropertyChanged("ListMultipleCasSearch");
            ListMultipleCasSearch_Review = _objLibraryFunction_Service.GetSameNameMultipleCAS_Review(productIds);
            NotifyPropertyChanged("ListMultipleCasSearch_Review");
            VisibilityMultipleCasSearch_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityMultipleCasSearch_Loader");
        }

        private void BindComboListIdentifier()
        {
            ListIdentCategories = new ObservableCollection<Library_IdentifierCategories>(_objLibraryFunction_Service.GetLibrary_IdentifierCatList());
            NotifyPropertyChanged("ListIdentCategories");
        }
        private void BindComboListPhraseCategories()
        {
            ListPhraseCategories = new ObservableCollection<Library_PhraseCategories>(_objLibraryFunction_Service.GetLibrary_PhraseCategoriesList());
            NotifyPropertyChanged("ListPhraseCategories");
        }
        public void GetRefreshListDetailCommonControl()
        {
            dcDateSearch_VM = new CommonDataGrid_ViewModel<DateSearchTab_VM>(GetListDetailGridColDateSearch(), "Qsid", "Date Search Result :");
            NotifyPropertyChanged("dcDateSearch_VM");
            dcCasSearch_VM = new CommonDataGrid_ViewModel<CasSearchSubstanceTab_VM>(GetListDetailGridCol(), "Qsid", "Cas Search Result :");
            NotifyPropertyChanged("dcCasSearch_VM");
            dcSubstanceSearch_VM = new CommonDataGrid_ViewModel<SubstanceSearchTab_VM>(GetSubstanceSearchGridCol(), "Qsid", "Substance Search Result :");
            NotifyPropertyChanged("dcSubstanceSearch_VM");
            dcSubstanceCasHistorySearch_VM = new CommonDataGrid_ViewModel<CasHistorySubstanceSearch>(GetCasHistorySearchGridCol(), "Cas", "Cas Search Result :");
            NotifyPropertyChanged("dcSubstanceCasHistorySearch_VM");
            dcSubstanceCasVarientSearch_VM = new CommonDataGrid_ViewModel<SubstanceSearchCasVariant_VM>(GetCasVariantSearchGridCol(), "Qsid", "Cas Variant Search Result :");
            NotifyPropertyChanged("dcSubstanceCasVarientSearch_VM");
            dcSubstanceIdentifierSearch_VM = new CommonDataGrid_ViewModel<SubstanceSearchIdentifier_VM>(GetIdentifierSearchGridCol(), "Qsid", "Identifier Search Result :");
            NotifyPropertyChanged("dcSubstanceCasVarientSearch_VM");
            dcSubstancePhraseSearch_VM = new CommonDataGrid_ViewModel<SubstanceSearchPhrase_VM>(GetPhraseSearchGridCol(), "Qsid", "Phrase Search Result :");
            NotifyPropertyChanged("dcSubstancePhraseSearch_VM");
            dcSameNameUnAccepted = new CommonDataGrid_ViewModel<SameNameMultipleCas_VM>(GetSameNameUnAcceptedCol(), "Qsid", "Same Name Multiple Cas Un-Accepted :");
            NotifyPropertyChanged("dcSameNameUnAccepted");
            dcSameNameAcceptedReview = new CommonDataGrid_ViewModel<SameNameMultipleCas_AcceptedReview_VM>(GetSameNameAcceptedReviewCol(), "Qsid", "Same Name Multiple Cas Accepted :");
            NotifyPropertyChanged("dcSameNameAcceptedReview");
            dcFieldNameSearch_VM = new CommonDataGrid_ViewModel<FieldNameSearch_VM>(GetFieldNameSearchCol(), "Qsid", "Field Name Search :");
            NotifyPropertyChanged("dcFieldNameSearch_VM");
        }

        private List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> GetListGridColumn_Flat()
        {
            List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> listColumnFlatGenerics = new List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn>();
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });           
            return listColumnFlatGenerics;
        }
        private List<CommonDataGridColumn> GetSameNameUnAcceptedCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Chemical Name", ColBindingName = "ChemicalName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas.CasFormatted", ColType = "Textbox", ColFilterMemberPath = "Cas.CasOrignal" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Frequency", ColBindingName = "Frequency", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Accepted", ColBindingName = "Accepted", ColType = "CheckboxEnable" });
            return listColumnQsidDetail;
        }

        private List<CommonDataGridColumn> GetSameNameAcceptedReviewCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Chemical Name", ColBindingName = "ChemicalName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas.CasFormatted", ColType = "Textbox", ColFilterMemberPath = "Cas.CasOrignal" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Accepted", ColBindingName = "Accepted", ColType = "CheckboxEnable" });
            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetFieldNameSearchCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox", });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Report Phrase", ColBindingName = "FieldReportPhrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product Name", ColBindingName = "ProductName", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetListDetailGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas.CasFormatted", ColType = "Textbox", ColFilterMemberPath = "Cas.CasOrignal" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Value", ColBindingName = "IdentifierValue_SubstanceName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Category", ColBindingName = "Type", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceName Language", ColBindingName = "SubstanceName_Language", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Count", ColBindingName = "ListCount", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetListDetailGridColDateSearch()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date on List", ColBindingName = "Phrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Harmonized Date", ColBindingName = "Harmonized_Date", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_ListCount", ColBindingName = "No_of_ListCount", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetSubstanceSearchGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Value SubstanceName", ColBindingName = "IdentifierValue_SubstanceName", ColType = "Textbox" ,ColumnWidth="250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceName Language", ColBindingName = "SubstanceName_Language", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Count", ColBindingName = "ListCount", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Substance Length", ColBindingName = "SubstanceLength", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Accepted Pair", ColBindingName = "SubstanceLength", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Substance Type", ColBindingName = "SubstanceType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Substance Category", ColBindingName = "SubstanceCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No. Flats Children", ColType = "Button", ColBindingName = "ChildCount",ContentName= "None", ColFilterMemberPath = "ChildCount" ,CustomStyleClass = "CustomFlatGenericChild", CommandParam = "ViewFlatGenericsDetail" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Qsid", ColType = "Button", ColumnWidth = "150", ContentName = "View Substance Identity", BackgroundColor = "#931a25", CommandParam = "ViewSubstanceIdentity" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Qsid", ColType = "Button", ColumnWidth = "150", ContentName = "View Substance On Generic Tree", BackgroundColor = "#194350", CommandParam = "ViewSubstanceGenericTree" });
            return listColumnQsidDetail;
        }

        public List<CommonDataGridColumn> GetCasHistorySearchGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas.CasFormatted", ColType = "Textbox", ColFilterMemberPath = "Cas.CasOrignal" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TimeStamp", ColBindingName = "TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "UserName", ColBindingName = "UserName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SessionId", ColBindingName = "SessionId", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetCasVariantSearchGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas.CasFormatted", ColType = "Textbox", ColFilterMemberPath = "Cas.CasOrignal" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetIdentifierSearchGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Substance Name", ColBindingName = "SubstanceName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Type", ColBindingName = "IdentifierType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Value", ColBindingName = "IdentifierValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Count", ColBindingName = "ListCount", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetPhraseSearchGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();

            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Substance Name", ColBindingName = "SubstanceName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Category", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Count", ColBindingName = "ListCount", ColType = "Textbox" });
            return listColumnQsidDetail;
        }


        public void CallBackRefreshCasSeacrhExecute(NotificationMessageAction<CasSearchSubstanceTab_VM> notificationMessageAction)
        {
            Task.Run(() => SearchCas());
        }
        public void CallBackRefreshSubstanceSeacrhExecute(NotificationMessageAction<SubstanceSearchTab_VM> notificationMessageAction)
        {
            VisibilitySubstanceNameSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilitySubstanceNameSearch_Loader");
            Task.Run(() => FetchSubstanceNameSearch());
        }
        public void CallBackRefreshIdentifierSeacrhExecute(NotificationMessageAction<SubstanceSearchIdentifier_VM> notificationMessageAction)
        {
            VisibilityIdentifierSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityIdentifierSearch_Loader");
            Task.Run(() => FetchListIdentifierSearch());
        }
        public void CallBackRefreshPhraseSeacrhExecute(NotificationMessageAction<SubstanceSearchPhrase_VM> notificationMessageAction)
        {
            VisibilityPhraseSearch_Loader = "Visible";
            NotifyPropertyChanged("VisibilityPhraseSearch_Loader");
            Task.Run(() => FetchListPhraseSearch());
        }

    }
}
