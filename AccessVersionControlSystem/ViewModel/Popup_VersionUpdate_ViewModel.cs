﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using CommonGenericUIView.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class Popup_VersionUpdate_ViewModel : Base_ViewModel
    {
        #region Property Member
        private CommonFunctions objCommonFunc;
        private string stringSqlCon;
        public ObservableCollection<IRelationShip> ListRelationshipName { get; set; }
        public PopUp_UpdateData_Version _popWindow { get; set; }
        public Visibility visibilityAddNote { get; set; }
        public Visibility visibilityParentChildQsid { get; set; }
        public Visibility visibilityQsidlst { get; set; }
        public Visibility visibilityAddProductTimestamp { get; set; }
        public Visibility visibilityTypeOfChange { get; set; }
        public Visibility WorkOrderNumberVisibility { get; set; }


        public Visibility ddlChangeVisibility_Combo { get; set; }
        public Visibility ddlChangeVisibility_List { get; set; }
        
        public string NoteDesc { get; set; }
        public DateTime? _ProductDateTimestamp { get; set; }

        public DateTime? ProductDateTimestamp
        {
            get { return _ProductDateTimestamp; }
            set
            {
                if (value != _ProductDateTimestamp)
                {
                    _ProductDateTimestamp = value;
                    NotifyPropertyChanged("ProductDateTimestamp");
                }

            }
        }

        public DateTime? _ProductDateTimestampForTime { get; set; }

        public DateTime? ProductDateTimestampForTime
        {
            get { return _ProductDateTimestampForTime; }
            set
            {
                if (value != _ProductDateTimestampForTime)
                {
                    _ProductDateTimestampForTime = value;
                    NotifyPropertyChanged("ProductDateTimestampForTime");
                }

            }
        }
        public DateTime? ProductDateStart { get; set; }
        public DateTime? ProductDateEnd { get; set; }
        private bool _CheckedNoWorkOrder { get; set; }
        public bool IsCheckedNoWorkOrder
        {
            get
            {
                return _CheckedNoWorkOrder;
            }
            set
            {
                if (value != _CheckedNoWorkOrder)
                {
                    _CheckedNoWorkOrder = value;
                    if (_CheckedNoWorkOrder)
                    {
                        WorkOrderNumberVisibility = Visibility.Collapsed;
                        //  WorkOrderNumber = (int?)null;
                        // NotifyPropertyChanged("WorkOrderNumber");
                    }
                    else
                    {
                        WorkOrderNumberVisibility = Visibility.Visible;
                    }
                    NotifyPropertyChanged("WorkOrderNumberVisibility");
                }

            }
        }

        public bool IsSelectedTypeChange { get; set; }
        public bool IsSelectedProductDate { get; set; }
        public bool IsSelectedNote { get; set; }
        public bool IsSelectedParentChildQsid { get; set; }
        public bool IsSelectedQsidlst { get; set; }

        private string _qsidSearch { get; set; }
        public string QsidSearch
        {
            get { return _qsidSearch; }
            set
            {
                if (value != _qsidSearch)
                {
                    _qsidSearch = value;
                    if (!string.IsNullOrEmpty(_qsidSearch))
                    {
                        Task.Run(() => BindDDLQsidList(_qsidSearch));
                    }

                }

            }
        }
        private string _QsidSearchlst { get; set; }
        public string QsidSearchlst
        {
            get { return _QsidSearchlst; }
            set
            {
                if (value != _QsidSearchlst)
                {
                    _QsidSearchlst = value;
                    if (!string.IsNullOrEmpty(_QsidSearchlst))
                    {
                        Task.Run(() => BindDDLQsidListRel(_QsidSearchlst));
                    }

                }

            }
        }
        public string _popUpFlag { get; set; }
        public Visibility visibilityTextBoxWO { get; set; }
        //public List<TypeChangeHeader_VM> listTypeHeader { get; set; }
        public List<EntityClass.Library_ListVersionChangeTypes> listTypeHeader { get; set; }
        public ObservableCollection<libQsid> listLibQsid { get; set; }
        public ObservableCollection<libQsid> listLibQsidlst { get; set; }
        private EntityClass.Library_ListVersionChangeTypes _SelectedTypeHeader { get; set; }
        public EntityClass.Library_ListVersionChangeTypes SelectedTypeHeader
        {
            get => _SelectedTypeHeader;
            set
            {
                if (Equals(_SelectedTypeHeader, value))
                {
                    return;
                }

                _SelectedTypeHeader = value;
                BindTypeChangeList();
            }
        }
        public ObservableCollection<ListEditorialChangeTypes_VM> listTypeChange { get; set; }
        private ListEditorialChangeTypes_VM _SelectedTypeChange { get; set; }
        public ListEditorialChangeTypes_VM SelectedTypeChange
        {
            get => _SelectedTypeChange;
            set
            {
                if (Equals(_SelectedTypeChange, value))
                {
                    return;
                }

                _SelectedTypeChange = value;
            }
        }

        private libQsid _SelectedParentQsid { get; set; }
        public libQsid SelectedParentQsid
        {
            get => _SelectedParentQsid;
            set
            {
                if (Equals(_SelectedParentQsid, value))
                {
                    return;
                }

                _SelectedParentQsid = value;
            }
        }

        private libQsid _SelectedQsidlst { get; set; }
        public libQsid SelectedQsidlst
        {
            get => _SelectedQsidlst;
            set
            {
                if (Equals(_SelectedQsidlst, value))
                {
                    return;
                }

                _SelectedQsidlst = value;
            }
        }
        public ObservableCollection<Log_VersionControl_WorkOrders_VM> listWorkOrder { get; set; } = new ObservableCollection<Log_VersionControl_WorkOrders_VM>();
        public ObservableCollection<Log_VersionControl_Tickets_VM> listTickets { get; set; } = new ObservableCollection<Log_VersionControl_Tickets_VM>();
        public ObservableCollection<Log_Map_EasiList_Qsid_VM> listEasiProList { get; set; } = new ObservableCollection<Log_Map_EasiList_Qsid_VM>();
        public ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM> listERC_EHSList { get; set; } = new ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM>();

        public List<TypeChange_VM> listChange_Combo { get; set; }
        public ICollectionView SelectedItems { get; set; }

        public ObservableCollection<MapProductListVM> listProductsToMap { get; set; }
        public ObservableCollection<MapParentChildVM> listParentChildQsid { get; set; }
        public ObservableCollection<MapFieldListRelationVM> listQsidlst { get; set; }
        public ObservableCollection<MapVerticalListVM> listVerticalsToMap { get; set; }



        public bool IsEnableNote { get; set; }
        public bool IsEnableProductTimestamp { get; set; }
        public Access_QSID_Data_VM _Select_Access_QSID_Data_VM { get; set; }
        public ICommand CommandUpdate { get; private set; }
        public ICommand CommandCancel { get; private set; }
        public ICommand ClearProductTimeStamp { get; private set; }

        public ICommand CommandAddWorkOrder { get; private set; }
        public ICommand CommandDeleteWorkOrder { get; private set; }
        public ICommand CommandAddEasiProListId { get; private set; }
        public ICommand CommandDeleteEasiProListId { get; private set; }
        public ICommand CommandAddERC_EHSId { get; private set; }
        public ICommand CommandAddNewParentQsid { get; private set; }
        public ICommand CommandAddNewQsidlst { get; private set; }
        public ICommand CommandDeleteERC_EHSListId { get; private set; }

        public ICommand CommandAddTicketNumber { get; private set; }
        public ICommand CommandDeleteTicketNumber { get; private set; }

        public Visibility parentQsidVisibility { get; set; }
        
        public string TimespentValue { get; set; }
        public ObservableCollection<SelectListItem> listTimeSpentUnits { get; set; } = new();
        public SelectListItem TimespentUnit { get; set; }

        public CommonDataGrid_ViewModel<QcChecksDetail_VM> commonDGQcCheck { get; set; }
        public QcCheckUC_VM qcCheckUC_Context { get; set; }

        #endregion

        #region Member Function
        public Popup_VersionUpdate_ViewModel(PopUp_UpdateData_Version popWindow, Access_QSID_Data_VM SelectedQSID_Version, string popUpFlag, bool isEnable)
        {
            objCommonFunc = new CommonFunctions();
            stringSqlCon = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            commonDGQcCheck = new CommonDataGrid_ViewModel<QcChecksDetail_VM>(GetColumnForQcCheck(), "QcCheckID", "List Qc Checks");
            _popUpFlag = popUpFlag;
            _popWindow = popWindow;
            IsEnableNote = isEnable;
            ProductDateTimestamp = SelectedQSID_Version.ProductTimeStamp;
            ProductDateTimestampForTime = SelectedQSID_Version.ProductTimeStamp;
            NoteDesc = SelectedQSID_Version.AddNote;
            using (var context = new CRAModel())
            {
                var resultDelimiter = context.Library_ListRelationships.AsNoTracking().Select(x => new IRelationShip { ID = x.ListRelationshipID, ListRelationship_Name = x.ListRelationship_Name }).ToList();
                ListRelationshipName = new ObservableCollection<IRelationShip>(resultDelimiter);
            }
            _Select_Access_QSID_Data_VM = SelectedQSID_Version;
            IsEnableProductTimestamp = isEnable;
            BindQsidList();
            BindTypeOfChange();
            BindTypeHeader();
            BindTypeChangeList();
            ExistingWorkOrderBinding();
            BindWorkOrderList();
            ExistingTicketBinding();
            BindTicketList();
            ExistingEasiproListBinding();
            ExistingERC_EHSListCodeBinding();
            BindEasiProListID();
            BindERC_EHSListID();
            BindQcCheckList();
            BindTimeSpentUnit();
            InitModel(isEnable);
           // MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(QcChecksDetail_VM), CallbackButtonCommand);
        }

        //private void CallbackButtonCommand(NotificationMessageAction<object> obj)
        //{
        //    if (obj.Notification == "CommandIsNotApplicable")
        //    { 
            
        //    }
        //}

        private List<CommonDataGridColumn> GetColumnForQcCheck()
        {
            List<CommonDataGridColumn> listColumn = new();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qc Checks", ColBindingName = "QcCheckName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qc Checks Description", ColBindingName = "QcCheckDescription", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Completed", ColBindingName = "IsCompleted", ColType = "ToggleButton", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsNotApplicable", ColBindingName = "IsNotApplicable", CommandParam = "CommandIsNotApplicable", ColType = "CheckboxEnable", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "User Name", ColBindingName = "UserName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Added", ColBindingName = "DateAdded", ColType = "Textbox", ColumnWidth = "200" });
            return listColumn;
        }

        List<libQsid> allFalseQsids = new List<libQsid>();
        List<libQsid> allFalseQsidsExcludeSelected = new List<libQsid>();
        public List<MapFieldListRelationVM> GetAllLibraryQsidRelationship(int qsid_ID)
        {
            string query = " select 'true' as IsSelected, Qsid, b.RelatedList_QSID_ID as Qsid_ID, ListRelationShipID " +
             " from Library_Qsids a, Map_ListRelationships b where " +
             " a.QSID_ID = b.RelatedList_QSID_ID and b.ParentList_QSID_ID = " + qsid_ID +
             " Union " +
             " Select 'false' as IsSelected, " +
             " Qsid, Qsid_ID,0 as ListRelationShipID from Library_Qsids a  where QSID_ID != " + qsid_ID + " and IsActive = 1 " +
             " and QSID_ID not in (select RelatedList_QSID_ID from Map_ListRelationships where ParentList_QSID_ID =" + qsid_ID + ") ";

            return objCommonFunc.DbaseQueryReturnSqlList<MapFieldListRelationVM>(query, stringSqlCon);
        }

        public void BindQcCheckList() {
            if (_popUpFlag == "AddTypeOfChange")
            {
                qcCheckUC_Context = new QcCheckUC_VM(0,false);
            }
            else
            {
                qcCheckUC_Context = new QcCheckUC_VM(_Select_Access_QSID_Data_VM.VersionControl_ID,false);
            }
            NotifyPropertyChanged("qcCheckUC_Context");            
        }
        private void BindQsidList()
        {

            var allQsids = _objLibraryFunction_Service.GetAllLibraryQsid(_Select_Access_QSID_Data_VM.QSID_ID);
            var selQsids = allQsids.Where(y => y.IsSelected == true).ToList();
            allFalseQsids = allQsids.Where(y => y.IsSelected == false).ToList().Select(z => new libQsid { QSID_ID = z.Qsid_ID, QSID = z.Qsid }).ToList();
            listLibQsid = new ObservableCollection<libQsid>(allFalseQsids);
            listParentChildQsid = new ObservableCollection<MapParentChildVM>(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapParentChildToViewModel(selQsids));

            var allQsidsForRelation = GetAllLibraryQsidRelationship(_Select_Access_QSID_Data_VM.QSID_ID);
            var selQsidslst = allQsidsForRelation.Where(y => y.IsSelected == true).ToList();
            allFalseQsidsExcludeSelected = allQsidsForRelation.Where(y => y.IsSelected == false).ToList().Select(z => new libQsid { QSID_ID = z.Qsid_ID, QSID = z.Qsid }).ToList();

            listLibQsidlst = new ObservableCollection<libQsid>(allFalseQsidsExcludeSelected);
            listQsidlst = new ObservableCollection<MapFieldListRelationVM>(selQsidslst.Select(y => new MapFieldListRelationVM
            {
                IsSelected = y.IsSelected,
                Qsid = y.Qsid,
                Qsid_ID = y.Qsid_ID,
                SelectedRelation = ListRelationshipName.FirstOrDefault(x => x.ID == y.ListRelationshipID)

            }));

        }

        public void BindDDLQsidList(string qsid)
        {
            visibilityParentChildQsid = Visibility.Visible;
            NotifyPropertyChanged("parentQsidVisibility");
            if (!string.IsNullOrEmpty(qsid))
            {
                listLibQsid = new ObservableCollection<libQsid>(allFalseQsids.FindAll(x => x.QSID.ToLower().Contains(qsid.ToLower())));
                NotifyPropertyChanged("listLibQsid");
            }
        }

        public void BindDDLQsidListRel(string qsid)
        {
            visibilityQsidlst = Visibility.Visible;
            NotifyPropertyChanged("visibilityQsidlst");
            if (!string.IsNullOrEmpty(qsid))
            {
                listLibQsidlst = new ObservableCollection<libQsid>(allFalseQsidsExcludeSelected.FindAll(x => x.QSID.ToLower().Contains(qsid.ToLower())));
                NotifyPropertyChanged("listLibQsidlst");
            }
        }
        private void InitModel(bool isEnable)
        {
            CheckVisibiltyPopUpContent(isEnable);
            SetProductDateStartEnd();
            listProductsToMap = new ObservableCollection<MapProductListVM>(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapProductToViewModel(_objLibraryFunction_Service.GetAllLibraryProductLists(_Select_Access_QSID_Data_VM.QSID_ID)));
            listVerticalsToMap = new ObservableCollection<MapVerticalListVM>(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapVerticalToViewModel(_objLibraryFunction_Service.GetAllLibraryVerticalLists(_Select_Access_QSID_Data_VM.QSID_ID)));
            CommandUpdate = new RelayCommand(CommandUpdateVersionDataExecute, CommandUpdateVersionDataCanExecute);
            CommandCancel = new RelayCommand(CommandCancelExecute, CommandCancelCanExecute);
            ClearProductTimeStamp = new RelayCommand(ClearProductTimeStampExecute, CommandCancelCanExecute);
            CommandAddWorkOrder = new RelayCommand(CommandAddWorkOrderExecute, CommandAddWorkOrderCanExecute);
            CommandDeleteWorkOrder = new RelayCommand(CommandDeleteWorkOrderExecute, CommandDeleteWorkOrderCanExecute);
            CommandAddEasiProListId = new RelayCommand(CommandAddEasiProListIdExecute, CommandAddEasiProListIdCanExecute);
            CommandDeleteEasiProListId = new RelayCommand(CommandDeleteEasiProListIdExecute, CommandDeleteEasiProListIdCanExecute);
            CommandAddERC_EHSId = new RelayCommand(CommandAddERC_EHSListIdExecute, CommandAddEasiProListIdCanExecute);
            CommandAddNewParentQsid = new RelayCommand(CommandAddNewParentExecute, CommandAddEasiProListIdCanExecute);
            CommandAddNewQsidlst = new RelayCommand(CommandAddNewQsidlstExecute, CommandAddEasiProListIdCanExecute);
            CommandDeleteERC_EHSListId = new RelayCommand(CommandDeleteERC_EHSListIdExecute, CommandDeleteEasiProListIdCanExecute);

            CommandAddTicketNumber = new RelayCommand(CommandAddTicketsExecute);
            CommandDeleteTicketNumber = new RelayCommand(CommandDeleteTicketExecute);
        }
        private void CommandAddERC_EHSListIdExecute(object obj)
        {
            if (!listERC_EHSList.Any(x => string.IsNullOrEmpty(x.EHS_ListCode)))
            {
                if (!listERC_EHSList.GroupBy(x => x.EHS_ListCode).Any(grp => grp.Count() > 1))
                {
                    listERC_EHSList.Add(new Log_Map_QSID_ERC_EHSListCode_VM { EHS_ListCode = string.Empty });
                    BindERC_EHSListID();
                }
                else
                {
                    _notifier.ShowWarning("New ERC EHS ListID not added due existing ERC EHS ListID is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New ERC EHS ListID not added due existing ERC EHS ListID is empty or null!");
            }

        }

        private void CommandAddNewQsidlstExecute(object obj)
        {
            if (SelectedQsidlst != null)
            {
                if (listQsidlst.Count(x => x.Qsid_ID == SelectedQsidlst.QSID_ID) > 0)
                {
                    _notifier.ShowWarning("Already Selected");
                }
                else
                {
                    listQsidlst.Add(new MapFieldListRelationVM { IsSelected = true, Qsid = SelectedQsidlst.QSID, Qsid_ID = SelectedQsidlst.QSID_ID });
                    allFalseQsidsExcludeSelected.Remove(SelectedQsidlst);
                    listLibQsidlst.Remove(SelectedQsidlst);
                    NotifyPropertyChanged("listQsidlst");
                    NotifyPropertyChanged("listLibQsidlst");

                }
            }
        }
        private void CommandAddNewParentExecute(object obj)
        {
            if (SelectedParentQsid != null)
            {
                if (listParentChildQsid.Count(x => x.Qsid_ID == SelectedParentQsid.QSID_ID) > 0)
                {
                    _notifier.ShowWarning("Already Selected");
                }
                else
                {
                    listParentChildQsid.Add(new MapParentChildVM { IsSelected = true, Qsid = SelectedParentQsid.QSID, Qsid_ID = SelectedParentQsid.QSID_ID });
                    allFalseQsids.Remove(SelectedParentQsid);
                    listLibQsid.Remove(SelectedParentQsid);
                    NotifyPropertyChanged("listParentChildQsid");
                    NotifyPropertyChanged("listLibQsid");

                }
            }
        }
        private void CommandDeleteERC_EHSListIdExecute(object obj)
        {
            string ehsListCode = (string)obj;
            listERC_EHSList = new ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM>(listERC_EHSList.Where(x => x.EHS_ListCode != ehsListCode).ToList());
            BindERC_EHSListID();
        }

        private void CommandDeleteEasiProListIdExecute(object obj)
        {
            int? woNum = (int?)obj;
            listEasiProList = new ObservableCollection<Log_Map_EasiList_Qsid_VM>(listEasiProList.Where(x => x.EasiPro_ListID != woNum).ToList());
            BindEasiProListID();
        }

        private bool CommandDeleteEasiProListIdCanExecute(object obj)
        {
            return true;
        }

        private void CommandDeleteWorkOrderExecute(object obj)
        {
            int? woNum = (int?)obj;
            listWorkOrder = new ObservableCollection<Log_VersionControl_WorkOrders_VM>(listWorkOrder.Where(x => x.WO_Number != woNum).ToList());
            BindWorkOrderList();
        }
        private void CommandDeleteTicketExecute(object obj)
        {
            string tNum = (string)obj;
            listTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(listTickets.Where(x => x.Ticket_Number != tNum).ToList());
            BindTicketList();
        }

        private bool CommandDeleteWorkOrderCanExecute(object obj)
        {
            return true;
        }
        public void BindEasiProListID()
        {

            if (listEasiProList.Count == 0)
            {
                listEasiProList.Add(new Log_Map_EasiList_Qsid_VM { EasiPro_ListID = null, CreatedDate = DateTime.Now });
            }
            listEasiProList = new ObservableCollection<Log_Map_EasiList_Qsid_VM>(listEasiProList.OrderByDescending(x => x.CreatedDate));
            NotifyPropertyChanged("listEasiProList");

        }
        public void BindERC_EHSListID()
        {

            if (listERC_EHSList.Count == 0)
            {
                listERC_EHSList.Add(new Log_Map_QSID_ERC_EHSListCode_VM { EHS_ListCode = null, CreatedDate = DateTime.Now });
            }
            listERC_EHSList = new ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM>(listERC_EHSList.OrderByDescending(x => x.CreatedDate));
            NotifyPropertyChanged("listERC_EHSList");

        }
        private void CommandAddEasiProListIdExecute(object obj)
        {
            if (!listEasiProList.Any(x => x.EasiPro_ListID == null))
            {
                if (!listEasiProList.GroupBy(x => x.EasiPro_ListID).Any(grp => grp.Count() > 1))
                {
                    listEasiProList.Add(new Log_Map_EasiList_Qsid_VM { EasiPro_ListID = null, });
                    BindEasiProListID();
                }
                else
                {
                    _notifier.ShowWarning("New EasiPro ListID not added due existing EasiPro ListID is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New EasiPro ListID not added due existing EasiPro ListID is empty or null!");
            }

        }

        private bool CommandAddEasiProListIdCanExecute(object obj)
        {
            return true;
        }

        private bool CommandAddWorkOrderCanExecute(object obj)
        {
            return true;
        }

        private void CommandAddWorkOrderExecute(object obj)
        {
            if (!listWorkOrder.Any(x => x.WO_Number == null))
            {
                if (!listWorkOrder.GroupBy(x => x.WO_Number).Any(grp => grp.Count() > 1))
                {
                    listWorkOrder.Add(new Log_VersionControl_WorkOrders_VM { WO_Number = null, CreatedDate = DateTime.Now });
                    BindWorkOrderList();
                }
                else
                {
                    _notifier.ShowWarning("New work order not added due existing work orders is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New work order not added due existing work order is empty or null!");
            }
        }
        public void ExistingWorkOrderBinding()
        {
            if (_popUpFlag != "AddTypeOfChange")
            {
                listWorkOrder = new ObservableCollection<Log_VersionControl_WorkOrders_VM>(Common.CommonFunctions.ConvertEntityWorkOrderModelToViewModel(_objAccessVersion_Service.GetLog_VersionControl_WorkOrders(_Select_Access_QSID_Data_VM.VersionControl_ID)));
            }
        }
        public void ExistingEasiproListBinding()
        {
            listEasiProList = AccessVersionControlSystem.Common.CommonFunctions.ConvertMapEasiProListIdBusinessToVM(_objAccessVersion_Service.GetListEasiProListID(_Select_Access_QSID_Data_VM.QSID_ID));
        }
        public void ExistingERC_EHSListCodeBinding()
        {
            listERC_EHSList = AccessVersionControlSystem.Common.CommonFunctions.ConvertMapEHSListCodeBusinessToVM(_objAccessVersion_Service.GetListERC_EHSListCode(_Select_Access_QSID_Data_VM.QSID_ID));
        }

        public void BindWorkOrderList()
        {

            if (listWorkOrder.Count == 0)
            {
                listWorkOrder.Add(new Log_VersionControl_WorkOrders_VM { WO_Number = null, CreatedDate = DateTime.Now });
            }
            listWorkOrder = new ObservableCollection<Log_VersionControl_WorkOrders_VM>(listWorkOrder.OrderByDescending(x => x.CreatedDate));
            NotifyPropertyChanged("listWorkOrder");

        }

       

        private void CommandAddTicketsExecute(object obj)
        {
            if (!listTickets.Any(x => x.Ticket_Number == null))
            {
                if (!listTickets.GroupBy(x => x.Ticket_Number).Any(grp => grp.Count() > 1))
                {
                    listTickets.Add(new Log_VersionControl_Tickets_VM { Ticket_Number = null, CreatedDate = DateTime.Now });
                    BindTicketList();
                }
                else
                {
                    _notifier.ShowWarning("New tickets not added due existing work orders is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New work order not added due existing work order is empty or null!");
            }
        }
        public void ExistingTicketBinding()
        {
            if (_popUpFlag != "AddTypeOfChange")
            {
                listTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(Common.CommonFunctions.ConvertEntityTicketModelToViewModel(_objAccessVersion_Service.GetLog_VersionControl_Tickets(_Select_Access_QSID_Data_VM.VersionControl_ID)));
            }
        }

        public void BindTicketList()
        {

            if (listTickets.Count == 0)
            {
                listTickets.Add(new Log_VersionControl_Tickets_VM { Ticket_Number = null, CreatedDate = DateTime.Now });
            }
            listTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(listTickets.OrderByDescending(x => x.CreatedDate));
            NotifyPropertyChanged("listTickets");

        }

        private bool CommandUpdateVersionDataCanExecute(object o)
        {
            bool resultFlag = false;
            if (IsCheckedNoWorkOrder || ((listWorkOrder != null) && (listWorkOrder.Count > 0)))
            {
                resultFlag = true;
            }
            return resultFlag;
        }

        private void ClearProductTimeStampExecute(object o)
        {

            ProductDateTimestamp = (DateTime?)null;
            NotifyPropertyChanged("ProductDateTimestamp");
        }

        private void CommandUpdateVersionDataExecute(object o)
        {
            try
            {
                if (_popUpFlag == "AddTypeOfChange")
                {
                    if (SelectedTypeHeader == null)
                    {
                        _notifier.ShowError("Change Type Header is mandatory");
                        return;
                    }
                    if (!listProductsToMap.Any(x => x.IsSelected))
                    {
                        _notifier.ShowError("Product(s) is mandatory");
                        return;
                    }
                    else
                    {
                        if (listProductsToMap.Any(x => x.ProductInternalCode == "I4C" && x.IsSelected) && !listVerticalsToMap.Any(x => x.IsSelected))
                        {
                            _notifier.ShowError("For I4C, Industry Vertical Mandatory!");
                            return;
                        }
                    }

                    if (SelectedTypeHeader.ListVersionChangeType == "Deleted List" && NoteDesc == string.Empty)
                    {
                        _notifier.ShowError("Comments are mandatory while deleting the list");
                        return;
                    }

                    if (SelectedTypeHeader.ListVersionChangeType == "Deleted List")
                    {
                        var result = MessageBox.Show("Are you sure want to delete the selected QSID?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result != MessageBoxResult.Yes)
                        {
                            return;
                        }
                    }

                    var _CurrentQsidVersion = _objAccessVersion_Service.CheckIn_Functionality(_Select_Access_QSID_Data_VM.QSID_ID, ProductDateTimestamp, NoteDesc);
                    if (_CurrentQsidVersion == null)
                    {
                        _notifier.ShowError("CheckedIn Process failed due to Meta data misiing in List Dictionary table");
                    }
                    else
                    {
                        var listChildQsid = _objLibraryFunction_Service.GetChildQsids((int)_CurrentQsidVersion.QSID_ID);
                        if (listChildQsid.Count > 0)
                        {
                            _notifier.ShowWarning("The following lists are derived from this list, and may also need to be updated" + Environment.NewLine + string.Join(",", listChildQsid));
                            if (SelectedTypeHeader.ListVersionChangeType == "Deleted List")
                            {
                                return;
                            }
                        }
                        SaveMapProduct(_CurrentQsidVersion.QSID_ID);
                        SaveEaseProListId(_CurrentQsidVersion.QSID_ID);
                        SaveMapVerticals(_CurrentQsidVersion.QSID_ID);
                        SaveTypeChangeToDB(_CurrentQsidVersion.VersionControl_HistoryID);
                        SaveEHSListCode(_CurrentQsidVersion.QSID_ID);
                        SaveRelationShip(_Select_Access_QSID_Data_VM.QSID_ID);
                        UpdateVersionControl_QcChecks(_CurrentQsidVersion.VersionControl_HistoryID);
                        SaveTimeSpentToDB(_CurrentQsidVersion.VersionControl_HistoryID);
                        if (SelectedTypeHeader.ListVersionChangeType == "Deleted List")
                        {
                            var chkExist = objCommonFunc.DbaseQueryReturnTableSql("Select top 1 qsid_id from Log_DeletedQSID_Comments where qsid_id =" + _CurrentQsidVersion.QSID_ID, stringSqlCon);
                            if (chkExist.Rows.Count > 0)
                            {
                                _notifier.ShowError("QSID already deleted, can't delete");
                                return;
                            }
                            else
                            {
                                var WorkOrderNo = listWorkOrder.Where(y => y.WO_Number != null).Select(x => x.WO_Number).FirstOrDefault().ToString();
                                _objLibraryFunction_Service.DeleteQsid((int)_CurrentQsidVersion.QSID_ID, Engine.CurrentUserSessionID, WorkOrderNo, NoteDesc);
                            }
                        }
                        if (_objAccessVersion_Service.CheckProductEchaByQsid((int)_CurrentQsidVersion.QSID_ID))
                        {
                            _notifier.ShowWarning("This list is associated with ECHA/EUCLEF, please be sure all necessary QC checks are completed");
                        }
                        _notifier.ShowSuccess("File Check In Successfully.");
                    }

                }
                else if (_popUpFlag == "AddTypeOfChange_FirstTime")
                {
                    SaveTypeChangeToDB(_Select_Access_QSID_Data_VM.VersionControl_ID);
                    _Select_Access_QSID_Data_VM.AddNote = NoteDesc;
                    SaveProductTimestamp();
                    SaveMapProduct(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveEaseProListId(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveMapVerticals(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveEHSListCode(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveRelationShip(_Select_Access_QSID_Data_VM.QSID_ID);
                    UpdateVersionControl_QcChecks(_Select_Access_QSID_Data_VM.VersionControl_ID);
                    SaveTimeSpentToDB(_Select_Access_QSID_Data_VM.VersionControl_ID);
                }
                else if (_popUpFlag == "UpdateTypeOfChange" || _popUpFlag == "AddProductTimestamp" || _popUpFlag == "AddNote")
                {
                    SaveTypeChangeToDB(_Select_Access_QSID_Data_VM.VersionControl_ID);
                    _Select_Access_QSID_Data_VM.AddNote = NoteDesc;
                    SaveProductTimestamp();
                    SaveMapProduct(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveEaseProListId(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveMapVerticals(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveEHSListCode(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveParentChildQsid(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveRelationShip(_Select_Access_QSID_Data_VM.QSID_ID);
                    UpdateVersionControl_QcChecks(_Select_Access_QSID_Data_VM.VersionControl_ID);
                    SaveTimeSpentToDB(_Select_Access_QSID_Data_VM.VersionControl_ID);
                    _notifier.ShowSuccess("Update version history data Successfully.");
                }
                else if (_popUpFlag == "AddParentChildQsid")
                {
                    SaveParentChildQsid(_Select_Access_QSID_Data_VM.QSID_ID);
                    SaveRelationShip(_Select_Access_QSID_Data_VM.QSID_ID);
                    _notifier.ShowSuccess("Update Parent Qsid Successfully");
                }
            }
            catch (Exception ex)
            {
                var logPath = _objLogUserSession_Service.ErrorLogging(ex);
                _notifier.ShowError("Internal Server Error:" + System.Environment.NewLine + ex.Message + System.Environment.NewLine + "Here is error detail: " + logPath);

            }
            _popWindow.Close();
        }
        public void SaveProductTimestamp()
        {
            string flag = _Select_Access_QSID_Data_VM.ProductTimeStamp != null && ProductDateTimestamp != null ? "C" : (_Select_Access_QSID_Data_VM.ProductTimeStamp == null && ProductDateTimestamp != null ? "A" : "D");
            //if (ProductDateTimestamp.HasValue)
            //{
            //_Select_Access_QSID_Data_VM.ProductTimeStamp = ProductDateTimestamp.Value.Add(VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime().TimeOfDay);
            //}
            //else
            //{
            DateTime blanktime;
            DateTime combinDateTime;
            if (ProductDateTimestamp.HasValue)
            {
                blanktime = Convert.ToDateTime(ProductDateTimestamp.Value.ToString("dd-MMM-yyyy"));


                if (ProductDateTimestampForTime != null)
                {
                    combinDateTime = Convert.ToDateTime(blanktime).AddHours(ProductDateTimestampForTime.Value.Hour);
                    combinDateTime = combinDateTime.AddMinutes(ProductDateTimestampForTime.Value.Minute);
                    combinDateTime = combinDateTime.AddSeconds(ProductDateTimestampForTime.Value.Second);
                }
                else
                {
                    combinDateTime = blanktime.AddHours(System.DateTime.Now.Hour);
                    combinDateTime = combinDateTime.AddMinutes(System.DateTime.Now.Minute);
                    combinDateTime = combinDateTime.AddSeconds(System.DateTime.Now.Second);
                }

                _Select_Access_QSID_Data_VM.ProductTimeStamp = combinDateTime;
            }
            //}
            _objAccessVersion_Service.UpdateAccessversionData(_Select_Access_QSID_Data_VM);
            var logProductTimeStamp = AccessVersionControlSystem.Common.CommonFunctions.BindProductTimeStampFromVersionHistory(_Select_Access_QSID_Data_VM, flag);
            _objLibraryFunction_Service.BulkIns(logProductTimeStamp);
        }

        public void SaveMapProduct(int? qsid_ID)
        {
            _objAccessVersion_Service.UpdateMapProductList(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapProductToViewModel(listProductsToMap.ToList()), (int)qsid_ID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
        }
        public void SaveParentChildQsid(int? qsid_ID)
        {
            _objAccessVersion_Service.UpdateParentChildList(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapParentChildToViewModel(listParentChildQsid.ToList()), (int)qsid_ID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);

        }
        public void SaveRelationShip(int? qsid_ID)
        {

            using (var context = new CRAModel())
            {
                var getprv = context.Map_ListRelationships.AsNoTracking().Where(x => x.ParentList_QSID_ID == qsid_ID).Select(y => new { y.Map_ListRelationShipID, y.RelatedList_QSID_ID, y.ListRelationshipID }).ToList();
                var newData = listQsidlst.Where(x => x.IsSelected == true).ToList();
                var delRect = (from d1 in getprv
                               join d2 in newData
                               on new { a = d1.RelatedList_QSID_ID, b = d1.ListRelationshipID } equals new { a = d2.Qsid_ID, b = d2.SelectedRelation.ID }
                               into tg
                               from tcheck in tg.DefaultIfEmpty()
                               where tcheck == null
                               select d1.Map_ListRelationShipID).ToList();
                if (delRect.Any())
                {
                    _objLibraryFunction_Service.DeleteRelation(delRect);
                }

                var AddRect = (from d1 in newData
                               join d2 in getprv
                               on new { a = d1.Qsid_ID, b = d1.SelectedRelation.ID } equals new { a = d2.RelatedList_QSID_ID, b = d2.ListRelationshipID }
                               into tg
                               from tcheck in tg.DefaultIfEmpty()
                               where tcheck == null
                               select new Map_ListRelationships
                               {
                                   ParentList_QSID_ID = (int)qsid_ID,
                                   RelatedList_QSID_ID = d1.Qsid_ID,
                                   ListRelationshipID = d1.SelectedRelation.ID,
                               }).ToList();
                if (AddRect.Any())
                {
                    _objLibraryFunction_Service.BulkIns<Map_ListRelationships>(AddRect);
                }
            }
        }
        private void SaveTypeChangeToDB(int? VersionControl_HistoryID)
        {
            if (SelectedTypeHeader != null)
            {
                List<ListEditorialChangeTypes_VM> listEditorialChangesVM = new List<ListEditorialChangeTypes_VM>();
                if (SelectedTypeHeader.ListVersionChangeType == "Editorial Update")
                {
                    listEditorialChangesVM = listTypeChange.Where(x => x.IsSelected).ToList();
                }
                List<Log_VersionControl_WorkOrders_VM> listWorkOrderVM = new List<Log_VersionControl_WorkOrders_VM>();
                if (!IsCheckedNoWorkOrder)
                {
                    listWorkOrderVM = listWorkOrder.Where(x => x.WO_Number != null).ToList();
                }

               
                List<EntityClass.Log_VersionControl_EditorialChangeType> listEditorialChangesEntity = AccessVersionControlSystem.Common.CommonFunctions.ConvertEditorialChangesVMToEntityModel(listEditorialChangesVM, VersionControl_HistoryID);
                List<EntityClass.Log_VersionControl_WorkOrders> listWorkOrderEntity = AccessVersionControlSystem.Common.CommonFunctions.ConvertWorkOrderToEntityModel(listWorkOrderVM, VersionControl_HistoryID);
                List<EntityClass.Log_VersionControl_CSCTickets> listTicketModel = AccessVersionControlSystem.Common.CommonFunctions.ConvertTicketsToEntityModel(listTickets.Where(x => x.Ticket_Number != null).ToList(), VersionControl_HistoryID);
                _objAccessVersion_Service.InsertUpdate_Version_TypeChange(VersionControl_HistoryID, SelectedTypeHeader.ListVersionChangeTypeID, listEditorialChangesEntity, listWorkOrderEntity, listTicketModel);
            }
        }

        private void SaveEaseProListId(int? qsid_ID)
        {
            List<EntityClass.Map_QSID_ListID> listWorkOrderEntity = AccessVersionControlSystem.Common.CommonFunctions.ConvertMapEasiProListIdToBusinessViewModel(listEasiProList.Where(x => x.EasiPro_ListID != null).ToList(), (int)qsid_ID);
            bool result = _objAccessVersion_Service.InsertUpdate_MapEasiProListId(listWorkOrderEntity, (int)qsid_ID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
            if (!result)
            {
                _notifier.ShowError("Internal Server Error While insert Map Qsid EasiPro List ID");
            }
        }
        public void SaveMapVerticals(int? qsid_ID)
        {
            _objAccessVersion_Service.UpdateMapVerticalList(AccessVersionControlSystem.Common.CommonFunctions.ConvertMapVerticalToViewModel(listVerticalsToMap.ToList()), (int)qsid_ID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
        }
        private void SaveEHSListCode(int? qsid_ID)
        {
            List<EntityClass.Map_QSID_ERC_EHSListCode> listEHSListCodeEntity = AccessVersionControlSystem.Common.CommonFunctions.ConvertMapEHSListCodeToBusinessViewModel(listERC_EHSList.Where(x => !string.IsNullOrEmpty(x.EHS_ListCode)).ToList(), (int)qsid_ID);
            bool result = _objAccessVersion_Service.InsertUpdate_MapERC_EHSListCode(listEHSListCodeEntity, (int)qsid_ID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
            if (!result)
            {
                _notifier.ShowError("Internal Server Error While insert Map Qsid EasiPro List ID");
            }
        }


        private bool CommandCancelCanExecute(object o)
        {
            return true;
        }

        private void CommandCancelExecute(object o)
        {
            _popWindow.Close();
        }
        private void SetProductDateStartEnd()
        {
            if (_popUpFlag == "UpdateTypeOfChange" || _popUpFlag == "AddProductTimestamp" || _popUpFlag == "AddNote")
            {
                ProductDateStart = _objAccessVersion_Service.GetProductDateTimeStampOfPreviousVersion(_Select_Access_QSID_Data_VM, false);
                ProductDateEnd = VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime().AddYears(1);
                NotifyPropertyChanged("ProductDateStart");
                NotifyPropertyChanged("ProductDateEnd");
            }

            if (_popUpFlag == "AddTypeOfChange")
            {
                NoteDesc = string.Empty;
                // WorkOrderNumber = (int?)null;
                ProductDateTimestamp = (DateTime?)null;
                ProductDateStart = _objAccessVersion_Service.GetProductDateTimeStampOfPreviousVersion(_Select_Access_QSID_Data_VM, true);
                ProductDateEnd = VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime().AddYears(1);
                NotifyPropertyChanged("NoteDesc");
                NotifyPropertyChanged("WorkOrderNumber");
                NotifyPropertyChanged("ProductDateTimestamp");
                NotifyPropertyChanged("ProductDateStart");
                NotifyPropertyChanged("ProductDateEnd");

            }
            //if (_popUpFlag == "UpdateTypeOfChange")
            //{
            //    ProductDateStart = _objAccessVersion_Service.GetProductDateTimeStampOfPreviousVersion(_Select_Access_QSID_Data_VM, false);
            //    ProductDateEnd = VersionControlSystem.Business.Common.CommonGeneralFunction.ESTTime().AddYears(1);
            //    NotifyPropertyChanged("ProductDateStart");
            //    NotifyPropertyChanged("ProductDateEnd");
            //}
        }

        private void CheckVisibiltyPopUpContent(bool isEnable)
        {
            visibilityAddNote = Visibility.Visible;
            visibilityParentChildQsid = Visibility.Visible;
            visibilityAddProductTimestamp = Visibility.Visible;
            visibilityTypeOfChange = Visibility.Visible;
            //IsSelectedTypeChange = true;         
            if (_popUpFlag == "AddNote")
            {
                //visibilityAddNote = Visibility.Visible; visibilityAddProductTimestamp = Visibility.Collapsed; visibilityTypeOfChange = Visibility.Collapsed;
                IsSelectedNote = true;
            }
            else if (_popUpFlag == "UpdateTypeOfChange")
            {
                //visibilityAddNote = Visibility.Collapsed; visibilityAddProductTimestamp = Visibility.Collapsed; visibilityTypeOfChange = Visibility.Visible;
                IsSelectedTypeChange = true;
            }
            else if (_popUpFlag == "AddProductTimestamp")
            {
                // visibilityAddNote = Visibility.Collapsed; visibilityAddProductTimestamp = Visibility.Visible; visibilityTypeOfChange = Visibility.Collapsed;
                IsSelectedProductDate = true;
            }
            else if (_popUpFlag == "AddTypeOfChange" || _popUpFlag == "AddTypeOfChange_FirstTime")
            {
                // visibilityAddNote = Visibility.Visible; visibilityAddProductTimestamp = Visibility.Visible; visibilityTypeOfChange = Visibility.Visible;
                IsSelectedTypeChange = true;
            }
            else if (_popUpFlag == "AddParentChildQsid")
            {
                // visibilityAddNote = Visibility.Collapsed; visibilityAddProductTimestamp = Visibility.Visible; visibilityTypeOfChange = Visibility.Collapsed;
                IsSelectedParentChildQsid = true;
            }
            else if (_popUpFlag == "AddQsidRelationShip")
            {
                IsSelectedQsidlst = true;
            }
        }

        public void BindTypeOfChange()
        {
            listTypeChange = new ObservableCollection<ListEditorialChangeTypes_VM>(Common.CommonFunctions.ConvertListEditorialChangeTypesToViewModel(_objAccessVersion_Service.GetLibrary_ListEditorialChangeTypes()));
        }

        public void BindTypeHeader()
        {
            //listTypeHeader = listTypeChange.GroupBy(x => x.TypeHeader).Select(x => new TypeChangeHeader_VM { TypeHeader = x.Key }).Distinct().ToList();

            listTypeHeader = _objAccessVersion_Service.GetLibrary_ListVersionChangeTypes();
            if (_popUpFlag == "UpdateTypeOfChange" || _popUpFlag == "AddProductTimestamp" || _popUpFlag == "AddNote")
            {

                if (_Select_Access_QSID_Data_VM.ListVersionChangeTypeID != null)
                {
                    SelectedTypeHeader = listTypeHeader.Where(x => x.ListVersionChangeTypeID == _Select_Access_QSID_Data_VM.ListVersionChangeTypeID).FirstOrDefault();
                }
            }
        }

        public void GetChangeTypeList()
        {
            if (_popUpFlag == "UpdateTypeOfChange" || _popUpFlag == "AddProductTimestamp" || _popUpFlag == "AddNote")
            {
                var existingEditorialTypeChange = _objAccessVersion_Service.GetLog_VersionControl_ListEditorialChangeTypes(_Select_Access_QSID_Data_VM.VersionControl_ID);
                listTypeChange.Where(x => existingEditorialTypeChange.Any(y => y.ListEditorialChangeTypeID == x.ListEditorialChangeTypeID)).ToList().ForEach(x => x.IsSelected = true);
            }
            NotifyPropertyChanged("listTypeChange");
        }

        public void BindTypeChangeList()
        {
            if (SelectedTypeHeader != null)
            {
                if (SelectedTypeHeader.ListVersionChangeType == "Editorial Update")
                {
                    GetChangeTypeList();
                    ddlChangeVisibility_List = Visibility.Visible;
                }
                else
                {
                    ddlChangeVisibility_List = Visibility.Collapsed;
                    visibilityTextBoxWO = Visibility.Visible;
                }
            }
            else
            {
                ddlChangeVisibility_List = Visibility.Collapsed;
            }

            NotifyPropertyChanged("ddlChangeVisibility_List");

        }

        public void EnableWorkOrderControl()
        {
            if (SelectedTypeChange != null)
            {
                if (SelectedTypeChange.ListEditorialChangeType == "WO Number")
                {
                    visibilityTextBoxWO = Visibility.Visible;
                }
                else
                {
                    visibilityTextBoxWO = Visibility.Collapsed;
                }
            }
            else
            {
                visibilityTextBoxWO = Visibility.Collapsed;
            }
        }

        public void UpdateVersionControl_QcChecks(int versionControlHistoryId)
        {
            List<Log_VersionControl_QcChecks> listLogQcCheck = new();
            listLogQcCheck = qcCheckUC_Context.listQcCheck.Select(x => new Log_VersionControl_QcChecks()
            {
                QcCheckID = x.QcCheckID,
                IsCompleted = x.IsCompleted,
                VersionControl_HistoryID = versionControlHistoryId,
                DateAdded = objCommonFunc.ESTTime(),
                IsNotApplicable = x.IsNotApplicable,
                UserID = Environment.UserName
            }).ToList();
            _objLibraryFunction_Service.InsertLogVersionControlQcCheck(listLogQcCheck, versionControlHistoryId);
        }
        private void SaveTimeSpentToDB(int? VersionControl_HistoryID)
        {
            if (!string.IsNullOrWhiteSpace(TimespentValue) && !string.IsNullOrEmpty(TimespentValue) && TimespentUnit != null)
            {
                _objLibraryFunction_Service.UpdateTimeSpentToVersionHistory((int)VersionControl_HistoryID, Convert.ToInt32(TimespentValue), TimespentUnit.Text);
            }

        }
        private void BindTimeSpentUnit()
        {
            var listUnit = new List<SelectListItem>();
            listUnit.Add(new SelectListItem() { Text = "Hours" });
            listUnit.Add(new SelectListItem() { Text = "Minutes" });
            listTimeSpentUnits = new ObservableCollection<SelectListItem>(listUnit);
            NotifyPropertyChanged("listTimeSpentUnits");
            if (!string.IsNullOrEmpty(_Select_Access_QSID_Data_VM.TimeSpentUnit))
            {
                TimespentUnit = listTimeSpentUnits.Where(x => x.Text == _Select_Access_QSID_Data_VM.TimeSpentUnit).FirstOrDefault();
                NotifyPropertyChanged("TimespentUnit");
                TimespentValue = _Select_Access_QSID_Data_VM.TimeSpentValue != null ? _Select_Access_QSID_Data_VM.TimeSpentValue.ToString() : "";
                NotifyPropertyChanged("TimespentValue");
            }
            else
            {
                TimespentUnit = listTimeSpentUnits.Where(x => x.Text == "Hours").FirstOrDefault();
                NotifyPropertyChanged("TimespentUnit");
            }
        }


        #endregion
    }
    public class libQsid
    {
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
    }

}
