﻿using AccessVersionControlSystem.Library;
using AccessVersionControlSystem.ModelPopupWindow;
using CurrentDataView.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class Popup_Normaliztion_ViewModel : Base_ViewModel
    {
        ILibraryFunction _objLibraryFunction_Service;
        public Popup_Normaliztion_ViewModel(PopUp_NormalizationHistory popup_Window, Log_Session_NormalizeHistory_VM session_VM, string columnName, int qsid_ID)
        {
            _objLibraryFunction_Service = LibraryFunction.GetInstance;
            _popup_Window = popup_Window;
            _session_VM = session_VM;
            _qsid_ID = qsid_ID;
            //_session_ID = session_VM.session_ID;
            _columnName = columnName;
            //_qsidVersion = qsidVersion;
            Init();
        }
        public int _qsid_ID { get; set; }
        public Log_Session_NormalizeHistory_VM _session_VM { get; set; }
        public PopUp_NormalizationHistory _popup_Window { get; set; }
        public string TitlePopUp { get; set; }
        public string _columnName { get; set; }
        public string _qsidVersion { get; set; }


        public IList ListUniqueAddRow { get; set; }
        public IList ListUniqueChangeRow { get; set; }
        public IList ListUniqueDeleteRow { get; set; }

        public bool IsSelectedErrorFail { get; set; }  =false;
        public bool IsSelectedErrorPass { get; set; }  =false;
        public bool IsSelectedRowsMainTab { get; set; }=false;
        public bool IsSelectedRowsAdded { get; set; }  =false;
        public bool IsSelectedRowsChanged { get; set; }=false;
        public bool IsSelectedRowsDeleted { get; set; }=false;
        public bool IsSelectedCasMainTab { get; set; } =false;
        public bool IsSelectedCasAdded { get; set; }   =false;
        public bool IsSelectedCasDeleted { get; set; } = false;
        public bool IsSelectedDataDicMainTab { get; set; } = false;
        public bool IsSelectedDataDicAdded { get; set; } = false;
        public bool IsSelectedDataDicDeleted { get; set; } = false;
        public bool IsSelectedListDicMainTab { get; set; } = false;

        public bool IsSelectedListFieldExplainChangesTab { get; set; } = false;

        public ObservableCollection<Error_Warning_Summary_VM> ListErrorPass { get; set; }
        public ObservableCollection<Error_Warning_Summary_VM> ListErrorFail { get; set; }
        public ObservableCollection<Error_Warning_Summary_VM> ListWarningFail { get; set; }
        public ObservableCollection<Error_Warning_Summary_VM> ListRequiredAction { get; set; }

        public StandardDataGrid_VM objSummaryAdded { get; set; }
        public StandardDataGrid_VM objSummaryDeleted { get; set; }
        public StandardDataGrid_VM objCASAdded { get; set; }
        public StandardDataGrid_VM objCASDeleted { get; set; }
        public StandardDataGrid_VM objCASChanged { get; set; }

        public StandardDataGrid_VM objSummaryChanged { get; set; }

        public Visibility LoadSummAdded { get; set; }
        public Visibility LoadSummDel { get; set; }
        public Visibility LoadCASChanges { get; set; }
        public Visibility LoadSummChg { get; set; }

        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }

        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {
                case "Rows Addded":
                    LoadSummChg = Visibility.Visible;
                    NotifyPropertyChanged("LoadSummChg");
                    Task.Run(() => UniqueAdded());
                    break;
                case "Rows Changed":
                    Task.Run(() => UniqueChanges());
                    break;
                case "Rows Deleted":
                    Task.Run(() => UniqueDeleted());
                    break;
                case "CAS Changes":
                    Task.Run(() => CASChanges());
                    break;
            }
        }
        public CommonDataGrid_ViewModel<LogNormalizedAddQsidField> comonDGChangesReportAddField_VM { get; set; }
        public CommonDataGrid_ViewModel<LogNormalizedDeleteQsidField> comonDGChangesReportDeleteField_VM { get; set; }
        public CommonDataGrid_ViewModel<Log_ListDictionaryChanges_VM> comonDGListDicChanges_VM { get; set; }
        public CommonDataGrid_ViewModel<Log_ListFieldExplainChanges_VM> comonDGListFieldExplainChanges_VM { get; set; }

        public ObservableCollection<LogNormalizedAddQsidField> _listAddedFieldQsid { get; set; }
        public ObservableCollection<LogNormalizedAddQsidField> ListAddedFieldQsid { get { return _listAddedFieldQsid; } set {
                if (_listAddedFieldQsid != value) {
                    _listAddedFieldQsid = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<LogNormalizedAddQsidField>>>(new PropertyChangedMessage<List<LogNormalizedAddQsidField>>(_listAddedFieldQsid.ToList(), _listAddedFieldQsid.ToList(), "Default List"));

                }
            } }
        public ObservableCollection<LogNormalizedDeleteQsidField> _listDeletedFieldQsid { get; set; }
        public ObservableCollection<LogNormalizedDeleteQsidField> ListDeletedFieldQsid
        {
            get { return _listDeletedFieldQsid; }
            set
            {
                if (_listDeletedFieldQsid != value)
                {
                    _listDeletedFieldQsid = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<LogNormalizedDeleteQsidField>>>(new PropertyChangedMessage<List<LogNormalizedDeleteQsidField>>(_listDeletedFieldQsid.ToList(), _listDeletedFieldQsid.ToList(), "Default List"));

                }
            }
        }
        public ObservableCollection<Log_ListDictionaryChanges_VM> _lisDicionaryChanges { get; set; }
        public ObservableCollection<Log_ListDictionaryChanges_VM> ListDicionaryChanges
        {
            get { return _lisDicionaryChanges; }
            set
            {
                if (_lisDicionaryChanges != value)
                {
                    _lisDicionaryChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Log_ListDictionaryChanges_VM>>>(new PropertyChangedMessage<List<Log_ListDictionaryChanges_VM>>(_lisDicionaryChanges.ToList(), _lisDicionaryChanges.ToList(), "Default List"));

                }
            }
        }
        public ObservableCollection<Log_ListFieldExplainChanges_VM> _ListFieldExplainChanges { get; set; }
        public ObservableCollection<Log_ListFieldExplainChanges_VM> ListFieldExplainChanges
        {
            get { return _ListFieldExplainChanges; }
            set
            {
                if (_ListFieldExplainChanges != value)
                {
                    _ListFieldExplainChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Log_ListFieldExplainChanges_VM>>>(new PropertyChangedMessage<List<Log_ListFieldExplainChanges_VM>>(_ListFieldExplainChanges.ToList(), _ListFieldExplainChanges.ToList(), "Default List"));

                }
            }
        }

        public void Init()
        {
            comonDGChangesReportAddField_VM = new CommonDataGrid_ViewModel<LogNormalizedAddQsidField>(GetListReportAddFieldGridCol(), "Qsid", "New fields added :");
            NotifyPropertyChanged("comonDGChangesReportAddField_VM");
            comonDGChangesReportDeleteField_VM = new CommonDataGrid_ViewModel<LogNormalizedDeleteQsidField>(GetListReportChangeDeleteFieldGridCol(), "Qsid", "Deleted fields :");
            NotifyPropertyChanged("comonDGChangesReportDeleteField_VM");
            comonDGListDicChanges_VM = new CommonDataGrid_ViewModel<Log_ListDictionaryChanges_VM>(GetListDictionaryChanges(), "PhraseCategory", "List Dictionary Changes :");
            NotifyPropertyChanged("comonDGListDicChanges_VM");
            comonDGListFieldExplainChanges_VM = new CommonDataGrid_ViewModel<Log_ListFieldExplainChanges_VM>(GetListFieldExplainChanges(), "FieldName", "Field Explain Changes :");
            NotifyPropertyChanged("comonDGListFieldExplainChanges_VM");
            BindSummaryErrorWarningPass();
            BindSummaryError();
            BindSummaryWarning();
            BindSummaryRequiedAction();
            BindDataDictonaryChanges();
            ActiveTabControlAccordingColumn();
            //Task.Run(() => BindSummary_UniqueRowAdded());
            TitlePopUp = "Summary Normalization QSIDversion: " + _session_VM.VersionNumber + "; Field Type: " + _columnName;
        }
        public void BindDataDictonaryChanges() {
            Task.Run(() =>
            {
                ListAddedFieldQsid = new ObservableCollection<LogNormalizedAddQsidField>(Common.CommonFunctions.CovertAddQsidBusinessToViewModel(_objAccessVersion_Service.GetAddedQsidFieldBySessionID(_session_VM.SessionId, _qsid_ID)));
            });
            Task.Run(() =>
            {
                ListDeletedFieldQsid = new ObservableCollection<LogNormalizedDeleteQsidField>(Common.CommonFunctions.CovertDeletedQsidBusinessToViewModel( _objAccessVersion_Service.GetDeletedQsidFieldBySessionID(_session_VM.SessionId, _qsid_ID)));
            });
            Task.Run(() =>
            {
                ListDicionaryChanges = new ObservableCollection<Log_ListDictionaryChanges_VM>(_objAccessVersion_Service.GetListDictionaryChanges(_session_VM.SessionId, _qsid_ID));
            });
            Task.Run(() =>
            {
                ListFieldExplainChanges = new ObservableCollection<Log_ListFieldExplainChanges_VM>(_objAccessVersion_Service.GetListFieldExplainChanges(_session_VM.SessionId,_qsid_ID));
            });
        }
        private List<CommonDataGridColumn> GetListDictionaryChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Category", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "New Value", ColBindingName = "NewValue", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Old Value", ColBindingName = "OldValue", ColType = "Textbox" });            
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListFieldExplainChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldExplain", ColBindingName = "OldExplain", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewExplain", ColBindingName = "NewExplain", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldHeadCD", ColBindingName = "OldHeadCD", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewHeadCD", ColBindingName = "NewHeadCD", ColType = "Textbox" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListReportAddFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            return listColumn;
        }

        private List<CommonDataGridColumn> GetListReportChangeDeleteFieldGridCol()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Name", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Display Header", ColBindingName = "FieldDisplayHeader", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field Explanation", ColBindingName = "FieldExplanation", ColType = "Textbox" });
            return listColumn;
        }
        public void BindSummaryErrorWarningPass()
        {
            var listPassSummary = _objLibraryFunction_Service.GetErrorWarningPassSummaryBySession(_session_VM.SessionId).ToList();
            ListErrorPass = new ObservableCollection<Error_Warning_Summary_VM>(listPassSummary);
        }

        public void BindSummaryError()
        {
            var listErrorSummary = _objLibraryFunction_Service.GetErrorWarningSummaryBySession(_session_VM.SessionId, "Error").ToList();
            ListErrorFail = new ObservableCollection<Error_Warning_Summary_VM>(listErrorSummary);
        }
        public void BindSummaryWarning()
        {
            var listErrorSummary = _objLibraryFunction_Service.GetErrorWarningSummaryBySession(_session_VM.SessionId, "Warning").ToList();
            ListWarningFail = new ObservableCollection<Error_Warning_Summary_VM>(listErrorSummary);
        }
        public void BindSummaryRequiedAction()
        {
            var listErrorSummary = _objLibraryFunction_Service.GetErrorWarningSummaryBySession(_session_VM.SessionId, "Require Action").ToList();
            ListRequiredAction = new ObservableCollection<Error_Warning_Summary_VM>(listErrorSummary);
        }

        //public void BindSummary_UniqueRowAdded()
        //{
        //    UniqueAdded();
        //    UniqueDeleted();
        //    UniqueChanges();
        //}

        private void UniqueChanges()
        {
            
            var chgRows = _objLibraryFunction_Service.GetUniqueRow_Changes_Summary(_session_VM.SessionId, _qsid_ID, "C");
            if (chgRows != null && chgRows.Rows.Count > 0)
            {
                objSummaryChanged = new StandardDataGrid_VM("Summary Changed", chgRows, Visibility.Hidden);
                // ListUniqueChangeRow = BindListFromTable(chgRows);
                NotifyPropertyChanged("objSummaryChanged");
            }
            LoadSummChg = Visibility.Collapsed;
            NotifyPropertyChanged("LoadSummChg");
        }
        private void CASChanges()
        {
            LoadCASChanges = Visibility.Visible;
            NotifyPropertyChanged("LoadCASChanges");
            var newCAS = _objLibraryFunction_Service.NewCASAddedfromLastSessions(_session_VM.SessionId, _qsid_ID);
            if (newCAS != null && newCAS.Rows.Count > 0)
            {
                objCASAdded = new StandardDataGrid_VM("New CAS", newCAS, Visibility.Hidden);
                NotifyPropertyChanged("objCASAdded");
            }
            var delCAS = _objLibraryFunction_Service.CompletelyDeletedCASfromLastSessions(_session_VM.SessionId, _qsid_ID);
            if (delCAS != null && delCAS.Rows.Count > 0)
            {
                objCASDeleted = new StandardDataGrid_VM("Deleted CAS", delCAS, Visibility.Hidden);
                NotifyPropertyChanged("objCASDeleted");
            }
            var chgCAS = _objLibraryFunction_Service.ChangedInCASRowId(_session_VM.SessionId, _qsid_ID);
            if (chgCAS != null && chgCAS.Rows.Count > 0)
            {
                objCASChanged = new StandardDataGrid_VM("Changed CAS", chgCAS, Visibility.Hidden);
                NotifyPropertyChanged("objCASChanged");
            }
            LoadCASChanges = Visibility.Collapsed;
            NotifyPropertyChanged("LoadCASChanges");
        }



        private void UniqueDeleted()
        {
            LoadSummDel = Visibility.Visible;
            NotifyPropertyChanged("LoadSummDel");
            var delRows = _objLibraryFunction_Service.GetUniqueRow_ADC_Summary(_session_VM.SessionId, _qsid_ID, "D");
            if (delRows != null && delRows.Rows.Count > 0)
            {
                objSummaryDeleted = new StandardDataGrid_VM("Summary Deleted", delRows, Visibility.Hidden);
                //ListUniqueDeleteRow = BindListFromTable(delRows);
                NotifyPropertyChanged("objSummaryDeleted");
            }
            LoadSummDel = Visibility.Collapsed;
            NotifyPropertyChanged("LoadSummDel");
        }

        private void UniqueAdded()
        {
            LoadSummAdded = Visibility.Visible;
            NotifyPropertyChanged("LoadSummAdded");
            var addRows = _objLibraryFunction_Service.GetUniqueRow_ADC_Summary(_session_VM.SessionId, _qsid_ID, "A");
            if (addRows != null && addRows.Rows.Count > 0)
            {
                objSummaryAdded = new StandardDataGrid_VM("Summary Added", addRows, Visibility.Hidden);
                //ListUniqueAddRow = BindListFromTable(addRows);
                NotifyPropertyChanged("objSummaryAdded");
            }
            LoadSummAdded = Visibility.Collapsed;
            NotifyPropertyChanged("LoadSummAdded");
        }

        //public IList BindListFromTable(DataTable dt)
        //{
        //    var obj = MyTypeBuilder.CreateNewObject(dt.Columns);
        //    var listType = typeof(List<>).MakeGenericType(new[] { obj });
        //    System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
        //    var fields = obj.GetProperties();
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        var ob = Activator.CreateInstance(obj);

        //        foreach (var fieldInfo in fields)
        //        {
        //            foreach (DataColumn dc in dt.Columns)
        //            {
        //                if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
        //                {
        //                    object value = (dr[dc.ColumnName] == DBNull.Value) ? null : dr[dc.ColumnName];
        //                    if (value != null)
        //                    {
        //                        var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
        //                        var result = converter.ConvertFrom(value.ToString());
        //                        fieldInfo.SetValue(ob, result);
        //                    }
        //                    else
        //                    {
        //                        fieldInfo.SetValue(ob, value);

        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //        lst.Add(ob);
        //    }

        //    return lst;
        //}

        public void ActiveTabControlAccordingColumn()
        {
            switch (_columnName)
            {
                case "Errors":
                    IsSelectedErrorFail = true;
                    break;
                case "Warnings":
                    IsSelectedErrorPass = true;
                    break;
                case "Rows Added":
                    IsSelectedRowsMainTab = true;
                    IsSelectedRowsAdded = true;
                    break;
                case "Rows Deleted":
                    IsSelectedRowsMainTab = true;
                    IsSelectedRowsDeleted = true;
                    break;
                case "Rows Changed":
                    IsSelectedRowsMainTab = true;
                    IsSelectedRowsChanged = true;
                    break;
                case "Cas Added":
                    IsSelectedCasMainTab = true;
                    IsSelectedCasAdded = true;
                    break;
                case "Cas Deleted":
                    IsSelectedCasMainTab = true;
                    IsSelectedCasDeleted = true;
                    break;
                case "Fields Added":
                    IsSelectedDataDicMainTab = true;
                    IsSelectedDataDicAdded = true;
                    break;
                case "Fields Deleted":
                    IsSelectedDataDicMainTab = true;
                    IsSelectedDataDicDeleted = true;
                    break;
                case "List Dictionary Changes":
                    IsSelectedListDicMainTab = true;                  
                    break;
                case "Field Explain Changes":
                    IsSelectedListFieldExplainChangesTab = true;
                    break;
                default:
                    IsSelectedErrorPass = true;
                    break;

            }
        }
    }
}
