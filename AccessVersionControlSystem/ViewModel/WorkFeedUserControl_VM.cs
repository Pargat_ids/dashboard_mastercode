﻿using AccessVersionControlSystem.Library;
using GalaSoft.MvvmLight.Messaging;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class WorkFeedUserControl_VM : Library.Base_ViewModel
    {
        public ICommand CommandRefresh { get; set; }
        private readonly DispatcherTimer Counter_Timer = new DispatcherTimer();
        public int countListCheckOutUser { get; set; } = 0;
        public int countMarkedProduction { get; set; } = 0;
        public int countAddSuggestion { get; set; } = 0;
        public int countSuggestionDisapproved { get; set; } = 0;
        public int countSuggestionPendingReview { get; set; } = 0;
        public int countSuggestionDelete { get; set; } = 0;
        public int countDelSuggestionDisapproved { get; set; } = 0;
        public int countDelSuggestionPending { get; set; } = 0;
        public int countAdditionalGenericsReview { get; set; } = 0;
        public int countAdditionalCompletedGenerics { get; set; } = 0;

        public SeriesCollection serCol_MarkedProduction { get; set; } = new SeriesCollection();
        public string[] labels_MarkedProduction { get; set; }
        public SeriesCollection serCol_AddSuggestion { get; set; } = new SeriesCollection();
        public string[] labels_AddSuggestion { get; set; }

        public SeriesCollection serCol_AddDisapproveSuggestion { get; set; } = new SeriesCollection();
        public string[] labels_AddDisapproveSuggestion { get; set; }
        public SeriesCollection serCol_DeleteSuggestion { get; set; } = new SeriesCollection();
        public string[] labels_DeleteSuggestion { get; set; }
        public SeriesCollection serCol_DeleteDisapprovedSuggestion { get; set; } = new SeriesCollection();
        public string[] labels_DeleteDisapprovedSuggestion { get; set; }
        public SeriesCollection serCol_AdditionalGenericsCompleted { get; set; } = new SeriesCollection();
        public string[] labels_AdditionalGenericsCompleted { get; set; }

        public Func<double, string> Formatter { get; set; }
        public Func<DateTime, string> XFormatter { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public ICommand commandSearchWorkFeed { get; set; }
        public ObservableCollection<ListCommonComboBox_ViewModel> listUser_Res { get; set; } = new ObservableCollection<ListCommonComboBox_ViewModel>();
        private ListCommonComboBox_ViewModel _selectedUser_Res { get; set; }
        public ListCommonComboBox_ViewModel selectedUser_Res
        {
            get { return _selectedUser_Res; }
            set
            {
                _selectedUser_Res = value;
                NotifyPropertyChanged("selectedUser_Res");
            }
        }

        public List<WorkFeedDisapprovedSug_ViewModel> listDisapproveGenComments { get; set; }
        public List<GenericSuggestionComments_VM> listItemComments { get; set; }

        public ICommand CommandFilterDisaaprovedSuggestion { get; set; }
        public ICommand CommandFilterCheckOutUser { get; set; }
        public ICommand CommandFilterAddSuggestion { get; set; }
        public ICommand CommandFilterSuggestionPendingReview { get; set; }
        public ICommand CommandFilterSuggestionDelete { get; set; }
        public ICommand CommandFilterDelDisApprove { get; set; }
        public ICommand CommandFilterDelPending { get; set; }
        public ICommand CommandFilterAdditionalGenReview { get; set; }
        public ICommand CommandFilterAdditionalGenCompleted { get; set; }

        public WorkFeedUserControl_VM()
        {
            startDate = (DateTime?)null;
            endDate = (DateTime?)null;

            Formatter = value => value.ToString("N");


            Counter_Timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            // Counter_Timer.Tick += dispatcherTimer_Tick;
            commandSearchWorkFeed = new RelayCommand(commandSearchWorkFeedExecute);
            CommandFilterDisaaprovedSuggestion = new RelayCommand(CommandFilterDisaaprovedSuggestionExecute);
            CommandFilterCheckOutUser = new RelayCommand(CommandFilterCheckOutUserExecute);
            CommandFilterAddSuggestion = new RelayCommand(CommandFilterAddSuggestionExecute);
            CommandFilterSuggestionPendingReview = new RelayCommand(CommandFilterSuggestionPendingReviewExecute);
            CommandFilterSuggestionDelete = new RelayCommand(CommandFilterDelDisApproveExecute);
            CommandFilterDelDisApprove = new RelayCommand(CommandFilterDisaaprovedSuggestionExecute);
            CommandFilterDelPending = new RelayCommand(CommandFilterDelPendingExecute);
            CommandFilterAdditionalGenReview = new RelayCommand(CommandFilterAdditionalGenReviewExecute);
            CommandFilterAdditionalGenCompleted = new RelayCommand(CommandFilterAdditionalGenCompletedExecute);



            BindWorkFeedData();
            BindWorkFeedGraphDetail();
            BindListUser();
            BindItemComments();
            BindGenDisapproveFeed();
        }

        private void CommandFilterAdditionalGenCompletedExecute(object obj)
        {

        }

        private void CommandFilterAdditionalGenReviewExecute(object obj)
        {

        }

        private void CommandFilterDelPendingExecute(object obj)
        {

        }

        private void CommandFilterDelDisApproveExecute(object obj)
        {

        }

        private void CommandFilterSuggestionPendingReviewExecute(object obj)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("",  startDate + "@" + endDate, "FilterAddSuggestionReview"), typeof(WorkFeedUserControl_VM));
        }

        private void CommandFilterAddSuggestionExecute(object obj)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", (selectedUser_Res != null ? selectedUser_Res.Id_String : "") + "@" + startDate + "@" + endDate, "FilterAddSuggestionReview"), typeof(WorkFeedUserControl_VM));
        }

        private void CommandFilterCheckOutUserExecute(object obj)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", selectedUser_Res != null ? selectedUser_Res.Id_String : "", "FilterCheckOutUser"), typeof(WorkFeedUserControl_VM));
        }

        private void CommandFilterDisaaprovedSuggestionExecute(object obj)
        {

        }

        public void BindListUser()
        {
            var listUser = new List<ListCommonComboBox_ViewModel>();
            listUser.Add(new ListCommonComboBox_ViewModel()
            {
                Id = -1,
                Id_String = "-1",
                Name = "--- ALL ---",

            });
            listUser.AddRange(_objLibraryFunction_Service.GetAllActiveLibrary_Users().Select(x => new ListCommonComboBox_ViewModel()
            {
                Id = x.Id,
                Id_String = x.Id_String,
                Name = x.Name,

            }).ToList());
            listUser_Res = new ObservableCollection<ListCommonComboBox_ViewModel>(listUser);
            NotifyPropertyChanged("listUser_Res");
        }
        private void commandSearchWorkFeedExecute(object obj)
        {
            BindWorkFeedData();
            BindWorkFeedGraphDetail();
            BindItemComments();
            BindGenDisapproveFeed();
        }

        public void BindWorkFeedData()
        {
            Task.Run(() =>
            {
                var ObjWorkFeed = _objLibraryFunction_Service.GetWorkFeed((selectedUser_Res != null ? selectedUser_Res.Id_String : Environment.UserName), startDate, endDate);
                countAdditionalGenericsReview = ObjWorkFeed.countAdditionalGenericsReview;
                countAddSuggestion = ObjWorkFeed.CountAddSuggestion;
                countDelSuggestionDisapproved = ObjWorkFeed.CountDelRejectSuggestion;
                countDelSuggestionPending = ObjWorkFeed.CountDelPendingSuggestion;
                countListCheckOutUser = ObjWorkFeed.CountCheckOut;
                countMarkedProduction = ObjWorkFeed.CountMarkedProduction;
                countSuggestionDelete = ObjWorkFeed.CountDeleteSuggestion;
                countSuggestionDisapproved = ObjWorkFeed.CountAddRejectSuggestion;
                countSuggestionPendingReview = ObjWorkFeed.CountPendingSuggestion;
                countAdditionalCompletedGenerics = ObjWorkFeed.countAdditionalCompletedGenericsReview;

                NotifyPropertyChanged("countAdditionalGenericsReview");
                NotifyPropertyChanged("countAddSuggestion");
                NotifyPropertyChanged("countDelSuggestionDisapproved");
                NotifyPropertyChanged("countDelSuggestionPending");
                NotifyPropertyChanged("countListCheckOutUser");
                NotifyPropertyChanged("countMarkedProduction");
                NotifyPropertyChanged("countSuggestionDelete");
                NotifyPropertyChanged("countSuggestionDisapproved");
                NotifyPropertyChanged("countSuggestionPendingReview");
                NotifyPropertyChanged("countAdditionalCompletedGenerics");
            });
        }
        public void BindWorkFeedGraphDetail()
        {
            var detailWorkFeed = _objLibraryFunction_Service.GetWorkFeedDetail((selectedUser_Res != null ? selectedUser_Res.Id_String : Environment.UserName), startDate, endDate);
            BindGraphDetail_Production(detailWorkFeed.listMarkedProduction);
            BindGraphDetail_AddSuggestion(detailWorkFeed.listAddSuggestion, "Add");
            BindGraphDetail_AddSuggestion(detailWorkFeed.listAddRejectedSuggestion, "AddDisapprove");
            BindGraphDetail_AddSuggestion(detailWorkFeed.listDeleteSuggestion, "Delete");
            BindGraphDetail_AddSuggestion(detailWorkFeed.listDeleteRejectSuggestion, "DeleteDisapprove");
        }

        public void BindGraphDetail_Production(List<EntityClass.Log_VersionControl_History> list)
        {
            var objProductionList = list.OrderBy(x => x.ProductTimeStamp).GroupBy(x => x.ProductTimeStamp.Value.Date).Select(x =>
                new ColumnSeries
                {
                    Title = x.Key.ToString(),
                    Values = new ChartValues<double> { x.Count() }
                }).ToList();
            var serCol = new SeriesCollection();
            serCol.AddRange(objProductionList);
            serCol_MarkedProduction = new SeriesCollection();
            serCol_MarkedProduction.AddRange(serCol);
            NotifyPropertyChanged("serCol_MarkedProduction");
            labels_MarkedProduction = null;
            labels_MarkedProduction = objProductionList.Select(x => x.Title).ToArray();
            NotifyPropertyChanged("labels_MarkedProduction");
        }
        public void BindGraphDetail_AddSuggestion(List<EntityClass.Log_Map_Generics_Suggestions> list, string type)
        {
            var objList = list.OrderBy(x => x.SuggestedOn).GroupBy(x => x.SuggestedOn.Value.Date).Select(x =>
                new ColumnSeries
                {
                    Title = x.Key.ToString(),
                    Values = new ChartValues<double> { x.Count() }
                }).ToList();
            var serCol = new SeriesCollection();
            serCol.AddRange(objList);
            if (type == "Add")
            {
                serCol_AddSuggestion = new();
                labels_AddSuggestion = null;
                serCol_AddSuggestion.AddRange(serCol);
                NotifyPropertyChanged("serCol_AddSuggestion");
                labels_AddSuggestion = objList.Select(x => x.Title).ToArray();
                NotifyPropertyChanged("labels_AddSuggestion");
            }
            else if (type == "AddDisapprove")
            {
                serCol_AddDisapproveSuggestion = new();
                labels_AddDisapproveSuggestion = null;
                serCol_AddDisapproveSuggestion.AddRange(serCol);
                NotifyPropertyChanged("serCol_AddDisapproveSuggestion");
                labels_AddDisapproveSuggestion = objList.Select(x => x.Title).ToArray();
                NotifyPropertyChanged("labels_AddDisapproveSuggestion");
            }
            else if (type == "Delete")
            {
                serCol_DeleteSuggestion = new();
                labels_DeleteSuggestion = null;
                serCol_DeleteSuggestion.AddRange(serCol);
                NotifyPropertyChanged("serCol_DeleteSuggestion");
                labels_DeleteSuggestion = objList.Select(x => x.Title).ToArray();
                NotifyPropertyChanged("labels_DeleteSuggestion");
            }
            else if (type == "DeleteDisapprove")
            {
                serCol_DeleteDisapprovedSuggestion = new();
                labels_DeleteDisapprovedSuggestion = null;
                serCol_DeleteDisapprovedSuggestion.AddRange(serCol);
                NotifyPropertyChanged("serCol_DeleteDisapprovedSuggestion");
                labels_DeleteDisapprovedSuggestion = objList.Select(x => x.Title).ToArray();
                NotifyPropertyChanged("labels_DeleteDisapprovedSuggestion");
            }

        }

        public void BindItemComments()
        {
            listItemComments = _objLibraryFunction_Service.GetListCommentTaggedUser(selectedUser_Res != null ? selectedUser_Res.Id_String : Environment.UserName).Select(x => new GenericSuggestionComments_VM
            {
                Action = x.Action,
                ChildCasID = x.ChildCasID,
                CommentBy = x.CommentBy,
                Comments = x.Comments,
                ParentCasID = x.ParentCasID,
                Timestamp = x.Timestamp,
                Additional_Generics_research_flag = x.Additional_Generics_research_flag,
                CommentID = x.CommentID
            }).ToList();
            NotifyPropertyChanged("listItemComments");
        }
        public void BindGenDisapproveFeed()
        {
            listDisapproveGenComments = _objLibraryFunction_Service.GetListFeedDisapprovedGenSuggestion(selectedUser_Res != null ? selectedUser_Res.Id_String : Environment.UserName).Select(x => new WorkFeedDisapprovedSug_ViewModel
            {
                ChildCas = x.ChildCas,
                ChildCasid = x.ChildCasid,
                ParentCas = x.ParentCas,
                ParentCasID = x.ParentCasID,
                ReviewedOn = x.ReviewedOn,
                ReviewedUser = x.ReviewedUser,
                SuggestedOn = x.SuggestedOn,
                SuggestedUser = x.SuggestedUser,
            }).ToList();
            NotifyPropertyChanged("listDisapproveGenComments");
        }
    }
}
