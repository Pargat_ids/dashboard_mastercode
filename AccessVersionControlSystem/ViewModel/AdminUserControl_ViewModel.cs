﻿using AccessVersionControlSystem.Library;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using CRA_DataAccess;
using System.Data;
using EntityClass;
using Map_VersionControl_Permissions = VersionControlSystem.Model.DataModel.Map_VersionControl_Permissions;
using AccessVersionControlSystem.ModelPopupWindow;
using VersionControlSystem.Model.ApplicationEngine;

namespace AccessVersionControlSystem.ViewModel
{
    public class AdminUserControl_ViewModel : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public AdminUserControl_ViewModel()
        {
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            Init();
        }
        public CommonDataGrid_ViewModel<PhraseHashPending> comonUnMatchHash_VM { get; set; }
        public ICommand CommandupdateUnmatched { get; set; }
        public Visibility VisibleLoder { get; set; }
        
        public List<Map_VersionControl_Permissions> listRolePermission { get; set; }

       

        public List<EntityClass.Library_Roles> listRoles { get; set; }

        public EntityClass.Library_Roles _selectedRole { get; set; }
        public EntityClass.Library_Roles SelectedRole
        {
            get { return _selectedRole; }
            set
            {
                _selectedRole = value;
                NotifyPropertyChanged("SelectedRole");
                GetPermissionTree();
            }
        }
        public ObservableCollection<MapPermission_ViewModel> listItems { get; set; } = new ObservableCollection<MapPermission_ViewModel>();
        public ICommand CommandSavePermission { get; set; }
        public void Init()
        {
            // start upload chemical and phrases
            comonUploadChemical_VM = new CommonDataGrid_ViewModel<IUploadChem>(GetListGridColumnUploadChemical(), "", "Upload Data");

            ListChemType = new ObservableCollection<string>(AddType());
            NotifyPropertyChanged("ListChemType");
            DownloadTemplateCommand = new RelayCommand(DownloadTemplateCommandExecute, CommandInsertUpdateRolePermissionCanExecute);
            UploadCommand = new RelayCommand(UploadCommandExecute, CommandInsertUpdateRolePermissionCanExecute);
            objCommon = new CommonFunctions();
            BtnChecKAndSave = new RelayCommand(BtnChecKAndSaveExecute, CommandInsertUpdateRolePermissionCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<IUploadChem>>(this, NotifyMe);
            // end upload chemical and phrases

            CommandupdateUnmatched = new RelayCommand(CommandUpdateUnmatchedExecute, CommandInsertUpdateRolePermissionCanExecute);
          
           

            comonUnMatchHash_VM = new CommonDataGrid_ViewModel<PhraseHashPending>(GetListUnmatchedPhraseGridCol(), "QSID", "Hash UnMatched Phrases");
            System.Threading.Tasks.Task.Run(() => updateGrid(false));
            GetLibraryRoles();
            CommandSavePermission = new RelayCommand(CommandSavePermissionExecute);
        }



        private void CommandUpdateUnmatchedExecute(object o)
        {
            VisibleLoder = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoder");

            System.Threading.Tasks.Task.Run(() =>
            {
                _objLibraryFunction_Service.UpdateUnmatchedPhraseHash();
                updateGrid(true);
            });
        }

        private void updateGrid(bool upd)
        {
            var result = _objLibraryFunction_Service.GetUnmatchedPhraseHash();

            MessengerInstance.Send<PropertyChangedMessage<List<PhraseHashPending>>>(new PropertyChangedMessage<List<PhraseHashPending>>(result, result, "Default List"));

            VisibleLoder = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleLoder");

            if (upd)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("Phrase hash updated successfully.");
                });
            }

        }

      

        public List<CommonDataGridColumn> GetListUnmatchedPhraseGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseID", ColBindingName = "PhraseID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldPhraseHash", ColBindingName = "OldPhraseHash", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewPhraseHash", ColBindingName = "NewPhraseHash", ColType = "Textbox" });
            return listColumnQsidDetail;
        }

        private bool CommandInsertUpdateRolePermissionCanExecute(object obj)
        {
            return true;
        }
        

      

      
        public void GetLibraryRoles()
        {
            listRoles = _objLibraryFunction_Service.GetLibraryRoles();
            NotifyPropertyChanged("listRoles");
        }


        public void GetPermissionTree()
        {
            listItems = new ObservableCollection<MapPermission_ViewModel>(_objLibraryFunction_Service.GetPermissionsByRole(SelectedRole.RoleID).Select(x => new MapPermission_ViewModel()
            {
                PermissionID = x.PermissionID,
                ParentPermissionID = x.ParentPermissionID,
                Name = x.Name,
                IsChecked = x.IsChecked,
                RoleID = x.RoleID,
                ChildCount = x.ChildCount,
                IsExpanded = false
            }).ToList());
            NotifyPropertyChanged("listItems");
        }
        private void CommandSavePermissionExecute(object obj)
        {
            List<int> listAssignedPermissionID = new();
            RecursiveFetchSelecedPermission(listItems.ToList(), ref listAssignedPermissionID);
            _objLibraryFunction_Service.InsertMapPermissionID(listAssignedPermissionID, SelectedRole.RoleID);
            _notifier.ShowSuccess("Permission update successfully!!");
            GetPermissionByUser();
        }
        public void GetPermissionByUser()
        {
            Engine.listAllPermissionDashboard = _objLibraryFunction_Service.GetPermissionByUser(Environment.UserName).ToList();
        }
        public void RecursiveFetchSelecedPermission(List<MapPermission_ViewModel> listPermission, ref List<int> assignedPermissionId)
        {
            foreach (var item in listPermission)
            {
                if (item.IsChecked)
                {
                    assignedPermissionId.Add(item.PermissionID);
                }
                if (!item.IsExpanded && item.ChildCount > 0)
                {
                    item.IsExpanded = true;
                }
                if (item.SubGeneric != null)
                {
                    RecursiveFetchSelecedPermission(item.SubGeneric.ToList(), ref assignedPermissionId);
                }
            }
        }


        #region Upload Chemical and Phrases
        public ICommand DownloadTemplateCommand { get; set; }
        public ICommand BtnChecKAndSave { get; set; }
        public ICommand UploadCommand { get; set; }
        public CommonFunctions objCommon;
        public string EnterValue { get; set; }
        public CommonDataGrid_ViewModel<IUploadChem> comonUploadChemical_VM { get; set; }
        public Visibility LoaderuploadChemical { get; set; } = Visibility.Collapsed;
        public ObservableCollection<string> ListChemType { get; set; } = new ObservableCollection<string>();
        private List<string> AddType()
        {
            List<string> listSelectedCombo = new List<string>() { "Chemical", "Phrase" };
            return listSelectedCombo;
        }
        private List<CommonDataGridColumn> GetListGridColumnUploadChemical()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Description", ColBindingName = "Description", ColType = "Textbox", ColumnWidth = "500" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_Count", ColBindingName = "No_of_Count", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }
        private ObservableCollection<IUploadChem> _lstUpload { get; set; } = new ObservableCollection<IUploadChem>();
        public ObservableCollection<IUploadChem> lstUpload
        {
            get { return _lstUpload; }
            set
            {
                if (_lstUpload != value)
                {
                    _lstUpload = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IUploadChem>>>(new PropertyChangedMessage<List<IUploadChem>>(null, _lstUpload.ToList(), "Default List"));
                }
            }
        }
        private string _selectedChemType { get; set; }
        public string selectedChemType
        {
            get => _selectedChemType;
            set
            {
                if (Equals(_selectedChemType, value))
                {
                    return;
                }

                _selectedChemType = value;
            }
        }
        private List<IpopValues> lstLanguageNotExistInLibrary = new List<IpopValues>();
        private List<IpopValues> lstChemicalAlreadyExistInLibrary = new List<IpopValues>();
        private List<IpopValues> lstPhraseAlreadyExistInLibrary = new List<IpopValues>();
        private List<IpopValues> lstChemicalNotExistInLibrary = new List<IpopValues>();
        private List<IpopValues> lstPhraseNotExistInLibrary = new List<IpopValues>();
        private string SelectedPath { get; set; }
        private void UploadCommandExecute(object obj)
        {
            if (selectedChemType == null || selectedChemType == string.Empty)
            {
                _notifier.ShowError("Choose Chemical/phrase first");
                return;
            }
            LoaderuploadChemical = Visibility.Visible;
            NotifyPropertyChanged("LoaderuploadChemical");
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = false;
            openFileDlg.Filter = "Access File (.mdb)|*.accdb;*.mdb"; // Optional file extensions
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                SelectedPath = openFileDlg.FileName;
                if (selectedChemType == "Chemical")
                {
                    System.Threading.Tasks.Task.Run(() =>
                    {
                        var lstDesc = objCommon.ChkChemical(objCommon.DbaseQueryReturnTable("Select * from Chemical_Upload", SelectedPath), "ChemicalName", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstChemicalAlreadyExistInLibrary, ref lstChemicalNotExistInLibrary);
                        lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                        NotifyPropertyChanged("comonUploadChemical_VM");
                        LoaderuploadChemical = Visibility.Collapsed;
                        NotifyPropertyChanged("LoaderuploadChemical");
                    });
                }
                else
                {
                    System.Threading.Tasks.Task.Run(() =>
                    {
                        var lstDesc = objCommon.ChkPhrases(objCommon.DbaseQueryReturnTable("Select * from Phrase_Upload", SelectedPath), "Phrase", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstPhraseAlreadyExistInLibrary, ref lstPhraseNotExistInLibrary);
                        lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                        NotifyPropertyChanged("comonUploadChemical_VM");
                        LoaderuploadChemical = Visibility.Collapsed;
                        NotifyPropertyChanged("LoaderuploadChemical");
                    });
                }
            }
            else
            {
                _notifier.ShowError("No file selected");
            }

        }
        public void DownloadTemplateCommandExecute(object obj)
        {
            if (selectedChemType == null || selectedChemType == string.Empty)
            {
                _notifier.ShowError("No Type Selected");
                return;
            }
            DataTable dt = new DataTable();
            string fileName = string.Empty;
            if (selectedChemType == "Chemical")
            {
                dt.Columns.Add("ChemicalName");
                dt.Columns.Add("LanguageID");
                fileName = "Chemical_Upload";
            }
            else
            {
                dt.Columns.Add("Phrase");
                dt.Columns.Add("LanguageID");
                fileName = "Phrase_Upload";
            }
            dt.TableName = fileName;
            ExportAccessFile(dt, fileName + ".mdb");
            _notifier.ShowSuccess("file successfully download at " + Engine.CommonAccessExportFolderPath + fileName + ".mdb");
        }
        public string ExportAccessFile(DataTable _objDataTable, string fileName)
        {
            try
            {
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Engine.CommonAccessExportFolderPath + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                // IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable, Engine.CommonAccessExportFolderPath + "/" + fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private IUploadChem _SelectedRowUpload { get; set; }
        public IUploadChem SelectedRowUpload
        {
            get => _SelectedRowUpload;
            set
            {
                if (Equals(_SelectedRowUpload, value))
                {
                    return;
                }

                _SelectedRowUpload = value;
            }
        }
        private void NotifyMe(PropertyChangedMessage<IUploadChem> obj)
        {
            SelectedRowUpload = obj.NewValue;
            Engine.UploadChemPhrase = false;
            PoupUploadChemPhrases objFrm = null;
            if (SelectedRowUpload != null)
            {
                switch (SelectedRowUpload.Description)
                {
                    case "Language Code not Exist in Library":
                        objFrm = new PoupUploadChemPhrases(SelectedRowUpload.Description, lstLanguageNotExistInLibrary);
                        break;
                    case "Chemical Name Exist in Library":
                        objFrm = new PoupUploadChemPhrases(SelectedRowUpload.Description, lstChemicalAlreadyExistInLibrary);
                        break;
                    case "Phrase Name Exist in Library":
                        objFrm = new PoupUploadChemPhrases(SelectedRowUpload.Description, lstPhraseAlreadyExistInLibrary);
                        break;
                    case "Chemical Name not Exist in Library":
                        objFrm = new PoupUploadChemPhrases(SelectedRowUpload.Description, lstChemicalNotExistInLibrary);
                        break;
                    case "Phrase Name not Exist in Library":
                        objFrm = new PoupUploadChemPhrases(SelectedRowUpload.Description, lstPhraseNotExistInLibrary);
                        break;
                }

                objFrm.ShowDialog();

                try
                {
                    if (Engine.UploadChemPhrase == true)
                    {
                        if (selectedChemType == "Chemical" && SelectedRowUpload.Description == "Chemical Name not Exist in Library")
                        {
                            var res = new List<IUploadChem>();
                            lstUpload = new ObservableCollection<IUploadChem>(res);
                            NotifyPropertyChanged("comonUploadChemical_VM");
                            LoaderuploadChemical = Visibility.Visible;
                            NotifyPropertyChanged("LoaderuploadChemical");
                            System.Threading.Tasks.Task.Run(() =>
                            {
                                var lstDesc = objCommon.ChkChemical(objCommon.DbaseQueryReturnTable("Select * from Chemical_Upload", SelectedPath), "ChemicalName", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstChemicalAlreadyExistInLibrary, ref lstChemicalNotExistInLibrary);
                                lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                                NotifyPropertyChanged("comonUploadChemical_VM");
                                LoaderuploadChemical = Visibility.Collapsed;
                                NotifyPropertyChanged("LoaderuploadChemical");
                            });
                        }

                        if (selectedChemType == "Phrase" && SelectedRowUpload.Description == "Phrase Name not Exist in Library")
                        {
                            var res = new List<IUploadChem>();
                            lstUpload = new ObservableCollection<IUploadChem>(res);
                            NotifyPropertyChanged("comonUploadChemical_VM");
                            LoaderuploadChemical = Visibility.Visible;
                            NotifyPropertyChanged("LoaderuploadChemical");
                            System.Threading.Tasks.Task.Run(() =>
                            {
                                var lstDesc = objCommon.ChkPhrases(objCommon.DbaseQueryReturnTable("Select * from Phrase_Upload", SelectedPath), "Phrase", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstPhraseAlreadyExistInLibrary, ref lstPhraseNotExistInLibrary);
                                lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                                NotifyPropertyChanged("comonUploadChemical_VM");
                                LoaderuploadChemical = Visibility.Collapsed;
                                NotifyPropertyChanged("LoaderuploadChemical");
                            });
                        }
                    }
                }
                catch
                {
                }
            }
        }
        public void BtnChecKAndSaveExecute(object obj)
        {
            if (selectedChemType == null || selectedChemType == string.Empty)
            {
                _notifier.ShowError("Choose Chemical/phrase first");
                return;
            }
            if (string.IsNullOrEmpty(EnterValue))
            {
                _notifier.ShowError("Enter Chemical/phrase to continue");
                return;
            }
            LoaderuploadChemical = Visibility.Visible;
            NotifyPropertyChanged("LoaderuploadChemical");
            DataTable lstFinal = new DataTable();
            lstFinal.Columns.Add(selectedChemType == "Chemical" ? "ChemicalName" : "Phrase");
            lstFinal.Columns.Add("LanguageID");
            lstFinal.Rows.Add(EnterValue, 1);
            if (selectedChemType == "Chemical")
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    var lstDesc = objCommon.ChkChemical(lstFinal, "ChemicalName", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstChemicalAlreadyExistInLibrary, ref lstChemicalNotExistInLibrary);
                    lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                    NotifyPropertyChanged("comonUploadChemical_VM");
                    LoaderuploadChemical = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderuploadChemical");
                });
            }
            else
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    var lstDesc = objCommon.ChkPhrases(lstFinal, "Phrase", "LanguageID", ref lstLanguageNotExistInLibrary, ref lstPhraseAlreadyExistInLibrary, ref lstPhraseNotExistInLibrary);
                    lstUpload = new ObservableCollection<IUploadChem>(lstDesc);
                    NotifyPropertyChanged("comonUploadChemical_VM");
                    LoaderuploadChemical = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderuploadChemical");
                });
            }


            //var StripCharacters = _objLibraryFunction_Service.GetStripChar();
            //string finalValNew = objCommon.RemoveWhitespaceCharacter(EnterValue, StripCharacters);
            //string chemName = System.Text.RegularExpressions.Regex.Replace(finalValNew, @"\s+", " ").Trim();

            //lstFinal.Add(new IpopValues { LanguageID = 1, Name = chemName, IsSelected = true });
            //var ID = 0;
            //if (selectedChemType == "Chemical")
            //{
            //    _objLibraryFunction_Service.saveChemicals(lstFinal);
            //    ID = _objLibraryFunction_Service.GetChemicalID(chemName);
            //}
            //if (selectedChemType == "Phrase")
            //{
            //    _objLibraryFunction_Service.savePhrases(lstFinal);
            //    ID = _objLibraryFunction_Service.GetPhraseID(chemName);
            //}
            //_notifier.ShowSuccess("New " + selectedChemType + "ID is- " + ID);
            //LoaderuploadChemical = Visibility.Collapsed;
            //NotifyPropertyChanged("LoaderuploadChemical");
        }
        #endregion
    }
}
