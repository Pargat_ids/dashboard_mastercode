﻿using AccessVersionControlSystem.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;


namespace AccessVersionControlSystem.ViewModel
{
    public class DeleteQsidUserControl_ViewModel : Base_ViewModel
    {
        private CommonFunctions objCommonFunc;
        private string stringSqlCon;

        public DeleteQsidUserControl_ViewModel()
        {
            objCommonFunc = new CommonFunctions();
            stringSqlCon = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                Init();
            }
        }

        #region Property Member 

        private string _qsidSearch { get; set; }
        public string QsidSearch
        {
            get { return _qsidSearch; }
            set
            {
                if (value != _qsidSearch)
                {
                    _qsidSearch = value;
                    if (!string.IsNullOrEmpty(_qsidSearch))
                    {
                        Task.Run(() => BindDDLQsidList(_qsidSearch));
                    }

                }

            }
        }
        public ObservableCollection<LibraryInformation_QSID_VM> _listQsidDetail { get; set; } = new ObservableCollection<LibraryInformation_QSID_VM>();
        public ObservableCollection<LibraryInformation_QSID_VM> ListQsidDetail
        {
            get { return _listQsidDetail; }
            set
            {
                if (_listQsidDetail != value)
                {
                    _listQsidDetail = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<LibraryInformation_QSID_VM>>>(new PropertyChangedMessage<List<LibraryInformation_QSID_VM>>(_listQsidDetail.ToList(), _listQsidDetail.ToList(), "Default List"));
                }
            }
        }
        public ObservableCollection<QSID_VM> ListQsid { get; set; } = new ObservableCollection<QSID_VM>();
        public List<Library_QSIDs> ListAllDefaultQsid { get; set; } = new List<Library_QSIDs>();
        private ObservableCollection<DeletedQsidHistory> _listDeletedQsidHistory { get; set; }
        public ObservableCollection<DeletedQsidHistory> ListDeletedQsidHistory
        {
            get { return _listDeletedQsidHistory; }
            set
            {
                if (_listDeletedQsidHistory != value)
                {
                    _listDeletedQsidHistory = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<DeletedQsidHistory>>>(new PropertyChangedMessage<List<DeletedQsidHistory>>(_listDeletedQsidHistory.ToList(), _listDeletedQsidHistory.ToList(), "Default List"));
                }
            }
        }

        public QSID_VM _selectedQsid { get; set; }
        public QSID_VM SelectedQsid
        {
            get { return _selectedQsid; }
            set
            {
                if (_selectedQsid != value)
                {
                    _selectedQsid = value;
                    BindQsidDetailGrid();
                }
            }
        }
        private static string _WorkOrderNo { get; set; }
        public static string WorkOrderNo
        {
            get { return _WorkOrderNo; }
            set
            {
                if (value != _WorkOrderNo)
                {
                    _WorkOrderNo = value;

                }
            }
        }
        private static string _EnterComment { get; set; }
        public static string EnterComment
        {
            get { return _EnterComment; }
            set
            {
                if (value != _EnterComment)
                {
                    _EnterComment = value;

                }
            }
        }

        public ICommand CommandDeleteQSID { get; set; }
        //public ICommand CommandOneTimeDeleteQSID { get; set; }
        public CommonDataGrid_ViewModel<LibraryInformation_QSID_VM> commonDGInformationQsid { get; set; }
        public CommonDataGrid_ViewModel<DeletedQsidHistory> commonDGDeleteQsidHistory { get; set; }

        #endregion

        #region Member  Function

        public void Init()
        {
            commonDGInformationQsid = new CommonDataGrid_ViewModel<LibraryInformation_QSID_VM>(GetListDetailGridCol(), "QSID", "List Detail Grid :");
            commonDGDeleteQsidHistory = new CommonDataGrid_ViewModel<DeletedQsidHistory>(GetDeletedQsidHisGridCol(), "Qsid", "Deleted List History :");
            ListAllDefaultQsid = _objLibraryFunction_Service.GetAvailableLibraryQsidList();
            BindDDLQsidList("");
            BindDeletedQsidHistory();
            CommandDeleteQSID = new RelayCommand(CommandDeleteQSIDExecute, CommandDeleteQSIDCanExecute);
            //CommandOneTimeDeleteQSID = new RelayCommand(CommandOneTimeDeleteQSIDExecute, CommandDeleteQSIDCanExecute);
            MessengerInstance.Register<NotificationMessageAction<DeletedQsidHistory>>(this, CallBackBindListDetailGridExecute);

        }
        private void CallBackBindListDetailGridExecute(NotificationMessageAction<DeletedQsidHistory> notificationMessageAction)
        {
            BindDeletedQsidHistory();
        }
        public List<CommonDataGridColumn> GetDeletedQsidHisGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Deleted On", ColBindingName = "DeletedOn", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Deleted By", ColBindingName = "DeletedBy", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "WorkOrder No", ColBindingName = "WorkOrder", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comment", ColBindingName = "Comment", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetListDetailGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "QSID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Field Name", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Short Name", ColBindingName = "ShortName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Publication Date", ColBindingName = "PublicationDate", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Module", ColBindingName = "Module", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private bool CommandDeleteQSIDCanExecute(object o)
        {
            if (SelectedQsid == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //private void CommandOneTimeDeleteQSIDExecute(object o)
        //{
        //    var logErrors = new DataTable();
        //    logErrors.Columns.Add("QSID");
        //    logErrors.Columns.Add("FilePath");
        //    logErrors.Columns.Add("Error");
        //    using (var context = new CRAModel())
        //    {
        //        var result = new ObservableCollection<DeletedQsidHistory>(_objLibraryFunction_Service.GetListDeletedQSIDs());
        //        for (int i = 0; i < result.Count; i++)
        //        {
        //            var qsid = result[i].Qsid;
        //            var prodTime = result[i].DeletedOn;
        //            var comment = result[i].Comment;
        //            var wrord = string.IsNullOrEmpty(result[i].WorkOrder) ? (int?)null : Convert.ToInt32(result[i].WorkOrder);
        //            var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == qsid).Select(y => y.QSID_ID).FirstOrDefault();
        //            _objAccessVersion_Service.CheckIn_FunctionalityDharmesh(qsidid, prodTime, comment, wrord, ref logErrors);

        //        }
        //    }
        //}
        private void CommandDeleteQSIDExecute(object o)
        {
            if (EnterComment.Trim() == string.Empty)
            {
                _notifier.ShowError("Comment is Mandatory, Enter it before deleting Qsids");
                return;
            }
            var result = MessageBox.Show("Are you sure want to delete the selected QSID?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                var chkExist = objCommonFunc.DbaseQueryReturnTableSql("Select top 1 qsid_id from Log_DeletedQSID_Comments where qsid_id =" + SelectedQsid.QSID_ID, stringSqlCon);
                if (chkExist.Rows.Count > 0)
                {
                    _notifier.ShowError("QSID already deleted, can't delete");
                    return;
                }
                else
                {
                    _objLibraryFunction_Service.DeleteQsid(SelectedQsid.QSID_ID, Engine.CurrentUserSessionID, WorkOrderNo, EnterComment);
                    _notifier.ShowSuccess("Selected qsid successfully deleted.");
                    Task.Run(() => BindDDLQsidList(QsidSearch));
                    BindDeletedQsidHistory();
                }

            }
        }
        public void BindDDLQsidList(string qsid)
        {
            if (!string.IsNullOrEmpty(qsid))
            {
                ListQsid = new ObservableCollection<QSID_VM>(ListAllDefaultQsid.FindAll(x => x.QSID.ToLower().Contains(qsid.ToLower())).GroupBy(x => new { x.QSID, x.QSID_ID }).Select(x => new QSID_VM { QSID = x.Key.QSID, QSID_ID = x.Key.QSID_ID }));
                NotifyPropertyChanged("ListQsid");
            }
        }
        public void BindQsidDetailGrid()
        {
            if (SelectedQsid != null)
            {
                ListQsidDetail = new ObservableCollection<LibraryInformation_QSID_VM>(_objLibraryFunction_Service.GetQSIDListDetail("", SelectedQsid.QSID_ID));
                NotifyPropertyChanged("ListQsidDetail");
            }
            else
            {
                ListQsidDetail = new ObservableCollection<LibraryInformation_QSID_VM>();
                NotifyPropertyChanged("ListQsidDetail");
            }
        }

        public void BindDeletedQsidHistory()
        {
            ListDeletedQsidHistory = new ObservableCollection<DeletedQsidHistory>(_objLibraryFunction_Service.GetListDeletedQSIDs());
            NotifyPropertyChanged("ListDeletedQsidHistory");
        }
        #endregion

    }
}
