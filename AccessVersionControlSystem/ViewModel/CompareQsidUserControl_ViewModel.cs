﻿using AccessVersionControlSystem.Library;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ViewModel
{
    public class CompareQsidUserControl_ViewModel : Base_ViewModel
    {

        public CompareQsidUserControl_ViewModel()
        {
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                Init();
            }
        }

        public string IsDisplayQSIDClearBtn { get; set; }
        public bool DisplayQSIDCheckBoxColumn { get; set; }
        public string CheckQSIDVisibility { get; set; }
        public string IsDisplayCasFilter { get; set; }
        public bool IsDisplayQSIDCheckBoxColumn
        {
            get
            {
                return DisplayQSIDCheckBoxColumn;
            }
            set
            {
                DisplayQSIDCheckBoxColumn = value;
                if (DisplayQSIDCheckBoxColumn)
                {
                    CheckQSIDVisibility = "Visible";
                }
                else
                {
                    CheckQSIDVisibility = "Hidden";
                }
                NotifyPropertyChanged("CheckQSIDVisibility");
            }
        }
        public string VisibilityCompareQSID_Loader { get; set; }

        public string VisibilityListDetail_Loader { get; set; }
        public bool _IsCheckedFilterCas { get; set; }
        public bool IsCheckedFilterCas
        {
            get
            {
                return _IsCheckedFilterCas;
            }
            set
            {
                _IsCheckedFilterCas = value;
                if (_IsCheckedFilterCas)
                {
                    IsDisplayCasFilter = "Visible";
                }
                else
                { IsDisplayCasFilter = "Hidden"; }
                NotifyPropertyChanged("IsDisplayCasFilter");
            }
        }

        public ObservableCollection<ListCountries_ViewModel> ListCountry { get; set; }
        public ObservableCollection<ListModule_ViewModel> ListModule { get; set; }
        public ObservableCollection<ListTopic_ViewModel> ListTopic { get; set; }

        private ObservableCollection<ListQsidHitCount_VM> _listComparedHitsCount { get; set; }
        public ObservableCollection<ListQsidHitCount_VM> ListComparedHitsCount
        {
            get { return _listComparedHitsCount; }
            set
            {
                if (value != _listComparedHitsCount)
                {
                    _listComparedHitsCount = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListQsidHitCount_VM>>>(new PropertyChangedMessage<List<ListQsidHitCount_VM>>(_listComparedHitsCount.ToList(), _listComparedHitsCount.ToList(), "Default List"));
                }
            }
        }

        private ObservableCollection<DataGridColumn> _columnCollection = new ObservableCollection<DataGridColumn>();
        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get
            {
                return this._columnCollection;
            }
            set
            {
                _columnCollection = value;
                NotifyPropertyChanged("ColumnCollection");
            }
        }

        public DataTable Collection_CasID { get; set; } = new DataTable();
        public List<object> ResponseCollection_CasID { get; set; }

        public string _searchCas { get; set; }
        public string SearchCas
        {
            get { return _searchCas; }
            set
            {
                if (value != _searchCas)
                {
                    _searchCas = value;
                    CommandSearchCasExecuted();
                }
            }
        }

        public bool _IsCasExistAllQSID { get; set; }
        public bool IsCasExistAllQSID
        {
            get
            {
                return _IsCasExistAllQSID;
            }
            set
            {
                if (_IsCasExistAllQSID != value)
                {
                    _IsCasExistAllQSID = value;
                    CommandSearchCasExecuted();
                }
            }
        }
        private ObservableCollection<CompareQsidListDetail> _listCompareQSID_VM { get; set; } = new ObservableCollection<CompareQsidListDetail>();
        public ObservableCollection<CompareQsidListDetail> ListCompareQSID_VM { get { return _listCompareQSID_VM; } set {
                if (_listCompareQSID_VM != value) {
                    _listCompareQSID_VM = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<CompareQsidListDetail>>>(new PropertyChangedMessage<List<CompareQsidListDetail>>(_listCompareQSID_VM.ToList(), _listCompareQSID_VM.ToList(), "Default List"));
                }
            } } 
        public ObservableCollection<object> ListCasIDMatrix { get; set; } = new ObservableCollection<object>();

        public ICommand CommandClearSelectedQSID { get; private set; }

        public ICommand CommandCompareQSIDList { get; private set; }
        public ICommand CommandClearSearchCas { get; private set; }


        public CommonDataGrid_ViewModel<ListQsidHitCount_VM> commonDGDirectGenericHit { get; set; }
        public CommonDataGrid_ViewModel<object> commonDGMatrix { get; set; }
        public CommonDataGrid_ViewModel<CompareQsidListDetail> commonQsidListDetail { get; set; }
        public void Init()
        {
            IsDisplayQSIDCheckBoxColumn = false;
            CheckQSIDVisibility = "Hidden";
            VisibilityListDetail_Loader = "Hidden";
            IsDisplayCasFilter = "Hidden";
            VisibilityCompareQSID_Loader = "Hidden";
            GetRefreshListDetailCommonControl();
            BindListFilter_ColumnsData();
            Task.Run(() => BindCompareQsidGrid_ListDetail());
            CommandClearSelectedQSID = new RelayCommand(CommandClearSelectedQSIDExecuted, CommandClearSelectedQSIDCanExecute);
            CommandCompareQSIDList = new RelayCommand(CommandCompareQSIDListExecute, CommandCompareQSIDListCanExecute);
            CommandGenerateAccessCasResult = new RelayCommand(CommandGenerateAccessCasResultExecuted, CommandGenerateAccessCasResultCanExecute);
            CommandGenerateExcelCasResult = new RelayCommand(CommandGenerateExcelCasResultExecuted, CommandGenerateAccessCasResultCanExecute);
            CommandClearSearchCas = new RelayCommand(CommandClearSearchCasExecuted, CommandSearchCasCanExecute);
        }
        public void GetRefreshListDetailCommonControl()
        {
            commonDGDirectGenericHit = new CommonDataGrid_ViewModel<ListQsidHitCount_VM>(GetListDetailGridCol(), "Qsid", "List Direct/Generic Hit :");
            NotifyPropertyChanged("commonDGDirectGenericHit");

            commonQsidListDetail = new CommonDataGrid_ViewModel<CompareQsidListDetail>(GetCompareQsidListDetailGridCol(), "Qsid", "List Detail For Compare QSID(s) :");
            NotifyPropertyChanged("commonQsidListDetail");

        }

        private List<CommonDataGridColumn> GetCompareQsidListDetailGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Check Qsid", ColBindingName = "IsCheckedForCompare", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "QSID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Field Name", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Short Name", ColBindingName = "ShortName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Publication Date", ColBindingName = "ListPublicationDate", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Module", ColBindingName = "Module", ColType = "Combobox" });
            return listColumnQsidDetail;
        }

        public List<CommonDataGridColumn> GetListDetailGridCol()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Direct Hit Count", ColBindingName = "DirectHit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Generic Hit Count", ColBindingName = "GenericHit", ColType = "Textbox" });

            return listColumnQsidDetail;
        }

        private bool CommandClearSelectedQSIDCanExecute(object obj)
        {
            if (ListCompareQSID_VM.Any(x => x.IsCheckedForCompare))
            {
                IsDisplayQSIDClearBtn = "Visible";
                NotifyPropertyChanged("IsDisplayQSIDClearBtn");
                return true;
            }
            else
            {
                IsDisplayQSIDClearBtn = "Hidden";
                NotifyPropertyChanged("IsDisplayQSIDClearBtn");
                return false;
            }

        }
        public void CommandClearSelectedQSIDExecuted(object obj)
        {
            var listCompareQsid = ListCompareQSID_VM.ToList();
            listCompareQsid.ForEach(x => x.IsCheckedForCompare = false);
            MessengerInstance.Send<PropertyChangedMessage<List<CompareQsidListDetail>>>(new PropertyChangedMessage<List<CompareQsidListDetail>>(listCompareQsid.ToList(), listCompareQsid.ToList(), "Default List"));
        }
        public void BindListFilter_ColumnsData()
        {
            ListCountry = new ObservableCollection<ListCountries_ViewModel>(Engine._listCountry);
            ListModule = new ObservableCollection<ListModule_ViewModel>(Engine._listModule);
            ListTopic = new ObservableCollection<ListTopic_ViewModel>(Engine._listTopic);
        }
        public void BindCompareQsidGrid_ListDetail()
        {
            VisibilityListDetail_Loader = "Visible";
            NotifyPropertyChanged("VisibilityListDetail_Loader");
            Task.Run(() => FetchListDetailQsidInfo());

        }

        private void FetchListDetailQsidInfo()
        {
            var listQsidDetail = _objLibraryFunction_Service.GetQSIDListDetail("QC");
            var list_Library_Information_QSID_VM = AccessVersionControlSystem.Common.CommonFunctions.BindCompareQsidModel(listQsidDetail);
            ListCompareQSID_VM = new ObservableCollection<CompareQsidListDetail>(list_Library_Information_QSID_VM);
            this.NotifyPropertyChanged("ListCompareQSID_VM");
            VisibilityListDetail_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityListDetail_Loader");
        }

        public void BindDataGridCas_ComparedByQSID(List<object> _collectionCasid)
        {
            ListCasIDMatrix = new ObservableCollection<object>(_collectionCasid);
            NotifyPropertyChanged("ListCasIDMatrix");

            RefreshCommonDGMatrix(_collectionCasid);
            MessengerInstance.Send<PropertyChangedMessage<List<object>>>(new PropertyChangedMessage<List<object>>(_collectionCasid, _collectionCasid, "Default List"));
            //ColumnCollection = new ObservableCollection<DataGridColumn>();
            //if (_collectionCasid.Count > 0)
            //{
            //    var propertyInfo = _collectionCasid[0].GetType().GetProperties();
            //    foreach (var itemCas in propertyInfo)
            //    {
            //        if (itemCas.Name.ToLower() == "cas")
            //        {
            //            //System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(itemCas.Name);
            //            //DataGridTextColumn colText = new DataGridTextColumn();
            //            //colText.Header = itemCas.Name;
            //            //colText.Binding = bindings;
            //            //FrameworkElementFactory textblock_Dynamic = new FrameworkElementFactory(typeof(TextBlock));
            //            //textblock_Dynamic.Name = "text";
            //            //textblock_Dynamic.SetBinding(TextBlock.TextProperty, bindings);
            //            //textblock_Dynamic.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            //            //textblock_Dynamic.SetValue(TextBlock.NameProperty, "textblock");
            //            //ColumnCollection.Add(colText);
            //        }
            //        else
            //        {
            //            //System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(itemCas.Name);
            //            //DataGridTemplateColumn colText = new DataGridTemplateColumn();
            //            //colText.Header = itemCas.Name;
            //            //FrameworkElementFactory textblock_Dynamic = new FrameworkElementFactory(typeof(Image));
            //            //bindings.Mode = System.Windows.Data.BindingMode.TwoWay;
            //            //textblock_Dynamic.SetValue(Image.SourceProperty, bindings);
            //            //DataTemplate cellTemplate1 = new DataTemplate();
            //            //cellTemplate1.VisualTree = textblock_Dynamic;
            //            //colText.CellTemplate = cellTemplate1;
            //            //ColumnCollection.Add(colText);
            //        }

            //    }
            //}
        }
        public void RefreshCommonDGMatrix(List<object> _collectionCasid)
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            if (_collectionCasid != null && _collectionCasid.Count!=0)
            {
                var propertyInfo = _collectionCasid[0].GetType().GetProperties();
                foreach (var itemCas in propertyInfo)
                {
                    if (itemCas.Name.ToLower() == "cas")
                    {
                        listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = itemCas.Name, ColBindingName = itemCas.Name, ColType = "Textbox" });
                    }
                    else
                    {
                        listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = itemCas.Name, ColBindingName = itemCas.Name, ColType = "Image",ColumnWidth="200" });
                    }
                }
            }
            commonDGMatrix = new CommonDataGrid_ViewModel<object>(listColumnQsidDetail, "Cas", "Compare Qsid Matrix :");
            NotifyPropertyChanged("commonDGMatrix");
        }

        private bool CommandGenerateAccessCasResultCanExecute(object obj)
        {
            return true;
        }
        public void CommandGenerateAccessCasResultExecuted(object obj)
        {
            try
            {
                string fileName = "Cas_Comparsion_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
                var _selectedQsid = ListCompareQSID_VM.Where(x => x.IsCheckedForCompare == true).Select(x => x.QSID_ID).ToList();
                Collection_CasID = _objLibraryFunction_Service.GetParentChildCasby_DataTable(_selectedQsid, true);
                AccessVersionControlSystem.Common.CommonFunctions.ExportAccessFile(Collection_CasID, fileName, _objAccessVersion_Service);
                _notifier.ShowSuccess("Access File Export Successfully on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            catch (Exception ex)
            {
                _notifier.ShowError(ex.Message);
            }
        }
        public void CommandGenerateExcelCasResultExecuted(object obj)
        {
            try
            {
                string fileName = "Cas_Comparsion_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
                var _selectedQsid = ListCompareQSID_VM.Where(x => x.IsCheckedForCompare == true).Select(x => x.QSID_ID).ToList();
                Collection_CasID = _objLibraryFunction_Service.GetParentChildCasby_DataTable(_selectedQsid, true);
                AccessVersionControlSystem.Common.CommonFunctions.ExportExcelFile(Collection_CasID, Engine.CommonAccessExportFolderPath + fileName);
                _notifier.ShowSuccess("Excel File Export Successfully on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            catch (Exception ex)
            {
                _notifier.ShowError(ex.Message);

            }
        }

        private bool CommandCompareQSIDListCanExecute(object obj)
        {
            if (ListCompareQSID_VM != null)
            {
                if (ListCompareQSID_VM.Count(x => x.IsCheckedForCompare == true) > 1 && ListCompareQSID_VM.Count(x => x.IsCheckedForCompare == true) < 10)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        private void CommandCompareQSIDListExecute(object obj)
        {
            VisibilityCompareQSID_Loader = "Visible";
            NotifyPropertyChanged("VisibilityCompareQSID_Loader");
            Task.Run(() => FetchCompareQsidList());
        }

        private void FetchCompareQsidList()
        {

            var _selectedQsid = ListCompareQSID_VM.Where(x => x.IsCheckedForCompare == true).Select(x => x.QSID_ID).ToList();

            var dictonaryComparedQsidRes = _objLibraryFunction_Service.GetParentChildCasby_QsidList(_selectedQsid);

            ResponseCollection_CasID = dictonaryComparedQsidRes.Values.FirstOrDefault();
            ListComparedHitsCount = new ObservableCollection<ListQsidHitCount_VM>(dictonaryComparedQsidRes.Keys.FirstOrDefault());
            NotifyPropertyChanged("ListComparedHitsCount");
            if (ResponseCollection_CasID.Count > 0)
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    BindDataGridCas_ComparedByQSID(ResponseCollection_CasID);
                });
            }
            VisibilityCompareQSID_Loader = "Hidden";
            NotifyPropertyChanged("VisibilityCompareQSID_Loader");

        }

        private void CommandClearSearchCasExecuted(object obj)
        {
            this.SearchCas = string.Empty;
            NotifyPropertyChanged("SearchCas");

        }

        private bool CommandSearchCasCanExecute(object obj)
        {
            if (!String.IsNullOrEmpty(SearchCas))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandSearchCasExecuted()
        {

            var _listFilterCas = ResponseCollection_CasID.Where(x => x.GetType().GetProperty("Cas").GetValue(x).ToString().Contains(SearchCas != null ? SearchCas : "")).ToList();
            var listFilterIsCas = new List<object>();
            if (IsCasExistAllQSID)
            {
                var propertyInfo = ResponseCollection_CasID[0].GetType().GetProperties().Where(x => x.Name != "Cas");

                foreach (var item in _listFilterCas)
                {
                    bool IsExistCas = true;
                    foreach (var itemProp in propertyInfo)
                    {
                        if (string.IsNullOrEmpty((string)itemProp.GetValue(item)))
                        {
                            IsExistCas = false;
                            goto notExistStatment;
                        }
                    }
                notExistStatment:
                    if (IsExistCas)
                    {
                        listFilterIsCas.Add(item);
                    }
                }
            }
            else
            {
                listFilterIsCas.AddRange(_listFilterCas);
            }
            BindDataGridCas_ComparedByQSID(listFilterIsCas);

        }


    }
}
