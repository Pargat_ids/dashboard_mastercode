﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Libraries.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using Libraries.CustomUserControl;
using AccessVersionControlSystem.Library;

namespace AccessVersionControlSystem.ViewModel
{
    public class MapListRelationShip_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapRelation> comonLibRelation_VM { get; set; }
        public MapListRelationShip_VM()
        {
            objCommonFunc = new CommonFunctions();
            stringSqlCon = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            comonLibRelation_VM = new CommonDataGrid_ViewModel<MapRelation>(GetListGridColumnLibRelation(), "Map RelationShip", "Map RelationShip");
            MessengerInstance.Register<PropertyChangedMessage<string>>(this,typeof(MapListRelationShip_VM), refreshListRel);
            BindLibRelation();
        }

        private void refreshListRel(PropertyChangedMessage<string> obj)
        {
            if (obj.NewValue == "ListRel")
            {
                BindLibRelation();
            }
        }

        private CommonFunctions objCommonFunc;
        private string stringSqlCon;
        public Visibility VisibilityLibRelationDashboard_Loader { get; set; }

        private ObservableCollection<MapRelation> _lstLibRelation { get; set; } = new ObservableCollection<MapRelation>();
        public ObservableCollection<MapRelation> lstLibRelation
        {
            get { return _lstLibRelation; }
            set
            {
                if (_lstLibRelation != value)
                {
                    _lstLibRelation = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapRelation>>>(new PropertyChangedMessage<List<MapRelation>>(null, _lstLibRelation.ToList(), "Default List"));
                }
            }
        }

        public void BindLibRelation()
        {
            VisibilityLibRelationDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibRelationDashboard_Loader");
            //Task.Run(() =>
            //{
                var SAPLibRelationTable = objCommonFunc.DbaseQueryReturnSqlList<MapRelation>(
                "select c.QSID_ID as ParentQsid_ID, c.QSID as ParentQsid, " +
                " d.QSID_ID as RelatedQsid_ID, d.QSID as RelatedQsid, b.ListRelationship_Name, b.ListRelationship_Description from Map_ListRelationships a, " +
                " Library_ListRelationships b, Library_QSIDs c, Library_QSIDs d " +
                " where a.ListRelationshipID = b.ListRelationshipID and a.ParentList_QSID_ID = c.QSID_ID " +
                " and a.RelatedList_QSID_ID = d.QSID_ID", stringSqlCon);
                lstLibRelation = new ObservableCollection<MapRelation>(SAPLibRelationTable);
            //});
            NotifyPropertyChanged("comonLibRelation_VM");
            VisibilityLibRelationDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibRelationDashboard_Loader");
        }
        
        private List<CommonDataGridColumn> GetListGridColumnLibRelation()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();

            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentQsid", ColBindingName = "ParentQsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RelatedQsid", ColBindingName = "RelatedQsid", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListRelationship_Name", ColBindingName = "ListRelationship_Name", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListRelationship_Description", ColBindingName = "ListRelationship_Description", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapRelation
    {
        public int ParentQsid_ID { get; set; }
        public string ParentQsid { get; set; }
        public int RelatedQsid_ID { get; set; }
        public string RelatedQsid { get; set; }
        public string ListRelationship_Name { get; set; }
        public string ListRelationship_Description { get; set; }
    }

}
