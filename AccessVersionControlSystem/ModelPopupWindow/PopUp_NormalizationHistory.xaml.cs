﻿using AccessVersionControlSystem.ViewModel;
using System.Windows;
using CRA_DataAccess.ViewModel;
using CRA_DataAccess;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Business.BusinessService;
using System.Windows.Controls;
using VersionControlSystem.Model.ApplicationEngine;
using System.Linq;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PopUp_NormalizationHistory.xaml
    /// </summary>
    public partial class PopUp_NormalizationHistory : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public PopUp_NormalizationHistory(Log_Session_NormalizeHistory_VM session_VM, string columnName, int qsid_ID)
        {
            InitializeComponent();
            var vm = new Popup_Normaliztion_ViewModel(this, session_VM, columnName, qsid_ID);
            DataContext = vm;
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab2.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab3.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab5.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
        }
    }
}
