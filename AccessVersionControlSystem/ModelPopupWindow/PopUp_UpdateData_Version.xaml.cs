﻿using AccessVersionControlSystem.ViewModel;
using CRA_DataAccess;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PopUp_UpdateData_Version.xaml
    /// </summary>
    public partial class PopUp_UpdateData_Version : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public PopUp_UpdateData_Version(Access_QSID_Data_VM SelectedQSID_Version, string popUpFlag, bool IsEnable)
        {
            var vm = new Popup_VersionUpdate_ViewModel(this, SelectedQSID_Version, popUpFlag, IsEnable);
            DataContext = vm;
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
        }

        private void TextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[a-zA-Z]+$");
            if (regex.IsMatch(e.Text))
                e.Handled = true;
            base.OnPreviewTextInput(e);
        }
    }

}
