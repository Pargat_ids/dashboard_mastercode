﻿using AccessVersionControlSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CRA_DataAccess.ViewModel;
using CRA_DataAccess;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Model.ApplicationEngine;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PopUp_ListChangesReport.xaml
    /// </summary>
    public partial class PopUp_ListChangesReport : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public PopUp_ListChangesReport(int qsid_ID, DateTime? ProductDateMin, DateTime? ProductDateMax, string GridType, string ActiveTab)
        {
            InitializeComponent();
            var vm = new Popup_ListChangesReport_ViewModel( qsid_ID, ProductDateMin, ProductDateMax,GridType,ActiveTab);
            DataContext = vm;
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab3.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab2.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
        }
     
    }
}
