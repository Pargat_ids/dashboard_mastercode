﻿using CRA_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PopUp_GenericsDetail.xaml
    /// </summary>
    public partial class PopUp_GenericsDetail : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public PopUp_GenericsDetail()
        {
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
    }
}
