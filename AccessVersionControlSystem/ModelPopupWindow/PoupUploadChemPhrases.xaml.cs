﻿using AccessVersionControlSystem.ViewModel;
using CRA_DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VersionControlSystem.Model.DataModel;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PoupUploadChemPhrases.xaml
    /// </summary>
    public partial class PoupUploadChemPhrases : Window
    {
        public PoupUploadChemPhrases(string _messageHeader, List<IpopValues> lst)
        {
            InitializeComponent();
            var vm = new PoupUploadChemPhrases_VM(_messageHeader, lst);
            DataContext = vm;
        }
    }
}
