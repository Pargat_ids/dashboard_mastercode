﻿using AccessVersionControlSystem.ViewModel;
using System.Windows;

namespace AccessVersionControlSystem.ModelPopupWindow
{
    /// <summary>
    /// Interaction logic for PopUp_Message.xaml
    /// </summary>
    public partial class PopUp_Message : Window
    {
        public PopUp_Message(string _messageHeader, string _messageContent)
        {
            InitializeComponent();
            var vm = new Popup_Message_ViewModel(this, _messageHeader, _messageContent);
            DataContext = vm;
        }
    }
}
