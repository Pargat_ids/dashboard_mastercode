﻿using AccessVersionControlSystem.ViewModel;
using DocumentFormat.OpenXml.Packaging;
using EntityClass;
using Org.BouncyCastle.Crypto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Business.Common;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace AccessVersionControlSystem.Common
{
    public static class CommonFunctions
    {

        public static DataTable ConvertToDataTable<T>(IList<T> data, string[] arryColExcept = null)
        {
            DataTable table = new DataTable();
            if (typeof(T).Name == "Object")
            {
                var properties = data[0].GetType().GetProperties();
                foreach (var prop in properties)
                {
                    if (arryColExcept != null)
                    {
                        if (!arryColExcept.Contains(prop.Name))
                            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    }
                    else
                    {
                        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    }
                }
                foreach (var item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (var prop in properties)
                    {
                        if (arryColExcept != null)
                        {
                            if (!arryColExcept.Contains(prop.Name))
                                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        else
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            else
            {

                PropertyDescriptorCollection properties =
                   TypeDescriptor.GetProperties(typeof(T));


                foreach (PropertyDescriptor prop in properties)
                {
                    if (arryColExcept != null)
                    {
                        if (!arryColExcept.Contains(prop.Name))
                            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    }
                    else
                    {
                        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                    }
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        if (arryColExcept != null)
                        {
                            if (!arryColExcept.Contains(prop.Name))
                                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        else
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }

                    }
                    table.Rows.Add(row);
                }
            }
            return table;

        }

        public static DataTable ConvertDataTableFromListDetail(IList<Library_QSID_Information_VM> listDetails)
        {
            DataTable table = new DataTable();
            PropertyDescriptorCollection properties =
                  TypeDescriptor.GetProperties(typeof(Library_QSID_Information_VM));
            string[] arryColExcept = new string[] { "Country", "Module", "Topic" };

            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name.ToLower().Contains("ischeckedforcompare"))
                {
                    continue;
                }

                //if (!arryColExcept.Contains(prop.Name))
                //{
                //    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                //}
                //else
                //{
                table.Columns.Add(prop.Name, typeof(string));
                //  }
            }
            foreach (Library_QSID_Information_VM item in listDetails)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name.ToLower().Contains("ischeckedforcompare"))
                    {
                        continue;
                    }
                    if (!arryColExcept.Contains(prop.Name))
                    {
                        row[prop.Name] = prop.GetValue(item) != null ? prop.GetValue(item).ToString() : "";
                    }
                    else
                    {
                        var obj = prop.GetValue(item);
                        row[prop.Name] = obj != null ? (obj.GetType().GetProperty("Name").GetValue(obj).ToString()) : "";
                    }

                }
                table.Rows.Add(row);
            }
            return table;
        }

        public static string ExportAccessFile(DataTable _objDataTable, string fileName, IAccess_Version_BL _objAccessVersion_Service)
        {
            try
            {
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Engine.CommonAccessExportFolderPath + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                // IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable, Engine.CommonAccessExportFolderPath + "/" + fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string ExportExcelFile(DataTable table, string destination)
        {
            try
            {
                var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook);
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();
                    //foreach (System.Data.DataTable table in ds.Tables)
                    //{
                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }
                    //}
                }
                workbook.WorkbookPart.Workbook.Save();
                workbook.Close();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static List<LogNormalizedDeleteQsidField> CovertDeletedQsidBusinessToViewModel(List<ListChangesReportQsid_DeleteField_VM> lists)
        {
            return lists.Select(x => new LogNormalizedDeleteQsidField { FieldDisplayHeader = x.FieldDisplayHeader, FieldExplanation = x.FieldExplanation, FieldName = x.FieldName, Qsid = x.Qsid }).ToList();
        }

        public static List<LogNormalizedAddQsidField> CovertAddQsidBusinessToViewModel(List<ListChangesReportQsid_Field_VM> lists)
        {
            return lists.Select(x => new LogNormalizedAddQsidField { FieldDisplayHeader = x.FieldDisplayHeader, FieldExplanation = x.FieldExplanation, FieldName = x.FieldName, Qsid = x.Qsid }).ToList();
        }
        public static List<ListChangesReportDeleteFieldPopUP> CovertDeletedQsidBusinessToViewModelProductDate(List<ListChangesReportQsid_DeleteField_VM> lists)
        {
            return lists.Select(x => new ListChangesReportDeleteFieldPopUP { FieldDisplayHeader = x.FieldDisplayHeader, FieldExplanation = x.FieldExplanation, FieldName = x.FieldName, Qsid = x.Qsid, DateAddedDeleted = x.DateAddedDeleted }).ToList();
        }

        public static List<ListChangesReportAddQsidFieldPopUP> CovertAddQsidBusinessToViewModelProductDate(List<ListChangesReportQsid_Field_VM> lists)
        {
            return lists.Select(x => new ListChangesReportAddQsidFieldPopUP { FieldDisplayHeader = x.FieldDisplayHeader, FieldExplanation = x.FieldExplanation, FieldName = x.FieldName, Qsid = x.Qsid, DateAddedDeleted = x.DateAddedDeleted }).ToList();
        }

        public static ObservableCollection<Log_Map_EasiList_Qsid_VM> ConvertMapEasiProListIdBusinessToVM(List<Map_QSID_ListID> list)
        {
            return new ObservableCollection<Log_Map_EasiList_Qsid_VM>(list.Select(x => new Log_Map_EasiList_Qsid_VM { EasiPro_ListID = x.EasiPro_ListID, CreatedDate = DateTime.Now }).ToList());
        }

        public static List<Log_VersionControl_WorkOrders_VM> ConvertEntityWorkOrderModelToViewModel(List<Log_VersionControl_WorkOrders> listWorkOrder)
        {
            return listWorkOrder.Select(x => new Log_VersionControl_WorkOrders_VM { WO_Number = x.WO_Number, CreatedDate = DateTime.Now }).ToList();
        }
        public static List<Log_VersionControl_Tickets_VM> ConvertEntityTicketModelToViewModel(List<Log_VersionControl_CSCTickets> listTickets)
        {
            return listTickets.Select(x => new Log_VersionControl_Tickets_VM { Ticket_Number = x.CSC_Ticket_Number, Ticket_Url= x.CSC_Ticket_Url, CreatedDate = DateTime.Now }).ToList();
        }

        public static List<Library_QSID_Information_VM> BindLibraryInformationModel(List<LibraryInformation_QSID_VM> listQsidDetail)
        {
            List<Library_QSID_Information_VM> ListQsidInformationDetail = new List<Library_QSID_Information_VM>();
            Library_QSID_Information_VM obj_Library_Information_QSID_VM;//= new Library_QSID_Information_VM();

            foreach (var LibraryData_QSID in listQsidDetail)
            {
                obj_Library_Information_QSID_VM = new Library_QSID_Information_VM();
                if (!string.IsNullOrEmpty(LibraryData_QSID.Country))
                {
                    obj_Library_Information_QSID_VM.Country = Engine._listCountry.Where(x => x.Name.ToLower() == LibraryData_QSID.Country.ToLower()).FirstOrDefault();
                }
                obj_Library_Information_QSID_VM.ListFieldName = LibraryData_QSID.ListFieldName;
                obj_Library_Information_QSID_VM.ListIndustryVertical = LibraryData_QSID.ListIndustryVertical;
                obj_Library_Information_QSID_VM.ListId = LibraryData_QSID.ListId;
                obj_Library_Information_QSID_VM.NoteUrl = LibraryData_QSID.NoteUrl;
                obj_Library_Information_QSID_VM.EHS_ListCodes = LibraryData_QSID.EHS_ListCodes;
                obj_Library_Information_QSID_VM.ListObligationType = Engine._listObligationType.Any(x => x.Name == LibraryData_QSID.ListObligationType) ? Engine._listObligationType.Where(x => x.Name == LibraryData_QSID.ListObligationType).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.ListPhrase = LibraryData_QSID.ListPhrase;
                obj_Library_Information_QSID_VM.ListPublicationDate = CommonGeneralFunction.TryParseDateTime(LibraryData_QSID.PublicationDate);
                obj_Library_Information_QSID_VM.ListType = LibraryData_QSID.ListType;
                obj_Library_Information_QSID_VM.Module = !string.IsNullOrEmpty(LibraryData_QSID.Module) ? Engine._listModule.Any(x => x.Name.ToLower() == LibraryData_QSID.Module.ToLower()) ? Engine._listModule.Where(x => x.Name.ToLower() == (string.IsNullOrEmpty(LibraryData_QSID.Module) ? "" : LibraryData_QSID.Module.ToLower())).FirstOrDefault() : new ListModule_ViewModel() : new ListModule_ViewModel();
                obj_Library_Information_QSID_VM.QSID = LibraryData_QSID.QSID;
                obj_Library_Information_QSID_VM.QSID_ID = LibraryData_QSID.QSID_ID;
                obj_Library_Information_QSID_VM.ShortName = LibraryData_QSID.ShortName;
                obj_Library_Information_QSID_VM.ShortDescription = LibraryData_QSID.ShortDescription;
                obj_Library_Information_QSID_VM.Topic = Engine._listTopic.Any(x => x.Name == (string.IsNullOrEmpty(LibraryData_QSID.Topic) ? "" : LibraryData_QSID.Topic)) ? Engine._listTopic.Where(x => x.Name == (string.IsNullOrEmpty(LibraryData_QSID.Topic) ? "" : LibraryData_QSID.Topic)).FirstOrDefault() : new ListTopic_ViewModel();
                obj_Library_Information_QSID_VM.IsDeleted = Engine._listCheckboxType.Where(x => x.Value == LibraryData_QSID.IsDeleted).FirstOrDefault();
                obj_Library_Information_QSID_VM.ProductName = LibraryData_QSID.ProductName;// Engine._listProducts.Any(x => x.Name == LibraryData_QSID.ProductName) ? Engine._listProducts.Where(x => x.Name == LibraryData_QSID.ProductName).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.CurrentlyCheckedOut = Engine._listCheckedOutUser.Any(x => x.Name == LibraryData_QSID.CurrentlyCheckedOut) ? Engine._listCheckedOutUser.Where(x => x.Name == LibraryData_QSID.CurrentlyCheckedOut).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.ParentQsid = LibraryData_QSID.ParentQsid;
                obj_Library_Information_QSID_VM.ChildQsid = LibraryData_QSID.ChildQsid;
                obj_Library_Information_QSID_VM.GenericsCount = LibraryData_QSID.GenericsCount;
                ListQsidInformationDetail.Add(obj_Library_Information_QSID_VM);
            }
            return ListQsidInformationDetail;
        }

        public static List<ListChangesReportListDictionaryPopUP> CovertListChangesReportBusinessToViewModel(List<Log_ListDictionaryChanges_VM> lists)
        {
            return lists.Select(x => new ListChangesReportListDictionaryPopUP() { NewValue = x.NewValue, OldValue = x.OldValue, PhraseCategory = x.PhraseCategory }).ToList();
        }

        public static List<CompareQsidListDetail> BindCompareQsidModel(List<LibraryInformation_QSID_VM> listQsidDetail)
        {
            List<CompareQsidListDetail> ListQsidInformationDetail = new List<CompareQsidListDetail>();
            CompareQsidListDetail obj_Library_Information_QSID_VM;//= new Library_QSID_Information_VM();

            foreach (var LibraryData_QSID in listQsidDetail)
            {
                obj_Library_Information_QSID_VM = new CompareQsidListDetail();
                if (!string.IsNullOrEmpty(LibraryData_QSID.Country))
                {
                    obj_Library_Information_QSID_VM.Country = Engine._listCountry.Where(x => x.Name.ToLower() == LibraryData_QSID.Country.ToLower()).FirstOrDefault();
                }
                obj_Library_Information_QSID_VM.ListFieldName = LibraryData_QSID.ListFieldName;
                obj_Library_Information_QSID_VM.ListIndustryVertical = LibraryData_QSID.ListIndustryVertical;
                obj_Library_Information_QSID_VM.ListId = LibraryData_QSID.ListId;
                obj_Library_Information_QSID_VM.ListObligationType = Engine._listObligationType.Any(x => x.Name == LibraryData_QSID.ListObligationType) ? Engine._listObligationType.Where(x => x.Name == LibraryData_QSID.ListObligationType).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.ListPhrase = LibraryData_QSID.ListPhrase;
                obj_Library_Information_QSID_VM.ListPublicationDate = CommonGeneralFunction.TryParseDateTime(LibraryData_QSID.PublicationDate);
                obj_Library_Information_QSID_VM.ListType = LibraryData_QSID.ListType;
                obj_Library_Information_QSID_VM.Module = Engine._listModule.Any(x => x.Name == LibraryData_QSID.Module) ? Engine._listModule.Where(x => x.Name.ToLower() == (string.IsNullOrEmpty(LibraryData_QSID.Module) ? "" : LibraryData_QSID.Module.ToLower())).FirstOrDefault() : new ListModule_ViewModel();
                obj_Library_Information_QSID_VM.QSID = LibraryData_QSID.QSID;
                obj_Library_Information_QSID_VM.QSID_ID = LibraryData_QSID.QSID_ID;
                obj_Library_Information_QSID_VM.ShortName = LibraryData_QSID.ShortName;
                obj_Library_Information_QSID_VM.Topic = Engine._listTopic.Any(x => x.Name == (string.IsNullOrEmpty(LibraryData_QSID.Topic) ? "" : LibraryData_QSID.Topic)) ? Engine._listTopic.Where(x => x.Name == (string.IsNullOrEmpty(LibraryData_QSID.Topic) ? "" : LibraryData_QSID.Topic)).FirstOrDefault() : new ListTopic_ViewModel();
                obj_Library_Information_QSID_VM.IsDeleted = Engine._listCheckboxType.Where(x => x.Value == LibraryData_QSID.IsDeleted).FirstOrDefault();
                obj_Library_Information_QSID_VM.ProductName = LibraryData_QSID.ProductName;// Engine._listProducts.Any(x => x.Name == LibraryData_QSID.ProductName) ? Engine._listProducts.Where(x => x.Name == LibraryData_QSID.ProductName).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.CurrentlyCheckedOut = Engine._listCheckedOutUser.Any(x => x.Name == LibraryData_QSID.CurrentlyCheckedOut) ? Engine._listCheckedOutUser.Where(x => x.Name == LibraryData_QSID.CurrentlyCheckedOut).FirstOrDefault() : new ListCommonComboBox_ViewModel();
                obj_Library_Information_QSID_VM.ParentQsid = LibraryData_QSID.ParentQsid;
                obj_Library_Information_QSID_VM.ChildQsid = LibraryData_QSID.ChildQsid;
                ListQsidInformationDetail.Add(obj_Library_Information_QSID_VM);
            }
            return ListQsidInformationDetail;
        }

        public static List<Log_VersionControl_ProductTimestamp> BindProductTimeStampFromVersionHistory(Access_QSID_Data_VM objVersionHistory, string status)
        {
            List<Log_VersionControl_ProductTimestamp> listProductTimeStamp = new List<Log_VersionControl_ProductTimestamp>();
            listProductTimeStamp.Add(new Log_VersionControl_ProductTimestamp
            {
                ProductTimestamp = objVersionHistory.ProductTimeStamp,
                Qsid_Id = objVersionHistory.QSID_ID,
                Version = objVersionHistory.Version,
                DateFirstAdded = CommonGeneralFunction.ESTTime(),
                Status = status
            });
            return listProductTimeStamp;

        }

        public static List<Log_VersionControl_EditorialChangeType> ConvertEditorialChangesVMToEntityModel(List<ListEditorialChangeTypes_VM> listEditorialChangesVM, int? VersionControl_HistoryID)
        {
            return listEditorialChangesVM.Select(x => new Log_VersionControl_EditorialChangeType { ListEditorialChangeTypeID = x.ListEditorialChangeTypeID, VersionControl_HistoryID = VersionControl_HistoryID }).ToList();
        }

        public static List<ListChangesResportQsidChanges_VM> ConvertMapQsidChangesReportToViewModel(List<ListChangesReportQsid_Change_VM> listChangesReport_Change)
        {
            List<ListChangesResportQsidChanges_VM> listResultChanges_VM = new List<ListChangesResportQsidChanges_VM>();
            foreach (var itemChange in listChangesReport_Change)
            {
                ListChangesResportQsidChanges_VM objResultChange = new ListChangesResportQsidChanges_VM();
                objResultChange.IsMatch = itemChange.IsMatch;
                objResultChange.ListFieldName = itemChange.ListFieldName;
                objResultChange.ListVersionChangeType = Engine._listVersionChangeType.Where(x => x.Id == itemChange.ListVersionChangeTypeID).FirstOrDefault();
                objResultChange.NewListPhrase = itemChange.NewListPhrase;
                objResultChange.OldListPhrase = itemChange.OldListPhrase;
                objResultChange.Qsid = itemChange.Qsid;
                objResultChange.QSID_ID = itemChange.QSID_ID;
                objResultChange.WoNumber = GetWorkOrderNumber(itemChange.WoNumber);
                objResultChange.Versions = itemChange.Versions;
                objResultChange.RowAdded = itemChange.RowAdded;
                objResultChange.RowChanged = itemChange.RowChanged;
                objResultChange.RowDeleted = itemChange.RowDeleted;
                objResultChange.CasAdded = itemChange.CasAdded;
                objResultChange.CasDeleted = itemChange.CasDeleted;
                objResultChange.ProductDateMin = itemChange.ProductDateMin;
                objResultChange.ProductDateMax = itemChange.ProductDateMax;
                objResultChange.FieldsAdded = itemChange.FieldsAdded;
                objResultChange.FieldsDeleted = itemChange.FieldsDeleted;
                objResultChange.FieldExplainChanges = itemChange.FieldExplainChanges;
                objResultChange.ListDictionaryChanges = itemChange.ListDictionaryChanges;
                objResultChange.CountNOL = itemChange.CountNOL;
                listResultChanges_VM.Add(objResultChange);
            }
            return listResultChanges_VM;
        }
        public static List<ListChangesReportQsidAdd_VM> ConvertMapQsidAddReportToViewModel(List<ListChangesReportQsid_Add_VM> listChangesReport_Add)
        {
            List<ListChangesReportQsidAdd_VM> listResultChanges_VM = new List<ListChangesReportQsidAdd_VM>();
            foreach (var itemChange in listChangesReport_Add)
            {
                ListChangesReportQsidAdd_VM objResultChange = new ListChangesReportQsidAdd_VM();

                objResultChange.ListFieldName = itemChange.ListFieldName;
                objResultChange.ShortName = itemChange.ShortName;
                objResultChange.Qsid = itemChange.Qsid;
                objResultChange.QSID_ID = itemChange.QSID_ID;
                objResultChange.WoNumber = GetWorkOrderNumber(itemChange.WoNumber);
                objResultChange.Versions = itemChange.Versions;
                objResultChange.RowAdded = itemChange.RowAdded;
                objResultChange.ListPhrase = itemChange.ListPhrase;
                objResultChange.NormalizedFields = itemChange.NormalizedFields;
                objResultChange.CasAdded = itemChange.CasAdded;
                objResultChange.ProductDateMin = itemChange.ProductDateMin;
                objResultChange.ProductDateMax = itemChange.ProductDateMax;
                objResultChange.CountNOL = itemChange.CountNOL;
                listResultChanges_VM.Add(objResultChange);
            }
            return listResultChanges_VM;
        }
        public static List<ListChangesReportQsidDelete_VM> ConvertMapQsidDeleteReportToViewModel(List<ListChangesReportQsid_Delete_VM> listChangesReport_Delete)
        {
            List<ListChangesReportQsidDelete_VM> listResultChanges_VM = new List<ListChangesReportQsidDelete_VM>();
            foreach (var itemChange in listChangesReport_Delete)
            {
                ListChangesReportQsidDelete_VM objResultChange = new ListChangesReportQsidDelete_VM();

                objResultChange.ListFieldName = itemChange.ListFieldName;
                objResultChange.ShortName = itemChange.ShortName;
                objResultChange.QSID_ID = itemChange.QSID_ID;
                objResultChange.Qsid = itemChange.Qsid;
                objResultChange.WoNumber = GetWorkOrderNumber(itemChange.WoNumber);
                objResultChange.Versions = itemChange.Versions;
                objResultChange.ListPhrase = itemChange.ListPhrase;
                objResultChange.ProductDateMin = itemChange.ProductDateMin;
                objResultChange.ProductDateMax = itemChange.ProductDateMax;
                listResultChanges_VM.Add(objResultChange);
            }
            return listResultChanges_VM;
        }
        private static List<HyperLinkDataValue> GetWorkOrderNumber(string workOrderNumber)
        {
            List<HyperLinkDataValue> listWorkOrder = new List<HyperLinkDataValue>();
            if (!string.IsNullOrEmpty(workOrderNumber))
            {
                string[] WONumber = workOrderNumber.Split(';');
                listWorkOrder = WONumber.Select(x => new HyperLinkDataValue { DisplayValue = x.Trim().ToString(), NavigateData = "https://awm.3ecompany.com/ViewWorkOrder.aspx?WOId=" + Encrypt(x.Trim().ToString()) }).ToList();
            }
            return listWorkOrder;
        }
        private static string Encrypt(string plainText)

        {

            const string passPhrase = "Pass@sng23"; // can be any string

            const string saltValue = "salt@sng23"; // can be any string

            const string hashAlgorithm = "SHA1"; // can be "MD5"

            const int passwordIterations = 2; // can be any number

            const string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes

            const int keySize = 256; // can be 192 or 128



            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);

            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            var password = new PasswordDeriveBytes(passPhrase,

                                                   saltValueBytes,

                                                   hashAlgorithm,

                                                   passwordIterations);



            byte[] keyBytes = password.GetBytes(keySize / 8);

            var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(

                keyBytes,

                initVectorBytes);



            var memoryStream = new MemoryStream();

            var cryptoStream = new CryptoStream(memoryStream,

                                                encryptor,

                                                CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            cryptoStream.FlushFinalBlock();

            byte[] cipherTextBytes = memoryStream.ToArray();

            memoryStream.Close();

            cryptoStream.Close();



            string cipherText = Convert.ToBase64String(cipherTextBytes);

            return cipherText;

        }

        public static List<Log_VersionControl_WorkOrders> ConvertWorkOrderToEntityModel(List<Log_VersionControl_WorkOrders_VM> listWorkOrderVM, int? VersionControl_HistoryID)
        {
            return listWorkOrderVM.Select(x => new Log_VersionControl_WorkOrders { WO_Number = x.WO_Number, VersionControl_HistoryID = VersionControl_HistoryID }).ToList();
        }
        public static List<Log_VersionControl_CSCTickets> ConvertTicketsToEntityModel(List<Log_VersionControl_Tickets_VM> listTicketsVM, int? VersionControl_HistoryID)
        {
            return listTicketsVM.Select(x => new Log_VersionControl_CSCTickets { CSC_Ticket_Number = x.Ticket_Number,CSC_Ticket_Url=x.Ticket_Url, VersionControl_HistoryID = VersionControl_HistoryID }).ToList();
        }

        public static Access_QSID_Data_VM ConvertVersionHistoryTO_VM(VersionControlSystem.Model.DataModel.Log_VersionControl_History objVersionHistory, string qsid)
        {
            Access_QSID_Data_VM resultQSID_VM = new Access_QSID_Data_VM();
            resultQSID_VM.QSID = qsid;
            resultQSID_VM.ListVersionChangeTypeID = objVersionHistory.ListVersionChangeTypeID;
            resultQSID_VM.AddNote = objVersionHistory.AddNote;
            resultQSID_VM.VersionControl_ID = objVersionHistory.VersionControl_HistoryID;
            resultQSID_VM.Version = objVersionHistory.Version;
            resultQSID_VM.CheckedOut = objVersionHistory.Currently_Checked_Out;
            resultQSID_VM.Check_In_TimeStamp = objVersionHistory.Check_In_TimeStamp;
            resultQSID_VM.Check_Out_TimeStamp = objVersionHistory.Check_Out_TimeStamp;
            resultQSID_VM.FileName = objVersionHistory.FileName;
            resultQSID_VM.FilePath = objVersionHistory.FilePath;
            //resultQSID_VM.IsLocked = objVersionHistory.IsLocked;
            resultQSID_VM.ProductTimeStamp = objVersionHistory.ProductTimeStamp;
            resultQSID_VM.UserName = objVersionHistory.CheckedOutByUser;
            resultQSID_VM.DateFirstAdded = objVersionHistory.DateFirstAdded;
            //resultQSID_VM.UniqueSubstance = objVersionHistory.UniqueSubstance.ToString();
            //resultQSID_VM.TotalCount = objVersionHistory.TotalCount.ToString();
            resultQSID_VM.QSID_ID = Convert.ToInt32(objVersionHistory.QSID_ID);
            return resultQSID_VM;
        }


        public static void GenerateWordDocument(string ExportedPath, DateTime? startDate, DateTime? endDate, string ProductName, ListChangesReport_Detail_VM ObjChangesReportDetailInfo)
        {

            //Create an instance for word app  
            Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();

            //Set status for word application is to be visible or not.  
            //winword.Visible = false;


            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            //Create a new document  
            Microsoft.Office.Interop.Word.Document document = null;//winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

            //document.PageSetup = Page
            object styleHeader2 = "Heading 2";
            object styleHeader1 = "Heading 1";
            //Add header into the document  
            foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
            {
                //Get the header range and add the header details.  
                Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;

                headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;
                //headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
                headerRange.Font.Size = 10;
                Microsoft.Office.Interop.Word.Paragraph headerPara = headerRange.Paragraphs.Add(ref missing);
                // headerRange.Fields.Add(headerRange, Type: Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                headerPara.Range.Text = "List Changes Report";
                // headerRange.set_Style(ref styleHeading2);
            }

            //Add the footers into the document  
            foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
            {
                //Get the footer range and add the footer details.  

                Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdDarkRed;
                footerRange.Font.Size = 8;
                ////footerRange.F.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;
                //footerRange.Fields.Add(footerRange, Type: Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                Microsoft.Office.Interop.Word.Paragraph footerParaleft = footerRange.Paragraphs.Add(ref missing);
                footerParaleft.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
                footerParaleft.Range.Fields.Add(footerRange, Type: Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                footerParaleft.Range.InsertParagraphAfter();
                Microsoft.Office.Interop.Word.Paragraph footerParaCenter = footerRange.Paragraphs.Add(ref missing);
                footerParaCenter.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;
                footerParaCenter.Range.Text = "                                          Created Date: " + DateTime.Now + "                   Created By: " + Engine.CurrentUserName;
                footerParaCenter.Range.InsertParagraphAfter();
            }

            Microsoft.Office.Interop.Word.Paragraph MainHeader = document.Content.Paragraphs.Add(ref missing);
            MainHeader.Range.set_Style(ref styleHeader1);
            MainHeader.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            MainHeader.Range.Text = "List Changes Report";
            MainHeader.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph searchCriteriaPara = document.Content.Paragraphs.Add(ref missing);
            searchCriteriaPara.Range.Text = Environment.NewLine + "Search Criteria Applied:";
            searchCriteriaPara.Range.Text += "Product Time Stamp Start Date: " + startDate;
            searchCriteriaPara.Range.Text += "Product Time Stamp End Date: " + endDate;
            searchCriteriaPara.Range.Text += "Product Name: " + ProductName + Environment.NewLine;
            searchCriteriaPara.Range.InsertParagraphAfter();


            // Add paragraph with Heading 2 style
            Microsoft.Office.Interop.Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
            para2.Range.set_Style(ref styleHeader2);
            para2.Range.Shading.BackgroundPatternColor = Microsoft.Office.Interop.Word.WdColor.wdColorGray25;
            para2.Range.Text = Environment.NewLine + "Summary Count: ";
            para2.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para2.Range, 7, 2, ref missing, ref missing);
            firstTable.Borders.Enable = 1;
            foreach (Microsoft.Office.Interop.Word.Row row in firstTable.Rows)
            {
                foreach (Microsoft.Office.Interop.Word.Cell cell in row.Cells)
                {
                    if (cell.RowIndex == 1)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Description", true); } else { CreateCell(cell, "Count", true); }
                    }
                    else if (cell.RowIndex == 2)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Add New List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountNewAddedList, false); }

                    }
                    else if (cell.RowIndex == 3)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Changes in Existing List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountUpdatedList, false); }

                    }
                    else if (cell.RowIndex == 4)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Deleted List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountDeletedList, false); }

                    }
                    else if (cell.RowIndex == 5)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Add New Fields in Existing List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountNewFieldAdded, false); }

                    }
                    else if (cell.RowIndex == 6)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Deleted Fields in Existing List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountFieldDeleted, false); }

                    }
                    else if (cell.RowIndex == 7)
                    {
                        if (cell.ColumnIndex == 1) { CreateCell(cell, "Count Add New Units List", false); } else { CreateCell(cell, ObjChangesReportDetailInfo.ChangesReportSummary.CountNewUnitAdded, false); }

                    }
                }
            }



            if (ObjChangesReportDetailInfo.ListChangesReport_Add.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Add New List: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Add.Count + 1), 5, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Add);
            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Change.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Existing List Updated: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Change.Count + 1), 5, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Change);
            }


            if (ObjChangesReportDetailInfo.ListChangesReport_Delete.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Deleted List: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Delete.Count + 1), 5, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Delete);
            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Fields_Add.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Add New Fields In Existing List: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Fields_Add.Count + 1), 4, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Fields_Add);
            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Fields_Delete.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Deleted Fields In Existing List: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Fields_Delete.Count + 1), 4, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Fields_Delete);
            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Unit_Add.Count > 0)
            {

                // Add paragraph with Heading 2 style
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                para3.Range.set_Style(ref styleHeader2);
                para3.Range.Text = Environment.NewLine + "Summary Detail Add New Units In Existing List: ";
                para3.Range.InsertParagraphAfter();

                Microsoft.Office.Interop.Word.Table secondTable = document.Tables.Add(para3.Range, (ObjChangesReportDetailInfo.ListChangesReport_Unit_Add.Count + 1), 3, ref missing, ref missing);
                BuildTableinWordDocument(secondTable, ObjChangesReportDetailInfo.ListChangesReport_Unit_Add);
            }

            //Save the document  
            object filename = ExportedPath;
            document.SaveAs2(ref filename);
            document.Close(ref missing, ref missing, ref missing);
            document = null;
            winword.Quit(ref missing, ref missing, ref missing);
            winword = null;
            // MessageBox.Show("Document created successfully !");

        }



        public static void CreateCell(Microsoft.Office.Interop.Word.Cell cell, string inputText, bool isHeader)
        {
            if (isHeader)
            {
                cell.Range.Text = inputText;
                cell.Range.Font.Bold = 1;
                //other format properties goes here  
                cell.Range.Font.Name = "verdana";
                cell.Range.Font.Size = 10;
                //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                cell.Shading.BackgroundPatternColor = Microsoft.Office.Interop.Word.WdColor.wdColorGray25;
                //Center alignment for the Header cells  
                cell.VerticalAlignment = Microsoft.Office.Interop.Word.WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                cell.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            }
            else
            {
                cell.Range.Text = inputText;
            }
        }

        public static void BuildTableinWordDocument<T>(Microsoft.Office.Interop.Word.Table firstTable, List<T> listChangeReportData)
        {
            DataTable dtTable = ConvertToDataTable(listChangeReportData);
            firstTable.Borders.Enable = 1;
            foreach (Microsoft.Office.Interop.Word.Row row in firstTable.Rows)
            {
                foreach (Microsoft.Office.Interop.Word.Cell cell in row.Cells)
                {
                    //Header row  
                    if (cell.RowIndex == 1)
                    {
                        cell.Range.Text = dtTable.Columns[cell.ColumnIndex - 1].ColumnName.ToString();
                        cell.Range.Font.Bold = 1;
                        //other format properties goes here  
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 10;
                        //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                        cell.Shading.BackgroundPatternColor = Microsoft.Office.Interop.Word.WdColor.wdColorGray25;
                        //Center alignment for the Header cells  
                        cell.VerticalAlignment = Microsoft.Office.Interop.Word.WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                        cell.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

                    }
                    //Data row  
                    else
                    {
                        cell.Range.Text = dtTable.Rows[cell.RowIndex - 2][cell.ColumnIndex - 1].ToString();
                    }
                }
            }
            firstTable.AllowPageBreaks = true;
        }

        public static void GeneratePdfDocument(string ExportedPath, DateTime? startDate, DateTime? endDate, string ProductName, ListChangesReport_Detail_VM ObjChangesReportDetailInfo)
        {
            iTextSharp.text.Font baseFontHeader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16f, iTextSharp.text.Font.BOLDITALIC, iTextSharp.text.BaseColor.BLUE);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Document doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 36, 72, 108, 180);

            iTextSharp.text.pdf.PdfWriter pdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, new FileStream(ExportedPath, FileMode.Create));
            // doc.AddHeader("Header", "List Changes Report");
            pdfWriter.PageEvent = new Common.ITextEvents();
            doc.Open();

            AddHeaderInPdfSection(doc, "Search Criteria Applied:");

            //iTextSharp.text.Paragraph SearchCriteriaPara = new iTextSharp.text.Paragraph("Search Criteria Applied:");
            //SearchCriteriaPara.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            //doc.Add(SearchCriteriaPara);


            iTextSharp.text.pdf.PdfPTable pdfTab = new iTextSharp.text.pdf.PdfPTable(2);
            pdfTab.SpacingAfter = 10f;
            pdfTab.SpacingBefore = 10f;
            iTextSharp.text.pdf.PdfPCell pdfCell1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Product Time Stamp Start Date:", baseFontNormal));
            iTextSharp.text.pdf.PdfPCell pdfCell2 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(startDate.ToString(), baseFontBig));

            iTextSharp.text.pdf.PdfPCell pdfCell3 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Product Time Stamp End Date:", baseFontNormal));
            iTextSharp.text.pdf.PdfPCell pdfCell4 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(endDate.ToString(), baseFontBig));

            iTextSharp.text.pdf.PdfPCell pdfCell5 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Product Name:", baseFontNormal));
            iTextSharp.text.pdf.PdfPCell pdfCell6 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(ProductName.ToString(), baseFontBig));


            pdfCell1.Border = 0;
            pdfCell2.Border = 0;
            pdfCell3.Border = 0;
            pdfCell4.Border = 0;
            pdfCell5.Border = 0;
            pdfCell6.Border = 0;

            pdfCell1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell2.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell3.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell4.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell5.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell6.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

            pdfTab.AddCell(pdfCell1);
            pdfTab.AddCell(pdfCell2);
            pdfTab.AddCell(pdfCell3);
            pdfTab.AddCell(pdfCell4);
            pdfTab.AddCell(pdfCell5);
            pdfTab.AddCell(pdfCell6);

            //  pdfTab.TotalWidth = doc.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 100;
            doc.Add(pdfTab);

            //string strSearchCri = string.Empty;

            //strSearchCri = "Product Time Stamp Start Date: " + startDate + Environment.NewLine;
            //strSearchCri += "Product Time Stamp End Date: " + endDate + Environment.NewLine;
            //strSearchCri += "Is Food Data: " + IsFoodData + Environment.NewLine;

            //iTextSharp.text.Paragraph parastrSearchCri = new iTextSharp.text.Paragraph(strSearchCri);

            //doc.Add(parastrSearchCri);

            //iTextSharp.text.Paragraph SummaryCount = new iTextSharp.text.Paragraph("Summary Count:");
            //SummaryCount.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
            //doc.Add(SummaryCount);

            AddHeaderInPdfSection(doc, "Summary Count:");

            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(2);
            table.SpacingAfter = 10f;
            table.SpacingBefore = 10f;
            table.WidthPercentage = 100;
            iTextSharp.text.pdf.PdfPCell pdfCellHeader1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Description", baseFontBig));
            iTextSharp.text.pdf.PdfPCell pdfCellHeader2 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase("Count", baseFontBig));
            pdfCellHeader1.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            pdfCellHeader2.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            pdfCellHeader1.Padding = 8f;
            pdfCellHeader2.Padding = 8f;

            table.AddCell(pdfCellHeader1);
            table.AddCell(pdfCellHeader2);

            table.AddCell(new iTextSharp.text.Phrase("Count Add New List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountNewAddedList));

            table.AddCell(new iTextSharp.text.Phrase("Count Changes in Existing List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountUpdatedList));

            table.AddCell(new iTextSharp.text.Phrase("Count Deleted List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountDeletedList));

            table.AddCell(new iTextSharp.text.Phrase("Count Add New Fields in Existing List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountNewFieldAdded));

            table.AddCell(new iTextSharp.text.Phrase("Count Deleted Fields in Existing List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountFieldDeleted));

            table.AddCell(new iTextSharp.text.Phrase("Count Add New Units List"));
            table.AddCell(new iTextSharp.text.Phrase(ObjChangesReportDetailInfo.ChangesReportSummary.CountNewUnitAdded));

            doc.Add(table);



            if (ObjChangesReportDetailInfo.ListChangesReport_Add.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Add New List:");
                doc.Add(CreatePdfTable(5, ObjChangesReportDetailInfo.ListChangesReport_Add));


            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Change.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Existing List Updated:");
                doc.Add(CreatePdfTable(5, ObjChangesReportDetailInfo.ListChangesReport_Change));
            }


            if (ObjChangesReportDetailInfo.ListChangesReport_Delete.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Deleted List: ");
                doc.Add(CreatePdfTable(5, ObjChangesReportDetailInfo.ListChangesReport_Delete));

            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Fields_Add.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Add New Fields In Existing List:");
                doc.Add(CreatePdfTable(4, ObjChangesReportDetailInfo.ListChangesReport_Fields_Add));

            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Fields_Delete.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Deleted Fields In Existing List:");
                doc.Add(CreatePdfTable(4, ObjChangesReportDetailInfo.ListChangesReport_Fields_Delete));

            }
            if (ObjChangesReportDetailInfo.ListChangesReport_Unit_Add.Count > 0)
            {
                AddHeaderInPdfSection(doc, "Summary Detail Add New Units In Existing List:");
                doc.Add(CreatePdfTable(3, ObjChangesReportDetailInfo.ListChangesReport_Unit_Add));
            }

            doc.Close();

        }
        public static void GenerateAccessDocument(string ExportedPath, DateTime? startDate, DateTime? endDate, string ProductName, ListChangesReport_Detail_VM ObjChangesReportDetailInfo)
        {
        }
            public static void AddHeaderInPdfSection(iTextSharp.text.Document doc, string HeaderText)
        {
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(18, 119, 111));
            iTextSharp.text.pdf.PdfPTable pdfTab = new iTextSharp.text.pdf.PdfPTable(3);
            pdfTab.SpacingAfter = 10f;
            pdfTab.SpacingBefore = 10f;
            iTextSharp.text.pdf.PdfPCell pdfCell1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(HeaderText, baseFontBig));
            pdfCell1.Border = 0;
            pdfCell1.Colspan = 3;
            pdfCell1.Padding = 8f;
            pdfCell1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
            pdfCell1.BackgroundColor = new iTextSharp.text.BaseColor(231, 245, 242);
            pdfTab.AddCell(pdfCell1);
            //  pdfTab.TotalWidth = doc.PageSize.Width - 80f;
            pdfTab.WidthPercentage = 100;
            doc.Add(pdfTab);
        }

        public static iTextSharp.text.pdf.PdfPTable CreatePdfTable<T>(int colCount, List<T> listChangeReportData)
        {
            iTextSharp.text.Font baseFontBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(colCount);
            table.SpacingAfter = 10f;
            table.SpacingBefore = 10f;
            table.WidthPercentage = 100;
            DataTable dt = ConvertToDataTable(listChangeReportData);
            for (int counter = 0; counter < dt.Columns.Count; counter++)
            {
                if (counter < colCount)
                {
                    iTextSharp.text.pdf.PdfPCell pdfCell1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(dt.Columns[counter].ColumnName, baseFontBold));
                    pdfCell1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                    pdfCell1.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    pdfCell1.Padding = 8f;
                    table.AddCell(pdfCell1);
                }
            }

            foreach (DataRow r in dt.Rows)
            {
                for (int counter = 0; counter < dt.Columns.Count; counter++)
                {
                    if (dt.Rows.Count > 0 && counter < colCount)
                    {
                        iTextSharp.text.pdf.PdfPCell pdfCell1 = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(r[counter].ToString(), baseFontNormal));
                        pdfCell1.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                        pdfCell1.Padding = 6f;
                        table.AddCell(pdfCell1);
                    }
                }
            }

            return table;
        }

        public static List<VersionControlSystem.Model.ViewModel.MapProductListVM> ConvertMapProductToViewModel(List<CRA_DataAccess.ViewModel.MapProductList_VM> listMapProductVM)
        {
            return listMapProductVM.Select(x => new VersionControlSystem.Model.ViewModel.MapProductListVM { IsSelected = x.IsSelected, ProductInternalCode=x.ProductInternalCode, ProductID = x.ProductID, ProductName = x.ProductName, Qsid_ID = x.Qsid_ID }).ToList();
        }
        public static List<VersionControlSystem.Model.ViewModel.MapParentChildVM> ConvertMapParentChildToViewModel(List<CRA_DataAccess.ViewModel.MapParentChildList_VM> listParentChildQsid)
        {
            return listParentChildQsid.Select(x => new VersionControlSystem.Model.ViewModel.MapParentChildVM { IsSelected = x.IsSelected, Qsid = x.Qsid, Qsid_ID = x.Qsid_ID }).ToList();
        }
       

        public static List<CRA_DataAccess.ViewModel.MapParentChildList_VM> ConvertMapParentChildToViewModel(List<VersionControlSystem.Model.ViewModel.MapParentChildVM> listParentChildQsid)
        {
            return listParentChildQsid.Select(x => new CRA_DataAccess.ViewModel.MapParentChildList_VM { IsSelected = x.IsSelected, Qsid = x.Qsid, Qsid_ID = x.Qsid_ID }).ToList();
        }
        public static List<CRA_DataAccess.ViewModel.MapProductList_VM> ConvertMapProductToViewModel(List<VersionControlSystem.Model.ViewModel.MapProductListVM> listMapProductVM)
        {
            return listMapProductVM.Select(x => new CRA_DataAccess.ViewModel.MapProductList_VM { IsSelected = x.IsSelected, ProductID = x.ProductID, ProductName = x.ProductName, Qsid_ID = x.Qsid_ID }).ToList();
        }
        public static List<ListEditorialChangeTypes_VM> ConvertListEditorialChangeTypesToViewModel(List<EntityClass.Library_ListEditorialChangeTypes> listEditorialChanges)
        {
            return listEditorialChanges.Select(x => new ListEditorialChangeTypes_VM { ListEditorialChangeTypeID = x.ListEditorialChangeTypeID, ListEditorialChangeType = x.ListEditorialChangeType }).ToList();
        }
        public static List<EntityClass.Map_QSID_ListID> ConvertMapEasiProListIdToBusinessViewModel(List<VersionControlSystem.Model.ViewModel.Log_Map_EasiList_Qsid_VM> listEasiProlistId, int QsidId)
        {
            return listEasiProlistId.Select(x => new EntityClass.Map_QSID_ListID { EasiPro_ListID = (int?)x.EasiPro_ListID, QSID_ID = QsidId }).ToList();
        }
        public static List<VersionControlSystem.Model.ViewModel.MapVerticalListVM> ConvertMapVerticalToViewModel(List<CRA_DataAccess.ViewModel.MapVerticalList_VM> listMapVerticalVM)
        {
            return listMapVerticalVM.Select(x => new VersionControlSystem.Model.ViewModel.MapVerticalListVM { IsSelected = x.IsSelected, VerticalID = x.VerticalID, VerticalName = x.VerticalName, Qsid_ID = x.Qsid_ID }).ToList();
        }
        public static List<CRA_DataAccess.ViewModel.MapVerticalList_VM> ConvertMapVerticalToViewModel(List<VersionControlSystem.Model.ViewModel.MapVerticalListVM> listMapVerticalVM)
        {
            return listMapVerticalVM.Select(x => new CRA_DataAccess.ViewModel.MapVerticalList_VM { IsSelected = x.IsSelected, VerticalID = x.VerticalID, VerticalName = x.VerticalName, Qsid_ID = x.Qsid_ID }).ToList();
        }

        public static List<EntityClass.Map_QSID_ERC_EHSListCode> ConvertMapEHSListCodeToBusinessViewModel(List<VersionControlSystem.Model.ViewModel.Log_Map_QSID_ERC_EHSListCode_VM> listEhsListCode, int QsidId)
        {
            return listEhsListCode.Select(x => new EntityClass.Map_QSID_ERC_EHSListCode { EHS_ListCode = x.EHS_ListCode, QSID_ID = QsidId }).ToList();
        }
        public static ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM> ConvertMapEHSListCodeBusinessToVM(List<Map_QSID_ERC_EHSListCode> list)
        {
            return new ObservableCollection<Log_Map_QSID_ERC_EHSListCode_VM>(list.Select(x => new Log_Map_QSID_ERC_EHSListCode_VM { EHS_ListCode = x.EHS_ListCode, CreatedDate = DateTime.Now }).ToList());
        }
        public static List<T> Clone<T>(this List<T> list) where T : ICloneable
        {
            return list.Select(i => (T)i.Clone()).ToList();
        }
    }
}

