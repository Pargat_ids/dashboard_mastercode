﻿using AccessVersionControlSystem.ViewModel;
using EntityClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using CRA_DataAccess;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.Common;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;
using System.Configuration;
using CRA_DataAccess.ViewModel;

namespace AccessVersionControlSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public ILogUserSessionBL _objLogUserSession_Service { get; private set; }
        private static System.Threading.Mutex _mutex = null;

        public App()
        {
            _objLibraryFunction_Service = LibraryFunction.GetInstance;
            _objLogUserSession_Service = new LogUserSessionBL(_objLibraryFunction_Service);

        }
        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args.Count() > 0)
            {
                Engine.IsOpenCasAssignmentFromAccess = true;
                Engine.CasAssignmentAccessDBFilePath = GetAccessPathFromArgument(e.Args);
            }
            if (ValidateUserIdForCraDashboard())
            {
                CheckExistingInstanceIsOpen();
                if (Engine.IsOpenExistingInstance)
                {
                    MessageBox.Show("Cra Dashboard Tool is already opened. Please open the existing instance.");
                    System.Windows.Application.Current.Shutdown();
                }
                else
                {
                    base.OnStartup(e);
                }
            }
            else
            {
                Engine.IsOpenExistingInstance = true;
                MessageBox.Show("User is not authorized for run Cra Dashboard Tool. Please connect with administrator.");
                System.Windows.Application.Current.Shutdown();

            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            WelcomeWindow objWelcomeWin = new WelcomeWindow();
            objWelcomeWin.Show();

            System.Threading.Tasks.Task.Run(() =>
            {
                if (!Engine.IsOpenExistingInstance)
                {
                    GetToolDbType();
                    FetchLastCompileDate();
                    LoginUserSession();
                    GetPermissionByUser();
                    AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;


                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Render,
        new Action(() =>
        {
            MainWindow objWindow = new MainWindow();
            Application.Current.MainWindow = objWindow;
            objWindow.ShowActivated = true;
            objWindow.Show();
            objWelcomeWin.Close();

        }));
                }
                else
                {
                    objWelcomeWin.Close();
                }
            });

        }
        private void FetchLastCompileDate()
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            DateTime buildDate = new DateTime(2000, 1, 1)
                .AddDays(version.Build)
                .AddSeconds(version.Revision * 2);
            string tmp = " ";
            double tmpWidth = 0;
            for (; ((tmpWidth + 1) < 150);)
            {
                tmp += " ";
                tmpWidth = (tmpWidth + 1);
            }
            Engine.ToolVersion = buildDate.ToString("MM/dd/yyyy");
        }
        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            ErrorLogging(ex);
            LoginError(ex);
            LogoutUserSession();
            MessageBox.Show(ex.Message, "Error");
            MessageBox.Show(ex.StackTrace, "Error Detail");
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            if (!Engine.IsOpenExistingInstance)
            {
                LogoutUserSession();
            }
        }
        private void LogoutUserSession()
        {
            _objLogUserSession_Service.UpdateSessionForVersionControl(Engine.CurrentUserSessionID);
        }
        private void LoginUserSession()
        {
            var CurrentSession = _objLogUserSession_Service.CreateNewSessionForVersionControl(Environment.UserName);
            Engine.CurrentUserSessionID = CurrentSession.SessionID;
            Engine.CurrentUserName = _objLogUserSession_Service.GetLibrary_UserName(Environment.UserName);
            //var CurrentRolePermission = _objLogUserSession_Service.GetCurrentUserRolePermission(CurrentSession.UserName);
            //VersionControlSystem.Model.DataModel.Map_VersionControl_Permissions objRolepermission = new VersionControlSystem.Model.DataModel.Map_VersionControl_Permissions();
            //if (CurrentRolePermission != null)
            //{
            //    objRolepermission.RoleId = CurrentRolePermission.RoleId;
            //    objRolepermission.IsAddProductTimeStamp = CurrentRolePermission.IsAddProductTimeStamp;
            //    objRolepermission.IsCheckOutOrIN = CurrentRolePermission.IsCheckOutOrIN;
            //    objRolepermission.IsCompareList = CurrentRolePermission.IsCompareList;
            //    objRolepermission.IsGetCopy = CurrentRolePermission.IsGetCopy;
            //    objRolepermission.IsSubstanceSearch = CurrentRolePermission.IsSubstanceSearch;
            //    objRolepermission.IsViewAdmin = CurrentRolePermission.IsViewAdmin;
            //   // objRolepermission.IsApproveRejectGenericSuggestion = CurrentRolePermission.IsApproveRejectGenericSuggestion;
            //}
            //Engine.CurrentUserRolePermission = objRolepermission;
        }
        private void LoginError(Exception errorException)
        {
            try
            {
                List<Log_ErrorTracking> listLogErrorTrack = new List<Log_ErrorTracking>();
                Log_ErrorTracking objLogErrorTrack = new Log_ErrorTracking();
                objLogErrorTrack.ErrorMessage = errorException.Message;
                objLogErrorTrack.ErrorLocation = errorException.StackTrace.Length >= 2000 ? errorException.StackTrace.Substring(0, 1999) : errorException.StackTrace;
                objLogErrorTrack.UserName = Environment.UserName;
                objLogErrorTrack.TimeStamp = CommonGeneralFunction.ESTTime();
                objLogErrorTrack.SessionID = Engine.CurrentUserSessionID;
                listLogErrorTrack.Add(objLogErrorTrack);
                _objLogUserSession_Service.LoginError(listLogErrorTrack);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Exception In App XAML", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void ErrorLogging(Exception ex)
        {
            string currentDT = DateTime.Today.ToString("MM_dd_yyyy");
            string strPath = @"P:\CRA\Tools\VersionControl\ErrorLogs\" + "Error_Log_" + Engine.CurrentUserName.Replace(" ", "") + "_" + currentDT + ".txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(strPath))
            {
                sw.WriteLine("=============Error Logging ===========");
                sw.WriteLine("===========Start============= " + DateTime.Now);
                sw.WriteLine("Error Message: " + ex.Message);
                sw.WriteLine("Stack Trace: " + ex.StackTrace);
                sw.WriteLine("===========End============= " + DateTime.Now);

            }
        }

        private bool ValidateUserIdForCraDashboard()
        {
            return _objLogUserSession_Service.ValidateUserIDForCraDashboard(Environment.UserName);
        }
        private void CheckExistingInstanceIsOpen()
        {
            const string appName = "Cra Dashboard";
            bool createdNew;

            _mutex = new System.Threading.Mutex(true, appName, out createdNew);

            if (createdNew)
            {

                Engine.IsOpenExistingInstance = false;
                //Engine.IsOpenExistingInstance = _objLogUserSession_Service.CheckExistingInstanceIsOpen(Environment.UserName);
            }
            else
            {
                Engine.IsOpenExistingInstance = true;
            }
        }

        private string GetAccessPathFromArgument(string[] args)
        {
            string pathAccDB = string.Empty;
            int c = args.Count();
            if (c > 1)
            {
                for (int i = 0; i < c; i++)
                {
                    pathAccDB = pathAccDB + " " + args[i];
                }
            }
            else
            {
                pathAccDB = args[0];
            }
            return pathAccDB;
        }
        private void OnCellHyperlinkClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;

            //handle the event as before
        }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ErrorLogging(e.Exception);
            LoginError(e.Exception);
            MessageBox.Show("An unhandled exception just occurred: " + e.Exception.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }
        public void GetToolDbType()
        {
            string dbContext = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            string[] arrayContext = dbContext.Split(";");
            var dbName = arrayContext.Where(x => x.Contains("initial catalog")).FirstOrDefault();
            string initialCatalog = dbName.Split("=")[1];
            switch (initialCatalog)
            {
                case "CRA_Architecture_Dev":
                    Engine.ToolDbType = "UAT";
                    break;
                case "CRA_Architecture":
                    Engine.ToolDbType = "Production";
                    break;
                case "CRA_Dev":
                    Engine.ToolDbType = "Local";
                    break;
            }
        }

        public void GetPermissionByUser()
        {
            var listPermission = _objLibraryFunction_Service.GetPermissionByUser(Environment.UserName).Select(x => new MapPermission_VM()
            {
                PermissionName = x.PermissionName.ToLower(),
                PermissionID = x.PermissionID,
                UniquePermissionName = x.UniquePermissionName
            }).ToList();
            Engine.listAllPermissionDashboard = listPermission;
            Engine.IsApproveRejectGenericSuggestion = listPermission.Any(x => x.PermissionName == "is approve reject generic suggestion");

        }

    }
}
