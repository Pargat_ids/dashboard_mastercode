﻿using CRA_DataAccess;
using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;

namespace AccessVersionControlSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public MainWindow()
        {
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            if (!VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenExistingInstance)
            {
                InitializeComponent();
                FetchLastCompileDate();
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }

                if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }           
        }
        private void FetchLastCompileDate()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            DateTime buildDate = new DateTime(2000, 1, 1)
                .AddDays(version.Build)
                .AddSeconds(version.Revision * 2);
            string tmp = " ";
            double tmpWidth = 0;
            for (; ((tmpWidth + 1) < 150);)
            {
                tmp += " ";
                tmpWidth = (tmpWidth + 1);
            }
            this.Title = "CRA Dashboard    " + tmp + "Tool Version: " + buildDate.ToString("MM/dd/yyyy") + "          ( " + Engine.ToolDbType + " )";// +"            Development ";

        }


    }
}
