﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
      
        public Visibility visibilityExpandMore_Hydrates { get; set; }
        public Visibility visibility_Hydrates { get; set; }
        public ICommand CommandHydrates { get; set; }
        //public ICommand CommandShowPopUpAddHydrate { get; private set; }
        public GenericManagementTool.ViewModels.Hydrate_VM comonHydrates_VM { get; set; }
       

        private void CommandHydratesExecute(object obj)
        {
            visibility_Hydrates = GenericVisibleCollapsedFunction(visibility_Hydrates);
            visibilityExpandMore_Hydrates = GenericVisibleCollapsedFunction_Expand(visibility_Hydrates);
            NotifyPropertyChanged("visibility_Hydrates");
            NotifyPropertyChanged("visibilityExpandMore_Hydrates");
            //ExpanderViewHydrates = ExpanderVisibilityFunction(visibility_Hydrates);
            //NotifyPropertyChanged("ExpanderViewHydrates");
            //ExpanderVisibilityFunctionShow(visibility_Hydrates);
        }

        private void getHydratesDetail()
        {
            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                if (!string.IsNullOrEmpty(CASVal) && !string.IsNullOrWhiteSpace(CASVal))
                {
                    comonHydrates_VM = new GenericManagementTool.ViewModels.Hydrate_VM(CASVal);
                    NotifyPropertyChanged("comonHydrates_VM");
                }
            });
        }
    }   
}
