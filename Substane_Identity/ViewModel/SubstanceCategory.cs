﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibilityExpandMore_SubstanceCategory { get; set; }
        public ICommand CommandSubstanceCategory { get; set; }
        public Visibility visibility_SubstanceCategory { get; set; }

        public ICommand SubstanceCategoryUpdate { get; set; }
        public ICommand SubstanceCategoryDelete { get; set; }
        public CommonDataGrid_ViewModel<ListSubstanceCategory_VM> comonSubstanceCategory_VM { get; set; }
        public List<LibSubCategory> listSubstanceCategory { get; set; }
       
        public string SubstanceCategoryContent { get; set; }
        private ObservableCollection<ListSubstanceCategory_VM> _listChanges_SubstanceCategory { get; set; } = new ObservableCollection<ListSubstanceCategory_VM>();
        public ObservableCollection<ListSubstanceCategory_VM> ListChanges_SubstanceCategory
        {
            get { return _listChanges_SubstanceCategory; }
            set
            {
                if (value != _listChanges_SubstanceCategory)
                {
                    _listChanges_SubstanceCategory = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListSubstanceCategory_VM>>>(new PropertyChangedMessage<List<ListSubstanceCategory_VM>>(_listChanges_SubstanceCategory.ToList(), _listChanges_SubstanceCategory.ToList(), "Default List"));
                }
            }
        }
        private LibSubCategory _SelectedSubStanceCategory { get; set; }
        public LibSubCategory SelectedSubStanceCategory
        {
            get => _SelectedSubStanceCategory;
            set
            {
                if (Equals(_SelectedSubStanceCategory, value))
                {
                    return;
                }

                _SelectedSubStanceCategory = value;
            }
        }
        private void getSubstanceCategoryDetail()
        {
            var result = objCommon.DbaseQueryReturnStringSQL("Select top 1 SubstanceCategoryName from Map_SubstanceCategory a, library_cas b, Library_SubstanceCategories c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.SubStanceCategoryID = c.SubStanceCategoryID", Win_Administrator_VM.CommonListConn);
            //ListChanges_SubstanceCategory = new ObservableCollection<ListSubstanceCategory_VM>(result);
            SubstanceCategoryContent = result;
            NotifyPropertyChanged("SubstanceCategoryContent");
            SelectedSubStanceCategory = listSubstanceCategory.Where(x => x.SubstanceCategoryName == result).FirstOrDefault();
            NotifyPropertyChanged("SelectedSubStanceCategory");
        }
        private List<CommonDataGridColumn> GetListSubstanceCategory()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceCategoryID", ColBindingName = "SubstanceCategoryID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceCategory", ColBindingName = "SubstanceCategory", ColType = "Textbox", ColumnWidth = "500" });
            return listColumn;
        }
        private void SubstanceCategoryUpdateExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to update the Substance Category", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                List<Log_Map_SubstanceCategory> lstlog = new List<Log_Map_SubstanceCategory>();
                string status = string.Empty;
                using (var context = new CRAModel())
                {
                    var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                    var itemToUpdate = context.Map_SubstanceCategory.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                    if (itemToUpdate == null)
                    {
                        var main = new Map_SubstanceCategory
                        {
                            CASID = casId,
                            SubstanceCategoryID = SelectedSubStanceCategory.SubstanceCategoryID,
                        };
                        if (itemToUpdate == null)// means no record exist previously
                        {
                            List<Map_SubstanceCategory> lstmain = new List<Map_SubstanceCategory>();
                            lstmain.Add(main);
                            _objLibraryFunction_Service.BulkIns<Map_SubstanceCategory>(lstmain);
                            status = "A";
                            itemToUpdate = context.Map_SubstanceCategory.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if (itemToUpdate.SubstanceCategoryID != SelectedSubStanceCategory.SubstanceCategoryID)
                        {
                            objCommon.DbaseQueryReturnStringSQL("update Map_SubstanceCategory set SubstanceCategoryID = " + SelectedSubStanceCategory.SubstanceCategoryID + " where MappingSubstanceCategoryID =" + itemToUpdate.MappingSubstanceCategoryID, Win_Administrator_VM.CommonListConn);
                            status = "C";
                        }
                    }
                    if (status != string.Empty)
                    {
                        var log = new Log_Map_SubstanceCategory
                        {
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Status = status,
                            CASID = casId,
                            SubstanceCategoryID = SelectedSubStanceCategory.SubstanceCategoryID,
                            MappingSubstanceCategoryID = itemToUpdate.MappingSubstanceCategoryID,
                        };
                        lstlog.Add(log);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceCategory>(lstlog);
                        _notifier.ShowSuccess("Substance Category Update Successfully");
                        MaterialDesignThemes.Wpf.Flipper.FlipCommand.Execute(obj, null);
                    }

                }
            }
            SearchCommandExecute(obj);
        }
        private void SubstanceCategoryDeleteExecute(object obj)
        {

        }
        private void CommandSubstanceCategoryExecute(object obj)
        {
            visibility_SubstanceCategory = GenericVisibleCollapsedFunction(visibility_SubstanceCategory);
            visibilityExpandMore_SubstanceCategory = GenericVisibleCollapsedFunction_Expand(visibility_SubstanceCategory);
            NotifyPropertyChanged("visibility_SubstanceCategory");
            NotifyPropertyChanged("visibilityExpandMore_SubstanceCategory");
        }
    }
    public class ListSubstanceCategory_VM
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int SubstanceCategoryID { get; set; }
        public string SubstanceCategoryName { get; set; }
    }

    public class LibSubCategory
    {
        public int SubstanceCategoryID { get; set; }
        public string SubstanceCategoryName { get; set; }
    }
}
