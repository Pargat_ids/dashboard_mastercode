﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibleDataGridAddNewList { get; set; }
        public Visibility visibility_DirectHit { get; set; }
        public Visibility visibilityExpandMore_DirectHit { get; set; }
        public ICommand CommandDirectHit { get; set; }
        public CommonDataGrid_ViewModel<ListDirectHit_VM> comonDirectHit_VM { get; set; }
        private ObservableCollection<ListDirectHit_VM> _listChanges_DirectHit { get; set; } = new ObservableCollection<ListDirectHit_VM>();
        public ObservableCollection<ListDirectHit_VM> ListChanges_DirectHit
        {
            get { return _listChanges_DirectHit; }
            set
            {
                if (value != _listChanges_DirectHit)
                {
                    _listChanges_DirectHit = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListDirectHit_VM>>>(new PropertyChangedMessage<List<ListDirectHit_VM>>(_listChanges_DirectHit.ToList(), _listChanges_DirectHit.ToList(), "Default List"));
                }
            }
        }

        private void getDirectHitDetail()
        {
            visibleDataGridAddNewList = Visibility.Visible;
            NotifyPropertyChanged("visibleDataGridAddNewList");
            Task.Run(() =>
            {

                visibleDataGridAddNewList = Visibility.Collapsed;
                NotifyPropertyChanged("visibleDataGridAddNewList");
            });
            //var result = objCommon.DbaseQueryReturnSqlList<ListDirectHit_VM>("Select distinct a.Qsid_ID, Qsid from qsxxx_cas a, library_cas b, library_Qsids c where a.CASID = b.CASId and a.qsid_id = c.qsid_id and b.cas ='" + CAS + "'", Win_Administrator_VM.CommonListConn);
            var result = objCommon.DbaseQueryReturnTableSql("select distinct a.Qsid_ID, Qsid, f.ListFieldName, f.ShortName,f.Country, f.Module, f.ListPhrase, isnull(x.phrase_name,'') as ListVertical, i.Product_Name " +
                        " from qsxxx_cas a inner join Library_CAS b on a.CASID = b.CASId inner " +
                        " join library_Qsids c on a.qsid_id = c.qsid_id " +
                        " left join (select d.QSID_ID, " +
                        "  max(case when PhraseCategoryID = 20 then Phrase end) ListFieldName, " +
                        "  max(case when PhraseCategoryID = 15 then Phrase end) ShortName, " +
                        "  max(case when PhraseCategoryID = 14 then Phrase end) ListPhrase, " +
                        "  max(case when PhraseCategoryID = 97 then Phrase end) ListIndustryVertical, " +
                        " max(case when PhraseCategoryID = 21 then Phrase end) Country, " +
                        " max(case when PhraseCategoryID = 103 then Phrase end) Module " +
                        " from QSxxx_ListDictionary d inner " +
                        " join Library_Phrases e on d.PhraseID = e.PhraseID " +
                        " group by QSID_ID) f on f.QSID_ID = a.QSID_ID " +
                        " left join " +
                         " (select distinct ss.QSID_ID, " +
                 "   phrase_name = STUFF((SELECT N', ' + p2.phrase " +
                 "  FROM(select phrase, QSID_ID from Map_Vertical_Lists g inner " +
                 "                              join Library_Phrases h on " +
                 "   g.VerticalID = h.PhraseID) p2 " +
                 "  WHERE p2.QSID_ID = ss.QSID_ID " +
                 "  order by p2.phrase " +
                 "  FOR XML PATH(N'')), 1, 2, N'') " +
                 "  from Map_Vertical_Lists ss) x on x.QSID_ID = a.QSID_ID " +
                         " left join (select g.QSID_ID, " +
                          " Product_Name = STUFF((SELECT N', ' + p2.ProductName " +
                          " FROM(select ProductName, QSID_ID from Map_Product_Lists g inner join Library_Products h on g.ProductID =" +
                          " h.ProductID) p2 " +
                          " WHERE p2.QSID_ID = g.QSID_ID " +
                          " order by p2.ProductName " +
                          " FOR XML PATH(N'')), 1, 2, N'') " +
                          " from Map_Product_Lists g " +
                          " inner " +
                          " join Library_Products h on g.ProductID = h.ProductID GROUP BY QSID_ID) i on i.QSID_ID = a.QSID_ID " +
                        " where b.CAS = '" + CASVal + "'", Win_Administrator_VM.CommonListConn);
            var listResult = result.AsEnumerable().Select(x => new ListDirectHit_VM()
            {

                Country = string.IsNullOrEmpty(x.Field<string>("Country")) ? new ListCountries_ViewModel() : Engine._listCountry.Where(y => y.Name.ToLower() == x.Field<string>("Country").ToLower()).FirstOrDefault(),
                Module = string.IsNullOrEmpty(x.Field<string>("Module")) ? new ListModule_ViewModel() : Engine._listModule.Where(y => y.Name.ToLower() == x.Field<string>("Module").ToLower()).FirstOrDefault(),
                ListFieldName = x.Field<string>("ListFieldName"),
                ListIndustryVertical = x.Field<string>("ListVertical"),
                ListPhrase = x.Field<string>("ListPhrase"),
                Product_Name = x.Field<string>("Product_Name"),
                Qsid = new List<HyperLinkDataValue>() { new HyperLinkDataValue() { DisplayValue = x.Field<string>("Qsid"), NavigateData = x.Field<int>("QSID_ID") } },
                QSID_ID = x.Field<int>("QSID_ID"),
                ShortName = x.Field<string>("ShortName"),
                QsidFilter= x.Field<string>("Qsid")

            }).ToList();
            ListChanges_DirectHit = new ObservableCollection<ListDirectHit_VM>(listResult);
        }
        private List<CommonDataGridColumn> GetListDirectHit()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150",ColFilterMemberPath= "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListFieldName", ColBindingName = "ListFieldName", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ShortName", ColBindingName = "ShortName", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListPhrase", ColBindingName = "ListPhrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Combobox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Module", ColBindingName = "Module", ColType = "Combobox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListIndustryVertical", ColBindingName = "ListIndustryVertical", ColType = "Textbox", ColumnWidth = "250" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product_Name", ColBindingName = "Product_Name", ColType = "Textbox", ColumnWidth = "250" });

            return listColumn;
        }
        private void CommandDirectHitExecute(object obj)
        {
            visibility_DirectHit = GenericVisibleCollapsedFunction(visibility_DirectHit);
            visibilityExpandMore_DirectHit = GenericVisibleCollapsedFunction_Expand(visibility_DirectHit);
            NotifyPropertyChanged("visibility_DirectHit");
            NotifyPropertyChanged("visibilityExpandMore_DirectHit");
            //ExpanderViewDirectHit = ExpanderVisibilityFunction(visibility_DirectHit);
            //NotifyPropertyChanged("ExpanderViewDirectHit");
            //ExpanderVisibilityFunctionShow(visibility_DirectHit);
        }
    }
    public class ListDirectHit_VM
    {
        public int QSID_ID { get; set; }
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string ListFieldName { get; set; }
        public string ShortName { get; set; }
        public string ListPhrase { get; set; }
        public string ListIndustryVertical { get; set; }
        public string Product_Name { get; set; }
        public ListCountries_ViewModel Country { get; set; }
        public ListModule_ViewModel Module { get; set; }
        public string QsidFilter { get; set; }
    }
}
