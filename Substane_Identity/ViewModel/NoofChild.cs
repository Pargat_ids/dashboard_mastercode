﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;
using System.Text.RegularExpressions;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool IsChangeNoofChild { get; set; }
        public string NoofChild_ChildCAS { get; set; }
        public string NoofChild_ParentChemicalName { get; set; }
        public string NoofChild_ChildChemicalName { get; set; }
        public Visibility visibilityExpandMore_NoofChild { get; set; }
        public Visibility visibility_NoofChild { get; set; }
        public SubstanceIdentity_GenericChild SelectedPropertyNoofChild { get; set; }
        public ICommand CommandNoofChild { get; set; }
        public List<LibType> listCASType { get; set; } = new List<LibType>();
        public Visibility IsShowPopUpNoofChild { get; set; } = Visibility.Collapsed;
        public bool IsEditNoofChild { get; set; } = true;
        public string CasType { get; set; } = "";
        public bool IsEditChildCAS { get; set; } = true;
        public ICommand CommandClosePopUpNoofChild { get; private set; }
        public ICommand CommandShowPopUpAddNoofChild { get; private set; }
        public ICommand CommandAddNoofChild { get; private set; }
        public ICommand CommandAddMoreNoofChild { get; private set; }
        public CommonDataGrid_ViewModel<SubstanceIdentity_GenericChild> comonNoofChild_VM { get; set; }

        private void NotifyMeNoofChild(PropertyChangedMessage<SubstanceIdentity_GenericChild> obj)
        {
            SelectedPropertyNoofChild = obj.NewValue;
        }
        private void CommandAddMoreNoofChildExecute(object obj)
        {
            SaveIUpacName();
        }
        private List<SubstanceIdentity_GenericChild> _listChanges_NoofChild { get; set; } = new List<SubstanceIdentity_GenericChild>();
        public List<SubstanceIdentity_GenericChild> ListChanges_NoofChild
        {
            get { return _listChanges_NoofChild; }
            set
            {
                if (value != _listChanges_NoofChild)
                {
                    _listChanges_NoofChild = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SubstanceIdentity_GenericChild>>>(new PropertyChangedMessage<List<SubstanceIdentity_GenericChild>>(null, _listChanges_NoofChild, "Default List"));
                }
            }
        }
        private LibType _SelectedCASType { get; set; }
        public LibType SelectedCASType
        {
            get => _SelectedCASType;
            set
            {
                if (Equals(_SelectedCASType, value))
                {
                    return;
                }

                _SelectedCASType = value;
            }
        }
        private void getNoofChildDetail()
        {
            ListChanges_NoofChild = _objLibraryFunction_Service.GetAllChildCasFromFlatGenerics(CASVal, "OnlyChildCas").GroupBy(x => new
            {
                x.ChemicalName,
                x.ChemicalNameID,
                x.ParentChemicalName,
                x.ParentChemicalNameID,
                x.ParentCas,
                x.ParentCasID,
                x.ChildCas,
                x.ChildCasID,
                x.IsCalculatedGroup
            })
                .Select(x => new SubstanceIdentity_GenericChild()
                {
                    ChemicalName = x.Key.ChemicalName,
                    ChemicalNameID = x.Key.ChemicalNameID,
                    ParentChemicalName = x.Key.ParentChemicalName,
                    ParentChemicalNameID = x.Key.ParentChemicalNameID,
                    ChildCas = x.Key.ChildCas,
                    ChildCasID = x.Key.ChildCasID,
                    ParentCas = x.Key.ParentCas,
                    ParentCasID = x.Key.ParentCasID,
                    IsCalculatedGroup = x.Key.IsCalculatedGroup,
                    Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => new HyperLinkDataValue() { DisplayValue = y.Qsid + " ( " + y.ListFieldName + " )", NavigateData = y.Qsid_Id }).Distinct().ToList(),
                    QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Count(),
                    QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => y.Qsid).ToList())
                }).ToList();
        }
        private List<CommonDataGridColumn> GetListNoofChild()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCasID", ColBindingName = "ParentCasID", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCas", ColBindingName = "ParentCas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCasID", ColBindingName = "ChildCasID", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCas", ColBindingName = "ChildCas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentChemicalName", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Delete Link", BackgroundColor = "#ff414d", CommandParam = "DeleteNoofChild" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Update Link", BackgroundColor = "#07689f", CommandParam = "AddNoofChild" });
            return listColumn;
        }
        private void CommandNoofChildExecute(object obj)
        {

            visibility_NoofChild = GenericVisibleCollapsedFunction(visibility_NoofChild);
            visibilityExpandMore_NoofChild = GenericVisibleCollapsedFunction_Expand(visibility_NoofChild);
            NotifyPropertyChanged("visibility_NoofChild");
            NotifyPropertyChanged("visibilityExpandMore_NoofChild");
            //ExpanderViewNoofChild = ExpanderVisibilityFunction(visibility_NoofChild);
            //NotifyPropertyChanged("ExpanderViewNoofChild");
            //ExpanderVisibilityFunctionShow(visibility_NoofChild);
        }

        private void CommandShowPopUpAddNoofChildExecute(object obj)
        {
            ClearPopUpVariable();
            ClearVariable();
            CasType = obj.ToString();
            NotifyPropertyChanged("CasType");
            if (CasType == "Parent")
            {
                IsEnabledChildCas = false;
                IsEnabledParentCas = true;
                NotifyPropertyChanged("IsEnabledParentCas");
                NotifyPropertyChanged("IsEnabledChildCas");
                ChildCas = CAS;
                CommandLostFocusChildCas.Execute(null);
            }
            else
            {
                ParentCas = CAS;
                CommandLostFocusParentCas.Execute(null);
                IsEnabledChildCas = true;
                IsEnabledParentCas = false;
                NotifyPropertyChanged("IsEnabledParentCas");
                NotifyPropertyChanged("IsEnabledChildCas");
            }

            IsChangeNoofChild = false;

            IsShowPopUp = true;
            IsShowPopUpNoofChild = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpNoofChild");
            //IsEditChildCAS = true;
            //NotifyPropertyChanged("IsEditChildCAS");

        }
        private void CommandClosePopUpNoofChildExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpNoofChild = Visibility.Collapsed;
            ExpanderViewIUPAC = Visibility.Visible;
            NotifyPropertyChanged("ExpanderViewIUPAC");
        }
        private void CommandAddNoofChildExecute(object obj)
        {

            SaveNoofChild();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);

        }

        public void SaveNoofChild()
        {
            if (CASVal == string.Empty || NoofChild_ChildCAS == string.Empty || SelectedCASType.TypName == string.Empty)
            {
                _notifier.ShowError("CAS & Child CAS & Type Name can't be empty");
                return;
            }
            using (var context = new CRAModel())
            {
                int ParentcasId = 0;
                int ChildcasId = 0;
                if (CasType == "Child")
                {
                    ParentcasId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                    ChildcasId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == NoofChild_ChildCAS).Select(y => y.CASID).FirstOrDefault();
                    if (ChildcasId == 0)
                    {
                        //add in library_CAS
                        var lib = new Library_CAS
                        {
                            CAS = NoofChild_ChildCAS,
                            TimeStamp = objCommon.ESTTime(),
                            IsValid = objCommon.CheckIsValidCRN(NoofChild_ChildCAS),
                            IsInternalOnlyCAS = Regex.IsMatch(NoofChild_ChildCAS, "[a-z]"),
                            IsActive = true,
                        };
                        context.Library_CAS.Add(lib);
                        context.SaveChanges();
                    }
                }
                else
                {
                    ParentcasId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == NoofChild_ChildCAS).Select(y => y.CASID).FirstOrDefault();
                    ChildcasId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                    if (ParentcasId == 0)
                    {
                        //add in library_CAS
                        var lib = new Library_CAS
                        {
                            CAS = NoofChild_ChildCAS,
                            TimeStamp = objCommon.ESTTime(),
                            IsValid = objCommon.CheckIsValidCRN(NoofChild_ChildCAS),
                            IsInternalOnlyCAS = Regex.IsMatch(NoofChild_ChildCAS, "[a-z]"),
                            IsActive = true,
                        };
                        context.Library_CAS.Add(lib);
                        context.SaveChanges();
                    }
                }
                if (ParentcasId == 0 || ChildcasId == 0)
                {
                    _notifier.ShowError("Cas not added due to internal server error!");
                }
                List<IStripChar> listStripChar = GetStripCharacter();
                //parent ChemicalName
                if (NoofChild_ParentChemicalName.Trim() != string.Empty)
                {
                    NoofChild_ParentChemicalName = objCommon.RemoveWhitespaceCharacter(NoofChild_ParentChemicalName, listStripChar);
                    NoofChild_ParentChemicalName = System.Text.RegularExpressions.Regex.Replace(NoofChild_ParentChemicalName, @"\s+", " ").Trim();
                    var chkExist = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == NoofChild_ParentChemicalName).FirstOrDefault();
                    if (chkExist == null)
                    {
                        string squash = objCommon.FullSquash(NoofChild_ParentChemicalName);
                        var lib = new Library_ChemicalNames
                        {
                            ChemicalName = NoofChild_ParentChemicalName,
                            LanguageID = 1,
                            ChemicalNameHash = this.objCommon.GenerateSHA256String(NoofChild_ParentChemicalName),
                            ChemicalNameSquashHash = this.objCommon.GenerateSHA256String(squash),
                            ChemicalNameSquash = squash,
                            ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(NoofChild_ParentChemicalName),
                            TimeStamp = objCommon.ESTTime(),
                            IsActive = true,
                        };
                        context.Library_ChemicalNames.Add(lib);
                        context.SaveChanges();
                    }
                }

                //Child ChemicalName
                if (NoofChild_ChildChemicalName.Trim() != string.Empty)
                {
                    NoofChild_ChildChemicalName = objCommon.RemoveWhitespaceCharacter(NoofChild_ChildChemicalName, listStripChar);
                    NoofChild_ChildChemicalName = System.Text.RegularExpressions.Regex.Replace(NoofChild_ChildChemicalName, @"\s+", " ").Trim();
                    var chkExistChild = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == NoofChild_ChildChemicalName).FirstOrDefault();
                    if (chkExistChild == null)
                    {
                        string squash = objCommon.FullSquash(NoofChild_ChildChemicalName);
                        var lib = new Library_ChemicalNames
                        {
                            ChemicalName = NoofChild_ChildChemicalName,
                            LanguageID = 1,
                            ChemicalNameHash = this.objCommon.GenerateSHA256String(NoofChild_ChildChemicalName),
                            ChemicalNameSquashHash = this.objCommon.GenerateSHA256String(squash),
                            ChemicalNameSquash = squash,
                            ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(NoofChild_ChildChemicalName),
                            TimeStamp = objCommon.ESTTime(),
                            IsActive = true,
                        };
                        context.Library_ChemicalNames.Add(lib);
                        context.SaveChanges();
                    }
                }

                var ParentchemId = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == NoofChild_ParentChemicalName).Select(y => y.ChemicalNameID).FirstOrDefault();
                var ChildchemId = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == NoofChild_ChildChemicalName).Select(y => y.ChemicalNameID).FirstOrDefault();
                var chkAlready = context.Map_Generics_Combined_Hydrates_AlternateCAS_etc.AsNoTracking().Where(x => x.ParentCasID == ParentcasId && x.ChildCasID == ChildcasId).FirstOrDefault();
                var Stus = string.Empty;
                int ID = 0;
                if (chkAlready == null)
                {
                    Stus = "A";
                    var lgMain = new Map_Generics_Combined_Hydrates_AlternateCAS_etc();
                    lgMain.ParentCasID = ParentcasId;
                    lgMain.ChildCasID = ChildcasId;
                    lgMain.Type = SelectedCASType.TypName;
                    context.Map_Generics_Combined_Hydrates_AlternateCAS_etc.Add(lgMain);
                    context.SaveChanges();

                    ID = lgMain.CombinedGenerics_ID;
                }
                else
                {
                    Stus = "C";
                    ID = chkAlready.CombinedGenerics_ID;

                    List<Map_Generics_Combined_Hydrates_AlternateCAS_etc> lstMain = new List<Map_Generics_Combined_Hydrates_AlternateCAS_etc>();
                    var lgMain = context.Map_Generics_Combined_Hydrates_AlternateCAS_etc.AsNoTracking().Where(x => x.ParentCasID == ParentcasId && x.ChildCasID == ChildcasId).FirstOrDefault();
                    lgMain.Type = SelectedCASType.TypName; ;
                    lstMain.Add(lgMain);
                    EFBatchOperation.For(context, context.Map_Generics_Combined_Hydrates_AlternateCAS_etc).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.Type));
                    context.SaveChanges();
                }
                List<Log_Map_Generics_Combined_Hydrates_AlternateCAS_etc> lstLog = new List<Log_Map_Generics_Combined_Hydrates_AlternateCAS_etc>();
                var lgLog = new Log_Map_Generics_Combined_Hydrates_AlternateCAS_etc
                {
                    ParentCasID = ParentcasId,
                    ChildCasID = ChildcasId,
                    Status = Stus,
                    SessionID = Engine.CurrentUserSessionID,
                    Type = SelectedCASType.TypName,
                    Timestamp = objCommon.ESTTime(),
                    UserID = context.Log_Sessions.AsNoTracking().Where(x => x.SessionID == Engine.CurrentUserSessionID).Select(y => y.UserName).FirstOrDefault(),

                };
                lstLog.Add(lgLog);
                _objLibraryFunction_Service.BulkIns<Log_Map_Generics_Combined_Hydrates_AlternateCAS_etc>(lstLog);



                if (ParentchemId != 0)
                {
                    var parentStatus = string.Empty;
                    int parentID = 0;
                    var chkExist = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ParentcasId).FirstOrDefault();
                    if (chkExist == null)
                    {
                        parentStatus = "A";
                        var lgMainParent = new Map_PreferredSubstanceName();
                        lgMainParent.CASID = ParentcasId;
                        lgMainParent.ChemicalNameID = ParentchemId;
                        context.Map_PreferredSubstanceName.Add(lgMainParent);
                        context.SaveChanges();

                        parentID = lgMainParent.PreferredSubstanceNameID;
                    }
                    else
                    {
                        parentStatus = "C";
                        parentID = chkExist.PreferredSubstanceNameID;

                        List<Map_PreferredSubstanceName> lstMain = new List<Map_PreferredSubstanceName>();
                        var lgMain = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ParentcasId).FirstOrDefault();
                        lgMain.ChemicalNameID = ParentchemId;
                        lstMain.Add(lgMain);
                        EFBatchOperation.For(context, context.Map_PreferredSubstanceName).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.ChemicalNameID));
                        context.SaveChanges();

                    }
                    List<Log_Map_PreferredSubstanceName> lstLogParent = new List<Log_Map_PreferredSubstanceName>();
                    var lgLogParent = new Log_Map_PreferredSubstanceName
                    {
                        CASID = ParentcasId,
                        ChemicalNameID = ParentchemId,
                        Status = parentStatus,
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        PreferredSubstanceNameID = parentID,
                    };
                    lstLogParent.Add(lgLogParent);
                    _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(lstLogParent);
                }

                if (ChildchemId != 0)
                {
                    var childtStatus = string.Empty;
                    int childID = 0;
                    var chkExist = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ChildcasId).FirstOrDefault();
                    if (chkExist == null)
                    {
                        childtStatus = "A";
                        var lgMainChild = new Map_PreferredSubstanceName();
                        lgMainChild.CASID = ChildcasId;
                        lgMainChild.ChemicalNameID = ChildchemId;
                        context.Map_PreferredSubstanceName.Add(lgMainChild);
                        context.SaveChanges();

                        childID = lgMainChild.PreferredSubstanceNameID;
                    }
                    else
                    {
                        childtStatus = "C";
                        childID = chkExist.PreferredSubstanceNameID;

                        List<Map_PreferredSubstanceName> lstMain = new List<Map_PreferredSubstanceName>();
                        var lgMain = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ChildcasId).FirstOrDefault();
                        lgMain.ChemicalNameID = ChildchemId;
                        lstMain.Add(lgMain);
                        EFBatchOperation.For(context, context.Map_PreferredSubstanceName).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.ChemicalNameID));
                        context.SaveChanges();

                    }
                    List<Log_Map_PreferredSubstanceName> lstLogChild = new List<Log_Map_PreferredSubstanceName>();
                    var lgLogChild = new Log_Map_PreferredSubstanceName
                    {
                        CASID = ChildcasId,
                        ChemicalNameID = ChildchemId,
                        Status = childtStatus,
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        PreferredSubstanceNameID = childID,
                    };
                    lstLogChild.Add(lgLogChild);
                    _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(lstLogChild);
                }
                _notifier.ShowSuccess("Child Updated successfully");
            }

        }
        private void CommandButtonExecutedNoofChild(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "DeleteNoofChild")
            {
                if (SelectedPropertyNoofChild.IsCalculatedGroup == true)
                {
                    _notifier.ShowWarning("Cannot delete link because this is a calculated generic group");
                    return;
                }
                CasType = "Child";
                ShowPopUpDeleteGeneric(CASVal);

            }
            else if (obj.Notification == "AddNoofChild")
            {
                ShowPopUpAddEditName();

            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }

    }
    public class LibType
    {
        public int TypID { get; set; }
        public string TypName { get; set; }
    }
}
