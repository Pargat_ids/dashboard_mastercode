﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibility_Subroot { get; set; }
        public Visibility visibilityExpandMore_Subroot { get; set; }
        public ICommand CommandSubroot { get; set; }
        public CommonDataGrid_ViewModel<ListSubroot_VM> comonSubroot_VM { get; set; }
        private ObservableCollection<ListSubroot_VM> _listChanges_Subroot { get; set; } = new ObservableCollection<ListSubroot_VM>();
        public ObservableCollection<ListSubroot_VM> ListChanges_Subroot
        {
            get { return _listChanges_Subroot; }
            set
            {
                if (value != _listChanges_Subroot)
                {
                    _listChanges_Subroot = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListSubroot_VM>>>(new PropertyChangedMessage<List<ListSubroot_VM>>(_listChanges_Subroot.ToList(), _listChanges_Subroot.ToList(), "Default List"));
                }
            }
        }

        private void getSubrootDetail()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = d.QSID_ID) as ListFieldName";
            //var result = objCommon.DbaseQueryReturnSqlList<ListDirectHit_VM>("Select distinct a.Qsid_ID, Qsid from qsxxx_cas a, library_cas b, library_Qsids c where a.CASID = b.CASId and a.qsid_id = c.qsid_id and b.cas ='" + CAS + "'", Win_Administrator_VM.CommonListConn);
            var resultdt = objCommon.DbaseQueryReturnTableSql("select distinct QSID,b.QSID_ID, FieldName, Phrase, "+subListFieldNameQuery+" from library_cas a " +
                                                                           " inner join QSxxx_CAS b on a.CASID = b.CASID " +
                                                                           " inner " +
                                                                           " join library_qsids c on b.QSID_ID = c.QSID_ID " +
                                                                           " inner " +
                                                                           " join QSxxx_Phrases d on b.QSID_ID = d.QSID_ID and b.RowID = d.RowID " +
                                                                           " inner join Library_PhraseCategories e on d.PhraseCategoryID = e.PhraseCategoryID " +
                                                                           " inner join Library_FieldNames f on f.FieldNameID = d.FieldNameID " +
                                                                           " inner join Library_Phrases g on g.PhraseID = d.PhraseID " +
                                                                           " where a.CAS ='" + CASVal + "'  and e.PhraseCategory = 'Subroot'", Win_Administrator_VM.CommonListConn);

            var result = resultdt.AsEnumerable().GroupBy(x => new { fieldName = x.Field<string>("FieldName"), Phrase = x.Field<string>("Phrase") }).Select(g =>
                    new ListSubroot_VM()
                    {
                        Qsid = g.AsEnumerable().Where(kvp => !string.IsNullOrEmpty(kvp.Field<string>("QSID"))).Select(kvp => new HyperLinkDataValue() {DisplayValue= kvp.Field<string>("QSID") + " ( " + kvp.Field<string>("ListFieldName") + " )", NavigateData= kvp.Field<int?>("QSID_ID").ToString() }).ToList(),
                        FieldName = g.Key.fieldName,
                        Phrase = g.Key.Phrase,
                        ListCount= g.AsEnumerable().Where(kvp => !string.IsNullOrEmpty(kvp.Field<string>("QSID"))).Count(),
                        QsidFilter= String.Join(',', g.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
                        //Qsid_Ids = string.Join(", ", g.AsEnumerable().Where(kvp => kvp.Field<dynamic>("QSID_ID") != 0).Select(kvp => kvp.Field<dynamic>("QSID_ID")).ToList()),
                    }).Distinct().ToList();

            ListChanges_Subroot = new ObservableCollection<ListSubroot_VM>(result);
        }
        
        private List<CommonDataGridColumn> GetListSubroot()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No. of Lists", ColBindingName = "ListCount", ColType = "Textbox", ColumnWidth = "150" });
            return listColumn;
        }
        private void CommandSubrootExecute(object obj)
        {
            visibility_Subroot = GenericVisibleCollapsedFunction(visibility_Subroot);
            visibilityExpandMore_Subroot = GenericVisibleCollapsedFunction_Expand(visibility_Subroot);
            NotifyPropertyChanged("visibility_Subroot");
            NotifyPropertyChanged("visibilityExpandMore_Subroot");
        }
    }
    public class ListSubroot_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string QsidFilter { get; set; }
        public int QSID_ID { get; set; }
        public string FieldName { get; set; }
        public string Phrase { get; set; }
        public int ListCount { get; set; }
    }
}

