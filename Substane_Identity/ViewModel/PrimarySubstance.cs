﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibility_PrimarySubstance { get; set; }
        public ICommand CommandPrimarySubstance { get; set; }
        public Visibility visibilityExpandMore_PrimarySubstance { get; set; }
        public CommonDataGrid_ViewModel<ListPrimarySubstance_VM> comonPrimarySubstance_VM { get; set; }
        public string PrimarySubstanceContent { get; set; }
        public ICommand PrimarySubstanceUpdate { get; set; }
        public ICommand PrimarySubstanceDelete { get; set; }
        private ObservableCollection<ListPrimarySubstance_VM> _listChanges_PrimarySubstance { get; set; } = new ObservableCollection<ListPrimarySubstance_VM>();
        public ObservableCollection<ListPrimarySubstance_VM> ListChanges_PrimarySubstance
        {
            get { return _listChanges_PrimarySubstance; }
            set
            {
                if (value != _listChanges_PrimarySubstance)
                {
                    _listChanges_PrimarySubstance = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListPrimarySubstance_VM>>>(new PropertyChangedMessage<List<ListPrimarySubstance_VM>>(_listChanges_PrimarySubstance.ToList(), _listChanges_PrimarySubstance.ToList(), "Default List"));
                }
            }
        }
        private void PrimarySubstanceUpdateExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to update the Primary Substance Name", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                _notifier.ShowInformation("Primary substance name update in-progress, System will notify when it completed.");
                Task.Run(() =>
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        List<Log_Map_PreferredSubstanceName> lstlog = new List<Log_Map_PreferredSubstanceName>();
                        string status = string.Empty;
                        using (var context = new CRAModel())
                        {
                            var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                            var itemToUpdate = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                            var ChemicalName = objCommon.RemoveWhitespaceCharacter(PrimarySubstanceContent, GetStripCharacter());
                            ChemicalName = System.Text.RegularExpressions.Regex.Replace(ChemicalName, @"\s+", " ").Trim();
                            // var chemID = 0;// context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == PrimarySubstanceContent).Select(y => y.ChemicalNameID).FirstOrDefault();
                            //if (chemID == 0)
                            //{
                            var chemID = _objLibraryFunction_Service.InsertIfNotExistChemicalName(ChemicalName, true);
                            //}
                            //chemID = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == ChemicalName).Select(y => y.ChemicalNameID).FirstOrDefault();
                            if (itemToUpdate == null)
                            {
                                var main = new Map_PreferredSubstanceName
                                {
                                    CASID = casId,
                                    ChemicalNameID = chemID,
                                };
                                if (itemToUpdate == null)// means no record exist previously
                                {
                                    List<Map_PreferredSubstanceName> lstmain = new List<Map_PreferredSubstanceName>();
                                    lstmain.Add(main);
                                    _objLibraryFunction_Service.BulkIns<Map_PreferredSubstanceName>(lstmain);
                                    status = "A";
                                    itemToUpdate = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                                }
                            }
                            else
                            {
                                if (itemToUpdate.ChemicalNameID != chemID)
                                {
                                    objCommon.DbaseQueryReturnStringSQL("update Map_PreferredSubstanceName set ChemicalNameID = " + chemID + " where PreferredSubstanceNameID =" + itemToUpdate.PreferredSubstanceNameID, Win_Administrator_VM.CommonListConn);
                                    status = "C";
                                }
                            }
                            if (status != string.Empty)
                            {
                                var log = new Log_Map_PreferredSubstanceName
                                {
                                    SessionID = Engine.CurrentUserSessionID,
                                    TimeStamp = objCommon.ESTTime(),
                                    Status = status,
                                    CASID = casId,
                                    ChemicalNameID = chemID,
                                    PreferredSubstanceNameID = itemToUpdate.PreferredSubstanceNameID,
                                };
                                lstlog.Add(log);
                                _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(lstlog);
                                _notifier.ShowSuccess("Primary Substance Name Update Successfully");
                                MaterialDesignThemes.Wpf.Flipper.FlipCommand.Execute(obj, null);
                            }

                        }
                        SearchCommandExecute(obj);
                    });

                });
            }
            else
            {
                SearchCommandExecute(obj);
            }

        }
        private void PrimarySubstanceDeleteExecute(object obj)
        {
        }
        private void getPrimarySubstanceDetail()
        {
            var result = objCommon.DbaseQueryReturnStringSQL("Select top 1 C.ChemicalName from Map_PreferredSubstanceName a, library_cas b, library_ChemicalNames c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.ChemicalNameID = c.ChemicalNameID", Win_Administrator_VM.CommonListConn);
            //ListChanges_PrimarySubstance = new ObservableCollection<ListPrimarySubstance_VM>(result);
            PrimarySubstanceContent = result;
            NotifyPropertyChanged("PrimarySubstanceContent");
        }
        private void getDisplayName()
        {
            var result = objCommon.DbaseQueryReturnStringSQL("Select top 1 C.ChemicalName from Map_DisplayName a, library_cas b, library_ChemicalNames c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.ChemicalNameID = c.ChemicalNameID", Win_Administrator_VM.CommonListConn);
            
            DisplayNameSubstance = result;
            NotifyPropertyChanged("DisplayNameSubstance");
        }
        private List<CommonDataGridColumn> GetListPrimarySubstance()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalNameID", ColBindingName = "ChemicalNameID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "700" });
            return listColumn;
        }

        private void CommandPrimarySubstanceExecute(object obj)
        {
            visibility_PrimarySubstance = GenericVisibleCollapsedFunction(visibility_PrimarySubstance);
            visibilityExpandMore_PrimarySubstance = GenericVisibleCollapsedFunction_Expand(visibility_PrimarySubstance);
            NotifyPropertyChanged("visibility_PrimarySubstance");
            NotifyPropertyChanged("visibilityExpandMore_PrimarySubstance");
        }

    }
    public class ListPrimarySubstance_VM
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }

    }
}
