﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibility_GenericHit { get; set; }
        public Visibility visibilityExpandMore_GenericHit { get; set; }
        public ICommand CommandGenericHit { get; set; }
        public CommonDataGrid_ViewModel<ListGenericHit_VM> comonGenericHit_VM { get; set; }
        private ObservableCollection<ListGenericHit_VM> _listChanges_GenericHit { get; set; } = new ObservableCollection<ListGenericHit_VM>();
        public ObservableCollection<ListGenericHit_VM> ListChanges_GenericHit
        {
            get { return _listChanges_GenericHit; }
            set
            {
                if (value != _listChanges_GenericHit)
                {
                    _listChanges_GenericHit = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListGenericHit_VM>>>(new PropertyChangedMessage<List<ListGenericHit_VM>>(_listChanges_GenericHit.ToList(), _listChanges_GenericHit.ToList(), "Default List"));
                }
            }
        }
        private void getGenericHitDetail()
        {
            //var result = objCommon.DbaseQueryReturnSqlList<ListGenericHit_VM>("Select distinct a.ParentCasID, b.CAS from Map_Generics_Flat a, library_cas b where a.ParentCasID = b.CASId and b.cas ='" + CAS + "'", Win_Administrator_VM.CommonListConn);
            using (var context = new CRAModel())
            {
                var casid = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var result = objCommon.DbaseQueryReturnTableSql("select  distinct z.ParentCasID, z.CAS as ParentCAS, z.Type as PType, a.Qsid_ID, Qsid, f.ListFieldName, f.Country, f.Module, " +
                " f.ShortName, f.ListPhrase, isnull(x.phrase_name,'') as ListVertical, i.Product_Name , ff.ChemicalName as ParentSubstanceName " +
                 "        from (select distinct qsid_id, CASID from qsxxx_cas where casid in " +
                 "       (select ParentCasID from Map_Generics_Combined_Hydrates_AlternateCAS_etc where ChildCasID = " + casid + ")) " +
                 "                 a inner join Library_CAS b on a.CASID = b.CASId inner join library_Qsids c on a.qsid_id = c.qsid_id " +
                  "       left join (select d.QSID_ID, " +
                  "        max(case when PhraseCategoryID = 20 then Phrase end) ListFieldName,  " +
                  "        max(case when PhraseCategoryID = 15 then Phrase end) ShortName,  " +
                  "        max(case when PhraseCategoryID = 14 then Phrase end) ListPhrase,  " +
                  "        max(case when PhraseCategoryID = 97 then Phrase end) ListIndustryVertical, " +
                    " max(case when PhraseCategoryID = 21 then Phrase end) Country, " +
                        " max(case when PhraseCategoryID = 103 then Phrase end) Module " +
                  "       from QSxxx_ListDictionary d inner " +
                  "       join Library_Phrases e on d.PhraseID = e.PhraseID " +
                  "       group by QSID_ID) f on f.QSID_ID = a.QSID_ID " +
                  " left join " +
                 " (select distinct ss.QSID_ID, " +
                 "   phrase_name = STUFF((SELECT N', ' + p2.phrase " +
                 "  FROM(select phrase, QSID_ID from Map_Vertical_Lists g inner " +
                 "                              join Library_Phrases h on " +
                 "   g.VerticalID = h.PhraseID) p2 " +
                 "  WHERE p2.QSID_ID = ss.QSID_ID " +
                 "  order by p2.phrase " +
                 "  FOR XML PATH(N'')), 1, 2, N'') " +
                 "  from Map_Vertical_Lists ss) x on x.QSID_ID = a.QSID_ID " +
                 " left join (select g.QSID_ID, " +
                  " Product_Name = STUFF((SELECT N', ' + p2.ProductName " +
                  " FROM(select ProductName, QSID_ID from Map_Product_Lists g inner join Library_Products h on " +
                  " g.ProductID = h.ProductID) p2 " +
                  " WHERE p2.QSID_ID = g.QSID_ID " +
                  " order by p2.ProductName " +
                  " FOR XML PATH(N'')), 1, 2, N'') " +
                  " from Map_Product_Lists g " +
                  " inner " +
                  " join Library_Products h on g.ProductID = h.ProductID GROUP BY QSID_ID) i on i.QSID_ID = a.QSID_ID " +
                 " left join " +
                 " (select ParentCasID, ChildCasID, CAS, Type from Map_Generics_Combined_Hydrates_AlternateCAS_etc g inner " +
                 " join " +
                 " library_cas h on g.ParentCasID = h.CASID where ChildCasID = " + casid + ") z on z.ParentCasID = a.CASID " +
                 " left join " +
                 " Map_PreferredSubstanceName ii on  z.ParentCasID = ii.CASID " +
                 " left join " +
                 " Library_ChemicalNames ff on ii.ChemicalNameID = ff.ChemicalNameID where c.QSID not like 'QSPC%' and c.QSID not like 'QSTO%' and c.QSID not like 'QSEC%' and c.QSID not like 'QSNI%'", Win_Administrator_VM.CommonListConn);
                var listResult = result.AsEnumerable().Select(x => new ListGenericHit_VM()
                {
                    ParentCAS = x.Field<string>("ParentCAS"),
                    ParentCASID = x.Field<int>("ParentCASID"),
                    ParentSubstanceName = x.Field<string>("ParentSubstanceName"),
                    Type = x.Field<string>("PType"),
                    Country = string.IsNullOrEmpty(x.Field<string>("Country")) ? new ListCountries_ViewModel() : Engine._listCountry.Where(y => y.Name.ToLower() == x.Field<string>("Country").ToLower()).FirstOrDefault(),
                    Module = string.IsNullOrEmpty(x.Field<string>("Module")) ? new ListModule_ViewModel() : Engine._listModule.Where(y => y.Name.ToLower() == x.Field<string>("Module").ToLower()).FirstOrDefault(),
                    ListFieldName = x.Field<string>("ListFieldName"),
                    ListVertical = x.Field<string>("ListVertical"),
                    ListPhrase = x.Field<string>("ListPhrase"),
                    Product_Name = x.Field<string>("Product_Name"),
                    Qsid = new List<HyperLinkDataValue>() { new HyperLinkDataValue() { DisplayValue = x.Field<string>("Qsid"), NavigateData = x.Field<int>("QSID_ID") } },
                    QSID_ID = x.Field<int>("QSID_ID"),
                    ShortName = x.Field<string>("ShortName"),
                    QsidFilter = x.Field<string>("Qsid")

                }).ToList();
                ListChanges_GenericHit = new ObservableCollection<ListGenericHit_VM>(listResult);
            }
        }
        private List<CommonDataGridColumn> GetListGenericHit()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150",ColFilterMemberPath= "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCASID", ColBindingName = "ParentCASID", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCAS", ColBindingName = "ParentCAS", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentSubstanceName", ColBindingName = "ParentSubstanceName", ColType = "Textbox", ColumnWidth = "350" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox", ColumnWidth = "150" });

            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListFieldName", ColBindingName = "ListFieldName", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ShortName", ColBindingName = "ShortName", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Combobox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Module", ColBindingName = "Module", ColType = "Combobox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListPhrase", ColBindingName = "ListPhrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListIndustryVertical", ColBindingName = "ListIndustryVertical", ColType = "Textbox", ColumnWidth = "250" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Product_Name", ColBindingName = "Product_Name", ColType = "Textbox", ColumnWidth = "250" });
            return listColumn;
        }
        private void CommandGenericHitExecute(object obj)
        {
            visibility_GenericHit = GenericVisibleCollapsedFunction(visibility_GenericHit);
            visibilityExpandMore_GenericHit = GenericVisibleCollapsedFunction_Expand(visibility_GenericHit);
            NotifyPropertyChanged("visibility_GenericHit");
            NotifyPropertyChanged("visibilityExpandMore_GenericHit");
            //ExpanderViewGenericHit = ExpanderVisibilityFunction(visibility_GenericHit);
            //NotifyPropertyChanged("ExpanderViewGenericHit");
            //ExpanderVisibilityFunctionShow(visibility_GenericHit);
        }
    }
    public class ListGenericHit_VM
    {
        public int ParentCASID { get; set; }
        public string ParentCAS { get; set; }
        public string ParentSubstanceName { get; set; }
        public string Type { get; set; }
        public int QSID_ID { get; set; }
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string ListFieldName { get; set; }
        public string ShortName { get; set; }
        public string ListPhrase { get; set; }
        public string ListVertical { get; set; }
        public string Product_Name { get; set; }
        public ListCountries_ViewModel Country { get; set; }
        public ListModule_ViewModel Module { get; set; }
        public string QsidFilter { get; set; }
    }
}
