﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private List<ListSynonyms_VM> _listChanges_Synonyms { get; set; } = new List<ListSynonyms_VM>();
        private List<ListSynonymsDisapproved_VM> _listChangesDisapproved_Synonyms { get; set; } = new List<ListSynonymsDisapproved_VM>();
        public ListSynonymsDisapproved_VM SelectedPropertyDisapprovedSynonyms { get; set; }
        public Visibility IsShowPopUpSynonyms { get; set; } = Visibility.Collapsed;
        public ListSynonyms_VM SelectedPropertySynonyms { get; set; }
        public string Synonyms_ChemicalName { get; set; }
        public ICommand CommandAddSynonyms { get; private set; }
        public ICommand CommandAddMoreSynonyms { get; private set; }
        public bool IsEditSynonyms { get; set; } = true;
        public Visibility visibilityExpandMore_Synonyms { get; set; }
        public ICommand CommandClosePopUpSynonyms { get; private set; }
        public ICommand CommandShowPopUpAddSynonyms { get; private set; }
        public ICommand CommandSynonyms { get; set; }
        public Visibility visibility_Synonyms { get; set; }
        public CommonDataGrid_ViewModel<ListSynonyms_VM> comonSynonyms_VM { get; set; }
        public CommonDataGrid_ViewModel<ListSynonymsDisapproved_VM> comonSynonymsDisapproved_VM { get; set; }

        private void NotifyMeSynonyms(PropertyChangedMessage<ListSynonyms_VM> obj)
        {
            SelectedPropertySynonyms = obj.NewValue;
        }
        private void NotifyMeSynonyms(PropertyChangedMessage<ListSynonymsDisapproved_VM> obj)
        {
            SelectedPropertyDisapprovedSynonyms = obj.NewValue;
        }
        private void CommandAddMoreSynonymsExecute(object obj)
        {
            SaveSynonyms();
        }
        public List<ListSynonyms_VM> ListChanges_Synonyms
        {
            get { return _listChanges_Synonyms; }
            set
            {
                if (value != _listChanges_Synonyms)
                {
                    _listChanges_Synonyms = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListSynonyms_VM>>>(new PropertyChangedMessage<List<ListSynonyms_VM>>(null, _listChanges_Synonyms.ToList(), "Default List"));
                }
            }
        }
        public List<ListSynonymsDisapproved_VM> ListChangesDisapproved_Synonyms
        {
            get { return _listChangesDisapproved_Synonyms; }
            set
            {
                if (value != _listChangesDisapproved_Synonyms)
                {
                    _listChangesDisapproved_Synonyms = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListSynonymsDisapproved_VM>>>(new PropertyChangedMessage<List<ListSynonymsDisapproved_VM>>(null, _listChangesDisapproved_Synonyms.ToList(), "Default List"));
                }
            }
        }
        private void getSynonymsDetail()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qp.QSID_ID) as ListFieldName";
            string query = "Select distinct sn.SynonymID, lc.CASID, lc.CAS, lcn.ChemicalNameID, ChemicalName,ll.LanguageName as Language,lq.QSID,lq.QSID_ID, " + subListFieldNameQuery + " from Map_SubstanceNameSynonyms sn inner join library_cas lc on sn.CASID = lc.CASId inner join Library_ChemicalNames lcn on sn.ChemicalNameID = lcn.ChemicalNameID " +
            " inner join Library_Languages ll on lcn.LanguageID=ll.LanguageID left join QSxxx_PREF qp on qp.ChemicalNameID=sn.ChemicalNameID left join Library_QSIDs lq on lq.QSID_ID=qp.QSID_ID where  lc.CAS='" + CASVal + "'";
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);

            ListChanges_Synonyms = dtResult.AsEnumerable().GroupBy(x => new { SynonymID = x.Field<int>("SynonymID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), Language = x.Field<string>("Language"), ChemicalNameID = x.Field<int>("ChemicalNameID"), ChemicalName = x.Field<string>("ChemicalName") }).Select(x => new ListSynonyms_VM()
            {
                CAS = x.Key.CAS,
                SynonymID = x.Key.SynonymID,
                CASID = x.Key.CASID,
                ChemicalName = x.Key.ChemicalName,
                ChemicalNameID = x.Key.ChemicalNameID,
                Language = x.Key.Language,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();
        }
        private void getSynonymsDetailDisapprove()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qp.QSID_ID) as ListFieldName";
            string query = "Select distinct sn.SynonymID_Disapprove as SynonymID, lc.CASID, lc.CAS, lcn.ChemicalNameID, ChemicalName,ll.LanguageName as Language,lq.QSID,lq.QSID_ID, " + subListFieldNameQuery + " from Map_SubstanceNameSynonyms_Disapproved sn inner join library_cas lc on sn.CASID = lc.CASId inner join Library_ChemicalNames lcn on sn.ChemicalNameID = lcn.ChemicalNameID " +
            " inner join Library_Languages ll on lcn.LanguageID=ll.LanguageID left join QSxxx_PREF qp on qp.ChemicalNameID=sn.ChemicalNameID left join Library_QSIDs lq on lq.QSID_ID=qp.QSID_ID where  lc.CAS='" + CASVal + "'";
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);

            ListChangesDisapproved_Synonyms = dtResult.AsEnumerable().GroupBy(x => new { SynonymID = x.Field<int>("SynonymID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), Language = x.Field<string>("Language"), ChemicalNameID = x.Field<int>("ChemicalNameID"), ChemicalName = x.Field<string>("ChemicalName") }).Select(x => new ListSynonymsDisapproved_VM()
            {
                CAS = x.Key.CAS,
                SynonymID = x.Key.SynonymID,
                CASID = x.Key.CASID,
                ChemicalName = x.Key.ChemicalName,
                ChemicalNameID = x.Key.ChemicalNameID,
                Language = x.Key.Language,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();
        }
        private List<CommonDataGridColumn> GetListSynonyms(string type)
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalNameID", ColBindingName = "ChemicalNameID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "1000" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Language", ColBindingName = "Language", ColType = "Textbox", ColumnWidth = "1000" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "DeleteSynonyms" });
            if (type == "Approved")
            {
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowDisapproveComments" });
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "DeleteSynonyms" });
            }
            else
            {
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowApproveComments" });
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#53BF9D", CommandParam = "ApproveSynonyms" });
            }
            return listColumn;
        }

        private void CommandButtonExecutedSynonyms(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "DeleteSynonyms")
            {
                ShowConfirmPopUp("Are you sure do you want to Disapprove this Synonyms? " + Environment.NewLine + "Please specify the comments:", "SynonymsDisapprove", "Disapprove");
            }
            else if (obj.Notification == "ShowDisapproveComments")
            {               
                ShowCommentsPopUp(SelectedPropertySynonyms.CAS, SelectedPropertySynonyms.ChemicalName, SelectedPropertySynonyms.CASID, SelectedPropertySynonyms.ChemicalNameID, 0,true);
            }
            else if (obj.Notification == "ApproveSynonyms")
            {
                ShowConfirmPopUp("Are you sure do you want to approve this Synonyms? " + Environment.NewLine + "Please specify the comments:", "SynonymsApprove", "approve");
            }
            else if (obj.Notification == "ShowApproveComments")
            {               
                ShowCommentsPopUp(SelectedPropertyDisapprovedSynonyms.CAS, SelectedPropertyDisapprovedSynonyms.ChemicalName, SelectedPropertyDisapprovedSynonyms.CASID, SelectedPropertyDisapprovedSynonyms.ChemicalNameID, 0,true);
            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
        private void RevertSynonymsDisapproved()
        {
            List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
            List<Log_Map_SubstanceNameSynonyms_Disapproved> lstLogDis = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
            using (var context = new CRAModel())
            {

                var itemToRemoveDis = context.Map_SubstanceNameSynonyms_Disapproved.AsNoTracking().Where(x => x.SynonymID_Disapprove == SelectedPropertyDisapprovedSynonyms.SynonymID).FirstOrDefault();
                if (itemToRemoveDis != null)
                {
                    var lgLogDis = new Log_Map_SubstanceNameSynonyms_Disapproved
                    {
                        CASID = itemToRemoveDis.CASID,
                        ChemicalNameID = itemToRemoveDis.ChemicalNameID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID_Disapprove = itemToRemoveDis.SynonymID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLogDis);

                    var lgMain = new Map_SubstanceNameSynonyms();
                    lgMain.CASID = itemToRemoveDis.CASID;
                    lgMain.ChemicalNameID = itemToRemoveDis.ChemicalNameID;
                    lgMain.IsIUPACName = false;
                    context.Map_SubstanceNameSynonyms.Add(lgMain);
                    context.SaveChanges();

                    var lgLog = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = itemToRemoveDis.CASID,
                        ChemicalNameID = itemToRemoveDis.ChemicalNameID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        SynonymID = lgMain.SynonymID,
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);
                    EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms_Disapproved).Where(x => x.SynonymID_Disapprove == itemToRemoveDis.SynonymID_Disapprove).Delete();
                    _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName((int)itemToRemoveDis.ChemicalNameID, (int)itemToRemoveDis.CASID, Engine.CurrentUserSessionID, CommentGenericSuggestionAction);
                    _notifier.ShowSuccess("Selected Synonyms deleted successfully");
                    SearchCommandExecute(null);
                }

            }

        }
        private void DeleteSynonyms()
        {
            List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
            List<Log_Map_SubstanceNameSynonyms_Disapproved> lstLogDis = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_SubstanceNameSynonyms.AsNoTracking().Where(x => x.SynonymID == SelectedPropertySynonyms.SynonymID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var itemToRemoveDis = context.Map_SubstanceNameSynonyms_Disapproved.AsNoTracking().Where(x => x.CASID == itemtoRemove.CASID && x.ChemicalNameID == itemtoRemove.ChemicalNameID).FirstOrDefault();
                    if (itemToRemoveDis == null)
                    {
                        var lgLog = new Log_Map_SubstanceNameSynonyms
                        {
                            CASID = itemtoRemove.CASID,
                            ChemicalNameID = itemtoRemove.ChemicalNameID,
                            Status = "D",
                            SessionID = Engine.CurrentUserSessionID,
                            SynonymID = itemtoRemove.SynonymID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lgLog);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);

                        var lgMainDis = new Map_SubstanceNameSynonyms_Disapproved();
                        lgMainDis.CASID = itemtoRemove.CASID;
                        lgMainDis.ChemicalNameID = itemtoRemove.ChemicalNameID;
                        context.Map_SubstanceNameSynonyms_Disapproved.Add(lgMainDis);
                        context.SaveChanges();

                        var lgLogDis = new Log_Map_SubstanceNameSynonyms_Disapproved
                        {
                            CASID = itemtoRemove.CASID,
                            ChemicalNameID = itemtoRemove.ChemicalNameID,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            SynonymID_Disapprove = lgMainDis.SynonymID_Disapprove,
                        };
                        lstLogDis.Add(lgLogDis);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLogDis);


                        EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms).Where(x => x.SynonymID == itemtoRemove.SynonymID).Delete();
                        _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName((int)itemtoRemove.ChemicalNameID, (int)itemtoRemove.CASID, Engine.CurrentUserSessionID, CommentGenericSuggestionAction);
                        _notifier.ShowSuccess("Selected Synonyms deleted successfully");
                        SearchCommandExecute(null);
                    }
                    else
                    {
                        if (!context.Log_Map_SubstanceNameSynonyms.Any(x => x.SynonymID == itemtoRemove.SynonymID && x.Status == "D"))
                        {
                            var lgLog = new Log_Map_SubstanceNameSynonyms
                            {
                                CASID = itemtoRemove.CASID,
                                ChemicalNameID = itemtoRemove.ChemicalNameID,
                                Status = "D",
                                SessionID = Engine.CurrentUserSessionID,
                                SynonymID = itemtoRemove.SynonymID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lgLog);
                            _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);
                        }

                        if (!context.Map_SubstanceNameSynonyms_Disapproved.Any(x => x.CASID == itemtoRemove.CASID && x.ChemicalNameID == itemtoRemove.ChemicalNameID))
                        {
                            var lgMainDis = new Map_SubstanceNameSynonyms_Disapproved();
                            lgMainDis.CASID = itemtoRemove.CASID;
                            lgMainDis.ChemicalNameID = itemtoRemove.ChemicalNameID;
                            context.Map_SubstanceNameSynonyms_Disapproved.Add(lgMainDis);
                            context.SaveChanges();

                            var lgLogDis = new Log_Map_SubstanceNameSynonyms_Disapproved
                            {
                                CASID = itemtoRemove.CASID,
                                ChemicalNameID = itemtoRemove.ChemicalNameID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                TimeStamp = objCommon.ESTTime(),
                                SynonymID_Disapprove = lgMainDis.SynonymID_Disapprove,
                            };

                            lstLogDis.Add(lgLogDis);
                            _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLogDis);
                        }

                        EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms).Where(x => x.SynonymID == itemtoRemove.SynonymID).Delete();
                        _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName((int)itemtoRemove.ChemicalNameID, (int)itemtoRemove.CASID, Engine.CurrentUserSessionID, CommentGenericSuggestionAction);
                        _notifier.ShowSuccess("Selected Synonyms deleted successfully");
                        SearchCommandExecute(null);
                        //  _notifier.ShowError("Selected Synonyms not deleted, it already exist in Disapprove Table");
                    }
                }
            }
        }
        public void SaveSynonyms()
        {
            if (CASVal == string.Empty || Synonyms_ChemicalName == string.Empty)
            {
                _notifier.ShowError("CAS & Chemical Name can't be empty");
                return;
            }

            using (var context = new CRAModel())
            {
                List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
                List<IStripChar> listStripChar = GetStripCharacter();
                Synonyms_ChemicalName = objCommon.RemoveWhitespaceCharacter(Synonyms_ChemicalName, listStripChar);
                Synonyms_ChemicalName = System.Text.RegularExpressions.Regex.Replace(Synonyms_ChemicalName, @"\s+", " ").Trim();
                var chkExist = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == Synonyms_ChemicalName).FirstOrDefault();
                if (chkExist == null)
                {
                    string squash = objCommon.FullSquash(Synonyms_ChemicalName);
                    var lib = new Library_ChemicalNames
                    {
                        ChemicalName = Synonyms_ChemicalName,
                        LanguageID = 1,
                        ChemicalNameHash = this.objCommon.GenerateSHA256String(Synonyms_ChemicalName),
                        ChemicalNameSquashHash = this.objCommon.GenerateSHA256String(squash),
                        ChemicalNameSquash = squash,
                        ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(Synonyms_ChemicalName),
                        TimeStamp = objCommon.ESTTime(),
                        IsActive = true,
                    };
                    context.Library_ChemicalNames.Add(lib);
                    context.SaveChanges();
                }

                var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var chemId = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == Synonyms_ChemicalName).Select(y => y.ChemicalNameID).FirstOrDefault();
                var chkAlready = context.Map_SubstanceNameSynonyms.AsNoTracking().Where(x => x.CASID == casId && x.ChemicalNameID == chemId).FirstOrDefault();
                if (chkAlready == null)
                {
                    var lgMain = new Map_SubstanceNameSynonyms();
                    lgMain.CASID = casId;
                    lgMain.ChemicalNameID = chemId;
                    lgMain.IsIUPACName = false;
                    context.Map_SubstanceNameSynonyms.Add(lgMain);
                    context.SaveChanges();

                    var lgLog = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = casId,
                        ChemicalNameID = chemId,
                        Status = "A",
                        IsIUPACName = false,
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID = lgMain.SynonymID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);
                    _notifier.ShowSuccess("Synonyms Saved Successfully.");
                }
                else
                {
                    _notifier.ShowError("Synonyms already exists");
                }
            }

        }

        private void CommandSynonymsExecute(object obj)
        {
            visibility_Synonyms = GenericVisibleCollapsedFunction(visibility_Synonyms);
            visibilityExpandMore_Synonyms = GenericVisibleCollapsedFunction_Expand(visibility_Synonyms);
            NotifyPropertyChanged("visibility_Synonyms");
            NotifyPropertyChanged("visibilityExpandMore_Synonyms");
            //ExpanderViewSynonyms = ExpanderVisibilityFunction(visibility_Synonyms);
            //NotifyPropertyChanged("ExpanderViewSynonyms");
            //ExpanderVisibilityFunctionShow(visibility_Synonyms);
        }
        private void CommandShowPopUpAddSynonymsExecute(object obj)
        {
            ClearPopUpVariable();
            IsEditSynonyms = true;
            NotifyPropertyChanged("IsEditSynonyms");

            IsShowPopUp = true;
            IsShowPopUpSynonyms = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpSynonyms");
        }
        private void CommandClosePopUpSynonymsExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpSynonyms = Visibility.Collapsed;
        }
        private void CommandAddSynonymsExecute(object obj)
        {
            SaveSynonyms();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);

        }

    }
    public class ListSynonyms_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int SynonymID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }
        public string Language { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
    public class ListSynonymsDisapproved_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int SynonymID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }
        public string Language { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
}
