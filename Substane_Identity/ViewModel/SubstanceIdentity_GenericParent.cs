﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionControlSystem.Model.ViewModel;

namespace Substane_Identity.ViewModel
{
    public class SubstanceIdentity_GenericParent
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int? QsidCount { get; set; }
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string Type { get; set; }
        public string ChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ParentChemicalNameID { get; set; }
        public string QsidFilter { get; set; }
        public bool? IsCalculatedGroup { get; set; }
        
    }
    public class SubstanceIdentity_GenericChild
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int? QsidCount { get; set; }
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string Type { get; set; }
        public string ChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ParentChemicalNameID { get; set; }
        public string QsidFilter { get; set; }
        public bool? IsCalculatedGroup { get; set; }
    }
}
