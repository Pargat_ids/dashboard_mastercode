﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;
using System.Data.OleDb;
using VersionControlSystem.DataAccess.Repository;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand UploadCommand { get; set; }
        public Visibility visibility_Upload { get; set; } = Visibility.Collapsed;
        public ICommand DownloadTemplateCommand { get; set; }
        public ICommand SaveDataCommand { get; set; }
        private List<CommonDataGridColumn> GetListUpload()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "2000" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is CAS Valid", ColBindingName = "IsValidCAS", ColType = "Checkbox", ColumnWidth = "200" });
            return listColumn;
        }
        private ObservableCollection<UploadCASSub> _ListChanges_Upload { get; set; } = new ObservableCollection<UploadCASSub>();
        public ObservableCollection<UploadCASSub> ListChanges_Upload
        {
            get { return _ListChanges_Upload; }
            set
            {
                if (value != _ListChanges_Upload)
                {
                    _ListChanges_Upload = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<UploadCASSub>>>(new PropertyChangedMessage<List<UploadCASSub>>(_ListChanges_Upload.ToList(), _ListChanges_Upload.ToList(), "Default List"));
                }
            }
        }
        public void DownloadTemplateCommandExecute(object obj)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CAS");
            dt.Columns.Add("ChemicalName");
            dt.TableName = "CAS_ChemicalNames";
            ExportAccessFile(dt, "CAS_ChemicalNames.mdb");
            _notifier.ShowSuccess("file successfully download at " + Engine.CommonAccessExportFolderPath + "CAS_ChemicalNames.mdb");
        }
        public string ExportAccessFile(DataTable _objDataTable, string fileName)
        {
            try
            {
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Engine.CommonAccessExportFolderPath + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                // IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable, Engine.CommonAccessExportFolderPath + "/" + fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private void UploadCommandExecute(object obj)
        {
            visibility_Upload = Visibility.Visible;
            NotifyPropertyChanged("visibility_Upload");
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = false;
            openFileDlg.Filter = "Access File (.mdb)|*.accdb;*.mdb"; // Optional file extensions
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string SelectedPath = openFileDlg.FileName;
                string fileName = openFileDlg.SafeFileName;
                GetDataBasesNames(SelectedPath);
            }
            else
            {
                _notifier.ShowError("No file selected");
            }

        }
        public void GetDataBasesNames(string fullPath)
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                var result = SqlClassRepository.DbaseQueryReturnList<UploadCASSub>("Select CAS,ChemicalName from CAS_ChemicalNames", fullPath);
                var chkTb = CheckTables(fullPath, "CAS_CHEMICALNAMES");
                if (chkTb == false)
                {
                    return;
                }
                if (result == null)
                {
                    System.Windows.Forms.MessageBox.Show("CAS & ChemicalName Column not found in Table CAS_ChemicalNames");
                    return;
                }

                foreach (var ct in result)
                {
                    var isVal = objCommon.CheckIsValidCRN(ct.CAS.Replace("-", "").Trim());
                    ct.IsValidCAS = isVal == true ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault());
                }
                ListChanges_Upload = new ObservableCollection<UploadCASSub>(result);
                visibility_Upload = Visibility.Collapsed;
                NotifyPropertyChanged("visibility_Upload");
            });
        }
        public bool CheckTables(string filepath, string tableName)
        {
            OleDbConnection dbConn;
            using (dbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath))
            {
                if (dbConn.State == ConnectionState.Closed)
                {
                    dbConn.Open();
                }

                DataTable userTables = dbConn.GetSchema("Tables");
                EnumerableRowCollection<DataRow> dtTable = userTables.AsEnumerable().Where(p => (p.Field<string>("TABLE_TYPE").ToUpper().Trim() == "TABLE") && (p.Field<string>("Table_Name").ToUpper().Trim() == tableName));
                if (!dtTable.Any())
                {
                    System.Windows.Forms.MessageBox.Show("CAS_ChemicalNames table is not found in selected file!");
                    return false;
                }
            }

            return true;
        }
        private void SaveDataCommandExecute(object obj)
        {
            visibility_Upload = Visibility.Visible;
            NotifyPropertyChanged("visibility_Upload");
            System.Threading.Tasks.Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    List<Library_CAS> lstCAS = new List<Library_CAS>();
                    List<Library_ChemicalNames> lstPref = new List<Library_ChemicalNames>();
                    List<UploadCASSub> lstFinal = new List<UploadCASSub>();
                    List<IStripChar> listStripChar = GetStripCharacter();
                    for (int i = 0; i < ListChanges_Upload.Count(); i++)
                    {
                        string CASvl = string.Empty;
                        string ChemHash = string.Empty;
                        if (string.IsNullOrEmpty(ListChanges_Upload[i].CAS) == false)
                        {
                            CASvl = ListChanges_Upload[i].CAS.Replace("-", "").Trim();
                            var lib = new Library_CAS
                            {
                                CAS = CASvl,
                                IsValid = objCommon.CheckIsValidCRN(CASvl),
                            };
                            lstCAS.Add(lib);
                        }
                        if (string.IsNullOrEmpty(ListChanges_Upload[i].ChemicalName) == false)
                        {
                            var chemName = objCommon.RemoveWhitespaceCharacter(ListChanges_Upload[i].ChemicalName, listStripChar);
                            chemName = System.Text.RegularExpressions.Regex.Replace(chemName, @"\s+", " ").Trim();
                            string squash = objCommon.FullSquash(chemName);
                            ChemHash = this.objCommon.GenerateSHA256String(chemName);
                            var libChem = new Library_ChemicalNames
                            {
                                ChemicalName = chemName,
                                LanguageID = 1,
                                ChemicalNameHash = ChemHash,
                            };
                            lstPref.Add(libChem);
                            lstFinal.Add(new UploadCASSub { CAS = CASvl, ChemicalName = ChemHash });
                        }
                    }

                    var lstPrefNew = lstPref.Select(x => new { x.ChemicalNameHash, x.ChemicalName, x.LanguageID }).ToList();
                    var createTableAFields = new List<string>
                {
                    "ChemicalNameHash nvarchar(64)",
                    "ChemicalName nvarchar(max)",
                    "LanguageID int",
                };
                    int modValDic = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(lstPrefNew.Count()) / 50000)));
                    for (int i = 0; i < modValDic; i++)
                    {
                        string tempTableName = "##" + Engine.CurrentUserSessionID + "pref_" + DateTime.Now.Ticks;
                        string sqlQuery = "select distinct a.* from " + tempTableName + " a left join Library_ChemicalNames b on a.ChemicalNameHash = b.ChemicalNameHash and a.LanguageID = b.LanguageID where b.ChemicalNameHash is null";
                        var newList = lstPrefNew.Skip(i * 50000).Take(50000).ToList();
                        DataTable getAllNewChemicalNameNew = objCommon.ConvertToDataTable(newList);
                        using (GenericJoinSqlTables objJoin = new GenericJoinSqlTables())
                        {
                            List<dynamic> getNewChemicalName = objJoin.InsertTempBulk(createTableAFields, getAllNewChemicalNameNew, sqlQuery, tempTableName);
                            List<(object, object)> addNewChemicalName = new List<(object, object)>();
                            getNewChemicalName.ForEach(x => { addNewChemicalName.Add((x.ChemicalName, x.LanguageID)); });
                            var addInLibraryChemicalNames = AddNewChemicalData(addNewChemicalName, objCommon.ESTTime());
                            if (addInLibraryChemicalNames.Any())
                            {
                                EFBatchOperation.For(context, context.Library_ChemicalNames).InsertAll(addInLibraryChemicalNames);
                                context.SaveChanges();
                            }
                        }
                    }

                    List<string> createTableAFieldsCAS = new List<string> { "CAS nvarchar(50)", "IsValid bit" };
                    var lstCASNew = lstCAS.Select(x => new ICombineCAS { CAS = x.CAS, IsValid = x.IsValid }).ToList();
                    int modValDicCAS = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(lstCASNew.Count()) / 50000)));
                    for (int i = 0; i < modValDicCAS; i++)
                    {
                        string tempTableNameCAS = "##" + Engine.CurrentUserSessionID + "CAS_" + DateTime.Now.Ticks;
                        string sqlQueryCAS = "select distinct a.* from " + tempTableNameCAS + " a left join Library_CAS b on rtrim(ltrim(upper(a.CAS))) = rtrim(ltrim(upper(b.CAS))) where b.cas is null";
                        List<ICombineCAS> newList = lstCASNew.Skip(i * 50000).Take(50000).ToList();
                        DataTable newListNew = objCommon.ConvertToDataTable(newList);
                        using (GenericJoinSqlTables objJoin = new GenericJoinSqlTables())
                        {
                            List<dynamic> addCas = objJoin.InsertTempBulk(createTableAFieldsCAS, newListNew, sqlQueryCAS, tempTableNameCAS);
                            if (addCas.Any())
                            {
                                AddCasInLibrary(addCas, objCommon.ESTTime());
                            }
                        }
                    }


                    //now add in Map_PreferredSubstanceName
                    var dtEx = (from d1 in lstFinal
                                join d2 in context.Library_CAS.AsNoTracking()
                                on d1.CAS equals d2.CAS
                                select new { d1.CAS, d2.CASID, d1.ChemicalName }).Distinct().ToList();

                     var dt = (from d1 in dtEx
                                 join d2 in context.Library_ChemicalNames.AsNoTracking().Where(y=> y.LanguageID == 1)
                                 on d1.ChemicalName equals d2.ChemicalNameHash
                              select new Map_PreferredSubstanceName { CASID = d1.CASID, ChemicalNameID = d2.ChemicalNameID }).ToList();

                    var chkChg = (from d1 in dt
                                  join d2 in context.Map_PreferredSubstanceName.AsNoTracking()
                                  on d1.CASID equals d2.CASID
                                  where d1.ChemicalNameID != d2.ChemicalNameID
                                  select new Map_PreferredSubstanceName { CASID = d1.CASID, ChemicalNameID= d1.ChemicalNameID, PreferredSubstanceNameID = d2.PreferredSubstanceNameID}).ToList();

                    var chkAdd = (from d1 in dt
                                  join d2 in context.Map_PreferredSubstanceName.AsNoTracking()
                                  on d1.CASID equals d2.CASID
                                  into tg
                                  from tcheck in tg.DefaultIfEmpty()
                                  where tcheck == null
                                  select d1).ToList();
                    var curTime = objCommon.ESTTime();
                    List<Log_Map_PreferredSubstanceName> logTable = new List<Log_Map_PreferredSubstanceName>();

                    chkChg.ForEach(x =>
                    {
                        logTable.Add(new Log_Map_PreferredSubstanceName
                        {
                            CASID = x.CASID,
                            ChemicalNameID = x.ChemicalNameID,
                            TimeStamp = curTime,
                            Status = "C",
                            SessionID = Engine.CurrentUserSessionID,
                            PreferredSubstanceNameID = x.PreferredSubstanceNameID,
                        });
                    });

                    var maxId = objCommon.GetMaxAutoField("Map_PreferredSubstanceName");

                    chkAdd.ForEach(x =>
                    {
                        logTable.Add(new Log_Map_PreferredSubstanceName
                        {
                            CASID = x.CASID,
                            ChemicalNameID = x.ChemicalNameID,
                            TimeStamp = curTime,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            PreferredSubstanceNameID = maxId,
                        });
                        maxId += 1;
                    });

                    _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(logTable);
                    _objLibraryFunction_Service.BulkIns<Map_PreferredSubstanceName>(chkAdd);
                    EFBatchOperation.For(context, context.Map_PreferredSubstanceName).UpdateAll(chkChg, x => x.ColumnsToUpdate(c => c.ChemicalNameID));
                    context.SaveChanges();

                    visibility_Upload = Visibility.Collapsed;
                    NotifyPropertyChanged("visibility_Upload");
                }

                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("CAS + SubstanceName Added successfully");
                });
            });

        }
        public void AddCasInLibrary(IEnumerable<dynamic> populateCasInLibrary_Cas, DateTime timeStamp)
        {
            using (CRAModel context = new CRAModel())
            {
                List<IStripChar> listStripChar = GetStripCharacter();
                List<Library_CAS> getNewCas = (from d1 in populateCasInLibrary_Cas
                                               join d2 in context.Library_CAS.AsNoTracking().Select(x => new { CAS = x.CAS.Trim() }).Distinct().ToList()
                                                   on d1.CAS.Trim() equals objCommon.RemoveWhitespaceCharacter(d2.CAS.Trim(), listStripChar)
                                                   into tg
                                               from tcheck in tg.DefaultIfEmpty()
                                               where tcheck == null
                                               select new Library_CAS { CAS = d1.CAS.Trim(), IsValid = (bool)d1.IsValid, TimeStamp = timeStamp, IsInternalOnlyCAS = Regex.IsMatch(d1.CAS.Trim(), "[a-z]") }).Distinct().ToList();

                List<Library_CAS> addInCasLibary = getNewCas.AsEnumerable().Where(x => string.IsNullOrWhiteSpace(x.CAS) == false).GroupBy(y => y.CAS).Select(z => z.FirstOrDefault()).ToList();
                EFBatchOperation.For(context, context.Library_CAS).InsertAll(addInCasLibary);
            }
        }

        private List<Library_ChemicalNames> AddNewChemicalData(List<(object, object)> addChemicalNames, DateTime timeStamp)
        {
            List<Library_ChemicalNames> addInLibraryChemicalNames = new List<Library_ChemicalNames>();
            for (int i = 0; i < addChemicalNames.Count(); i++)
            {
                string squash = objCommon.FullSquash((string)addChemicalNames[i].Item1);
                string chemName = (string)addChemicalNames[i].Item1;
                addInLibraryChemicalNames.Add(new Library_ChemicalNames
                {
                    ChemicalName = chemName,
                    LanguageID = (int)addChemicalNames[i].Item2,
                    ChemicalNameHash = objCommon.GenerateSHA256String(chemName),
                    ChemicalNameSquash = squash,
                    ChemicalNameSquashHash = objCommon.GenerateSHA256String(squash),
                    ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(chemName),
                    TimeStamp = timeStamp,
                    IsActive = true,
                });
            }

            addInLibraryChemicalNames = addInLibraryChemicalNames.GroupBy(o => new { o.ChemicalNameHash, o.LanguageID }).Select(x => x.FirstOrDefault()).ToList();

            return addInLibraryChemicalNames;
        }
    }

    public class UploadCASSub
    {
        public string CAS { get; set; }
        public string ChemicalName { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsValidCAS { get; set; }
    }
    public class ICombineCAS
    {
        public string CAS { get; set; }

        public bool IsValid { get; set; }
    }
    public class GenericJoinSqlTables : IDisposable
    {
        private readonly CRAModel context;
        private bool disposed;
        private readonly CommonFunctions objCommon;

        // private StaticVariables staticVariables;
        public GenericJoinSqlTables()
        {
            // staticVariables = sVariables;
            context = new CRAModel();
            objCommon = new CommonFunctions();

        }

        public List<dynamic> InsertTempBulk(List<string> createTableAFields, DataTable lst, string sqlQuery, string tempTableName)
        {
            string str = "CREATE TABLE " + tempTableName + "( " + string.Join(",", createTableAFields) + " )";
            SqlConnection con = new SqlConnection(Win_Administrator_VM.CommonListConn);
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            DataTable dt = new DataTable();

            // if (lst.Rows.Count != 0)
            // {
            using (SqlCommand createCommand = new SqlCommand(str, con))
            {
                using (SqlCommand dCommand = new SqlCommand("DROP table " + tempTableName, con))
                {
                    createCommand.ExecuteNonQuery();
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.UseInternalTransaction, null)
                    {
                        DestinationTableName = tempTableName,
                    };
                    bulkCopy.WriteToServer(lst);
                    SqlCommand adp = new SqlCommand(sqlQuery, con) { CommandTimeout = 0 };
                    using (SqlDataReader dr = adp.ExecuteReader())
                    {
                        dt = new DataTable();
                        dt.BeginLoadData();
                        dt.Load(dr);
                        dt.EndLoadData();
                    }

                    dCommand.ExecuteNonQuery();
                }

                // }
            }

            List<string> convertList = (from DataColumn x in dt.Columns
                                        select x.ColumnName).ToList();
            List<dynamic> newLst = objCommon.ConvertToDynamicList(dt, convertList);
            objCommon.DisposeDataTable(ref dt);
            return newLst;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}
