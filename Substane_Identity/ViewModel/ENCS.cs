﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;


namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool IsChange { get; set; }
        private List<ListENCSNumber_VM> _listChanges_ENCSNumber { get; set; } = new List<ListENCSNumber_VM>();
        private List<ListENCSNumberDisapproved_VM> _listChanges_ENCSNumberDisapproved { get; set; } = new List<ListENCSNumberDisapproved_VM>();
        public ListENCSNumberDisapproved_VM SelectedPropertyENCSNumberDisapproved { get; set; }
        public Visibility IsShowPopUpENCSNumber { get; set; } = Visibility.Collapsed;
        public ListENCSNumber_VM SelectedPropertyENCSNumber { get; set; }
        public ICommand CommandAddENCSNumber { get; private set; }
        public ICommand CommandAddMoreENCSNumber { get; private set; }
        public bool IsEditENCSNumber { get; set; } = true;
        public Visibility visibilityExpandMore_ENCSNumber { get; set; }
        public ICommand CommandECNumber { get; set; }
        public Visibility visibility_ENCSNumber { get; set; }
        public ICommand CommandENCSNumber { get; set; }
        public ICommand CommandShowPopUpAddENCSNumber { get; private set; }
        public ICommand CommandClosePopUpENCSNumber { get; private set; }
        public CommonDataGrid_ViewModel<ListENCSNumber_VM> comonENCSNumber_VM { get; set; }
        public CommonDataGrid_ViewModel<ListENCSNumberDisapproved_VM> comonENCSNumberDisapproved_VM { get; set; }
        
        private void NotifyMeENCSNumber(PropertyChangedMessage<ListENCSNumber_VM> obj)
        {
            SelectedPropertyENCSNumber = obj.NewValue;
        }
        private void NotifyMeENCSNumber(PropertyChangedMessage<ListENCSNumberDisapproved_VM> obj)
        {
            SelectedPropertyENCSNumberDisapproved = obj.NewValue;
        }
        private void CommandAddMoreENCSNumberExecute(object obj)
        {
            SaveENCSNumber();
        }
        public List<ListENCSNumber_VM> ListChanges_ENCSNumber
        {
            get { return _listChanges_ENCSNumber; }
            set
            {
                if (value != _listChanges_ENCSNumber)
                {
                    _listChanges_ENCSNumber = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListENCSNumber_VM>>>(new PropertyChangedMessage<List<ListENCSNumber_VM>>(null, _listChanges_ENCSNumber, "Default List"));
                }
            }
        }
        public List<ListENCSNumberDisapproved_VM> ListChanges_ENCSNumberDisapproved
        {
            get { return _listChanges_ENCSNumberDisapproved; }
            set
            {
                if (value != _listChanges_ENCSNumberDisapproved)
                {
                    _listChanges_ENCSNumberDisapproved = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListENCSNumberDisapproved_VM>>>(new PropertyChangedMessage<List<ListENCSNumberDisapproved_VM>>(null, _listChanges_ENCSNumberDisapproved, "Default List"));
                }
            }
        }
        private void getENCSNumberDetail()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qi.QSID_ID) as ListFieldName";
            string query = "Select distinct encs.CASENCSNumberID, encs.CASID, lc.CAS, li.IdentifierValueID, IdentifierValue,lq.QSID_ID,lq.QSID, "+ subListFieldNameQuery + " from Map_CAS_ENCSNumber encs inner join  library_cas lc " +
" on encs.CASID = lc.CASID inner join Library_Identifiers li on encs.IdentifierValueID = li.IdentifierValueID  left join QSxxx_Identifiers qi on qi.IdentifierValueID = li.IdentifierValueID left join Library_QSIDs lq on lq.QSID_ID=qi.QSID_ID where lc.cas='" + CASVal + "'";
            //"Select distinct a.CASENCSNumberID, a.CASID, b.CAS, a.IdentifierValueID, IdentifierValue from Map_CAS_ENCSNumber a, library_cas b, Library_Identifiers c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.IdentifierValueID = c.IdentifierValueID"
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);

            var listEncs = dtResult.AsEnumerable().GroupBy(x => new { CASENCSNumberID = x.Field<int>("CASENCSNumberID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), IdentifierValueID = x.Field<int>("IdentifierValueID"), IdentifierValue = x.Field<string>("IdentifierValue") }).Select(x => new ListENCSNumber_VM
            {
                CAS =x.Key.CAS,
                CASENCSNumberID = x.Key.CASENCSNumberID,
                CASID = x.Key.CASID,
                IdentifierValue = x.Key.IdentifierValue,
                IdentifierValueID = x.Key.IdentifierValueID,
                Qsid = x.ToList().Where(y=> !string.IsNullOrEmpty( y.Field<string>("QSID"))).Select(y=> new HyperLinkDataValue { DisplayValue=y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData=y.Field<int>("QSID_ID") }).Distinct().ToList(),
                QsidCount =  x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();


            ListChanges_ENCSNumber = new List<ListENCSNumber_VM>(listEncs);
        }
    
        private void CommandENCSNumberExecute(object obj)
        {
            visibility_ENCSNumber = GenericVisibleCollapsedFunction(visibility_ENCSNumber);
            visibilityExpandMore_ENCSNumber = GenericVisibleCollapsedFunction_Expand(visibility_ENCSNumber);
            NotifyPropertyChanged("visibility_ENCSNumber");
            NotifyPropertyChanged("visibilityExpandMore_ENCSNumber");
            //ExpanderViewENCSNumber = ExpanderVisibilityFunction(visibility_ENCSNumber);
            //NotifyPropertyChanged("ExpanderViewENCSNumber");
            //ExpanderVisibilityFunctionShow(visibility_ENCSNumber);
        }
        private List<CommonDataGridColumn> GetListENCSNumber()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValueID", ColBindingName = "IdentifierValueID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValue", ColBindingName = "IdentifierValue", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteENCSNumber" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddENCSNumber" });
            return listColumn;
        }
        private void CommandShowPopUpAddENCSNumberExecute(object obj)
        {
            ClearPopUpVariable();
            IsEditENCSNumber = true;
            NotifyPropertyChanged("IsEditENCSNumber");
            IsChange = false;

            IsShowPopUp = true;
            IsShowPopUpENCSNumber = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpENCSNumber");
        }
        private void CommandClosePopUpENCSNumberExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpENCSNumber = Visibility.Collapsed;
        }
        private void CommandAddENCSNumberExecute(object obj)
        {
            
            SaveENCSNumber();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);
        }
        
        private void CommandButtonExecutedENCSNumber(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteENCSNumber")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this ENCSNumber?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    List<Log_Map_CAS_ENCSNumber> lstLog = new List<Log_Map_CAS_ENCSNumber>();
                    using (var context = new CRAModel())
                    {
                        var itemtoRemove = context.Map_CAS_ENCSNumber.AsNoTracking().Where(x => x.CASENCSNumberID == SelectedPropertyENCSNumber.CASENCSNumberID).FirstOrDefault();
                        if (itemtoRemove != null)
                        {
                            var lgLog = new Log_Map_CAS_ENCSNumber
                            {
                                CASID = itemtoRemove.CASID,
                                IdentifierValueID = itemtoRemove.IdentifierValueID,
                                Status = "D",
                                SessionID = Engine.CurrentUserSessionID,
                                CASENCSNumberID = itemtoRemove.CASENCSNumberID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lgLog);
                            _objLibraryFunction_Service.BulkIns<Log_Map_CAS_ENCSNumber>(lstLog);

                            EFBatchOperation.For(context, context.Map_CAS_ENCSNumber).Where(x => x.CASENCSNumberID == itemtoRemove.CASENCSNumberID).Delete();
                            _notifier.ShowSuccess("Selected ENCS Number deleted successfully");
                            SearchCommandExecute(obj);
                        }
                    }
                }
            }
            else if (obj.Notification == "AddENCSNumber")
            {
                IsChange = true;
                IsEditENCSNumber = true;
                NotifyPropertyChanged("IsEditENCSNumber");
                IsShowPopUp = true;
                IsShowPopUpENCSNumber = Visibility.Visible;
                NotifyPropertyChanged("IsShowPopUpENCSNumber");
                ENCSNumber_IdentifierValue = SelectedPropertyENCSNumber.IdentifierValue;
                NotifyPropertyChanged("ENCSNumber_IdentifierValue");
            }
            else {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
        public string ENCSNumber_IdentifierValue { get; set; }
        public void SaveENCSNumber()
        {
            if (CASVal == string.Empty || ENCSNumber_IdentifierValue == null)
            {
                _notifier.ShowError("CAS & Identifier Value can't be empty");
                return;
            }
            using (var context = new CRAModel())
            {
                List<Log_Map_CAS_ENCSNumber> lstLog = new List<Log_Map_CAS_ENCSNumber>();
                List<Library_Identifiers> lstLib = new List<Library_Identifiers>();
                var chkExist = context.Library_Identifiers.AsNoTracking().Where(x => x.IdentifierValue == ENCSNumber_IdentifierValue).FirstOrDefault();
                if (chkExist == null)
                {
                    var lib = new Library_Identifiers
                    {
                        IdentifierTypeID = 17,
                        IdentifierValue = ENCSNumber_IdentifierValue,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLib.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Identifiers>(lstLib);
                }

                var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var identId = context.Library_Identifiers.AsNoTracking().Where(x => (x.IdentifierValue == ENCSNumber_IdentifierValue || x.IdentifierValue == ENCSNumber_IdentifierValue.Replace("-", "")) && x.IdentifierTypeID == 17).Select(y => y.IdentifierValueID).FirstOrDefault();
                var chkAlready = context.Map_CAS_ENCSNumber.AsNoTracking().Where(x => x.CASID == casId && x.IdentifierValueID == identId).FirstOrDefault();
                if (chkAlready == null)
                {
                    string updStatus = string.Empty;
                    int encnum = 0;
                    if (IsChange == false)
                    {
                        updStatus ="A";
                        var lgMain = new Map_CAS_ENCSNumber();
                        lgMain.CASID = casId;
                        lgMain.IdentifierValueID = identId;
                        context.Map_CAS_ENCSNumber.Add(lgMain);
                        context.SaveChanges();

                        encnum = lgMain.CASENCSNumberID;
                    }
                    else
                    {
                        updStatus = "C";
                        List<Map_CAS_ENCSNumber> lstMain= new List<Map_CAS_ENCSNumber>();
                        var lgMain = context.Map_CAS_ENCSNumber.AsNoTracking().Where(x => x.CASENCSNumberID == SelectedPropertyENCSNumber.CASENCSNumberID).FirstOrDefault();
                        lgMain.IdentifierValueID = identId;
                        lstMain.Add(lgMain);
                        EFBatchOperation.For(context, context.Map_CAS_ENCSNumber).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.IdentifierValueID));
                        context.SaveChanges();

                        encnum = SelectedPropertyENCSNumber.CASENCSNumberID;
                    }
                    var lgLog = new Log_Map_CAS_ENCSNumber
                    {
                        CASID = casId,
                        IdentifierValueID = identId,
                        Status = updStatus,
                        SessionID = Engine.CurrentUserSessionID,
                        CASENCSNumberID = encnum,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_ENCSNumber>(lstLog);
                    _notifier.ShowSuccess("ENCS Number Updated Successfully.");
                }
                else
                {
                    _notifier.ShowError("ENCS Number already exists");
                }
            }

        }

    }
    public class ListENCSNumber_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASENCSNumberID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }

        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
    public class ListENCSNumberDisapproved_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASENCSNumberID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }

        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
}
