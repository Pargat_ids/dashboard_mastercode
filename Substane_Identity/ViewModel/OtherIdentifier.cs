﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;


namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibilityExpandMore_Otherident { get; set; }
        public Visibility visibility_Otherident { get; set; }

        public ICommand CommandOtherident { get; set; }
        public CommonDataGrid_ViewModel<ListOtherident_VM> comonOtherident_VM { get; set; }
        private List<ListOtherident_VM> _listChanges_Otherident { get; set; } = new List<ListOtherident_VM>();
        public List<ListOtherident_VM> ListChanges_Otherident
        {
            get { return _listChanges_Otherident; }
            set
            {
                if (value != _listChanges_Otherident)
                {
                    _listChanges_Otherident = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListOtherident_VM>>>(new PropertyChangedMessage<List<ListOtherident_VM>>(null, _listChanges_Otherident.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListOtherident()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierTypeID", ColBindingName = "IdentifierTypeID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierTypeName", ColBindingName = "IdentifierTypeName", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Value", ColBindingName = "IdentifierValue", ColType = "Textbox", ColumnWidth = "400" });



            return listColumn;
        }

        private void getOtheridentDetail()
        {
            var casID = _objLibraryFunction_Service.GetLibraryCasByCas(CASVal).CASID;
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qc.QSID_ID) as ListFieldName";
            string query = "SELECT distinct lc.CASID,lc.CAS, qi.IdentifierValueID,IdentifierTypeName,IdentifierValue,lic.IdentifierTypeID,qc.QSID_ID,lq.QSID, "+ subListFieldNameQuery + " FROM(QSxxx_CAS qc INNER JOIN QSxxx_Identifiers qi ON(qc.QSID_ID = qi.QSID_ID) AND(qc.RowID = qi.RowID)) " +
            " INNER JOIN(Library_IdentifierCategories lic INNER JOIN Map_QSID_FieldName mf ON lic.IdentifierTypeID = mf.IdentifierTypeID) ON qi.FieldNameID = mf.FieldNameID inner join Library_Identifiers li on li.IdentifierValueID = qi.IdentifierValueID inner join Library_CAS lc on lc.CASID = qc.CASID left join Library_QSIDs lq on lq.QSID_ID=qc.QSID_ID " +
            " WHERE(((lic.IdentifierTypeName)Not In('EC Number', 'ENCS Number', 'Molecular Formula')) AND((qc.CASID) = (" + casID + "))) ";
            var result = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);

            //ListChanges_Otherident = new VM>(result);
            ListChanges_Otherident = result.AsEnumerable().GroupBy(x=> new { 
                //Cas= x.Field<string>("Cas"), casId = x.Field<int>("CASID"),
                IdentifierTypeID = x.Field<int>("IdentifierTypeID"),
                IdentifierTypeName = x.Field<string>("IdentifierTypeName"),
                IdentifierValue = x.Field<string>("IdentifierValue"),
            }).Select(x => new ListOtherident_VM()
            {
                //CAS= x.Key.Cas,
                //CASID = x.Key.casId,
                IdentifierTypeID = x.Key.IdentifierTypeID,
                IdentifierTypeName = x.Key.IdentifierTypeName,
                IdentifierValue = x.Key.IdentifierValue,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();


        }
        private void CommandOtheridentExecute(object obj)
        {
            visibility_Otherident = GenericVisibleCollapsedFunction(visibility_Otherident);
            visibilityExpandMore_Otherident = GenericVisibleCollapsedFunction_Expand(visibility_Otherident);
            NotifyPropertyChanged("visibility_Otherident");
            NotifyPropertyChanged("visibilityExpandMore_Otherident");
            //ExpanderViewOtherident = ExpanderVisibilityFunction(visibility_Otherident);
            //NotifyPropertyChanged("ExpanderViewOtherident");
            //ExpanderVisibilityFunctionShow(visibility_Otherident);
        }
    }
    public class ListOtherident_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierTypeID { get; set; }
        public string IdentifierTypeName { get; set; }
        public string IdentifierValue { get; set; }

    }
}
