﻿using CRA_DataAccess.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {

        public string NoofParent_ParentCAS { get; set; }

        public Visibility visibilityExpandMore_NoofParent { get; set; }
        public Visibility visibility_NoofParent { get; set; }
        public SubstanceIdentity_GenericParent SelectedPropertyNoofParent { get; set; }
        public ICommand CommandNoofParent { get; set; }

        public Visibility IsShowPopUpNoofParent { get; set; } = Visibility.Collapsed;

        public ICommand CommandClosePopUpNoofParent { get; private set; }
        public ICommand CommandShowPopUpAddNoofParent { get; private set; }
        public ICommand CommandAddNoofParent { get; private set; }
        public ICommand CommandAddMoreNoofParent { get; private set; }
        public CommonDataGrid_ViewModel<SubstanceIdentity_GenericParent> commonNoofParent_VM { get; set; }
        private void NotifyMeNoofParent(PropertyChangedMessage<SubstanceIdentity_GenericParent> obj)
        {
            SelectedPropertyNoofParent = obj.NewValue;
        }

        private List<SubstanceIdentity_GenericParent> _listChanges_NoofParent { get; set; } = new List<SubstanceIdentity_GenericParent>();
        public List<SubstanceIdentity_GenericParent> ListChanges_NoofParent
        {
            get { return _listChanges_NoofParent; }
            set
            {
                if (value != _listChanges_NoofParent)
                {
                    _listChanges_NoofParent = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<SubstanceIdentity_GenericParent>>>(new PropertyChangedMessage<List<SubstanceIdentity_GenericParent>>(null, _listChanges_NoofParent, "Default List"));
                }
            }
        }

        private void getNoofParentDetail()
        {
            ListChanges_NoofParent = _objLibraryFunction_Service.GetAllParentCasFromFlatGenerics(CASVal, "OnlyParentCas")
                .GroupBy(x => new
                {
                    x.ChemicalName,
                    x.ChemicalNameID,
                    x.ParentChemicalName,
                    x.ParentChemicalNameID,
                    x.ParentCas,
                    x.ParentCasID,
                    x.ChildCas,
                    x.ChildCasID,
                    x.IsCalculatedGroup
                }).Select(x => new SubstanceIdentity_GenericParent()
                {
                    ChemicalName = x.Key.ChemicalName,
                    ChemicalNameID = x.Key.ChemicalNameID,
                    ChildCas = x.Key.ChildCas,
                    ChildCasID = x.Key.ChildCasID,
                    ParentCas = x.Key.ParentCas,
                    ParentCasID = x.Key.ParentCasID,
                    IsCalculatedGroup = x.Key.IsCalculatedGroup,
                    ParentChemicalName = x.Key.ParentChemicalName,
                    ParentChemicalNameID = x.Key.ParentChemicalNameID,
                    Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => new HyperLinkDataValue() { DisplayValue = y.Qsid + " ( " + y.ListFieldName + " )", NavigateData = y.Qsid_Id }).Distinct().ToList(),
                    QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Count(),
                    QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y=>y.Qsid).ToList())
                }).ToList();
        }
        private List<CommonDataGridColumn> GetListNoofParent()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCasID", ColBindingName = "ParentCasID", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCas", ColBindingName = "ParentCas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCasID", ColBindingName = "ChildCasID", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCas", ColBindingName = "ChildCas", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentChemicalName", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox", ColumnWidth = "150" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "150", ContentName = "Delete Link", BackgroundColor = "#ff414d", CommandParam = "DeleteNoofParent" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "150", ContentName = "Update Name", BackgroundColor = "#07689f", CommandParam = "AddNoofParent" });
            return listColumn;
        }
        private void CommandNoofParentExecute(object obj)
        {

            visibility_NoofParent = GenericVisibleCollapsedFunction(visibility_NoofParent);
            visibilityExpandMore_NoofParent = GenericVisibleCollapsedFunction_Expand(visibility_NoofParent);
            NotifyPropertyChanged("visibility_NoofParent");
            NotifyPropertyChanged("visibilityExpandMore_NoofParent");
            //ExpanderViewNoofParent = ExpanderVisibilityFunction(visibility_NoofParent);
            //NotifyPropertyChanged("ExpanderViewNoofParent");
            //ExpanderVisibilityFunctionShow(visibility_NoofParent);
        }

        private void CommandButtonExecutedNoofParent(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "DeleteNoofParent")
            {
                if (SelectedPropertyNoofParent.IsCalculatedGroup == true)
                {
                    _notifier.ShowWarning("Cannot delete link because this is a calculated generic group");
                    return;
                }
                CasType = "Parent";
                ShowPopUpDeleteGeneric(CASVal);
            }
            else if (obj.Notification == "AddNoofParent")
            {
                ShowPopUpAddEditName();
            }
            else {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }

       



    }


}
