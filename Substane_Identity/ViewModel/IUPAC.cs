﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;


namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool IsChangeIupac { get; set; }
        public Visibility ExpanderViewIUPAC { get; set; }
        public Visibility ExpanderViewDirectHit { get; set; }
        public Visibility ExpanderViewGenericHit { get; set; }
        public Visibility ExpanderViewWebsite { get; set; }
        //public Visibility ExpanderViewGroupName { get; set; }
        public Visibility ExpanderViewAlternateCAS { get; set; }
        public Visibility ExpanderViewNoofChild { get; set; }
        public Visibility ExpanderViewSynonyms { get; set; }
        public Visibility ExpanderViewECNumber { get; set; }
        public Visibility ExpanderViewMFormula { get; set; }
        public Visibility ExpanderViewENCSNumber { get; set; }
        public Visibility ExpanderViewOtherident { get; set; }
        public Visibility ExpanderViewHydrates { get; set; }
        private ObservableCollection<ListIUpacName_VM> _listChanges_IUpacName { get; set; } = new ObservableCollection<ListIUpacName_VM>();
        public Visibility IsShowPopUpIUpacName { get; set; } = Visibility.Collapsed;
        public string IUPACName_Chemical { get; set; }
        public bool IsIUPAC { get; set; }
        public ListIUpacName_VM SelectedPropertyIUpacName { get; set; }
        public ICommand CommandAddIUpacName { get; private set; }
        public ICommand CommandAddMoreIUpacName { get; private set; }
        public bool IsEditIUpacName { get; set; } = true;
        public Visibility visibilityExpandMore_IUpacName { get; set; }
        public ICommand CommandShowPopUpAddIUpacName { get; private set; }
        public ICommand CommandIUpacName { get; set; }
        public ICommand CommandClosePopUpIUpacName { get; private set; }
        public Visibility visibility_IUpacName { get; set; }
        public CommonDataGrid_ViewModel<ListIUpacName_VM> comonIUpacName_VM { get; set; }
        private void NotifyMeIUpacName(PropertyChangedMessage<ListIUpacName_VM> obj)
        {
            SelectedPropertyIUpacName = obj.NewValue;
        }
        private void CommandAddMoreIUpacNameExecute(object obj)
        {
            SaveIUpacName();
        }
        public ObservableCollection<ListIUpacName_VM> ListChanges_IUpacName
        {
            get { return _listChanges_IUpacName; }
            set
            {
                if (value != _listChanges_IUpacName)
                {
                    _listChanges_IUpacName = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListIUpacName_VM>>>(new PropertyChangedMessage<List<ListIUpacName_VM>>(_listChanges_IUpacName.ToList(), _listChanges_IUpacName.ToList(), "Default List"));
                }
            }
        }
        private void getIUpacNameDetail()
        {
            var result = objCommon.DbaseQueryReturnSqlList<ListIUpacName_VM>("Select distinct a.synonymID, a.CASID, b.CAS, a.ChemicalNameID, ChemicalName, a.isIUpACName from Map_SubstanceNameSynonyms a, library_cas b, Library_ChemicalNames c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.ChemicalNameID = c.ChemicalNameID and IsIUPACName = 1", Win_Administrator_VM.CommonListConn);
            ListChanges_IUpacName = new ObservableCollection<ListIUpacName_VM>(result);
        }
        private List<CommonDataGridColumn> GetListIUpacName()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalNameID", ColBindingName = "ChemicalNameID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "500" });
            //listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsIUPACName", ColBindingName = "IsIUPACName", ColType = "Textbox", ColumnWidth = "300" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_SubstanceNameSynonyms", ColType = "Button", ColumnWidth = "150", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteIUpacName" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "Map_SubstanceNameSynonyms", ColType = "Button", ColumnWidth = "150", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddIUpacName" });
            return listColumn;
        }

        private void CommandIUpacNameExecute(object obj)
        {
            visibility_IUpacName = GenericVisibleCollapsedFunction(visibility_IUpacName);
            visibilityExpandMore_IUpacName = GenericVisibleCollapsedFunction_Expand(visibility_IUpacName);
            NotifyPropertyChanged("visibility_IUpacName");
            NotifyPropertyChanged("visibilityExpandMore_IUpacName");
            //ExpanderViewIUPAC = ExpanderVisibilityFunction(visibility_IUpacName);
            //NotifyPropertyChanged("ExpanderViewIUPAC");
            //ExpanderVisibilityFunctionShow(visibility_IUpacName);
        }
        private void CommandShowPopUpAddIUpacNameExecute(object obj)
        {
            IUPACName_Chemical = string.Empty;
            ClearPopUpVariable();
            IsEditIUpacName = true;
            NotifyPropertyChanged("IsEditIUpacName");
            IsChangeIupac = false;
            IsShowPopUp = true;
            IsShowPopUpIUpacName = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpIUpacName");
        }
        private void CommandClosePopUpIUpacNameExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpIUpacName = Visibility.Collapsed; ;
        }
        private void CommandAddIUpacNameExecute(object obj)
        {
            SaveIUpacName();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);
        }
      
        private void CommandButtonExecutedIUpacName(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteIUpacName")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this IUpacName?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
                    using (var context = new CRAModel())
                    {
                        var itemtoRemove = context.Map_SubstanceNameSynonyms.AsNoTracking().Where(x => x.SynonymID == SelectedPropertyIUpacName.SynonymID).FirstOrDefault();
                        if (itemtoRemove != null)
                        {
                            if (itemtoRemove.IsIUPACName != false)
                            {
                                var lgLog = new Log_Map_SubstanceNameSynonyms
                                {
                                    CASID = itemtoRemove.CASID,
                                    ChemicalNameID = itemtoRemove.ChemicalNameID,
                                    Status = "C",
                                    SessionID = Engine.CurrentUserSessionID,
                                    IsIUPACName = false,
                                    SynonymID = itemtoRemove.SynonymID,
                                    TimeStamp = objCommon.ESTTime(),
                                };
                                lstLog.Add(lgLog);
                                _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);

                                List<Map_SubstanceNameSynonyms> lstMain = new List<Map_SubstanceNameSynonyms>();

                                itemtoRemove.IsIUPACName = false;
                                lstMain.Add(itemtoRemove);
                                EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.IsIUPACName));
                                context.SaveChanges();
                                _notifier.ShowSuccess("Selected IUPAC deleted successfully");
                                SearchCommandExecute(obj);
                            }
                        }
                    }
                }
            }
            else if (obj.Notification == "AddIUpacName")
            {
                IsChangeIupac = true;
                IsEditIUpacName = true;
                NotifyPropertyChanged("IsEditIUpacName");
                IsShowPopUp = true;
                IsShowPopUpIUpacName = Visibility.Visible;
                NotifyPropertyChanged("IsShowPopUpIUpacName");
                IUPACName_Chemical = SelectedPropertyIUpacName.ChemicalName;
                IsIUPAC = SelectedPropertyIUpacName.IsIUPACName == null ? false : SelectedPropertyIUpacName.IsIUPACName;
                NotifyPropertyChanged("IUPACName_Chemical");
                NotifyPropertyChanged("IsIUPAC");
            }
        }
        public void SaveIUpacName()
        {
            if (CASVal == string.Empty || IUPACName_Chemical == string.Empty)
            {
                _notifier.ShowError("CAS & Chemical Name can't be empty");
                return;
            }

            using (var context = new CRAModel())
            {
                var languageID = context.Library_Languages.Where(x => x.LanguageName == "English").Select(x=> x.LanguageID).FirstOrDefault();
                List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
                List<IStripChar> listStripChar = GetStripCharacter();
                IUPACName_Chemical = objCommon.RemoveWhitespaceCharacter(IUPACName_Chemical, listStripChar);
                IUPACName_Chemical = System.Text.RegularExpressions.Regex.Replace(IUPACName_Chemical, @"\s+", " ").Trim();
                var chkExist = _objLibraryFunction_Service.GetListExactLibraryChemicalName(IUPACName_Chemical,languageID).FirstOrDefault();
                if (chkExist == null)
                {
                    string squash = objCommon.FullSquash(IUPACName_Chemical);
                    var lib = new Library_ChemicalNames
                    {
                        ChemicalName = IUPACName_Chemical,
                        LanguageID = languageID,
                        ChemicalNameHash = this.objCommon.GenerateSHA256String(IUPACName_Chemical),
                        ChemicalNameSquashHash = this.objCommon.GenerateSHA256String(squash),
                        ChemicalNameSquash = squash,
                        ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(IUPACName_Chemical),
                        TimeStamp = objCommon.ESTTime(),
                        IsActive = true,
                    };
                    context.Library_ChemicalNames.Add(lib);
                    context.SaveChanges();
                }

                var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var chemId = _objLibraryFunction_Service.GetListExactLibraryChemicalName(IUPACName_Chemical, languageID).Select(y => y.ChemicalNameID).FirstOrDefault();

                var existingIupac = context.Map_SubstanceNameSynonyms.Where(x => x.CASID == casId && x.IsIUPACName == true).FirstOrDefault();
                if (existingIupac != null)
                {
                    var response = System.Windows.MessageBox.Show("There is already have IUPAC name, Do you want to Overwrite it?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (response.ToString() == "Yes")
                    {
                        existingIupac.IsIUPACName = false;
                        context.SaveChanges();
                    }
                    else
                    {
                        return;
                    }
                }

                var chkAlready = context.Map_SubstanceNameSynonyms.Where(x => x.CASID == casId && x.ChemicalNameID == chemId).FirstOrDefault();
                if (chkAlready == null)
                {
                    string updStatus = string.Empty;
                    int encnum = 0;
                    //if (IsChangeIupac == false)
                    //{
                    updStatus = "A";

                    var lgMain = new Map_SubstanceNameSynonyms();
                    lgMain.CASID = casId;
                    lgMain.ChemicalNameID = chemId;
                    lgMain.IsIUPACName = true;
                    context.Map_SubstanceNameSynonyms.Add(lgMain);
                    context.SaveChanges();

                    encnum = lgMain.SynonymID;
                    //}
                    //else
                    //{
                    //    updStatus = "C";
                    //    List<Map_SubstanceNameSynonyms> lstMain = new List<Map_SubstanceNameSynonyms>();
                    //    var lgMain = context.Map_SubstanceNameSynonyms.AsNoTracking().Where(x => x.SynonymID == SelectedPropertyIUpacName.SynonymID).FirstOrDefault();
                    //    lgMain.ChemicalNameID = chemId;
                    //    lgMain.IsIUPACName = IsIUPAC;
                    //    lstMain.Add(lgMain);
                    //    EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.ChemicalNameID, c => c.IsIUPACName));
                    //    context.SaveChanges();

                    //    encnum = SelectedPropertyIUpacName.SynonymID;
                    //}
                    var lgLog = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = casId,
                        ChemicalNameID = chemId,
                        Status = updStatus,
                        IsIUPACName = true,
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID = encnum,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);

                }
                else
                {
                    chkAlready.IsIUPACName = true;
                    context.SaveChanges();


                }
                _notifier.ShowSuccess("IUPACName Updated Successfully.");

                // }
            }
        }
    }
    public class ListIUpacName_VM
    {
        public int SynonymID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }
        public bool IsIUPACName { get; set; }
    }
}
