﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.IO;
using System.Drawing;
using System.Net;
using System.ComponentModel;
using CommonGenericUIView.ViewModel;
using System.Text.RegularExpressions;
//using I4FTab.ViewModel;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged
          = delegate { };
        public static void NotifyStaticPropertyChanged(string propertyName)
        {
            StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }
        private CommonFunctions objCommon;
        public static string CASFromUpstream = string.Empty;
        public ObservableCollection<string> ListCASSearchTyp { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CommandGenerateAccessCasResult { get; set; }
        private static string _CAS { get; set; } = string.Empty;
        public static string CAS
        {
            get { return _CAS; }
            set
            {
                if (value != _CAS)
                {
                    _CAS = value;
                    NotifyStaticPropertyChanged("CAS");
                }
            }
        }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public string structurePath { get; set; }
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }


        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {

                case "List Dictionary":
                    //RunSAPData();
                    break;
                case "Data":
                    //RunSAPDataMain();
                    break;
                case "Properties":
                    //RunSAPProperties();
                    break;
                case "Characteristics":
                    //RunSAPCharacteristics();
                    break;
                case "SAP-Lists":
                    //RunListDic();
                    break;
                case "Prop-Characteristics":
                    //BindPropChar();
                    break;
                    //case "Unique Phrases":
                    //    CallUniquePhrase();
                    //    break;
            }
        }

        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommandTabChanged { get; set; }
        public ICommand CommandAddAdditionalComment { get; set; }

        public static string CommonListConn;
        public ICommand SearchCommand { get; set; }
        public ICommand SaveStructureImage { get; set; }
        public string CommentAction { get; set; }

        public CommonDataGrid_ViewModel<UploadCASSub> comonUpload_VM { get; set; }

        public string CASVal { get; set; }


        #region Generic Parent/Child
        public ICommand CommandLostFocusParentCas { get; set; }
        public ICommand CommandLostFocusChildCas { get; private set; }
        public Library_GenericsCategories selectedParentItemCategory { get; set; }
        public Library_GenericsCategories selectedChildItemCategory { get; set; }
        public string ParentCas
        {
            get { return _parentCas; }
            set
            {
                _parentCas = value;
                NotifyPropertyChanged("ParentCas");
            }
        }
        private string _parentCas { get; set; } = string.Empty;
        private string _childCas { get; set; } = "";
        public string ChildCas
        {
            get { return _childCas; }
            set
            {
                _childCas = value;
                NotifyPropertyChanged("ChildCas");
            }
        }
        public string ParentCasInvalid { get; set; } = string.Empty;
        public string ChildCasInvalid { get; set; } = string.Empty;
        private Library_SubstanceType _selectedParentSubstanceType { get; set; }

        public Library_SubstanceType selectedParentSubstanceType
        {
            get { return _selectedParentSubstanceType; }
            set
            {
                if (value == null)
                {
                    _selectedParentSubstanceType = value;
                    return;
                }
                if (value.SubstanceTypeID != 1)
                {
                    _selectedParentSubstanceType = value;
                }
                else
                {
                    ValidateSubstanceTypeInFlat(CASVal);
                    _selectedParentSubstanceType = value;
                    _notifier.ShowError("Substance Type not valid for Parent");
                }

            }
        }
        private Library_SubstanceType _selectedChildSubstanceType { get; set; }
        public Library_SubstanceType selectedChildSubstanceType
        {
            get { return _selectedChildSubstanceType; }
            set
            {
                _selectedChildSubstanceType = value;
                if (_selectedChildSubstanceType == null)
                {
                    return;
                }
                if (_selectedChildSubstanceType.SubstanceTypeID == 1)
                {
                    ValidateSubstanceTypeInFlat(CASVal);
                    IsEnableChildGenericCate = false;
                }
                else
                {
                    IsEnableChildGenericCate = true;
                }
                NotifyPropertyChanged("IsEnableChildGenericCate");
            }
        }
        public bool IsEnableChildGenericCate { get; set; } = false;
        private bool _isEditSubInfo { get; set; } = false;
        public bool IsEditSubInfo
        {
            get { return _isEditSubInfo; }
            set
            {
                _isEditSubInfo = value;
                NotifyPropertyChanged("IsEditSubInfo");
            }
        }

        public Library_SubstanceCategories selectedParentSubstanceCategory { get; set; }
        public Library_SubstanceCategories selectedChildSubstanceCategory { get; set; }

        public ObservableCollection<Library_SubstanceType> listParentSubstanceType { get; set; }
        public ObservableCollection<Library_SubstanceType> listChildSubstanceType { get; set; }

        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Parent { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        private string _parentName { get; set; }
        public string ParentName
        {
            get { return _parentName; }
            set
            {
                if (value != _parentName)
                {
                    ParentNameID = 0;
                    _parentName = value;
                    ListChemNameParentVisibility = Visibility.Visible;
                    NotifyPropertyChanged("ListChemNameParentVisibility");
                    FilterChemicalName_Parent(_parentName);
                    NotifyPropertyChanged("ParentName");
                }
            }
        }
        public int ParentNameID { get; set; }
        private GenericChemicalName_VM _selected_Chemical_ParentName { get; set; }
        public GenericChemicalName_VM Selected_Chemical_ParentName
        {
            get { return _selected_Chemical_ParentName; }
            set
            {
                if (_selected_Chemical_ParentName != value)
                {
                    _selected_Chemical_ParentName = value;
                    if (_selected_Chemical_ParentName != null)
                    {
                        ParentName = _selected_Chemical_ParentName.ChemicalName;
                        ParentNameID = _selected_Chemical_ParentName.ChemicalNameID;
                        ListChemNameParentVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameParentVisibility");
                    }
                    NotifyPropertyChanged("Selected_Chemical_ParentName");
                }
            }
        }
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Child { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        private string _ChildName { get; set; }
        public string ChildName
        {
            get { return _ChildName; }
            set
            {
                if (value != _ChildName)
                {
                    ChildNameID = 0;
                    _ChildName = value;
                    ListChemNameChildVisibility = Visibility.Visible;
                    NotifyPropertyChanged("ListChemNameChildVisibility");
                    FilterChemicalName_Child(_ChildName);
                    NotifyPropertyChanged("ChildName");
                }
            }
        }
        public int ChildNameID { get; set; }
        public Visibility ListChemNameParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemNameChildVisibility { get; set; } = Visibility.Collapsed;
        public ObservableCollection<Library_SubstanceCategories> listChildSubstanceCategory { get; set; }
        public ObservableCollection<Library_GenericsCategories> listGenericsCategory { get; set; }
        public ObservableCollection<Library_SubstanceCategories> listParentSubstanceCategory { get; set; }
        private GenericChemicalName_VM _selected_Chemical_ChildName { get; set; }
        public GenericChemicalName_VM Selected_Chemical_ChildName
        {
            get { return _selected_Chemical_ChildName; }
            set
            {
                if (_selected_Chemical_ChildName != value)
                {
                    _selected_Chemical_ChildName = value;
                    if (_selected_Chemical_ChildName != null)
                    {
                        ChildName = _selected_Chemical_ChildName.ChemicalName;
                        ChildNameID = _selected_Chemical_ChildName.ChemicalNameID;
                        ListChemNameChildVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameChildVisibility");
                    }
                    NotifyPropertyChanged("Selected_Chemical_ChildName");
                }
            }
        }
        public bool? IsExclude { get; set; } = false;
        public ObservableCollection<SelectListItem> ListItemNodeType { get; set; }
        public SelectListItem SelectedItemNodeType { get; set; }
        private string _commentSuggestionParent { get; set; }
        public string CommentSuggestionParent
        {
            get { return _commentSuggestionParent; }
            set
            {
                _commentSuggestionParent = value;
                NotifyPropertyChanged("CommentSuggestionParent");
            }
        }
        private string _commentGenericSuggestionAction { get; set; }
        public string CommentGenericSuggestionAction
        {
            get { return _commentGenericSuggestionAction; }
            set
            {
                _commentGenericSuggestionAction = value;
                NotifyPropertyChanged("CommentGenericSuggestionAction");
            }
        }
        public SelectListItem SelectedItemStatus { get; set; }
        public ICommand CommandAddMultipleGenericSuggestion { get; private set; }
        public ICommand CommandAddGenericSuggestion { get; private set; }
        public bool IsEnabledChildCas { get; set; } = false;
        public bool IsEnabledParentCas { get; set; } = false;
        public Visibility contentRemoveVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibilityAddEditName { get; set; } = Visibility.Collapsed;
        public ICommand CommandSaveAddEditChemicalName { get; set; }
        public ICommand CommandRemoveGenericSuggestion { get; set; }

        public bool _isCheckedCollpaseAll { get; set; } = false;
        public bool IsCheckedCollpaseAll
        {
            get { return _isCheckedCollpaseAll; }
            set
            {
                _isCheckedCollpaseAll = value;
                if (_isCheckedCollpaseAll)
                { ChangeAllSectionVisibility(Visibility.Visible); }
                else
                {
                    ChangeAllSectionVisibility(Visibility.Collapsed);
                }
                NotifyPropertyChanged("IsCheckedCollpaseAll");
            }
        }
        #endregion
        public Visibility VisibilityMain_Loader { get; set; } = Visibility.Collapsed;
        public string headerDisapproveItem { get; set; }
        public string CommentPopUpType { get; set; }
        public Visibility contentDisapproveVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentCommentsVisibility { get; set; }=Visibility.Collapsed;
        public ICommand CommandDisapproveItem { get; set; }
        public string PopupButtonColor { get; set; }
        public string PopupButtonText { get; set; }

        //private void CommandAddAdditionalCommentExecute(object obj)
        //{
        //    if (SelectedSubStanceType != null && SelectedSubStanceType.SubstanceTypeName != string.Empty)
        //    {
        //        using (var context = new CRAModel())
        //        {
        //            var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
        //            if (casId != 0)
        //            {

        //                objCommon.DbaseQueryReturnStringSQL("insert into Map_Substance_Comments(casid,comments, action,commentby,timestamp)  values (" + casId + ",'" + CommentAction + "','Single Substance Comment','" + Environment.UserName + "','" + objCommon.ESTTime() + "')", Win_Administrator_VM.CommonListConn);
        //                _notifier.ShowSuccess("comment added successfully");
        //                RefreshComments(SelectedSubStanceType.SubstanceTypeName);
        //                CommentAction = string.Empty;
        //                NotifyPropertyChanged("CommentAction");
        //            }
        //        }
        //    }
        //}


        private void SaveStructureImageExecute(object obj)
        {
            if (!structurePath.Contains("StructureNotAvailable"))
            {
                SaveImage();
                _notifier.ShowSuccess("Image saved in clipboard, save it anywhere you want");
            }
        }

        public void SaveImage()
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(structurePath);
            Bitmap bitmap; bitmap = new Bitmap(stream);

            if (bitmap != null)
            {
                System.Windows.Forms.Clipboard.SetImage(bitmap);
            }

            stream.Flush();
            stream.Close();
            client.Dispose();
        }

        private void SearchCommandExecute(object obj)
        {
            ChangeAllSectionVisibility(Visibility.Collapsed);
            using (var context = new CRAModel())
            {
                CommentAction = string.Empty;
                NotifyPropertyChanged("CommentAction");
                if (SelectedCasSrch != "Search By CAS")
                {
                    var casid = Convert.ToInt32(CAS.Replace("-", "").Trim());
                    CASVal = context.Library_CAS.AsNoTracking().Where(x => x.CASID == casid).Select(y => y.CAS).FirstOrDefault();
                    if (CASVal == null)
                    {
                        _notifier.ShowError("No such CAS ID exists, Kindly check");
                    }
                }
                else
                {

                    CASVal = CAS.Replace("-", "").Trim();
                    var chkExist = context.Library_CAS.AsNoTracking().FirstOrDefault(x => x.CAS == CASVal);
                    if (chkExist == null)
                    {
                        var chkValid = objCommon.CheckIsValidCRN(CASVal);
                        if (chkValid)
                        {
                            AddCasInSubstanidentity(chkValid);
                            _notifier.ShowInformation("CAS was not found in Library, and was added");
                        }
                        else
                        {

                            var result = System.Windows.MessageBox.Show("CAS not found in library, do you want to add it as a new CAS?" + Environment.NewLine + Environment.NewLine + "FYI CAs is " + (chkValid == true ? "Valid" : "Invalid"), "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (result == MessageBoxResult.Yes)
                            {
                                AddCasInSubstanidentity(chkValid);
                                _notifier.ShowSuccess("CAS Added successfully");
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                ValidateAlternateCas();
            }

            VisibilityListChangesReportQSID_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");


            ClearExistingFilterDataFromGrid();
            System.Threading.Tasks.Task.Run(() =>
            {

                var result = objCommon.DbaseQueryReturnSqlList<ListSIReport_VM>("Exec SubstanceIdentityDetail '" + CASVal + "'", Win_Administrator_VM.CommonListConn);
                ListChangesResult = result[0];
                NotifyPropertyChanged("ListChangesResult");
                Task.Run(() => CommandShowStructure());
                Task.Run(() => getDirectHitDetail());
                Task.Run(() => getSubrootDetail());
                Task.Run(() => getGenericHitDetail());
                Task.Run(() => getWebsitetDetail());
                Task.Run(() => getPrimarySubstanceDetail());
                Task.Run(() => getDisplayName());
                //getGroupNameDetail();
                Task.Run(() => getAlternateCASDetail());
                Task.Run(() => getNoofChildDetail());
                Task.Run(() => getNoofParentDetail());

                Task.Run(() => getSubstanceCategoryDetail());
                Task.Run(() => getSubstanceTypeDetail());
                Task.Run(() => getSynonymsDetail());
                Task.Run(() => getSynonymsDetailDisapprove());                
                Task.Run(() => getECNumberDetail());
                Task.Run(() => GetEcNumberDisapprovedDetail());               
                Task.Run(() => getMFormulaDetail());
                Task.Run(() => getMFormulaDetailDisapproved());
                Task.Run(() => getIUpacNameDetail());
                Task.Run(() => getENCSNumberDetail());
                Task.Run(() => getOtheridentDetail());
                Task.Run(() => getHydratesDetail());
                VisibilityListChangesReportQSID_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");
            });
            // System.Windows.Forms.Clipboard.Clear();
        }
        public void ClearExistingFilterDataFromGrid()
        {
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListDirectHit_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListSubroot_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListGenericHit_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListWebsite_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListPrimarySubstance_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(SubstanceIdentity_GenericChild));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(SubstanceIdentity_GenericParent));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListSubstanceCategory_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListSubstanceType_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListSynonyms_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListECNumber_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListECNumberDisapproved_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListMFormula_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListIUpacName_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListENCSNumber_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(ListOtherident_VM));
            MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>("", "Clear Filter", "DataGridFilter"), typeof(UploadCASSub));

        }

        private void CallbackExecute(object obj)
        {
        }

        public void AddCasInSubstanidentity(bool chkValid)
        {
            using (var context = new CRAModel())
            {

                var lib = new Library_CAS
                {
                    CAS = CASVal,
                    TimeStamp = objCommon.ESTTime(),
                    IsValid = chkValid,
                    IsInternalOnlyCAS = Regex.IsMatch(CASVal, "[a-z]"),
                    IsActive = true,
                };
                context.Library_CAS.Add(lib);
                context.SaveChanges();
            }


        }
        public Visibility GenericVisibleCollapsedFunction(Visibility currentVisibility)
        {

            if (currentVisibility == Visibility.Collapsed)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }

        }


        public void ChangeAllSectionVisibility(Visibility currentVisibility)
        {
            visibility_DirectHit = currentVisibility;
            visibility_GenericHit = currentVisibility;
            visibility_Website = currentVisibility;
            visibility_PrimarySubstance = currentVisibility;
            visibility_AlternateCAS = currentVisibility;
            visibility_NoofChild = currentVisibility;
            visibility_NoofParent = currentVisibility;
            visibility_SubstanceCategory = currentVisibility;
            visibility_SubstanceType = currentVisibility;
            visibility_Synonyms = currentVisibility;
            visibility_ECNumber = currentVisibility;
            visibility_MFormula = currentVisibility;
            visibility_IUpacName = currentVisibility;
            visibility_ENCSNumber = currentVisibility;
            visibility_Otherident = currentVisibility;
            visibility_Hydrates = currentVisibility;

            NotifyPropertyChanged("visibility_DirectHit");
            NotifyPropertyChanged("visibility_GenericHit");
            NotifyPropertyChanged("visibility_Website");
            NotifyPropertyChanged("visibility_PrimarySubstance");
            NotifyPropertyChanged("visibility_AlternateCAS");
            NotifyPropertyChanged("visibility_NoofChild");
            NotifyPropertyChanged("visibility_NoofParent");
            NotifyPropertyChanged("visibility_SubstanceCategory");
            NotifyPropertyChanged("visibility_SubstanceType");
            NotifyPropertyChanged("visibility_Synonyms");
            NotifyPropertyChanged("visibility_ECNumber");
            NotifyPropertyChanged("visibility_MFormula");
            NotifyPropertyChanged("visibility_IUpacName");
            NotifyPropertyChanged("visibility_ENCSNumber");
            NotifyPropertyChanged("visibility_Otherident");
            NotifyPropertyChanged("visibility_Hydrates");

            currentVisibility = currentVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;

            visibilityExpandMore_DirectHit = currentVisibility;
            visibilityExpandMore_Subroot = currentVisibility;
            visibilityExpandMore_GenericHit = currentVisibility;
            visibilityExpandMore_Website = currentVisibility;
            visibilityExpandMore_PrimarySubstance = currentVisibility;
            visibilityExpandMore_AlternateCAS = currentVisibility;
            visibilityExpandMore_NoofChild = currentVisibility;
            visibilityExpandMore_NoofParent = currentVisibility;
            visibilityExpandMore_SubstanceCategory = currentVisibility;
            visibilityExpandMore_SubstanceType = currentVisibility;
            visibilityExpandMore_Synonyms = currentVisibility;
            visibilityExpandMore_ECNumber = currentVisibility;
            visibilityExpandMore_MFormula = currentVisibility;
            visibilityExpandMore_IUpacName = currentVisibility;
            visibilityExpandMore_ENCSNumber = currentVisibility;
            visibilityExpandMore_Otherident = currentVisibility;
            visibilityExpandMore_Hydrates = currentVisibility;

            NotifyPropertyChanged("visibilityExpandMore_DirectHit");
            NotifyPropertyChanged("visibilityExpandMore_Subroot");
            NotifyPropertyChanged("visibilityExpandMore_GenericHit");
            NotifyPropertyChanged("visibilityExpandMore_Website");
            NotifyPropertyChanged("visibilityExpandMore_PrimarySubstance");
            NotifyPropertyChanged("visibilityExpandMore_AlternateCAS");
            NotifyPropertyChanged("visibilityExpandMore_NoofChild");
            NotifyPropertyChanged("visibilityExpandMore_NoofParent");
            NotifyPropertyChanged("visibilityExpandMore_SubstanceCategory");
            NotifyPropertyChanged("visibilityExpandMore_SubstanceType");
            NotifyPropertyChanged("visibilityExpandMore_Synonyms");
            NotifyPropertyChanged("visibilityExpandMore_ECNumber");
            NotifyPropertyChanged("visibilityExpandMore_MFormula");
            NotifyPropertyChanged("visibilityExpandMore_IUpacName");
            NotifyPropertyChanged("visibilityExpandMore_ENCSNumber");
            NotifyPropertyChanged("visibilityExpandMore_Otherident");
            NotifyPropertyChanged("visibilityExpandMore_Hydrates");

        }

        public Visibility GenericVisibleCollapsedFunction_Expand(Visibility currentVisibility)
        {
            if (currentVisibility == Visibility.Collapsed)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public bool IsSelectedTabData { get; set; }

        public CommonDataGrid_ViewModel<LibSI_Properties> comonSI_VM { get; set; }
        public ICommand CommandClosePopUp { get; private set; }

        public ListSIReport_VM ListChangesResult { get; set; }
        public Visibility VisibilityListChangesReportQSID_Loader { get; set; } = Visibility.Collapsed;

        private string _SelectedCasSrch { get; set; }
        public string SelectedCasSearchU { get; set; }
        public string SelectedCasSrch
        {
            get { return _SelectedCasSrch; }
            set
            {
                if (_SelectedCasSrch != value)
                {
                    _SelectedCasSrch = value;
                    NotifyPropertyChanged("SelectedCasSrch");
                    if (value == "Search By CAS")
                    {
                        SelectedCasSearchU = "Enter CAS";
                    }
                    else
                    {
                        SelectedCasSearchU = "Enter CASID";
                    }
                    NotifyPropertyChanged("SelectedCasSearchU");
                }

            }
        }
        public ICommand commandOpenCurrentDataViewNewAddList { get; set; }
        public CommentsUC_VM objCommentDC_PopUpComment { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public CommentsUC_VM objCommentDC_SubstanceIdentity { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        
        public ICommand CommandUpdateDisplayName { get; set; }

        public string DisplayNameSubstance { get; set; }

        public Win_Administrator_VM()
        {
            objCommon = new CommonFunctions();
            comonSI_VM = new CommonDataGrid_ViewModel<LibSI_Properties>(GetListGridColumnSI(), "SubstanceIdentity", "");

            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            Task.Run(() =>
            {
                listSubstanceType = objCommon.DbaseQueryReturnSqlList<LibSubType>("select SubstanceTypeID, SubstanceTypeName from Library_SubstanceType order by SubstanceTypeName", Win_Administrator_VM.CommonListConn);
                listSubstanceCategory = objCommon.DbaseQueryReturnSqlList<LibSubCategory>("select SubstanceCategoryID, SubstanceCategoryName from Library_SubstanceCategories order by SubstanceCategoryName", Win_Administrator_VM.CommonListConn);
            });
            Task.Run(() => { BindSubstanceType(); });
            Task.Run(() => { BindSubstanceCategories(); });
            Task.Run(() => { BindGenericsCategories(); });
            BindListItemNodeType();
            listCASType.Add(new LibType { TypID = 1, TypName = "Alternate CAS" });
            listCASType.Add(new LibType { TypID = 2, TypName = "Generics" });
            listCASType.Add(new LibType { TypID = 3, TypName = "Hydrates" });

            GetRefreshListDetailCommonControl();
            visibility_DirectHit = Visibility.Collapsed;
            visibility_Subroot = Visibility.Collapsed;
            visibility_GenericHit = Visibility.Collapsed;
            visibility_Website = Visibility.Collapsed;
            visibility_PrimarySubstance = Visibility.Collapsed;
            //visibility_GroupName = Visibility.Collapsed;
            visibility_AlternateCAS = Visibility.Collapsed;
            visibility_NoofChild = Visibility.Collapsed;
            visibility_NoofParent = Visibility.Collapsed;
            visibility_SubstanceCategory = Visibility.Collapsed;
            visibility_SubstanceType = Visibility.Collapsed;
            visibility_Synonyms = Visibility.Collapsed;
            visibility_ECNumber = Visibility.Collapsed;
            visibility_MFormula = Visibility.Collapsed;
            visibility_IUpacName = Visibility.Collapsed;
            visibility_ENCSNumber = Visibility.Collapsed;
            visibility_Otherident = Visibility.Collapsed;
            visibility_Hydrates = Visibility.Collapsed;

            visibilityExpandMore_DirectHit = Visibility.Visible;
            visibilityExpandMore_Subroot = Visibility.Visible;
            visibilityExpandMore_GenericHit = Visibility.Visible;
            visibilityExpandMore_Website = Visibility.Visible;
            visibilityExpandMore_PrimarySubstance = Visibility.Visible;
            //visibilityExpandMore_GroupName = Visibility.Visible;
            visibilityExpandMore_AlternateCAS = Visibility.Visible;
            visibilityExpandMore_NoofChild = Visibility.Visible;
            visibilityExpandMore_NoofParent = Visibility.Visible;
            visibilityExpandMore_SubstanceCategory = Visibility.Visible;
            visibilityExpandMore_SubstanceType = Visibility.Visible;
            visibilityExpandMore_Synonyms = Visibility.Visible;
            visibilityExpandMore_ECNumber = Visibility.Visible;
            visibilityExpandMore_MFormula = Visibility.Visible;
            visibilityExpandMore_IUpacName = Visibility.Visible;
            visibilityExpandMore_ENCSNumber = Visibility.Visible;
            visibilityExpandMore_Otherident = Visibility.Visible;
            visibilityExpandMore_Hydrates = Visibility.Visible;

            CommandDirectHit = new RelayCommand(CommandDirectHitExecute, CommandAccessCanExecute);
            CommandSubroot = new RelayCommand(CommandSubrootExecute, CommandAccessCanExecute);
            CommandGenericHit = new RelayCommand(CommandGenericHitExecute, CommandAccessCanExecute);
            CommandWebsite = new RelayCommand(CommandWebsiteExecute, CommandAccessCanExecute);
            CommandPrimarySubstance = new RelayCommand(CommandPrimarySubstanceExecute, CommandAccessCanExecute);
            CommandLostFocusParentCas = new RelayCommand(CommandLostFocusParentCasExecute);
            //CommandGroupName = new RelayCommand(CommandGroupNameExecute, CommandAccessCanExecute);
            CommandAlternateCAS = new RelayCommand(CommandAlternateCASExecute, CommandAccessCanExecute);
            CommandNoofChild = new RelayCommand(CommandNoofChildExecute, CommandAccessCanExecute);
            CommandNoofParent = new RelayCommand(CommandNoofParentExecute, CommandAccessCanExecute);
            CommandSubstanceCategory = new RelayCommand(CommandSubstanceCategoryExecute, CommandAccessCanExecute);
            CommandSubstanceType = new RelayCommand(CommandSubstanceTypeExecute, CommandAccessCanExecute);
            CommandSynonyms = new RelayCommand(CommandSynonymsExecute, CommandAccessCanExecute);
            CommandECNumber = new RelayCommand(CommandECNumberExecute, CommandAccessCanExecute);
            CommandMFormula = new RelayCommand(CommandMFormulaExecute, CommandAccessCanExecute);
            CommandIUpacName = new RelayCommand(CommandIUpacNameExecute, CommandAccessCanExecute);
            CommandENCSNumber = new RelayCommand(CommandENCSNumberExecute, CommandAccessCanExecute);
            CommandOtherident = new RelayCommand(CommandOtheridentExecute, CommandAccessCanExecute);
            CommandHydrates = new RelayCommand(CommandHydratesExecute, CommandAccessCanExecute);
            // CommandAddAdditionalComment = new RelayCommand(CommandAddAdditionalCommentExecute, CommandAccessCanExecute);
            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExecute, commandOpenCurrentDataViewNewAddListCanExecute);
            CommandUpdateDisplayName = new RelayCommand(CommandUpdateDisplayNameExecute);

            PrimarySubstanceUpdate = new RelayCommand(PrimarySubstanceUpdateExecute, CommandAccessCanExecute);
            PrimarySubstanceDelete = new RelayCommand(PrimarySubstanceDeleteExecute, CommandAccessCanExecute);
            SubstanceCategoryUpdate = new RelayCommand(SubstanceCategoryUpdateExecute, CommandAccessCanExecute);
            SubstanceCategoryDelete = new RelayCommand(SubstanceCategoryDeleteExecute, CommandAccessCanExecute);
            SubstanceTypeUpdate = new RelayCommand(SubstanceTypeUpdateExecute, CommandAccessCanExecute);
            SubstanceTypeDelete = new RelayCommand(SubstanceTypeDeleteExecute, CommandAccessCanExecute);

            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);
            UploadCommand = new RelayCommand(UploadCommandExecute, CommandAccessCanExecute);
            DownloadTemplateCommand = new RelayCommand(DownloadTemplateCommandExecute, CommandAccessCanExecute);
            SaveDataCommand = new RelayCommand(SaveDataCommandExecute, CommandAccessCanExecute);
            SaveStructureImage = new RelayCommand(SaveStructureImageExecute, CommandAccessCanExecute);

            var listItemCAS = new List<string>();
            listItemCAS.Add("Search By CAS");
            listItemCAS.Add("Search By CASID");
            ListCASSearchTyp = new ObservableCollection<string>(listItemCAS);

            SelectedCasSrch = ListCASSearchTyp.Where(x => x == "Search By CAS").FirstOrDefault();
            NotifyPropertyChanged("SelectedCasSearch");

            //IsSelectedTabData = true;
            CommandClosePopUp = new RelayCommand(CommandClosePopUpExecute);
            //CommandShowPopUpAddHydrate = new RelayCommand(CommandShowPopUpAddHydrateExecute);
            //CommandAddHydrate = new RelayCommand(CommandAddHydrateExecute, CommandAddHydrateCanExecute);
            //CommandAddMoreHydrate = new RelayCommand(CommandAddMoreHydrateExecute, CommandAddHydrateCanExecute);

            CommandClosePopUpECNumber = new RelayCommand(CommandClosePopUpECNumberExecute);
            CommandShowPopUpAddECNumber = new RelayCommand(CommandShowPopUpAddECNumberExecute);
            CommandAddECNumber = new RelayCommand(CommandAddECNumberExecute);
            CommandAddMoreECNumber = new RelayCommand(CommandAddMoreECNumberExecute);

            CommandClosePopUpMFormula = new RelayCommand(CommandClosePopUpMFormulaExecute);
            CommandShowPopUpAddMFormula = new RelayCommand(CommandShowPopUpAddMFormulaExecute);
            CommandAddMFormula = new RelayCommand(CommandAddMFormulaExecute);
            CommandAddMoreMFormula = new RelayCommand(CommandAddMoreMFormulaExecute);

            CommandClosePopUpIUpacName = new RelayCommand(CommandClosePopUpIUpacNameExecute);
            CommandShowPopUpAddIUpacName = new RelayCommand(CommandShowPopUpAddIUpacNameExecute);
            CommandAddIUpacName = new RelayCommand(CommandAddIUpacNameExecute);
            CommandAddMoreIUpacName = new RelayCommand(CommandAddMoreIUpacNameExecute);

            CommandClosePopUpENCSNumber = new RelayCommand(CommandClosePopUpENCSNumberExecute);
            CommandShowPopUpAddENCSNumber = new RelayCommand(CommandShowPopUpAddENCSNumberExecute);
            CommandAddENCSNumber = new RelayCommand(CommandAddENCSNumberExecute);
            CommandAddMoreENCSNumber = new RelayCommand(CommandAddMoreENCSNumberExecute);

            CommandClosePopUpSynonyms = new RelayCommand(CommandClosePopUpSynonymsExecute);
            CommandShowPopUpAddSynonyms = new RelayCommand(CommandShowPopUpAddSynonymsExecute);
            CommandAddSynonyms = new RelayCommand(CommandAddSynonymsExecute);
            CommandAddMoreSynonyms = new RelayCommand(CommandAddMoreSynonymsExecute);

            CommandClosePopUpNoofChild = new RelayCommand(CommandClosePopUpNoofChildExecute);
            CommandShowPopUpAddNoofChild = new RelayCommand(CommandShowPopUpAddNoofChildExecute);
            CommandAddNoofChild = new RelayCommand(CommandAddNoofChildExecute);
            CommandAddMoreNoofChild = new RelayCommand(CommandAddMoreNoofChildExecute);
            CommandLostFocusChildCas = new RelayCommand(CommandLostFocusChildCasExecute);
            CommandAddMultipleGenericSuggestion = new RelayCommand(CommandAddMultipleGenericSuggestionExecute, CommandAddGenericSuggestionCanExecute);
            CommandAddGenericSuggestion = new RelayCommand(CommandAddGenericSuggestionExecute, CommandAddGenericSuggestionCanExecute);
            CommandSaveAddEditChemicalName = new RelayCommand(CommandSaveAddEditChemicalName_Execute);
            CommandRemoveGenericSuggestion = new RelayCommand(CommandRemoveGenericSuggestionExecute);

            CommandDisapproveItem = new RelayCommand(CommandDisapproveItemExecute);

            MessengerInstance.Register<PropertyChangedMessage<ListECNumber_VM>>(this, NotifyMeECNumber);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListECNumber_VM), CommandButtonExecutedECNumber);

            MessengerInstance.Register<PropertyChangedMessage<ListMFormula_VM>>(this, NotifyMeMFormula);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListMFormula_VM), CommandButtonExecutedMFormula);

            MessengerInstance.Register<PropertyChangedMessage<ListIUpacName_VM>>(this, NotifyMeIUpacName);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListIUpacName_VM), CommandButtonExecutedIUpacName);

            MessengerInstance.Register<PropertyChangedMessage<ListENCSNumber_VM>>(this, NotifyMeENCSNumber);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListENCSNumber_VM), CommandButtonExecutedENCSNumber);

            MessengerInstance.Register<PropertyChangedMessage<ListSynonyms_VM>>(this, NotifyMeSynonyms);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListSynonyms_VM), CommandButtonExecutedSynonyms);

            MessengerInstance.Register<PropertyChangedMessage<ListSynonymsDisapproved_VM>>(this, NotifyMeSynonyms);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListSynonymsDisapproved_VM), CommandButtonExecutedSynonyms);

            MessengerInstance.Register<PropertyChangedMessage<SubstanceIdentity_GenericChild>>(this, NotifyMeNoofChild);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceIdentity_GenericChild), CommandButtonExecutedNoofChild);

            MessengerInstance.Register<PropertyChangedMessage<SubstanceIdentity_GenericParent>>(this, NotifyMeNoofParent);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceIdentity_GenericParent), CommandButtonExecutedNoofParent);

            MessengerInstance.Register<PropertyChangedMessage<ListWebsite_VM>>(this, NotifyMeWebsite);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListWebsite_VM), CommandButtonExecutedWebsite);

            MessengerInstance.Register<PropertyChangedMessage<ListECNumberDisapproved_VM>>(this, NotifyMeECNumberDisapproved);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListECNumberDisapproved_VM), CommandButtonExecutedECNumberDisapproved);

            MessengerInstance.Register<PropertyChangedMessage<ListMFormulaDisapproved_VM>>(this, NotifyMeMFormula);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListMFormulaDisapproved_VM), CommandButtonExecutedMFormula);

            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListSubroot_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListDirectHit_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListGenericHit_VM), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ListOtherident_VM), CallbackHyperlink);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), NotifyAfterSubstanceIdentityTabActivate);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            if (CASFromUpstream != string.Empty)
            {
                SearchCommandExecute(null);
            }

        }

        private void CommandDisapproveItemExecute(object obj)
        {
            string type = (string)obj;
            if (type == "EcNumberDisapprove")
            {
                DeleteECNumber();
            } else if (type == "EcNumberApprove")
            { 
            RevertECNumberDisapproved();
            }
            else if (type == "MFormulaDisapprove")
            {
                DeleteMFormula();
            }
            else if (type == "MFormulaApprove")
            {
                RevertMFormulaDisapproved();
            }
            else if (type == "SynonymsDisapprove")
            {
                DeleteSynonyms();
            }
            else if (type == "SynonymsApprove")
            {
                RevertSynonymsDisapproved();
            }
            IsShowPopUp = false;
        }

        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        private void CommandUpdateDisplayNameExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to update the Display Name", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                _notifier.ShowInformation("Display name update in-progress, System will notify when it completed.");
                Task.Run(() =>
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        List<Log_Map_DisplayName> lstlog = new List<Log_Map_DisplayName>();
                        string status = string.Empty;
                        using (var context = new CRAModel())
                        {
                            var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                            var itemToUpdate = context.Map_DisplayName.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                            var ChemicalName = objCommon.RemoveWhitespaceCharacter(DisplayNameSubstance, GetStripCharacter());
                            ChemicalName = System.Text.RegularExpressions.Regex.Replace(ChemicalName, @"\s+", " ").Trim();
                           
                            var chemID = _objLibraryFunction_Service.InsertIfNotExistChemicalName(ChemicalName, true);
                           
                            if (itemToUpdate == null)
                            {
                                var main = new Map_DisplayName
                                {
                                    CASID = casId,
                                    ChemicalNameID = chemID,
                                };
                                if (itemToUpdate == null)// means no record exist previously
                                {
                                    List<Map_DisplayName> lstmain = new List<Map_DisplayName>();
                                    lstmain.Add(main);
                                    _objLibraryFunction_Service.BulkIns<Map_DisplayName>(lstmain);
                                    status = "A";
                                    itemToUpdate = context.Map_DisplayName.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                                }
                            }
                            else
                            {
                                if (itemToUpdate.ChemicalNameID != chemID)
                                {
                                    objCommon.DbaseQueryReturnStringSQL("update Map_DisplayName set ChemicalNameID = " + chemID + " where Map_DisplayNameID =" + itemToUpdate.Map_DisplayNameID, Win_Administrator_VM.CommonListConn);
                                    status = "C";
                                }
                            }
                            if (status != string.Empty)
                            {
                                var log = new Log_Map_DisplayName
                                {
                                    //SessionID = Engine.CurrentUserSessionID,
                                    Timestamp = objCommon.ESTTime(),
                                    Status = status,
                                    CASID = casId,
                                    ChemicalNameID = chemID,
                                   // PreferredSubstanceNameID = itemToUpdate.Map_DisplayNameID,
                                };
                                lstlog.Add(log);
                                _objLibraryFunction_Service.BulkIns<Log_Map_DisplayName>(lstlog);
                                _notifier.ShowSuccess("Display Name Update Successfully");
                                MaterialDesignThemes.Wpf.Flipper.FlipCommand.Execute(obj, null);
                            }

                        }
                        SearchCommandExecute(obj);
                    });

                });
            }
            else
            {
                SearchCommandExecute(obj);
            }
        }

        private void NotifyAfterSubstanceIdentityTabActivate(PropertyChangedMessage<string> flag)
        {
            if (flag.PropertyName == "RefreshDetailGrids")
            {
                Task.Run(() => getHydratesDetail());
                Task.Run(() => getAlternateCASDetail());
            }
            else if (flag.PropertyName == "SearchCasInSubstanceIdentityTab")
            {
                CAS = flag.NewValue;
                SelectedCasSearchU = "Enter CAS";
                NotifyPropertyChanged("SelectedCasSearchU");
                SearchCommandExecute(null);
            }
            if (IsShowPopUpNoofParent == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpNoofParent");
            }
            if (IsShowPopUpNoofChild == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpNoofChild");
            }
            if (IsShowPopUpSynonyms == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpSynonyms");
            }
            if (IsShowPopUpENCSNumber == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpENCSNumber");
            }
            if (IsShowPopUpIUpacName == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpIUpacName");
            }
            if (IsShowPopUpMFormula == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpMFormula");
            }
            if (IsShowPopUpECNumber == Visibility.Visible)
            {
                NotifyPropertyChanged("IsShowPopUpECNumber");
            }
        }

        private void CommandRemoveGenericSuggestionExecute(object obj)
        {
            DeleteGenericSugestion();
        }

        public void BindSubstanceType()
        {
            listParentSubstanceType = new ObservableCollection<Library_SubstanceType>(_objGeneric_Service.GetAllSubstanceType());
            NotifyPropertyChanged("listParentSubstanceType");
            listChildSubstanceType = new ObservableCollection<Library_SubstanceType>(_objGeneric_Service.GetAllSubstanceType());
            NotifyPropertyChanged("listChildSubstanceType");
        }
        public void BindSubstanceCategories()
        {
            listChildSubstanceCategory = new ObservableCollection<Library_SubstanceCategories>(_objGeneric_Service.GetAllSubstanceCategories());
            NotifyPropertyChanged("listSubstanceCategory");
            listParentSubstanceCategory = new ObservableCollection<Library_SubstanceCategories>(_objGeneric_Service.GetAllSubstanceCategories());
            NotifyPropertyChanged("listParentSubstanceCategory");
        }
        public void BindGenericsCategories()
        {
            listGenericsCategory = new ObservableCollection<Library_GenericsCategories>(_objGeneric_Service.GetAllGenericsCategories());
            NotifyPropertyChanged("listGenericsCategory");
        }
        public void BindListItemNodeType()
        {
            var listItemNode = new List<SelectListItem>();
            listItemNode.Add(new SelectListItem { Text = "Nested" });
            listItemNode.Add(new SelectListItem { Text = "Separate" });
            ListItemNodeType = new ObservableCollection<SelectListItem>(listItemNode);
            SelectedItemNodeType = ListItemNodeType.Where(x => x.Text == "Nested").First();
            NotifyPropertyChanged("ListItemNodeType");
            NotifyPropertyChanged("SelectedItemNodeType");
        }

        private bool commandOpenCurrentDataViewNewAddListCanExecute(object obj)
        {
            if (VisibilityListChangesReportQSID_Loader == Visibility.Visible)
            { return false; }
            else
            {
                return true;
            }
        }

        private void CallbackHyperlink(NotificationMessageAction<object> obj)
        {

            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
        public void NotifyMe(string callAction)
        {

        }
        public void commandOpenCurrentDataViewNewAddListExecute(object obj)
        {
            VisibilityListChangesReportQSID_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");
            Task.Run(() =>
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    try
                    {

                        Engine.IsOpenCurrentDataViewFromVersionHis = true;
                        Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                        Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                        MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

                    }
                    catch (Exception ex)
                    {

                        _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);

                    }
                    VisibilityListChangesReportQSID_Loader = Visibility.Collapsed;
                    NotifyPropertyChanged("VisibilityListChangesReportQSID_Loader");
                });
            });
        }


        public List<IStripChar> GetStripCharacter()
        {
            using (var context = new CRAModel())
            {
                return context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
            }
        }

        public void GetRefreshListDetailCommonControl()
        {
            if (Engine._listCheckboxType == null)
            {
                var listCheckBox = new List<VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel>();
                listCheckBox.Add(new VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel { Content = "Yes", Value = true });
                listCheckBox.Add(new VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel { Content = "No", Value = false });
                Engine._listCheckboxType = listCheckBox;
            }
            comonDirectHit_VM = new CommonDataGrid_ViewModel<ListDirectHit_VM>(GetListDirectHit(), "Qsid", "DirectHit :");
            NotifyPropertyChanged("comonDirectHit_VM");
            comonSubroot_VM = new CommonDataGrid_ViewModel<ListSubroot_VM>(GetListSubroot(), "Qsid", "Subroot :");
            NotifyPropertyChanged("comonSubroot_VM");
            comonGenericHit_VM = new CommonDataGrid_ViewModel<ListGenericHit_VM>(GetListGenericHit(), "Qsid", "GenericHit :");
            NotifyPropertyChanged("comonGenericHit_VM");
            comonWebsite_VM = new CommonDataGrid_ViewModel<ListWebsite_VM>(GetListWebsite(), "Qsid", "Websites :");
            NotifyPropertyChanged("comonWebsite_VM");
            comonPrimarySubstance_VM = new CommonDataGrid_ViewModel<ListPrimarySubstance_VM>(GetListPrimarySubstance(), "Qsid", "PrimarySubstance :");
            NotifyPropertyChanged("comonPrimarySubstance_VM");
            //comonGroupName_VM = new CommonDataGrid_ViewModel<ListGroupName_VM>(GetListGroupName(), "Qsid", "GroupName :");
            //NotifyPropertyChanged("comonGroupName_VM");

            comonNoofChild_VM = new CommonDataGrid_ViewModel<SubstanceIdentity_GenericChild>(GetListNoofChild(), "Qsid", "NoofChild :");
            NotifyPropertyChanged("comonNoofChild_VM");
            commonNoofParent_VM = new CommonDataGrid_ViewModel<SubstanceIdentity_GenericParent>(GetListNoofParent(), "Qsid", "Generic Parent List :");
            NotifyPropertyChanged("commonNoofParent_VM");
            comonSubstanceCategory_VM = new CommonDataGrid_ViewModel<ListSubstanceCategory_VM>(GetListSubstanceCategory(), "Qsid", "SubstanceCategory :");
            NotifyPropertyChanged("comonSubstanceCategory_VM");
            comonSubstanceType_VM = new CommonDataGrid_ViewModel<ListSubstanceType_VM>(GetListSubstanceType(), "Qsid", "SubstanceType :");
            NotifyPropertyChanged("comonSubstanceType_VM");
            comonSynonyms_VM = new CommonDataGrid_ViewModel<ListSynonyms_VM>(GetListSynonyms("Approved"), "Qsid", "Synonyms :");
            NotifyPropertyChanged("comonSynonyms_VM");
            comonSynonymsDisapproved_VM = new CommonDataGrid_ViewModel<ListSynonymsDisapproved_VM>(GetListSynonyms("DisApproved"), "Qsid", "Synonyms :");
            NotifyPropertyChanged("comonSynonymsDisapproved_VM");
            comonECNumber_VM = new CommonDataGrid_ViewModel<ListECNumber_VM>(GetListECNumber(), "Qsid", "ECNumber :");
            NotifyPropertyChanged("comonECNumber_VM");
            comonECNumberDisaaproved_VM = new CommonDataGrid_ViewModel<ListECNumberDisapproved_VM>(GetListECNumberDisapproved(), "Qsid", "ECNumber :");
            NotifyPropertyChanged("comonECNumberDisaaproved_VM");

            comonMFormula_VM = new CommonDataGrid_ViewModel<ListMFormula_VM>(GetListMFormula("Approved"), "Qsid", "MFormula :");
            NotifyPropertyChanged("comonMFormula_VM");

            comonMFormulaDisapproved_VM = new CommonDataGrid_ViewModel<ListMFormulaDisapproved_VM>(GetListMFormula("DisApproved"), "Qsid", "MFormula Disapproved :");
            NotifyPropertyChanged("comonMFormulaDisapproved_VM");

            
            comonIUpacName_VM = new CommonDataGrid_ViewModel<ListIUpacName_VM>(GetListIUpacName(), "Qsid", "IUpacName :");
            NotifyPropertyChanged("comonIUpacName_VM");
            comonENCSNumber_VM = new CommonDataGrid_ViewModel<ListENCSNumber_VM>(GetListENCSNumber(), "Qsid", "ENCSNumber :");
            NotifyPropertyChanged("comonENCSNumber_VM");
            comonOtherident_VM = new CommonDataGrid_ViewModel<ListOtherident_VM>(GetListOtherident(), "Qsid", "Otherident :");
            NotifyPropertyChanged("comonOtherident_VM");
            comonUpload_VM = new CommonDataGrid_ViewModel<UploadCASSub>(GetListUpload(), "Qsid", "Upload CAS + SubstanceName :");
            NotifyPropertyChanged("comonUpload_VM");
        }
        private void CommandLostFocusChildCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ChildCas))
            {
                Task.Run(() =>
                {
                    Map_SubstanceType_CategoryByCasID(ChildCas, "child");
                    var IsValid = CheckCasInValid(ChildCas);
                    //if (casTypes == "parent")
                    //{
                    //    ParentCasInvalid = IsValid ? string.Empty : "InValid";
                    //    NotifyPropertyChanged("ParentCasInvalid");
                    //}
                    //else if (casTypes == "child")
                    //{
                    ChildCasInvalid = IsValid ? string.Empty : "InValid";
                    NotifyPropertyChanged("ChildCasInvalid");
                    //}
                });

                int? ChemNameID = _objGeneric_Service.GetChildChemicalNameIDByCas(ChildCas);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();// listAllChemicalName_Backend.Find(x => x.ChemicalNameID == ChemNameID);
                    if (foundRes != null)
                    {
                        Selected_Chemical_ChildName = foundRes;
                    }
                }
                else
                {
                    ChildName = string.Empty;
                    ChildNameID = 0;
                    Selected_Chemical_ChildName = null;
                }
            }
        }
        private void CommandClosePopUpExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            if (Currenttab == "DisapproveSection") {
                contentDisapproveVisibility=Visibility.Collapsed;
                NotifyPropertyChanged("contentDisapproveVisibility");
            }else if (Currenttab == "DisapproveSection")
            {
                contentDisapproveVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("contentCommentsVisibility");
                IsShowPopUp = false;
            }
        }
        public bool IsEdit { get; set; } = true;

        public void ClearPopUpVariable()
        {
            IsShowPopUpECNumber = Visibility.Collapsed;
            IsShowPopUpMFormula = Visibility.Collapsed;
            IsShowPopUpIUpacName = Visibility.Collapsed;
            IsShowPopUpENCSNumber = Visibility.Collapsed;
            IsShowPopUpSynonyms = Visibility.Collapsed;
            //IsShowPopUpGroupName = Visibility.Collapsed;
            IsShowPopUpNoofChild = Visibility.Collapsed;
            contentVisibilityAddEditName = Visibility.Collapsed;
            contentRemoveVisibility = Visibility.Collapsed;
            contentCommentsVisibility = Visibility.Collapsed;
            contentDisapproveVisibility = Visibility.Collapsed;
            CommentGenericSuggestionAction = string.Empty;
            NotifyPropertyChanged("CommentGenericSuggestionAction");
            NotifyPropertyChanged("contentCommentsVisibility");
            NotifyPropertyChanged("contentDisapproveVisibility");
            NotifyPropertyChanged("IsShowPopUpECNumber");
            NotifyPropertyChanged("IsShowPopUpMFormula");
            NotifyPropertyChanged("IsShowPopUpIUpacName");
            NotifyPropertyChanged("IsShowPopUpENCSNumber");
            NotifyPropertyChanged("IsShowPopUpSynonyms");
            //NotifyPropertyChanged("IsShowPopUpGroupName");
            NotifyPropertyChanged("IsShowPopUpNoofChild");
            NotifyPropertyChanged("contentVisibilityAddEditName");
            NotifyPropertyChanged("contentRemoveVisibility");
        }

        public LibSI_Properties SelectedProperty { get; set; }

        private void NotifyMe(PropertyChangedMessage<LibSI_Properties> obj)
        {
            SelectedProperty = obj.NewValue;
        }

        private bool _IsShowPopUp { get; set; }
        public bool IsShowPopUp
        {
            get { return _IsShowPopUp; }
            set
            {
                if (_IsShowPopUp != value)
                {
                    _IsShowPopUp = value;
                    NotifyPropertyChanged("IsShowPopUp");
                }
            }
        }

        private ObservableCollection<LibSI_Properties> _lstSAPProperty { get; set; } = new ObservableCollection<LibSI_Properties>();
        public ObservableCollection<LibSI_Properties> lstSAPProperty
        {
            get { return _lstSAPProperty; }
            set
            {
                if (_lstSAPProperty != value)
                {
                    _lstSAPProperty = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<LibSI_Properties>>>(new PropertyChangedMessage<List<LibSI_Properties>>(null, _lstSAPProperty.ToList(), "Default List"));
                }
            }
        }

        public Visibility VisibilityListDetailDashboard_Loader { get; set; }
        private void BindProperty()
        {
            VisibilityListDetailDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
            Task.Run(() =>
            {
                var SAPPropertyTable = objCommon.DbaseQueryReturnSqlList<LibSI_Properties>("select 1  as NoofListDirectHit from SAP.Library_SAP_Properties", Win_Administrator_VM.CommonListConn);
                lstSAPProperty = new ObservableCollection<LibSI_Properties>(SAPPropertyTable);

            });
            VisibilityListDetailDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
        }
        private void RefreshGrid(PropertyChangedMessage<string> flag)
        {
            BindProperty();
            if (IsShowPopUp)
            {
                NotifyPropertyChanged("IsShowPopUp");
            }
        }

        private void CommandShowStructure()
        {
            try
            {
                var casNew = objCommon.SetCasRN(CASVal);
                string chemID = objCommon.DbaseQueryReturnStringSQL("Select top 1 mce.CompoundId from Map_CAS_ExtWebsiteIDs mce inner join Library_CAS lc on lc.CASID=mce.CASID where lc.CAS='" + CASVal + "'", Win_Administrator_VM.CommonListConn);
                if (!string.IsNullOrEmpty(chemID))
                {
                    structurePath = "https://pubchem.ncbi.nlm.nih.gov/image/imagefly.cgi?cid=" + chemID;

                }
                else
                {
                    structurePath = @"../Images/StructureNotAvailable.png";
                }
                NotifyPropertyChanged("structurePath");
                //ChromeDriverService service = ChromeDriverService.CreateDefaultService(System.Windows.Forms.Application.StartupPath + "\\chromedriver_win32");
                //service.HideCommandPromptWindow = true;
                //var options = new ChromeOptions();
                //options.AddArgument("--window-position=-32000,-32000");
                //options.AddArgument("headless");
                //var driverGc = new ChromeDriver(service, options);
                //driverGc.Navigate().GoToUrl("https://pubchem.ncbi.nlm.nih.gov/#query=" + casNew);
                //System.Threading.Thread.Sleep(5000);
                ////driverGc.FindElement(By.XPath("(//div[text()='Compound Best Match']/parent::div//a)[1]")).Click();
                //var path = driverGc.FindElement(By.XPath("(//div[text()='Compound Best Match']/parent::div//img)[1]"));
                //structurePath = path.GetAttribute("src") + "&t=l";
                //NotifyPropertyChanged("structurePath");
                //driverGc.Close();
                //driverGc.Quit();
            }
            catch (Exception ex)
            {
                structurePath = @"../Images/StructureNotAvailable.png";
                NotifyPropertyChanged("structurePath");
            }

        }
        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapHydrate")
            {
            }
            else if (obj.Notification == "AddEditName")
            {
                IsEdit = false;
                NotifyPropertyChanged("IsEdit");
                NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnSI()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NoofListDirectHit", ColBindingName = "NoofListDirectHit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NoofListGenericHit", ColBindingName = "NoofListGenericHit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PrimarySubstanceName", ColBindingName = "PrimarySubstanceName", ColType = "Textbox" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GroupName", ColBindingName = "GroupName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "AlternateCAS", ColBindingName = "AlternateCAS", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NoofChildren", ColBindingName = "NoofChildren", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceCategory", ColBindingName = "SubstanceCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceType", ColBindingName = "SubstanceType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Synonyms", ColBindingName = "Synonyms", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ECNumbers", ColBindingName = "ECNumbers", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "MolecularFormula", ColBindingName = "MolecularFormula", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IUPACNames", ColBindingName = "IUPACNames", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ENCSNumber", ColBindingName = "ENCSNumber", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OtherIdentifiers", ColBindingName = "OtherIdentifiers", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Hydrates", ColBindingName = "Hydrates", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "ID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditName" });

            return listColumnQsidDetail;
        }
        #region Function Generic parent and Child Cas
        private void CommandAddGenericSuggestionExecute(object obj)
        {
            if (ValidationAddSuggestion(true, true))
            {
                return;
            }
            AddGenericSuggestion();
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }
        private bool CommandAddGenericSuggestionCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(ParentCas) || string.IsNullOrEmpty(ChildCas))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ValidationAddSuggestion(bool? IsParentCheck, bool? IsChildCheck)
        {
            bool result = false;
            if (IsParentCheck == true)
            {
                if (!string.IsNullOrEmpty(ParentCas) && (string.IsNullOrEmpty(ParentName) || string.IsNullOrWhiteSpace(ParentName)))
                {
                    result = true;
                    _notifier.ShowWarning("Parent Name is mandatory!");
                }
            }
            if (IsChildCheck == true)
            {
                if (!string.IsNullOrEmpty(ChildCas) && (string.IsNullOrEmpty(ChildName) || string.IsNullOrWhiteSpace(ChildName)))
                {
                    result = true;
                    _notifier.ShowWarning("Child Name is mandatory!");
                }
            }
            return result;
        }
        private void CommandAddMultipleGenericSuggestionExecute(object obj)
        {
            if (ValidationAddSuggestion(true, true))
            {
                return;
            }
            AddGenericSuggestion();
            ClearVariable();
        }
        public void AddGenericSuggestion()
        {
            List<GenericSuggestionVM> listGenericSuggestionVM = new List<GenericSuggestionVM>();
            GenericSuggestionVM objGenericSugg = MapSuggestionVMFromLocalVar(ParentCas, ChildCas, ParentName, ChildName, ParentNameID, ChildNameID, SelectedItemNodeType.Text, CommentGenericSuggestionAction, IsExclude, CommentSuggestionParent, "Add");
            objGenericSugg.ParentMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedParentItemCategory, selectedParentSubstanceType, selectedParentSubstanceCategory);
            objGenericSugg.ChildMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedChildItemCategory, selectedChildSubstanceType, selectedChildSubstanceCategory);
            listGenericSuggestionVM.Add(objGenericSugg);
            var dicOutRes = _objGeneric_Service.InsertGenericSuggestion(listGenericSuggestionVM, Engine.CurrentUserSessionID, "TreeView");
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            if (dicOutRes.Any(x => x.Type == "Error"))
            {
                foreach (var item in dicOutRes.Where(x => x.Type == "Error").ToList())
                {
                    _notifier.ShowError("Error: " + item.Description);
                }
            }
            else
            {

                _notifier.ShowSuccess("Generic Suggestion saved successfully");
            }

        }
        public void ClearVariable()
        {
            ParentCas = string.Empty;
            ChildCas = string.Empty;
            SelectedItemStatus = null;
            ParentCasInvalid = string.Empty;
            ChildCasInvalid = string.Empty;
            selectedChildSubstanceType = null;
            selectedChildSubstanceCategory = null;
            selectedChildItemCategory = null;
            selectedParentSubstanceType = null;
            selectedParentSubstanceCategory = null;
            selectedParentItemCategory = null;
            NotifyPropertyChanged("selectedChildSubstanceType");
            NotifyPropertyChanged("selectedChildSubstanceCategory");
            NotifyPropertyChanged("selectedChildItemCategory");
            NotifyPropertyChanged("selectedParentSubstanceType");
            NotifyPropertyChanged("selectedParentSubstanceCategory");
            NotifyPropertyChanged("selectedParentItemCategory");
            NotifyPropertyChanged("ParentCasInvalid");
            NotifyPropertyChanged("ChildCasInvalid");
            NotifyPropertyChanged("ParentCas");
            NotifyPropertyChanged("ChildCas");
            NotifyPropertyChanged("SelectedItemStatus");
        }
        public Map_Generic_Detail MapGenericCatIDsVMFromLocalVar(Library_GenericsCategories selectedGenericCate, Library_SubstanceType selectedSubType, Library_SubstanceCategories selectedSubCate)
        {
            Map_Generic_Detail objMapGeneric = new Map_Generic_Detail();
            objMapGeneric.GenericsCategoryID = selectedGenericCate != null ? selectedGenericCate.GenericsCategoryID : (int?)null;
            objMapGeneric.SubstanceTypeID = selectedSubType != null ? selectedSubType.SubstanceTypeID : (int?)null;
            objMapGeneric.SubstanceCategoryID = selectedSubCate != null ? selectedSubCate.SubstanceCategoryID : (int?)null;
            return objMapGeneric;
        }
        public GenericSuggestionVM MapSuggestionVMFromLocalVar(string _parentCas, string _childCas, string _parentName, string _childName, int _parentNameID, int _childNameID, string _nodetype, string _comment, bool? _isExclude, string _parentComment, string _status)
        {
            GenericSuggestionVM objGenericSugg = new GenericSuggestionVM();
            objGenericSugg.ParentCas = _parentCas;
            objGenericSugg.ChildCas = _childCas;
            objGenericSugg.IsExclude = _isExclude;
            objGenericSugg.Status = _status;
            objGenericSugg.ChemicalName = _childName;
            objGenericSugg.ChemicalNameID = _childNameID;
            objGenericSugg.ParentChemicalName = _parentName;
            objGenericSugg.ParentChemicalNameID = _parentNameID;
            objGenericSugg.NodeType = _nodetype;
            objGenericSugg.UserID = Environment.UserName;
            objGenericSugg.Timestamp = DateTime.Now;
            objGenericSugg.Note = _comment;
            objGenericSugg.ParentComment = _parentComment;
            // objGenericSugg.GenericsCategoryID = objSelectCat != null ? objSelectCat.GenericsCategoryID : (int?)null;
            return objGenericSugg;
        }
        private void CommandLostFocusParentCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ParentCas))
            {
                Task.Run(() =>
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Map_SubstanceType_CategoryByCasID(ParentCas, "parent");
                        var IsValid = CheckCasInValid(ParentCas);
                        ParentCasInvalid = IsValid ? string.Empty : "InValid";
                        NotifyPropertyChanged("ParentCasInvalid");
                    });
                });
                int? ChemNameID = _objGeneric_Service.GetParentChemicalNameIDByCas(ParentCas);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();
                    if (foundRes != null)
                    {
                        Selected_Chemical_ParentName = foundRes;
                    }
                }
                else
                {
                    ParentName = string.Empty;
                    ParentNameID = 0;
                    Selected_Chemical_ParentName = null;
                }
            }
        }
        public void Map_SubstanceType_CategoryByCasID(string cas, string type)
        {
            var objCas = _objGeneric_Service.GetLibraryCasByCas(cas);
            if (objCas != null)
            {
                var existingsubstanceTypeID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceType", objCas.CASID);
                var existingsubstanceCategoryID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceCategory", objCas.CASID);
                var existinggenericCategoryID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("genericCategory", objCas.CASID);
                if (type == "child")
                {
                    if (existingsubstanceTypeID != 0)
                    {
                        selectedChildSubstanceType = listChildSubstanceType.Where(x => x.SubstanceTypeID == existingsubstanceTypeID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildSubstanceType");
                    }
                    //else
                    //{
                    //    selectedChildSubstanceType = null;// new Library_SubstanceType();
                    //    NotifyPropertyChanged("selectedChildSubstanceType");
                    //}
                    if (existingsubstanceCategoryID != 0)
                    {
                        selectedChildSubstanceCategory = listChildSubstanceCategory.Where(x => x.SubstanceCategoryID == existingsubstanceCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildSubstanceCategory");
                    }
                    //else
                    //{
                    //    selectedChildSubstanceCategory = null;// new Library_SubstanceType();
                    //    NotifyPropertyChanged("selectedChildSubstanceCategory");
                    //}
                    if (existinggenericCategoryID != 0)
                    {
                        selectedChildItemCategory = listGenericsCategory.Where(x => x.GenericsCategoryID == existinggenericCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildItemCategory");
                    }
                }
                else
                {
                    if (existingsubstanceTypeID != 0)
                    {
                        selectedParentSubstanceType = listParentSubstanceType.Where(x => x.SubstanceTypeID == existingsubstanceTypeID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentSubstanceType");
                    }
                    if (existingsubstanceCategoryID != 0)
                    {
                        selectedParentSubstanceCategory = listParentSubstanceCategory.Where(x => x.SubstanceCategoryID == existingsubstanceCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentSubstanceCategory");
                    }
                    if (existinggenericCategoryID != 0)
                    {
                        selectedParentItemCategory = listGenericsCategory.Where(x => x.GenericsCategoryID == existinggenericCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentItemCategory");
                    }
                }
            }
        }

        public bool CheckCasInValid(string cas) // can be move to common business layer
        {
            bool IsValid = false;
            if (!string.IsNullOrEmpty(cas))
            {
                cas = _objGeneric_Service.RemoveStripCharcterFromCas(cas);
                var regex = new System.Text.RegularExpressions.Regex(@"^\d+$");
                if (regex.IsMatch(cas.Replace("-", "")))
                {
                    if (_objGeneric_Service.CheckCasValid(cas))
                    {
                        IsValid = true;
                    }
                }
                else
                {
                    IsValid = true;
                }
            }
            return IsValid;

        }

        private void FilterChemicalName_Parent(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Parent = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Parent");
                    if (listAllChemicalName_Parent.Count == 0)
                    {
                        ListChemNameParentVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameParentVisibility");
                    }
                });
            }
            else
            {
                listAllChemicalName_Parent = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllChemicalName_Parent");
                ListChemNameParentVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemNameParentVisibility");
            }


        }
        private void FilterChemicalName_Child(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Child = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Child");
                    if (listAllChemicalName_Child.Count == 0)
                    {
                        ListChemNameChildVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameChildVisibility");
                    }
                });
            }
            else
            {
                listAllChemicalName_Child = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllChemicalName_Child");
                ListChemNameChildVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemNameChildVisibility");
            }


        }
        private void CommandSaveAddEditChemicalName_Execute(object obj)
        {
            AddEditChemicalName();
        }


        private void ShowPopUpAddEditName()
        {

            if (CasType == "Parent")
            {
                ParentCas = SelectedPropertyNoofParent.ParentCas;
                ParentName = SelectedPropertyNoofParent.ParentChemicalName;
            }
            else
            {
                ParentCas = SelectedPropertyNoofChild.ChildCas;
                ParentName = SelectedPropertyNoofChild.ChemicalName;
            }
            IsShowPopUp = true;
            contentVisibilityAddEditName = Visibility.Visible;
            ListChemNameParentVisibility = Visibility.Collapsed;
            ListChemNameChildVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("ListChemNameParentVisibility");
            NotifyPropertyChanged("ListChemNameChildVisibility");
            NotifyPropertyChanged("contentVisibilityAddEditName");
            NotifyPropertyChanged("IsShowPopUp");

        }
        private void AddEditChemicalName()
        {
            GenericParentChildCasName_VM objGenericSuggestion = new GenericParentChildCasName_VM();
            if (CasType == "Parent")
            {
                objGenericSuggestion.ParentName = ParentName;
                objGenericSuggestion.ParentNameID = ParentNameID;
                objGenericSuggestion.CasID = (int)SelectedPropertyNoofParent.ParentCasID;
                objGenericSuggestion.CasType = "Parent";
                objGenericSuggestion.SessionID = Engine.CurrentUserSessionID;
            }
            else
            {
                objGenericSuggestion.ParentName = ParentName;
                objGenericSuggestion.ParentNameID = ParentNameID;
                objGenericSuggestion.CasID = (int)SelectedPropertyNoofChild.ChildCasID;
                objGenericSuggestion.CasType = "Child";
                objGenericSuggestion.SessionID = Engine.CurrentUserSessionID;
            }
            _objGeneric_Service.SaveParentChildCasName(objGenericSuggestion);
            _notifier.ShowSuccess("Chemical name updated successfully");
            IsShowPopUp = false;
        }

        private void ShowPopUpDeleteGeneric(string cas)
        {
            var result = System.Windows.MessageBox.Show("Are you sure do you want to remove this node?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                ParentCas = cas;
                CommentGenericSuggestionAction = string.Empty;
                contentRemoveVisibility = Visibility.Visible;
                NotifyPropertyChanged("contentRemoveVisibility");
                IsShowPopUp = true;
                NotifyPropertyChanged("IsShowPopUp");
            }
        }

        private void DeleteGenericSugestion()
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) && !string.IsNullOrWhiteSpace(CommentGenericSuggestionAction))
            {
                GenericSuggestionVM objGenericSuggestionTree_VM = new GenericSuggestionVM();
                objGenericSuggestionTree_VM.Note = CommentGenericSuggestionAction;
                if (CasType == "Parent")
                {
                    objGenericSuggestionTree_VM.ParentCasID = (int)SelectedPropertyNoofParent.ParentCasID;
                    objGenericSuggestionTree_VM.ChildCasID = (int)SelectedPropertyNoofParent.ChildCasID;
                }
                else
                {
                    objGenericSuggestionTree_VM.ParentCasID = (int)SelectedPropertyNoofChild.ParentCasID;
                    objGenericSuggestionTree_VM.ChildCasID = (int)SelectedPropertyNoofChild.ChildCasID;

                }
                objGenericSuggestionTree_VM.UserID = Engine.CurrentUserName;
                _objGeneric_Service.RemoveGenericSuggestion(objGenericSuggestionTree_VM);
                _notifier.ShowSuccess("Generic suggestion remove added successfully");
                IsShowPopUp = false;


            }
            else
            {

                _notifier.ShowWarning("Comment is mandatory for perform this action.");
            }
        }

        #endregion
        private void ValidateSubstanceTypeInFlat(string cas)
        {
            if (_objLibraryFunction_Service.ValidateSubstanceTypeInFlatAndHydrate(cas))
            {
                _notifier.ShowError("Substance Type can not change to Single because it active in Generics or Hydrate");
            }
        }
        private void ValidateAlternateCas()
        {
            var casID = _objLibraryFunction_Service.GetPrimaryCasFromAlternateCas(CASVal);
            if (!string.IsNullOrEmpty(casID))
            {
                _notifier.ShowWarning("This is an alternate CAS for CAS# " + casID);
            }
        }

        public int GetIdentifierTypeID(string name)
        {
            using (var context = new CRAModel())
            {
                return context.Library_IdentifierCategories.Where(x => x.IdentifierTypeName == name).Select(x => x.IdentifierTypeID).FirstOrDefault();

            }
        }
        public void ShowCommentsPopUp(string cas, string identifierValue, int? casId, int? indentifierValueID, int? indentifierTypeID, bool isChemicalView=false)
        {
            if (isChemicalView)
            {
                objCommentDC_PopUpComment = new CommonGenericUIView.ViewModel.CommentsUC_VM(new GenericComment_VM(cas, "", casId, 0, 400, false, "", true, identifierValue, indentifierValueID, false, 0));
            }
            else
            {
                objCommentDC_PopUpComment = new CommonGenericUIView.ViewModel.CommentsUC_VM(new GenericComment_VM(cas, identifierValue, casId, indentifierValueID, 400, false, "", false, "", 0, true, indentifierTypeID));
            }
            NotifyPropertyChanged("objCommentDC_PopUpComment");
            contentCommentsVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentCommentsVisibility");
            IsShowPopUp = true;
        }
        public void ShowConfirmPopUp(string headerDesc, string sectionType, string type)
        {
            if (type == "Disapprove")
            {
                PopupButtonText = "Dis-Approve";
                PopupButtonColor = "Red";
            }
            else
            {
                PopupButtonText = "Approve";
                PopupButtonColor = "Green";
            }
            headerDisapproveItem = headerDesc;
            CommentPopUpType = sectionType;
            contentDisapproveVisibility = Visibility.Visible;
            NotifyPropertyChanged("headerDisapproveItem");
            NotifyPropertyChanged("CommentPopUpType");
            NotifyPropertyChanged("contentDisapproveVisibility");
            NotifyPropertyChanged("PopupButtonText");
            NotifyPropertyChanged("PopupButtonColor");
            IsShowPopUp = true;
        }
    }

    public class LibSI_Properties
    {
        public int NoofListDirectHit { get; set; }
        public int NoofListGenericHit { get; set; }
        public string PrimarySubstanceName { get; set; }
        //public string GroupName { get; set; }
        public string AlternateCAS { get; set; }
        public int NoofChildren { get; set; }
        public string SubstanceCategory { get; set; }
        public string SubstanceType { get; set; }
        public string Synonyms { get; set; }
        public string ECNumbers { get; set; }
        public string MolecularFormula { get; set; }
        public string IUPACNames { get; set; }
        public string ENCSNumber { get; set; }
        public string OtherIdentifiers { get; set; }
        public string Hydrates { get; set; }

    }

    public class ListSIReport_VM
    {
        public string CountNoofListDirectHit { get; set; }
        public string CountNoofListGenericHit { get; set; }
        public string CountPrimarySubstanceName { get; set; }
        //public string CountGroupName { get; set; }
        public string CountAlternateCAS { get; set; }
        public string CountNoofChildren { get; set; }
        public string CountNoofParent { get; set; }

        public string CountSubstanceCategory { get; set; }
        public string CountSubstanceType { get; set; }
        public string CountSynonyms { get; set; }
        public string CountECNumbers { get; set; }
        public string CountECNumbersDisapproved { get; set; }
        public string CountMolecularFormula { get; set; }
        public string CountIUPACNames { get; set; }
        public string CountENCSNumber { get; set; }
        public string CountOtherIdentifiers { get; set; }
        public string CountNoofListSubroot { get; set; }
        public string CountHydrates { get; set; }
    }

}
