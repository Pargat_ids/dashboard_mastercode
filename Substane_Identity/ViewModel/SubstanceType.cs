﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;
using CommonGenericUIView.ViewModel;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibilityExpandMore_SubstanceType { get; set; }
        public ICommand CommandSubstanceType { get; set; }
        public Visibility visibility_SubstanceType { get; set; }
        public ICommand SubstanceTypeUpdate { get; set; }
        public ICommand SubstanceTypeDelete { get; set; }
        public List<LibSubType> listSubstanceType { get; set; }
        public string SubstanceTypeContent { get; set; }
        public CommonDataGrid_ViewModel<ListSubstanceType_VM> comonSubstanceType_VM { get; set; }
        private ObservableCollection<ListSubstanceType_VM> _listChanges_SubstanceType { get; set; } = new ObservableCollection<ListSubstanceType_VM>();
        public ObservableCollection<ListSubstanceType_VM> ListChanges_SubstanceType
        {
            get { return _listChanges_SubstanceType; }
            set
            {
                if (value != _listChanges_SubstanceType)
                {
                    _listChanges_SubstanceType = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListSubstanceType_VM>>>(new PropertyChangedMessage<List<ListSubstanceType_VM>>(_listChanges_SubstanceType.ToList(), _listChanges_SubstanceType.ToList(), "Default List"));
                }
            }
        }
        private LibSubType _SelectedSubStanceType { get; set; }
        public LibSubType SelectedSubStanceType
        {
            get => _SelectedSubStanceType;
            set
            {
                if (Equals(_SelectedSubStanceType, value))
                {
                    return;
                }

                _SelectedSubStanceType = value;
                if (_SelectedSubStanceType.SubstanceTypeID == 1)
                {
                    ValidateSubstanceTypeInFlat(CASVal);
                }
            }
        }
        private void getSubstanceTypeDetail()
        {
            var result = objCommon.DbaseQueryReturnStringSQL("Select top 1 SubstanceTypeName from Map_SubstanceType a, library_cas b, Library_SubstanceType c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.SubStanceTypeID = c.SubStanceTypeID", Win_Administrator_VM.CommonListConn);
            //ListChanges_SubstanceType = new ObservableCollection<ListSubstanceType_VM>(result);
            SubstanceTypeContent = result;
            RefreshComments();
            NotifyPropertyChanged("SubstanceTypeContent");
            SelectedSubStanceType = listSubstanceType.Where(x => x.SubstanceTypeName == result).FirstOrDefault();
            NotifyPropertyChanged("SelectedSubStanceType");
        }

        private void RefreshComments()
        {
            var casId = _objLibraryFunction_Service.GetCasIdByCas(CASVal);
            if (casId != 0)
            {
                objCommentDC_SubstanceIdentity = new CommentsUC_VM(new GenericComment_VM ( CASVal, "", casId, 0, 700, false, "Substance Identity"));
            }
            else
            {
                objCommentDC_SubstanceIdentity = new CommentsUC_VM(new GenericComment_VM( "", "", 0, 0, 700, false, "Substance Identity"));
            }
            NotifyPropertyChanged("objCommentDC_SubstanceIdentity");
        }

        private List<CommonDataGridColumn> GetListSubstanceType()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceTypeID", ColBindingName = "SubstanceTypeID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceType", ColBindingName = "SubstanceType", ColType = "Textbox", ColumnWidth = "500" });
            return listColumn;
        }
        private void SubstanceTypeUpdateExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to update the Substance Type", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                List<Log_Map_SubstanceType> lstlog = new List<Log_Map_SubstanceType>();
                string status = string.Empty;
                using (var context = new CRAModel())
                {
                    var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                    var itemToUpdate = context.Map_SubstanceType.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                    if (itemToUpdate == null)
                    {
                        var main = new Map_SubstanceType
                        {
                            CASID = casId,
                            SubstanceTypeID = SelectedSubStanceType.SubstanceTypeID,
                        };
                        if (itemToUpdate == null)// means no record exist previously
                        {
                            List<Map_SubstanceType> lstmain = new List<Map_SubstanceType>();
                            lstmain.Add(main);
                            _objLibraryFunction_Service.BulkIns<Map_SubstanceType>(lstmain);
                            status = "A";
                            itemToUpdate = context.Map_SubstanceType.AsNoTracking().Where(x => x.CASID == casId).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if (itemToUpdate.SubstanceTypeID != SelectedSubStanceType.SubstanceTypeID)
                        {
                            objCommon.DbaseQueryReturnStringSQL("update Map_SubstanceType set SubstanceTypeID = " + SelectedSubStanceType.SubstanceTypeID + " where MappingSubstanceTypeID =" + itemToUpdate.MappingSubstanceTypeID, Win_Administrator_VM.CommonListConn);
                            status = "C";
                        }
                    }
                    if (status != string.Empty)
                    {
                        var log = new Log_Map_SubstanceType
                        {
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Status = status,
                            CASID = casId,
                            SubstanceTypeID = SelectedSubStanceType.SubstanceTypeID,
                            MappingSubstanceTypeID = itemToUpdate.MappingSubstanceTypeID,
                        };
                        lstlog.Add(log);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceType>(lstlog);
                        _notifier.ShowSuccess("Substance Type Update Successfully");
                        MaterialDesignThemes.Wpf.Flipper.FlipCommand.Execute(obj, null);
                    }
                }
            }
            SearchCommandExecute(obj);
        }
        private void SubstanceTypeDeleteExecute(object obj)
        {

        }
        private void CommandSubstanceTypeExecute(object obj)
        {
            visibility_SubstanceType = GenericVisibleCollapsedFunction(visibility_SubstanceType);
            visibilityExpandMore_SubstanceType = GenericVisibleCollapsedFunction_Expand(visibility_SubstanceType);
            NotifyPropertyChanged("visibility_SubstanceType");
            NotifyPropertyChanged("visibilityExpandMore_SubstanceType");
        }
    }
    public class ListSubstanceType_VM
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int SubstanceTypeID { get; set; }
        public string SubstanceTypeName { get; set; }
    }


    public class LibSubType
    {
        public int SubstanceTypeID { get; set; }
        public string SubstanceTypeName { get; set; }
    }
    public class Comments_VM
    {
        public int? CASID { get; set; }

        public string CAS { get; set; }


        public string Comments { get; set; }

        public string Action { get; set; }

        public string CommentBy { get; set; }

        public DateTime? Timestamp { get; set; }
    }
}
