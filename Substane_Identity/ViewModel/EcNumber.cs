﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;
using System.Drawing;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private List<ListECNumber_VM> _listChanges_ECNumber { get; set; } = new List<ListECNumber_VM>();
        private List<ListECNumberDisapproved_VM> _listChanges_ECNumberDisapproved { get; set; } = new();
        public Visibility IsShowPopUpECNumber { get; set; } = Visibility.Collapsed;
        public ListECNumber_VM SelectedPropertyECNumber { get; set; }
        public ListECNumberDisapproved_VM SelectedPropertyECNumberDisapproved { get; set; }
        public string ECNumber_IdentifierValue { get; set; }
        public ICommand CommandAddECNumber { get; private set; }
        public ICommand CommandAddMoreECNumber { get; private set; }
        public bool IsEditECNumber { get; set; } = true;
        public Visibility visibilityExpandMore_ECNumber { get; set; }
        public Visibility visibility_ECNumber { get; set; }
        public ICommand CommandClosePopUpECNumber { get; private set; }
        public ICommand CommandShowPopUpAddECNumber { get; private set; }
        public CommonDataGrid_ViewModel<ListECNumber_VM> comonECNumber_VM { get; set; }
        public CommonDataGrid_ViewModel<ListECNumberDisapproved_VM> comonECNumberDisaaproved_VM { get; set; }

        private void NotifyMeECNumber(PropertyChangedMessage<ListECNumber_VM> obj)
        {
            SelectedPropertyECNumber = obj.NewValue;            

        }
        private void NotifyMeECNumberDisapproved(PropertyChangedMessage<ListECNumberDisapproved_VM> obj)
        {
            SelectedPropertyECNumberDisapproved = obj.NewValue;
        }


        private void CommandAddMoreECNumberExecute(object obj)
        {
            SaveECNumber();
            SearchCommandExecute(obj);
        }
        public List<ListECNumber_VM> ListChanges_ECNumber
        {
            get { return _listChanges_ECNumber; }
            set
            {
                if (value != _listChanges_ECNumber)
                {
                    _listChanges_ECNumber = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListECNumber_VM>>>(new PropertyChangedMessage<List<ListECNumber_VM>>(null, _listChanges_ECNumber.ToList(), "Default List"));
                }
            }
        }
        public List<ListECNumberDisapproved_VM> ListChanges_ECNumberDisaaproved
        {
            get { return _listChanges_ECNumberDisapproved; }
            set
            {
                if (value != _listChanges_ECNumberDisapproved)
                {
                    _listChanges_ECNumberDisapproved = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListECNumberDisapproved_VM>>>(new PropertyChangedMessage<List<ListECNumberDisapproved_VM>>(null, _listChanges_ECNumberDisapproved, "Default List"));
                }
            }
        }
        private void getECNumberDetail()
        {
            //"Select distinct a.CASECNumberID, a.CASID, b.CAS, a.IdentifierValueID, IdentifierValue from Map_CAS_ECNumber a, library_cas b, Library_Identifiers c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.IdentifierValueID = c.IdentifierValueID
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qi.QSID_ID) as ListFieldName";
            string query = "Select distinct ec.CASECNumberID, lc.CASID, lc.CAS, li.IdentifierValueID, IdentifierValue,lq.QSID,lq.QSID_ID," + subListFieldNameQuery + " from Map_CAS_ECNumber ec inner join  library_cas lc on ec.CASID=lc.CASID " +
            " inner join Library_Identifiers li on li.IdentifierValueID = ec.IdentifierValueID left join QSxxx_Identifiers qi on qi.IdentifierValueID = li.IdentifierValueID left join Library_QSIDs lq on lq.QSID_ID = qi.QSID_ID where lc.CAS = '" + CASVal + "'";
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);
            ListChanges_ECNumber = dtResult.AsEnumerable().GroupBy(x => new { CASECNumberID = x.Field<int>("CASECNumberID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), IdentifierValueID = x.Field<int>("IdentifierValueID"), IdentifierValue = x.Field<string>("IdentifierValue") }).Select(x => new ListECNumber_VM()
            {
                CAS = x.Key.CAS,
                CASECNumberID = x.Key.CASECNumberID,
                CASID = x.Key.CASID,
                IdentifierValue = x.Key.IdentifierValue,
                IdentifierValueID = x.Key.IdentifierValueID,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();

            //ListChanges_ECNumber = new List<ListECNumber_VM>(listECNumber);
        }
        private void GetEcNumberDisapprovedDetail()
        {
            var dtResult = _objLibraryFunction_Service.GetECNumberDetailDisapprove(CASVal);
            ListChanges_ECNumberDisaaproved = dtResult.AsEnumerable().GroupBy(x => new { CASECNumberID = x.Field<int>("CASECNumberID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), IdentifierValueID = x.Field<int>("IdentifierValueID"), IdentifierValue = x.Field<string>("IdentifierValue") }).Select(x => new ListECNumberDisapproved_VM()
            {
                CAS = x.Key.CAS,
                CASECNumberID = x.Key.CASECNumberID,
                CASID = x.Key.CASID,
                IdentifierValue = x.Key.IdentifierValue,
                IdentifierValueID = x.Key.IdentifierValueID,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();
        }
        private List<CommonDataGridColumn> GetListECNumber()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValueID", ColBindingName = "IdentifierValueID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValue", ColBindingName = "IdentifierValue", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_ECNumber", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowApprovedComments" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_ECNumber", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "DeleteECNumber" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetListECNumberDisapproved()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValueID", ColBindingName = "IdentifierValueID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValue", ColBindingName = "IdentifierValue", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_ECNumber", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowDisapproveComments" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_ECNumber", ColType = "Button", ColumnWidth = "200", ContentName = "Revert To Approve", BackgroundColor = "#53BF9D", CommandParam = "RevertToApprove" });
            return listColumn;
        }

        private void CommandButtonExecutedECNumber(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "DeleteECNumber")
            {
                ShowConfirmPopUp("Are you sure do you want to Disapprove this ECNumber? " + Environment.NewLine + "Please specify the comments:", "EcNumberDisapprove", "Disapprove");
            }
            else if (obj.Notification == "ShowApprovedComments")
            {
                var identTypeId = GetIdentifierTypeID("EC Number");
                ShowCommentsPopUp(SelectedPropertyECNumber.CAS, SelectedPropertyECNumber.IdentifierValue, SelectedPropertyECNumber.CASID, SelectedPropertyECNumber.IdentifierValueID, identTypeId);

            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
       
        private void CommandButtonExecutedECNumberDisapproved(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "ShowDisapproveComments")
            {
                var identTypeId = GetIdentifierTypeID("EC Number");
                ShowCommentsPopUp(SelectedPropertyECNumberDisapproved.CAS, SelectedPropertyECNumberDisapproved.IdentifierValue, SelectedPropertyECNumberDisapproved.CASID, SelectedPropertyECNumberDisapproved.IdentifierValueID, identTypeId);

            }
            else if (obj.Notification == "RevertToApprove")
            {
                ShowConfirmPopUp("Are you sure do you want to Revert this ECNumber to approve? " + Environment.NewLine + "Please specify the comments:", "EcNumberApprove", "Approve");                
            }
          
        }

        private void DeleteECNumber()
        {
            List<Log_Map_CAS_EC> lstLog = new List<Log_Map_CAS_EC>();
            List<Log_Map_CAS_EC_Disapproved> lstLogDis = new List<Log_Map_CAS_EC_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_CAS_ECNumber.AsNoTracking().Where(x => x.CASECNumberID == SelectedPropertyECNumber.CASECNumberID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var itemToRemoveDis = context.Map_CAS_ECNumber_Disapproved.AsNoTracking().Where(x => x.CASID == itemtoRemove.CASID && x.IdentifierValueID == itemtoRemove.IdentifierValueID).FirstOrDefault();
                    if (itemToRemoveDis == null)
                    {
                        var lgLog = new Log_Map_CAS_EC
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "D",
                            SessionID = Engine.CurrentUserSessionID,
                            CASECNumberID = itemtoRemove.CASECNumberID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lgLog);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);

                        var lgMainDis = new Map_CAS_ECNumber_Disapproved();
                        lgMainDis.CASID = itemtoRemove.CASID;
                        lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                        context.Map_CAS_ECNumber_Disapproved.Add(lgMainDis);
                        context.SaveChanges();

                        var lgLogDis = new Log_Map_CAS_EC_Disapproved
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            CASECNumberID_Disapproved = lgMainDis.CASECNumberID_Disapproved,
                        };
                        lstLogDis.Add(lgLogDis);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLogDis);
                        EFBatchOperation.For(context, context.Map_CAS_ECNumber).Where(x => x.CASECNumberID == itemtoRemove.CASECNumberID).Delete();

                        var identTypeId = GetIdentifierTypeID("EC Number");
                        SaveCommentsForIdentifier(itemtoRemove.CASID, itemtoRemove.IdentifierValueID, identTypeId, "DisApprove_ECNumbers");
                        _notifier.ShowSuccess("Selected ECNumber deleted successfully");
                        SearchCommandExecute(null);
                    }
                    else
                    {
                        _notifier.ShowError("Selected ECNumber not deleted, it already exist in Disapprove Table");
                    }
                }
            }
        }

        private void RevertECNumberDisapproved()
        {
            List<Log_Map_CAS_EC> lstLog = new List<Log_Map_CAS_EC>();
            List<Log_Map_CAS_EC_Disapproved> lstLogDis = new List<Log_Map_CAS_EC_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_CAS_ECNumber_Disapproved.AsNoTracking().Where(x => x.CASECNumberID_Disapproved == SelectedPropertyECNumberDisapproved.CASECNumberID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var itemToRemoveDis = context.Map_CAS_ECNumber.AsNoTracking().Where(x => x.CASID == itemtoRemove.CASID && x.IdentifierValueID == itemtoRemove.IdentifierValueID).FirstOrDefault();
                    if (itemToRemoveDis == null)
                    {
                        var lgMain = new Map_CAS_ECNumber();
                        lgMain.CASID = itemtoRemove.CASID;
                        lgMain.IdentifierValueID = itemtoRemove.IdentifierValueID;
                        context.Map_CAS_ECNumber.Add(lgMain);
                        context.SaveChanges();

                        var lgLog = new Log_Map_CAS_EC
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            Log_CASECNumberID = lgMain.CASECNumberID,
                        };
                        lstLog.Add(lgLog);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);

                        var lgLogDis = new Log_Map_CAS_EC_Disapproved
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "D",
                            SessionID = Engine.CurrentUserSessionID,
                            CASECNumberID_Disapproved = itemtoRemove.CASECNumberID_Disapproved,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLogDis.Add(lgLogDis);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLogDis);

                        EFBatchOperation.For(context, context.Map_CAS_ECNumber_Disapproved).Where(x => x.CASECNumberID_Disapproved == itemtoRemove.CASECNumberID_Disapproved).Delete();
                        var identTypeId = GetIdentifierTypeID("EC Number");
                        SaveCommentsForIdentifier(itemtoRemove.CASID, itemtoRemove.IdentifierValueID, identTypeId, "DisApprove_ECNumbers");
                        _notifier.ShowSuccess("Selected ECNumber revert to approve successfully");
                        SearchCommandExecute(null);
                    }
                    else
                    {
                        _notifier.ShowError("Selected ECNumber not deleted, it already exist in Disapprove Table");
                    }
                }
            }
        }

        public void SaveCommentsForIdentifier(int casId, int identifierValueId,int identifierTypeId,string action)
        {
            Map_IdentifierSubstance_Comments objCom = new Map_IdentifierSubstance_Comments();
            objCom.CommentBy = Environment.UserName;
            objCom.IdentifierValueID = identifierValueId;
            objCom.CasID = casId;
            objCom.IdentifierTypeID = identifierTypeId;
            objCom.Comments = CommentGenericSuggestionAction;
            objCom.Action = action;
            _objLibraryFunction_Service.InsertMapIdentifierSubstance(objCom);
        }

        public void SaveECNumber()
        {
            if (CASVal == string.Empty || ECNumber_IdentifierValue == string.Empty)
            {
                _notifier.ShowError("CAS & Identfier Value can't be empty");
                return;
            }

            using (var context = new CRAModel())
            {
                List<Log_Map_CAS_EC> lstLog = new List<Log_Map_CAS_EC>();
                List<Library_Identifiers> lstLib = new List<Library_Identifiers>();
                var chkExist = context.Library_Identifiers.AsNoTracking().Where(x => x.IdentifierValue == ECNumber_IdentifierValue).FirstOrDefault();
                if (chkExist == null)
                {
                    var lib = new Library_Identifiers
                    {
                        IdentifierTypeID = 9,
                        IdentifierValue = ECNumber_IdentifierValue,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLib.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Identifiers>(lstLib);
                }

                var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var identId = context.Library_Identifiers.AsNoTracking().Where(x => (x.IdentifierValue == ECNumber_IdentifierValue || x.IdentifierValue == ECNumber_IdentifierValue.Replace("-", "")) && x.IdentifierTypeID == 9).Select(y => y.IdentifierValueID).FirstOrDefault();

                var CheckEcNumberDisapprove = context.Map_CAS_ECNumber_Disapproved.AsNoTracking().Where(x => x.CASID == casId && x.IdentifierValueID == identId).FirstOrDefault();
                if (CheckEcNumberDisapprove == null)
                {

                    var chkAlready = context.Map_CAS_ECNumber.AsNoTracking().Where(x => x.CASID == casId && x.IdentifierValueID == identId).FirstOrDefault();
                    if (chkAlready == null)
                    {
                        var lgMain = new Map_CAS_ECNumber();
                        lgMain.CASID = casId;
                        lgMain.IdentifierValueID = identId;
                        context.Map_CAS_ECNumber.Add(lgMain);
                        context.SaveChanges();

                        var lgLog = new Log_Map_CAS_EC
                        {
                            CASID = casId,
                            IdentifierValueID = identId,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            CASECNumberID = lgMain.CASECNumberID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lgLog);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);
                        _notifier.ShowSuccess("EC-NUmber Saved Successfully.");
                    }
                    else
                    {
                        _notifier.ShowError("EC-NUmber already exists");
                    }
                }
                else
                {
                    _notifier.ShowError("this EC number is currently disapproved, please approve this CAS-EC number first before adding it again.");
                }
            }
        }

        private void CommandECNumberExecute(object obj)
        {
            visibility_ECNumber = GenericVisibleCollapsedFunction(visibility_ECNumber);
            visibilityExpandMore_ECNumber = GenericVisibleCollapsedFunction_Expand(visibility_ECNumber);
            NotifyPropertyChanged("visibility_ECNumber");
            NotifyPropertyChanged("visibilityExpandMore_ECNumber");
            //ExpanderViewECNumber = ExpanderVisibilityFunction(visibility_ECNumber);
            //NotifyPropertyChanged("ExpanderViewECNumber");
            //ExpanderVisibilityFunctionShow(visibility_ECNumber);
        }
        private void CommandShowPopUpAddECNumberExecute(object obj)
        {
            ClearPopUpVariable();
            IsEditECNumber = true;
            NotifyPropertyChanged("IsEditECNumber");

            IsShowPopUp = true;
            IsShowPopUpECNumber = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpECNumber");

        }
        private void CommandClosePopUpECNumberExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpECNumber = Visibility.Collapsed;
        }
        private void CommandAddECNumberExecute(object obj)
        {
            SaveECNumber();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);
        }

    }
    public class ListECNumber_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASECNumberID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
    public class ListECNumberDisapproved_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASECNumberID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
}
