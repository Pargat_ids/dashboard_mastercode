﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
    //    private bool IsChangeGroupName { get; set; }
    //    public ICommand CommandGroupName { get; set; }
    //    public string GroupName_ChemicalName { get; set; }
    //    public Visibility visibilityExpandMore_GroupName { get; set; }
    //    public Visibility visibility_GroupName { get; set; }
    //    public ListGroupName_VM SelectedPropertyGroupName { get; set; }
    //    public Visibility IsShowPopUpGroupName { get; set; } = Visibility.Collapsed;
    //    public bool IsEditGroupName { get; set; } = true;
    //    public ICommand CommandClosePopUpGroupName { get; private set; }
    //    public ICommand CommandShowPopUpAddGroupName { get; private set; }
    //    public ICommand CommandAddGroupName { get; private set; }
    //    public ICommand CommandAddMoreGroupName { get; private set; }
    //    public CommonDataGrid_ViewModel<ListGroupName_VM> comonGroupName_VM { get; set; }
    //    private ObservableCollection<ListGroupName_VM> _listChanges_GroupName { get; set; } = new ObservableCollection<ListGroupName_VM>();
    //    public ObservableCollection<ListGroupName_VM> ListChanges_GroupName
    //    {
    //        get { return _listChanges_GroupName; }
    //        set
    //        {
    //            if (value != _listChanges_GroupName)
    //            {
    //                _listChanges_GroupName = value;
    //                MessengerInstance.Send<PropertyChangedMessage<List<ListGroupName_VM>>>(new PropertyChangedMessage<List<ListGroupName_VM>>(_listChanges_GroupName.ToList(), _listChanges_GroupName.ToList(), "Default List"));
    //            }
    //        }
    //    }
    //    private void getGroupNameDetail()
    //    {
    //        using (var context = new CRAModel())
    //        {
    //            var casid = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
    //            var result = objCommon.DbaseQueryReturnSqlList<ListGroupName_VM>("select distinct a.casid as ParentCasID,b.cas as ParentCas,c.ChemicalNameID, " +
    //            " d.ChemicalName from Map_SubstanceType a " +
    //            " inner join library_cas b " +
    //            " on a.casid = b.casid " +
    //            " inner join Map_PreferredSubstanceName c " +
    //            " on a.casid = c.Casid " +
    //            " inner join Library_ChemicalNames d " +
    //            " on c.ChemicalNameID = d.ChemicalNameID " +
    //            " where a.casid = " + casid + " and a.SubstanceTypeID = 2 ", Win_Administrator_VM.CommonListConn);
    //            ListChanges_GroupName = new ObservableCollection<ListGroupName_VM>(result);
    //        }
    //    }

    //    private List<CommonDataGridColumn> GetListGroupName()
    //    {
    //        List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCasID", ColBindingName = "ParentCasID", ColType = "Textbox", ColumnWidth = "200" });
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCas", ColBindingName = "ParentCas", ColType = "Textbox", ColumnWidth = "200" });
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalNameID", ColBindingName = "ChemicalNameID", ColType = "Textbox", ColumnWidth = "200" });
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChemicalName", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "500" });
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteGroupName" });
    //        listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddGroupName" });
    //        return listColumn;
    //    }
    //    private void CommandGroupNameExecute(object obj)
    //    {
    //        visibility_GroupName = GenericVisibleCollapsedFunction(visibility_GroupName);
    //        visibilityExpandMore_GroupName = GenericVisibleCollapsedFunction_Expand(visibility_GroupName);
    //        NotifyPropertyChanged("visibility_GroupName");
    //        NotifyPropertyChanged("visibilityExpandMore_GroupName");
    //        ExpanderViewGroupName = ExpanderVisibilityFunction(visibility_GroupName);
    //        NotifyPropertyChanged("ExpanderViewGroupName");
    //        ExpanderVisibilityFunctionShow(visibility_GroupName);
    //    }
    //    private void CommandShowPopUpAddGroupNameExecute(object obj)
    //    {
    //        ClearPopUpVariable();
    //        IsEditGroupName = true;
    //        NotifyPropertyChanged("IsEditGroupName");
    //        IsChangeGroupName = false;

    //        IsShowPopUp = true;
    //        IsShowPopUpGroupName = Visibility.Visible;
    //        NotifyPropertyChanged("IsShowPopUpGroupName");
    //    }
    //    private void CommandAddGroupNameExecute(object obj)
    //    {

    //        SaveGroupName();
    //        ClearPopUpVariable();

    //        IsShowPopUp = false;
    //        NotifyPropertyChanged("IsShowPopUp");
    //        SearchCommandExecute(obj);
    //    }
    //    private void RefreshGridGroupName(PropertyChangedMessage<string> flag)
    //    {
    //        SearchCommandExecute(null);
    //        if (IsShowPopUpGroupName == Visibility.Visible)
    //        {
    //            NotifyPropertyChanged("IsShowPopUpGroupName");
    //        }
    //    }
    //    private void CommandAddMoreGroupNameExecute(object obj)
    //    {
    //        SaveGroupName();
    //    }
    //    private void NotifyMeGroupName(PropertyChangedMessage<ListGroupName_VM> obj)
    //    {
    //        SelectedPropertyGroupName = obj.NewValue;
    //    }
    //    public void SaveGroupName()
    //    {
    //        if (CASVal == string.Empty || GroupName_ChemicalName == string.Empty)
    //        {
    //            _notifier.ShowError("CAS & Chemical Name can't be empty");
    //            return;
    //        }
    //        using (var context = new CRAModel())
    //        {
    //            List<IStripChar> listStripChar = GetStripCharacter();
    //            var ParentcasId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
    //            GroupName_ChemicalName = objCommon.RemoveWhitespaceCharacter(GroupName_ChemicalName, listStripChar);
    //            GroupName_ChemicalName = System.Text.RegularExpressions.Regex.Replace(GroupName_ChemicalName, @"\s+", " ").Trim();
    //            var chkExist = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == GroupName_ChemicalName).FirstOrDefault();
    //            if (chkExist == null)
    //            {
    //                string squash = objCommon.FullSquash(GroupName_ChemicalName);
    //                var lib = new Library_ChemicalNames
    //                {
    //                    ChemicalName = GroupName_ChemicalName,
    //                    LanguageID = 1,
    //                    ChemicalNameHash = this.objCommon.GenerateSHA256String(GroupName_ChemicalName),
    //                    ChemicalNameSquashHash = this.objCommon.GenerateSHA256String(squash),
    //                    ChemicalNameSquash = squash,
    //                    ChemicalNameSquash_UnOrder = objCommon.FullSquashWithUnOrdered(GroupName_ChemicalName),
    //                    TimeStamp = objCommon.ESTTime(),
    //                    IsActive = true,
    //                };
    //                context.Library_ChemicalNames.Add(lib);
    //                context.SaveChanges();
    //            }
    //            var chemId = context.Library_ChemicalNames.AsNoTracking().Where(x => x.ChemicalName == GroupName_ChemicalName).Select(y => y.ChemicalNameID).FirstOrDefault();

    //            var itemTo = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ParentcasId).FirstOrDefault();
    //            var Stus = string.Empty;
    //            int ID = 0;
    //            List<Log_Map_PreferredSubstanceName> lstLogDis = new List<Log_Map_PreferredSubstanceName>();
    //            if (itemTo == null)
    //            {
    //                var lgMainDis = new Map_PreferredSubstanceName();
    //                lgMainDis.CASID = ParentcasId;
    //                lgMainDis.ChemicalNameID = chemId;
    //                context.Map_PreferredSubstanceName.Add(lgMainDis);
    //                context.SaveChanges();
    //                Stus = "A";
    //                ID = lgMainDis.PreferredSubstanceNameID;

    //            }
    //            else
    //            {
    //                Stus = "C";
    //                ID = itemTo.PreferredSubstanceNameID;
    //                List<Map_PreferredSubstanceName> lstMain = new List<Map_PreferredSubstanceName>();
    //                var lgMain = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == ParentcasId).FirstOrDefault();
    //                lgMain.ChemicalNameID = chemId;
    //                lstMain.Add(lgMain);
    //                EFBatchOperation.For(context, context.Map_PreferredSubstanceName).UpdateAll(lstMain, x => x.ColumnsToUpdate(c => c.ChemicalNameID));
    //                context.SaveChanges();
    //            }

    //            var lgLogDis = new Log_Map_PreferredSubstanceName
    //            {
    //                CASID = ParentcasId,
    //                ChemicalNameID = chemId,
    //                Status = Stus,
    //                SessionID = Engine.CurrentUserSessionID,
    //                TimeStamp = objCommon.ESTTime(),
    //                PreferredSubstanceNameID = ID,
    //            };
    //            lstLogDis.Add(lgLogDis);
    //            _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(lstLogDis);
    //            _notifier.ShowSuccess("Group Name Updated successfully");
    //        }
    //    }
    //    private void CommandButtonExecutedGroupName(NotificationMessageAction<object> obj)
    //    {
    //        if (obj.Notification == "DeleteGroupName")
    //        {
    //            var result = System.Windows.MessageBox.Show("Are you sure do you want to delete Group Name?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
    //            if (result == MessageBoxResult.Yes)
    //            {
    //                List<Log_Map_PreferredSubstanceName> lstLog = new List<Log_Map_PreferredSubstanceName>();
    //                using (var context = new CRAModel())
    //                {
    //                    var itemtoRemove = context.Map_PreferredSubstanceName.AsNoTracking().Where(x => x.CASID == SelectedPropertyGroupName.ParentCasID).FirstOrDefault();
    //                    if (itemtoRemove != null)
    //                    {
    //                        var lgLog = new Log_Map_PreferredSubstanceName
    //                        {
    //                            CASID = itemtoRemove.CASID,
    //                            ChemicalNameID = itemtoRemove.ChemicalNameID,
    //                            Status = "D",
    //                            SessionID = Engine.CurrentUserSessionID,
    //                            TimeStamp = objCommon.ESTTime(),
    //                        };
    //                        lstLog.Add(lgLog);
    //                        _objLibraryFunction_Service.BulkIns<Log_Map_PreferredSubstanceName>(lstLog);

    //                        EFBatchOperation.For(context, context.Map_PreferredSubstanceName).Where(x => x.CASID == SelectedPropertyGroupName.ParentCasID).Delete();
    //                        context.SaveChanges();
    //                    }
    //                }
    //                _notifier.ShowSuccess("Selected Group Name deleted successfully");
    //                SearchCommandExecute(obj);
    //            }
    //        }
    //        else if (obj.Notification == "AddGroupName")
    //        {
    //            IsChangeGroupName = true;
    //            IsEditGroupName = true;
    //            NotifyPropertyChanged("IsEditGroupName");
    //            IsShowPopUp = true;
    //            IsShowPopUpGroupName = Visibility.Visible;
    //            NotifyPropertyChanged("IsShowPopUpGroupName");
    //            GroupName_ChemicalName = SelectedPropertyGroupName.ChemicalName;
    //            NotifyPropertyChanged("GroupName_ChemicalName");
    //        }
    //    }
    //    private void CommandClosePopUpGroupNameExecute(object obj)
    //    {
    //        string Currenttab = (string)obj;
    //        IsShowPopUp = false;
    //        IsShowPopUpGroupName = Visibility.Collapsed;
    //    }
    //}
    //public class ListGroupName_VM
    //{
    //    public int ParentCasID { get; set; }
    //    public string ParentCas { get; set; }
    //    public int ChemicalNameID { get; set; }
    //    public string ChemicalName { get; set; }
    }
}
