﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility visibility_Website { get; set; }
        public Visibility visibilityExpandMore_Website { get; set; }
        public ICommand CommandWebsite { get; set; }
        public ListWebsite_VM SelectedPropertyWebsite { get; set; }
        public CommonDataGrid_ViewModel<ListWebsite_VM> comonWebsite_VM { get; set; }
        private List<ListWebsite_VM> _ListChanges_Website { get; set; } = new List<ListWebsite_VM>();
        public List<ListWebsite_VM> ListChanges_Website
        {
            get { return _ListChanges_Website; }
            set
            {
                if (value != _ListChanges_Website)
                {
                    _ListChanges_Website = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListWebsite_VM>>>(new PropertyChangedMessage<List<ListWebsite_VM>>(null, _ListChanges_Website, "Default List"));
                }
            }
        }
        private void getWebsitetDetail()
        {
            var casNew = objCommon.SetCasRN(CASVal);
            var listExistingWebsite = _objLibraryFunction_Service.GetLibraryWebsitesExternal();
            var result = listExistingWebsite.Select(x => new ListWebsite_VM() { WebsiteName = x.Name, URL = x.URL.Replace("{0}", casNew) }).ToList();
            //result.Add(new ListWebsite_VM { WebsiteName = "Google for CAS Search", URL = "https://www.google.com/search?q=" + "CAS " + casNew });
            //result.Add(new ListWebsite_VM { WebsiteName = "Google for Substance Name Search", URL = "https://www.google.com/search?q=" + "Substance Name " });
            //result.Add(new ListWebsite_VM { WebsiteName = "Chemical Book", URL = "https://www.chemicalbook.com/Search_EN.aspx?keyword=" + casNew });
            //result.Add(new ListWebsite_VM { WebsiteName = "ChemIDplus", URL = "https://chem.nlm.nih.gov/chemidplus/rn/" + casNew });
            //result.Add(new ListWebsite_VM { WebsiteName = "eChemPortal", URL = "https://www.echemportal.org/echemportal/" });
            //result.Add(new ListWebsite_VM { WebsiteName = "PubChem", URL = "https://pubchem.ncbi.nlm.nih.gov/#query=" + casNew });
            //result.Add(new ListWebsite_VM { WebsiteName = "US EPA Chem View", URL = "https://chemview.epa.gov/chemview" });
            //result.Add(new ListWebsite_VM { WebsiteName = "US EPA substance registry", URL = "https://sor.epa.gov/sor_internet/registry/substreg/LandingPage.do" });

            ListChanges_Website = result;

        }
        private List<CommonDataGridColumn> GetListWebsite()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Website Name", ColBindingName = "WebsiteName", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Show", BackgroundColor = "#ff414d", CommandParam = "ShowWebsite" });
            return listColumn;
        }
        private void NotifyMeWebsite(PropertyChangedMessage<ListWebsite_VM> obj)
        {
            SelectedPropertyWebsite = obj.NewValue;
        }
        private void CommandButtonExecutedWebsite(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "ShowWebsite")
            {

                if (SelectedPropertyWebsite.WebsiteName == "Google for Substance Name Search")
                {
                    var p = new System.Diagnostics.Process();
                    p.StartInfo = new System.Diagnostics.ProcessStartInfo(SelectedPropertyWebsite.URL + PrimarySubstanceContent)
                    {
                        UseShellExecute = true
                    };
                    p.Start();

                }
                else if (SelectedPropertyWebsite.WebsiteName == "Japan J-CHECK (CHEmicals Collaborative Knowledge database)" || SelectedPropertyWebsite.WebsiteName == "Japan ISHL" || SelectedPropertyWebsite.WebsiteName == "ECHA (European Chemicals Agency)" || SelectedPropertyWebsite.WebsiteName == "eChemPortal" || SelectedPropertyWebsite.WebsiteName == "US EPA Substance Registry Services (SRS)" || SelectedPropertyWebsite.WebsiteName == "US EPA ChemView")
                {
                    OpenWebSiteUsingChrome();
                }
                else
                {
                    var p = new System.Diagnostics.Process();
                    p.StartInfo = new System.Diagnostics.ProcessStartInfo(SelectedPropertyWebsite.URL)
                    {
                        UseShellExecute = true
                    };
                    p.Start();

                }
                //}
            }

        }

        private void OpenWebSiteUsingChrome()
        {
            var casNew = objCommon.SetCasRN(CASVal);
            var driverGc = ChromeDriverUrl(SelectedPropertyWebsite.URL);
            System.Threading.Thread.Sleep(2000);
            if (SelectedPropertyWebsite.WebsiteName == "eChemPortal")
            {
                driverGc.FindElement(By.XPath("//div//input[@type='text']")).SendKeys(casNew);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//button[@type='submit']"));
                clickonCSV.Click();
            }
            if (SelectedPropertyWebsite.WebsiteName == "US EPA Substance Registry Services (SRS)")
            {
                driverGc.FindElement(By.XPath("//div//input[@name='substanceName']")).SendKeys(casNew);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//input[@onclick='executeSearch()']"));
                clickonCSV.Click();
            }
            if (SelectedPropertyWebsite.WebsiteName == "US EPA ChemView")
            {
                driverGc.FindElement(By.XPath("//div//input[@id='s2id_autogen1']")).SendKeys(casNew);
                System.Threading.Thread.Sleep(2000);
                driverGc.FindElement(By.XPath("//div//input[@id='s2id_autogen1']")).SendKeys(OpenQA.Selenium.Keys.Tab);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//button[@id='generate-results-btn']"));
                clickonCSV.Click();

            }
            if (SelectedPropertyWebsite.WebsiteName == "ECHA (European Chemicals Agency)")
            {                
                driverGc.FindElement(By.XPath("//div//label[@class='disclaimerIdCheckboxLabel']")).Click();
                System.Threading.Thread.Sleep(2000);
                driverGc.FindElement(By.XPath("//span//input[@id='autocompleteKeywordInput']")).SendKeys(casNew);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//button[@id='_disssimplesearchhomepage_WAR_disssearchportlet_searchButton']"));
                clickonCSV.Click();
            }
            if (SelectedPropertyWebsite.WebsiteName == "Japan ISHL")
            {
                driverGc.FindElement(By.XPath("//div//input[@name='cas']")).SendKeys(casNew);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//input[@name='bs_search_1']"));
                clickonCSV.Click();
            }
            if (SelectedPropertyWebsite.WebsiteName == "Japan J-CHECK (CHEmicals Collaborative Knowledge database)")
            {
                driverGc.FindElement(By.XPath("//div//input[@id='search_action_cas_no']")).SendKeys(casNew);
                var clickonCSV = driverGc.FindElement(By.XPath("//div//input[@name='button1']"));
                clickonCSV.Click();
            }

        }

        public ChromeDriver ChromeDriverUrl(string url)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(System.Windows.Forms.Application.StartupPath + "\\chromedriver_win32");

            //var FileName = Environment.GetEnvironmentVariable("ProgramFiles(x86)") + "\\Google\\Chrome\\Application\\chorme.exe";
            //ChromeDriverService service = ChromeDriverService.CreateDefaultService(FileName);

            service.HideCommandPromptWindow = true;
            ChromeOptions option = new ChromeOptions();
            option.AddArgument("--no-sandbox");
            var driverGc = new ChromeDriver(service, option);
            driverGc.Navigate().GoToUrl(url);
            return driverGc;
        }
        private void CommandWebsiteExecute(object obj)
        {
            visibility_Website = GenericVisibleCollapsedFunction(visibility_Website);
            visibilityExpandMore_Website = GenericVisibleCollapsedFunction_Expand(visibility_Website);
            NotifyPropertyChanged("visibility_Website");
            NotifyPropertyChanged("visibilityExpandMore_Website");
            //ExpanderViewWebsite = ExpanderVisibilityFunction(visibility_Website);
            //NotifyPropertyChanged("ExpanderViewWebsite");
            //ExpanderVisibilityFunctionShow(visibility_Website);
        }
    }
    public class ListWebsite_VM
    {
        public string WebsiteName { get; set; }
        public string URL { get; set; }
    }
}
