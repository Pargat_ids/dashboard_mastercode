﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private List<ListMFormulaDisapproved_VM> _listChangesDisapproved_MFormula { get; set; } = new List<ListMFormulaDisapproved_VM>();
        public ListMFormulaDisapproved_VM SelectedPropertyMFormulaDisapproved { get; set; }
        public CommonDataGrid_ViewModel<ListMFormulaDisapproved_VM> comonMFormulaDisapproved_VM { get; set; }

        private List<ListMFormula_VM> _listChanges_MFormula { get; set; } = new List<ListMFormula_VM>();
        public Visibility IsShowPopUpMFormula { get; set; } = Visibility.Collapsed;

        public ListMFormula_VM SelectedPropertyMFormula { get; set; }
        public string MFormula_IdentifierValue { get; set; }
        public ICommand CommandAddMFormula { get; private set; }
        public ICommand CommandAddMoreMFormula { get; private set; }
        public bool IsEditMFormula { get; set; } = true;
        public Visibility visibilityExpandMore_MFormula { get; set; }
        public ICommand CommandClosePopUpMFormula { get; private set; }
        public ICommand CommandShowPopUpAddMFormula { get; private set; }
        public ICommand CommandMFormula { get; set; }
        public Visibility visibility_MFormula { get; set; }
        public CommonDataGrid_ViewModel<ListMFormula_VM> comonMFormula_VM { get; set; }
        private void NotifyMeMFormula(PropertyChangedMessage<ListMFormula_VM> obj)
        {
            SelectedPropertyMFormula = obj.NewValue;
        }
        private void NotifyMeMFormula(PropertyChangedMessage<ListMFormulaDisapproved_VM> obj)
        {
            SelectedPropertyMFormulaDisapproved = obj.NewValue;
        }
        private void CommandAddMoreMFormulaExecute(object obj)
        {
            SaveMFormula();
            SearchCommandExecute(obj);
        }
        public List<ListMFormula_VM> ListChanges_MFormula
        {
            get { return _listChanges_MFormula; }
            set
            {
                if (value != _listChanges_MFormula)
                {
                    _listChanges_MFormula = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListMFormula_VM>>>(new PropertyChangedMessage<List<ListMFormula_VM>>(_listChanges_MFormula.ToList(), _listChanges_MFormula.ToList(), "Default List"));
                }
            }
        }
        public List<ListMFormulaDisapproved_VM> ListChangesDisapproved_MFormula
        {
            get { return _listChangesDisapproved_MFormula; }
            set
            {
                if (value != _listChangesDisapproved_MFormula)
                {
                    _listChangesDisapproved_MFormula = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ListMFormulaDisapproved_VM>>>(new PropertyChangedMessage<List<ListMFormulaDisapproved_VM>>(null, _listChangesDisapproved_MFormula, "Default List"));
                }
            }
        }
        private void getMFormulaDetail()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qi.QSID_ID) as ListFieldName";
            string query = "Select distinct mf.CASMolecularFormulaID, lc.CASID, lc.CAS, mf.IdentifierValueID, IdentifierValue,lq.QSID,lq.QSID_ID," + subListFieldNameQuery + " from Map_CAS_MolecularFormula mf inner join library_cas lc on " +
                           " mf.CASID = lc.CASID inner join Library_Identifiers li on li.IdentifierValueID = mf.IdentifierValueID  left join QSxxx_Identifiers qi on qi.IdentifierValueID = li.IdentifierValueID left join Library_QSIDs lq on lq.QSID_ID = qi.QSID_ID where lc.cas = '" + CASVal + "'";
            //"Select distinct a.CASMolecularFormulaID, a.CASID, b.CAS, a.IdentifierValueID, IdentifierValue from Map_CAS_MolecularFormula a, library_cas b, Library_Identifiers c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.IdentifierValueID = c.IdentifierValueID"
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);
            ListChanges_MFormula = dtResult.AsEnumerable().GroupBy(x => new { CASMolecularFormulaID = x.Field<int>("CASMolecularFormulaID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), IdentifierValueID = x.Field<int>("IdentifierValueID"), IdentifierValue = x.Field<string>("IdentifierValue") }).Select(x => new ListMFormula_VM()
            {
                CAS = x.Key.CAS,
                CASMolecularFormulaID = x.Key.CASMolecularFormulaID,
                CASID = x.Key.CASID,
                IdentifierValue = x.Key.IdentifierValue,
                IdentifierValueID = x.Key.IdentifierValueID,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();
            //ListChanges_MFormula = new ObservableCollection<ListMFormula_VM>(result);
        }
        private void getMFormulaDetailDisapproved()
        {
            string subListFieldNameQuery = " (Select top 1 lp.Phrase from QSxxx_ListDictionary qld inner join Library_Phrases lp on qld.PhraseID=lp.PhraseID and qld.PhraseCategoryID = (select top 1 PhraseCategoryID from Library_PhraseCategories where PhraseCategory = 'List Field Name') where qld.QSID_ID = qi.QSID_ID) as ListFieldName";
            string query = "Select distinct mf.CASMolecularFormulaID_Disapprove as CASMolecularFormulaID, lc.CASID, lc.CAS, mf.IdentifierValueID, IdentifierValue,lq.QSID,lq.QSID_ID," + subListFieldNameQuery + " from Map_CAS_MolecularFormula_Disapproved mf inner join library_cas lc on " +
                           " mf.CASID = lc.CASID inner join Library_Identifiers li on li.IdentifierValueID = mf.IdentifierValueID  left join QSxxx_Identifiers qi on qi.IdentifierValueID = li.IdentifierValueID left join Library_QSIDs lq on lq.QSID_ID = qi.QSID_ID where lc.cas = '" + CASVal + "'";
            //"Select distinct a.CASMolecularFormulaID, a.CASID, b.CAS, a.IdentifierValueID, IdentifierValue from Map_CAS_MolecularFormula a, library_cas b, Library_Identifiers c where a.CASID = b.CASId and b.cas ='" + CASVal + "' and a.IdentifierValueID = c.IdentifierValueID"
            var dtResult = objCommon.DbaseQueryReturnTableSql(query, Win_Administrator_VM.CommonListConn);
            ListChangesDisapproved_MFormula = dtResult.AsEnumerable().GroupBy(x => new { CASMolecularFormulaID = x.Field<int>("CASMolecularFormulaID"), CASID = x.Field<int>("CASID"), CAS = x.Field<string>("CAS"), IdentifierValueID = x.Field<int>("IdentifierValueID"), IdentifierValue = x.Field<string>("IdentifierValue") }).Select(x => new ListMFormulaDisapproved_VM()
            {
                CAS = x.Key.CAS,
                CASMolecularFormulaID = x.Key.CASMolecularFormulaID,
                CASID = x.Key.CASID,
                IdentifierValue = x.Key.IdentifierValue,
                IdentifierValueID = x.Key.IdentifierValueID,
                Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => new HyperLinkDataValue() { DisplayValue = y.Field<string>("QSID") + " ( " + y.Field<string>("ListFieldName") + " )", NavigateData = y.Field<int?>("QSID_ID") }).Distinct().ToList(),
                QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Distinct().Count(),
                QsidFilter = String.Join(',', x.ToList().Where(y => !string.IsNullOrEmpty(y.Field<string>("QSID"))).Select(y => y.Field<string>("QSID")).ToList())
            }).ToList();
            //ListChanges_MFormula = new ObservableCollection<ListMFormula_VM>(result);
        }
        private List<CommonDataGridColumn> GetListMFormula(string type)
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValueID", ColBindingName = "IdentifierValueID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValue", ColBindingName = "IdentifierValue", ColType = "Textbox", ColumnWidth = "400" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });

            if (type == "Approved")
            {
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowDisapproveComments" }); 
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "DeleteMFormula" });
            }
            else
            {
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowApproveComments" });
                listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_MolecularFormula", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#53BF9D", CommandParam = "ApproveMFormula" });
            }
            return listColumn;
        }



        private void CommandButtonExecutedMFormula(NotificationMessageAction<object> obj)
        {
            ClearPopUpVariable();
            if (obj.Notification == "DeleteMFormula")
            {
                ShowConfirmPopUp("Are you sure do you want to Disapprove this MFormula? " + Environment.NewLine + "Please specify the comments:", "MFormulaDisapprove", "Disapprove");
            }
            else if (obj.Notification == "ShowDisapproveComments")
            {
                var identTypeId = GetIdentifierTypeID("Molecular Formula");
                ShowCommentsPopUp(SelectedPropertyMFormula.CAS, SelectedPropertyMFormula.IdentifierValue, SelectedPropertyMFormula.CASID, SelectedPropertyMFormula.IdentifierValueID, identTypeId);
            }
            else if (obj.Notification == "ApproveMFormula")
            {
                ShowConfirmPopUp("Are you sure do you want to approve this MFormula? " + Environment.NewLine + "Please specify the comments:", "MFormulaApprove", "approve");
            }
            else if (obj.Notification == "ShowApproveComments")
            {
                var identTypeId = GetIdentifierTypeID("Molecular Formula");
                ShowCommentsPopUp(SelectedPropertyMFormulaDisapproved.CAS, SelectedPropertyMFormulaDisapproved.IdentifierValue, SelectedPropertyMFormulaDisapproved.CASID, SelectedPropertyMFormulaDisapproved.IdentifierValueID, identTypeId);
            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }

        }
        private void DeleteMFormula()
        {
            List<Log_Map_CAS_MolecularFormula> lstLog = new List<Log_Map_CAS_MolecularFormula>();
            List<Log_Map_CAS_MolecularFormula_Disapproved> lstLogDis = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_CAS_MolecularFormula.AsNoTracking().Where(x => x.CASMolecularFormulaID == SelectedPropertyMFormula.CASMolecularFormulaID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var itemToRemoveDis = context.Map_CAS_MolecularFormula_Disapproved.AsNoTracking().Where(x => x.CASID == itemtoRemove.CASID && x.IdentifierValueID == itemtoRemove.IdentifierValueID).FirstOrDefault();
                    if (itemToRemoveDis == null)
                    {
                        var lgLog = new Log_Map_CAS_MolecularFormula
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "D",
                            SessionID = Engine.CurrentUserSessionID,
                            CASMolecularFormulaID = itemtoRemove.CASMolecularFormulaID,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstLog.Add(lgLog);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);

                        var lgMainDis = new Map_CAS_MolecularFormula_Disapproved();
                        lgMainDis.CASID = itemtoRemove.CASID;
                        lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                        context.Map_CAS_MolecularFormula_Disapproved.Add(lgMainDis);
                        context.SaveChanges();

                        var lgLogDis = new Log_Map_CAS_MolecularFormula_Disapproved
                        {
                            CASID = itemtoRemove.CASID,
                            IdentifierValueID = itemtoRemove.IdentifierValueID,
                            Status = "A",
                            SessionID = Engine.CurrentUserSessionID,
                            TimeStamp = objCommon.ESTTime(),
                            CASMolecularFormulaID_Disapprove = lgMainDis.CASMolecularFormulaID_Disapprove,
                        };
                        lstLogDis.Add(lgLogDis);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLogDis);

                        EFBatchOperation.For(context, context.Map_CAS_MolecularFormula).Where(x => x.CASMolecularFormulaID == itemtoRemove.CASMolecularFormulaID).Delete();
                        var identTypeId = GetIdentifierTypeID("Molecular Formula");
                        SaveCommentsForIdentifier(itemtoRemove.CASID, itemtoRemove.IdentifierValueID, identTypeId, "MolecularFormula");

                        _notifier.ShowSuccess("Selected Molecular Formula deleted successfully");
                        SearchCommandExecute(null);
                    }
                    else
                    {
                        if (!context.Log_Map_CAS_MolecularFormula.Any(x => x.CASMolecularFormulaID != itemtoRemove.CASMolecularFormulaID && x.Status == "D"))
                        {
                            var lgLog = new Log_Map_CAS_MolecularFormula
                            {
                                CASID = itemtoRemove.CASID,
                                IdentifierValueID = itemtoRemove.IdentifierValueID,
                                Status = "D",
                                SessionID = Engine.CurrentUserSessionID,
                                CASMolecularFormulaID = itemtoRemove.CASMolecularFormulaID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lgLog);
                            _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);
                        }

                        if (!context.Map_CAS_MolecularFormula_Disapproved.Any(x => x.CASID == itemtoRemove.CASID && x.IdentifierValueID == itemtoRemove.IdentifierValueID))
                        {
                            var lgMainDis = new Map_CAS_MolecularFormula_Disapproved();
                            lgMainDis.CASID = itemtoRemove.CASID;
                            lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                            context.Map_CAS_MolecularFormula_Disapproved.Add(lgMainDis);
                            context.SaveChanges();

                            var lgLogDis = new Log_Map_CAS_MolecularFormula_Disapproved
                            {
                                CASID = itemtoRemove.CASID,
                                IdentifierValueID = itemtoRemove.IdentifierValueID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                TimeStamp = objCommon.ESTTime(),
                                CASMolecularFormulaID_Disapprove = lgMainDis.CASMolecularFormulaID_Disapprove,
                            };
                            lstLogDis.Add(lgLogDis);
                            _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLogDis);
                        }
                        EFBatchOperation.For(context, context.Map_CAS_MolecularFormula).Where(x => x.CASMolecularFormulaID == itemtoRemove.CASMolecularFormulaID).Delete();
                        var identTypeId = GetIdentifierTypeID("Molecular Formula");
                        SaveCommentsForIdentifier(itemtoRemove.CASID, itemtoRemove.IdentifierValueID, identTypeId, "MolecularFormula");
                        _notifier.ShowSuccess("Selected Molecular Formula deleted successfully");
                        SearchCommandExecute(null);
                        // _notifier.ShowError("Selected Molecular Formula not deleted, it already exist in Disapprove Table");
                    }
                }
            }
        }
        private void RevertMFormulaDisapproved()
        {
            List<Log_Map_CAS_MolecularFormula> lstLog = new List<Log_Map_CAS_MolecularFormula>();
            List<Log_Map_CAS_MolecularFormula_Disapproved> lstLogDis = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
            using (var context = new CRAModel())
            {

                var itemToRemoveDis = context.Map_CAS_MolecularFormula_Disapproved.AsNoTracking().Where(x => x.CASID == SelectedPropertyMFormulaDisapproved.CASID && x.IdentifierValueID == SelectedPropertyMFormulaDisapproved.IdentifierValueID).FirstOrDefault();
                if (itemToRemoveDis != null)
                {
                    var lgLogDis = new Log_Map_CAS_MolecularFormula_Disapproved
                    {
                        CASID = itemToRemoveDis.CASID,
                        IdentifierValueID = itemToRemoveDis.IdentifierValueID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID_Disapprove = itemToRemoveDis.CASMolecularFormulaID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLogDis);

                    var lgMain = new Map_CAS_MolecularFormula();
                    lgMain.CASID = itemToRemoveDis.CASID;
                    lgMain.IdentifierValueID = itemToRemoveDis.IdentifierValueID;
                    context.Map_CAS_MolecularFormula.Add(lgMain);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_MolecularFormula
                    {
                        CASID = itemToRemoveDis.CASID,
                        IdentifierValueID = itemToRemoveDis.IdentifierValueID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        CASMolecularFormulaID = lgMain.CASMolecularFormulaID,
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);

                    EFBatchOperation.For(context, context.Map_CAS_MolecularFormula_Disapproved).Where(x => x.CASMolecularFormulaID_Disapprove == itemToRemoveDis.CASMolecularFormulaID_Disapprove).Delete();
                    var identTypeId = GetIdentifierTypeID("Molecular Formula");
                    SaveCommentsForIdentifier(itemToRemoveDis.CASID, itemToRemoveDis.IdentifierValueID, identTypeId, "MolecularFormula");
                    _notifier.ShowSuccess("Selected Molecular Formula revert to approved successfully");
                    SearchCommandExecute(null);
                }
            }
        }
        public void SaveMFormula()
        {
            if (CASVal == string.Empty || MFormula_IdentifierValue == string.Empty)
            {
                _notifier.ShowError("CAS & Identfier Value can't be empty");
                return;
            }

            using (var context = new CRAModel())
            {
                List<Log_Map_CAS_MolecularFormula> lstLog = new List<Log_Map_CAS_MolecularFormula>();
                List<Library_Identifiers> lstLib = new List<Library_Identifiers>();
                var chkExist = context.Library_Identifiers.AsNoTracking().Where(x => x.IdentifierValue == MFormula_IdentifierValue).FirstOrDefault();
                if (chkExist == null)
                {
                    var lib = new Library_Identifiers
                    {
                        IdentifierTypeID = 13,
                        IdentifierValue = MFormula_IdentifierValue,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLib.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Identifiers>(lstLib);
                }

                var casId = context.Library_CAS.AsNoTracking().Where(x => x.CAS == CASVal).Select(y => y.CASID).FirstOrDefault();
                var identId = context.Library_Identifiers.AsNoTracking().Where(x => (x.IdentifierValue == MFormula_IdentifierValue || x.IdentifierValue == MFormula_IdentifierValue.Replace("-", "")) && x.IdentifierTypeID == 13).Select(y => y.IdentifierValueID).FirstOrDefault();
                var chkAlready = context.Map_CAS_MolecularFormula.AsNoTracking().Where(x => x.CASID == casId && x.IdentifierValueID == identId).FirstOrDefault();
                if (chkAlready == null)
                {
                    var lgMain = new Map_CAS_MolecularFormula();
                    lgMain.CASID = casId;
                    lgMain.IdentifierValueID = identId;
                    context.Map_CAS_MolecularFormula.Add(lgMain);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_MolecularFormula
                    {
                        CASID = casId,
                        IdentifierValueID = identId,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID = lgMain.CASMolecularFormulaID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);
                    _notifier.ShowSuccess("Molecular Formula Saved Successfully.");
                }
                else
                {
                    _notifier.ShowError("Molecular Formula already exists");
                }
            }
        }
        private void CommandMFormulaExecute(object obj)
        {
            visibility_MFormula = GenericVisibleCollapsedFunction(visibility_MFormula);
            visibilityExpandMore_MFormula = GenericVisibleCollapsedFunction_Expand(visibility_MFormula);
            NotifyPropertyChanged("visibility_MFormula");
            NotifyPropertyChanged("visibilityExpandMore_MFormula");
            //ExpanderViewMFormula = ExpanderVisibilityFunction(visibility_MFormula);
            //NotifyPropertyChanged("ExpanderViewMFormula");
            //ExpanderVisibilityFunctionShow(visibility_MFormula);
        }
        private void CommandShowPopUpAddMFormulaExecute(object obj)
        {
            ClearPopUpVariable();

            IsEditMFormula = true;
            NotifyPropertyChanged("IsEditMFormula");

            IsShowPopUp = true;
            IsShowPopUpMFormula = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUpMFormula");
        }
        private void CommandClosePopUpMFormulaExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
            IsShowPopUpMFormula = Visibility.Collapsed;
        }

        private void CommandAddMFormulaExecute(object obj)
        {
            SaveMFormula();
            ClearPopUpVariable();

            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
            SearchCommandExecute(obj);
        }

    }
    public class ListMFormula_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASMolecularFormulaID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
    public class ListMFormulaDisapproved_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int CASMolecularFormulaID { get; set; }
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int IdentifierValueID { get; set; }
        public string IdentifierValue { get; set; }
        public int? QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
}
