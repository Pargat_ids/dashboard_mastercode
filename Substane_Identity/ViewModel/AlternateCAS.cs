﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Substane_Identity.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;
using ToastNotifications.Messages;
using EntityFramework.Utilities;

namespace Substane_Identity.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {

        public ICommand CommandAlternateCAS { get; set; }
        public Visibility visibility_AlternateCAS { get; set; }
        public Visibility visibilityExpandMore_AlternateCAS { get; set; }
        public GenericManagementTool.ViewModels.AlternateCas_VM comonAlternateCAS_VM { get; set; }        
        private void getAlternateCASDetail()
        {
            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                if (!string.IsNullOrEmpty(CASVal) && !string.IsNullOrWhiteSpace(CASVal))
                {
                    comonAlternateCAS_VM = new GenericManagementTool.ViewModels.AlternateCas_VM(CASVal);
                    NotifyPropertyChanged("comonAlternateCAS_VM");
                }
            });
        }
       
        private void CommandAlternateCASExecute(object obj)
        {
            visibility_AlternateCAS = GenericVisibleCollapsedFunction(visibility_AlternateCAS);
            visibilityExpandMore_AlternateCAS = GenericVisibleCollapsedFunction_Expand(visibility_AlternateCAS);
            NotifyPropertyChanged("visibility_AlternateCAS");
            NotifyPropertyChanged("visibilityExpandMore_AlternateCAS");
            //ExpanderViewAlternateCAS = ExpanderVisibilityFunction(visibility_AlternateCAS);
            //NotifyPropertyChanged("ExpanderViewAlternateCAS");
            //ExpanderVisibilityFunctionShow(visibility_AlternateCAS);
        }
    }
    
}
