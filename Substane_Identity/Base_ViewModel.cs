﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using ToastNotifications.Messages;
using System.Windows.Input;
using CRA_DataAccess;
using GalaSoft.MvvmLight.Messaging;
using CRA_DataAccess;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using System.Linq.Expressions;

namespace Substane_Identity
{
    public class Base_ViewModel : INotifyPropertyChanged
    {
        public Notifier _notifier { get; set; }
        public IGenericBL _objGeneric_Service { get; set; }
        public IAccess_Version_BL _objAccessVersion_Service { get; private set; }
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public ICommand CommandGenerateExcelCasResult { get; set; }
        public Base_ViewModel()
        {
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (_objGeneric_Service == null)
            {
                _objGeneric_Service = new GenericBL(_objLibraryFunction_Service);
            }
            
            if (_objAccessVersion_Service == null)
            {
                _objAccessVersion_Service = new Access_Version_BL(_objLibraryFunction_Service);
            }
            _notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new WindowPositionProvider(
                    parentWindow: Application.Current.MainWindow,
                    corner: Corner.BottomRight,
                    offsetX: 10,
                    offsetY: 10);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(10),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(5));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });
        }
        private IMessenger _messengerInstance { get; set; }
        protected IMessenger MessengerInstance
        {
            get
            {
                return this._messengerInstance ?? Messenger.Default;
            }
            set
            {
                this._messengerInstance = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {


                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(info));
                    }
                    catch { }
                });

            }
        }
        
    }
}
