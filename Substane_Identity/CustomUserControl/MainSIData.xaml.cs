﻿using CRA_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;

namespace Substane_Identity.CustomUserControl
{
    /// <summary>
    /// Interaction logic for MainSIData.xaml
    /// </summary>
    public partial class MainSIData : UserControl
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public MainSIData()
        {
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in TabSI.Items)
            {
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scroll = (ScrollViewer)sender;
            if (e.Delta < 0)
            {
                if (scroll.VerticalOffset - e.Delta <= scroll.ExtentHeight - scroll.ViewportHeight)
                {
                    scroll.ScrollToVerticalOffset(scroll.VerticalOffset - e.Delta);
                }
                else
                {
                    scroll.ScrollToBottom();
                }
            }
            else
            {
                if (scroll.VerticalOffset + e.Delta > 0)
                {
                    scroll.ScrollToVerticalOffset(scroll.VerticalOffset - e.Delta);
                }
                else
                {
                    scroll.ScrollToTop();
                }
            }
            e.Handled = true;
        }
    }
}
