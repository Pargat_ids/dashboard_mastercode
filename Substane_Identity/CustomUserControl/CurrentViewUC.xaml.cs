﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Substane_Identity.CustomUserControl
{
    /// <summary>
    /// Interaction logic for CurrentViewUC.xaml
    /// </summary>
    public partial class CurrentViewUC : UserControl
    {
        public CurrentViewUC()
        {
            InitializeComponent();
        }
        private void dataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                var dg = sender as DataGrid;
                if (dg != null && e.AddedCells != null && e.AddedCells.Count > 0)
                {
                    var cell = e.AddedCells[0];
                    if (!cell.IsValid)
                        return;
                    var generator = dg.ItemContainerGenerator;
                    //int columnIndex = cell.Column.DisplayIndex; //OK as long user can't reorder
                    int rowIndex = generator.IndexFromContainer(generator.ContainerFromItem(cell.Item));
                    lblCurrentRow.Content = "Current Row - " + (Convert.ToInt32(rowIndex) + 1);
                }
            }
            catch { }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
