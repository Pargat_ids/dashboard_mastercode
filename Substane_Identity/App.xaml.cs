﻿using GalaSoft.MvvmLight.Messaging;
using Substane_Identity.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Substane_Identity
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        string[] args;
        protected override void OnStartup(StartupEventArgs e)
        {
            args = Environment.GetCommandLineArgs();
            args = e.Args;

            if (args.Count() > 0)
            {
                if (args[0] != string.Empty)
                {
                    Win_Administrator_VM.CAS = args[0];
                    Win_Administrator_VM.CASFromUpstream = args[0];
                }
            }
        }


        private IMessenger _messengerInstance { get; set; }
        protected IMessenger MessengerInstance
        {
            get
            {
                return this._messengerInstance ?? Messenger.Default;
            }
            set
            {
                this._messengerInstance = value;
            }
        }
    }

}
