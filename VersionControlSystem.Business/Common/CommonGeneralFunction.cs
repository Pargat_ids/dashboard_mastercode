﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.DataAccess.Repository;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Business.Common
{
    public static class CommonGeneralFunction
    {
        public static List_Dictionary_VM GetMeta_List_Dictionary_FromAccessFile(string query, string AccessDBPath)
        {
            List_Dictionary_VM objDictionaryMetaData = new List_Dictionary_VM();
            try
            {
                objDictionaryMetaData = SqlClassRepository.DbaseQueryReturnObject<List_Dictionary_VM>(query, AccessDBPath);
            }
            catch (Exception ex)
            {
                objDictionaryMetaData = null;
            }
            return objDictionaryMetaData;
        }
        public static void CreateDirectoryPath(string destinationPath)
        {
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }
        }
        public static bool CheckFileExist(string filePath)
        {
            return File.Exists(filePath);

        }
        public static void GenerateFileToDestination(string sourcePath, string destinationPath)
        {
            System.IO.File.Copy(sourcePath, destinationPath, true);
        }
        public static bool DeleteSourceFile(string sourcePath)
        {
            bool isDelete = false;
            if (!IsFileLocked(sourcePath))
            {
                System.IO.File.Delete(sourcePath);
                isDelete = true;
            }
            return isDelete;
        }
        public static bool IsFileLocked(string filename)
        {
            bool Locked = false;
            try
            {
                FileStream fs =
                    File.Open(filename, FileMode.OpenOrCreate,
                    FileAccess.ReadWrite, FileShare.None);
                fs.Close();
            }
            catch (IOException ex)
            {
                Locked = true;
            }
            return Locked;
        }

        public static string GetFileNameWithCurrentVersion(string originalFileName, string currentVersion)
        {
            string resultFileName = string.Empty;
            if (originalFileName.IndexOf(".") > 0)
            {
                string[] splitedFileName = originalFileName.Split('.');
                resultFileName = splitedFileName[0] + "_" + currentVersion + "." + splitedFileName[1];
            }
            return resultFileName;
        }

        public static Log_VersionControl_History BindModelVersionData(int? qsid_Id, string filePath, string origialFileName, string currentVersion, DateTime? Check_Out_TimeStamp, DateTime? Check_In_TimeStamp, int totalRecords, int totalUniqueRecords, string Note, DateTime? ProductDateTimestamp, int? chgTypeId = null)
        {
            Log_VersionControl_History oVersionControlData = new Log_VersionControl_History();
            oVersionControlData.QSID_ID = qsid_Id;
            oVersionControlData.FilePath = filePath;
            oVersionControlData.FileName = origialFileName;
            oVersionControlData.CheckedOutByUser = Environment.UserName;
            oVersionControlData.Version = currentVersion;
            oVersionControlData.Check_Out_TimeStamp = Check_Out_TimeStamp;
            oVersionControlData.Check_In_TimeStamp = Check_In_TimeStamp;
            // oVersionControlData.IsLocked = true;
            oVersionControlData.Currently_Checked_Out = false;
            oVersionControlData.DateFirstAdded = ESTTime();
            oVersionControlData.TotalCount = totalRecords;
            oVersionControlData.UniqueSubstance = totalUniqueRecords;
            oVersionControlData.ProductTimeStamp = ProductDateTimestamp;
            oVersionControlData.AddNote = Note;
            oVersionControlData.SessionID = Engine.CurrentUserSessionID;
            oVersionControlData.ListVersionChangeTypeID = chgTypeId;
            return oVersionControlData;
        }

        public static DateTime TryParseDateTime(string date)
        {
            DateTime resDate;
            DateTime.TryParse(date, out resDate);
            return resDate;
        }
        public static DateTime ESTTime()
        {
            var timeToConvert = DateTime.UtcNow;
            var est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var targetTime = TimeZoneInfo.ConvertTime(timeToConvert, est);
            return targetTime;
        }

        public static List<CasSearchSubstanceTab_VM> MapCasSearchSunbstanceBusinessToView(List<CasSearchSubstance_VM> lists)
        {
            List<CasSearchSubstanceTab_VM> listfinalResult = new List<CasSearchSubstanceTab_VM>();
            CasSearchSubstanceTab_VM objCasSearchSub;
            foreach (var item in lists)
            {
                objCasSearchSub = new CasSearchSubstanceTab_VM();
                objCasSearchSub.Qsid = ConvertInfoToHyoerlinkVal(item.Qsid, item.Qsid_Ids);
                objCasSearchSub.QsidFilter = item.Qsid;
                objCasSearchSub.Cas = item.Cas;
                objCasSearchSub.Type = item.Type;
                objCasSearchSub.IdentifierValue_SubstanceName = item.IdentifierValue_SubstanceName;
                objCasSearchSub.SubstanceName_Language = item.SubstanceName_Language;
                objCasSearchSub.ListCount = item.ListCount;
                listfinalResult.Add(objCasSearchSub);
            }
            return listfinalResult;
        }
        public static List<DateSearchTab_VM> MapDateSearchBusinessToView(List<DateSearch_VM> lists)
        {
            List<DateSearchTab_VM> listfinalResult = new List<DateSearchTab_VM>();
            DateSearchTab_VM objCasSearchSub;
            foreach (var item in lists)
            {
                objCasSearchSub = new DateSearchTab_VM();
                objCasSearchSub.Qsid = ConvertInfoToHyoerlinkVal(item.Qsid, item.Qsid_Ids);
                objCasSearchSub.QsidFilter = item.Qsid;
                objCasSearchSub.FieldName = item.FieldName;
                objCasSearchSub.Phrase = item.Phrase;
                objCasSearchSub.Harmonized_Date = DateTime.ParseExact(item.Phrase, item.DateFormat, CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
                objCasSearchSub.No_of_ListCount = item.No_of_ListCount;
                listfinalResult.Add(objCasSearchSub);
            }
            return listfinalResult;
        }

        public static List<HyperLinkDataValue> ConvertInfoToHyoerlinkVal(string qsids, string qsidIds)
        {
            string[] qsidArry = qsids.Split(',');
            string[] qsid_Id_Arry = qsidIds.Split(',');
            List<HyperLinkDataValue> listHyperLinkValues = new List<HyperLinkDataValue>();
            for (int index = 0; index < qsidArry.Length; index++)
            {
                listHyperLinkValues.Add(new HyperLinkDataValue() { DisplayValue = qsidArry[index].Trim(), NavigateData = qsid_Id_Arry[index].Trim() });
            }
            return listHyperLinkValues;
        }

        public static List<SubstanceSearchTab_VM> MapSubstanceSearchBusinessToView(List<SubstanceSearch_VM> lists)
        {
            List<SubstanceSearchTab_VM> listfinalResult = new List<SubstanceSearchTab_VM>();
            SubstanceSearchTab_VM objCasSearchSub;
            foreach (var item in lists)
            {
                objCasSearchSub = new SubstanceSearchTab_VM();
                objCasSearchSub.Qsid = ConvertInfoToHyoerlinkVal(item.Qsid, item.Qsid_Ids);
                objCasSearchSub.QsidFilter = item.Qsid;
                objCasSearchSub.Cas = item.Cas;
                objCasSearchSub.Type = item.Type;
                objCasSearchSub.IdentifierValue_SubstanceName = item.IdentifierValue_SubstanceName;
                objCasSearchSub.SubstanceName_Language = item.SubstanceName_Language;
                objCasSearchSub.SubstanceLength = item.SubstanceLength;
                objCasSearchSub.ListCount = item.ListCount;
                objCasSearchSub.SubstanceCategory = item.MapSubstanceCategory;
                objCasSearchSub.SubstanceType = item.MapSubstanceType;
                objCasSearchSub.ChildCount = item.ChildCount;
                listfinalResult.Add(objCasSearchSub);
            }
            return listfinalResult;
        }

        public static List<SubstanceSearchIdentifier_VM> MapIdentifierSearchBusinessToView(List<IdentifierSearch_VM> lists)
        {
            List<SubstanceSearchIdentifier_VM> listfinalResult = new List<SubstanceSearchIdentifier_VM>();
            SubstanceSearchIdentifier_VM objCasSearchSub;
            foreach (var item in lists)
            {
                objCasSearchSub = new SubstanceSearchIdentifier_VM();
                objCasSearchSub.Qsid = ConvertInfoToHyoerlinkVal(item.Qsid, item.Qsid_Ids);
                objCasSearchSub.QsidFilter = item.Qsid;
                objCasSearchSub.Cas = item.Cas;
                objCasSearchSub.IdentifierType = item.Type;
                objCasSearchSub.IdentifierValue = item.IdentifierValue;
                objCasSearchSub.ListCount = item.ListCount;
                objCasSearchSub.SubstanceName = item.SubstanceName;
                listfinalResult.Add(objCasSearchSub);
            }
            return listfinalResult;
        }

        public static List<SubstanceSearchPhrase_VM> MapPhraseSearchBusinessToView(List<PhraseSearch_VM> lists)
        {
            List<SubstanceSearchPhrase_VM> listfinalResult = new List<SubstanceSearchPhrase_VM>();
            SubstanceSearchPhrase_VM objCasSearchSub;
            foreach (var item in lists)
            {
                objCasSearchSub = new SubstanceSearchPhrase_VM();
                objCasSearchSub.Qsid = ConvertInfoToHyoerlinkVal(item.Qsid, item.Qsid_Ids);
                objCasSearchSub.QsidFilter = item.Qsid;
                objCasSearchSub.Cas = item.Cas;
                objCasSearchSub.Phrase = item.Phrase;
                objCasSearchSub.PhraseCategory = item.PhraseCategory;
                objCasSearchSub.ListCount = item.ListCount;
                objCasSearchSub.SubstanceName = item.SubstanceName;
                listfinalResult.Add(objCasSearchSub);
            }
            return listfinalResult;
        }

        public static List<MapGenericsCalculated_ViewModel> MapSubstanceSearchBusinessToView(List<MapGenericsCalculated_VM> listMapGenericCal)
        {            
            var listfinalResult = (from c in listMapGenericCal
                         group c by new { c.Cas, c.CasID, c.ParentName } into g
                         select new MapGenericsCalculated_ViewModel()
                         {
                             Cas = g.Key.Cas,
                             ParentName = g.Key.ParentName,
                             CasID = g.Key.CasID,
                             Qsid = g.Select(x => new HyperLinkDataValue() { DisplayValue = x.Qsid, NavigateData = x.Qsid_ID }).ToList(),
                             QsidFilter = string.Join(",", g.Select(x => x.Qsid).ToArray())
                         }
             ).ToList();           
            return listfinalResult;
        }
    }
}
