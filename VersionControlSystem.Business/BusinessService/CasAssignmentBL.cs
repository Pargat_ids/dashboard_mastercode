﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Business.IBusinessService;


namespace VersionControlSystem.Business.BusinessService
{
    public class CasAssignmentBL : ICasAssignmentBL
    {
        CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service { get; set; }
        public CasAssignmentBL(CRA_DataAccess.ILibraryFunction objLibraryFunction_Service)
        {
            _objLibraryFunction_Service = objLibraryFunction_Service;
        }
        public IEnumerable<string> GetQsidListByCasID(int casId, bool isFoodData, bool isNonFoodData, string chemicalName)
        {
            return _objLibraryFunction_Service.GetQsidListByCasID(casId, isFoodData, isNonFoodData, chemicalName);
        }
        public bool UpdateAccessData(List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> listUpdateCas, string SelectedMdbPath, string userName, string qsid, string tableName)
        {
            return _objLibraryFunction_Service.updateBulkAccessData(listUpdateCas, SelectedMdbPath, userName, qsid, Model.ApplicationEngine.Engine.ToolVersion, tableName);
        }
        public Dictionary<string, List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM>> GetChemicalNameNoCasFromAccess(string path, string qsid, bool IsFoodData, bool IsNonFoodData, string tableName)
        {
            return _objLibraryFunction_Service.GetChemicalNameNoCasFromAccess(path, qsid, IsFoodData, IsNonFoodData, tableName);
        }
        public Dictionary<string, int> GetNextSeqCasByModule(string moduleCode, int countNoCasSeq)
        {
            return _objLibraryFunction_Service.GetNextSeqCasByModule(moduleCode, countNoCasSeq);

        }
        public List<CasAssignmentHistory> GetCasAssignmentHistory(string selectedMdbQSID)
        {
            return _objLibraryFunction_Service.GetCasAssignmentHistory(selectedMdbQSID);
        }

        public List<string> GetTableNameFromAccess(string selectedMdbPath)
        {
            return _objLibraryFunction_Service.GetTableNameFromAccess(selectedMdbPath);
        }
        public bool ValidateDefaultAccessTable(string tableName, string selectedMdbPath)
        {
            return _objLibraryFunction_Service.ValidateAccessFilePath(tableName, selectedMdbPath);
        }
        public List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetMICNextSeedList(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedMIC)
        {
            var list_MIC_Seed = _objLibraryFunction_Service.GetMICNextSeedList();
            var ConvertedMIC_Seed = list_MIC_Seed.Select(x => new VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed { Code = x.Code, CatCode = x.CatCode, Prefix = x.Prefix, CatName = x.CatName, NextSeed = x.NextSeed }).ToList();
            if (listSelectedMIC.Count > 0)
            {
                foreach (var item in listSelectedMIC)
                {
                    var matchedRecord = ConvertedMIC_Seed.Where(x => x.CatCode == item.CatCode && x.CatName == item.CatName).FirstOrDefault();
                    matchedRecord.NextSeed = item.NextSeed + 1;
                }
            }
            return ConvertedMIC_Seed;

        }
        public List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetINGNextSeedList(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedING)
        {
            var list_ING_Seed = _objLibraryFunction_Service.GetINGNextSeedList();
            var ConvertedING_Seed = list_ING_Seed.Select(x => new VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed { Code = x.Code, CatCode = x.CatCode, Prefix = x.Prefix, CatName = x.CatName, NextSeed = x.NextSeed }).ToList();
            if (listSelectedING.Count > 0)
            {
                foreach (var item in listSelectedING)
                {
                    var matchedRecord = ConvertedING_Seed.Where(x => x.CatCode == item.CatCode && x.CatName == item.CatName).FirstOrDefault();
                    matchedRecord.NextSeed = item.NextSeed + 1;
                }
            }
            return ConvertedING_Seed;
        }
        public void UpdateINGNextSeed(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedING)
        {
            List<Cas_MIC_ING_Seed> listUpdateIng = listSelectedING.Select(x => new Cas_MIC_ING_Seed { Code = x.Code, CatCode = x.CatCode, Prefix = x.Prefix, CatName = x.CatName, NextSeed = x.NextSeed + 1 }).ToList();
            //listUpdateIng.ForEach(x => x.NextSeed = x.NextSeed + 1);
            _objLibraryFunction_Service.UpdateINGNextSeed(listUpdateIng);
        }
        public void UpdateMICNextSeed(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedMIC)
        {
            List<Cas_MIC_ING_Seed> listUpdateMIC = listSelectedMIC.Select(x => new Cas_MIC_ING_Seed { Code = x.Code, CatCode = x.CatCode, Prefix = x.Prefix, CatName = x.CatName, NextSeed = x.NextSeed + 1 }).ToList();
            //listUpdateMIC.ForEach(x => x.NextSeed = x.NextSeed + 1);
            _objLibraryFunction_Service.UpdateMICNextSeed(listUpdateMIC);
        }
        public Dictionary<string, List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM>> GetChemicalNameExistingCasFromAccess(string path, string tableName, bool isFoodData, bool isNonFoodData)
        {
            return _objLibraryFunction_Service.GetChemicalNameExistingCasFromAccess(path, tableName, isFoodData, isNonFoodData);
        }
        public List<EntityClass.Library_SubstanceType> GetLibrarySubstanceType()
        {

            return _objLibraryFunction_Service.GetLibrarySubstanceType();
        }
        public List<EntityClass.Library_SubstanceCategories> GetLibrarySubstanceCategory()
        {
            return _objLibraryFunction_Service.GetLibrarySubstanceCategory();
        }
        public List<Map_Incorrect_Cas_ChemicalName_VM> GetMap_Incorrect_Cas_ChemicalName()
        {
            return _objLibraryFunction_Service.GetMap_Incorrect_Cas_ChemicalName();
        }
        //public bool DeleteMap_Incorrect_Cas_ChemicalName(int mapCasNameId)
        //{
        //    //return _objLibraryFunction_Service.DeleteMap_Incorrect_Cas_ChemicalName(mapCasNameId, Model.ApplicationEngine.Engine.CurrentUserSessionID);
        //}
        public List<string> FetchOtherCasByChemicalName(string chemicalName, string casAssigned)
        {
            return _objLibraryFunction_Service.FetchOtherCasByChemicalName(chemicalName, casAssigned);
        }
        public List<string> FetchOtherChemicalNameByCas(string chemicalName, string casAssigned) { return _objLibraryFunction_Service.FetchOtherChemicalNameByCas(chemicalName, casAssigned); }
        public List<string> FetchOtherQsidByChemicalNameAndCas(string chemicalName, string casAssigned) { return _objLibraryFunction_Service.FetchOtherQsidByChemicalNameAndCas(chemicalName, casAssigned); }

        public string GetDelimiterByQsid(string qsid)
        {
            int? qsid_id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(qsid);
            return _objLibraryFunction_Service.GetDelimiterByQsid_ID(qsid_id);
        }
        public bool InsertUpdateIsIncorrectCasName(string chemicalName, string delimiterForQsidCasAssignmentTool, string cas, int qsid_id, int currentUserSessionID)
        {
            return _objLibraryFunction_Service.InsertUpdateIsIncorrectCasName(chemicalName, delimiterForQsidCasAssignmentTool, cas, qsid_id, currentUserSessionID);
        }
        public bool DeleteMap_Incorrect_Cas_ChemicalName(string chemicalName, string delimiterForQsidCasAssignmentTool, string cas, int currentUserSessionID)
        {
            return _objLibraryFunction_Service.DeleteMap_Incorrect_Cas_ChemicalName(chemicalName, delimiterForQsidCasAssignmentTool, cas, currentUserSessionID);
        }
        public List<string> GetActiveListQsid(int? chemicalNameId, int? casID)
        {
            return _objLibraryFunction_Service.GetActiveListQsid(chemicalNameId, casID);
        }
        public List<string> GetCasChildrenList(string casAssigned, bool isFoodData, bool isNonFoodData)
        {
            return _objLibraryFunction_Service.GetCasChildrenList(casAssigned, isFoodData, isNonFoodData);
        }
        public List<string> GetCasAlternateList(string casAssigned)
        {
            return _objLibraryFunction_Service.GetCasAlternateList(casAssigned);
        }
        public int AddProcessTimeTaken_CasAssignment(string tabName, string processDesc, DateTime? startTime, DateTime? endTime, string timeTaken, decimal? startMem, decimal? endMem, int qsidId, int sessId)
        {
            return _objLibraryFunction_Service.AddProcessTimeTaken_CasAssignment(tabName, processDesc, startTime, endTime, timeTaken, startMem, endMem, qsidId, sessId);
        }
        public void UpdateProcessTimeTaken_CasAssignment(int processId, DateTime? endTime, string timeTaken)
        {
            _objLibraryFunction_Service.UpdateProcessTimeTaken_CasAssignment(processId, endTime, timeTaken);
        }
        public List<Log_Map_Incorrect_Cas_ChemicalName_VM> GetLog_Map_Incorrect_Cas_ChemicalName()
        {
            return _objLibraryFunction_Service.GetLog_Map_Incorrect_Cas_ChemicalName();
        }
        public Response_VM CreateMapIncorrectCasChemicalNameWithComments(string chemicalName, string cas, string comment, int SessionID)
        {
            return _objLibraryFunction_Service.CreateMapIncorrectCasChemicalNameWithComments(chemicalName,cas,comment,SessionID);
        }
        public bool DeleteMap_Incorrect_Cas_ChemicalName(int chemicalNameID, int casID, int SessionID,string comment)
        {
            return _objLibraryFunction_Service.DeleteMap_Incorrect_Cas_ChemicalName(chemicalNameID, casID,  SessionID, comment);
        }
        public bool ValidateNextSeq(string seqCas) {
            return _objLibraryFunction_Service.ValidateNextSeq(seqCas);
        }
    }

}
