﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Business.BusinessService
{

    public class GenericBL : IGenericBL
    {
        ILibraryFunction _objLibraryFunction_Service;
        public GenericBL(ILibraryFunction objLibraryFunction_Service)
        {
            _objLibraryFunction_Service = objLibraryFunction_Service;
        }
        public DataTable GetListGenericSuggesionImport_Access(string path)
        {
            return _objLibraryFunction_Service.GetListGenericSuggesionImport_Access(path);
        }
        public List<Response_VM> InsertGenericSuggestion(List<GenericSuggestionVM> listGenericSuggestionVM, int CurrentSessionID, string tab)
        {
            return _objLibraryFunction_Service.InsertGenericSuggestion(listGenericSuggestionVM, CurrentSessionID, tab);
        }
        public List<GenericSuggestionVM> GetListGenericSuggestionForConfirm()
        {
            return _objLibraryFunction_Service.GetListGenericSuggestionForConfirm();
        }
        public void InsertConfirmedGenericSuggestion(List<GenericSuggestionVM> listGenericSuggestionVM)
        {
            _objLibraryFunction_Service.InsertConfirmedGenericSuggestion(listGenericSuggestionVM);
        }

        public bool UpdateIsExcludeInGenericNested(GenericSuggestionVM objGenericSuggestionTree_VM)
        {
            return _objLibraryFunction_Service.UpdateIsExcludeInGenericNested(objGenericSuggestionTree_VM);
        }

        private GenericSuggestionTree_ViewModel GetParent(GenericSuggestionVM genericSugesstion)
        {
            GenericSuggestionTree_ViewModel objGeneric = new GenericSuggestionTree_ViewModel();
            objGeneric.ChildCas = genericSugesstion.ChildCas;
            objGeneric.ChildCasID = genericSugesstion.ChildCasID;
            objGeneric.ParentCasID = genericSugesstion.ParentCasID;
            return objGeneric;
        }
        public GenericSuggestionVM GetListGenericSuggestionByParentAndChildCasID(int? parentCasID, int? ChildCasID, string status)
        {
            return _objLibraryFunction_Service.GetListGenericSuggestionByParentAndChildCasID(parentCasID, ChildCasID, status);
        }
        public bool ApproveGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType)
        {
            return _objLibraryFunction_Service.ApproveGenericSuggestion(objGenericSuggestionTree_VM, tabType);
        }
        public bool RejectGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType)
        {
            return _objLibraryFunction_Service.RejectGenericSuggestion(objGenericSuggestionTree_VM, tabType);
        }
        public void RemoveGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM)
        {
            _objLibraryFunction_Service.RemoveGenericSuggestion(objGenericSuggestionTree_VM);
        }
        public List<GenericSuggestionComments_VM> GetListGenericsComments(int? parentCasID, int? ChildCasID)
        {
            return _objLibraryFunction_Service.GetListComment(parentCasID, ChildCasID).Select(x => new GenericSuggestionComments_VM
            {
                Action = x.Action,
                ChildCasID = x.ChildCasID,
                CommentBy = x.CommentBy,
                Comments = x.Comments,
                ParentCasID = x.ParentCasID,
                Timestamp = x.Timestamp,
                Additional_Generics_research_flag=x.Additional_Generics_research_flag,
                CommentID=x.CommentID
            }).ToList();
        }
        public List<GenericSuggestionComments_VM> GetListGenericsParentComments(int? parentCasID)
        {
            return _objLibraryFunction_Service.GetListParentComment(parentCasID).Select(x => new GenericSuggestionComments_VM
            {
                Action = x.Action,
                //ChildCasID = x.ChildCasID,
                CommentBy = x.CommentBy,
                Comments = x.Comments,
                ParentCasID = x.ParentCasID,
                Timestamp = x.Timestamp,
                Additional_Generics_research_flag = x.Additional_Generics_research_flag
            }).ToList();
        }
        public int InsertAdditionalComment(GenericSuggestionVM objGenericComment, string type)
        {
           return _objLibraryFunction_Service.InsertAdditionalComment(objGenericComment, type);
        }
        public List<FilterGenericSuggestion> SearchGenericSuggestionCasList(string searchKeyword, string searchFor, string searchType, string matchCondition)
        {
            return _objLibraryFunction_Service.SearchGenericSuggestionCasList(searchKeyword, searchFor, searchType, matchCondition);
        }

        public void SaveParentChildCasName(GenericParentChildCasName_VM objGenericParentChildCasName)
        {
            _objLibraryFunction_Service.SaveParentChildCasName(objGenericParentChildCasName);
        }

        public List<Map_Hydrates_VM> GetListAllMapHydrates()
        {
            return _objLibraryFunction_Service.GetListAllMapHydrates();
        }
        public List<Map_Hydrates_VM> GetListAllMapHydratesSingleCAS(string casname)
        {
            return _objLibraryFunction_Service.GetListAllMapHydratesSingleCAS(casname);
        }
        public Response_VM AddNewMap_Hydrates(Map_Hydrates_VM map_Hydrates_VM)
        {
            return _objLibraryFunction_Service.AddNewMap_Hydrates(map_Hydrates_VM);
        }
        public bool DeleteMap_Hydrate(int hydrateID, int currentUserSessionID,string comment)
        {
            return _objLibraryFunction_Service.DeleteMap_Hydrate(hydrateID, currentUserSessionID,comment);
        }
        public int? GetChildChemicalNameIDByCas(string cas)
        {
            return _objLibraryFunction_Service.GetChildChemicalNameIDByCas(cas);
        }
        public int? GetParentChemicalNameIDByCas(string cas)
        {
            return _objLibraryFunction_Service.GetParentChemicalNameIDByCas(cas);
        }

        public List<GenericChemicalName_VM> FilterChemicalNameForGeneric(string name, int? chemID = 0)
        {
            return _objLibraryFunction_Service.FilterChemicalNameForGeneric(name, chemID);
        }
        public List<MapProductList_VM> GetAllLibraryProductLists()
        {
            return _objLibraryFunction_Service.GetAllLibraryProductLists();
        }
        public List<Library_QSIDs> GetActiveQsidsListDetailByCasID(int childCasID)
        {
            return _objLibraryFunction_Service.GetActiveQsidsListDetailByCasID(childCasID);
        }
        private void RecursiveNestedParent(List<GenericeNestedParent> nestedList, int GroupNo, int NestingLevel, int childCasId, ref List<GenericeNestedParent> listNestedGroup)
        {
            var childNested = nestedList.Where(x => x.ParentCasID == childCasId).ToList();
            if (childNested != null && childNested.Count>0)
            {
                foreach(var item in childNested)
                if (!listNestedGroup.Any(x => x.ParentCas == item.ParentCas && x.ChildCas == item.ChildCas))
                {
                    listNestedGroup.Add(MapGenericNestedParent(item, GroupNo, (NestingLevel + 1)));
                    RecursiveNestedParent(nestedList, GroupNo, NestingLevel + 1, item.ChildCasID, ref listNestedGroup);
                }
            }
        }
        public List<GenericeNestedParent> GetGenericNestedParent(int childCasID, string casXPath, string searchType)
        {
            var nestedList = _objLibraryFunction_Service.GetGenericNestedParent(childCasID, casXPath, searchType);
            var parentList = nestedList.Where(x => x.IsParent == false).ToList();
            List<GenericeNestedParent> listNestedGroup = new List<GenericeNestedParent>();
            for (int counter = 0; counter < parentList.Count; counter++)
            {
                listNestedGroup.Add(MapGenericNestedParent(parentList[counter], counter + 1, 1));
                RecursiveNestedParent(nestedList, counter + 1, 1, parentList[counter].ChildCasID, ref listNestedGroup);
            }
            return listNestedGroup;


        }
        public GenericeNestedParent MapGenericNestedParent(GenericeNestedParent existingNestedObject, int NestedGroupNo, int NestingLevel)
        {
            GenericeNestedParent objMapNestedParent = new GenericeNestedParent();
            objMapNestedParent.ChildCas = existingNestedObject.ChildCas;
            objMapNestedParent.ChildCasID = existingNestedObject.ChildCasID;
            objMapNestedParent.ChildName = existingNestedObject.ChildName;
            objMapNestedParent.NestingGroupNo = NestedGroupNo;
            objMapNestedParent.NestingLevel = NestingLevel;
            objMapNestedParent.ParentCas = existingNestedObject.ParentCas;
            objMapNestedParent.ParentCasID = existingNestedObject.ParentCasID;
            objMapNestedParent.ParentName = existingNestedObject.ParentName;
            objMapNestedParent.IsExclude = existingNestedObject.IsExclude;
            objMapNestedParent.Status = existingNestedObject.Status;
            objMapNestedParent.DateAdded = existingNestedObject.DateAdded;
            return objMapNestedParent;
        }
        public Library_CAS GetLibraryCasByCas(string cas)
        {
            return _objLibraryFunction_Service.GetLibraryCasByCas(cas);
        }
        public List<Library_GenericsCategories> GetAllGenericsCategories()
        {
            return _objLibraryFunction_Service.GetAllGenericsCategories();
        }

        public List<Library_SubstanceType> GetAllSubstanceType()
        {
            return _objLibraryFunction_Service.GetAllSubstanceType();
        }
        public List<Library_SubstanceCategories> GetAllSubstanceCategories()
        {
            return _objLibraryFunction_Service.GetAllSubstanceCategories();
        }
        public int Get_Map_Generic_Detail_IDs(string type, int casID)
        {
            return _objLibraryFunction_Service.Get_Map_Generic_Detail_IDs(type, casID);
        }
        public bool UpdateSubstanceInfo(Map_Generic_Detail objMapGenericDetail, string cas, int currentSessionID)
        {
            return _objLibraryFunction_Service.UpdateSubstanceInfo(objMapGenericDetail, cas, currentSessionID);
        }
        public bool ValidateIsParentSearch(int casID)
        {
            return _objLibraryFunction_Service.ValidateIsParentSearch(casID);
        }
        public List<Map_AlternateCas_VM> GetListAllMapAlternateCas()
        {
            return _objLibraryFunction_Service.GetListAllMapAlternateCas();
        }
        public List<Map_AlternateCas_VM> GetListAllMapAlternateCasSingleCAS(string casname)
        {
            return _objLibraryFunction_Service.GetListAllMapAlternateCasSingleCAS(casname);
        }
        public Response_VM AddNewMap_AlternateCas(Map_AlternateCas_VM map_AlternateCas_VM)
        {
            return _objLibraryFunction_Service.AddNewMap_AlternateCas(map_AlternateCas_VM);
        }
        public bool DeleteMap_AlternateCas(int alternateID, int currentUserSessionID, string comment)
        {
            return _objLibraryFunction_Service.DeleteMap_AlternateCas(alternateID, currentUserSessionID,comment);
        }
        public bool CheckCasValid(string cas)
        {
            return _objLibraryFunction_Service.CheckCasValid(cas);
        }
        public string RemoveStripCharcterFromCas(string cas)
        {
            return _objLibraryFunction_Service.RemoveStripCharcterFromCas(cas);
        }
        public List<Map_Generic_Flat_VM> GetAllFlatGenerics(string cas, string selectType)
        {
            return _objLibraryFunction_Service.GetAllFlatGenerics(cas, selectType);
        }
        public List<Map_Generic_Flat_VM> GetAllFlatGenericsByQsid(string qsid, string selectType)
        {
            return _objLibraryFunction_Service.GetAllFlatGenericsByQsid(qsid, selectType);
        }
        public List<GenericSuggestionVM> GetLogGenericsDeleted(string cas, string searchType)
        {
            return _objLibraryFunction_Service.GetLogGenericsDeleted(cas, searchType);
        }
        public List<GenericSuggestionVM> GetLogGenericsSuggestionRejected()
        {
            return _objLibraryFunction_Service.GetLogGenericsSuggestionRejected();
        }
        public List<Map_Combined_Generic_VM> GetAllCombinedGenerics(string cas, string Type)
        {
            return _objLibraryFunction_Service.GetAllCombinedGenerics(cas, Type);
        }
        public List<Map_Combined_Generic_VM> GetAllCombinedGenericsByQsid(string cas, string Type)
        {
            return _objLibraryFunction_Service.GetAllCombinedGenericsByQsid(cas, Type);
        }
        public int InsertIfNotExistChemicalName(string chemicalName)
        {
            return _objLibraryFunction_Service.InsertIfNotExistChemicalName(chemicalName);
        }
        public List<Map_AlternateCas_VM> GetListAllMapAlternateCasSearch(string searchCas_AlternateCas, string text)
        {
            return _objLibraryFunction_Service.GetListAllMapAlternateCasSearch(searchCas_AlternateCas, text);
        }
        public List<Map_Hydrates_VM> GetListAllMapHydratesSearch(string searchCasHydrate, string text)
        {

            return _objLibraryFunction_Service.GetListAllMapHydratesSearch(searchCasHydrate, text);

        }
        public void Refresh_RecalculateFlatGenerics(string searchCasFlatGenerics, string text)
        {
            _objLibraryFunction_Service.Refresh_RecalculateFlatGenerics(searchCasFlatGenerics, text);
        }

        public List<Generic_CircularRef_VM> GetAllUniqueCircularRef(string searchCas_UniqueCircularRef, string text)
        {

            var list = _objLibraryFunction_Service.GetAllUniqueCircularRef(searchCas_UniqueCircularRef, text);
            return list.Select(x => new Generic_CircularRef_VM()
            {
                ChildCas = x.ChildCas,
                ParentCas = x.ParentCas,
                ParentCasID = x.ParentCasID,
                ChildCasID = x.ChildCasID,
                ParentChemicalName=x.ParentChemicalName,
                ChemicalName=x.ChemicalName

            }).ToList();
        }
        public bool ApproveGenericSuggestion_Bulk(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType)
        {
            return _objLibraryFunction_Service.ApproveGenericSuggestion(objGenericSuggestionTree_VM, tabType);
        }
        public bool RejectGenericSuggestion_Bulk(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType)
        {
            return _objLibraryFunction_Service.RejectGenericSuggestion(objGenericSuggestionTree_VM, tabType);
        }

        public List<MapGenericsCalculated_ViewModel> GetListDetailGenericsCalculated_ViewModel()
        {
           return Common.CommonGeneralFunction.MapSubstanceSearchBusinessToView(_objLibraryFunction_Service.GetListDetailGenericsCalculated());
        }

        public List<GenericSuggestionComments_VM> GetListIdentifierComments(int? casId, int? identifierValueID,int? identifierTypeId)
        {
            return _objLibraryFunction_Service.GetListIdentifierComment(casId, identifierValueID, identifierTypeId).Select(x => new GenericSuggestionComments_VM
            {
                Action = x.Action,
                ChildCasID = x.ChildCasID,
                CommentBy = x.CommentBy,
                Comments = x.Comments,
                ParentCasID = x.ParentCasID,
                Timestamp = x.Timestamp,
                Additional_Generics_research_flag = x.Additional_Generics_research_flag,
                CommentID = x.CommentID
            }).ToList();
        }
    }
}
