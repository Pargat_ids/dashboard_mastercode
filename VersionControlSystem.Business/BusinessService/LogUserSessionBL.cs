﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.IO;
using VersionControlSystem.Business.Common;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;

namespace VersionControlSystem.Business.BusinessService
{
    public class LogUserSessionBL : ILogUserSessionBL
    {
        CRA_DataAccess.ILibraryFunction _objLibraryFunction_Service { get; set; }
        public LogUserSessionBL(CRA_DataAccess.ILibraryFunction objLibraryFunction_Service)
        {
            _objLibraryFunction_Service = objLibraryFunction_Service;
        }

        public void UpdateSessionForVersionControl(int sessionID)
        {
            _objLibraryFunction_Service.UpdateSessionForVersionControl(sessionID);
        }
        public Log_Sessions CreateNewSessionForVersionControl(string username)
        {
            return _objLibraryFunction_Service.CreateNewSessionForVersionControl(username, Model.ApplicationEngine.Engine.ToolVersion);
        }
        public string GetLibrary_UserName(string username)
        {
            return _objLibraryFunction_Service.GetLibrary_UserName(username);
        }
        public Map_VersionControl_Permissions GetCurrentUserRolePermission(string username)
        {
            return _objLibraryFunction_Service.GetCurrentUserRolePermission(username);
        }
        public void LoginError(List<Log_ErrorTracking> listLogErrorTrack)
        {
            _objLibraryFunction_Service.BulkIns<Log_ErrorTracking>(listLogErrorTrack);
        }
        public bool CheckExistingInstanceIsOpen(string username)
        {
            return _objLibraryFunction_Service.CheckExistingInstanceIsOpen(username);
        }
        public bool ValidateUserIDForCraDashboard(string username)
        {
            return _objLibraryFunction_Service.ValidateUserIDForCraDashboard(username);
        }
        private void InsertLogErrorDB(Exception errorException)
        {
            
                List<Log_ErrorTracking> listLogErrorTrack = new List<Log_ErrorTracking>();
                Log_ErrorTracking objLogErrorTrack = new Log_ErrorTracking();
                objLogErrorTrack.ErrorMessage = errorException.Message;
                objLogErrorTrack.ErrorLocation = errorException.StackTrace.Length >= 2000 ? errorException.StackTrace.Substring(0, 1999) : errorException.StackTrace;
                objLogErrorTrack.UserName = Environment.UserName;
                objLogErrorTrack.TimeStamp = CommonGeneralFunction.ESTTime();
                objLogErrorTrack.SessionID = Engine.CurrentUserSessionID;
                listLogErrorTrack.Add(objLogErrorTrack);
                LoginError(listLogErrorTrack);
            
           
        }
        public string ErrorLogging(Exception ex)
        {
            string currentDT = DateTime.Today.ToString("MM_dd_yyyy");
            string strPath = @"P:\CRA\Tools\VersionControl\ErrorLogs\" + "Error_Log_" + Engine.CurrentUserName.Replace(" ", "") + "_" + currentDT + ".txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(strPath))
            {
                sw.WriteLine("=============Error Logging ===========");
                sw.WriteLine("===========Start============= " + DateTime.Now);
                sw.WriteLine("Error Message: " + ex.Message);
                sw.WriteLine("Stack Trace: " + ex.StackTrace);
                sw.WriteLine("===========End============= " + DateTime.Now);

            }
            InsertLogErrorDB(ex);
            return strPath;
        }
    }
}
