﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Business.Common;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.DataAccess.DataService;
using VersionControlSystem.DataAccess.IDataService;
using VersionControlSystem.DataAccess.Repository;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Business.BusinessService
{
    public class Access_Version_BL : IAccess_Version_BL
    {
        public IGenericDataService _objGenericData_Service;
        ILibraryFunction _objLibraryFunction_Service;
        public Access_Version_BL(ILibraryFunction objLibraryFunction_Service)
        {
            _objGenericData_Service = new GenericDataService();
            _objLibraryFunction_Service = objLibraryFunction_Service;
            // _objLibraryFunction_Service = LibraryFunction.GetInstance;
        }


        public string FetchCurrentVersion_QSID(string QSID, string AccessDBPath)
        {
            var checkQsid = _objLibraryFunction_Service.ChkLibraryQsid(QSID, Engine.CurrentUserSessionID, AccessDBPath, CommonGeneralFunction.ESTTime());
            if (string.IsNullOrEmpty(checkQsid.Item1) && checkQsid.Item3 != 0)
            {
                int obj_QSID = checkQsid.Item3;
                if (obj_QSID > 0)
                {
                    var obj_Access_Data = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == obj_QSID).OrderByDescending(x => x.DateFirstAdded).FirstOrDefault();
                    if (obj_Access_Data == null) { return "00001"; }
                    return obj_Access_Data.Version;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public bool ValidateQSID(int? Qsid_ID)
        {
            bool flagRes = false;
            //if (_objLibraryFunction_Service.CheckExist_Qsid(QSID))
            //{
            //    int Qsid_ID = _objLibraryFunction_Service.ChkLibraryQsid(QSID, Engine.CurrentUserSessionID, AccessDBPath, DateTime.Now).Item3;
            if (_objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).Count > 0)
            {
                flagRes = true;
            }
            //};
            return flagRes;
        }

        public bool CheckGetCopyQsidForCurrentUser(int qsid_ID)
        {
            bool resultFlag = false;
            Log_VersionControl_History obj_VersionData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == qsid_ID).OrderByDescending(x => x.DateFirstAdded).FirstOrDefault();
            if (obj_VersionData != null)
            {
                if (obj_VersionData.Currently_Checked_Out == true)
                {
                    resultFlag = true;
                }
            }
            return resultFlag;
        }

        public bool CheckUnlockForCurrentUser(int qsid_ID)
        {
            bool resultFlag = false;
            //VersionControl_Access_QSID obj_QSID = FecthQsid_IDAccordingToQsid(qsid);
            Log_VersionControl_History obj_VersionData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();

            if (obj_VersionData != null)
            {
                if (obj_VersionData.Currently_Checked_Out == true)//&& obj_VersionData.IsLocked == true
                {
                    string incrementedVersion = IncrementVersion(obj_VersionData.Version);
                    var objCheckoutTemp = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == qsid_ID && x.CheckOut_Version == incrementedVersion).OrderByDescending(x => x.CheckOut_TimeStamp).FirstOrDefault();
                    if (objCheckoutTemp != null)
                    {
                        if (objCheckoutTemp.UserName.ToLower() == Environment.UserName.ToLower())
                        {
                            resultFlag = true;
                        }
                    }
                }
            }
            return resultFlag;
        }

        public List<Log_VersionControl_History> GetAllVersionHistoryByProductionDate(DateTime? FromProduction, DateTime? ToProduction)
        {
            return _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.ProductTimeStamp >= (FromProduction != null ? FromProduction : x.ProductTimeStamp) && x.ProductTimeStamp <= (ToProduction != null ? ToProduction : x.ProductTimeStamp));
        }

        public bool CheckActiveCheckOutButton(int qsid_ID)
        {
            bool resultFlag = false;

            Log_VersionControl_History obj_VersionData = _objGenericData_Service.AttachInstance_ReloadContext<Log_VersionControl_History>().Get(x => x.QSID_ID == qsid_ID).OrderByDescending(x => Convert.ToInt32(Convert.ToInt32(x.Version))).FirstOrDefault();
            if (obj_VersionData != null)
            {
                if (obj_VersionData.Currently_Checked_Out == false || obj_VersionData.Currently_Checked_Out == null)
                {
                    resultFlag = true;
                }
            }
            return resultFlag;
        }
        public bool CheckActiveCheckInButton(int qsid_ID)
        {
            bool resultFlag = false;

            Log_VersionControl_History obj_VersionData = _objGenericData_Service.AttachInstance_ReloadContext<Log_VersionControl_History>().Get(x => x.QSID_ID == qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            if (obj_VersionData != null)
            {
                string increVersion = IncrementVersion(obj_VersionData.Version);
                Log_VersionControl_History_Temp objVersionData_Temp = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == qsid_ID && x.CheckOut_Version == increVersion).OrderByDescending(x => x.CheckOut_Version).FirstOrDefault();
                if (objVersionData_Temp != null)
                {
                    if (obj_VersionData.Currently_Checked_Out == true && objVersionData_Temp.UserName.ToLower() == Environment.UserName.ToLower())
                    {
                        resultFlag = true;
                    }
                }
            }

            return resultFlag;
        }

        public Dictionary<string, Log_VersionControl_History> AddNewQSIDList(string SelectedPath, string listfileName)
        {
            Dictionary<string, Log_VersionControl_History> resultQsid_ID = new Dictionary<string, Log_VersionControl_History>(); ;

            string currentVersion = string.Empty;
            string AccessDBPath = SelectedPath;
            List_Dictionary_VM obj_MetaData = CommonGeneralFunction.GetMeta_List_Dictionary_FromAccessFile("Select [LIST_FIELD_NAME], [QSID], [MODULE],[COUNTRY], [SHORT_NAME] as shortName, [SHORT_DESCRIPTION] as ShortDescription from [List_Dictionary]", AccessDBPath);
            string validateMetaDataResult = ValidateListDictionaryData(obj_MetaData, AccessDBPath);
            if (!string.IsNullOrEmpty(validateMetaDataResult))
            {
                MessageBox.Show("Information missing following column in List_Dictionary table in selected file: " + Environment.NewLine + validateMetaDataResult, "Error:");
                resultQsid_ID.Add(string.Empty, null);
                return resultQsid_ID;
            }
            int? qsid_id = _objLibraryFunction_Service.GetQsid_IDFromLibrary(obj_MetaData.QSID);
            if (ValidateQSID(qsid_id))
            {
                MessageBox.Show("Qsid already exist in log version control history data table.", "Error:");
                resultQsid_ID.Add(obj_MetaData.QSID, null);
                return resultQsid_ID;
            }
            int totalCount = CalculateTotalRecordsFromMDB(AccessDBPath);
            int totalUniqueSubstanceCount = CalculateUniqueSubstanceFromMDB(AccessDBPath);
            string module = obj_MetaData.Module;
            string country = _objLibraryFunction_Service.GetLibrary_CountryName(obj_MetaData.Country);
            currentVersion = FetchCurrentVersion_QSID(obj_MetaData.QSID, AccessDBPath);
            if (string.IsNullOrEmpty(currentVersion))
            {
                MessageBox.Show("Internal server error due to Add new QSID to DB.", "Error:");
                resultQsid_ID.Add(string.Empty, null);
                return resultQsid_ID;
            }
            string destinationPath = Engine.CommonAccessFolderPath + module.Replace("/", "\\") + "\\" + country;
            CommonGeneralFunction.CreateDirectoryPath(destinationPath);
            string fileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(listfileName, currentVersion);
            CommonGeneralFunction.GenerateFileToDestination(SelectedPath, destinationPath + "\\" + fileName);

            int currentqsid_ID = insert_QSIDAndPharseCategory(obj_MetaData.QSID, AccessDBPath);
            if (currentqsid_ID == 0)
            {
                resultQsid_ID.Add(string.Empty, null);
                return resultQsid_ID;
            }
            Log_VersionControl_History oVersionControlData = CommonGeneralFunction.BindModelVersionData(currentqsid_ID, destinationPath + "\\" + fileName, listfileName, currentVersion, (DateTime?)null, CommonGeneralFunction.ESTTime(), totalCount, totalUniqueSubstanceCount, string.Empty, (DateTime?)null);
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Insert(oVersionControlData);
            resultQsid_ID.Add(obj_MetaData.QSID, oVersionControlData);
            return resultQsid_ID;
        }

        public int insert_QSIDAndPharseCategory(string currentQSID, string AccessDB_Path)
        {
            int currentQsid_Id = 0;
            bool refparam = false;
            List<IStripChar> lstStripChar = new List<IStripChar>();
            List<IQsxxxListDictionary> listQsxxDic = new List<IQsxxxListDictionary>();
            var QSID_ID = _objLibraryFunction_Service.ChkLibraryQsid(currentQSID, Engine.CurrentUserSessionID, AccessDB_Path, CommonGeneralFunction.ESTTime());
            string PhraseStatus = _objLibraryFunction_Service.AddLibraryPhraseCategoriesDashboard(ref refparam, ref refparam, ref refparam, QSID_ID.Item3, CommonGeneralFunction.ESTTime(), lstStripChar, Engine.CurrentUserSessionID, AccessDB_Path);
            if (PhraseStatus != "Pass")
            {
                MessageBox.Show(PhraseStatus, "Error While Inserting List Dictionary: ");
            }
            else
            {
                currentQsid_Id = QSID_ID.Item3;
            }
            return currentQsid_Id;
        }
        public bool Update_QSIDAndPharseCategory(int QSID_ID, string AccessDB_Path)
        {
            bool IsUpdate = false;
            bool refparam = false;
            List<IStripChar> lstStripChar = _objLibraryFunction_Service.GetStripCharacter();
            List<IQsxxxListDictionary> listQsxxDic = new List<IQsxxxListDictionary>();
            string PhraseStatus = _objLibraryFunction_Service.AddLibraryPhraseCategoriesDashboard(ref refparam, ref refparam, ref refparam, QSID_ID, CommonGeneralFunction.ESTTime(), lstStripChar, Engine.CurrentUserSessionID, AccessDB_Path);
            if (PhraseStatus != "Pass")
            {
                MessageBox.Show(PhraseStatus, "Error While Inserting List Dictionary: ");
            }
            else
            {
                IsUpdate = true;
            }
            return IsUpdate;
        }

        public bool CheckEnableForEditVersionHistoryInfo(int qSID_ID, string version, string flagType)
        {
            bool resultFlag = false;
            int CurrentVersion = Convert.ToInt32(version);
            List<Log_VersionControl_History> list_VersionAccessData = _objGenericData_Service.AttachInstance_ReloadContext<Log_VersionControl_History>().Get(x => x.QSID_ID == qSID_ID).OrderByDescending(x => x.Version).ToList();
            if (list_VersionAccessData.FirstOrDefault().Version == version)
            {
                resultFlag = true;
            }
            else
            {
                if (flagType == "AddProductTimestamp")
                {
                    var objVersionProductDateTimestamp = list_VersionAccessData.Where(x => int.Parse(x.Version) > CurrentVersion && x.ProductTimeStamp != null).FirstOrDefault();
                    if (objVersionProductDateTimestamp == null)
                    {
                        resultFlag = true;
                    }
                }
                else if (flagType == "AddNote")
                {
                    var objVersionProductDateTimestamp = list_VersionAccessData.Where(x => int.Parse(x.Version) > CurrentVersion && !string.IsNullOrEmpty(x.AddNote)).FirstOrDefault();
                    if (objVersionProductDateTimestamp == null)
                    {
                        resultFlag = true;
                    }
                }
            }
            return resultFlag;
        }

        public List<Access_QSID_Data_VM> GetList_VersionHistory(int? Qsid_ID, DateTime? FromProduction, DateTime? ToProduction)
        {
            //_objGenericData_Service.AttachInstance_ReloadContext<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID && (FromProduction != null ? (x.ProductTimeStamp >= FromProduction) : true) && (ToProduction != null ? x.ProductTimeStamp <= ToProduction : true)).OrderByDescending(x => x.Version)
            List<Access_QSID_Data_VM> list_VersionAccessData = _objLibraryFunction_Service.GetAllVersionHistoryList(Qsid_ID, FromProduction, ToProduction).Select(
             x => new Access_QSID_Data_VM
             {
                 VersionControl_ID = x.VersionControl_HistoryID,
                 ListVersionChangeTypeID = x.ListVersionChangeTypeID,
                 QSID = _objLibraryFunction_Service.GetQsidFromLibrary(Convert.ToInt32(x.QSID_ID)),
                 Check_In_TimeStamp = x.Check_In_TimeStamp,
                 Check_Out_TimeStamp = x.Check_Out_TimeStamp,
                 FileName = x.FileName,
                 FilePath = x.FilePath,
                 Version = x.Version,
                 UserName = _objLibraryFunction_Service.GetLibrary_UserName(x.CheckedOutByUser),
                 DateFirstAdded = x.DateFirstAdded,
                 AddNote = x.AddNote,
                 ProductTimeStamp = x.ProductTimeStamp,
                 QSID_ID = Convert.ToInt32(x.QSID_ID),
                 TotalCount = x.TotalCount.ToString(),
                 UniqueSubstance = x.UniqueSubstance.ToString(),
                 WorkOrderNumber = GetWorkOrderNumber(x.VersionControl_HistoryID),
                 TypeChange = GetChangeType(x.ListVersionChangeTypeID),
                 EditorialChangeType = GetEditorialChangeType(x.VersionControl_HistoryID),
                 CheckedOut = x.Currently_Checked_Out,
                 TimeSpentUnit = x.TimeSpentUnit,
                 TimeSpentValue = x.TimeSpentValue,
                 TicketNumber = GetTicketNumber(x.VersionControl_HistoryID)
                 //  IsLocked = x.IsLocked                 
             }).ToList();
            return list_VersionAccessData;
        }

        private List<HyperLinkDataValue> GetWorkOrderNumber(int? versionControl_ID)
        {
            int?[] WONumber = _objLibraryFunction_Service.GetWorkOrderNumber(versionControl_ID);
            List<HyperLinkDataValue> listWorkOrder = WONumber.Select(x => new HyperLinkDataValue { DisplayValue = x.ToString(), NavigateData = "https://awm.3ecompany.com/ViewWorkOrder.aspx?WOId=" + Encrypt(x.ToString()) }).ToList();
            return listWorkOrder;
        }
        private List<HyperLinkDataValue> GetTicketNumber(int? versionControl_ID)
        {
            List<HyperLinkDataValue> listTicketNumber = _objLibraryFunction_Service.GetLog_VersionControl_Tickets(versionControl_ID).Select(x =>
            new HyperLinkDataValue
            {
                DisplayValue = x.CSC_Ticket_Number,
                NavigateData = x.CSC_Ticket_Url,
                IsValid = !string.IsNullOrEmpty(x.CSC_Ticket_Url) && Uri.IsWellFormedUriString(x.CSC_Ticket_Url, UriKind.Absolute) ? true : false
            }).ToList();
            return listTicketNumber;
        }
        private string Encrypt(string plainText)

        {

            const string passPhrase = "Pass@sng23"; // can be any string

            const string saltValue = "salt@sng23"; // can be any string

            const string hashAlgorithm = "SHA1"; // can be "MD5"

            const int passwordIterations = 2; // can be any number

            const string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes

            const int keySize = 256; // can be 192 or 128



            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);

            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            var password = new PasswordDeriveBytes(passPhrase,

                                                   saltValueBytes,

                                                   hashAlgorithm,

                                                   passwordIterations);



            byte[] keyBytes = password.GetBytes(keySize / 8);

            var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(

                keyBytes,

                initVectorBytes);



            var memoryStream = new MemoryStream();

            var cryptoStream = new CryptoStream(memoryStream,

                                                encryptor,

                                                CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            cryptoStream.FlushFinalBlock();

            byte[] cipherTextBytes = memoryStream.ToArray();

            memoryStream.Close();

            cryptoStream.Close();



            string cipherText = Convert.ToBase64String(cipherTextBytes);

            return cipherText;

        }
        private string GetChangeType(int? versionControl_ID)
        {
            return _objLibraryFunction_Service.GetChangeType(versionControl_ID);
        }
        private string GetEditorialChangeType(int? versionControl_ID)
        {
            return _objLibraryFunction_Service.GetEditorialChangeType(versionControl_ID);
        }

        public void UpdateAccessversionData(Access_QSID_Data_VM updatedAccess_Version_Data)
        {
            Log_VersionControl_History objVersionControldata = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().GetByID(updatedAccess_Version_Data.VersionControl_ID);
            objVersionControldata.VersionControl_HistoryID = updatedAccess_Version_Data.VersionControl_ID;
            objVersionControldata.QSID_ID = updatedAccess_Version_Data.QSID_ID;
            objVersionControldata.Check_In_TimeStamp = updatedAccess_Version_Data.Check_In_TimeStamp;
            objVersionControldata.Check_Out_TimeStamp = updatedAccess_Version_Data.Check_Out_TimeStamp;
            objVersionControldata.FilePath = updatedAccess_Version_Data.FilePath;
            objVersionControldata.FileName = updatedAccess_Version_Data.FileName;
            objVersionControldata.Version = updatedAccess_Version_Data.Version;
            objVersionControldata.CheckedOutByUser = updatedAccess_Version_Data.UserName;
            objVersionControldata.DateFirstAdded = updatedAccess_Version_Data.DateFirstAdded;
            //objVersionControldata.TotalCount = updatedAccess_Version_Data.TotalCount;           
            //objVersionControldata.UniqueSubstance = updatedAccess_Version_Data.UniqueSubstance;
            objVersionControldata.ProductTimeStamp = updatedAccess_Version_Data.ProductTimeStamp;
            objVersionControldata.AddNote = updatedAccess_Version_Data.AddNote;
            objVersionControldata.Currently_Checked_Out = updatedAccess_Version_Data.CheckedOut;
            //objVersionControldata.IsLocked = updatedAccess_Version_Data.IsLocked;
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Update(objVersionControldata);
        }

        public List<QSID_VM> GetAllQSID_VM()
        {
            return _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get().GroupBy(x => x.QSID_ID).Distinct().Select(x => new QSID_VM { QSID_ID = Convert.ToInt32(x.Key) }).ToList();
        }
        public string IncrementVersion(string version)
        {
            int currentVersion = Convert.ToInt32(version);
            return (currentVersion + 1).ToString().PadLeft(5, '0');
        }
        public bool CheckOutFunctionality(int? Qsid_ID)
        {
            bool result = false;
            bool replaceOldFile = true;
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();//x.UserName == Environment.UserName &&
            if (!File.Exists(objVersionAccessData.FilePath))
            {
                MessageBox.Show("Error: Source File not exists on below path:" + Environment.NewLine + objVersionAccessData.FilePath);
                return result;
            }
            if (objVersionAccessData != null)
            {
                string incrementedVersion = IncrementVersion(objVersionAccessData.Version);
                string FileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(objVersionAccessData.FileName, incrementedVersion);
                string destinationPath = Engine.CommonCheckoutFolderPath + FileName;
                if (CommonGeneralFunction.CheckFileExist(destinationPath))
                {
                    if (DialogResult.Yes != MessageBox.Show("System detect same file on user version folder, Did you replace the old file?", "Confirm", MessageBoxButtons.YesNo))
                    {
                        replaceOldFile = false;
                    }
                }
                if (replaceOldFile)
                {
                    CommonGeneralFunction.CreateDirectoryPath(Engine.CommonCheckoutFolderPath);
                    CommonGeneralFunction.GenerateFileToDestination(objVersionAccessData.FilePath, destinationPath);
                }
                objVersionAccessData.Currently_Checked_Out = true;
                // objVersionAccessData.IsLocked = true;
                _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Update(objVersionAccessData);

                Log_VersionControl_History_Temp objAccess_Data_temp_Checkout = new Log_VersionControl_History_Temp();
                objAccess_Data_temp_Checkout.QSID_ID = objVersionAccessData.QSID_ID;
                objAccess_Data_temp_Checkout.CheckOut_Version = incrementedVersion;
                objAccess_Data_temp_Checkout.DateFirstAdded = CommonGeneralFunction.ESTTime();
                objAccess_Data_temp_Checkout.UserName = Environment.UserName;
                objAccess_Data_temp_Checkout.CheckOut_TimeStamp = CommonGeneralFunction.ESTTime();
                _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Insert(objAccess_Data_temp_Checkout);
                result = true;
            }
            return result;
        }
        public string CheckOutFileName(int? Qsid_ID)
        {
            Log_VersionControl_History objLastVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            Log_VersionControl_History_Temp obj_LastCheckOutData = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.CheckOut_Version).FirstOrDefault();
            if (objLastVersionAccessData != null && obj_LastCheckOutData != null && objLastVersionAccessData.Currently_Checked_Out == true)
            {
                string fileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(objLastVersionAccessData.FileName, obj_LastCheckOutData.CheckOut_Version);
                return Engine.CommonCheckoutFolderPath + fileName;
            }
            else
            {
                return string.Empty;
            }
        }
        public string CheckInFileName(int? Qsid_ID)
        {
            Log_VersionControl_History objLastVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            Log_VersionControl_History_Temp obj_LastCheckOutData = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.CheckOut_Version).FirstOrDefault();
            if (objLastVersionAccessData != null && obj_LastCheckOutData == null && objLastVersionAccessData.Currently_Checked_Out == false)
            {
                return objLastVersionAccessData.FilePath;
            }
            else
            {
                return string.Empty;
            }
        }

        public Log_VersionControl_History CheckIn_Functionality(int Qsid_ID, DateTime? ProductDateTimeStamp, string Note)
        {
            Log_VersionControl_History obj_NewVersionData = null;
            Log_VersionControl_History objLastVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault(); // x.UserName == Environment.UserName &&
            if (objLastVersionAccessData != null)
            {
                string originalFileName = objLastVersionAccessData.FileName;
                string currentVersion = (Convert.ToInt32(objLastVersionAccessData.Version) + 1).ToString().PadLeft(5, '0');
                Log_VersionControl_History_Temp obj_LastCheckOutData = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID && x.UserName.ToLower() == Environment.UserName.ToLower()).OrderByDescending(x => x.CheckOut_Version).FirstOrDefault();

                string fileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(objLastVersionAccessData.FileName, currentVersion);
                string sourcePath = Engine.CommonCheckoutFolderPath + fileName;
                List_Dictionary_VM obj_MetaData = SqlClassRepository.DbaseQueryReturnObject<List_Dictionary_VM>("Select [LIST_FIELD_NAME], [QSID], [MODULE],[COUNTRY], [SHORT_NAME] as shortName, [SHORT_DESCRIPTION] as ShortDescription from [List_Dictionary]", sourcePath);
                string resultValidateListDic = ValidateListDictionaryData(obj_MetaData, sourcePath);
                if (!string.IsNullOrEmpty(resultValidateListDic))
                {
                    MessageBox.Show("Information missing following column in List_Dictionary table in selected file: " + Environment.NewLine + resultValidateListDic, "Error:");
                    return obj_NewVersionData;
                }

                string module = obj_MetaData.Module; //_objLibraryFunction_Service.GetLibrary_Module(obj_MetaData.Module);
                string country = _objLibraryFunction_Service.GetLibrary_CountryName(obj_MetaData.Country);
                int totalCount = CalculateTotalRecordsFromMDB(sourcePath);
                int totalUniqueSubstanceCount = CalculateUniqueSubstanceFromMDB(sourcePath);

                string destinationPath = Engine.CommonAccessFolderPath + module + "\\" + country;
                CommonGeneralFunction.CreateDirectoryPath(destinationPath);
                CommonGeneralFunction.GenerateFileToDestination(sourcePath, destinationPath + "\\" + fileName);
                bool IsUpdate = Update_QSIDAndPharseCategory(Qsid_ID, sourcePath);
                if (!IsUpdate)
                {
                    return obj_NewVersionData;
                }
                obj_NewVersionData = CommonGeneralFunction.BindModelVersionData(Qsid_ID, destinationPath + "\\" + fileName, originalFileName, currentVersion, obj_LastCheckOutData.CheckOut_TimeStamp, CommonGeneralFunction.ESTTime(), totalCount, totalUniqueSubstanceCount, Note, ProductDateTimeStamp);
                _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Insert(obj_NewVersionData);
                DeleteLastCheckoutTempAfterCheckIn(Qsid_ID);
                UpdateStatusLastCheckedOut(Qsid_ID, objLastVersionAccessData.Version);
                bool IsDeleted = CommonGeneralFunction.DeleteSourceFile(sourcePath);
                if (!IsDeleted)
                {
                    MessageBox.Show("Check-In completed, but could not delete the local copy in C:\\crawork\\versioncontrol as file is open", "Information:", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            return obj_NewVersionData;
        }

        //public Log_VersionControl_History CheckIn_FunctionalityDharmesh(int Qsid_ID, DateTime? ProductDateTimeStamp, string Note, int? wrkOrd, ref DataTable lstError)
        //{
        //    var sourcePath = string.Empty;
        //    try
        //    {
        //        Log_VersionControl_History obj_NewVersionData = null;
        //        Log_VersionControl_History objLastVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault(); // x.UserName == Environment.UserName &&
        //        Log_VersionControl_History objLastVersionAccessData1 = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID && x.ListVersionChangeTypeID == 4).FirstOrDefault();
        //        if (objLastVersionAccessData != null && objLastVersionAccessData1 == null)
        //        {
        //            string originalFileName = objLastVersionAccessData.FileName;
        //            string currentVersion = (Convert.ToInt32(objLastVersionAccessData.Version) + 1).ToString().PadLeft(5, '0');
        //            Log_VersionControl_History_Temp obj_LastCheckOutData = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.CheckOut_Version).FirstOrDefault();

        //            string fileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(objLastVersionAccessData.FileName, currentVersion);
        //            sourcePath = objLastVersionAccessData.FilePath;//Engine.CommonCheckoutFolderPath + fileName;
        //            //List_Dictionary_VM obj_MetaData = SqlClassRepository.DbaseQueryReturnObject<List_Dictionary_VM>("Select [LIST_FIELD_NAME], [QSID], [MODULE],[COUNTRY], [SHORT_NAME] as shortName, [SHORT_DESCRIPTION] as ShortDescription from [List_Dictionary]", sourcePath);
        //            //string resultValidateListDic = ValidateListDictionaryData(obj_MetaData, sourcePath);
        //            //if (!string.IsNullOrEmpty(resultValidateListDic))
        //            //{
        //            //    MessageBox.Show("Information missing following column in List_Dictionary table in selected file: " + Environment.NewLine + resultValidateListDic, "Error:");
        //            //    return obj_NewVersionData;
        //            //}

        //            //string module = obj_MetaData.Module; //_objLibraryFunction_Service.GetLibrary_Module(obj_MetaData.Module);
        //            //string country = _objLibraryFunction_Service.GetLibrary_CountryName(obj_MetaData.Country);
        //            //int totalCount = CalculateTotalRecordsFromMDB(sourcePath);
        //            //int totalUniqueSubstanceCount = CalculateUniqueSubstanceFromMDB(sourcePath);

        //            //string destinationPath = Engine.CommonAccessFolderPath + module + "\\" + country;
        //            string destinationPath = objLastVersionAccessData.FilePath.Substring(0, objLastVersionAccessData.FilePath.LastIndexOf('\\'));
        //            //CommonGeneralFunction.CreateDirectoryPath(destinationPath);
        //            //CommonGeneralFunction.GenerateFileToDestination(sourcePath, destinationPath + "\\" + fileName);
        //            //bool IsUpdate = Update_QSIDAndPharseCategory(Qsid_ID, sourcePath);
        //            //if (!IsUpdate)
        //            //{
        //            //    return obj_NewVersionData;
        //            //}
        //            Console.WriteLine(sourcePath + Environment.NewLine + destinationPath + "\\" + fileName);
        //            obj_NewVersionData = CommonGeneralFunction.BindModelVersionData(Qsid_ID, destinationPath + "\\" + fileName, originalFileName, currentVersion, obj_LastCheckOutData == null ? ProductDateTimeStamp : obj_LastCheckOutData.CheckOut_TimeStamp, CommonGeneralFunction.ESTTime(), 0, 0, Note, ProductDateTimeStamp, 4);

        //            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Insert(obj_NewVersionData);
        //            if (wrkOrd != null)
        //            {
        //                var lst = new Log_VersionControl_WorkOrders
        //                {
        //                    WO_Number = wrkOrd,
        //                    VersionControl_HistoryID = obj_NewVersionData.VersionControl_HistoryID,
        //                };
        //                _objGenericData_Service.AttachInstance<Log_VersionControl_WorkOrders>().Insert(lst);
        //            }
        //            DeleteLastCheckoutTempAfterCheckIn(Qsid_ID);
        //            UpdateStatusLastCheckedOut(Qsid_ID, objLastVersionAccessData.Version);
        //            //bool IsDeleted = CommonGeneralFunction.DeleteSourceFile(sourcePath);
        //            //if (!IsDeleted)
        //            //{
        //            //    MessageBox.Show("Check-In completed, but could not delete the local copy in C:\\crawork\\versioncontrol as file is open", "Information:", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            //}
        //        }
        //        return obj_NewVersionData;
        //    }
        //    catch (Exception ex)
        //    {
        //        var newRow = lstError.NewRow();
        //        newRow[0] = Qsid_ID;
        //        newRow[1] = sourcePath;
        //        newRow[2] = ex.Message;
        //        lstError.Rows.Add(newRow);
        //        return null;
        //    }

        //}
        public void UpdateStatusLastCheckedOut(int qsid_ID, string version)
        {
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == qsid_ID && x.Version == version).FirstOrDefault();
            objVersionAccessData.Currently_Checked_Out = false;
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Update(objVersionAccessData);
        }
        public int CalculateUniqueSubstanceFromMDB(string sourcePath)
        {
            string resultUniqueSubstance = SqlClassRepository.DbaseQueryReturnString("SELECT count(*) as UniqueSubstanceCount from ( SELECT cas from Data where omit is null group by cas,editorial)", sourcePath);
            int count = 0;
            if (!String.IsNullOrEmpty(resultUniqueSubstance))
            {
                Int32.TryParse(resultUniqueSubstance, out count);
            }
            return count;

        }
        public int CalculateTotalRecordsFromMDB(string sourcePath)
        {
            string resultTotalRecords = SqlClassRepository.DbaseQueryReturnString("SELECT count(*) as TotalCount from data where omit is null", sourcePath);// SELECT count(*) as TotalCount from data where omit is null
            int count = 0;
            if (!String.IsNullOrEmpty(resultTotalRecords))
            {
                Int32.TryParse(resultTotalRecords, out count);
            }
            return count;
        }

        public void UpdateUnlockLastCheckOutQsidList(int Qsid_ID)
        {
            DeleteLastCheckoutTemp(Qsid_ID);
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();// x.UserName == Environment.UserName &&
            //objVersionAccessData.IsLocked = false;
            objVersionAccessData.Currently_Checked_Out = false;
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Update(objVersionAccessData);
        }

        public void DeleteLastCheckoutTemp(int Qsid_ID)
        {
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            string increVersion = IncrementVersion(objVersionAccessData.Version);
            var checkoutVersionTemp = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID && x.CheckOut_Version == increVersion && x.UserName == Environment.UserName).FirstOrDefault();// x.UserName == Environment.UserName &&
            if (checkoutVersionTemp != null)
            {
                _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Delete(checkoutVersionTemp);
            }
        }
        public void DeleteLastCheckoutTempAfterCheckIn(int Qsid_ID)
        {
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            var checkoutVersionTemp = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == Qsid_ID && x.CheckOut_Version == objVersionAccessData.Version && x.UserName == Environment.UserName).FirstOrDefault();// x.UserName == Environment.UserName &&
            if (checkoutVersionTemp != null)
            {
                _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Delete(checkoutVersionTemp);
            }
        }

        public string GetLatestVersionCopyOfQSID(int Qsid_ID, long versionId)
        {
            string resultDestinationPath = string.Empty;
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == Qsid_ID && x.VersionControl_HistoryID == versionId).OrderByDescending(x => x.Version).FirstOrDefault();//x.UserName == Environment.UserName &&
            if (objVersionAccessData != null)
            {
                if (File.Exists(objVersionAccessData.FilePath))
                {
                    string currentVersion = objVersionAccessData.Version;
                    string FileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(objVersionAccessData.FileName, currentVersion);
                    resultDestinationPath = Engine.CommonCheckoutFolderPath + "Get_" + FileName;
                    //string sourcePath = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.FilePath != null).OrderByDescending(x => x.DateFirstAdded).Select(x => x.FilePath).FirstOrDefault();
                    CommonGeneralFunction.CreateDirectoryPath(Engine.CommonCheckoutFolderPath);
                    CommonGeneralFunction.GenerateFileToDestination(objVersionAccessData.FilePath, resultDestinationPath);
                }
                else
                {
                    MessageBox.Show("File not found on below path." + Environment.NewLine + objVersionAccessData.FilePath, "Error:");
                }
            }
            return resultDestinationPath;
        }

        public void InsertUpdate_Version_TypeChange(int? versionControl_HistoryID, int ListVersionChangeTypeID, List<EntityClass.Log_VersionControl_EditorialChangeType> listEditorialChangesVM, List<EntityClass.Log_VersionControl_WorkOrders> listWorkOrderVM, List<EntityClass.Log_VersionControl_CSCTickets> listTicketsVM)
        {
            UpdateChangeTypeHeaderInVersionControlHistory(versionControl_HistoryID, ListVersionChangeTypeID);

            _objLibraryFunction_Service.AddUpdateLog_VersionControl_ListEditorialChangeTypes(listEditorialChangesVM, versionControl_HistoryID);
            _objLibraryFunction_Service.AddUpdateLog_VersionControl_WorkOrders(listWorkOrderVM, versionControl_HistoryID);
            _objLibraryFunction_Service.AddUpdateLog_VersionControl_Tickets(listTicketsVM, versionControl_HistoryID);
        }

        private void UpdateChangeTypeHeaderInVersionControlHistory(int? versionControl_HistoryID, int ListVersionChangeTypeID)
        {
            Log_VersionControl_History objVersionAccessData = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.VersionControl_HistoryID == versionControl_HistoryID).FirstOrDefault();
            objVersionAccessData.ListVersionChangeTypeID = ListVersionChangeTypeID;
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Update(objVersionAccessData);
        }


        public void ExportAccessData(DataTable list_CasId, string fileName, string TableName, string[] paramterName, string[] paramterVal)
        {
            SqlClassRepository.InserttoDatabase(paramterName, paramterVal, paramterVal, TableName, list_CasId, fileName);
        }
        public List<Map_VersionControl_Permissions> GetListMap_VersionControl_Permissions()
        {
            return _objGenericData_Service.AttachInstance<Map_VersionControl_Permissions>().Get(null, null, "Library_Roles");
        }
        public bool InsertUpdateMap_VersionControl_Permissions(List<Map_VersionControl_Permissions> listRolePermInsertUpdate)
        {
            bool IsResult = false;
            try
            {
                List<Map_VersionControl_Permissions> updateList = listRolePermInsertUpdate.Where(x => x.VersionControl_PermissionId != 0).ToList();
                if (updateList.Count > 0)
                {
                    foreach (var itemList in updateList)
                    {
                        _objGenericData_Service.AttachInstance<Map_VersionControl_Permissions>().Update(itemList);
                    }
                    //_objGenericData_Service.AttachInstance<Map_VersionControl_Permissions>().UpdateBulk(updateList, y => y.IsAddProductTimeStamp, y => y.IsCheckOutOrIN, y => y.IsGetCopy, y => y.IsCompareList, y => y.IsSubstanceSearch, y => y.IsViewAdmin, y => y.RoleId);
                }

                var newlistPermission = listRolePermInsertUpdate.Where(x => x.VersionControl_PermissionId == 0).ToList();
                if (newlistPermission.Count > 0)
                {
                    _objGenericData_Service.AttachInstance<Map_VersionControl_Permissions>().InsertBulk(newlistPermission);
                }
                IsResult = true;
            }
            catch (Exception ex)
            {
                IsResult = false;
            }
            return IsResult;

        }

        public void RollbackCheckInProcess(Log_VersionControl_History selectedQSID_Version)
        {
            _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Delete(selectedQSID_Version);
        }
        public DateTime? GetProductDateTimeStampOfPreviousVersion(Access_QSID_Data_VM objVersionHis, bool flagIsTypeChange)
        {
            DateTime? dtresult = null;
            if (flagIsTypeChange)
            {
                var listLogVersionHistory = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == objVersionHis.QSID_ID && x.ProductTimeStamp != null);
                dtresult = listLogVersionHistory.OrderByDescending(x => x.Version).Select(x => x.ProductTimeStamp).FirstOrDefault();
            }
            else
            {
                var listLogVersionHistory = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == objVersionHis.QSID_ID && x.ProductTimeStamp != null && x.Version != objVersionHis.Version);
                dtresult = listLogVersionHistory.OrderByDescending(x => x.Version).Select(x => x.ProductTimeStamp).FirstOrDefault();
            }
            if (dtresult != null)
            {
                dtresult.Value.AddDays(1);
            }
            return dtresult;
        }

        public string ValidateListDictionaryData(List_Dictionary_VM objListDict_VM, string AccessDBPath)
        {

            string resultFlag = string.Empty;
            if (string.IsNullOrEmpty(objListDict_VM.QSID))
            {
                resultFlag = "QSID, ";
            }
            else
            {
                var checkQsid = _objLibraryFunction_Service.ChkLibraryQsid(objListDict_VM.QSID, Engine.CurrentUserSessionID, AccessDBPath, CommonGeneralFunction.ESTTime());
                if (!string.IsNullOrEmpty(checkQsid.Item1) || checkQsid.Item3 == 0)
                {
                    return checkQsid.Item1;
                }

            }
            if (string.IsNullOrEmpty(objListDict_VM.List_Field_Name)) { resultFlag += "List_Field_Name,"; }
            //if (string.IsNullOrEmpty(objListDict_VM.LongName)) { resultFlag += "LongName,"; }
            if (string.IsNullOrEmpty(objListDict_VM.Module)) { resultFlag += "Module,"; }
            if (string.IsNullOrEmpty(objListDict_VM.ShortDescription)) { resultFlag += "ShortDescription,"; }
            if (string.IsNullOrEmpty(objListDict_VM.ShortName)) { resultFlag += "ShortName,"; }
            if (objListDict_VM.Country == 0) { resultFlag += "Country,"; }
            return resultFlag.Trim(',');
        }

        public string GetAlreadyCheckOutUserByQSID(int qSID_ID)
        {
            string objUserName = string.Empty;

            var objVersionHistory = _objGenericData_Service.AttachInstance<Log_VersionControl_History>().Get(x => x.QSID_ID == qSID_ID).OrderByDescending(x => x.Version).FirstOrDefault();
            string increVersion = IncrementVersion(objVersionHistory.Version);
            var objVersionHisCheckout = _objGenericData_Service.AttachInstance<Log_VersionControl_History_Temp>().Get(x => x.QSID_ID == qSID_ID && x.CheckOut_Version == increVersion).FirstOrDefault();
            if (objVersionHisCheckout != null)
            {
                objUserName = "Checked Out By: " + _objLibraryFunction_Service.GetLibrary_UserName(objVersionHisCheckout.UserName);
            }
            return objUserName;
        }

        public List<int> GetOnlyCheckedOutQSIDList(DateTime? CheckedStartDate, DateTime? CheckedEndDate)
        {
            List<int> listCheckedOut = new List<int>();
            listCheckedOut = _objLibraryFunction_Service.GetCheckedOutQsid_IDList(CheckedStartDate, CheckedEndDate);
            return listCheckedOut;
        }

        //public Library_VersionControl_ChangeTypes GetChangeTypeHeaderVersionHistory(int versionControlHisId)
        //{
        //    return _objGenericData_Service.AttachInstance<Library_VersionControl_ChangeTypes>().Get(x => x.VersionControl_HistoryID == versionControlHisId).FirstOrDefault();
        //}
        //public List<Library_VersionControl_ChangeTypes> GetChangeTypeVersionHistory(int versionControlHisId)
        //{
        //    return _objGenericData_Service.AttachInstance<Library_VersionControl_ChangeTypes>().Get(x => x.VersionControl_HistoryID == versionControlHisId).ToList();
        //}
        //public void DeleteExistingVersion_TypeChange(int? versionControlHisId)
        //{
        //    var objListTypeChangeHistory = _objGenericData_Service.AttachInstance<Library_VersionControl_ChangeTypes>().Get(x => x.VersionControl_HistoryID == versionControlHisId).ToList();
        //    _objGenericData_Service.AttachInstance<Library_VersionControl_ChangeTypes>().DeleteBulk(objListTypeChangeHistory);
        //}
        public void UpdateMapProductList(List<MapProductList_VM> list, int qsid_ID, int SessionID)
        {
            _objLibraryFunction_Service.Add_Map_ProductLists(list, qsid_ID, SessionID);
        }
        public void UpdateParentChildList(List<MapParentChildList_VM> list, int qsid_ID, int SessionID)
        {
            _objLibraryFunction_Service.Add_Map_ParentChildLists(list, qsid_ID, SessionID);
        }
        public List<EntityClass.Library_ListVersionChangeTypes> GetLibrary_ListVersionChangeTypes()
        {
            return _objLibraryFunction_Service.GetLibrary_ListVersionChangeTypes();
        }
        public List<EntityClass.Library_ListEditorialChangeTypes> GetLibrary_ListEditorialChangeTypes()
        {
            return _objLibraryFunction_Service.GetLibrary_ListEditorialChangeTypes();
        }
        public List<EntityClass.Log_VersionControl_EditorialChangeType> GetLog_VersionControl_ListEditorialChangeTypes(int? VersionControlHistoryID)
        {
            return _objLibraryFunction_Service.GetLog_VersionControl_ListEditorialChangeTypes(VersionControlHistoryID);
        }
        public List<EntityClass.Log_VersionControl_WorkOrders> GetLog_VersionControl_WorkOrders(int? VersionControlHistoryID)
        {
            return _objLibraryFunction_Service.GetLog_VersionControl_WorkOrders(VersionControlHistoryID);
        }

        public string GetCurrentCheckedOutFilePath(string version, string fileName)
        {
            string updateFileName = string.Empty;
            string incrementedVersion = IncrementVersion(version);
            updateFileName = CommonGeneralFunction.GetFileNameWithCurrentVersion(fileName, incrementedVersion);
            return updateFileName;
        }
        public bool InsertUpdate_MapEasiProListId(List<EntityClass.Map_QSID_ListID> listWorkOrderEntity, int qsid_Id, int currentUserSessionID)
        {
            return _objLibraryFunction_Service.InsertUpdate_MapEasiProListId(listWorkOrderEntity, qsid_Id, currentUserSessionID);
        }
        public List<EntityClass.Map_QSID_ListID> GetListEasiProListID(int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListEasiProListID(qsid_ID);
        }
        public void UpdateMapVerticalList(List<MapVerticalList_VM> list, int qsid_ID, int currentUserSessionID)
        {
            _objLibraryFunction_Service.Add_Map_VerticalLists(list, qsid_ID, currentUserSessionID);
        }
        public bool InsertUpdate_MapERC_EHSListCode(List<EntityClass.Map_QSID_ERC_EHSListCode> listEHSListCodeEntity, int qsid_ID, int currentUserSessionID)
        {
            return _objLibraryFunction_Service.InsertUpdate_MapERC_EHSListCode(listEHSListCodeEntity, qsid_ID, currentUserSessionID);
        }
        public List<EntityClass.Map_QSID_ERC_EHSListCode> GetListERC_EHSListCode(int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListERC_EHS_ListCode(qsid_ID);
        }
        public void AddQsidNoteUrl(int qSID_ID, string listNoteUrl)
        {
            _objLibraryFunction_Service.InsertUpdateNoteUrlQsid(listNoteUrl, qSID_ID);
        }
        public List<ListChangesReportQsid_Field_VM> GetAddedQsidFieldBySessionID(int sessionID, int qsid_ID)
        {
            return _objLibraryFunction_Service.GetAddedQsidFieldBySessionID(qsid_ID, sessionID);
        }
        public List<ListChangesReportQsid_DeleteField_VM> GetDeletedQsidFieldBySessionID(int sessionID, int qsid_ID)
        {
            return _objLibraryFunction_Service.GetDeletedQsidFieldBySessionID(qsid_ID, sessionID);
        }
        public List<Log_ListDictionaryChanges_VM> GetListDictionaryChanges(int sessionId, int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListDictionaryChanges(qsid_ID, sessionId);
        }
        public List<Log_ListNOL_VM> GetListNOL(int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListNOL(qsid_ID);
        }

        public List<Log_ListFieldExplainChanges_VM> GetListFieldExplainChanges(int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListFieldExplainChanges(qsid_ID);
        }

        public List<Log_ListFieldExplainChanges_VM> GetListFieldExplainChanges(int sessionId, int qsid_ID)
        {
            return _objLibraryFunction_Service.GetListFieldExplainChanges(sessionId, qsid_ID);
        }

        public List<ListChangesReportQsid_Field_VM> GetAddedQsidFieldbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax)
        {
            return _objLibraryFunction_Service.GetAddedQsidFieldbyProductDateTimestamp(qsid_ID, productDateMin, productDateMax);
        }
        public List<ListChangesReportQsid_DeleteField_VM> GetDeletedQsidFieldbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax)
        {
            return _objLibraryFunction_Service.GetDeletedQsidFieldbyProductDateTimestamp(qsid_ID, productDateMin, productDateMax);
        }
        public List<Log_ListDictionaryChanges_VM> GetListDictionaryChangesbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax)
        {
            return _objLibraryFunction_Service.GetListDictionaryChangesbyProductDateTimestamp(qsid_ID, productDateMin, productDateMax);
        }
        public List<int> GetSearchWorkOrderAndNote(string searchTypeFilter, string text, string inputTextSearch)
        {
            List<int> list = _objLibraryFunction_Service.GetSearchWorkOrderAndNote(searchTypeFilter, text, inputTextSearch);
            return list;
        }

        public List<DateSearchTab_VM> GetDateSearch(DateTime? StartDate, DateTime? Enddate)
        {
            return CommonGeneralFunction.MapDateSearchBusinessToView(_objLibraryFunction_Service.DateSearchQuery(StartDate, Enddate));
        }
        public List<CasSearchSubstanceTab_VM> GetCASDataSubstanceSearch(string productIds, string casSearch, string text)
        {
            return CommonGeneralFunction.MapCasSearchSunbstanceBusinessToView(_objLibraryFunction_Service.GetCASDataSubstanceSearch(productIds, casSearch, text));
        }
        public List<SubstanceSearchTab_VM> GetChemDataSubstanceSearch(string productIds, string chemicalName, string text)
        {
            return CommonGeneralFunction.MapSubstanceSearchBusinessToView(_objLibraryFunction_Service.GetChemDataSubstanceSearch(productIds, chemicalName, text));
        }

        public LibraryInformation_QSID_VM GetQsidDetailInfoByQsid_ID(int qsid_Id)
        {
            return _objLibraryFunction_Service.GetDetailQsidbyQsid_ID(qsid_Id);
        }
        public List<SubstanceSearchIdentifier_VM> GetListIdentifierDetail(int? identifierTypeId, string txtSearchIdentifier, string searchCond, string productIds)
        {
            return CommonGeneralFunction.MapIdentifierSearchBusinessToView(_objLibraryFunction_Service.GetListIdentifierDetail(identifierTypeId, txtSearchIdentifier, searchCond, productIds));
        }
        public List<SubstanceSearchPhrase_VM> GetListPhraseDetail(int? phraseCateId, string txtSearchPhrase, string searchCond, string productIds)
        {
            return CommonGeneralFunction.MapPhraseSearchBusinessToView(_objLibraryFunction_Service.GetListPhraseDetail(phraseCateId, txtSearchPhrase, searchCond, productIds));
        }
        public List<FieldNameSearch_VM> SearchByFieldName(string FieldName, string SearchFieldNameType, string searchType, string productIds)
        {
            return _objLibraryFunction_Service.SearchByFieldName(FieldName, SearchFieldNameType, searchType, productIds);
        }
        public bool CheckProductEchaByQsid(int qsid_id)
        {
            return _objLibraryFunction_Service.CheckProductEchaByQsid(qsid_id);
        }
        public List<EntityClass.Log_VersionControl_CSCTickets> GetLog_VersionControl_Tickets(int? VersionControlHistoryID)
        {
            return _objLibraryFunction_Service.GetLog_VersionControl_Tickets(VersionControlHistoryID);
        }

        public void DownloadAndShowDocument()
        {
            string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string pathDownload = Path.Combine(pathUser, "Downloads\\");
            var fileName = _objLibraryFunction_Service.GetdocumentFileName(Engine.CurrentSelectedTabItem);
            if (string.IsNullOrEmpty(fileName) == false)
            {
                File.Copy(Engine.CommonDocumentPath + fileName, pathDownload + fileName, true);
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(pathDownload + fileName)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            else
            {
                System.Windows.MessageBox.Show("No document found", "Information", System.Windows.MessageBoxButton.OK);
            }
        }

        public bool IsAttachmentAvaliable(string obj)
        {
            var fileName = _objLibraryFunction_Service.GetdocumentFileName(obj);
            return (string.IsNullOrEmpty(fileName) == false) ? true : false;
        }

        public void AttachDocument()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = @"All Files|*.*|Text File (.txt)|*.txt|Word File (.docx ,.doc)|*.docx;*.doc|PDF (.pdf)|*.pdf|Spreadsheet (.xls ,.xlsx)|  *.xls ;*.xlsx|Presentation (.pptx ,.ppt)|*.pptx;*.ppt";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var fileName = System.IO.Path.GetFileName(openFileDialog.FileName);
                var fileNamePrev = _objLibraryFunction_Service.GetdocumentFileName(Engine.CurrentSelectedTabItem);
                if (string.IsNullOrEmpty(fileNamePrev) == false)
                {
                    var result1 = MessageBox.Show("Document Already exist, Do you want overwrite", "Confirmation", MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        var chkSameFileForAnotherTab = _objLibraryFunction_Service.SameFileForAnotherTab(Engine.CurrentSelectedTabItem, fileNamePrev);
                        if (chkSameFileForAnotherTab)
                        {
                            File.Copy(Engine.CommonDocumentPath + fileNamePrev, Engine.PrevDocumentPath + fileNamePrev + "_" + Environment.UserName + "_" + DateTime.Now.ToString("dd-MMM-yyyyhhmmss"), true);
                        }
                        else
                        {
                            File.Move(Engine.CommonDocumentPath + fileNamePrev, Engine.PrevDocumentPath + fileNamePrev + "_" + Environment.UserName + "_" + DateTime.Now.ToString("dd-MMM-yyyyhhmmss"), true);
                        }
                        File.Copy(openFileDialog.FileName, Engine.CommonDocumentPath + fileName, true);
                        _objLibraryFunction_Service.SetdocumentFileName(Engine.CurrentSelectedTabItem, fileName, Engine.CommonDocumentPath + fileName);
                        System.Windows.MessageBox.Show("File attached Successfully ", "Information");
                    }
                }
                else
                {
                    File.Copy(openFileDialog.FileName, Engine.CommonDocumentPath + fileName, true);
                    _objLibraryFunction_Service.SetdocumentFileName(Engine.CurrentSelectedTabItem, fileName, Engine.CommonDocumentPath + fileName);
                    System.Windows.MessageBox.Show("File attached Successfully ", "Information");
                }

            }
        }
    }
}
