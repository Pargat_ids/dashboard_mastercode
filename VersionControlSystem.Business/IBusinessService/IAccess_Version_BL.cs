﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Business.IBusinessService
{
    public interface IAccess_Version_BL
    {
        void UpdateAccessversionData(Access_QSID_Data_VM updatedAccess_Version_Data);
        Dictionary<string, Model.DataModel.Log_VersionControl_History> AddNewQSIDList(string SelectedPath, string listfileName);
        List<Access_QSID_Data_VM> GetList_VersionHistory(int? Qsid_ID, DateTime? FromProduction, DateTime? ToProduction);
        //List<Access_QSID_Data_VM> GetList_VersionAccess(int? Qsid_ID, DateTime? FromProduction, DateTime? ToProduction);
        List<QSID_VM> GetAllQSID_VM();
        bool CheckOutFunctionality(int? Qsid_ID);
        string IncrementVersion(string version);
        Model.DataModel.Log_VersionControl_History CheckIn_Functionality(int Qsid_ID, DateTime? ProductDateTimeStamp, string Note);
        //Model.DataModel.Log_VersionControl_History CheckIn_FunctionalityDharmesh(int Qsid_ID, DateTime? ProductDateTimeStamp, string Note, int? wrkOrd, ref DataTable lstError);
        bool CheckActiveCheckOutButton(int qsid);
        bool CheckUnlockForCurrentUser(int qsid);
        bool CheckActiveCheckInButton(int qsid);
        void UpdateUnlockLastCheckOutQsidList(int qsid_ID);
        string GetLatestVersionCopyOfQSID(int Qsid_ID, long versionId);
        bool CheckGetCopyQsidForCurrentUser(int qsid);
        //void InsertVersion_TypeChange(List<Library_VersionControl_ChangeTypes> listTypeChange);
        List<Model.DataModel.Log_VersionControl_History> GetAllVersionHistoryByProductionDate(DateTime? FromProduction, DateTime? ToProduction);
        void ExportAccessData(DataTable list_CasId, string fileName, string TableName, string[] paramterName, string[] paramterVal);
        List<Model.DataModel.Map_VersionControl_Permissions> GetListMap_VersionControl_Permissions();
        bool InsertUpdateMap_VersionControl_Permissions(List<Model.DataModel.Map_VersionControl_Permissions> listRolePermInsertUpdate);
        void RollbackCheckInProcess(Model.DataModel.Log_VersionControl_History selectedQSID_Version);
        DateTime? GetProductDateTimeStampOfPreviousVersion(Access_QSID_Data_VM select_Access_QSID_Data_VM, bool flagIsTypeChange);
        string GetAlreadyCheckOutUserByQSID(int qSID_ID);
        List<int> GetOnlyCheckedOutQSIDList(DateTime? CheckedStartDate, DateTime? CheckedEndDate);
        bool CheckEnableForEditVersionHistoryInfo(int qSID_ID, string version, string flagType);
        //Library_VersionControl_ChangeTypes GetChangeTypeHeaderVersionHistory(int versionControlHisId);
        //List<Library_VersionControl_ChangeTypes> GetChangeTypeVersionHistory(int versionControlHisId);
        //  void DeleteExistingVersion_TypeChange(int? versionControlHisId);
        void UpdateMapProductList(List<MapProductList_VM> list, int qsid_ID, int SessionID);
        void UpdateParentChildList(List<MapParentChildList_VM> list, int qsid_ID, int SessionID);
        List<EntityClass.Library_ListVersionChangeTypes> GetLibrary_ListVersionChangeTypes();
        List<EntityClass.Library_ListEditorialChangeTypes> GetLibrary_ListEditorialChangeTypes();
        void InsertUpdate_Version_TypeChange(int? versionControl_HistoryID, int ListVersionChangeTypeID, List<EntityClass.Log_VersionControl_EditorialChangeType> listEditorialChangesVM, List<EntityClass.Log_VersionControl_WorkOrders> listWorkOrderVM, List<EntityClass.Log_VersionControl_CSCTickets> listTicketsVM);
        List<EntityClass.Log_VersionControl_EditorialChangeType> GetLog_VersionControl_ListEditorialChangeTypes(int? VersionControlHistoryID);
        List<EntityClass.Log_VersionControl_WorkOrders> GetLog_VersionControl_WorkOrders(int? VersionControlHistoryID);
        string GetCurrentCheckedOutFilePath(string version, string fileName);
        bool InsertUpdate_MapEasiProListId(List<Map_QSID_ListID> listWorkOrderEntity, int qsid_ID, int currentUserSessionID);
        List<EntityClass.Map_QSID_ListID> GetListEasiProListID(int qsid_ID);
        void UpdateMapVerticalList(List<MapVerticalList_VM> list, int qsid_ID, int currentUserSessionID);
        bool InsertUpdate_MapERC_EHSListCode(List<Map_QSID_ERC_EHSListCode> listEHSListCodeEntity, int qsid_ID, int currentUserSessionID);
        List<EntityClass.Map_QSID_ERC_EHSListCode> GetListERC_EHSListCode(int qsid_ID);
        void AddQsidNoteUrl(int qSID_ID, string listNoteUrl);
        List<ListChangesReportQsid_Field_VM> GetAddedQsidFieldBySessionID(int qsid_ID, int sessionID);
        List<ListChangesReportQsid_DeleteField_VM> GetDeletedQsidFieldBySessionID(int qsid_ID, int sessionID);
        List<Log_ListDictionaryChanges_VM> GetListDictionaryChanges(int sessionId, int qsid_ID);
        List<Log_ListNOL_VM> GetListNOL(int qsid_ID);
        List<Log_ListFieldExplainChanges_VM> GetListFieldExplainChanges(int qsid_ID);
        List<Log_ListFieldExplainChanges_VM> GetListFieldExplainChanges(int sessionId, int qsid_ID);

        List<ListChangesReportQsid_Field_VM> GetAddedQsidFieldbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax);
        List<ListChangesReportQsid_DeleteField_VM> GetDeletedQsidFieldbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax);
        List<Log_ListDictionaryChanges_VM> GetListDictionaryChangesbyProductDateTimestamp(int qsid_ID, DateTime? productDateMin, DateTime? productDateMax);
        List<int> GetSearchWorkOrderAndNote(string searchTypeFilter,string text, string inputTextSearch);
        List<DateSearchTab_VM> GetDateSearch(DateTime? StartDate, DateTime? Enddate);
        List<CasSearchSubstanceTab_VM> GetCASDataSubstanceSearch(string productIds, string casSearch, string text);
        List<SubstanceSearchTab_VM> GetChemDataSubstanceSearch(string productIds, string chemicalName, string text);
        LibraryInformation_QSID_VM GetQsidDetailInfoByQsid_ID(int qsid_Id);
        List<SubstanceSearchIdentifier_VM> GetListIdentifierDetail(int? identifierTypeId, string txtSearchIdentifier, string searchCond, string productIds);
        List<SubstanceSearchPhrase_VM> GetListPhraseDetail(int? phraseCateId, string txtSearchPhrase, string text, string productIds);
        List<FieldNameSearch_VM> SearchByFieldName(string FieldName, string SearchFieldNameType, string searchType,string productIds);
        string CheckOutFileName(int? Qsid_ID);
        string CheckInFileName(int? Qsid_ID);
        bool CheckProductEchaByQsid(int qsid_id);
        List<EntityClass.Log_VersionControl_CSCTickets> GetLog_VersionControl_Tickets(int? VersionControlHistoryID);
        void AttachDocument();
        void DownloadAndShowDocument();
        bool IsAttachmentAvaliable(string obj);
    }
}
