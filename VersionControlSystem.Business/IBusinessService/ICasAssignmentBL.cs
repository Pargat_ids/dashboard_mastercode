﻿using System;
using System.Collections.Generic;
using CRA_DataAccess.ViewModel;

namespace VersionControlSystem.Business.IBusinessService
{
    public interface ICasAssignmentBL
    {
        IEnumerable<string> GetQsidListByCasID(int casId, bool isFoodData, bool isNonFoodData,string chemicalName);
        bool UpdateAccessData(List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM> listUpdateCas, string SelectedMdbPath, string userName, string qsid, string tableName);
        Dictionary<string, List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM>> GetChemicalNameNoCasFromAccess(string path, string qsid, bool IsFoodData, bool IsNonFoodData, string tableName);
        Dictionary<string, int> GetNextSeqCasByModule(string moduleCode, int countNoCasSeq);
        List<CasAssignmentHistory> GetCasAssignmentHistory(string selectedMdbQSID);
        List<string> GetTableNameFromAccess(string selectedMdbPath);
        bool ValidateDefaultAccessTable(string tableName, string selectedMdbPath);
        List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetMICNextSeedList(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedMIC);
        List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> GetINGNextSeedList(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedING);
        void UpdateINGNextSeed(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedING);
        void UpdateMICNextSeed(List<VersionControlSystem.Model.ViewModel.Cas_MIC_ING_Seed> listSelectedING);
        Dictionary<string, List<CRA_DataAccess.ViewModel.CasAssignChemicalNamesVM>> GetChemicalNameExistingCasFromAccess(string path, string tableName, bool isFoodData, bool isNonFoodData);
        List<EntityClass.Library_SubstanceType> GetLibrarySubstanceType();
        List<EntityClass.Library_SubstanceCategories> GetLibrarySubstanceCategory();
        List<Map_Incorrect_Cas_ChemicalName_VM> GetMap_Incorrect_Cas_ChemicalName();
        //bool DeleteMap_Incorrect_Cas_ChemicalName(int mapCasNameId);
        List<string> FetchOtherCasByChemicalName(string chemicalName, string casAssigned);
        List<string> FetchOtherChemicalNameByCas(string chemicalName, string casAssigned);
        List<string> FetchOtherQsidByChemicalNameAndCas(string chemicalName, string casAssigned);
        string GetDelimiterByQsid(string qsid);
        bool InsertUpdateIsIncorrectCasName(string chemicalName, string delimiterForQsidCasAssignmentTool, string cas, int v, int currentUserSessionID);
        bool DeleteMap_Incorrect_Cas_ChemicalName(string chemicalName, string delimiterForQsidCasAssignmentTool, string cas, int currentUserSessionID);
        List<string> GetActiveListQsid(int? chemicalNameId, int? casID);
        List<string> GetCasChildrenList(string casAssigned, bool isFoodData, bool isNonFoodData);
        List<string> GetCasAlternateList(string casAssigned);
        int AddProcessTimeTaken_CasAssignment(string tabName, string processDesc, DateTime? startTime, DateTime? endTime, string timeTaken, decimal? startMem, decimal? endMem, int qsidId, int sessId);
        void UpdateProcessTimeTaken_CasAssignment(int processId, DateTime? endTime, string timeTaken);
        List<Log_Map_Incorrect_Cas_ChemicalName_VM> GetLog_Map_Incorrect_Cas_ChemicalName();
        Response_VM CreateMapIncorrectCasChemicalNameWithComments(string chemicalName, string cas, string comment, int SessionID);
        bool DeleteMap_Incorrect_Cas_ChemicalName(int chemicalNameID, int casID, int SessionID, string comment);
        bool ValidateNextSeq(string seqCas);
    }
}
