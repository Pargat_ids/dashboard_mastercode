﻿using EntityClass;
using System;
using System.Collections.Generic;


namespace VersionControlSystem.Business.IBusinessService
{
    public interface ILogUserSessionBL
    {
        void UpdateSessionForVersionControl(int sessionID);
        Log_Sessions CreateNewSessionForVersionControl(string username);
        string GetLibrary_UserName(string username);
        Map_VersionControl_Permissions GetCurrentUserRolePermission(string username);
        void LoginError(List<Log_ErrorTracking> listLogErrorTrack);
        bool CheckExistingInstanceIsOpen(string username);
        bool ValidateUserIDForCraDashboard(string username);
        string ErrorLogging(Exception ex);
    }
}
