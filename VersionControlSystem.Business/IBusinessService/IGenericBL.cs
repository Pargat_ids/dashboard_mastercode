﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Business.IBusinessService
{
    public interface IGenericBL
    {
       DataTable GetListGenericSuggesionImport_Access(string path);
        List<Response_VM>  InsertGenericSuggestion(List<GenericSuggestionVM> listGenericSuggestionVM, int CurrentSessionID,string tab);
        List<GenericSuggestionVM> GetListGenericSuggestionForConfirm();
        void InsertConfirmedGenericSuggestion(List<GenericSuggestionVM> listGenericSuggestionVM);
        //List<GenericTreeView> GetGenericManagementTree(int? parentCasId, string searchKeyword, string searchType);
        GenericSuggestionVM GetListGenericSuggestionByParentAndChildCasID(int? parentCasID, int? ChildCasID,string status);
        bool ApproveGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM,string tabType);
        bool RejectGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM,string tabType);
        void RemoveGenericSuggestion(GenericSuggestionVM objGenericSuggestionTree_VM);
        List<GenericSuggestionComments_VM> GetListGenericsComments(int? parentCasID, int? ChildCasID);
        int InsertAdditionalComment(GenericSuggestionVM objGenericComment, string type);
        List<FilterGenericSuggestion> SearchGenericSuggestionCasList(string searchKeyword, string searchFor, string searchType, string matchCondition);
        //List<GenericChemicalName_VM> GetListChemicalNameForGenerics();
        void SaveParentChildCasName(GenericParentChildCasName_VM objGenericParentChildCasName);
        bool UpdateIsExcludeInGenericNested(GenericSuggestionVM objGenericSuggestionTree_VM);
        //List<GenericSuggestionVM> GetParentListSearchChildCas(int childCasID,string status);
        List<Map_Hydrates_VM> GetListAllMapHydrates();
        List<Map_Hydrates_VM> GetListAllMapHydratesSingleCAS(string casname);
        Response_VM AddNewMap_Hydrates(Map_Hydrates_VM map_Hydrates_VM);
        bool DeleteMap_Hydrate(int hydrateID, int currentUserSessionID,string comment);
        int? GetChildChemicalNameIDByCas(string cas);
        int? GetParentChemicalNameIDByCas(string cas);
        List<GenericChemicalName_VM> FilterChemicalNameForGeneric(string name, int? chemID = 0);
        List<MapProductList_VM> GetAllLibraryProductLists();
        List<Library_QSIDs> GetActiveQsidsListDetailByCasID(int childCasID);
        List<GenericeNestedParent> GetGenericNestedParent(int childCasID,string casXPath,string searchType);
        Library_CAS GetLibraryCasByCas(string cas);
        List<Library_GenericsCategories> GetAllGenericsCategories();
        List<Library_SubstanceType> GetAllSubstanceType();
        List<Library_SubstanceCategories> GetAllSubstanceCategories();
        int Get_Map_Generic_Detail_IDs(string type, int casID);
        bool UpdateSubstanceInfo(Map_Generic_Detail objMapGenericDetail, string cas, int currentSessionID);
        bool ValidateIsParentSearch(int casID);
        List<GenericSuggestionComments_VM> GetListGenericsParentComments(int? parentCasID);
        List<Map_AlternateCas_VM> GetListAllMapAlternateCas();
        List<Map_AlternateCas_VM> GetListAllMapAlternateCasSingleCAS(string casname);
        Response_VM AddNewMap_AlternateCas(Map_AlternateCas_VM map_AlternateCas_VM);
        bool DeleteMap_AlternateCas(int alternateID, int currentUserSessionID,string comment);
        bool CheckCasValid(string cas);
        string RemoveStripCharcterFromCas(string cas);
        List<Map_Generic_Flat_VM> GetAllFlatGenerics(string cas, string selectType);
        List<Map_Generic_Flat_VM> GetAllFlatGenericsByQsid(string qsid,string selectType);
        List<GenericSuggestionVM> GetLogGenericsDeleted(string cas,string searchType);
        List<GenericSuggestionVM> GetLogGenericsSuggestionRejected();
        List<Map_Combined_Generic_VM> GetAllCombinedGenerics(string cas, string Type);
        List<Map_Combined_Generic_VM> GetAllCombinedGenericsByQsid(string cas, string Type);
         int InsertIfNotExistChemicalName(string chemicalName);
        List<Map_AlternateCas_VM> GetListAllMapAlternateCasSearch(string searchCas_AlternateCas, string text);
        List<Map_Hydrates_VM> GetListAllMapHydratesSearch(string searchCasHydrate, string text);
        void Refresh_RecalculateFlatGenerics(string searchCasFlatGenerics, string text);
        List<Generic_CircularRef_VM> GetAllUniqueCircularRef(string searchCas_UniqueCircularRef, string text);
        bool ApproveGenericSuggestion_Bulk(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType);
        bool RejectGenericSuggestion_Bulk(GenericSuggestionVM objGenericSuggestionTree_VM, string tabType);
        List<MapGenericsCalculated_ViewModel> GetListDetailGenericsCalculated_ViewModel();
        List<GenericSuggestionComments_VM> GetListIdentifierComments(int? casId, int? identifierValueID, int? identifierTypeId);
    }
}
