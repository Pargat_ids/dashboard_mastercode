﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CommonGenericUIView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for ConfirmDialog_UC.xaml
    /// </summary>
    public partial class ConfirmDialog_UC : Window
    {
        public bool IsAccepted { get; set; }
        public ConfirmDialog_UC(string header)
        {
            InitializeComponent();
            headerDialog.Text = header;
        }

        private void Button_Yes_Click(object sender, RoutedEventArgs e)
        {
            IsAccepted = true;
            this.Close();
        }

        private void Button_No_Click(object sender, RoutedEventArgs e)
        {
            IsAccepted = false;
            this.Close();
        }
    }
}
