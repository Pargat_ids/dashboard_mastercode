﻿using CommonGenericUIView.Library;
using CRA_DataAccess.ViewModel;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace CommonGenericUIView.ViewModel
{
    public class CommentsUC_VM : Base_ViewModel 
    {

        public GenericComment_VM objGenericComment { get; set; }
        public string TitleCommentHeader { get; set; }
        public string TitleParentCommentHeader { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemComments { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemParentComments { get; set; }
        public ObservableCollection<ListCommonComboBox_ViewModel> listUser_Res { get; set; } = new ObservableCollection<ListCommonComboBox_ViewModel>();
        private ListCommonComboBox_ViewModel _selectedUser_Res { get; set; }
        public ListCommonComboBox_ViewModel selectedUser_Res
        {
            get { return _selectedUser_Res; }
            set
            {
                _selectedUser_Res = value;
                NotifyPropertyChanged("selectedUser_Res");
                if (!ListSelectedUser.Any(x => x.Id == _selectedUser_Res.Id))
                {
                    ListSelectedUser.Add(_selectedUser_Res);
                    var existingUser = ListSelectedUser;
                    // ListSelectedUser = new ObservableCollection<ListCommonComboBox_ViewModel>(null);
                    ListSelectedUser = new ObservableCollection<ListCommonComboBox_ViewModel>(existingUser);
                    NotifyPropertyChanged("ListSelectedUser");
                    NotifyPropertyChanged("ListSelectedUser");
                }
            }
        }
        public ObservableCollection<ListCommonComboBox_ViewModel> ListSelectedUser { get; set; } = new ObservableCollection<ListCommonComboBox_ViewModel>();
        public Visibility IsGeneResearchParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility IsGeneResearchParentChildVisibility { get; set; } = Visibility.Collapsed;
        public bool? IsNeedMoreResearch { get; set; } = false;
        public bool? IsNeedMoreResearchParentChild { get; set; } = false;
        private string _commentGenericSuggestionAction { get; set; }
        public string CommentGenericSuggestionAction
        {
            get { return _commentGenericSuggestionAction; }
            set
            {
                _commentGenericSuggestionAction = value;
                NotifyPropertyChanged("CommentGenericSuggestionAction");
            }
        }
        public ICommand CommandAddAdditionalComment { get; private set; }
        public ICommand CommandIsGeneResearchChange { get; private set; }
        public ICommand CommandClosePopUp { get; private set; }
        public ICommand CommandDisableMoreResearch { get; set; }
        public ICommand CommandRemoveSelectedUserFromList { get; set; }
        public ICommand CommandCopy { get; set; }

        public bool IsShowPopUp { get; set; } = false;
        public string ActionType { get; set; }
        public double CommentWindowHeight { get; set; }
        public Visibility ParentChildWindowVisibility { get; set; }
        public CommentsUC_VM(GenericComment_VM _objGenericComment)
        {
            objGenericComment = _objGenericComment;
            Init();
        }
        public void Init() {
          
            CommentWindowHeight = System.Windows.SystemParameters.PrimaryScreenHeight - objGenericComment.CustomScreenHeight;
            CommandAddAdditionalComment = new RelayCommand(CommandAddAdditionalCommentExecute, CommandAddAdditionalCommentCanExecute);
            CommandIsGeneResearchChange = new RelayCommand(CommandIsGeneResearchChangeExecute);
            CommandClosePopUp = new RelayCommand(CommondClosePopUpExecute);
            CommandDisableMoreResearch = new RelayCommand(CommandDisableMoreResearchExecute);
            CommandRemoveSelectedUserFromList = new RelayCommand(CommandRemoveSelectedUserFromListExecute);
            CommandCopy = new RelayCommand(CommandCopyExecute);


            if (!string.IsNullOrEmpty(objGenericComment.ParentCas) && objGenericComment.ParentCasId == 0)
            {
                var casId = _objLibraryFunction_Service.GetCasIdByCas(objGenericComment.ParentCas);
                if (casId != null && casId != 0)
                {
                    objGenericComment.ParentCasId = casId;

                }
                else
                {
                    _notifier.ShowError("Selected Cas not exists in database");
                }
            }

            BindTitleHeader();
            BindListItemComments();
            BindListItemParentComments();
            BindListUser();
            ParentChildWindowVisibility = objGenericComment.IsParentChildView ? Visibility.Visible : Visibility.Collapsed;
            NotifyPropertyChanged("ParentChildWindowVisibility");
            NotifyPropertyChanged("objGenericComment");
        }

        private void CommandCopyExecute(object obj)
        {
            Clipboard.SetDataObject(obj);
        }

        private void CommandRemoveSelectedUserFromListExecute(object obj)
        {
            var userId = (int)obj;
            var objUser = ListSelectedUser.Where(x => x.Id == userId).FirstOrDefault();
            if (objUser != null)
            {
                ListSelectedUser.Remove(objUser);
                var existingUser = ListSelectedUser;
                //ListSelectedUser = new ObservableCollection<ListCommonComboBox_ViewModel>(null);
                ListSelectedUser = new ObservableCollection<ListCommonComboBox_ViewModel>(existingUser);
                NotifyPropertyChanged("ListSelectedUser");
            }
        }

        public void BindListUser()
        {
            listUser_Res = new ObservableCollection<ListCommonComboBox_ViewModel>(
                _objLibraryFunction_Service.GetAllActiveLibrary_Users().Select(x => new ListCommonComboBox_ViewModel()
                {
                    Id = x.Id,
                    Id_String = x.Id_String,
                    Name = x.Name,

                }).ToList()
                );
        }
        public void CommandDisableMoreResearchExecute(object obj)
        {
            string type = (string)obj;
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction))
            {
                _objLibraryFunction_Service.PutFlagDisableAdditionalResearch((int)objGenericComment.ParentCasId, objGenericComment.ChildCasId, type);
                InsertNewComments(type);

                CommandClosePopUp.Execute("");
            }
            else
            {
                _notifier.ShowError("comment mandatory to add addition comment!");
            }

        }
        private void CommondClosePopUpExecute(object obj)
        {
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }

        public void BindTitleHeader()
        {
            if (objGenericComment.IsIdentifierCommentView)
            {
                TitleParentCommentHeader = "Generic Comments for Cas : '" + objGenericComment.ParentCas + "' and Identifier Value = '" + objGenericComment.ChildCas + "' :";
            }
            else if (objGenericComment.IsChemicalCommentView)
            {
                TitleParentCommentHeader = "Map Incorrect cas + name comments for : '" + objGenericComment.ParentCas + "' ( " + objGenericComment.ChemicalName + " )";
            }
            else
            {
                TitleCommentHeader = "Generic Comments for Parent : '" + objGenericComment.ParentCas + "' and Child = '" + objGenericComment.ChildCas + "' :";
                TitleParentCommentHeader = "Generic Parent Comments for : '" + objGenericComment.ParentCas + "'";
            }
            NotifyPropertyChanged("TitleCommentHeader");
            NotifyPropertyChanged("TitleParentCommentHeader");
        }
        public void BindListItemComments()
        {                      
                Task.Run(() =>
            {
                listItemComments = new ObservableCollection<GenericSuggestionComments_VM>(_objGeneric_Service.GetListGenericsComments(objGenericComment.ParentCasId, objGenericComment.ChildCasId));
                NotifyPropertyChanged("listItemComments");
                if (listItemComments.Any(x => x.Additional_Generics_research_flag))
                {
                    IsGeneResearchParentChildVisibility = Visibility.Visible;
                }
                else
                {
                    IsGeneResearchParentChildVisibility = Visibility.Collapsed;
                }
                NotifyPropertyChanged("IsGeneResearchParentChildVisibility");
            });
            
        }

        public void BindListItemCommentsForIdentifier()
        {
            Task.Run(() =>
            {
                listItemParentComments = new ObservableCollection<GenericSuggestionComments_VM>(_objGeneric_Service.GetListIdentifierComments(objGenericComment.ParentCasId, objGenericComment.ChildCasId, objGenericComment.IdentifierTypeID));
                NotifyPropertyChanged("listItemParentComments");
               
            });
        }
        public void BindListItemParentComments()
        {
            Task.Run(() =>
            {
                var listParentComment = new List<GenericSuggestionComments_VM>();
                if (objGenericComment.IsChemicalCommentView)
                {
                    listParentComment = _objLibraryFunction_Service.GetListCommentsMapIncorrectCasName((int)objGenericComment.ChemicalNameId, (int)objGenericComment.ParentCasId).Select(x => new VersionControlSystem.Model.ViewModel.GenericSuggestionComments_VM
                    {
                        Action = x.Action,
                        CommentBy = x.CommentBy,
                        CommentID = x.CommentID,
                        Comments = x.Comments,
                        Timestamp = x.Timestamp
                    }).ToList();
                }
                else if (objGenericComment.IsIdentifierCommentView)
                {
                    BindListItemCommentsForIdentifier();
                }
                else
                {
                    listParentComment = _objGeneric_Service.GetListGenericsParentComments(objGenericComment.ParentCasId);
                }
                if (listParentComment.Any(x => x.Additional_Generics_research_flag))
                {
                    IsGeneResearchParentVisibility = Visibility.Visible;
                }
                else
                {
                    IsGeneResearchParentVisibility = Visibility.Collapsed;
                }
                NotifyPropertyChanged("IsGeneResearchParentVisibility");
                listItemParentComments = new ObservableCollection<GenericSuggestionComments_VM>(listParentComment);
                NotifyPropertyChanged("listItemParentComments");

            });
        }
        private bool CommandAddAdditionalCommentCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandAddAdditionalCommentExecute(object obj)
        {
            string type = (string)obj;

            if ((IsNeedMoreResearch == true || IsNeedMoreResearchParentChild == true) && !CheckCasSubstanceIsGroup((int)objGenericComment.ParentCasId))
            {
                _notifier.ShowError("Add research flag faild due to cas substance type is not 'Group'");
                return;
            }

            InsertNewComments(type);
            IsNeedMoreResearch = false;
            NotifyPropertyChanged("IsNeedMoreResearch");
            IsNeedMoreResearchParentChild = false;
            NotifyPropertyChanged("IsNeedMoreResearchParentChild");
        }

        public void InsertNewComments(string type)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) && !string.IsNullOrWhiteSpace(CommentGenericSuggestionAction))
            {
                int commentID = 0;
                if (objGenericComment.IsChemicalCommentView)
                {
                    _objLibraryFunction_Service.AddAdditionalComment_Map_Incorrect_Cas_ChemicalName((int)objGenericComment.ChemicalNameId, (int)objGenericComment.ParentCasId, Engine.CurrentUserSessionID, CommentGenericSuggestionAction);
                }
                if (objGenericComment.IsIdentifierCommentView)
                {
                    Map_IdentifierSubstance_Comments objCom = new Map_IdentifierSubstance_Comments();
                    objCom.CommentBy = Environment.UserName;
                    objCom.IdentifierValueID =(int) objGenericComment.ChildCasId;
                    objCom.CasID = (int)objGenericComment.ParentCasId;
                    objCom.IdentifierTypeID = (int)objGenericComment.IdentifierTypeID;
                    objCom.Comments = CommentGenericSuggestionAction;
                    objCom.Action = "AdditionalComments";
                    _objLibraryFunction_Service.InsertMapIdentifierSubstance(objCom);
                }
                else
                {
                    GenericSuggestionVM objGC = new GenericSuggestionVM();
                    if (type == "ParentChild")
                    {
                        objGC.ParentCasID = objGenericComment.ParentCasId;
                        objGC.ChildCasID = (int)objGenericComment.ChildCasId;
                        objGC.Note = CommentGenericSuggestionAction;
                        objGC.IsAdditionalGenericsResearchFlag = IsNeedMoreResearchParentChild == true ? true : false;
                    }
                    else if (type == "Parent")
                    {
                        objGC.ChildCasID = (int)objGenericComment.ParentCasId;
                        objGC.ParentComment = CommentGenericSuggestionAction;
                        objGC.IsAdditionalGenericsResearchFlag = IsNeedMoreResearch == true ? true : false;
                    }
                    objGC.UserID = Environment.UserName;
                    objGC.CommentTypeId = _objLibraryFunction_Service.GetLibraryCommentTypes().Where(x => x.CommentType == objGenericComment.CommentType).Select(x => x.CommentTypeID).FirstOrDefault();
                    commentID = _objGeneric_Service.InsertAdditionalComment(objGC, type);
                }

                if (ListSelectedUser.Count > 0 && commentID !=0)
                {
                    var addUserComment = ListSelectedUser.Select(x => new Map_Comments_UserMentions()
                    {
                        CommentID = commentID,
                        UserID = x.Id_String,
                        IsReview = false,
                        DateAdded = ESTTime(),
                        CommentType = type
                    }).ToList();
                    _objLibraryFunction_Service.AddUpdateMapUserComments(addUserComment);
                }

                _notifier.ShowSuccess("Additonal Comment Added Successfully");
                BindListItemComments();
                BindListItemParentComments();
                CommentGenericSuggestionAction = string.Empty;
                ListSelectedUser = new ObservableCollection<ListCommonComboBox_ViewModel>();
                NotifyPropertyChanged("ListSelectedUser");
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", "", "UpdateUserMentionComments"), typeof(CommentsUC_VM));

            }
            else
            {
                _notifier.ShowError("Comment should not be empty and null! Kindly fill it try again");
            }
        }

        private bool CheckCasSubstanceIsGroup(int? casID)
        {
            bool result = false;
            if (casID != null)
            {
                var existingsubstanceTypeID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceType", (int)casID);
                if (existingsubstanceTypeID == 2)
                {
                    result = true;
                }
            }
            return result;
        }
        private void CommandIsGeneResearchChangeExecute(object obj)
        {
            IsShowPopUp = true;
            NotifyPropertyChanged("IsShowPopUp");
            ActionType = (string)obj;
            NotifyPropertyChanged("ActionType");

        }
    }
}
