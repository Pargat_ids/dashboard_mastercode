﻿using CommonGenericUIView.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace CommonGenericUIView.ViewModel
{
    public class QcCheckUC_VM:Base_ViewModel
    {
        public CommonDataGrid_ViewModel<QcChecksDetail_VM> commonDGQcCheck { get; set; }
        public ICommand CommandUpdateQcCheck { get; set; }
        
        public int? _versionControlID { get; set; }
        private List<QcChecksDetail_VM> _listQcCheck { get; set; } = new();
        public List<QcChecksDetail_VM> listQcCheck
        {
            get { return _listQcCheck; }
            set
            {
                _listQcCheck = value;
                MessengerInstance.Send<PropertyChangedMessage<List<QcChecksDetail_VM>>>(new PropertyChangedMessage<List<QcChecksDetail_VM>>(null, _listQcCheck, "Default List"));
            }
        }
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public Visibility SaveBtnVisibility { get; set; }
        public QcCheckUC_VM(int? versionControlID=null,bool? IsSaveButtonShow=true)
        {
            SaveBtnVisibility = IsSaveButtonShow ==true ? Visibility.Visible : Visibility.Collapsed;
            _versionControlID = versionControlID;
            CommandUpdateQcCheck = new RelayCommand(CommandUpdateQcCheckExecute, CommandUpdateQcCheckCanExecute);
            commonDGQcCheck = new CommonDataGrid_ViewModel<QcChecksDetail_VM>(GetColumnForQcCheck(), "QcCheckID", "List Qc Checks");
            BindQcCheckList();
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(QcChecksDetail_VM), CallbackButtonCommand);
            NotifyPropertyChanged("SaveBtnVisibility");
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        private bool CommandUpdateQcCheckCanExecute(object obj)
        {
            if (_versionControlID == null)
            {
                return false;
            }
            else {
                return true;
            }
        }

        private void CommandUpdateQcCheckExecute(object obj)
        {
            UpdateVersionControl_QcChecks((int)_versionControlID);
            _notifier.ShowSuccess("QcChecks update successfully!");
        }

        private void CallbackButtonCommand(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "CommandIsNotApplicable")
            {

            }
        }
        private List<CommonDataGridColumn> GetColumnForQcCheck()
        {
            List<CommonDataGridColumn> listColumn = new();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qc Checks", ColBindingName = "QcCheckName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qc Checks Description", ColBindingName = "QcCheckDescription", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Completed", ColBindingName = "IsCompleted", ColType = "ToggleButton", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsNotApplicable", ColBindingName = "IsNotApplicable", CommandParam = "CommandIsNotApplicable", ColType = "CheckboxEnable", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "User Name", ColBindingName = "UserName", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Added", ColBindingName = "DateAdded", ColType = "Textbox", ColumnWidth = "200" });
            return listColumn;
        }
        public void BindQcCheckList()
        {
            Task.Run(() =>
            {
                if (_versionControlID != null)
                {
                    listQcCheck = _objLibraryFunction_Service.GetQcChecks(_versionControlID).Select(x => new QcChecksDetail_VM()
                    {
                        DateAdded = x.DateAdded,
                        IsCompleted = x.IsCompleted,
                        IsNotApplicable = x.IsNotApplicable == true ? true : false,
                        QcCheckDescription = x.QcCheckDescription,
                        QcCheckID = x.QcCheckID,
                        QcCheckName = x.QcCheckName,
                        UserName = x.UserName
                    }).ToList();
                }

            });
        }
        public void UpdateVersionControl_QcChecks(int versionControlHistoryId)
        {
            List<Log_VersionControl_QcChecks> listLogQcCheck = new();
            listLogQcCheck = listQcCheck.Select(x => new Log_VersionControl_QcChecks()
            {
                QcCheckID = x.QcCheckID,
                IsCompleted = x.IsCompleted,
                VersionControl_HistoryID = versionControlHistoryId,
                DateAdded = ESTTime(),
                IsNotApplicable = x.IsNotApplicable,
                UserID = Environment.UserName
            }).ToList();
            _objLibraryFunction_Service.InsertLogVersionControlQcCheck(listLogQcCheck, versionControlHistoryId);
        }
    }
}
