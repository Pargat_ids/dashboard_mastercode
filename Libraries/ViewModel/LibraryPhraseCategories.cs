﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibPhraseCategory> comonLibPhraseCategories_VM { get; set; }
        public Visibility VisibilityLibPhraseCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibPhraseCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibPhraseCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibPhraseCategories { get; private set; }
        public ICommand CommandAddLibPhraseCategories { get; private set; }
        public ICommand CommandAddMoreLibPhraseCategories { get; private set; }
        private bool _IsShowPopUpLibPhraseCategories { get; set; }
        public bool IsShowPopUpLibPhraseCategories
        {
            get { return _IsShowPopUpLibPhraseCategories; }
            set
            {
                if (_IsShowPopUpLibPhraseCategories != value)
                {
                    _IsShowPopUpLibPhraseCategories = value;
                    NotifyPropertyChanged("IsShowPopUpLibPhraseCategories");
                }
            }
        }
        private ObservableCollection<MapLibPhraseCategory> _lstLibPhraseCategories { get; set; } = new ObservableCollection<MapLibPhraseCategory>();
        public ObservableCollection<MapLibPhraseCategory> lstLibPhraseCategories
        {
            get { return _lstLibPhraseCategories; }
            set
            {
                if (_lstLibPhraseCategories != value)
                {
                    _lstLibPhraseCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibPhraseCategory>>>(new PropertyChangedMessage<List<MapLibPhraseCategory>>(null, _lstLibPhraseCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibPhraseCategories()
        {
            VisibilityLibPhraseCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibPhraseCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibPhraseCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibPhraseCategory>(
                "select PhraseCategory, PhraseCategory_Description, ARCodePrefix, PhraseKeyPrefix, PhraseKeyCollection, " +
                " PhraseSet, DisplayToUser, CodeRequiresCategoryName, TimeStamp as DateAdded from Library_PhraseCategories a order by PhraseCategoryID desc", Win_Administrator_VM.CommonListConn);
                lstLibPhraseCategories = new ObservableCollection<MapLibPhraseCategory>(SAPLibPhraseCategoriesTable);
            });
            NotifyPropertyChanged("comonLibPhraseCategories_VM");
            VisibilityLibPhraseCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibPhraseCategoriesDashboard_Loader");
        }
        private void RefreshGridLibPhraseCategories(PropertyChangedMessage<string> flag)
        {
            BindLibPhraseCategories();
            if (IsShowPopUpLibPhraseCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibPhraseCategories");
            }
        }

        private void CommandAddLibPhraseCategoriesExecute(object obj)
        {
            SaveLibPhraseCategories();
            IsShowPopUpLibPhraseCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibPhraseCategories");
            ClearPopUpVariableLibPhraseCategories();
            PopUpContentAddLibPhraseCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibPhraseCategoriesVisibility");
        }
        private bool CommandAddLibPhraseCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newPhraseCategory.ToString()) || string.IsNullOrEmpty(newPhraseCategoryDesc.ToString()) || string.IsNullOrEmpty(newPhraseCategoryARCodePrefix.ToString()) || string.IsNullOrEmpty(newPhraseCategoryPhrasekeyPrefix.ToString()) || string.IsNullOrEmpty(newPhraseCategoryPhraseKeyCol.ToString()) || string.IsNullOrEmpty(newPhraseCategoryPhraseSet.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveLibPhraseCategories()
        {
            Library_PhraseCategories obj = new Library_PhraseCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_PhraseCategories')", Win_Administrator_VM.CommonListConn));
                obj.PhraseCategoryID = mx + 1;
                obj.PhraseCategory = newPhraseCategory.Trim();
                obj.PhraseCategory_Description = newPhraseCategoryDesc.Trim();
                obj.ARCodePrefix = newPhraseCategoryARCodePrefix;
                obj.PhraseKeyPrefix = newPhraseCategoryPhrasekeyPrefix;
                obj.PhraseKeyCollection = newPhraseCategoryPhraseKeyCol;
                obj.PhraseSet = newPhraseCategoryPhraseSet;
                obj.DisplayToUser = newPhraseCategoryDisplaytoUser;
                obj.CodeRequiresCategoryName = newPhraseCategoryCodeRequireCatName;
                obj.TimeStamp = System.DateTime.Now;


                var chkAlreadyExist = context.Library_PhraseCategories.Count(x => x.PhraseCategory.ToLower() == obj.PhraseCategory.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("PhraseCategory can't be duplicate");
                }
                else
                {
                    context.Library_PhraseCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New PhraseCategory Saved Successfully.");
                }
            }

            ClearPopUpVariableLibPhraseCategories();
            BindLibPhraseCategories();

        }
        private void CommandAddMoreLibPhraseCategoriesExecute(object obj)
        {
            SaveLibPhraseCategories();
        }
        private void CommandClosePopUpExecuteLibPhraseCategories(object obj)
        {
            IsShowPopUpLibPhraseCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibPhraseCategories");
        }
        private void CommandShowPopUpAddLibPhraseCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibPhraseCategories();
            IsShowPopUpLibPhraseCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibPhraseCategories");
            PopUpContentAddLibPhraseCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibPhraseCategoriesVisibility");
        }

        public string newPhraseCategory { get; set; } = string.Empty;
        public string newPhraseCategoryDesc { get; set; } = string.Empty;
        public string newPhraseCategoryARCodePrefix { get; set; } = string.Empty;
        public string newPhraseCategoryPhrasekeyPrefix { get; set; } = string.Empty;
        public string newPhraseCategoryPhraseKeyCol { get; set; } = string.Empty;
        public string newPhraseCategoryPhraseSet { get; set; } = string.Empty;
        public bool newPhraseCategoryDisplaytoUser { get; set; } = false;
        public bool newPhraseCategoryCodeRequireCatName { get; set; } = false;
        public void ClearPopUpVariableLibPhraseCategories()
        {
            newPhraseCategory = "";
            newPhraseCategoryDesc = "";
            newPhraseCategoryARCodePrefix = "";
            newPhraseCategoryPhrasekeyPrefix = "";
            newPhraseCategoryPhraseKeyCol = "";
            newPhraseCategoryPhraseSet = "";
            newPhraseCategoryDisplaytoUser = false;
            newPhraseCategoryCodeRequireCatName = false;
            NotifyPropertyChanged("newPhraseCategory");
            NotifyPropertyChanged("newPhraseCategoryDesc");
            NotifyPropertyChanged("newPhraseCategoryARCodePrefix");
            NotifyPropertyChanged("newPhraseCategoryPhrasekeyPrefix");
            NotifyPropertyChanged("newPhraseCategoryPhraseKeyCol");
            NotifyPropertyChanged("newPhraseCategoryPhraseSet");
            NotifyPropertyChanged("newPhraseCategoryDisplaytoUser");
            NotifyPropertyChanged("newPhraseCategoryCodeRequireCatName");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibPhraseCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseCategory", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseCategory_Description", ColBindingName = "PhraseCategory_Description", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ARCodePrefix", ColBindingName = "ARCodePrefix", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseKeyPrefix", ColBindingName = "PhraseKeyPrefix", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseKeyCollection", ColBindingName = "PhraseKeyCollection", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseSet", ColBindingName = "PhraseSet", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DisplayToUser", ColBindingName = "DisplayToUser", ColType = "CheckBoxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CodeRequiresCategoryName", ColBindingName = "CodeRequiresCategoryName", ColType = "CheckBoxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibPhraseCategory
    {
        public string PhraseCategory { get; set; }
        public string PhraseCategory_Description { get; set; }
        public string ARCodePrefix { get; set; }
        public string PhraseKeyPrefix { get; set; }
        public string PhraseKeyCollection { get; set; }
        public string PhraseSet { get; set; }
        public bool DisplayToUser { get; set; }
        public bool CodeRequiresCategoryName { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
