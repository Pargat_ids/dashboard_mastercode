﻿using System.Windows.Input;
using Libraries.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using CRA_DataAccess.ViewModel;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public RelayCommand commandOpenCurrentDataViewNewAddList { get; set; }
        public CommonDataGrid_ViewModel<MapLibSubroot> comonLibSubroots_VM { get; set; }

        public List<LibLang> listSubrootLanguage { get; set; }
        public Visibility VisibilityLibSubrootsDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibSubrootsVisibility { get; set; }
        public ICommand CommandClosePopUpLibSubroots { get; private set; }
        public ICommand CommandShowPopUpAddLibSubroots { get; private set; }
        public ICommand CommandAddLibSubroots { get; private set; }
        public ICommand CommandAddMoreLibSubroots { get; private set; }
        private bool _IsShowPopUpLibSubroots { get; set; }
        public bool IsShowPopUpLibSubroots
        {
            get { return _IsShowPopUpLibSubroots; }
            set
            {
                if (_IsShowPopUpLibSubroots != value)
                {
                    _IsShowPopUpLibSubroots = value;
                    NotifyPropertyChanged("_IsShowPopUpLibSubroots");
                }
            }
        }
        private ObservableCollection<MapLibSubroot> _lstLibSubroots { get; set; } = new ObservableCollection<MapLibSubroot>();
        public ObservableCollection<MapLibSubroot> lstLibSubroots
        {
            get { return _lstLibSubroots; }
            set
            {
                if (_lstLibSubroots != value)
                {
                    _lstLibSubroots = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibSubroot>>>(new PropertyChangedMessage<List<MapLibSubroot>>(null, _lstLibSubroots.ToList(), "Default List"));
                }
            }
        }
        private LibLang _SelectedSubrootLanguage { get; set; }
        public LibLang SelectedSubrootLanguage
        {
            get => _SelectedSubrootLanguage;
            set
            {
                if (Equals(_SelectedSubrootLanguage, value))
                {
                    return;
                }

                _SelectedSubrootLanguage = value;
            }
        }
        private void CallbackHyperlink(NotificationMessageAction<object> obj)
        {
            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
        private void commandOpenCurrentDataViewNewAddListExected(object obj)
        {

            //Task.Run(() =>
            //{
            try
            {
                Engine.IsOpenCurrentDataViewFromVersionHis = true;
                Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

            }
            catch (Exception ex)
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);
                });
            }
            //});
        }
        public void NotifyMe(string callAction)
        {

        }
        private void BindLibSubroots()
        {
            List<MapLibSubroot> newFinalList = new List<MapLibSubroot>();
            VisibilityLibSubrootsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibSubrootsDashboard_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var SAPLibSubrootsTable = objCommon.DbaseQueryReturnSqlList<MapLibSubrootprev>(
                    "select b.PhraseID, Phrase as Subroot, b.LanguageID,C.LanguageName, a.SubrootTypeID, a.TranslatedPhraseGroupID,0 as No_of_active_lists,'' as Active_Lists, a.TimeStamp as DateAdded from [ecMap].Library_SubRoots a, Library_Phrases b, LIbrary_Languages c where a.PhraseId_Subroot = b.PhraseId and b.LanguageID = c.LanguageID order by LibrarySubrootsID desc", Win_Administrator_VM.CommonListConn);
                    if (SAPLibSubrootsTable.Any())
                    {
                        var onlyPhrases = SAPLibSubrootsTable.Select(x => x.PhraseID).Distinct().ToList();
                        var CatIdListField = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "List Field Name").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var subrootcat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Subroot").Select(y => y.PhraseCategoryID).FirstOrDefault();

                        var findQsxxxQsid = (from d1 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.PhraseCategoryID == subrootcat)
                                             join d2 in onlyPhrases
                                             on d1.PhraseID equals d2
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID, d1.QSID_ID, d3.QSID }).Distinct().ToList();
                        var onlyQsids = findQsxxxQsid.Select(y => y.QSID_ID).Distinct().ToList();
                        var onlyQsxxPhrID = (from d1 in context.QSxxx_ListDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == CatIdListField)
                                             join d2 in onlyQsids
                                             on d1.QSID_ID equals d2
                                             select new { d1.PhraseID, d1.QSID_ID }).Distinct().ToList();
                        var qsxxListDic = (from d1 in onlyQsxxPhrID
                                           join d2 in context.Library_Phrases.AsNoTracking()
                                           on d1.PhraseID equals d2.PhraseID
                                           select new { d1.QSID_ID, d2.PhraseID, d2.Phrase }).Distinct().ToList();
                        var combineFinal = (from d1 in findQsxxxQsid
                                            join d2 in qsxxListDic
                                            on d1.QSID_ID
                                            equals d2.QSID_ID
                                            select new { d1.PhraseID, QSID = d1.QSID + "(" + d2.Phrase + ")", QSID_ID = d1.QSID_ID }).Distinct().ToList();
                        var joinQsids = (from d1 in combineFinal
                                         group d1 by d1.PhraseID into g
                                         select new
                                         {
                                             PhraseID = g.Key,
                                             displayValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID).ToList()),
                                             navigateValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID_ID).ToList()),
                                             count = g.Count(),
                                         }).Distinct().ToList();
                        newFinalList = (from d1 in SAPLibSubrootsTable
                                        join d2 in joinQsids
                                        on d1.PhraseID equals d2.PhraseID into tg
                                        select new MapLibSubroot
                                        {
                                            DateAdded = d1.DateAdded,
                                            PhraseID = d1.PhraseID,
                                            Subroot = d1.Subroot,
                                            LanguageID = d1.LanguageID,
                                            LanguageName = d1.LanguageName,
                                            SubrootTypeID = d1.SubrootTypeID,
                                            TranslatedPhraseGroupID = d1.TranslatedPhraseGroupID,
                                            No_of_active_lists = tg.Select(x => x.count).FirstOrDefault(),
                                            Active_Lists = ConvertInfoToHyoerlinkVal(tg.Select(x => x.displayValue).FirstOrDefault(), tg.Select(x => x.navigateValue).FirstOrDefault()),
                                        }).ToList();
                    }
                    lstLibSubroots = new ObservableCollection<MapLibSubroot>(newFinalList);
                }
                NotifyPropertyChanged("comonLibSubroots_VM");
                VisibilityLibSubrootsDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibSubrootsDashboard_Loader");
            });

        }

        private List<HyperLinkDataValue> ConvertInfoToHyoerlinkVal(string qsids, string qsidIds)
        {
            List<HyperLinkDataValue> listHyperLinkValues = new List<HyperLinkDataValue>();
            if (qsidIds != null)
            {
                string[] qsidArry = qsids.Split(',');
                string[] qsid_Id_Arry = qsidIds.Split(',');
                for (int index = 0; index < qsidArry.Length; index++)
                {
                    listHyperLinkValues.Add(new HyperLinkDataValue() { DisplayValue = qsidArry[index].Trim(), NavigateData = qsid_Id_Arry[index].Trim() });
                }
            }
            return listHyperLinkValues;

        }
        private void RefreshGridLibSubroots(PropertyChangedMessage<string> flag)
        {
            BindLibSubroots();
            if (IsShowPopUpLibSubroots)
            {
                NotifyPropertyChanged("IsShowPopUpLibSubroots");
            }
        }

        private void CommandAddLibSubrootsExecute(object obj)
        {
            SaveLibSubroots();
            IsShowPopUpLibSubroots = false;
            NotifyPropertyChanged("IsShowPopUpLibSubroots");
            ClearPopUpVariableLibSubroots();
            PopUpContentAddLibSubrootsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibSubrootsVisibility");
        }
        private bool commandOpenCurrentDataViewNewAddListCanExecte(object obj)
        {
            return true;
        }
        private bool CommandAddLibSubrootsCanExecute(object obj)
        {
            if (SelectedSubrootLanguage == null || string.IsNullOrEmpty(newSubroot.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibSubroots()
        {
            Library_SubRoots obj = new Library_SubRoots();
            using (var context = new CRAModel())
            {
                var repPhr = Replacement("Subroot", newSubroot);
                if (string.IsNullOrEmpty(repPhr) == false)
                {
                    var resultBox = System.Windows.Forms.MessageBox.Show("Exist in replacement table, replaced by -" + repPhr + Environment.NewLine + "Do you want to Continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (resultBox == DialogResult.Yes)
                    {
                        newSubroot = repPhr;
                        NotifyPropertyChanged("newSubroot");
                    }
                    else
                    {
                        return;
                    }
                }

                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('[ecMap].Library_SubRoots')", Win_Administrator_VM.CommonListConn));
                obj.LibrarySubRootsID = mx + 1;
                var phrVal = newSubroot.Trim();
                var subrootVal = objCommon.RemoveWhitespaceCharacter(phrVal, stripCharacter);
                obj.PhraseID_Subroot = 0;
                obj.PhraseHash_LowerCase = objCommon.GenerateSHA256String(subrootVal.ToLower());
                obj.SubRootTypeID = newSubrootTypeID == 0 || newSubrootTypeID == null ? null : newSubrootTypeID;
                obj.TranslatedPhraseGroupID = null;
                obj.IsActive = true;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(phrVal);
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == SelectedSubrootLanguage.LanguageID).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = subrootVal,
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = SelectedSubrootLanguage.LanguageID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == SelectedSubrootLanguage.LanguageID).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID_Subroot = phrId;

                var chkAlreadyExist = context.Library_SubRoots.Count(x => x.PhraseHash_LowerCase == obj.PhraseHash_LowerCase);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Subroot can't be duplicate");
                }
                else
                {
                    context.Library_SubRoots.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Subroot Saved Successfully.");
                }
            }

            ClearPopUpVariableLibSubroots();
            BindLibSubroots();

        }
        private void CommandAddMoreLibSubrootsExecute(object obj)
        {
            SaveLibSubroots();
        }
        private void CommandClosePopUpExecuteLibSubroots(object obj)
        {
            IsShowPopUpLibSubroots = false;
            NotifyPropertyChanged("IsShowPopUpLibSubroots");
        }
        private void CommandShowPopUpAddLibSubrootsExecute(object obj)
        {
            ClearPopUpVariableLibSubroots();
            IsShowPopUpLibSubroots = true;
            NotifyPropertyChanged("IsShowPopUpLibSubroots");
            PopUpContentAddLibSubrootsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibSubrootsVisibility");
        }
        public string newSubroot { get; set; } = string.Empty;
        public int? newSubrootTypeID { get; set; } = 0;
        public void ClearPopUpVariableLibSubroots()
        {
            newSubroot = "";
            newSubrootTypeID = null;
            NotifyPropertyChanged("newSubroot");
            NotifyPropertyChanged("newSubrootTypeID");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibSubroots()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Subroot", ColBindingName = "Subroot", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubrootTypeID", ColBindingName = "SubrootTypeID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TranslatedPhraseGroupID", ColBindingName = "TranslatedPhraseGroupID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_active_lists", ColBindingName = "No_of_active_lists", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Active_Lists", ColBindingName = "Active_Lists", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibSubroot
    {
        public int PhraseID { get; set; }
        public string Subroot { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public int? SubrootTypeID { get; set; }
        public int? TranslatedPhraseGroupID { get; set; }
        public int No_of_active_lists { get; set; }
        public List<HyperLinkDataValue> Active_Lists { get; set; }
        public DateTime? DateAdded { get; set; }
    }
    public class MapLibSubrootprev
    {
        public int PhraseID { get; set; }
        public string Subroot { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public int? SubrootTypeID { get; set; }
        public int? TranslatedPhraseGroupID { get; set; }
        public int No_of_active_lists { get; set; }
        public string Active_Lists { get; set; }
        public DateTime? DateAdded { get; set; }
    }
    public class LibLang
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
    }
}
