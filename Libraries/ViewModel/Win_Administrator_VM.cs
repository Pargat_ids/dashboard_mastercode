﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using Libraries.Library;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using System;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public TranslatePhrases.ViewModel.Win_Administrator_VM TPDataViewDataContext { get; set; }

        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }

        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {
                case "Units":
                    RunLibUnit();
                    break;
                case "Unit Replacement":
                    ListChangeToUnit = objCommon.DbaseQueryReturnSqlList<LibUnits>("select UnitID, Unit from Library_Units order by Unit", Win_Administrator_VM.CommonListConn);
                    NotifyPropertyChanged("ListChangeToUnit");
                    RunUnitRep();
                    break;
                case "ControlPhrase Replacement":
                    ListChangeToControlPhrase = objCommon.DbaseQueryReturnSqlList<LibPhrase>("select y.PhraseID, y.Phrase from (select PhraseID_ExpressedAs as PhraseID from Library_ExpressedAs " +
                                            " union " +
                                            " select PhraseID_ThresholdBasis as PhraseID from Library_Threshold_Calculation_Basis " +
                                            " union " +
                                            " select PhraseID_Subroot as PhraseID from ecMap.Library_SubRoots " +
                                            " union " +
                                            " select PhraseID_PhysicalForm as PhraseID from Library_PhysicalForm) as x, Library_Phrases y " +
                                            " where x.PhraseID = y.PhraseID", Win_Administrator_VM.CommonListConn);
                    NotifyPropertyChanged("ListChangeToUnit");
                    RunControlPhraseRep();
                    break;
                case "Modules":
                    BindLibModules();
                    break;
                case "US Federal Groups":
                    BindLibUSFederal();
                    break;
                case "Unit Conversions":
                    BindLibUnitConversions();
                    break;
                case "Threshold Categories":
                    BindLibThresholdCategories();
                    break;
                case "SubstanceName Category":
                    BindLibSubstanceNameCategories();
                    break;
                case "Substance Categories":
                    BindLibSubstanceCategories();
                    break;
                case "Strip Characters":
                    BindLibStripCharacters();
                    break;
                case "Squash Characters":
                    BindLibSquashCharacters();
                    break;
                case "Products":
                    BindLibProducts();
                    break;
                case "Phrase Categories":
                    BindLibPhraseCategories();
                    break;
                case "List Level MetaData":
                    BindLibMetaData();
                    break;
                case "List Verticals":
                    BindLibListVerticals();
                    break;
                case "Identifier Categories":
                    BindLibIdentifierCategories();
                    break;
                case "Generics Categories":
                    BindLibGenericsCategories();
                    break;
                case "Functional Classes":
                    BindLibFunctionalClasses();
                    break;
                case "Data Types":
                    BindLibDataTypes();
                    break;
                case "Countries":
                    BindLibCountries();
                    break;
                case "Subroots":
                    BindLibSubroots();
                    break;
                case "Expressed As (Phrase)":
                    BindLibExpressedAs();
                    break;
                case "Physical Form (Phrase)":
                    BindLibPhysicalForm();
                    break;
                case "Threshold calculation basis":
                    BindLibThresholdBasis();
                    break;
                case "List Relationships":
                    BindLibRelation();
                    break;
                case "HTML Conversion Characters":
                    BindLibGreekConversion();
                    break;
                case "Disapproved CAS-Identifiers":
                    BindLiDisapprovedCasIdent();
                    break;
            }
        }

        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommandTabChanged { get; set; }

        public static string CommonListConn;
        public ICommand SearchCommand { get; set; }
        private void SearchCommandExecute(object obj)
        {
            SelectedTabItemChanged();
        }

        //public bool IsSelectedTabData { get; set; }
        public List<LibYesNo> listFlavor { get; set; } = new List<LibYesNo>();
        public List<LibYesNo> listFema { get; set; } = new List<LibYesNo>();

        public CommonDataGrid_ViewModel<MapLibModule> comonLibModules_VM { get; set; }
        public CommonDataGrid_ViewModel<MapLibRelation> comonLibRelation_VM { get; set; }

        public List<IStripChar> stripCharacter = new List<IStripChar>();

        public Win_Administrator_VM()
        {
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            TPDataViewDataContext = new TranslatePhrases.ViewModel.Win_Administrator_VM();
            using (var context = new CRAModel())
            {
                stripCharacter = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();

                listSubrootLanguage = objCommon.DbaseQueryReturnSqlList<LibLang>("select LanguageID, LanguageName from Library_Languages order by LanguageName", Win_Administrator_VM.CommonListConn);
                ListChangeToUnit = objCommon.DbaseQueryReturnSqlList<LibUnits>("select UnitID, Unit from Library_Units order by Unit", Win_Administrator_VM.CommonListConn);
                ListChangeToControlPhrase = objCommon.DbaseQueryReturnSqlList<LibPhrase>("select y.PhraseID, y.Phrase from (select PhraseID_ExpressedAs as PhraseID from Library_ExpressedAs " +
                                            " union " +
                                            " select PhraseID_ThresholdBasis as PhraseID from Library_Threshold_Calculation_Basis " +
                                            " union " +
                                            " select PhraseID_Subroot as PhraseID from ecMap.Library_SubRoots " +
                                            " union " +
                                            " select PhraseID_PhysicalForm as PhraseID from Library_PhysicalForm) as x, Library_Phrases y " +
                                            " where x.PhraseID = y.PhraseID", Win_Administrator_VM.CommonListConn);
            }
            newPhysicalForm = string.Empty;
            comonLibUnit_VM = new CommonDataGrid_ViewModel<MapLibUnit>(GetListGridColumnLibUnit(), "Library Units", "Units");
            comonUnitRep_VM = new CommonDataGrid_ViewModel<MapUnitRep>(GetListGridColumnUnitRep(), "Unit Replacement", "Unit Replacement");
            comonControlPhraseRep_VM = new CommonDataGrid_ViewModel<MapControlPhraseRep>(GetListGridColumnControlPhraseRep(), "Control Phrase Replacement", "Control Phrase Replacement");
            comonLibLanguages_VM = new CommonDataGrid_ViewModel<MapLibLanguage>(GetListGridColumnLibLanguages(), "Library Languages", "Languages");


            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);

            CommandShowPopUpAddLibUnit = new RelayCommand(CommandShowPopUpAddLibUnitExecute);
            CommandShowPopUpAddUnitRep = new RelayCommand(CommandShowPopUpAddUnitRepExecute);
            CommandShowPopUpAddControlPhraseRep = new RelayCommand(CommandShowPopUpAddControlPhraseRepExecute);
            CommandClosePopUpLibUnit = new RelayCommand(CommandClosePopUpExecuteLibUnit);
            CommandClosePopUpUnitRep = new RelayCommand(CommandClosePopUpExecuteUnitRep);
            CommandClosePopUpControlPhraseRep = new RelayCommand(CommandClosePopUpExecuteControlPhraseRep);
            CommandAddLibUnit = new RelayCommand(CommandAddLibUnitExecute, CommandAddLibUnitCanExecute);
            CommandAddUnitRep = new RelayCommand(CommandAddUnitRepExecute, CommandAddUnitRepCanExecute);
            CommandAddControlPhraseRep = new RelayCommand(CommandAddControlPhraseRepExecute, CommandAddControlPhraseRepCanExecute);
            CommandAddMoreLibUnit = new RelayCommand(CommandAddMoreLibUnitExecute, CommandAddLibUnitCanExecute);
            CommandAddMoreUnitRep = new RelayCommand(CommandAddMoreUnitRepExecute, CommandAddUnitRepCanExecute);
            CommandAddMoreControlPhraseRep = new RelayCommand(CommandAddMoreControlPhraseRepExecute, CommandAddControlPhraseRepCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<MapLibUnit>>(this, NotifyMeLibUnit);
            MessengerInstance.Register<PropertyChangedMessage<MapUnitRep>>(this, NotifyMeUnitRep);
            MessengerInstance.Register<PropertyChangedMessage<MapControlPhraseRep>>(this, NotifyMeControlPhraseRep);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibUnit), CommandButtonExecutedLibUnit);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapUnitRep), CommandButtonExecutedUnitRep);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapControlPhraseRep), CommandButtonExecutedControlPhraseRep);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibUnit), RefreshGridLibUnit);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapUnitRep), RefreshGridUnitRep);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapControlPhraseRep), RefreshGridControlPhraseRep) ;
            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCell);
            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCellRep);
            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCellControlPhraseRep);


            CommandShowPopUpAddLibLanguages = new RelayCommand(CommandShowPopUpAddLibLanguagesExecute);
            CommandClosePopUpLibLanguages = new RelayCommand(CommandClosePopUpExecuteLibLanguages);
            CommandAddLibLanguages = new RelayCommand(CommandAddLibLanguagesExecute, CommandAddLibLanguagesCanExecute);
            CommandAddMoreLibLanguages = new RelayCommand(CommandAddMoreLibLanguagesExecute, CommandAddLibLanguagesCanExecute);

            //MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibLanguage), RefreshGridLibLanguages);
            MessengerInstance.Register<NotificationMessageAction<MapLibLanguage>>(this, CallBackBindListDetailGridExecute);

            comonLibModules_VM = new CommonDataGrid_ViewModel<MapLibModule>(GetListGridColumnLibModules(), "Library Module", "Module");
            CommandShowPopUpAddLibModules = new RelayCommand(CommandShowPopUpAddLibModulesExecute);
            CommandClosePopUpLibModules = new RelayCommand(CommandClosePopUpExecuteLibModules);
            CommandAddLibModules = new RelayCommand(CommandAddLibModulesExecute, CommandAddLibModulesCanExecute);
            CommandAddMoreLibModules = new RelayCommand(CommandAddMoreLibModulesExecute, CommandAddLibModulesCanExecute);


            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibModule), RefreshGridLibModules);
            comonLibRelation_VM = new CommonDataGrid_ViewModel<MapLibRelation>(GetListGridColumnLibRelation(), "Library ListRelationShip", "ListRelationShip");
            CommandShowPopUpAddLibRelation = new RelayCommand(CommandShowPopUpAddLibRelationExecute);
            CommandClosePopUpLibRelation = new RelayCommand(CommandClosePopUpExecuteLibRelation);
            CommandAddLibRelation = new RelayCommand(CommandAddLibRelationExecute, CommandAddLibRelationCanExecute);
            CommandAddMoreLibRelation = new RelayCommand(CommandAddMoreLibRelationExecute, CommandAddLibRelationCanExecute);


            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibRelation), RefreshGridLibRelation);

            comonLibUSFederal_VM = new CommonDataGrid_ViewModel<MapLibUSFederal>(GetListGridColumnLibUSFederal(), "Library_USFederal_ListGroups", "US Federal List Groups");
            CommandShowPopUpAddLibUSFederal = new RelayCommand(CommandShowPopUpAddLibUSFederalExecute);
            CommandClosePopUpLibUSFederal = new RelayCommand(CommandClosePopUpExecuteLibUSFederal);
            CommandAddLibUSFederal = new RelayCommand(CommandAddLibUSFederalExecute, CommandAddLibUSFederalCanExecute);
            CommandAddMoreLibUSFederal = new RelayCommand(CommandAddMoreLibUSFederalExecute, CommandAddLibUSFederalCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibUSFederal), RefreshGridLibUSFederal);

            comonLibUnitConversions_VM = new CommonDataGrid_ViewModel<MapLibUnitConversion>(GetListGridColumnLibUnitConversions(), "Library UnitConversions", "Unit Conversions");
            CommandShowPopUpAddLibUnitConversions = new RelayCommand(CommandShowPopUpAddLibUnitConversionsExecute);
            CommandClosePopUpLibUnitConversions = new RelayCommand(CommandClosePopUpExecuteLibUnitConversions);
            CommandAddLibUnitConversions = new RelayCommand(CommandAddLibUnitConversionsExecute, CommandAddLibUnitConversionsCanExecute);
            CommandAddMoreLibUnitConversions = new RelayCommand(CommandAddMoreLibUnitConversionsExecute, CommandAddLibUnitConversionsCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibUnitConversion), RefreshGridLibUnitConversions);

            comonLibThresholdCategories_VM = new CommonDataGrid_ViewModel<MapLibThresholdCategory>(GetListGridColumnLibThresholdCategories(), "Library ThresholdCategories", "Substance Name Categories");
            CommandShowPopUpAddLibThresholdCategories = new RelayCommand(CommandShowPopUpAddLibThresholdCategoriesExecute);
            CommandClosePopUpLibThresholdCategories = new RelayCommand(CommandClosePopUpExecuteLibThresholdCategories);
            CommandAddLibThresholdCategories = new RelayCommand(CommandAddLibThresholdCategoriesExecute, CommandAddLibThresholdCategoriesCanExecute);
            CommandAddMoreLibThresholdCategories = new RelayCommand(CommandAddMoreLibThresholdCategoriesExecute, CommandAddLibThresholdCategoriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibThresholdCategory), RefreshGridLibThresholdCategories);

            comonLibSubstanceNameCategories_VM = new CommonDataGrid_ViewModel<MapLibSubstanceNameCategory>(GetListGridColumnLibSubstanceNameCategories(), "Library SubstanceNameCategories", "Substance Name Categories");
            CommandShowPopUpAddLibSubstanceNameCategories = new RelayCommand(CommandShowPopUpAddLibSubstanceNameCategoriesExecute);
            CommandClosePopUpLibSubstanceNameCategories = new RelayCommand(CommandClosePopUpExecuteLibSubstanceNameCategories);
            CommandAddLibSubstanceNameCategories = new RelayCommand(CommandAddLibSubstanceNameCategoriesExecute, CommandAddLibSubstanceNameCategoriesCanExecute);
            CommandAddMoreLibSubstanceNameCategories = new RelayCommand(CommandAddMoreLibSubstanceNameCategoriesExecute, CommandAddLibSubstanceNameCategoriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibSubstanceNameCategory), RefreshGridLibSubstanceNameCategories);

            comonLibSubstanceCategories_VM = new CommonDataGrid_ViewModel<MapLibSubstanceCategory>(GetListGridColumnLibSubstanceCategories(), "Library SubstanceCategories", "Strip Characters");
            CommandShowPopUpAddLibSubstanceCategories = new RelayCommand(CommandShowPopUpAddLibSubstanceCategoriesExecute);
            CommandClosePopUpLibSubstanceCategories = new RelayCommand(CommandClosePopUpExecuteLibSubstanceCategories);
            CommandAddLibSubstanceCategories = new RelayCommand(CommandAddLibSubstanceCategoriesExecute, CommandAddLibSubstanceCategoriesCanExecute);
            CommandAddMoreLibSubstanceCategories = new RelayCommand(CommandAddMoreLibSubstanceCategoriesExecute, CommandAddLibSubstanceCategoriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibSubstanceCategory), RefreshGridLibSubstanceCategories);

            comonLibStripCharacters_VM = new CommonDataGrid_ViewModel<MapLibStripCharacter>(GetListGridColumnLibStripCharacters(), "Library StripCharacters", "Strip Characters");
            CommandShowPopUpAddLibStripCharacters = new RelayCommand(CommandShowPopUpAddLibStripCharactersExecute);
            CommandClosePopUpLibStripCharacters = new RelayCommand(CommandClosePopUpExecuteLibStripCharacters);
            CommandAddLibStripCharacters = new RelayCommand(CommandAddLibStripCharactersExecute, CommandAddLibStripCharactersCanExecute);
            CommandAddMoreLibStripCharacters = new RelayCommand(CommandAddMoreLibStripCharactersExecute, CommandAddLibStripCharactersCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibStripCharacter), RefreshGridLibStripCharacters);

            comonLibSquashCharacters_VM = new CommonDataGrid_ViewModel<MapLibSquashCharacter>(GetListGridColumnLibSquashCharacters(), "Library SquashCharacters", "Squash Characters");
            CommandShowPopUpAddLibSquashCharacters = new RelayCommand(CommandShowPopUpAddLibSquashCharactersExecute);
            CommandClosePopUpLibSquashCharacters = new RelayCommand(CommandClosePopUpExecuteLibSquashCharacters);
            CommandAddLibSquashCharacters = new RelayCommand(CommandAddLibSquashCharactersExecute, CommandAddLibSquashCharactersCanExecute);
            CommandAddMoreLibSquashCharacters = new RelayCommand(CommandAddMoreLibSquashCharactersExecute, CommandAddLibSquashCharactersCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibSquashCharacter), RefreshGridLibSquashCharacters);

            comonLibProducts_VM = new CommonDataGrid_ViewModel<MapLibProduct>(GetListGridColumnLibProducts(), "Library Products", "Products");
            CommandShowPopUpAddLibProducts = new RelayCommand(CommandShowPopUpAddLibProductsExecute);
            CommandClosePopUpLibProducts = new RelayCommand(CommandClosePopUpExecuteLibProducts);
            CommandAddLibProducts = new RelayCommand(CommandAddLibProductsExecute, CommandAddLibProductsCanExecute);
            CommandAddMoreLibProducts = new RelayCommand(CommandAddMoreLibProductsExecute, CommandAddLibProductsCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibProduct), RefreshGridLibProducts);

            comonLibPhraseCategories_VM = new CommonDataGrid_ViewModel<MapLibPhraseCategory>(GetListGridColumnLibPhraseCategories(), "Library PhraseCategory", "PhraseCategory");
            CommandShowPopUpAddLibPhraseCategories = new RelayCommand(CommandShowPopUpAddLibPhraseCategoriesExecute);
            CommandClosePopUpLibPhraseCategories = new RelayCommand(CommandClosePopUpExecuteLibPhraseCategories);
            CommandAddLibPhraseCategories = new RelayCommand(CommandAddLibPhraseCategoriesExecute, CommandAddLibPhraseCategoriesCanExecute);
            CommandAddMoreLibPhraseCategories = new RelayCommand(CommandAddMoreLibPhraseCategoriesExecute, CommandAddLibPhraseCategoriesCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibPhraseCategory), RefreshGridLibPhraseCategories);

            comonLibMetaData_VM = new CommonDataGrid_ViewModel<MapLibMetaData>(GetListGridColumnLibMetaData(), "Library ListLevel MetaData", "ListLevel MetaData");
            CommandShowPopUpAddLibMetaData = new RelayCommand(CommandShowPopUpAddLibMetaDataExecute);
            CommandClosePopUpLibMetaData = new RelayCommand(CommandClosePopUpExecuteLibMetaData);
            CommandAddLibMetaData = new RelayCommand(CommandAddLibMetaDataExecute, CommandAddLibMetaDataCanExecute);
            CommandAddMoreLibMetaData = new RelayCommand(CommandAddMoreLibMetaDataExecute, CommandAddLibMetaDataCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibPhraseCategory), RefreshGridLibMetaData);

            comonLibListVerticals_VM = new CommonDataGrid_ViewModel<MapLibListVerticals>(GetListGridColumnLibListVerticals(), "Library ListVertical", "ListVertical");
            CommandShowPopUpAddLibListVerticals = new RelayCommand(CommandShowPopUpAddLibListVerticalsExecute);
            CommandClosePopUpLibListVerticals = new RelayCommand(CommandClosePopUpExecuteLibListVerticals);
            CommandAddLibListVerticals = new RelayCommand(CommandAddLibListVerticalsExecute, CommandAddLibListVerticalsCanExecute);
            CommandAddMoreLibListVerticals = new RelayCommand(CommandAddMoreLibListVerticalsExecute, CommandAddLibListVerticalsCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibListVerticals), RefreshGridLibListVerticals);

            comonLibIdentifierCategories_VM = new CommonDataGrid_ViewModel<MapLibIdentifierCategory>(GetListGridColumnLibIdentifierCategories(), "Library Identifier Categories", "Identifier Categories");
            CommandShowPopUpAddLibIdentifierCategories = new RelayCommand(CommandShowPopUpAddLibIdentifierCategoriesExecute);
            CommandClosePopUpLibIdentifierCategories = new RelayCommand(CommandClosePopUpExecuteLibIdentifierCategories);
            CommandAddLibIdentifierCategories = new RelayCommand(CommandAddLibIdentifierCategoriesExecute, CommandAddLibIdentifierCategoriesCanExecute);
            CommandAddMoreLibIdentifierCategories = new RelayCommand(CommandAddMoreLibIdentifierCategoriesExecute, CommandAddLibIdentifierCategoriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibIdentifierCategory), RefreshGridLibIdentifierCategories);

            comonLibGenericsCategories_VM = new CommonDataGrid_ViewModel<MapLibGenericsCategory>(GetListGridColumnLibGenericsCategories(), "Library Generics Categories", "Generics Categories");
            CommandShowPopUpAddLibGenericsCategories = new RelayCommand(CommandShowPopUpAddLibGenericsCategoriesExecute);
            CommandClosePopUpLibGenericsCategories = new RelayCommand(CommandClosePopUpExecuteLibGenericsCategories);
            CommandAddLibGenericsCategories = new RelayCommand(CommandAddLibGenericsCategoriesExecute, CommandAddLibGenericsCategoriesCanExecute);
            CommandAddMoreLibGenericsCategories = new RelayCommand(CommandAddMoreLibGenericsCategoriesExecute, CommandAddLibGenericsCategoriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibGenericsCategory), RefreshGridLibGenericsCategories);

            comonLibFunctionalClasses_VM = new CommonDataGrid_ViewModel<MapLibFunctionalClass>(GetListGridColumnLibFunctionalClasses(), "Library Functional Classes", "Functional Classes");
            CommandShowPopUpAddLibFunctionalClasses = new RelayCommand(CommandShowPopUpAddLibFunctionalClassesExecute);
            CommandClosePopUpLibFunctionalClasses = new RelayCommand(CommandClosePopUpExecuteLibFunctionalClasses);
            CommandAddLibFunctionalClasses = new RelayCommand(CommandAddLibFunctionalClassesExecute, CommandAddLibFunctionalClassesCanExecute);
            CommandAddMoreLibFunctionalClasses = new RelayCommand(CommandAddMoreLibFunctionalClassesExecute, CommandAddLibFunctionalClassesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibFunctionalClass), RefreshGridLibFunctionalClasses);

            comonLibDataTypes_VM = new CommonDataGrid_ViewModel<MapLibDataType>(GetListGridColumnLibDataTypes(), "Library DataType", "DataType");
            CommandShowPopUpAddLibDataTypes = new RelayCommand(CommandShowPopUpAddLibDataTypesExecute);
            CommandClosePopUpLibDataTypes = new RelayCommand(CommandClosePopUpExecuteLibDataTypes);
            CommandAddLibDataTypes = new RelayCommand(CommandAddLibDataTypesExecute, CommandAddLibDataTypesCanExecute);
            CommandAddMoreLibDataTypes = new RelayCommand(CommandAddMoreLibDataTypesExecute, CommandAddLibDataTypesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibDataType), RefreshGridLibDataTypes);

            comonLibCountries_VM = new CommonDataGrid_ViewModel<MapLibCountries>(GetListGridColumnLibCountries(), "Library Countries", "Countries");
            CommandShowPopUpAddLibCountries = new RelayCommand(CommandShowPopUpAddLibCountriesExecute);
            CommandClosePopUpLibCountries = new RelayCommand(CommandClosePopUpExecuteLibCountries);
            CommandAddLibCountries = new RelayCommand(CommandAddLibCountriesExecute, CommandAddLibCountriesCanExecute);
            CommandAddMoreLibCountries = new RelayCommand(CommandAddMoreLibCountriesExecute, CommandAddLibCountriesCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibCountries), RefreshGridLibCountries);

            comonLibSubroots_VM = new CommonDataGrid_ViewModel<MapLibSubroot>(GetListGridColumnLibSubroots(), "Library Subroot", "Subroot");

            comonLibExpressedAs_VM = new CommonDataGrid_ViewModel<MapLibExpressedAs>(GetListGridColumnLibExpressedAs(), "Library ExpressedAs", "ExpressedAs");

            comonLibPhysicalForm_VM = new CommonDataGrid_ViewModel<MapLibPhysicalForm>(GetListGridColumnLibPhysicalForm(), "Library PhysicalForm", "PhysicalForm");

            comonLibThresholdBasis_VM = new CommonDataGrid_ViewModel<MapLibThresholdBasis>(GetListGridColumnLibThresholdBasis(), "Library Threshold Basis", "ThresholdBasis ");
            comonLibGreekConversion_VM = new CommonDataGrid_ViewModel<MapLibGreekConversion>(GetListGridColumnLibGreekConversion(), "HTML Conversion Characters", "HTML Conversion Characters");
            comonDisapprovedCasIdent_VM = new CommonDataGrid_ViewModel<MapLibDisapprovedCasIdent>(GetListGridColumnDisapprovedCasIdent(), "Disapproved Cas Identifier", "Disapproved Cas Identifier");
            MessengerInstance.Register<PropertyChangedMessage<MapLibDisapprovedCasIdent>>(this, NotifyMeDisapproved);
            

            CommandShowPopUpAddLibSubroots = new RelayCommand(CommandShowPopUpAddLibSubrootsExecute);
            CommandShowPopUpAddLibExpressedAs = new RelayCommand(CommandShowPopUpAddLibExpressedAsExecute);
            CommandShowPopUpAddLibPhysicalForm = new RelayCommand(CommandShowPopUpAddLibPhysicalFormExecute);
            CommandShowPopUpAddLibThresholdBasis = new RelayCommand(CommandShowPopUpAddLibThresholdBasisExecute);
            CommandShowPopUpAddLibGreekConversion = new RelayCommand(CommandShowPopUpAddLibGreekConversionExecute);
            CommandClosePopUpLibSubroots = new RelayCommand(CommandClosePopUpExecuteLibSubroots);
            CommandClosePopUpLibExpressedAs = new RelayCommand(CommandClosePopUpExecuteLibExpressedAs);
            CommandClosePopUpLibPhysicalForm = new RelayCommand(CommandClosePopUpExecuteLibPhysicalForm);
            CommandClosePopUpLibThresholdBasis = new RelayCommand(CommandClosePopUpExecuteLibThresholdBasis);
            CommandClosePopUpLibGreekConversion = new RelayCommand(CommandClosePopUpExecuteLibGreekConversion);
            CommandAddLibSubroots = new RelayCommand(CommandAddLibSubrootsExecute, CommandAddLibSubrootsCanExecute);
            CommandAddLibExpressedAs = new RelayCommand(CommandAddLibExpressedAsExecute, CommandAddLibExpressedAsCanExecute);
            CommandAddLibPhysicalForm = new RelayCommand(CommandAddLibPhysicalFormExecute, CommandAddLibPhysicalFormCanExecute);
            CommandAddLibThresholdBasis = new RelayCommand(CommandAddLibThresholdBasisExecute, CommandAddLibThresholdBasisCanExecute);
            CommandAddLibGreekConversion = new RelayCommand(CommandAddLibGreekConversionExecute, CommandAddLibGreekConversionCanExecute);
            CommandAddMoreLibSubroots = new RelayCommand(CommandAddMoreLibSubrootsExecute, CommandAddLibSubrootsCanExecute);
            CommandAddMoreLibExpressedAs = new RelayCommand(CommandAddMoreLibExpressedAsExecute, CommandAddLibExpressedAsCanExecute);
            CommandAddMoreLibPhysicalForm = new RelayCommand(CommandAddMoreLibPhysicalFormExecute, CommandAddLibPhysicalFormCanExecute);
            CommandAddMoreLibThresholdBasis = new RelayCommand(CommandAddMoreLibThresholdBasisExecute, CommandAddLibThresholdBasisCanExecute);
            CommandAddMoreLibGreekConversion = new RelayCommand(CommandAddMoreLibGreekConversionExecute, CommandAddLibGreekConversionCanExecute);

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(MapLibSubroot), RefreshGridLibSubroots);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibSubroot), CallbackHyperlink);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibExpressedAs), CallbackHyperlinkExpressedAs);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibPhysicalForm), CallbackHyperlinkPhysicalForm);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibThresholdBasis), CallbackHyperlinkThresholdBasis);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapLibGreekConversion), CallbackHyperlinkGreekConversion);

            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExected, commandOpenCurrentDataViewNewAddListCanExecte);
            RunLibUnit();
            BindLibLanguages();
        }

       
    }
}
