﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibThresholdCategory> comonLibThresholdCategories_VM { get; set; }
        public string newThresholdCategory { get; set; } = string.Empty;
        public Visibility VisibilityLibThresholdCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibThresholdCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibThresholdCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibThresholdCategories { get; private set; }
        public ICommand CommandAddLibThresholdCategories { get; private set; }
        public ICommand CommandAddMoreLibThresholdCategories { get; private set; }
        private bool _IsShowPopUpLibThresholdCategories { get; set; }
        public bool IsShowPopUpLibThresholdCategories
        {
            get { return _IsShowPopUpLibThresholdCategories; }
            set
            {
                if (_IsShowPopUpLibThresholdCategories != value)
                {
                    _IsShowPopUpLibThresholdCategories = value;
                    NotifyPropertyChanged("_IsShowPopUpLibThresholdCategories");
                }
            }
        }
        private ObservableCollection<MapLibThresholdCategory> _lstLibThresholdCategories { get; set; } = new ObservableCollection<MapLibThresholdCategory>();
        public ObservableCollection<MapLibThresholdCategory> lstLibThresholdCategories
        {
            get { return _lstLibThresholdCategories; }
            set
            {
                if (_lstLibThresholdCategories != value)
                {
                    _lstLibThresholdCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibThresholdCategory>>>(new PropertyChangedMessage<List<MapLibThresholdCategory>>(null, _lstLibThresholdCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibThresholdCategories()
        {
            VisibilityLibThresholdCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibThresholdCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibThresholdCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibThresholdCategory>(
                "select ThresholdCategory, Timestamp as DateAdded from Library_ThresholdCategories a order by ThresholdCategoryID desc", Win_Administrator_VM.CommonListConn);
                lstLibThresholdCategories = new ObservableCollection<MapLibThresholdCategory>(SAPLibThresholdCategoriesTable);
            });
            NotifyPropertyChanged("comonLibThresholdCategories_VM");
            VisibilityLibThresholdCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibThresholdCategoriesDashboard_Loader");
        }
        private void RefreshGridLibThresholdCategories(PropertyChangedMessage<string> flag)
        {
            BindLibThresholdCategories();
            if (IsShowPopUpLibThresholdCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibThresholdCategories");
            }
        }

        private void CommandAddLibThresholdCategoriesExecute(object obj)
        {
            SaveLibThresholdCategories();
            IsShowPopUpLibThresholdCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibThresholdCategories");
            ClearPopUpVariableLibThresholdCategories();
            PopUpContentAddLibThresholdCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibThresholdCategoriesVisibility");
        }
        private bool CommandAddLibThresholdCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newThresholdCategory.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibThresholdCategories()
        {
            Library_ThresholdCategories obj = new Library_ThresholdCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select max(ThresholdCategoryID) from Library_ThresholdCategories", Win_Administrator_VM.CommonListConn));
                obj.ThresholdCategoryID = mx + 1;
                obj.ThresholdCategory = newThresholdCategory.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_ThresholdCategories.Count(x => x.ThresholdCategory.ToLower() == obj.ThresholdCategory.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("ThresholdCategory can't be duplicate");
                }
                else
                {
                    context.Library_ThresholdCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Threshold Category Saved Successfully.");
                }
            }

            ClearPopUpVariableLibThresholdCategories();
            BindLibThresholdCategories();

        }
        private void CommandAddMoreLibThresholdCategoriesExecute(object obj)
        {
            SaveLibThresholdCategories();
        }
        private void CommandClosePopUpExecuteLibThresholdCategories(object obj)
        {
            IsShowPopUpLibThresholdCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibThresholdCategories");
        }
        private void CommandShowPopUpAddLibThresholdCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibThresholdCategories();
            IsShowPopUpLibThresholdCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibThresholdCategories");
            PopUpContentAddLibThresholdCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibThresholdCategoriesVisibility");
        }

        public void ClearPopUpVariableLibThresholdCategories()
        {
            newThresholdCategory = "";
            NotifyPropertyChanged("newThresholdCategory");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibThresholdCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ThresholdCategory", ColBindingName = "ThresholdCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibThresholdCategory
    {
        public string ThresholdCategory { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
