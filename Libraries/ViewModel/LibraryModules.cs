﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string newModuleName { get; set; } = string.Empty;
        public string newModuleCode { get; set; } = string.Empty;

        public Visibility VisibilityLibModulesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibModulesVisibility { get; set; }
        public ICommand CommandClosePopUpLibModules { get; private set; }
        public ICommand CommandShowPopUpAddLibModules { get; private set; }
        public ICommand CommandAddLibModules { get; private set; }
        public ICommand CommandAddMoreLibModules { get; private set; }
        private bool _IsShowPopUpLibModules { get; set; }
        public bool IsShowPopUpLibModules
        {
            get { return _IsShowPopUpLibModules; }
            set
            {
                if (_IsShowPopUpLibModules != value)
                {
                    _IsShowPopUpLibModules = value;
                    NotifyPropertyChanged("_IsShowPopUpLibModules");
                }
            }
        }
        private ObservableCollection<MapLibModule> _lstLibModules { get; set; } = new ObservableCollection<MapLibModule>();
        public ObservableCollection<MapLibModule> lstLibModules
        {
            get { return _lstLibModules; }
            set
            {
                if (_lstLibModules != value)
                {
                    _lstLibModules = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibModule>>>(new PropertyChangedMessage<List<MapLibModule>>(null, _lstLibModules.ToList(), "Default List"));
                }
            }
        }

        public void BindLibModules()
        {
            VisibilityLibModulesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibModulesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibModulesTable = objCommon.DbaseQueryReturnSqlList<MapLibModule>(
                "select ModuleCode, ModuleName, TimeStamp as DateAdded from Library_Modules a order by ModuleID desc", Win_Administrator_VM.CommonListConn);
                lstLibModules = new ObservableCollection<MapLibModule>(SAPLibModulesTable);
            });
            NotifyPropertyChanged("comonLibModules_VM");
            VisibilityLibModulesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibModulesDashboard_Loader");
        }
        private void RefreshGridLibModules(PropertyChangedMessage<string> flag)
        {
            BindLibModules();
            if (IsShowPopUpLibModules)
            {
                NotifyPropertyChanged("IsShowPopUpLibModules");
            }
        }

        private void CommandAddLibModulesExecute(object obj)
        {
            SaveLibModules();
            IsShowPopUpLibModules = false;
            NotifyPropertyChanged("IsShowPopUpLibModules");
            ClearPopUpVariableLibModules();
            PopUpContentAddLibModulesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibModulesVisibility");
        }
        private bool CommandAddLibModulesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newModuleName.ToString()) || string.IsNullOrEmpty(newModuleCode.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibModules()
        {
            Library_Modules obj = new Library_Modules();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Modules')", Win_Administrator_VM.CommonListConn));
                obj.ModuleID = mx + 1;
                obj.ModuleName = newModuleName.Trim();
                obj.ModuleCode = newModuleCode.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_Modules.Count(x => x.ModuleName.ToLower() == obj.ModuleName.ToLower() || x.ModuleCode.ToLower() == obj.ModuleCode.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Module/ModuleCode can't be duplicate");
                }
                else
                {
                    context.Library_Modules.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Module Saved Successfully.");
                }
            }

            ClearPopUpVariableLibModules();
            BindLibModules();

        }
        private void CommandAddMoreLibModulesExecute(object obj)
        {
            SaveLibModules();
        }
        private void CommandClosePopUpExecuteLibModules(object obj)
        {
            IsShowPopUpLibModules = false;
            NotifyPropertyChanged("IsShowPopUpLibModules");
        }
        private void CommandShowPopUpAddLibModulesExecute(object obj)
        {
            ClearPopUpVariableLibModules();
            IsShowPopUpLibModules = true;
            NotifyPropertyChanged("IsShowPopUpLibModules");
            PopUpContentAddLibModulesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibModulesVisibility");
        }

        public void ClearPopUpVariableLibModules()
        {
            newModuleName = "";
            newModuleCode = "";
            NotifyPropertyChanged("newModuleName");
            NotifyPropertyChanged("newModuleCode");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibModules()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ModuleCode", ColBindingName = "ModuleCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ModuleName", ColBindingName = "ModuleName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibModule
    {
        public int ModuleID { get; set; }
        public int Ident { get; set; }
        public string ModuleCode { get; set; }
        public string ModuleName { get; set; }
        public string ISO_Two { get; set; }
        public string ISO_Three { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
