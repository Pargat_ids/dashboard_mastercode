﻿using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CRA_DataAccess;
using VersionControlSystem.Model.ViewModel;

namespace Libraries.ViewModel
{
    public class PopupLibUnit_VM : Base_ViewModel
    {
        private CommonFunctions objCommon;
        public CommonDataGrid_ViewModel<ILibUnit> comonLibUnit_VM { get; set; }
        private int unitValue;
        public PopupLibUnit_VM(int unitid)
        {
            objCommon = new CommonFunctions();
            unitValue = unitid;
            Init();
            Task.Run(() => UniqueList());

        }
        private ObservableCollection<ILibUnit> _lstLibUnitList { get; set; } = new ObservableCollection<ILibUnit>();
        public ObservableCollection<ILibUnit> lstLibUnitList
        {
            get { return _lstLibUnitList; }
            set
            {
                if (_lstLibUnitList != value)
                {
                    _lstLibUnitList = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ILibUnit>>>(new PropertyChangedMessage<List<ILibUnit>>(null, _lstLibUnitList.ToList(), "List"));
                }
            }
        }
        public Visibility VisibilityLibUnit_Loader { get; set; }
        private void UniqueList()
        {
            VisibilityLibUnit_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibUnit_Loader");
            Task.Run(() =>
            {
                var SAPLibUnitTable = objCommon.DbaseQueryReturnSqlList<ILibUnit>(
                " select distinct a.QSID_ID, QSID, STUFF((SELECT N', ' + p2.fieldname " +
                " FROM(select distinct QSID_ID, UnitID, FieldName from QSxxx_Values g inner " +
                " join Library_FieldNames h " +
                " on g.UnitsFieldID = h.FieldNameID ) p2 WHERE p2.QSID_ID = a.QSID_ID and " +
                " p2.UnitID = a.UnitID " +
                " order by p2.fieldname FOR XML PATH(N'')), 1, 2, N'') as FieldNames from " +
                " QSxxx_Values a, Library_Qsids b " +
                " where a.qsid_id = b.qsid_id and a.unitid = " +  unitValue + " order by a.qsid_id ", Win_Administrator_VM.CommonListConn);
                lstLibUnitList = new ObservableCollection<ILibUnit>(SAPLibUnitTable);
            });
            NotifyPropertyChanged("lstLibUnitList");
            VisibilityLibUnit_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibUnit_Loader");
        }
        public void Init()
        {
            comonLibUnit_VM = new CommonDataGrid_ViewModel<ILibUnit>(GetQsidList(), "Qsid", "Lists");
            NotifyPropertyChanged("comonLibUnit_VM");
        }
        private List<CommonDataGridColumn> GetQsidList()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColType = "Textbox" , ColumnWidth="200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldNames", ColBindingName = "FieldNames", ColType = "Textbox", ColumnWidth = "500" });
            return listColumn;
        }

    }
    public class ILibUnit
    {
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string FieldNames { get; set; }
    }
}
