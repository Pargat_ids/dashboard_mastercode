﻿using System.Windows.Input;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool _IsShowPopUpLibThresholdBasis { get; set; }
        public Visibility PopUpContentAddLibThresholdBasisVisibility { get; set; }
        public CommonDataGrid_ViewModel<MapLibThresholdBasis> comonLibThresholdBasis_VM { get; set; }
        public string newThresholdBasis { get; set; } = string.Empty;
        public ICommand CommandAddMoreLibThresholdBasis { get; private set; }
        public ICommand CommandAddLibThresholdBasis { get; private set; }
        public ICommand CommandClosePopUpLibThresholdBasis { get; private set; }
        public ICommand CommandShowPopUpAddLibThresholdBasis { get; private set; }
        public Visibility VisibilityLibThresholdBasisDashboard_Loader { get; set; }
        public bool IsShowPopUpLibThresholdBasis
        {
            get { return _IsShowPopUpLibThresholdBasis; }
            set
            {
                if (_IsShowPopUpLibThresholdBasis != value)
                {
                    _IsShowPopUpLibThresholdBasis = value;
                    NotifyPropertyChanged("_IsShowPopUpLibThresholdBasis");
                }
            }
        }
        private void RefreshGridLibThresholdBasiss(PropertyChangedMessage<string> flag)
        {
            BindLibThresholdBasis();
            if (IsShowPopUpLibThresholdBasis)
            {
                NotifyPropertyChanged("IsShowPopUpLibThresholdBasis");
            }
        }
        private void CommandAddLibThresholdBasisExecute(object obj)
        {
            SaveLibThresholdBasis();
            IsShowPopUpLibThresholdBasis = false;
            NotifyPropertyChanged("IsShowPopUpLibThresholdBasis");
            ClearPopUpVariableLibThresholdBasis();
            PopUpContentAddLibThresholdBasisVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibThresholdBasisVisibility");
        }
        private void CommandClosePopUpExecuteLibThresholdBasis(object obj)
        {
            IsShowPopUpLibThresholdBasis = false;
            NotifyPropertyChanged("IsShowPopUpLibThresholdBasis");
        }
        private void CommandShowPopUpAddLibThresholdBasisExecute(object obj)
        {
            ClearPopUpVariableLibThresholdBasis();
            IsShowPopUpLibThresholdBasis = true;
            NotifyPropertyChanged("IsShowPopUpLibThresholdBasis");
            PopUpContentAddLibThresholdBasisVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibThresholdBasisVisibility");
        }
        private bool CommandAddLibThresholdBasisCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newThresholdBasis.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveLibThresholdBasis()
        {
            Library_Threshold_Calculation_Basis obj = new Library_Threshold_Calculation_Basis();
            using (var context = new CRAModel())
            {
                var repPhr = Replacement("Threshold calculation basis", newThresholdBasis);
                if (string.IsNullOrEmpty(repPhr) == false)
                {
                    var resultBox = System.Windows.Forms.MessageBox.Show("Exist in replacement table, replaced by -" + repPhr + Environment.NewLine + "Do you want to Continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (resultBox == DialogResult.Yes)
                    {
                        newThresholdBasis = repPhr;
                        NotifyPropertyChanged("newThresholdBasis");
                    }
                    else
                    {
                        return;
                    }
                }

                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Threshold_Calculation_Basis')", Win_Administrator_VM.CommonListConn));
                obj.LibraryThresholdBasisID = mx + 1;
                var phrVal = newThresholdBasis.Trim();
                var ThresholdBasisVal = objCommon.RemoveWhitespaceCharacter(phrVal, stripCharacter);
                obj.PhraseID_ThresholdBasis = 0;
                obj.PhraseHash_LowerCase = objCommon.GenerateSHA256String(ThresholdBasisVal.ToLower());
                obj.IsActive = true;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(phrVal);
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = ThresholdBasisVal,
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID_ThresholdBasis = phrId;

                var chkAlreadyExist = context.Library_Threshold_Calculation_Basis.Count(x => x.PhraseHash_LowerCase == obj.PhraseHash_LowerCase);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Threshold Calculation Basis can't be duplicate");
                }
                else
                {
                    context.Library_Threshold_Calculation_Basis.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New ThresholdBasis Saved Successfully.");
                }
            }

            ClearPopUpVariableLibThresholdBasis();
            BindLibThresholdBasis();

        }
        public void ClearPopUpVariableLibThresholdBasis()
        {
            newThresholdBasis = "";
            NotifyPropertyChanged("newThresholdBasis");
        }
        private void CommandAddMoreLibThresholdBasisExecute(object obj)
        {
            SaveLibThresholdBasis();
        }
        private void BindLibThresholdBasis()
        {
            List<MapLibThresholdBasis> newFinalList = new List<MapLibThresholdBasis>();
            VisibilityLibThresholdBasisDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibThresholdBasisDashboard_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var SAPLibThresholdBasisTable = objCommon.DbaseQueryReturnSqlList<MapLibThresholdBasis>(
                    "select b.PhraseID, Phrase as ThresholdCalculationBasis, 0 as No_of_active_lists, a.TimeStamp as DateAdded from Library_Threshold_Calculation_Basis a, Library_Phrases b where a.PhraseId_ThresholdBasis = b.PhraseId order by LibraryThresholdBasisID desc", Win_Administrator_VM.CommonListConn);
                    if (SAPLibThresholdBasisTable.Any())
                    {
                        var onlyPhrases = SAPLibThresholdBasisTable.Select(x => x.PhraseID).Distinct().ToList();
                        var CatIdListField = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "List Field Name").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var subrootcat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Threshold calculation basis").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var findQsxxxQsid = (from d1 in context.QSxxx_Phrases.AsNoTracking().AsNoTracking().Where(x => x.PhraseCategoryID == subrootcat)
                                             join d2 in onlyPhrases
                                             on d1.PhraseID equals d2
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID, d1.QSID_ID, d3.QSID }).Distinct().ToList();
                        var onlyQsids = findQsxxxQsid.Select(y => y.QSID_ID).Distinct().ToList();
                        var onlyQsxxPhrID = (from d1 in context.QSxxx_ListDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == CatIdListField)
                                             join d2 in onlyQsids
                                             on d1.QSID_ID equals d2
                                             select new { d1.PhraseID, d1.QSID_ID }).Distinct().ToList();
                        var qsxxListDic = (from d1 in onlyQsxxPhrID
                                           join d2 in context.Library_Phrases.AsNoTracking()
                                           on d1.PhraseID equals d2.PhraseID
                                           select new { d1.QSID_ID, d2.PhraseID, d2.Phrase }).Distinct().ToList();
                        var combineFinal = (from d1 in findQsxxxQsid
                                            join d2 in qsxxListDic
                                            on d1.QSID_ID
                                            equals d2.QSID_ID
                                            select new { d1.PhraseID, QSID = d1.QSID + "(" + d2.Phrase + ")", QSID_ID = d1.QSID_ID }).Distinct().ToList();
                        var joinQsids = (from d1 in combineFinal
                                         group d1 by d1.PhraseID into g
                                         select new
                                         {
                                             PhraseID = g.Key,
                                             displayValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID).ToList()),
                                             navigateValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID_ID).ToList()),
                                             count = g.Count(),
                                         }).Distinct().ToList();
                        newFinalList = (from d1 in SAPLibThresholdBasisTable
                                        join d2 in joinQsids
                                        on d1.PhraseID equals d2.PhraseID into tg
                                        select new MapLibThresholdBasis
                                        {
                                            DateAdded = d1.DateAdded,
                                            PhraseID = d1.PhraseID,
                                            ThresholdCalculationBasis = d1.ThresholdCalculationBasis,
                                            No_of_active_lists = tg.Select(x => x.count).FirstOrDefault(),
                                            Active_Lists = ConvertInfoToHyoerlinkVal(tg.Select(x => x.displayValue).FirstOrDefault(), tg.Select(x => x.navigateValue).FirstOrDefault()),
                                        }).ToList();
                    }
                    lstLibThresholdBasis = new ObservableCollection<MapLibThresholdBasis>(newFinalList);
                }
                NotifyPropertyChanged("comonLibThresholdBasis_VM");
                VisibilityLibThresholdBasisDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibThresholdBasisDashboard_Loader");
            });

        }
        private void CallbackHyperlinkThresholdBasis(NotificationMessageAction<object> obj)
        {
            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        private ObservableCollection<MapLibThresholdBasis> _lstLibThresholdBasis { get; set; } = new ObservableCollection<MapLibThresholdBasis>();
        public ObservableCollection<MapLibThresholdBasis> lstLibThresholdBasis
        {
            get { return _lstLibThresholdBasis; }
            set
            {
                if (_lstLibThresholdBasis != value)
                {
                    _lstLibThresholdBasis = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibThresholdBasis>>>(new PropertyChangedMessage<List<MapLibThresholdBasis>>(null, _lstLibThresholdBasis.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnLibThresholdBasis()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ThresholdCalculationBasis", ColBindingName = "ThresholdCalculationBasis", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_active_lists", ColBindingName = "No_of_active_lists", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Active_Lists", ColBindingName = "Active_Lists", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibThresholdBasis
    {
        public int PhraseID { get; set; }
        public string ThresholdCalculationBasis { get; set; }
        public int No_of_active_lists { get; set; }
        public List<HyperLinkDataValue> Active_Lists { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
