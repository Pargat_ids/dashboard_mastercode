﻿using System.Windows.Input;
using System.Windows.Controls;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using Libraries.CustomUserControl;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapUnitRep> comonUnitRep_VM { get; set; }
        public Visibility VisibilityUnitRepDashboard_Loader { get; set; }
        public ICommand CommandClosePopUpUnitRep { get; private set; }
        public ICommand CommandShowPopUpAddUnitRep { get; private set; }
        public bool IsEditUnitRep { get; set; } = true;
        public List<LibUnits> ListChangeToUnit { get; set; }
        public Visibility PopUpContentAddUnitRepVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<MapUnitRep> _lstUnitRep { get; set; } = new ObservableCollection<MapUnitRep>();
        public ObservableCollection<MapUnitRep> lstUnitRep
        {
            get { return _lstUnitRep; }
            set
            {
                if (_lstUnitRep != value)
                {
                    _lstUnitRep = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapUnitRep>>>(new PropertyChangedMessage<List<MapUnitRep>>(null, _lstUnitRep.ToList(), "Default List"));
                }
            }
        }
        private void BindUnitRep()
        {
            VisibilityUnitRepDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityUnitRepDashboard_Loader");
            Task.Run(() =>
            {
                var SAPUnitRepTable = objCommon.DbaseQueryReturnSqlList<MapUnitRep>(
                "select Orginal_Value as UnitNeedtoReplace, Replace_Unit_ID as UnitID, Unit as ChangeTo " +
                " from Map_Unit_Replacement a, Library_Units b where Replace_Unit_ID = b.UnitID order by MapReplaceUnit_ID desc", Win_Administrator_VM.CommonListConn);
                lstUnitRep = new ObservableCollection<MapUnitRep>(SAPUnitRepTable);
            });
            NotifyPropertyChanged("comonUnitRep_VM");
            VisibilityUnitRepDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityUnitRepDashboard_Loader");
        }
        private bool _IsShowPopUpUnitRep { get; set; }
        public bool IsShowPopUpUnitRep
        {
            get { return _IsShowPopUpUnitRep; }
            set
            {
                if (_IsShowPopUpUnitRep != value)
                {
                    _IsShowPopUpUnitRep = value;
                    NotifyPropertyChanged("IsShowPopUpUnitRep");
                }
            }
        }
        private void RunUnitRep()
        {
            BindUnitRep();
        }
        private void RefreshGridUnitRep(PropertyChangedMessage<string> flag)
        {
            BindUnitRep();
            if (IsShowPopUpUnitRep)
            {
                NotifyPropertyChanged("IsShowPopUpUnitRep");
            }
        }
        public ICommand CommandAddUnitRep { get; private set; }
        private void CommandAddUnitRepExecute(object obj)
        {
            SaveUnitRep();
            IsShowPopUpUnitRep = false;
            IsEditUnitRep = true;
            NotifyPropertyChanged("IsEditUnitRep");
            ClearPopUpVariableUnitRep();
            PopUpContentAddUnitRepVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddUnitRepVisibility");
        }
        private bool CommandAddUnitRepCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newUnitRep.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveUnitRep()
        {
            Map_Unit_Replacement obj = new Map_Unit_Replacement();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Map_Unit_Replacement')", Win_Administrator_VM.CommonListConn));
                obj.MapReplaceUnit_ID = mx + 1;
                obj.Orginal_Value = newUnitRep;
                obj.Replace_Unit_ID = SelectedChangeToUnit.UnitID;

                var chkAlreadyExist = context.Map_Unit_Replacement.Count(x => x.Orginal_Value.ToLower() == obj.Orginal_Value.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Unit Replacement already exists");
                }
                else
                {
                    context.Map_Unit_Replacement.Add(obj);
                    context.SaveChanges();
                }
            }

            ClearPopUpVariableUnitRep();
            BindUnitRep();

        }
        public ICommand CommandAddMoreUnitRep { get; private set; }
        private void CommandAddMoreUnitRepExecute(object obj)
        {
            SaveUnitRep();
        }
        private void CommandClosePopUpExecuteUnitRep(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpUnitRep = false;

        }
        private void CommandShowPopUpAddUnitRepExecute(object obj)
        {
            ClearPopUpVariableUnitRep();
            IsEditUnitRep = true;
            NotifyPropertyChanged("IsEditUnitRep");
            IsShowPopUpUnitRep = true;
            PopUpContentAddUnitRepVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddUnitRepVisibility");
        }

        private LibUnits _SelectedChangeToUnit { get; set; }
        public LibUnits SelectedChangeToUnit
        {
            get => _SelectedChangeToUnit;
            set
            {
                if (Equals(_SelectedChangeToUnit, value))
                {
                    return;
                }

                _SelectedChangeToUnit = value;
            }
        }
        private MapUnitRep _SelectedUnitRep { get; set; }
        public MapUnitRep SelectedUnitRep
        {
            get => (MapUnitRep)_SelectedUnitRep;
            set
            {
                if (Equals(_SelectedUnitRep, value))
                {
                    return;
                }
                _SelectedUnitRep = value;
                OpenPopupUnitRep();
            }
        }

        private void OpenPopupUnitRep()
        {
            if (SelectedUnitRep != null)
            {
                PopupLibUnit objChangesAdd = new PopupLibUnit(SelectedUnitRep.UnitID);
                objChangesAdd.ShowDialog();
            }
        }
        private void NotifyMeUnitRep(PropertyChangedMessage<MapUnitRep> obj)
        {
            SelectedUnitRep = obj.NewValue;
        }

        public void ClearPopUpVariableUnitRep()
        {
            newUnitRep = "";
            NotifyPropertyChanged("newUnitRep");
        }
        private void CommandButtonExecutedUnitRep(NotificationMessageAction<object> obj)
        {

        }
        public string newUnitRep { get; set; } = string.Empty;
        private List<CommonDataGridColumn> GetListGridColumnUnitRep()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Unit Need to Replace", ColBindingName = "UnitNeedtoReplace", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChangeTo", ColBindingName = "ChangeTo", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private DataGridCellInfo _cellInfo_AddNewRep { get; set; }
        public DataGridCellInfo CellInfo_AddNewRep
        {
            get { return _cellInfo_AddNewRep; }
            set
            {
                _cellInfo_AddNewRep = value;
            }
        }
        private void UpdateSourceTriggerCurrentCellRep(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "MapUnitRep")
            {
                CellInfo_AddNewRep = obj.NewValue;
            }
        }

    }
    public class MapUnitRep
    {
        public string UnitNeedtoReplace { get; set; }
        public string ChangeTo { get; set; }
        public int UnitID { get; set; }
    }
    public class LibUnits
    {
        public int UnitID { get; set; }
        public string Unit { get; set; }
    }
}

