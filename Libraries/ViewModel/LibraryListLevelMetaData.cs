﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public List<LibPhraseCategory> listPhraseCategory { get; set; }
        public CommonDataGrid_ViewModel<MapLibMetaData> comonLibMetaData_VM { get; set; }
        public Visibility VisibilityLibMetaDataDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibMetaDataVisibility { get; set; }
        public ICommand CommandClosePopUpLibMetaData { get; private set; }
        public ICommand CommandShowPopUpAddLibMetaData { get; private set; }
        public ICommand CommandAddLibMetaData { get; private set; }
        public ICommand CommandAddMoreLibMetaData { get; private set; }
        private bool _IsShowPopUpLibMetaData { get; set; }
        public bool IsShowPopUpLibMetaData
        {
            get { return _IsShowPopUpLibMetaData; }
            set
            {
                if (_IsShowPopUpLibMetaData != value)
                {
                    _IsShowPopUpLibMetaData = value;
                    NotifyPropertyChanged("IsShowPopUpLibMetaData");
                }
            }
        }
        private ObservableCollection<MapLibMetaData> _lstLibMetaData { get; set; } = new ObservableCollection<MapLibMetaData>();
        public ObservableCollection<MapLibMetaData> lstLibMetaData
        {
            get { return _lstLibMetaData; }
            set
            {
                if (_lstLibMetaData != value)
                {
                    _lstLibMetaData = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibMetaData>>>(new PropertyChangedMessage<List<MapLibMetaData>>(null, _lstLibMetaData.ToList(), "Default List"));
                }
            }
        }
        private LibPhraseCategory _SelectedPhraseCategory { get; set; }
        public LibPhraseCategory SelectedPhraseCategory
        {
            get => _SelectedPhraseCategory;
            set
            {
                if (Equals(_SelectedPhraseCategory, value))
                {
                    return;
                }

                _SelectedPhraseCategory = value;
            }
        }
        private void BindLibMetaData()
        {
            VisibilityLibMetaDataDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibMetaDataDashboard_Loader");
            Task.Run(() =>
            {
                listPhraseCategory = objCommon.DbaseQueryReturnSqlList<LibPhraseCategory>("select PhraseCategoryID, PhraseCategory from Library_PhraseCategories order by PhraseCategory", Win_Administrator_VM.CommonListConn);
                var SAPLibMetaDataTable = objCommon.DbaseQueryReturnSqlList<MapLibMetaData>(
                "select b.PhraseCategoryID, b.PhraseCategory,  IsMaintainedInAccessFile, IsMaintainedInDashboardCPGListPriorities ,IsMaintainedInDashboardListLevelMetaData, a.TimeStamp as DateAdded from Library_ListLevelMetaData a, Library_PhraseCategories b where a.ListLevelMetaData_PhraseCategoryID = b.PhraseCategoryID  order by b.PhraseCategory", Win_Administrator_VM.CommonListConn);
                lstLibMetaData = new ObservableCollection<MapLibMetaData>(SAPLibMetaDataTable);
            });
            NotifyPropertyChanged("listPhraseCategory");
            NotifyPropertyChanged("comonLibMetaData_VM");
            VisibilityLibMetaDataDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibMetaDataDashboard_Loader");
        }
        private void RefreshGridLibMetaData(PropertyChangedMessage<string> flag)
        {
            BindLibMetaData();
            if (IsShowPopUpLibMetaData)
            {
                NotifyPropertyChanged("IsShowPopUpLibMetaData");
            }
        }

        private void CommandAddLibMetaDataExecute(object obj)
        {
            int cnt = 0;
            if (newIsMaintainedInAccessFile)
            {
                cnt += 1;
            }

            if (newIsMaintainedInDashboardCPGListPriorities)
            {
                cnt += 1;
            }

            if (newIsMaintainedInDashboardListLevelMetaData)
            {
                cnt += 1;
            }
            if (cnt > 1)
            {
                _notifier.ShowError("Only one checkbox should checked");
                return;
            }
            SaveLibMetaData();
            IsShowPopUpLibMetaData = false;
            NotifyPropertyChanged("IsShowPopUpLibMetaData");
            ClearPopUpVariableLibMetaData();
            PopUpContentAddLibMetaDataVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibMetaDataVisibility");
        }
        private bool CommandAddLibMetaDataCanExecute(object obj)
        {
            if (SelectedPhraseCategory == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveLibMetaData()
        {
            Library_ListLevelMetaData obj = new Library_ListLevelMetaData();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_ListLevelMetaData')", Win_Administrator_VM.CommonListConn));
                obj.LibraryListLevelMetaDataID = mx + 1;
                obj.ListLevelMetaData_PhraseCategoryID = SelectedPhraseCategory.PhraseCategoryID;
                obj.ListLevelMetaData_Description = SelectedPhraseCategory.PhraseCategory;
                obj.IsMaintainedInAccessFile = newIsMaintainedInAccessFile;
                obj.IsMaintainedInDashboardCPGListPriorities = newIsMaintainedInDashboardCPGListPriorities;
                obj.IsMaintainedInDashboardListLevelMetaData = newIsMaintainedInDashboardListLevelMetaData;
                obj.TimeStamp = objCommon.ESTTime();
                if (obj.IsMaintainedInAccessFile)
                {
                    obj.AccessFile_FieldName = SelectedPhraseCategory.PhraseCategory;
                }
                var chkAlreadyExist = context.Library_ListLevelMetaData.Count(x => x.ListLevelMetaData_PhraseCategoryID == SelectedPhraseCategory.PhraseCategoryID);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("PhraseCategory can't be duplicate");
                }
                else
                {
                    context.Library_ListLevelMetaData.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New List Level MetaData Saved Successfully.");
                }
            }

            ClearPopUpVariableLibMetaData();
            BindLibMetaData();

        }
        private void CommandAddMoreLibMetaDataExecute(object obj)
        {
            SaveLibPhraseCategories();
        }
        private void CommandClosePopUpExecuteLibMetaData(object obj)
        {
            IsShowPopUpLibMetaData = false;
            NotifyPropertyChanged("IsShowPopUpLibMetaData");
        }
        private void CommandShowPopUpAddLibMetaDataExecute(object obj)
        {
            ClearPopUpVariableLibMetaData();
            IsShowPopUpLibMetaData = true;
            NotifyPropertyChanged("IsShowPopUpLibMetaData");
            PopUpContentAddLibMetaDataVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibMetaDataVisibility");
        }

        public bool newIsMaintainedInAccessFile { get; set; } = false;
        public bool newIsMaintainedInDashboardCPGListPriorities { get; set; } = false;
        public bool newIsMaintainedInDashboardListLevelMetaData { get; set; } = false;

        public void ClearPopUpVariableLibMetaData()
        {
            newIsMaintainedInAccessFile = false;
            newIsMaintainedInDashboardCPGListPriorities = false;
            newIsMaintainedInDashboardListLevelMetaData = false;
            NotifyPropertyChanged("newIsMaintainedInAccessFile");
            NotifyPropertyChanged("newIsMaintainedInDashboardCPGListPriorities");
            NotifyPropertyChanged("newIsMaintainedInDashboardListLevelMetaData");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibMetaData()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseCategory", ColBindingName = "PhraseCategory", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsMaintainedInAccessFile", ColBindingName = "IsMaintainedInAccessFile", ColType = "CheckBoxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsMaintainedInDashboardCPGListPriorities", ColBindingName = "IsMaintainedInDashboardCPGListPriorities", ColType = "CheckBoxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsMaintainedInDashboardListLevelMetaData", ColBindingName = "IsMaintainedInDashboardListLevelMetaData", ColType = "CheckBoxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibMetaData
    {
        public int PhraseCategoryID { get; set; }
        public string PhraseCategory { get; set; }
        public bool IsMaintainedInAccessFile { get; set; }
        public bool IsMaintainedInDashboardCPGListPriorities { get; set; }
        public bool IsMaintainedInDashboardListLevelMetaData { get; set; }
        public DateTime? DateAdded { get; set; }
    }
    public class LibPhraseCategory
    {
        public int PhraseCategoryID { get; set; }
        public string PhraseCategory { get; set; }
    }
}
