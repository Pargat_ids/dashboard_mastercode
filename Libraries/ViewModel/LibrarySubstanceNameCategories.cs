﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibSubstanceNameCategory> comonLibSubstanceNameCategories_VM { get; set; }
        public string newSubstanceNameCategoryName { get; set; } = string.Empty;
        public string newSubstanceNameCategoryDesc { get; set; } = string.Empty;
        public Visibility VisibilityLibSubstanceNameCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibSubstanceNameCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibSubstanceNameCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibSubstanceNameCategories { get; private set; }
        public ICommand CommandAddLibSubstanceNameCategories { get; private set; }
        public ICommand CommandAddMoreLibSubstanceNameCategories { get; private set; }
        private bool _IsShowPopUpLibSubstanceNameCategories { get; set; }
        public bool IsShowPopUpLibSubstanceNameCategories
        {
            get { return _IsShowPopUpLibSubstanceNameCategories; }
            set
            {
                if (_IsShowPopUpLibSubstanceNameCategories != value)
                {
                    _IsShowPopUpLibSubstanceNameCategories = value;
                    NotifyPropertyChanged("_IsShowPopUpLibSubstanceNameCategories");
                }
            }
        }
        private ObservableCollection<MapLibSubstanceNameCategory> _lstLibSubstanceNameCategories { get; set; } = new ObservableCollection<MapLibSubstanceNameCategory>();
        public ObservableCollection<MapLibSubstanceNameCategory> lstLibSubstanceNameCategories
        {
            get { return _lstLibSubstanceNameCategories; }
            set
            {
                if (_lstLibSubstanceNameCategories != value)
                {
                    _lstLibSubstanceNameCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibSubstanceNameCategory>>>(new PropertyChangedMessage<List<MapLibSubstanceNameCategory>>(null, _lstLibSubstanceNameCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibSubstanceNameCategories()
        {
            VisibilityLibSubstanceNameCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibSubstanceNameCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibSubstanceNameCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibSubstanceNameCategory>(
                "select SubstanceNameCategoryName, SubstanceNameCategoryDescription, TimeStamp as DateAdded from Library_SubstanceNameCategories a order by SubstanceNameCategoryID desc", Win_Administrator_VM.CommonListConn);
                lstLibSubstanceNameCategories = new ObservableCollection<MapLibSubstanceNameCategory>(SAPLibSubstanceNameCategoriesTable);
            });
            NotifyPropertyChanged("comonLibSubstanceNameCategories_VM");
            VisibilityLibSubstanceNameCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibSubstanceNameCategoriesDashboard_Loader");
        }
        private void RefreshGridLibSubstanceNameCategories(PropertyChangedMessage<string> flag)
        {
            BindLibSubstanceNameCategories();
            if (IsShowPopUpLibSubstanceNameCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibSubstanceNameCategories");
            }
        }

        private void CommandAddLibSubstanceNameCategoriesExecute(object obj)
        {
            SaveLibSubstanceNameCategories();
            IsShowPopUpLibSubstanceNameCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceNameCategories");
            ClearPopUpVariableLibSubstanceNameCategories();
            PopUpContentAddLibSubstanceNameCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibSubstanceNameCategoriesVisibility");
        }
        private bool CommandAddLibSubstanceNameCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newSubstanceNameCategoryName.ToString()) || string.IsNullOrEmpty(newSubstanceNameCategoryDesc.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibSubstanceNameCategories()
        {
            Library_SubstanceNameCategories obj = new Library_SubstanceNameCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_SubstanceNameCategories')", Win_Administrator_VM.CommonListConn));
                obj.SubstanceNameCategoryID = mx + 1;
                obj.SubstanceNameCategoryName = newSubstanceNameCategoryName.Trim();
                obj.SubstanceNameCategoryDescription = newSubstanceNameCategoryDesc.Trim();
                obj.TimeStamp = System.DateTime.Now;
                obj.IsActive = true;

                var chkAlreadyExist = context.Library_SubstanceNameCategories.Count(x => x.SubstanceNameCategoryName.ToLower() == obj.SubstanceNameCategoryName.ToLower() || x.SubstanceNameCategoryDescription.ToLower() == obj.SubstanceNameCategoryDescription.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("SubstanceNameCategoryName/SubstanceNameCategoryDescription can't be duplicate");
                }
                else
                {
                    context.Library_SubstanceNameCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New SubstanceNameCategory Saved Successfully.");
                }
            }

            ClearPopUpVariableLibSubstanceNameCategories();
            BindLibSubstanceNameCategories();

        }
        private void CommandAddMoreLibSubstanceNameCategoriesExecute(object obj)
        {
            SaveLibSubstanceNameCategories();
        }
        private void CommandClosePopUpExecuteLibSubstanceNameCategories(object obj)
        {
            IsShowPopUpLibSubstanceNameCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceNameCategories");
        }
        private void CommandShowPopUpAddLibSubstanceNameCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibSubstanceNameCategories();
            IsShowPopUpLibSubstanceNameCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceNameCategories");
            PopUpContentAddLibSubstanceNameCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibSubstanceNameCategoriesVisibility");
        }

        public void ClearPopUpVariableLibSubstanceNameCategories()
        {
            newSubstanceNameCategoryName = "";
            newSubstanceNameCategoryDesc = "";
            NotifyPropertyChanged("newSubstanceNameCategoryName");
            NotifyPropertyChanged("newSubstanceNameCategoryDesc");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibSubstanceNameCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceNameCategoryName", ColBindingName = "SubstanceNameCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceNameCategoryDescription", ColBindingName = "SubstanceNameCategoryDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibSubstanceNameCategory
    {
        public string SubstanceNameCategoryName { get; set; }
        public string SubstanceNameCategoryDescription { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
