﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibSquashCharacter> comonLibSquashCharacters_VM { get; set; }
        public string newCharacter { get; set; } = string.Empty;
        public Visibility VisibilityLibSquashCharactersDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibSquashCharactersVisibility { get; set; }
        public ICommand CommandClosePopUpLibSquashCharacters { get; private set; }
        public ICommand CommandShowPopUpAddLibSquashCharacters { get; private set; }
        public ICommand CommandAddLibSquashCharacters { get; private set; }
        public ICommand CommandAddMoreLibSquashCharacters { get; private set; }
        private bool _IsShowPopUpLibSquashCharacters { get; set; }
        public bool IsShowPopUpLibSquashCharacters
        {
            get { return _IsShowPopUpLibSquashCharacters; }
            set
            {
                if (_IsShowPopUpLibSquashCharacters != value)
                {
                    _IsShowPopUpLibSquashCharacters = value;
                    NotifyPropertyChanged("_IsShowPopUpLibSquashCharacters");
                }
            }
        }
        private ObservableCollection<MapLibSquashCharacter> _lstLibSquashCharacters { get; set; } = new ObservableCollection<MapLibSquashCharacter>();
        public ObservableCollection<MapLibSquashCharacter> lstLibSquashCharacters
        {
            get { return _lstLibSquashCharacters; }
            set
            {
                if (_lstLibSquashCharacters != value)
                {
                    _lstLibSquashCharacters = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibSquashCharacter>>>(new PropertyChangedMessage<List<MapLibSquashCharacter>>(null, _lstLibSquashCharacters.ToList(), "Default List"));
                }
            }
        }

        private void BindLibSquashCharacters()
        {
            VisibilityLibSquashCharactersDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibSquashCharactersDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibSquashCharactersTable = objCommon.DbaseQueryReturnSqlList<MapLibSquashCharacter>(
                "select Character, TimeStamp as DateAdded from Library_SquashCharacters a order by SquashID desc", Win_Administrator_VM.CommonListConn);
                lstLibSquashCharacters = new ObservableCollection<MapLibSquashCharacter>(SAPLibSquashCharactersTable);
            });
            NotifyPropertyChanged("comonLibSquashCharacters_VM");
            VisibilityLibSquashCharactersDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibSquashCharactersDashboard_Loader");
        }
        private void RefreshGridLibSquashCharacters(PropertyChangedMessage<string> flag)
        {
            BindLibSquashCharacters();
            if (IsShowPopUpLibSquashCharacters)
            {
                NotifyPropertyChanged("IsShowPopUpLibSquashCharacters");
            }
        }

        private void CommandAddLibSquashCharactersExecute(object obj)
        {
            SaveLibSquashCharacters();
            IsShowPopUpLibSquashCharacters = false;
            NotifyPropertyChanged("IsShowPopUpLibSquashCharacters");
            ClearPopUpVariableLibSquashCharacters();
            PopUpContentAddLibSquashCharactersVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibSquashCharactersVisibility");
        }
        private bool CommandAddLibSquashCharactersCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newCharacter.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibSquashCharacters()
        {
            Library_SquashCharacters obj = new Library_SquashCharacters();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_SquashCharacters')", Win_Administrator_VM.CommonListConn));
                obj.SquashID = mx + 1;
                obj.Character = newCharacter.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_SquashCharacters.Count(x => x.Character.ToLower() == obj.Character.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Character can't be duplicate");
                }
                else
                {
                    context.Library_SquashCharacters.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Squash Character Saved Successfully.");
                }
            }

            ClearPopUpVariableLibSquashCharacters();
            BindLibSquashCharacters();

        }
        private void CommandAddMoreLibSquashCharactersExecute(object obj)
        {
            SaveLibSquashCharacters();
        }
        private void CommandClosePopUpExecuteLibSquashCharacters(object obj)
        {
            IsShowPopUpLibSquashCharacters = false;
            NotifyPropertyChanged("IsShowPopUpLibSquashCharacters");
        }
        private void CommandShowPopUpAddLibSquashCharactersExecute(object obj)
        {
            ClearPopUpVariableLibSquashCharacters();
            IsShowPopUpLibSquashCharacters = true;
            NotifyPropertyChanged("IsShowPopUpLibSquashCharacters");
            PopUpContentAddLibSquashCharactersVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibSquashCharactersVisibility");
        }

        public void ClearPopUpVariableLibSquashCharacters()
        {
            newCharacter = "";
            NotifyPropertyChanged("newCharacter");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibSquashCharacters()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Character", ColBindingName = "Character", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibSquashCharacter
    {
        public string Character { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
