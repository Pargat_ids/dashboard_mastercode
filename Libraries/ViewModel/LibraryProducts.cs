﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibProduct> comonLibProducts_VM { get; set; }
        public string newProductInternalCode { get; set; } = string.Empty;
        public string newProductName { get; set; } = string.Empty;
        public Visibility VisibilityLibProductsDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibProductsVisibility { get; set; }
        public ICommand CommandClosePopUpLibProducts { get; private set; }
        public ICommand CommandShowPopUpAddLibProducts { get; private set; }
        public ICommand CommandAddLibProducts { get; private set; }
        public ICommand CommandAddMoreLibProducts { get; private set; }
        private bool _IsShowPopUpLibProducts { get; set; }
        public bool IsShowPopUpLibProducts
        {
            get { return _IsShowPopUpLibProducts; }
            set
            {
                if (_IsShowPopUpLibProducts != value)
                {
                    _IsShowPopUpLibProducts = value;
                    NotifyPropertyChanged("_IsShowPopUpLibProducts");
                }
            }
        }
        private ObservableCollection<MapLibProduct> _lstLibProducts { get; set; } = new ObservableCollection<MapLibProduct>();
        public ObservableCollection<MapLibProduct> lstLibProducts
        {
            get { return _lstLibProducts; }
            set
            {
                if (_lstLibProducts != value)
                {
                    _lstLibProducts = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibProduct>>>(new PropertyChangedMessage<List<MapLibProduct>>(null, _lstLibProducts.ToList(), "Default List"));
                }
            }
        }

        private void BindLibProducts()
        {
            VisibilityLibProductsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibProductsDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibProductsTable = objCommon.DbaseQueryReturnSqlList<MapLibProduct>(
                "select ProductInternalCode, ProductName, TimeStamp as DateAdded from Library_Products a order by ProductID desc", Win_Administrator_VM.CommonListConn);
                lstLibProducts = new ObservableCollection<MapLibProduct>(SAPLibProductsTable);
            });
            NotifyPropertyChanged("comonLibProducts_VM");
            VisibilityLibProductsDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibProductsDashboard_Loader");
        }
        private void RefreshGridLibProducts(PropertyChangedMessage<string> flag)
        {
            BindLibProducts();
            if (IsShowPopUpLibProducts)
            {
                NotifyPropertyChanged("IsShowPopUpLibProducts");
            }
        }

        private void CommandAddLibProductsExecute(object obj)
        {
            SaveLibProducts();
            IsShowPopUpLibProducts = false;
            NotifyPropertyChanged("IsShowPopUpLibProducts");
            ClearPopUpVariableLibProducts();
            PopUpContentAddLibProductsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibProductsVisibility");
        }
        private bool CommandAddLibProductsCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newProductName.ToString()) || string.IsNullOrEmpty(newProductInternalCode.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibProducts()
        {
            Library_Products obj = new Library_Products();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Products')", Win_Administrator_VM.CommonListConn));
                obj.ProductID = mx + 1;
                obj.ProductName = newProductName.Trim();
                obj.ProductInternalCode = newProductInternalCode.Trim();
                obj.TimeStamp = objCommon.ESTTime();

                var chkAlreadyExist = context.Library_Products.Count(x => x.ProductName.ToLower() == obj.ProductName.ToLower() || x.ProductInternalCode.ToLower() == obj.ProductInternalCode.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("ProductName/ProductInternalCode can't be duplicate");
                }
                else
                {
                    context.Library_Products.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Product Saved Successfully.");
                }
            }

            ClearPopUpVariableLibProducts();
            BindLibProducts();

        }
        private void CommandAddMoreLibProductsExecute(object obj)
        {
            SaveLibProducts();
        }
        private void CommandClosePopUpExecuteLibProducts(object obj)
        {
            IsShowPopUpLibProducts = false;
            NotifyPropertyChanged("IsShowPopUpLibProducts");
        }
        private void CommandShowPopUpAddLibProductsExecute(object obj)
        {
            ClearPopUpVariableLibProducts();
            IsShowPopUpLibProducts = true;
            NotifyPropertyChanged("IsShowPopUpLibProducts");
            PopUpContentAddLibProductsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibProductsVisibility");
        }

        public void ClearPopUpVariableLibProducts()
        {
            newProductName = "";
            newProductInternalCode = "";
            NotifyPropertyChanged("newProductName");
            NotifyPropertyChanged("newProductInternalCode");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibProducts()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ProductName", ColBindingName = "ProductName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ProductInternalCode", ColBindingName = "ProductInternalCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibProduct
    {
        public string ProductName { get; set; }
        public string ProductInternalCode { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
