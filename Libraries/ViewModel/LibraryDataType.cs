﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibDataType> comonLibDataTypes_VM { get; set; }
        public string newDescription { get; set; } = string.Empty;
        public string newRegType { get; set; } = string.Empty;
        public Visibility VisibilityLibDataTypesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibDataTypesVisibility { get; set; }
        public ICommand CommandClosePopUpLibDataTypes { get; private set; }
        public ICommand CommandShowPopUpAddLibDataTypes { get; private set; }
        public ICommand CommandAddLibDataTypes { get; private set; }
        public ICommand CommandAddMoreLibDataTypes { get; private set; }
        private bool _IsShowPopUpLibDataTypes { get; set; }
        public bool IsShowPopUpLibDataTypes
        {
            get { return _IsShowPopUpLibDataTypes; }
            set
            {
                if (_IsShowPopUpLibDataTypes != value)
                {
                    _IsShowPopUpLibDataTypes = value;
                    NotifyPropertyChanged("_IsShowPopUpLibDataTypes");
                }
            }
        }
        private ObservableCollection<MapLibDataType> _lstLibDataTypes { get; set; } = new ObservableCollection<MapLibDataType>();
        public ObservableCollection<MapLibDataType> lstLibDataTypes
        {
            get { return _lstLibDataTypes; }
            set
            {
                if (_lstLibDataTypes != value)
                {
                    _lstLibDataTypes = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibDataType>>>(new PropertyChangedMessage<List<MapLibDataType>>(null, _lstLibDataTypes.ToList(), "Default List"));
                }
            }
        }

        private void BindLibDataTypes()
        {
            VisibilityLibDataTypesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibDataTypesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibDataTypesTable = objCommon.DbaseQueryReturnSqlList<MapLibDataType>(
                "select TypeDescription, REGType, TimeStamp as DateAdded  from Library_DataType a order by DataTypeID desc", Win_Administrator_VM.CommonListConn);
                lstLibDataTypes = new ObservableCollection<MapLibDataType>(SAPLibDataTypesTable);
            });
            NotifyPropertyChanged("comonLibDataTypes_VM");
            VisibilityLibDataTypesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibDataTypesDashboard_Loader");
        }
        private void RefreshGridLibDataTypes(PropertyChangedMessage<string> flag)
        {
            BindLibDataTypes();
            if (IsShowPopUpLibDataTypes)
            {
                NotifyPropertyChanged("IsShowPopUpLibDataTypes");
            }
        }

        private void CommandAddLibDataTypesExecute(object obj)
        {
            SaveLibDataTypes();
            IsShowPopUpLibDataTypes = false;
            NotifyPropertyChanged("IsShowPopUpLibDataTypes");
            ClearPopUpVariableLibDataTypes();
            PopUpContentAddLibDataTypesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibDataTypesVisibility");
        }
        private bool CommandAddLibDataTypesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newDescription.ToString()) || string.IsNullOrEmpty(newRegType.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibDataTypes()
        {
            Library_DataType obj = new Library_DataType();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_DataType')", Win_Administrator_VM.CommonListConn));
                obj.DataTypeID = mx + 1;
                obj.TypeDescription = newDescription.Trim();
                obj.REGTYPE = newRegType.Trim();
                obj.TimeStamp = objCommon.ESTTime();

                var chkAlreadyExist = context.Library_DataType.Count(x => x.TypeDescription.ToLower() == obj.TypeDescription.ToLower() || x.REGTYPE.ToLower() == obj.REGTYPE.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("TypeDescription/REGTYPE can't be duplicate");
                }
                else
                {
                    context.Library_DataType.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New DataType Saved Successfully.");
                }
            }

            ClearPopUpVariableLibDataTypes();
            BindLibDataTypes();

        }
        private void CommandAddMoreLibDataTypesExecute(object obj)
        {
            SaveLibDataTypes();
        }
        private void CommandClosePopUpExecuteLibDataTypes(object obj)
        {
            IsShowPopUpLibDataTypes = false;
            NotifyPropertyChanged("IsShowPopUpLibDataTypes");
        }
        private void CommandShowPopUpAddLibDataTypesExecute(object obj)
        {
            ClearPopUpVariableLibDataTypes();
            IsShowPopUpLibDataTypes = true;
            NotifyPropertyChanged("IsShowPopUpLibDataTypes");
            PopUpContentAddLibDataTypesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibDataTypesVisibility");
        }

        public void ClearPopUpVariableLibDataTypes()
        {
            newDescription = "";
            newRegType = "";
            NotifyPropertyChanged("newDescription");
            NotifyPropertyChanged("newRegType");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibDataTypes()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TypeDescription", ColBindingName = "TypeDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "REGType", ColBindingName = "REGType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibDataType
    {
        public string TypeDescription { get; set; }
        public string REGType { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
