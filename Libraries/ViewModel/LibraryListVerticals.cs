﻿using System.Windows.Input;
using System.Data;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibListVerticals> comonLibListVerticals_VM { get; set; }
        public string newListVertical { get; set; } = string.Empty;
        public Visibility VisibilityLibListVerticalsDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibListVerticalsVisibility { get; set; }
        public ICommand CommandClosePopUpLibListVerticals { get; private set; }
        public ICommand CommandShowPopUpAddLibListVerticals { get; private set; }
        public ICommand CommandAddLibListVerticals { get; private set; }
        public ICommand CommandAddMoreLibListVerticals { get; private set; }
        private bool _IsShowPopUpLibListVerticals { get; set; }
        public bool IsShowPopUpLibListVerticals
        {
            get { return _IsShowPopUpLibListVerticals; }
            set
            {
                if (_IsShowPopUpLibListVerticals != value)
                {
                    _IsShowPopUpLibListVerticals = value;
                    NotifyPropertyChanged("_IsShowPopUpLibListVerticals");
                }
            }
        }
        private ObservableCollection<MapLibListVerticals> _lstLibListVerticals { get; set; } = new ObservableCollection<MapLibListVerticals>();
        public ObservableCollection<MapLibListVerticals> lstLibListVerticals
        {
            get { return _lstLibListVerticals; }
            set
            {
                if (_lstLibListVerticals != value)
                {
                    _lstLibListVerticals = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibListVerticals>>>(new PropertyChangedMessage<List<MapLibListVerticals>>(null, _lstLibListVerticals.ToList(), "Default List"));
                }
            }
        }

        private void BindLibListVerticals()
        {
            VisibilityLibListVerticalsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibListVerticalsDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibListVerticalsTable = objCommon.DbaseQueryReturnSqlList<MapLibListVerticals>(
                "select ListVertical, TimeStamp as DateAdded from Library_ListVerticals a order by timestamp desc", Win_Administrator_VM.CommonListConn);
                lstLibListVerticals = new ObservableCollection<MapLibListVerticals>(SAPLibListVerticalsTable);
            });
            NotifyPropertyChanged("comonLibListVerticals_VM");
            VisibilityLibListVerticalsDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibListVerticalsDashboard_Loader");
        }
        private void RefreshGridLibListVerticals(PropertyChangedMessage<string> flag)
        {
            BindLibListVerticals();
            if (IsShowPopUpLibListVerticals)
            {
                NotifyPropertyChanged("IsShowPopUpLibListVerticals");
            }
        }

        private void CommandAddLibListVerticalsExecute(object obj)
        {
            SaveLibListVerticals();
            IsShowPopUpLibListVerticals = false;
            NotifyPropertyChanged("IsShowPopUpLibListVerticals");
            ClearPopUpVariableLibListVerticals();
            PopUpContentAddLibListVerticalsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibListVerticalsVisibility");
        }
        private bool CommandAddLibListVerticalsCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newListVertical.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibListVerticals()
        {
            Library_ListVerticals obj = new Library_ListVerticals();
            using (var context = new CRAModel())
            {
                
                obj.ListVertical = newListVertical.Trim();
                obj.TimeStamp = System.DateTime.Now;
                var val = objCommon.RemoveWhitespaceCharacter(newListVertical, stripCharacter);
                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID = phrId;

                var chkAlreadyExist = context.Library_ListVerticals.Count(x => x.PhraseID == obj.PhraseID);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("ListVertical can't be duplicate");
                }
                else
                {
                    context.Library_ListVerticals.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New ListVertical Saved Successfully.");
                }
            }

            ClearPopUpVariableLibListVerticals();
            BindLibListVerticals();

        }
        private void CommandAddMoreLibListVerticalsExecute(object obj)
        {
            SaveLibListVerticals();
        }
        private void CommandClosePopUpExecuteLibListVerticals(object obj)
        {
            IsShowPopUpLibListVerticals = false;
            NotifyPropertyChanged("IsShowPopUpLibListVerticals");
        }
        private void CommandShowPopUpAddLibListVerticalsExecute(object obj)
        {
            ClearPopUpVariableLibListVerticals();
            IsShowPopUpLibListVerticals = true;
            NotifyPropertyChanged("IsShowPopUpLibListVerticals");
            PopUpContentAddLibListVerticalsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibListVerticalsVisibility");
        }

        public void ClearPopUpVariableLibListVerticals()
        {
            newListVertical = "";
            NotifyPropertyChanged("newListVertical");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibListVerticals()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListVertical", ColBindingName = "ListVertical", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibListVerticals
    {
        public string ListVertical { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
