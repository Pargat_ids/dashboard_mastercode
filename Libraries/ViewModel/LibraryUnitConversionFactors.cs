﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibUnitConversion> comonLibUnitConversions_VM { get; set; }
        public List<LibUnit> listConversionUnit { get; set; }
        public Visibility VisibilityLibUnitConversionsDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibUnitConversionsVisibility { get; set; }
        public ICommand CommandClosePopUpLibUnitConversions { get; private set; }
        public ICommand CommandShowPopUpAddLibUnitConversions { get; private set; }
        public ICommand CommandAddLibUnitConversions { get; private set; }
        public ICommand CommandAddMoreLibUnitConversions { get; private set; }
        private bool _IsShowPopUpLibUnitConversions { get; set; }
        public bool IsShowPopUpLibUnitConversions
        {
            get { return _IsShowPopUpLibUnitConversions; }
            set
            {
                if (_IsShowPopUpLibUnitConversions != value)
                {
                    _IsShowPopUpLibUnitConversions = value;
                    NotifyPropertyChanged("_IsShowPopUpLibUnitConversions");
                }
            }
        }
        private ObservableCollection<MapLibUnitConversion> _lstLibUnitConversions { get; set; } = new ObservableCollection<MapLibUnitConversion>();
        public ObservableCollection<MapLibUnitConversion> lstLibUnitConversions
        {
            get { return _lstLibUnitConversions; }
            set
            {
                if (_lstLibUnitConversions != value)
                {
                    _lstLibUnitConversions = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibUnitConversion>>>(new PropertyChangedMessage<List<MapLibUnitConversion>>(null, _lstLibUnitConversions.ToList(), "Default List"));
                }
            }
        }
        private LibUnit _SelectedConversionUnit { get; set; }
        public LibUnit SelectedConversionUnit
        {
            get => _SelectedConversionUnit;
            set
            {
                if (Equals(_SelectedConversionUnit, value))
                {
                    return;
                }

                _SelectedConversionUnit = value;
            }
        }
       
        private void BindLibUnitConversions()
        {
            VisibilityLibUnitConversionsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibUnitConversionsDashboard_Loader");
            Task.Run(() =>
            {
                listConversionUnit = objCommon.DbaseQueryReturnSqlList<LibUnit>("select UnitID, UNit as UnitName from Library_UNits order by Unit", Win_Administrator_VM.CommonListConn);
                var SAPLibUnitConversionsTable = objCommon.DbaseQueryReturnSqlList<MapLibUnitConversion>(
                "select Unit, ConvertedUnit,ConversionFactor,ConversionPriorityOrder, TimeStamp as DateAdded from Library_UnitConversionFactors a order by UnitConversionFactorID desc", Win_Administrator_VM.CommonListConn);
                lstLibUnitConversions = new ObservableCollection<MapLibUnitConversion>(SAPLibUnitConversionsTable);
            });
            NotifyPropertyChanged("comonLibUnitConversions_VM");
            VisibilityLibUnitConversionsDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibUnitConversionsDashboard_Loader");
        }
        private void RefreshGridLibUnitConversions(PropertyChangedMessage<string> flag)
        {
            BindLibUnitConversions();
            if (IsShowPopUpLibUnitConversions)
            {
                NotifyPropertyChanged("IsShowPopUpLibUnitConversions");
            }
        }

        private void CommandAddLibUnitConversionsExecute(object obj)
        {
            SaveLibUnitConversions();
            IsShowPopUpLibUnitConversions = false;
            NotifyPropertyChanged("IsShowPopUpLibUnitConversions");
            ClearPopUpVariableLibUnitConversions();
            PopUpContentAddLibUnitConversionsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibUnitConversionsVisibility");
        }
        private bool CommandAddLibUnitConversionsCanExecute(object obj)
        {
            if (SelectedConversionUnit == null || string.IsNullOrEmpty(newConvertedUnit.ToString()) || string.IsNullOrEmpty(newConversionFactor.ToString()) || string.IsNullOrEmpty(newConversionPriorityOrder.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibUnitConversions()
        {
            Library_UnitConversionFactors obj = new Library_UnitConversionFactors();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_UnitConversionFactors')", Win_Administrator_VM.CommonListConn));
                obj.UnitConversionFactorID = mx + 1;
                obj.Unit = SelectedConversionUnit.UnitName.Trim();
                obj.ConvertedUnit = newConvertedUnit.Trim();
                obj.ConversionFactor = newConversionFactor;
                obj.ConversionPriorityOrder = newConversionPriorityOrder;
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_UnitConversionFactors.Count(x => x.Unit.ToLower() == obj.Unit.ToLower() && x.ConvertedUnit.ToLower() == obj.ConvertedUnit.ToLower() && x.ConversionFactor == obj.ConversionFactor && x.ConversionPriorityOrder == obj.ConversionPriorityOrder);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Unit/ConvertedUnit can't be duplicate");
                }
                else
                {
                    context.Library_UnitConversionFactors.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Unit Conversion Factor Saved Successfully.");
                }
            }

            ClearPopUpVariableLibUnitConversions();
            BindLibUnitConversions();

        }
        private void CommandAddMoreLibUnitConversionsExecute(object obj)
        {
            SaveLibUnitConversions();
        }
        private void CommandClosePopUpExecuteLibUnitConversions(object obj)
        {
            IsShowPopUpLibUnitConversions = false;
            NotifyPropertyChanged("IsShowPopUpLibUnitConversions");
        }
        private void CommandShowPopUpAddLibUnitConversionsExecute(object obj)
        {
            ClearPopUpVariableLibUnitConversions();
            IsShowPopUpLibUnitConversions = true;
            NotifyPropertyChanged("IsShowPopUpLibUnitConversions");
            PopUpContentAddLibUnitConversionsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibUnitConversionsVisibility");
        }
        public string newConvertedUnit { get; set; } = string.Empty;
        public double newConversionFactor { get; set; } = 0;
        public int newConversionPriorityOrder { get; set; } = 0;
        public void ClearPopUpVariableLibUnitConversions()
        {
            newConvertedUnit = "";
            newConversionFactor = 0;
            newConversionPriorityOrder = 0;
            NotifyPropertyChanged("newConvertedUnit");
            NotifyPropertyChanged("newConversionFactor");
            NotifyPropertyChanged("newConversionPriorityOrder");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibUnitConversions()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Unit", ColBindingName = "Unit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ConvertedUnit", ColBindingName = "ConvertedUnit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ConversionFactor", ColBindingName = "ConversionFactor", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ConversionPriorityOrder", ColBindingName = "ConversionPriorityOrder", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibUnitConversion
    {
        public string Unit { get; set; }
        public string ConvertedUnit { get; set; }
        public double ConversionFactor { get; set; }
        public int ConversionPriorityOrder { get; set; }
        public DateTime? DateAdded { get; set; }
    }
    public class LibUnit
    {
        public int UnitID { get; set; }
        public string UnitName { get; set; }
    }
}
