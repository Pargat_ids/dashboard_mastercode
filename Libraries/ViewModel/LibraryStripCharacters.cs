﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibStripCharacter> comonLibStripCharacters_VM { get; set; }
        public string newCharacterUnicode { get; set; } = string.Empty;
        public string newStripCharactersDesc { get; set; } = string.Empty;
        public Visibility VisibilityLibStripCharactersDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibStripCharactersVisibility { get; set; }
        public ICommand CommandClosePopUpLibStripCharacters { get; private set; }
        public ICommand CommandShowPopUpAddLibStripCharacters { get; private set; }
        public ICommand CommandAddLibStripCharacters { get; private set; }
        public ICommand CommandAddMoreLibStripCharacters { get; private set; }
        private bool _IsShowPopUpLibStripCharacters { get; set; }
        public bool IsShowPopUpLibStripCharacters
        {
            get { return _IsShowPopUpLibStripCharacters; }
            set
            {
                if (_IsShowPopUpLibStripCharacters != value)
                {
                    _IsShowPopUpLibStripCharacters = value;
                    NotifyPropertyChanged("_IsShowPopUpLibStripCharacters");
                }
            }
        }
        private ObservableCollection<MapLibStripCharacter> _lstLibStripCharacters { get; set; } = new ObservableCollection<MapLibStripCharacter>();
        public ObservableCollection<MapLibStripCharacter> lstLibStripCharacters
        {
            get { return _lstLibStripCharacters; }
            set
            {
                if (_lstLibStripCharacters != value)
                {
                    _lstLibStripCharacters = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibStripCharacter>>>(new PropertyChangedMessage<List<MapLibStripCharacter>>(null, _lstLibStripCharacters.ToList(), "Default List"));
                }
            }
        }

        private void BindLibStripCharacters()
        {
            VisibilityLibStripCharactersDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibStripCharactersDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibStripCharactersTable = objCommon.DbaseQueryReturnSqlList<MapLibStripCharacter>(
                "select CharacterUnicode, Description, TimeStamp as DateAdded from Library_StripCharacters a order by StripCharacterID desc", Win_Administrator_VM.CommonListConn);
                lstLibStripCharacters = new ObservableCollection<MapLibStripCharacter>(SAPLibStripCharactersTable);
            });
            NotifyPropertyChanged("comonLibStripCharacters_VM");
            VisibilityLibStripCharactersDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibStripCharactersDashboard_Loader");
        }
        private void RefreshGridLibStripCharacters(PropertyChangedMessage<string> flag)
        {
            BindLibStripCharacters();
            if (IsShowPopUpLibStripCharacters)
            {
                NotifyPropertyChanged("IsShowPopUpLibStripCharacters");
            }
        }

        private void CommandAddLibStripCharactersExecute(object obj)
        {
            SaveLibStripCharacters();
            IsShowPopUpLibStripCharacters = false;
            NotifyPropertyChanged("IsShowPopUpLibStripCharacters");
            ClearPopUpVariableLibStripCharacters();
            PopUpContentAddLibStripCharactersVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibStripCharactersVisibility");
        }
        private bool CommandAddLibStripCharactersCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newCharacterUnicode.ToString()) || string.IsNullOrEmpty(newStripCharactersDesc.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibStripCharacters()
        {
            Library_StripCharacters obj = new Library_StripCharacters();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_StripCharacters')", Win_Administrator_VM.CommonListConn));
                obj.StripCharacterID = mx + 1;
                obj.CharacterUnicode = newCharacterUnicode.Trim();
                obj.Description = newStripCharactersDesc.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_StripCharacters.Count(x => x.CharacterUnicode.ToLower() == obj.CharacterUnicode.ToLower() || x.Description.ToLower() == obj.Description.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("CharacterUnicode/Description can't be duplicate");
                }
                else
                {
                    context.Library_StripCharacters.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Strip Character Saved Successfully.");
                }
            }

            ClearPopUpVariableLibStripCharacters();
            BindLibStripCharacters();

        }
        private void CommandAddMoreLibStripCharactersExecute(object obj)
        {
            SaveLibStripCharacters();
        }
        private void CommandClosePopUpExecuteLibStripCharacters(object obj)
        {
            IsShowPopUpLibStripCharacters = false;
            NotifyPropertyChanged("IsShowPopUpLibStripCharacters");
        }
        private void CommandShowPopUpAddLibStripCharactersExecute(object obj)
        {
            ClearPopUpVariableLibStripCharacters();
            IsShowPopUpLibStripCharacters = true;
            NotifyPropertyChanged("IsShowPopUpLibStripCharacters");
            PopUpContentAddLibStripCharactersVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibStripCharactersVisibility");
        }

        public void ClearPopUpVariableLibStripCharacters()
        {
            newCharacterUnicode = "";
            newStripCharactersDesc = "";
            NotifyPropertyChanged("newCharacterUnicode");
            NotifyPropertyChanged("newStripCharactersDesc");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibStripCharacters()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CharacterUnicode", ColBindingName = "CharacterUnicode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Description", ColBindingName = "Description", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibStripCharacter
    {
        public string CharacterUnicode { get; set; }
        public string Description { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
