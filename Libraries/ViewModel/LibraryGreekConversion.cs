﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool _IsShowPopUpLibGreekConversion { get; set; }
        public Visibility PopUpContentAddLibGreekConversionVisibility { get; set; }
        public CommonDataGrid_ViewModel<MapLibGreekConversion> comonLibGreekConversion_VM { get; set; }
        public string newFindPattern { get; set; } = string.Empty;
        public string newReplaceWith { get; set; } = string.Empty;
        public ICommand CommandAddMoreLibGreekConversion { get; private set; }
        public ICommand CommandAddLibGreekConversion { get; private set; }
        public ICommand CommandClosePopUpLibGreekConversion { get; private set; }
        public ICommand CommandShowPopUpAddLibGreekConversion { get; private set; }
        public Visibility VisibilityLibGreekConversionDashboard_Loader { get; set; }
        public bool IsShowPopUpLibGreekConversion
        {
            get { return _IsShowPopUpLibGreekConversion; }
            set
            {
                if (_IsShowPopUpLibGreekConversion != value)
                {
                    _IsShowPopUpLibGreekConversion = value;
                    NotifyPropertyChanged("_IsShowPopUpLibGreekConversion");
                }
            }
        }
        private void RefreshGridLibGreekConversions(PropertyChangedMessage<string> flag)
        {
            BindLibGreekConversion();
            if (IsShowPopUpLibGreekConversion)
            {
                NotifyPropertyChanged("IsShowPopUpLibGreekConversion");
            }
        }
        private void CommandAddLibGreekConversionExecute(object obj)
        {
            SaveLibGreekConversion();
            IsShowPopUpLibGreekConversion = false;
            NotifyPropertyChanged("IsShowPopUpLibGreekConversion");
            ClearPopUpVariableLibGreekConversion();
            PopUpContentAddLibGreekConversionVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibGreekConversionVisibility");
        }
        private void CommandClosePopUpExecuteLibGreekConversion(object obj)
        {
            IsShowPopUpLibGreekConversion = false;
            NotifyPropertyChanged("IsShowPopUpLibGreekConversion");
        }
        private void CommandShowPopUpAddLibGreekConversionExecute(object obj)
        {
            ClearPopUpVariableLibGreekConversion();
            IsShowPopUpLibGreekConversion = true;
            NotifyPropertyChanged("IsShowPopUpLibGreekConversion");
            PopUpContentAddLibGreekConversionVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibGreekConversionVisibility");
        }
        private bool CommandAddLibGreekConversionCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newFindPattern.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveLibGreekConversion()
        {
            Library_ConversionCharacters obj = new Library_ConversionCharacters();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_ConversionCharacters')", Win_Administrator_VM.CommonListConn));
                obj.ID = mx + 1;
                obj.Pattern = newFindPattern.Trim();
                obj.ReplaceWith = newReplaceWith;
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_ConversionCharacters.Count(x => x.Pattern == obj.Pattern);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("HTML Conversion Character can't be duplicate");
                }
                else
                {
                    context.Library_ConversionCharacters.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New HTML Conversion Character Saved Successfully.");
                }
            }

            ClearPopUpVariableLibGreekConversion();
            BindLibGreekConversion();

        }
        public void ClearPopUpVariableLibGreekConversion()
        {
            newFindPattern = "";
            NotifyPropertyChanged("newFindPattern");
        }
        private void CommandAddMoreLibGreekConversionExecute(object obj)
        {
            SaveLibGreekConversion();
        }
        private void BindLibGreekConversion()
        {
            List<MapLibGreekConversion> newFinalList = new List<MapLibGreekConversion>();
            VisibilityLibGreekConversionDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibGreekConversionDashboard_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var SAPLibGreekConversionTable = objCommon.DbaseQueryReturnSqlList<MapLibGreekConversion>(
                    "select ID, Pattern, ReplaceWith, TimeStamp as DateAdded from Library_ConversionCharacters order by ID desc", Win_Administrator_VM.CommonListConn);
                    lstLibGreekConversion = new ObservableCollection<MapLibGreekConversion>(SAPLibGreekConversionTable);
                }
                NotifyPropertyChanged("comonLibGreekConversion_VM");
                VisibilityLibGreekConversionDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibGreekConversionDashboard_Loader");
            });

        }
        private void CallbackHyperlinkGreekConversion(NotificationMessageAction<object> obj)
        {
            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        private ObservableCollection<MapLibGreekConversion> _lstLibGreekConversion { get; set; } = new ObservableCollection<MapLibGreekConversion>();
        public ObservableCollection<MapLibGreekConversion> lstLibGreekConversion
        {
            get { return _lstLibGreekConversion; }
            set
            {
                if (_lstLibGreekConversion != value)
                {
                    _lstLibGreekConversion = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibGreekConversion>>>(new PropertyChangedMessage<List<MapLibGreekConversion>>(null, _lstLibGreekConversion.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnLibGreekConversion()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Pattern", ColBindingName = "Pattern", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ReplaceWith", ColBindingName = "ReplaceWith", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibGreekConversion
    {
        public int ID { get; set; }
        public string Pattern { get; set; }
        public string ReplaceWith { get; set; }
        public DateTime? DateAdded { get; set; }

    }
}

