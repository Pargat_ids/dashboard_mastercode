﻿using System.Windows.Input;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using MessageBox = System.Windows.Forms.MessageBox;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool _IsShowPopUpLibPhysicalForm { get; set; }
        public Visibility PopUpContentAddLibPhysicalFormVisibility { get; set; }
        public CommonDataGrid_ViewModel<MapLibPhysicalForm> comonLibPhysicalForm_VM { get; set; }
        public string newPhysicalForm { get; set; } = string.Empty;
        public ICommand CommandAddMoreLibPhysicalForm { get; private set; }
        public ICommand CommandAddLibPhysicalForm { get; private set; }
        public ICommand CommandClosePopUpLibPhysicalForm { get; private set; }
        public ICommand CommandShowPopUpAddLibPhysicalForm { get; private set; }
        public Visibility VisibilityLibPhysicalFormDashboard_Loader { get; set; }
        public bool IsShowPopUpLibPhysicalForm
        {
            get { return _IsShowPopUpLibPhysicalForm; }
            set
            {
                if (_IsShowPopUpLibPhysicalForm != value)
                {
                    _IsShowPopUpLibPhysicalForm = value;
                    NotifyPropertyChanged("IsShowPopUpLibPhysicalForm");
                }
            }
        }
        private void RefreshGridLibPhysicalForm(PropertyChangedMessage<string> flag)
        {
            BindLibPhysicalForm();
            if (IsShowPopUpLibPhysicalForm)
            {
                NotifyPropertyChanged("IsShowPopUpLibPhysicalForm");
            }
        }
        private void CommandAddLibPhysicalFormExecute(object obj)
        {
            SaveLibPhysicalForm();
            IsShowPopUpLibPhysicalForm = false;
            NotifyPropertyChanged("IsShowPopUpLibPhysicalForm");
            ClearPopUpVariableLibPhysicalForm();
            PopUpContentAddLibPhysicalFormVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibPhysicalFormVisibility");
        }
        private void CommandClosePopUpExecuteLibPhysicalForm(object obj)
        {
            IsShowPopUpLibPhysicalForm = false;
            NotifyPropertyChanged("IsShowPopUpLibPhysicalForm");
        }
        private void CommandShowPopUpAddLibPhysicalFormExecute(object obj)
        {
            ClearPopUpVariableLibPhysicalForm();
            IsShowPopUpLibPhysicalForm = true;
            NotifyPropertyChanged("IsShowPopUpLibPhysicalForm");
            PopUpContentAddLibPhysicalFormVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibPhysicalFormVisibility");
        }
        private bool CommandAddLibPhysicalFormCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newPhysicalForm.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public string Replacement(string phrCat, string newPhr)
        {
            var repPhr = string.Empty;
            using (var context = new CRAModel())
            {
                // check in replacement Table
                var chkCaseControl = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == phrCat && x.IsCaseControlledPhrase == true).FirstOrDefault();
                if (chkCaseControl != null)
                {
                    var replaceID = context.Map_ControlPhrase_Replacement.AsNoTracking().Where(x => x.Orginal_Value == newPhr.Trim()).Select(y => y.Replace_ControlPhrase_ID).FirstOrDefault();
                    if (replaceID != null)
                    {
                        repPhr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseID == replaceID).Select(y => y.Phrase).FirstOrDefault();
                        return repPhr;
                    }
                }
                return repPhr;
            }
        }
        public void SaveLibPhysicalForm()
        {

            Library_PhysicalForm obj = new Library_PhysicalForm();
            using (var context = new CRAModel())
            {

                var repPhr = Replacement("Physical Form (Phrase)", newPhysicalForm);
                if (string.IsNullOrEmpty(repPhr)==false)
                {
                    var resultBox = MessageBox.Show("Exists in replacement table, replaced by - " + repPhr + Environment.NewLine + "Do you want to Continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (resultBox == DialogResult.Yes)
                    {
                        newPhysicalForm = repPhr;
                        NotifyPropertyChanged("newPhysicalForm");
                    }
                    else
                    {
                        return;
                    }
                }

                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_PhysicalForm')", Win_Administrator_VM.CommonListConn));
                obj.LibraryPhysicalFormID = mx + 1;
                var phrVal = newPhysicalForm.Trim();
                var PhysicalFormVal = objCommon.RemoveWhitespaceCharacter(phrVal, stripCharacter);
                obj.PhraseID_PhysicalForm = 0;
                obj.PhraseHash_LowerCase = objCommon.GenerateSHA256String(PhysicalFormVal.ToLower());
                obj.IsActive = true;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(phrVal);
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = PhysicalFormVal,
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID_PhysicalForm = phrId;

                var chkAlreadyExist = context.Library_PhysicalForm.Count(x => x.PhraseHash_LowerCase == obj.PhraseHash_LowerCase);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("PhysicalForm can't be duplicate");
                }
                else
                {
                    context.Library_PhysicalForm.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New PhysicalForm Saved Successfully.");
                }
            }

            ClearPopUpVariableLibPhysicalForm();
            BindLibPhysicalForm();

        }
        public void ClearPopUpVariableLibPhysicalForm()
        {
            newPhysicalForm = "";
            NotifyPropertyChanged("newPhysicalForm");
        }
        private void CommandAddMoreLibPhysicalFormExecute(object obj)
        {
            SaveLibPhysicalForm();
        }
        private void CallbackHyperlinkPhysicalForm(NotificationMessageAction<object> obj)
        {
            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        private void BindLibPhysicalForm()
        {
            List<MapLibPhysicalForm> newFinalList = new List<MapLibPhysicalForm>();
            VisibilityLibPhysicalFormDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibPhysicalFormDashboard_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var SAPLibPhysicalFormTable = objCommon.DbaseQueryReturnSqlList<MapLibPhysicalForm>(
                    "select b.PhraseID, Phrase as PhysicalForm, 0 as No_of_active_lists, a.TimeStamp as DateAdded from Library_PhysicalForm a, Library_Phrases b where a.PhraseId_PhysicalForm = b.PhraseId order by LibraryPhysicalFormID desc", Win_Administrator_VM.CommonListConn);
                    if (SAPLibPhysicalFormTable.Any())
                    {
                        var onlyPhrases = SAPLibPhysicalFormTable.Select(x => x.PhraseID).Distinct().ToList();
                        var CatIdListField = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "List Field Name").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var subrootcat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Physical Form (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var findQsxxxQsid = (from d1 in context.QSxxx_Phrases.AsNoTracking().AsNoTracking().Where(x => x.PhraseCategoryID == subrootcat)
                                             join d2 in onlyPhrases
                                             on d1.PhraseID equals d2
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID, d1.QSID_ID, d3.QSID }).Distinct().ToList();
                        var onlyQsids = findQsxxxQsid.Select(y => y.QSID_ID).Distinct().ToList();
                        var onlyQsxxPhrID = (from d1 in context.QSxxx_ListDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == CatIdListField)
                                             join d2 in onlyQsids
                                             on d1.QSID_ID equals d2
                                             select new { d1.PhraseID, d1.QSID_ID }).Distinct().ToList();
                        var qsxxListDic = (from d1 in onlyQsxxPhrID
                                           join d2 in context.Library_Phrases.AsNoTracking()
                                           on d1.PhraseID equals d2.PhraseID
                                           select new { d1.QSID_ID, d2.PhraseID, d2.Phrase }).Distinct().ToList();
                        var combineFinal = (from d1 in findQsxxxQsid
                                            join d2 in qsxxListDic
                                            on d1.QSID_ID
                                            equals d2.QSID_ID
                                            select new { d1.PhraseID, QSID = d1.QSID + "(" + d2.Phrase + ")", QSID_ID = d1.QSID_ID }).Distinct().ToList();
                        var joinQsids = (from d1 in combineFinal
                                         group d1 by d1.PhraseID into g
                                         select new
                                         {
                                             PhraseID = g.Key,
                                             displayValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID).ToList()),
                                             navigateValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID_ID).ToList()),
                                             count = g.Count(),
                                         }).Distinct().ToList();
                        newFinalList = (from d1 in SAPLibPhysicalFormTable
                                        join d2 in joinQsids
                                        on d1.PhraseID equals d2.PhraseID into tg
                                        select new MapLibPhysicalForm
                                        {
                                            DateAdded = d1.DateAdded,
                                            PhraseID = d1.PhraseID,
                                            PhysicalForm = d1.PhysicalForm,
                                            No_of_active_lists = tg.Select(x => x.count).FirstOrDefault(),
                                            Active_Lists = ConvertInfoToHyoerlinkVal(tg.Select(x => x.displayValue).FirstOrDefault(), tg.Select(x => x.navigateValue).FirstOrDefault()),
                                        }).ToList();
                    }
                    lstLibPhysicalForm = new ObservableCollection<MapLibPhysicalForm>(newFinalList);
                }
                NotifyPropertyChanged("comonLibPhysicalForm_VM");
                VisibilityLibPhysicalFormDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibPhysicalFormDashboard_Loader");
            });

        }
        private ObservableCollection<MapLibPhysicalForm> _lstLibPhysicalForm { get; set; } = new ObservableCollection<MapLibPhysicalForm>();
        public ObservableCollection<MapLibPhysicalForm> lstLibPhysicalForm
        {
            get { return _lstLibPhysicalForm; }
            set
            {
                if (_lstLibPhysicalForm != value)
                {
                    _lstLibPhysicalForm = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibPhysicalForm>>>(new PropertyChangedMessage<List<MapLibPhysicalForm>>(null, _lstLibPhysicalForm.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnLibPhysicalForm()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhysicalForm", ColBindingName = "PhysicalForm", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_active_lists", ColBindingName = "No_of_active_lists", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Active_Lists", ColBindingName = "Active_Lists", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibPhysicalForm
    {
        public int PhraseID { get; set; }
        public string PhysicalForm { get; set; }
        public int No_of_active_lists { get; set; }
        public List<HyperLinkDataValue> Active_Lists { get; set; }

        public DateTime? DateAdded { get; set; }
    }
}
