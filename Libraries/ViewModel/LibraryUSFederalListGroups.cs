﻿using System.Windows.Input;
using System.Data;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibUSFederal> comonLibUSFederal_VM { get; set; }
        public string newUSFederalName { get; set; } = string.Empty;
        public string newUSFederalCode { get; set; } = string.Empty;
        public Visibility VisibilityLibUSFederalDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibUSFederalVisibility { get; set; }
        public ICommand CommandClosePopUpLibUSFederal { get; private set; }
        public ICommand CommandShowPopUpAddLibUSFederal { get; private set; }
        public ICommand CommandAddLibUSFederal { get; private set; }
        public ICommand CommandAddMoreLibUSFederal { get; private set; }
        private bool _IsShowPopUpLibUSFederal { get; set; }
        public bool IsShowPopUpLibUSFederal
        {
            get { return _IsShowPopUpLibUSFederal; }
            set
            {
                if (_IsShowPopUpLibUSFederal != value)
                {
                    _IsShowPopUpLibUSFederal = value;
                    NotifyPropertyChanged("_IsShowPopUpLibUSFederal");
                }
            }
        }
        private ObservableCollection<MapLibUSFederal> _lstLibUSFederal { get; set; } = new ObservableCollection<MapLibUSFederal>();
        public ObservableCollection<MapLibUSFederal> lstLibUSFederal
        {
            get { return _lstLibUSFederal; }
            set
            {
                if (_lstLibUSFederal != value)
                {
                    _lstLibUSFederal = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibUSFederal>>>(new PropertyChangedMessage<List<MapLibUSFederal>>(null, _lstLibUSFederal.ToList(), "Default List"));
                }
            }
        }

        private void BindLibUSFederal()
        {
            VisibilityLibUSFederalDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibUSFederalDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibUSFederalTable = objCommon.DbaseQueryReturnSqlList<MapLibUSFederal>(
                "select code, Name, TimeStamp as DateAdded from Library_USFederal_ListGroups a order by USFederalListGroupID desc", Win_Administrator_VM.CommonListConn);
                lstLibUSFederal = new ObservableCollection<MapLibUSFederal>(SAPLibUSFederalTable);
            });
            NotifyPropertyChanged("comonLibUSFederal_VM");
            VisibilityLibUSFederalDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibUSFederalDashboard_Loader");
        }
        private void RefreshGridLibUSFederal(PropertyChangedMessage<string> flag)
        {
            BindLibUSFederal();
            if (IsShowPopUpLibUSFederal)
            {
                NotifyPropertyChanged("IsShowPopUpLibUSFederal");
            }
        }

        private void CommandAddLibUSFederalExecute(object obj)
        {
            SaveLibUSFederal();
            IsShowPopUpLibUSFederal = false;
            NotifyPropertyChanged("IsShowPopUpLibUSFederal");
            ClearPopUpVariableLibUSFederal();
            PopUpContentAddLibUSFederalVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibUSFederalVisibility");
        }
        private bool CommandAddLibUSFederalCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newUSFederalCode.ToString()) || string.IsNullOrEmpty(newUSFederalName.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibUSFederal()
        {
            Library_USFederal_ListGroups obj = new Library_USFederal_ListGroups();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_USFederal_ListGroups')", Win_Administrator_VM.CommonListConn));
                obj.USFederalListGroupID = mx + 1;
                var val = objCommon.RemoveWhitespaceCharacter(newUSFederalName, stripCharacter);
                obj.Code = newUSFederalCode.Trim();
                obj.Name = val.Trim();
                obj.TimeStamp = objCommon.ESTTime();
                

                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID = phrId;

                var chkAlreadyExist = context.Library_USFederal_ListGroups.Count(x => x.Code.ToLower() == obj.Code.ToLower() || x.PhraseID == obj.PhraseID);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("USFederal/USFederalCode can't be duplicate");
                }
                else
                {
                    context.Library_USFederal_ListGroups.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New US Federal List Group Saved Successfully.");
                }
            }

            ClearPopUpVariableLibUSFederal();
            BindLibUSFederal();

        }
        private void CommandAddMoreLibUSFederalExecute(object obj)
        {
            SaveLibUSFederal();
        }
        private void CommandClosePopUpExecuteLibUSFederal(object obj)
        {
            IsShowPopUpLibUSFederal = false;
            NotifyPropertyChanged("IsShowPopUpLibUSFederal");
        }
        private void CommandShowPopUpAddLibUSFederalExecute(object obj)
        {
            ClearPopUpVariableLibUSFederal();
            IsShowPopUpLibUSFederal = true;
            NotifyPropertyChanged("IsShowPopUpLibUSFederal");
            PopUpContentAddLibUSFederalVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibUSFederalVisibility");
        }

        public void ClearPopUpVariableLibUSFederal()
        {
            newUSFederalCode = "";
            newUSFederalName = "";
            NotifyPropertyChanged("newUSFederalCode");
            NotifyPropertyChanged("newUSFederalName");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibUSFederal()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Code", ColBindingName = "Code", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Name", ColBindingName = "Name", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibUSFederal
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public DateTime? DateAdded { get; set; }
    }
}
