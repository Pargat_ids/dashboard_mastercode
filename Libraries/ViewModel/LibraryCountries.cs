﻿using System.Windows.Input;
using System.Data;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibCountries> comonLibCountries_VM { get; set; }
        public Visibility VisibilityLibCountriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibCountriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibCountries { get; private set; }
        public ICommand CommandShowPopUpAddLibCountries { get; private set; }
        public ICommand CommandAddLibCountries { get; private set; }
        public ICommand CommandAddMoreLibCountries { get; private set; }
        private bool _IsShowPopUpLibCountries { get; set; }
        public bool IsShowPopUpLibCountries
        {
            get { return _IsShowPopUpLibCountries; }
            set
            {
                if (_IsShowPopUpLibCountries != value)
                {
                    _IsShowPopUpLibCountries = value;
                    NotifyPropertyChanged("_IsShowPopUpLibCountries");
                }
            }
        }
        private ObservableCollection<MapLibCountries> _lstLibCountries { get; set; } = new ObservableCollection<MapLibCountries>();
        public ObservableCollection<MapLibCountries> lstLibCountries
        {
            get { return _lstLibCountries; }
            set
            {
                if (_lstLibCountries != value)
                {
                    _lstLibCountries = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibCountries>>>(new PropertyChangedMessage<List<MapLibCountries>>(null, _lstLibCountries.ToList(), "Default List"));
                }
            }
        }
        
        private void BindLibCountries()
        {
            VisibilityLibCountriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibCountriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibCountriesTable = objCommon.DbaseQueryReturnSqlList<MapLibCountries>(
                "select CountryName, I4CCode, I4FCode, SAPCode, ISOCode,ADMName, ADMCode, Ident,TimeStamp as DateAdded from Library_Countries a order by CountryID desc", Win_Administrator_VM.CommonListConn);
                lstLibCountries = new ObservableCollection<MapLibCountries>(SAPLibCountriesTable);
            });
            NotifyPropertyChanged("comonLibCountries_VM");
            VisibilityLibCountriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibCountriesDashboard_Loader");
        }
        private void RefreshGridLibCountries(PropertyChangedMessage<string> flag)
        {
            BindLibCountries();
            if (IsShowPopUpLibCountries)
            {
                NotifyPropertyChanged("IsShowPopUpLibCountries");
            }
        }

        private void CommandAddLibCountriesExecute(object obj)
        {
            SaveLibCountries();
            IsShowPopUpLibCountries = false;
            NotifyPropertyChanged("IsShowPopUpLibCountries");
            ClearPopUpVariableLibCountries();
            PopUpContentAddLibCountriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibCountriesVisibility");
        }
        private bool CommandAddLibCountriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newCountry.ToString()) || string.IsNullOrEmpty(newI4CCode.ToString()) || string.IsNullOrEmpty(newI4FCode.ToString()) || string.IsNullOrEmpty(newSAPCode.ToString()) || string.IsNullOrEmpty(newISOCode.ToString()) || string.IsNullOrEmpty(newADMName.ToString()) || string.IsNullOrEmpty(newADMCode.ToString()) || string.IsNullOrEmpty(newIdent.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibCountries()
        {
            Library_Countries obj = new Library_Countries();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Countries')", Win_Administrator_VM.CommonListConn));
                obj.CountryID = mx + 1;
                var val = objCommon.RemoveWhitespaceCharacter(newCountry, stripCharacter);
                obj.CountryName = val.Trim();
                obj.I4CCode = newI4CCode.Trim();
                obj.I4FCode = newI4FCode;
                obj.SAPCode = newSAPCode;
                obj.ISOCode = newISOCode;
                obj.ADMCode = newADMCode;
                obj.ADMName = newADMName;
                obj.IDENT = newIdent;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                        ARCode = null,
                        PhraseKey = null,
                        
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID = phrId;

                var chkAlreadyExist = context.Library_Countries.Count(x => x.CountryName.ToLower() == obj.CountryName.ToLower() || x.I4CCode.ToLower() == obj.I4CCode.ToLower() || x.IDENT == obj.IDENT || x.I4FCode.ToLower() == obj.I4FCode.ToLower() || x.SAPCode.ToLower() == obj.SAPCode.ToLower() || x.ADMCode.ToLower() == obj.ADMCode.ToLower() || x.ADMName.ToLower() == obj.ADMName.ToLower() || x.ISOCode.ToLower() == obj.ISOCode.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("CountryName/I4CCode/I4FCode/SAPCode/ADMCode/ADMName/ISOCode/IDENT can't be duplicate");
                }
                else
                {
                    context.Library_Countries.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Countries Saved Successfully.");
                }
            }

            ClearPopUpVariableLibCountries();
            BindLibCountries();

        }
        private void CommandAddMoreLibCountriesExecute(object obj)
        {
            SaveLibCountries();
        }
        private void CommandClosePopUpExecuteLibCountries(object obj)
        {
            IsShowPopUpLibCountries = false;
            NotifyPropertyChanged("IsShowPopUpLibCountries");
        }
        private void CommandShowPopUpAddLibCountriesExecute(object obj)
        {
            ClearPopUpVariableLibCountries();
            IsShowPopUpLibCountries = true;
            NotifyPropertyChanged("IsShowPopUpLibCountries");
            PopUpContentAddLibCountriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibCountriesVisibility");
        }
        public string newCountry { get; set; } = string.Empty;
        public string newI4CCode { get; set; } = string.Empty;
        public string newI4FCode { get; set; } = string.Empty;
        public string newSAPCode { get; set; } = string.Empty;
        public string newISOCode { get; set; } = string.Empty;
        public string newADMName { get; set; } = string.Empty;
        public string newADMCode { get; set; } = string.Empty;
        public int newIdent { get; set; } = 0;
        public void ClearPopUpVariableLibCountries()
        {
            newCountry = "";
            newI4CCode = "";
            newI4FCode = "";
            newSAPCode = "";
            newISOCode = "";
            newADMName = "";
            newADMCode = "";
            newIdent = 0;
            NotifyPropertyChanged("newCountry");
            NotifyPropertyChanged("newI4CCode");
            NotifyPropertyChanged("newI4FCode");
            NotifyPropertyChanged("newSAPCode");
            NotifyPropertyChanged("newISOCode");
            NotifyPropertyChanged("newADMName");
            NotifyPropertyChanged("newADMCode");
            NotifyPropertyChanged("newIdent");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibCountries()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CountryName", ColBindingName = "CountryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "I4CCode", ColBindingName = "I4CCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "I4FCode", ColBindingName = "I4FCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAPCode", ColBindingName = "SAPCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ISOCode", ColBindingName = "ISOCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ADMName", ColBindingName = "ADMName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ADMCode", ColBindingName = "ADMCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Ident", ColBindingName = "Ident", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibCountries
    {
        public string CountryName { get; set; }
        public string I4CCode { get; set; }
        public string I4FCode { get; set; }
        public string SAPCode { get; set; }
        public string ISOCode { get; set; }
        public string ADMName { get; set; }
        public string ADMCode { get; set; }
        public int Ident { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
