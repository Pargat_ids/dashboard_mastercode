﻿using System.Windows.Input;
using System.Windows.Controls;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapControlPhraseRep> comonControlPhraseRep_VM { get; set; }
        public Visibility VisibilityControlPhraseRepDashboard_Loader { get; set; }
        public List<LibPhrase> ListChangeToControlPhrase { get; set; }
        public ICommand CommandClosePopUpControlPhraseRep { get; private set; }
        public ICommand CommandShowPopUpAddControlPhraseRep { get; private set; }
        public bool IsEditControlPhraseRep { get; set; } = true;
        public Visibility PopUpContentAddControlPhraseRepVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<MapControlPhraseRep> _lstControlPhraseRep { get; set; } = new ObservableCollection<MapControlPhraseRep>();
        public ObservableCollection<MapControlPhraseRep> lstControlPhraseRep
        {
            get { return _lstControlPhraseRep; }
            set
            {
                if (_lstControlPhraseRep != value)
                {
                    _lstControlPhraseRep = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapControlPhraseRep>>>(new PropertyChangedMessage<List<MapControlPhraseRep>>(null, _lstControlPhraseRep.ToList(), "Default List"));
                }
            }
        }
        private void BindControlPhraseRep()
        {
            VisibilityControlPhraseRepDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityControlPhraseRepDashboard_Loader");
            Task.Run(() =>
            {
                var SAPControlPhraseRepTable = objCommon.DbaseQueryReturnSqlList<MapControlPhraseRep>(
               "select Orginal_Value as ControlPhraseNeedtoReplace, Replace_ControlPhrase_ID as PhraseID, Phrase as ChangeTo " +
                " from Map_ControlPhrase_Replacement a, Library_Phrases b where Replace_ControlPhrase_ID = b.PhraseID order by MapReplaceControlPhrase_ID desc", Win_Administrator_VM.CommonListConn);

                lstControlPhraseRep = new ObservableCollection<MapControlPhraseRep>(SAPControlPhraseRepTable);
            });
            NotifyPropertyChanged("comonControlPhraseRep_VM");
            VisibilityControlPhraseRepDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityControlPhraseRepDashboard_Loader");
        }
        private bool _IsShowPopUpControlPhraseRep { get; set; }
        public bool IsShowPopUpControlPhraseRep
        {
            get { return _IsShowPopUpControlPhraseRep; }
            set
            {
                if (_IsShowPopUpControlPhraseRep != value)
                {
                    _IsShowPopUpControlPhraseRep = value;
                    NotifyPropertyChanged("IsShowPopUpControlPhraseRep");
                }
            }
        }
        private void RunControlPhraseRep()
        {
            BindControlPhraseRep();

        }

        private void RefreshGridControlPhraseRep(PropertyChangedMessage<string> flag)
        {
            BindControlPhraseRep();
            if (IsShowPopUpControlPhraseRep)
            {
                NotifyPropertyChanged("IsShowPopUpControlPhraseRep");
            }
        }
        public ICommand CommandAddControlPhraseRep { get; private set; }
        private void CommandAddControlPhraseRepExecute(object obj)
        {
            SaveControlPhraseRep();
            IsShowPopUpControlPhraseRep = false;
            IsEditControlPhraseRep = true;
            NotifyPropertyChanged("IsEditControlPhraseRep");
            ClearPopUpVariableControlPhraseRep();
            PopUpContentAddControlPhraseRepVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddControlPhraseRepVisibility");
        }
        private bool CommandAddControlPhraseRepCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newControlPhraseRep.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private LibPhrase _SelectedChangeToControlPhrase { get; set; }
        public LibPhrase SelectedChangeToControlPhrase
        {
            get => _SelectedChangeToControlPhrase;
            set
            {
                if (Equals(_SelectedChangeToControlPhrase, value))
                {
                    return;
                }

                _SelectedChangeToControlPhrase = value;
            }
        }
        public void SaveControlPhraseRep()
        {
            Map_ControlPhrase_Replacement obj = new Map_ControlPhrase_Replacement();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Map_ControlPhrase_Replacement')", Win_Administrator_VM.CommonListConn));
                obj.MapReplaceControlPhrase_ID = mx + 1;
                obj.Orginal_Value = newControlPhraseRep;
                obj.Replace_ControlPhrase_ID = SelectedChangeToControlPhrase.PhraseID;

                var chkAlreadyExist = context.Map_ControlPhrase_Replacement.Count(x => x.Orginal_Value.ToLower() == obj.Orginal_Value.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("ControlPhrase Replacement already exists");
                }
                else
                {
                    context.Map_ControlPhrase_Replacement.Add(obj);
                    context.SaveChanges();
                }
            }

            ClearPopUpVariableControlPhraseRep();
            BindControlPhraseRep();

        }
        public ICommand CommandAddMoreControlPhraseRep { get; private set; }
        private void CommandAddMoreControlPhraseRepExecute(object obj)
        {
            SaveControlPhraseRep();
        }
        private void CommandClosePopUpExecuteControlPhraseRep(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpControlPhraseRep = false;

        }
        private void CommandShowPopUpAddControlPhraseRepExecute(object obj)
        {
            ClearPopUpVariableControlPhraseRep();
            IsEditControlPhraseRep = true;
            NotifyPropertyChanged("IsEditControlPhraseRep");
            IsShowPopUpControlPhraseRep = true;
            PopUpContentAddControlPhraseRepVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddControlPhraseRepVisibility");
        }

        private MapControlPhraseRep _SelectedControlPhraseRep { get; set; }
        public MapControlPhraseRep SelectedControlPhraseRep
        {
            get => (MapControlPhraseRep)_SelectedControlPhraseRep;
            set
            {
                if (Equals(_SelectedControlPhraseRep, value))
                {
                    return;
                }
                _SelectedControlPhraseRep = value;
                OpenPopupControlPhraseRep();
            }
        }

        private void OpenPopupControlPhraseRep()
        {
            if (SelectedControlPhraseRep != null)
            {
                //PopupLibUnit objChangesAdd = new PopupLibUnit(SelectedControlPhraseRep.UnitID);
                //objChangesAdd.ShowDialog();
            }
        }
        private void NotifyMeControlPhraseRep(PropertyChangedMessage<MapControlPhraseRep> obj)
        {
            SelectedControlPhraseRep = obj.NewValue;
        }

        public void ClearPopUpVariableControlPhraseRep()
        {
            newControlPhraseRep = "";
            NotifyPropertyChanged("newControlPhraseRep");
        }
        private void CommandButtonExecutedControlPhraseRep(NotificationMessageAction<object> obj)
        {

        }
        public string newControlPhraseRep { get; set; } = string.Empty;
        private List<CommonDataGridColumn> GetListGridColumnControlPhraseRep()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Control Phrase Need to Replace", ColBindingName = "ControlPhraseNeedtoReplace", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChangeTo", ColBindingName = "ChangeTo", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private DataGridCellInfo _cellInfo_AddNewControlPhraseRep { get; set; }
        public DataGridCellInfo CellInfo_AddNewControlPhraseRep
        {
            get { return _cellInfo_AddNewControlPhraseRep; }
            set
            {
                _cellInfo_AddNewControlPhraseRep = value;
            }
        }
        private void UpdateSourceTriggerCurrentCellControlPhraseRep(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "MapControlPhraseRep")
            {
                CellInfo_AddNewControlPhraseRep = obj.NewValue;
            }
        }

    }
    public class MapControlPhraseRep
    {
        public string ControlPhraseNeedtoReplace { get; set; }
        public string ChangeTo { get; set; }
        public int PhraseID { get; set; }
    }
    public class LibPhrase
    {
        public int PhraseID { get; set; }
        public string Phrase { get; set; }
    }
}


