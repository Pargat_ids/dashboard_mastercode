﻿using System.Windows.Input;
using System.Data;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibFunctionalClass> comonLibFunctionalClasses_VM { get; set; }
        public string newFunctionalClassCode { get; set; } = string.Empty;
        public string newFunctionalClass { get; set; } = string.Empty;
        public Visibility VisibilityLibFunctionalClassesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibFunctionalClassesVisibility { get; set; }
        public ICommand CommandClosePopUpLibFunctionalClasses { get; private set; }
        public ICommand CommandShowPopUpAddLibFunctionalClasses { get; private set; }
        public ICommand CommandAddLibFunctionalClasses { get; private set; }
        public ICommand CommandAddMoreLibFunctionalClasses { get; private set; }
        private bool _IsShowPopUpLibFunctionalClasses { get; set; }
        public bool IsShowPopUpLibFunctionalClasses
        {
            get { return _IsShowPopUpLibFunctionalClasses; }
            set
            {
                if (_IsShowPopUpLibFunctionalClasses != value)
                {
                    _IsShowPopUpLibFunctionalClasses = value;
                    NotifyPropertyChanged("_IsShowPopUpLibFunctionalClasses");
                }
            }
        }
        private ObservableCollection<MapLibFunctionalClass> _lstLibFunctionalClasses { get; set; } = new ObservableCollection<MapLibFunctionalClass>();
        public ObservableCollection<MapLibFunctionalClass> lstLibFunctionalClasses
        {
            get { return _lstLibFunctionalClasses; }
            set
            {
                if (_lstLibFunctionalClasses != value)
                {
                    _lstLibFunctionalClasses = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibFunctionalClass>>>(new PropertyChangedMessage<List<MapLibFunctionalClass>>(null, _lstLibFunctionalClasses.ToList(), "Default List"));
                }
            }
        }

        private void BindLibFunctionalClasses()
        {
            VisibilityLibFunctionalClassesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibFunctionalClassesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibFunctionalClassesTable = objCommon.DbaseQueryReturnSqlList<MapLibFunctionalClass>(
                "select  FunctionalClassCode,FunctionalClass, a.TimeStamp as DateAdded from Library_FunctionalClasses a order by FunctionalClassID desc", Win_Administrator_VM.CommonListConn);
                lstLibFunctionalClasses = new ObservableCollection<MapLibFunctionalClass>(SAPLibFunctionalClassesTable);
            });
            NotifyPropertyChanged("comonLibFunctionalClasses_VM");
            VisibilityLibFunctionalClassesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibFunctionalClassesDashboard_Loader");
        }
        private void RefreshGridLibFunctionalClasses(PropertyChangedMessage<string> flag)
        {
            BindLibFunctionalClasses();
            if (IsShowPopUpLibFunctionalClasses)
            {
                NotifyPropertyChanged("IsShowPopUpLibFunctionalClasses");
            }
        }

        private void CommandAddLibFunctionalClassesExecute(object obj)
        {
            SaveLibFunctionalClasses();
            IsShowPopUpLibFunctionalClasses = false;
            NotifyPropertyChanged("IsShowPopUpLibFunctionalClasses");
            ClearPopUpVariableLibFunctionalClasses();
            PopUpContentAddLibFunctionalClassesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibFunctionalClassesVisibility");
        }
        private bool CommandAddLibFunctionalClassesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newFunctionalClass.ToString()) || string.IsNullOrEmpty(newFunctionalClassCode.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibFunctionalClasses()
        {
            Library_FunctionalClasses obj = new Library_FunctionalClasses();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_FunctionalClasses')", Win_Administrator_VM.CommonListConn));
                obj.FunctionalClassID = mx + 1;
                var val = objCommon.RemoveWhitespaceCharacter(newFunctionalClass, stripCharacter);
                var val1 = objCommon.RemoveWhitespaceCharacter(newFunctionalClassCode, stripCharacter);
                obj.FunctionalClass = val.Trim();
                obj.FunctionalClassCode = val1.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseIDFunctionalClass = phrId;

                var phrHashCode = objCommon.GenerateSHA256String(val1.Trim());
                int phrIdCode = 0;
                var chkinLIbPHrCode = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHashCode && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHrCode == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val1.Trim(),
                        PhraseHash = phrHashCode,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrIdCode = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHashCode && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrIdCode = chkinLIbPHrCode.PhraseID;

                }
                obj.PhraseIDFunctionalClassCode = phrIdCode;

                var chkAlreadyExist = context.Library_FunctionalClasses.Count(x => x.PhraseIDFunctionalClass == obj.PhraseIDFunctionalClass || x.PhraseIDFunctionalClassCode == obj.PhraseIDFunctionalClassCode);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("FunctionalClass/FunctionalClassCode can't be duplicate");
                }
                else
                {
                    context.Library_FunctionalClasses.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New FunctionalClass Saved Successfully.");
                }
            }

            ClearPopUpVariableLibFunctionalClasses();
            BindLibFunctionalClasses();

        }
        private void CommandAddMoreLibFunctionalClassesExecute(object obj)
        {
            SaveLibFunctionalClasses();
        }
        private void CommandClosePopUpExecuteLibFunctionalClasses(object obj)
        {
            IsShowPopUpLibFunctionalClasses = false;
            NotifyPropertyChanged("IsShowPopUpLibFunctionalClasses");
        }
        private void CommandShowPopUpAddLibFunctionalClassesExecute(object obj)
        {
            ClearPopUpVariableLibFunctionalClasses();
            IsShowPopUpLibFunctionalClasses = true;
            NotifyPropertyChanged("IsShowPopUpLibFunctionalClasses");
            PopUpContentAddLibFunctionalClassesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibFunctionalClassesVisibility");
        }

        public void ClearPopUpVariableLibFunctionalClasses()
        {
            newFunctionalClass = "";
            newFunctionalClassCode = "";
            NotifyPropertyChanged("newFunctionalClass");
            NotifyPropertyChanged("newFunctionalClassCode");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibFunctionalClasses()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FunctionalClass", ColBindingName = "FunctionalClass", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FunctionalClassCode", ColBindingName = "FunctionalClassCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibFunctionalClass
    {
        public string FunctionalClassCode { get; set; }
        public string FunctionalClass { get; set; }

        public DateTime? DateAdded { get; set; }
    }
}
