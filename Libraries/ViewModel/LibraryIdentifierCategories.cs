﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibIdentifierCategory> comonLibIdentifierCategories_VM { get; set; }
        public string newIdentifierTypeName { get; set; } = string.Empty;
        public string newIdentifierSAPName { get; set; } = string.Empty;
        public string newIdentifierTypeDesc { get; set; } = string.Empty;
        public string newIdentifierValueType { get; set; } = string.Empty;
        public Visibility VisibilityLibIdentifierCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibIdentifierCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibIdentifierCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibIdentifierCategories { get; private set; }
        public ICommand CommandAddLibIdentifierCategories { get; private set; }
        public ICommand CommandAddMoreLibIdentifierCategories { get; private set; }
        private bool _IsShowPopUpLibIdentifierCategories { get; set; }
        public bool IsShowPopUpLibIdentifierCategories
        {
            get { return _IsShowPopUpLibIdentifierCategories; }
            set
            {
                if (_IsShowPopUpLibIdentifierCategories != value)
                {
                    _IsShowPopUpLibIdentifierCategories = value;
                    NotifyPropertyChanged("_IsShowPopUpLibIdentifierCategories");
                }
            }
        }
        private ObservableCollection<MapLibIdentifierCategory> _lstLibIdentifierCategories { get; set; } = new ObservableCollection<MapLibIdentifierCategory>();
        public ObservableCollection<MapLibIdentifierCategory> lstLibIdentifierCategories
        {
            get { return _lstLibIdentifierCategories; }
            set
            {
                if (_lstLibIdentifierCategories != value)
                {
                    _lstLibIdentifierCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibIdentifierCategory>>>(new PropertyChangedMessage<List<MapLibIdentifierCategory>>(null, _lstLibIdentifierCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibIdentifierCategories()
        {
            VisibilityLibIdentifierCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibIdentifierCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibIdentifierCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibIdentifierCategory>(
                "select IdentifierTypeName, IdentifierSAPName,IdentifierTypeDescr,IdentifierValueType, TimeStamp as DateAdded from Library_IdentifierCategories a order by IdentifierTypeID desc", Win_Administrator_VM.CommonListConn);
                lstLibIdentifierCategories = new ObservableCollection<MapLibIdentifierCategory>(SAPLibIdentifierCategoriesTable);
            });
            NotifyPropertyChanged("comonLibIdentifierCategories_VM");
            VisibilityLibIdentifierCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibIdentifierCategoriesDashboard_Loader");
        }
        private void RefreshGridLibIdentifierCategories(PropertyChangedMessage<string> flag)
        {
            BindLibIdentifierCategories();
            if (IsShowPopUpLibIdentifierCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibIdentifierCategories");
            }
        }

        private void CommandAddLibIdentifierCategoriesExecute(object obj)
        {
            SaveLibIdentifierCategories();
            IsShowPopUpLibIdentifierCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibIdentifierCategories");
            ClearPopUpVariableLibIdentifierCategories();
            PopUpContentAddLibIdentifierCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibIdentifierCategoriesVisibility");
        }
        private bool CommandAddLibIdentifierCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newIdentifierTypeName.ToString()) || string.IsNullOrEmpty(newIdentifierSAPName.ToString()) || string.IsNullOrEmpty(newIdentifierTypeDesc.ToString()) || string.IsNullOrEmpty(newIdentifierValueType.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibIdentifierCategories()
        {
            Library_IdentifierCategories obj = new Library_IdentifierCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_IdentifierCategories')", Win_Administrator_VM.CommonListConn));
                obj.IdentifierTypeID = mx + 1;
                obj.IdentifierTypeName = newIdentifierTypeName.Trim();
                obj.IdentifierSAPName = newIdentifierSAPName.Trim();
                obj.IdentifierTypeDescr = newIdentifierTypeDesc;
                obj.IdentifierValueType = newIdentifierValueType;
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_IdentifierCategories.Count(x => x.IdentifierTypeName.ToLower() == obj.IdentifierTypeName.ToLower() || x.IdentifierSAPName.ToLower() == obj.IdentifierSAPName.ToLower() || x.IdentifierTypeDescr.ToLower() == obj.IdentifierTypeDescr.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("IdentifierTypeName/IdentifierSAPName/IdentifierTypeDescr/IdentifierValueType can't be duplicate");
                }
                else
                {
                    context.Library_IdentifierCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New IdentifierCategory Saved Successfully.");
                }
            }

            ClearPopUpVariableLibIdentifierCategories();
            BindLibIdentifierCategories();

        }
        private void CommandAddMoreLibIdentifierCategoriesExecute(object obj)
        {
            SaveLibIdentifierCategories();
        }
        private void CommandClosePopUpExecuteLibIdentifierCategories(object obj)
        {
            IsShowPopUpLibIdentifierCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibIdentifierCategories");
        }
        private void CommandShowPopUpAddLibIdentifierCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibIdentifierCategories();
            IsShowPopUpLibIdentifierCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibIdentifierCategories");
            PopUpContentAddLibIdentifierCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibIdentifierCategoriesVisibility");
        }

        public void ClearPopUpVariableLibIdentifierCategories()
        {
            newIdentifierSAPName = "";
            newIdentifierTypeDesc = "";
            newIdentifierTypeName = "";
            newIdentifierValueType = "";
            NotifyPropertyChanged("newIdentifierSAPName");
            NotifyPropertyChanged("newIdentifierTypeDesc");
            NotifyPropertyChanged("newIdentifierTypeName");
            NotifyPropertyChanged("newIdentifierValueType");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibIdentifierCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierTypeName", ColBindingName = "IdentifierTypeName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierSAPName", ColBindingName = "IdentifierSAPName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierTypeDescr", ColBindingName = "IdentifierTypeDescr", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IdentifierValueType", ColBindingName = "IdentifierValueType", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibIdentifierCategory
    {
        public string IdentifierTypeName { get; set; }
        public string IdentifierSAPName { get; set; }
        public string IdentifierTypeDescr { get; set; }
        public string IdentifierValueType { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
