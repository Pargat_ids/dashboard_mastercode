﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string newListRelationName { get; set; } = string.Empty;
        public string newListRelationDesc { get; set; } = string.Empty;

        public Visibility VisibilityLibRelationDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibRelationVisibility { get; set; }
        public ICommand CommandClosePopUpLibRelation { get; private set; }
        public ICommand CommandShowPopUpAddLibRelation { get; private set; }
        public ICommand CommandAddLibRelation { get; private set; }
        public ICommand CommandAddMoreLibRelation { get; private set; }
        private bool _IsShowPopUpLibRelation { get; set; }
        public bool IsShowPopUpLibRelation
        {
            get { return _IsShowPopUpLibRelation; }
            set
            {
                if (_IsShowPopUpLibRelation != value)
                {
                    _IsShowPopUpLibRelation = value;
                    NotifyPropertyChanged("_IsShowPopUpLibRelation");
                }
            }
        }
        private ObservableCollection<MapLibRelation> _lstLibRelation { get; set; } = new ObservableCollection<MapLibRelation>();
        public ObservableCollection<MapLibRelation> lstLibRelation
        {
            get { return _lstLibRelation; }
            set
            {
                if (_lstLibRelation != value)
                {
                    _lstLibRelation = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibRelation>>>(new PropertyChangedMessage<List<MapLibRelation>>(null, _lstLibRelation.ToList(), "Default List"));
                }
            }
        }

        public void BindLibRelation()
        {
            VisibilityLibRelationDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibRelationDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibRelationTable = objCommon.DbaseQueryReturnSqlList<MapLibRelation>(
                "select ListRelationShipID, ListRelationShip_Name, ListRelationShip_Description, TimeStamp as DateAdded from Library_ListRelationships a order by ListRelationShipID desc", Win_Administrator_VM.CommonListConn);
                lstLibRelation = new ObservableCollection<MapLibRelation>(SAPLibRelationTable);
            });
            NotifyPropertyChanged("comonLibRelation_VM");
            VisibilityLibRelationDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibRelationDashboard_Loader");
        }
        private void RefreshGridLibRelation(PropertyChangedMessage<string> flag)
        {
            BindLibRelation();
            if (IsShowPopUpLibRelation)
            {
                NotifyPropertyChanged("IsShowPopUpLibRelation");
            }
        }

        private void CommandAddLibRelationExecute(object obj)
        {
            SaveLibRelation();
            IsShowPopUpLibRelation = false;
            NotifyPropertyChanged("IsShowPopUpLibRelation");
            ClearPopUpVariableLibRelation();
            PopUpContentAddLibRelationVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibRelationVisibility");
        }
        private bool CommandAddLibRelationCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newListRelationName.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibRelation()
        {
            Library_ListRelationships obj = new Library_ListRelationships();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_ListRelationships')", Win_Administrator_VM.CommonListConn));
                obj.ListRelationshipID = mx + 1;
                obj.ListRelationship_Name = newListRelationName.Trim();
                obj.ListRelationship_Description = newListRelationDesc.Trim();
                obj.TimeStamp = System.DateTime.Now;

                var chkAlreadyExist = context.Library_ListRelationships.Count(x => x.ListRelationship_Name.ToLower() == obj.ListRelationship_Name.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("RelationShip Name can't be duplicate");
                }
                else
                {
                    context.Library_ListRelationships.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Relation Saved Successfully.");
                }
            }

            ClearPopUpVariableLibRelation();
            BindLibRelation();

        }
        private void CommandAddMoreLibRelationExecute(object obj)
        {
            SaveLibRelation();
        }
        private void CommandClosePopUpExecuteLibRelation(object obj)
        {
            IsShowPopUpLibRelation = false;
            NotifyPropertyChanged("IsShowPopUpLibRelation");
        }
        private void CommandShowPopUpAddLibRelationExecute(object obj)
        {
            ClearPopUpVariableLibRelation();
            IsShowPopUpLibRelation = true;
            NotifyPropertyChanged("IsShowPopUpLibRelation");
            PopUpContentAddLibRelationVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibRelationVisibility");
        }

        public void ClearPopUpVariableLibRelation()
        {
            newListRelationDesc = "";
            newListRelationName = "";
            NotifyPropertyChanged("newListRelationDesc");
            NotifyPropertyChanged("newListRelationName");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibRelation()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();

            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListRelationShip_Name", ColBindingName = "ListRelationShip_Name", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListRelationShip_Description", ColBindingName = "ListRelationShip_Description", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibRelation
    {
        public int ListRelationShipID { get; set; }
        public string ListRelationShip_Name { get; set; }
        public string ListRelationShip_Description { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
