﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibSubstanceCategory> comonLibSubstanceCategories_VM { get; set; }
        public string newSubstanceCategoryName { get; set; } = string.Empty;
        public string newSubstanceCategoryDesc { get; set; } = string.Empty;
        public Visibility VisibilityLibSubstanceCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibSubstanceCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibSubstanceCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibSubstanceCategories { get; private set; }
        public ICommand CommandAddLibSubstanceCategories { get; private set; }
        public ICommand CommandAddMoreLibSubstanceCategories { get; private set; }
        private bool _IsShowPopUpLibSubstanceCategories { get; set; }
        public bool IsShowPopUpLibSubstanceCategories
        {
            get { return _IsShowPopUpLibSubstanceCategories; }
            set
            {
                if (_IsShowPopUpLibSubstanceCategories != value)
                {
                    _IsShowPopUpLibSubstanceCategories = value;
                    NotifyPropertyChanged("_IsShowPopUpLibSubstanceCategories");
                }
            }
        }
        private ObservableCollection<MapLibSubstanceCategory> _lstLibSubstanceCategories { get; set; } = new ObservableCollection<MapLibSubstanceCategory>();
        public ObservableCollection<MapLibSubstanceCategory> lstLibSubstanceCategories
        {
            get { return _lstLibSubstanceCategories; }
            set
            {
                if (_lstLibSubstanceCategories != value)
                {
                    _lstLibSubstanceCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibSubstanceCategory>>>(new PropertyChangedMessage<List<MapLibSubstanceCategory>>(null, _lstLibSubstanceCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibSubstanceCategories()
        {
            VisibilityLibSubstanceCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibSubstanceCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibSubstanceCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibSubstanceCategory>(
                "select SubstanceCategoryName, SubstanceCategoryDescription, TimeStamp as DateAdded from Library_SubstanceCategories a order by SubstanceCategoryID desc", Win_Administrator_VM.CommonListConn);
                lstLibSubstanceCategories = new ObservableCollection<MapLibSubstanceCategory>(SAPLibSubstanceCategoriesTable);
            });
            NotifyPropertyChanged("comonLibSubstanceCategories_VM");
            VisibilityLibSubstanceCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibSubstanceCategoriesDashboard_Loader");
        }
        private void RefreshGridLibSubstanceCategories(PropertyChangedMessage<string> flag)
        {
            BindLibSubstanceCategories();
            if (IsShowPopUpLibSubstanceCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibSubstanceCategories");
            }
        }

        private void CommandAddLibSubstanceCategoriesExecute(object obj)
        {
            SaveLibSubstanceCategories();
            IsShowPopUpLibSubstanceCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceCategories");
            ClearPopUpVariableLibSubstanceCategories();
            PopUpContentAddLibSubstanceCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibSubstanceCategoriesVisibility");
        }
        private bool CommandAddLibSubstanceCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newSubstanceCategoryName.ToString()) || string.IsNullOrEmpty(newSubstanceCategoryDesc.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibSubstanceCategories()
        {
            Library_SubstanceCategories obj = new Library_SubstanceCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_SubstanceCategories')", Win_Administrator_VM.CommonListConn));
                obj.SubstanceCategoryID = mx + 1;
                obj.SubstanceCategoryName = newSubstanceCategoryName.Trim();
                obj.SubstanceCategoryDescription = newSubstanceCategoryDesc.Trim();
                obj.TimeStamp = System.DateTime.Now;
                obj.IsActive = true;

                var chkAlreadyExist = context.Library_SubstanceCategories.Count(x => x.SubstanceCategoryName.ToLower() == obj.SubstanceCategoryName.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Substance Category Name can't be duplicate");
                }
                else
                {
                    context.Library_SubstanceCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Substance Category Saved Successfully.");
                }
            }

            ClearPopUpVariableLibSubstanceCategories();
            BindLibSubstanceCategories();

        }
        private void CommandAddMoreLibSubstanceCategoriesExecute(object obj)
        {
            SaveLibSubstanceCategories();
        }
        private void CommandClosePopUpExecuteLibSubstanceCategories(object obj)
        {
            IsShowPopUpLibSubstanceCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceCategories");
        }
        private void CommandShowPopUpAddLibSubstanceCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibSubstanceCategories();
            IsShowPopUpLibSubstanceCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibSubstanceCategories");
            PopUpContentAddLibSubstanceCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibSubstanceCategoriesVisibility");
        }

        public void ClearPopUpVariableLibSubstanceCategories()
        {
            newSubstanceCategoryName = "";
            newSubstanceCategoryDesc = "";
            NotifyPropertyChanged("newSubstanceCategoryName");
            NotifyPropertyChanged("newSubstanceCategoryDesc");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibSubstanceCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceCategoryName", ColBindingName = "SubstanceCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SubstanceCategoryDescription", ColBindingName = "SubstanceCategoryDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibSubstanceCategory
    {
        public string SubstanceCategoryName { get; set; }
        public string SubstanceCategoryDescription { get; set; }

        public DateTime? DateAdded { get; set; }
    }
}
