﻿using System.Windows.Input;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibGenericsCategory> comonLibGenericsCategories_VM { get; set; }
        public string newGenericsCategoryName { get; set; } = string.Empty;
        public string newGenericsCategoryDesc { get; set; } = string.Empty;
        public Visibility VisibilityLibGenericsCategoriesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibGenericsCategoriesVisibility { get; set; }
        public ICommand CommandClosePopUpLibGenericsCategories { get; private set; }
        public ICommand CommandShowPopUpAddLibGenericsCategories { get; private set; }
        public ICommand CommandAddLibGenericsCategories { get; private set; }
        public ICommand CommandAddMoreLibGenericsCategories { get; private set; }
        private bool _IsShowPopUpLibGenericsCategories { get; set; }
        public bool IsShowPopUpLibGenericsCategories
        {
            get { return _IsShowPopUpLibGenericsCategories; }
            set
            {
                if (_IsShowPopUpLibGenericsCategories != value)
                {
                    _IsShowPopUpLibGenericsCategories = value;
                    NotifyPropertyChanged("_IsShowPopUpLibGenericsCategories");
                }
            }
        }
        private ObservableCollection<MapLibGenericsCategory> _lstLibGenericsCategories { get; set; } = new ObservableCollection<MapLibGenericsCategory>();
        public ObservableCollection<MapLibGenericsCategory> lstLibGenericsCategories
        {
            get { return _lstLibGenericsCategories; }
            set
            {
                if (_lstLibGenericsCategories != value)
                {
                    _lstLibGenericsCategories = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibGenericsCategory>>>(new PropertyChangedMessage<List<MapLibGenericsCategory>>(null, _lstLibGenericsCategories.ToList(), "Default List"));
                }
            }
        }

        private void BindLibGenericsCategories()
        {
            VisibilityLibGenericsCategoriesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibGenericsCategoriesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibGenericsCategoriesTable = objCommon.DbaseQueryReturnSqlList<MapLibGenericsCategory>(
                "select GenericsCategoryName, GenericsCategoryDescription, TimeStamp as DateAdded from Library_GenericsCategories a order by GenericsCategoryID desc", Win_Administrator_VM.CommonListConn);
                lstLibGenericsCategories = new ObservableCollection<MapLibGenericsCategory>(SAPLibGenericsCategoriesTable);
            });
            NotifyPropertyChanged("comonLibGenericsCategories_VM");
            VisibilityLibGenericsCategoriesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibGenericsCategoriesDashboard_Loader");
        }
        private void RefreshGridLibGenericsCategories(PropertyChangedMessage<string> flag)
        {
            BindLibGenericsCategories();
            if (IsShowPopUpLibGenericsCategories)
            {
                NotifyPropertyChanged("IsShowPopUpLibGenericsCategories");
            }
        }

        private void CommandAddLibGenericsCategoriesExecute(object obj)
        {
            SaveLibGenericsCategories();
            IsShowPopUpLibGenericsCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibGenericsCategories");
            ClearPopUpVariableLibGenericsCategories();
            PopUpContentAddLibGenericsCategoriesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibGenericsCategoriesVisibility");
        }
        private bool CommandAddLibGenericsCategoriesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newGenericsCategoryName.ToString()) || string.IsNullOrEmpty(newGenericsCategoryDesc.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibGenericsCategories()
        {
            Library_GenericsCategories obj = new Library_GenericsCategories();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_GenericsCategories')", Win_Administrator_VM.CommonListConn));
                obj.GenericsCategoryID = mx + 1;
                obj.GenericsCategoryName = newGenericsCategoryName.Trim();
                obj.GenericsCategoryDescription = newGenericsCategoryDesc.Trim();
                obj.TimeStamp = objCommon.ESTTime();
                
                var chkAlreadyExist = context.Library_GenericsCategories.Count(x => x.GenericsCategoryName.ToLower() == obj.GenericsCategoryName.ToLower() || x.GenericsCategoryDescription.ToLower() == obj.GenericsCategoryDescription.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("GenericsCategoryName/GenericsCategoryDescription can't be duplicate");
                }
                else
                {
                    context.Library_GenericsCategories.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Generics Category Saved Successfully.");
                }
            }

            ClearPopUpVariableLibGenericsCategories();
            BindLibGenericsCategories();

        }
        private void CommandAddMoreLibGenericsCategoriesExecute(object obj)
        {
            SaveLibGenericsCategories();
        }
        private void CommandClosePopUpExecuteLibGenericsCategories(object obj)
        {
            IsShowPopUpLibGenericsCategories = false;
            NotifyPropertyChanged("IsShowPopUpLibGenericsCategories");
        }
        private void CommandShowPopUpAddLibGenericsCategoriesExecute(object obj)
        {
            ClearPopUpVariableLibGenericsCategories();
            IsShowPopUpLibGenericsCategories = true;
            NotifyPropertyChanged("IsShowPopUpLibGenericsCategories");
            PopUpContentAddLibGenericsCategoriesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibGenericsCategoriesVisibility");
        }

        public void ClearPopUpVariableLibGenericsCategories()
        {
            newGenericsCategoryName = "";
            newGenericsCategoryDesc = "";
            NotifyPropertyChanged("newGenericsCategoryName");
            NotifyPropertyChanged("newGenericsCategoryDesc");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibGenericsCategories()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GenericsCategoryName", ColBindingName = "GenericsCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GenericsCategoryDescription", ColBindingName = "GenericsCategoryDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibGenericsCategory
    {
        public string GenericsCategoryDescription { get; set; }
        public string GenericsCategoryName { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
