﻿using System.Windows.Input;
using System.Windows.Controls;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using Libraries.CustomUserControl;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibUnit> comonLibUnit_VM { get; set; }
        public Visibility VisibilityLibUnitDashboard_Loader { get; set; }
        public ICommand CommandClosePopUpLibUnit { get; private set; }
        public ICommand CommandShowPopUpAddLibUnit { get; private set; }
        public bool IsEditLibUnit { get; set; } = true;
        public Visibility PopUpContentAddLibUnitVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<MapLibUnit> _lstLibUnit { get; set; } = new ObservableCollection<MapLibUnit>();
        public ObservableCollection<MapLibUnit> lstLibUnit
        {
            get { return _lstLibUnit; }
            set
            {
                if (_lstLibUnit != value)
                {
                    _lstLibUnit = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibUnit>>>(new PropertyChangedMessage<List<MapLibUnit>>(null, _lstLibUnit.ToList(), "Default List"));
                }
            }
        }
        private void BindLibUnit()
        {
            VisibilityLibUnitDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibUnitDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibUnitTable = objCommon.DbaseQueryReturnSqlList<MapLibUnit>(
                "select UnitID, Unit, UnitDescription, (select count(distinct qsid_id) from QSxxx_Values where UnitID = a.UnitID) as ListCount, TimeStamp as DateAdded  from Library_Units a order by UnitID desc", Win_Administrator_VM.CommonListConn);
                lstLibUnit = new ObservableCollection<MapLibUnit>(SAPLibUnitTable);
            });
            NotifyPropertyChanged("comonLibUnit_VM");
            VisibilityLibUnitDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibUnitDashboard_Loader");
        }
        private bool _IsShowPopUpLibUnit { get; set; }
        public bool IsShowPopUpLibUnit
        {
            get { return _IsShowPopUpLibUnit; }
            set
            {
                if (_IsShowPopUpLibUnit != value)
                {
                    _IsShowPopUpLibUnit = value;
                    NotifyPropertyChanged("IsShowPopUpLibUnit");
                }
            }
        }
        private void RunLibUnit()
        {
            BindLibUnit();

        }

        private void RefreshGridLibUnit(PropertyChangedMessage<string> flag)
        {
            BindLibUnit();
            if (IsShowPopUpLibUnit)
            {
                NotifyPropertyChanged("IsShowPopUpLibUnit");
            }
        }
        public ICommand CommandAddLibUnit { get; private set; }
        private void CommandAddLibUnitExecute(object obj)
        {
            SaveLibUnit();
            IsShowPopUpLibUnit = false;
            IsEditLibUnit = true;
            NotifyPropertyChanged("IsEditLibUnit");
            ClearPopUpVariableLibUnit();
            PopUpContentAddLibUnitVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibUnitVisibility");
        }
        private bool CommandAddLibUnitCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newUnit.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public string ReplacementUnit(string newPhr)
        {
            var repPhr = string.Empty;
            using (var context = new CRAModel())
            {
                // check in replacement Table
                var replaceID = context.Map_Unit_Replacement.AsNoTracking().Where(x => x.Orginal_Value.ToLower() == newPhr.Trim().ToLower()).Select(y => y.Replace_Unit_ID).FirstOrDefault();
                if (replaceID != null)
                {
                    repPhr = context.Library_Units.AsNoTracking().Where(x => x.UnitID == replaceID).Select(y => y.Unit).FirstOrDefault();
                    return repPhr;
                }
                return repPhr;
            }
        }
        public void SaveLibUnit()
        {
            Library_Units obj = new Library_Units();
            using (var context = new CRAModel())
            {
                var repPhr = ReplacementUnit(newUnit);
                if (string.IsNullOrEmpty(repPhr) == false)
                {
                    var resultBox = System.Windows.Forms.MessageBox.Show("Exist in replacement table, replaced by -" + repPhr + Environment.NewLine + "Do you want to Continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (resultBox == DialogResult.Yes)
                    {
                        newUnit = repPhr;
                        NotifyPropertyChanged("newUnit");
                    }
                    else
                    {
                        return;
                    }
                }

                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Units')", Win_Administrator_VM.CommonListConn));
                obj.UnitID = mx + 1;
                obj.Unit = objCommon.ChkGreekSmallLetter(newUnit);
                obj.IsActive = true;
                obj.TimeStamp = System.DateTime.Now;
                obj.UnitDescription = newUnitDesc;


                var chkAlreadyExist = context.Library_Units.Count(x => x.Unit.ToLower() == obj.Unit.ToLower());
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Unit already exists");
                }
                else
                {
                    context.Library_Units.Add(obj);
                    context.SaveChanges();
                    if (newUnit != obj.Unit && newUnit.Contains("\u03BC"))
                    {
                        _notifier.ShowSuccess("We convert Mu(03BC) to Micro sign(00B5) while saving, New Unit Saved Successfully.");
                    }
                    else
                    {
                        _notifier.ShowSuccess("New Unit Saved Successfully.");
                    }
                }

            }

            ClearPopUpVariableLibUnit();
            BindLibUnit();

        }
        public ICommand CommandAddMoreLibUnit { get; private set; }
        private void CommandAddMoreLibUnitExecute(object obj)
        {
            SaveLibUnit();
        }
        private void CommandClosePopUpExecuteLibUnit(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpLibUnit = false;

        }
        private void CommandShowPopUpAddLibUnitExecute(object obj)
        {
            ClearPopUpVariableLibUnit();
            IsEditLibUnit = true;
            NotifyPropertyChanged("IsEditLibUnit");
            IsShowPopUpLibUnit = true;
            PopUpContentAddLibUnitVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibUnitVisibility");
        }

        private MapLibUnit _SelectedLibUnit { get; set; }
        public MapLibUnit SelectedLibUnit
        {
            get => (MapLibUnit)_SelectedLibUnit;
            set
            {
                if (Equals(_SelectedLibUnit, value))
                {
                    return;
                }
                _SelectedLibUnit = value;
                OpenPopupLibUnit();
            }
        }

        private void OpenPopupLibUnit()
        {
            if (SelectedLibUnit != null)
            {
                PopupLibUnit objChangesAdd = new PopupLibUnit(SelectedLibUnit.UnitID);
                objChangesAdd.ShowDialog();
            }
        }
        private void NotifyMeLibUnit(PropertyChangedMessage<MapLibUnit> obj)
        {
            SelectedLibUnit = obj.NewValue;
        }

        public void ClearPopUpVariableLibUnit()
        {
            newUnit = "";
            newUnitDesc = "";
            NotifyPropertyChanged("newUnit");
            NotifyPropertyChanged("newUnitDesc");
        }
        private void CommandButtonExecutedLibUnit(NotificationMessageAction<object> obj)
        {

        }
        public string newUnit { get; set; } = string.Empty;
        public string newUnitDesc { get; set; } = string.Empty;

        private List<CommonDataGridColumn> GetListGridColumnLibUnit()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "UnitID", ColBindingName = "UnitID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Unit", ColBindingName = "Unit", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "UnitDescription", ColBindingName = "UnitDescription", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListCount", ColBindingName = "ListCount", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });

            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapLibUnit" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNameLibUnit" });

            return listColumnQsidDetail;
        }
        private DataGridCellInfo _cellInfo_AddNew { get; set; }
        public DataGridCellInfo CellInfo_AddNew
        {
            get { return _cellInfo_AddNew; }
            set
            {
                _cellInfo_AddNew = value;
            }
        }
        private void UpdateSourceTriggerCurrentCell(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "MapLibUnit")
            {
                CellInfo_AddNew = obj.NewValue;
            }
        }

    }
    public class MapLibUnit
    {
        public int UnitID { get; set; }
        public string Unit { get; set; }
        public string UnitDescription { get; set; }
        public int ListCount { get; set; }
        public DateTime? DateAdded { get; set; }
    }

    public class LibYesNo
    {
        public string Value { get; set; }
    }

}
