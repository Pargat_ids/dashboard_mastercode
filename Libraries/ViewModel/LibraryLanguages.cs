﻿using System.Windows.Input;
using System.Data;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibLanguage> comonLibLanguages_VM { get; set; }
        public string newLanguage { get; set; } = string.Empty;
        public string newLanguageCode { get; set; } = string.Empty;
        public string newLanguageISOTwo { get; set; } = string.Empty;
        public string newLanguageISOThr { get; set; } = string.Empty;
        public int newLanguageIdent { get; set; } = 0;
        public Visibility VisibilityLibLanguagesDashboard_Loader { get; set; }
        public Visibility PopUpContentAddLibLanguagesVisibility { get; set; }
        public ICommand CommandClosePopUpLibLanguages { get; private set; }
        public ICommand CommandShowPopUpAddLibLanguages { get; private set; }
        public ICommand CommandAddLibLanguages { get; private set; }
        public ICommand CommandAddMoreLibLanguages { get; private set; }
        private bool _IsShowPopUpLibLanguages { get; set; }
        public bool IsShowPopUpLibLanguages
        {
            get { return _IsShowPopUpLibLanguages; }
            set
            {
                if (_IsShowPopUpLibLanguages != value)
                {
                    _IsShowPopUpLibLanguages = value;
                    NotifyPropertyChanged("_IsShowPopUpLibLanguages");
                }
            }
        }
        private ObservableCollection<MapLibLanguage> _lstLibLanguages { get; set; } = new ObservableCollection<MapLibLanguage>();
        public ObservableCollection<MapLibLanguage> lstLibLanguages
        {
            get { return _lstLibLanguages; }
            set
            {
                if (_lstLibLanguages != value)
                {
                    _lstLibLanguages = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibLanguage>>>(new PropertyChangedMessage<List<MapLibLanguage>>(null, _lstLibLanguages.ToList(), "Default List"));
                }
            }
        }
        private void BindLibLanguages()
        {
            VisibilityLibLanguagesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibLanguagesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPLibLanguagesTable = objCommon.DbaseQueryReturnSqlList<MapLibLanguage>(
                "select LanguageID, Ident, LanguageCode, LanguageName, ISO_Two,ISO_Three, TimeStamp as DateAdded from Library_Languages a order by LanguageID desc", Win_Administrator_VM.CommonListConn);
                lstLibLanguages = new ObservableCollection<MapLibLanguage>(SAPLibLanguagesTable);
            });
            NotifyPropertyChanged("comonLibLanguages_VM");
            VisibilityLibLanguagesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityLibLanguagesDashboard_Loader");
        }
        //private void RefreshGridLibLanguages(PropertyChangedMessage<string> flag)
        //{
        //    BindLibLanguages();
        //    if (IsShowPopUpLibLanguages)
        //    {
        //        NotifyPropertyChanged("IsShowPopUpLibLanguages");
        //    }
        //}

        private void CallBackBindListDetailGridExecute(NotificationMessageAction<MapLibLanguage> notificationMessageAction)
        {
            BindLibLanguages();
            if (IsShowPopUpLibLanguages)
            {
                NotifyPropertyChanged("IsShowPopUpLibLanguages");
            }
        }

        private void CommandAddLibLanguagesExecute(object obj)
        {
            SaveLibLanguages();
            IsShowPopUpLibLanguages = false;
            NotifyPropertyChanged("IsShowPopUpLibLanguages");
            ClearPopUpVariableLibLanguages();
            PopUpContentAddLibLanguagesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibLanguagesVisibility");
        }
        private bool CommandAddLibLanguagesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newLanguage.ToString()) || string.IsNullOrEmpty(newLanguageCode.ToString()) || string.IsNullOrEmpty(newLanguageIdent.ToString()) || string.IsNullOrEmpty(newLanguageISOTwo.ToString()) || string.IsNullOrEmpty(newLanguageISOThr.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveLibLanguages()
        {
            Library_Languages obj = new Library_Languages();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_Languages')", Win_Administrator_VM.CommonListConn));
                obj.LanguageID = mx + 1;
                var val = objCommon.RemoveWhitespaceCharacter(newLanguage, stripCharacter);
                obj.LanguageName = val.Trim();
                obj.LanguageCode = newLanguageCode.Trim();
                obj.IDENT = newLanguageIdent;
                obj.ISO_Two = newLanguageISOTwo;
                obj.ISO_Three = newLanguageISOThr;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID = phrId;

                var chkAlreadyExist = context.Library_Languages.Count(x => x.PhraseID == obj.PhraseID || x.LanguageCode == obj.LanguageCode || x.IDENT == obj.IDENT || x.ISO_Two == obj.ISO_Two || x.ISO_Three == obj.ISO_Three);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("Language/LanguageCode/Ident/ISO_Two/ISO_Three can't be duplicate");
                }
                else
                {
                    context.Library_Languages.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New Language Saved Successfully.");
                }
            }

            ClearPopUpVariableLibLanguages();
            BindLibLanguages();

        }
        private void CommandAddMoreLibLanguagesExecute(object obj)
        {
            SaveLibLanguages();
        }
        private void CommandClosePopUpExecuteLibLanguages(object obj)
        {
            IsShowPopUpLibLanguages = false;
            NotifyPropertyChanged("IsShowPopUpLibLanguages");
        }
        private void CommandShowPopUpAddLibLanguagesExecute(object obj)
        {
            ClearPopUpVariableLibLanguages();
            IsShowPopUpLibLanguages = true;
            NotifyPropertyChanged("IsShowPopUpLibLanguages");
            PopUpContentAddLibLanguagesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibLanguagesVisibility");
        }

        public void ClearPopUpVariableLibLanguages()
        {
            newLanguage = "";
            newLanguageCode = "";
            newLanguageIdent = 0;
            newLanguageISOTwo = "";
            newLanguageISOThr = "";
            NotifyPropertyChanged("newLanguage");
            NotifyPropertyChanged("newLanguageCode");
            NotifyPropertyChanged("newLanguageIdent");
            NotifyPropertyChanged("newLanguageISOTwo");
            NotifyPropertyChanged("newLanguageISOThr");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibLanguages()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageID", ColBindingName = "LanguageID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Ident", ColBindingName = "Ident", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageCode", ColBindingName = "LanguageCode", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ISO_Two", ColBindingName = "ISO_Two", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ISO_Three", ColBindingName = "ISO_Three", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }

    }
    public class MapLibLanguage
    {
        public int LanguageID { get; set; }
        public int Ident { get; set; }
        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
        public string ISO_Two { get; set; }
        public string ISO_Three { get; set; }
        public DateTime? DateAdded { get; set; }
    }


}
