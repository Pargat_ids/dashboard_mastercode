﻿using System.Windows.Input;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private bool _IsShowPopUpLibExpressedAs { get; set; }
        public Visibility PopUpContentAddLibExpressedAsVisibility { get; set; }
        public CommonDataGrid_ViewModel<MapLibExpressedAs> comonLibExpressedAs_VM { get; set; }
        public string newExpressedAs { get; set; } = string.Empty;
        public ICommand CommandAddMoreLibExpressedAs { get; private set; }
        public ICommand CommandAddLibExpressedAs { get; private set; }
        public ICommand CommandClosePopUpLibExpressedAs { get; private set; }
        public ICommand CommandShowPopUpAddLibExpressedAs { get; private set; }
        public Visibility VisibilityLibExpressedAsDashboard_Loader { get; set; }
        public bool IsShowPopUpLibExpressedAs
        {
            get { return _IsShowPopUpLibExpressedAs; }
            set
            {
                if (_IsShowPopUpLibExpressedAs != value)
                {
                    _IsShowPopUpLibExpressedAs = value;
                    NotifyPropertyChanged("_IsShowPopUpLibExpressedAs");
                }
            }
        }
        private void RefreshGridLibExpressedAs(PropertyChangedMessage<string> flag)
        {
            BindLibExpressedAs();
            if (IsShowPopUpLibExpressedAs)
            {
                NotifyPropertyChanged("IsShowPopUpLibExpressedAs");
            }
        }
        private void CommandAddLibExpressedAsExecute(object obj)
        {
            SaveLibExpressedAs();
            IsShowPopUpLibExpressedAs = false;
            NotifyPropertyChanged("IsShowPopUpLibExpressedAs");
            ClearPopUpVariableLibExpressedAs();
            PopUpContentAddLibExpressedAsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddLibExpressedAsVisibility");
        }
        private void CommandClosePopUpExecuteLibExpressedAs(object obj)
        {
            IsShowPopUpLibExpressedAs = false;
            NotifyPropertyChanged("IsShowPopUpLibExpressedAs");
        }
        private void CommandShowPopUpAddLibExpressedAsExecute(object obj)
        {
            ClearPopUpVariableLibExpressedAs();
            IsShowPopUpLibExpressedAs = true;
            NotifyPropertyChanged("IsShowPopUpLibExpressedAs");
            PopUpContentAddLibExpressedAsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddLibExpressedAsVisibility");
        }
        private bool CommandAddLibExpressedAsCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newExpressedAs.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveLibExpressedAs()
        {
            Library_ExpressedAs obj = new Library_ExpressedAs();
            using (var context = new CRAModel())
            {
                var repPhr = Replacement("Expressed As (Phrase)", newExpressedAs);
                if (string.IsNullOrEmpty(repPhr) == false)
                {
                    var resultBox = System.Windows.Forms.MessageBox.Show("Exist in replacement table, replaced by -" + repPhr + Environment.NewLine + "Do you want to Continue", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (resultBox == DialogResult.Yes)
                    {
                        newExpressedAs = repPhr;
                        NotifyPropertyChanged("newExpressedAs");
                    }
                    else
                    {
                        return;
                    }
                }

                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Library_ExpressedAs')", Win_Administrator_VM.CommonListConn));
                obj.LibraryExpressedAsID = mx + 1;
                var phrVal = newExpressedAs.Trim();
                var ExpressedAsVal = objCommon.RemoveWhitespaceCharacter(phrVal, stripCharacter);
                obj.PhraseID_ExpressedAs = 0;
                obj.PhraseHash_LowerCase = objCommon.GenerateSHA256String(ExpressedAsVal.ToLower());
                obj.IsActive = true;
                obj.TimeStamp = System.DateTime.Now;

                var phrHash = objCommon.GenerateSHA256String(phrVal);
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = ExpressedAsVal,
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID_ExpressedAs = phrId;

                var chkAlreadyExist = context.Library_ExpressedAs.Count(x => x.PhraseHash_LowerCase == obj.PhraseHash_LowerCase);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("ExpressedAs can't be duplicate");
                }
                else
                {
                    context.Library_ExpressedAs.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New ExpressedAs Saved Successfully.");
                }
            }

            ClearPopUpVariableLibExpressedAs();
            BindLibExpressedAs();

        }
        public void ClearPopUpVariableLibExpressedAs()
        {
            newExpressedAs = "";
            NotifyPropertyChanged("newExpressedAs");
        }
        private void CommandAddMoreLibExpressedAsExecute(object obj)
        {
            SaveLibExpressedAs();
        }
        private void CallbackHyperlinkExpressedAs(NotificationMessageAction<object> obj)
        {
            int qsid_Id = Convert.ToInt32(obj.Notification);
            Task.Run(() => FetchQsidDetail(qsid_Id));
        }
        private void BindLibExpressedAs()
        {
            List<MapLibExpressedAs> newFinalList = new List<MapLibExpressedAs>();
            VisibilityLibExpressedAsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityLibExpressedAsDashboard_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var SAPLibExpressedAsTable = objCommon.DbaseQueryReturnSqlList<MapLibExpressedAs>(
                    "select b.PhraseID, Phrase as ExpressedAs, 0 as No_of_active_lists, a.TimeStamp as DateAdded from Library_ExpressedAs a, Library_Phrases b where a.PhraseId_ExpressedAs = b.PhraseId order by LibraryExpressedAsID desc", Win_Administrator_VM.CommonListConn);
                    if (SAPLibExpressedAsTable.Any())
                    {
                        var onlyPhrases = SAPLibExpressedAsTable.Select(x => x.PhraseID).Distinct().ToList();
                        var CatIdListField = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "List Field Name").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        var subrootcat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Expressed As (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();

                        var findQsxxxQsid = (from d1 in context.QSxxx_Phrases.AsNoTracking().AsNoTracking().Where(x => x.PhraseCategoryID == subrootcat)
                                             join d2 in onlyPhrases
                                             on d1.PhraseID equals d2
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID, d1.QSID_ID, d3.QSID }).Distinct().ToList();
                        var onlyQsids = findQsxxxQsid.Select(y => y.QSID_ID).Distinct().ToList();
                        var onlyQsxxPhrID = (from d1 in context.QSxxx_ListDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == CatIdListField)
                                             join d2 in onlyQsids
                                             on d1.QSID_ID equals d2
                                             select new { d1.PhraseID, d1.QSID_ID }).Distinct().ToList();
                        var qsxxListDic = (from d1 in onlyQsxxPhrID
                                           join d2 in context.Library_Phrases.AsNoTracking()
                                           on d1.PhraseID equals d2.PhraseID
                                           select new { d1.QSID_ID, d2.PhraseID, d2.Phrase }).Distinct().ToList();
                        var combineFinal = (from d1 in findQsxxxQsid
                                            join d2 in qsxxListDic
                                            on d1.QSID_ID
                                            equals d2.QSID_ID
                                            select new { d1.PhraseID, QSID = d1.QSID + "(" + d2.Phrase + ")", QSID_ID = d1.QSID_ID }).Distinct().ToList();
                        var joinQsids = (from d1 in combineFinal
                                         group d1 by d1.PhraseID into g
                                         select new
                                         {
                                             PhraseID = g.Key,
                                             displayValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID).ToList()),
                                             navigateValue = string.Join(", ", g.AsEnumerable().Select(kvp => kvp.QSID_ID).ToList()),
                                             count = g.Count(),
                                         }).Distinct().ToList();
                        newFinalList = (from d1 in SAPLibExpressedAsTable
                                        join d2 in joinQsids
                                        on d1.PhraseID equals d2.PhraseID into tg
                                        select new MapLibExpressedAs
                                        {
                                            DateAdded = d1.DateAdded,
                                            PhraseID = d1.PhraseID,
                                            ExpressedAs = d1.ExpressedAs,
                                            No_of_active_lists = tg.Select(x => x.count).FirstOrDefault(),
                                            Active_Lists = ConvertInfoToHyoerlinkVal(tg.Select(x => x.displayValue).FirstOrDefault(), tg.Select(x => x.navigateValue).FirstOrDefault()),
                                        }).ToList();
                    }
                    lstLibExpressedAs = new ObservableCollection<MapLibExpressedAs>(newFinalList);
                }
                NotifyPropertyChanged("comonLibExpressedAs_VM");
                VisibilityLibExpressedAsDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityLibExpressedAsDashboard_Loader");
            });

        }
        private ObservableCollection<MapLibExpressedAs> _lstLibExpressedAs { get; set; } = new ObservableCollection<MapLibExpressedAs>();
        public ObservableCollection<MapLibExpressedAs> lstLibExpressedAs
        {
            get { return _lstLibExpressedAs; }
            set
            {
                if (_lstLibExpressedAs != value)
                {
                    _lstLibExpressedAs = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibExpressedAs>>>(new PropertyChangedMessage<List<MapLibExpressedAs>>(null, _lstLibExpressedAs.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnLibExpressedAs()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ExpressedAs", ColBindingName = "ExpressedAs", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_active_lists", ColBindingName = "No_of_active_lists", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Active_Lists", ColBindingName = "Active_Lists", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DateAdded", ColBindingName = "DateAdded", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
    }
    public class MapLibExpressedAs
    {
        public int PhraseID { get; set; }
        public string ExpressedAs { get; set; }
        public int No_of_active_lists { get; set; }
        public List<HyperLinkDataValue> Active_Lists { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
