﻿using CommonGenericUIView.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VersionControlSystem.Model.ViewModel;

namespace Libraries.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<MapLibDisapprovedCasIdent> comonDisapprovedCasIdent_VM { get; set; }
        public Visibility VisibilityDisapprovedCasIdent_Loader { get; set; }
        private MapLibDisapprovedCasIdent SelectedDisapprovedCasIdent { get; set; }
        private List<MapLibDisapprovedCasIdent> _lstDisapprovedCasIdent { get; set; } = new ();
        public List<MapLibDisapprovedCasIdent> lstDisapprovedCasIdent
        {
            get { return _lstDisapprovedCasIdent; }
            set
            {
                if (_lstDisapprovedCasIdent != value)
                {
                    _lstDisapprovedCasIdent = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapLibDisapprovedCasIdent>>>(new PropertyChangedMessage<List<MapLibDisapprovedCasIdent>>(null, _lstDisapprovedCasIdent, "Default List"));
                }
            }
        }
        public CommentsUC_VM objCommentDC_AlternateCas { get; set; } = new CommentsUC_VM(new GenericComment_VM());

        private void BindLiDisapprovedCasIdent()
        {
            VisibilityDisapprovedCasIdent_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityDisapprovedCasIdent_Loader");
            Task.Run(() =>
            {
                string query = "Select t.* , lc.CAS,lu.FirstName+' '+lu.LastName as Disapprovedby from (Select msd.CASID, msd.ChemicalNameID as IdentifierValueID, lcn.ChemicalName as IdentifierValue, null as IdentifierTypeID,'Synonyms' as IdentifierType,lmsd.SessionID,lmsd.TimeStamp as DisapprovedOn  from Map_SubstanceNameSynonyms_Disapproved msd ";
                query +=" left join Library_ChemicalNames lcn on msd.ChemicalNameID=lcn.ChemicalNameID left join Log_Map_SubstanceNameSynonyms_Disapproved lmsd on lmsd.SynonymID_Disapprove=msd.SynonymID_Disapprove union all ";
                query +=" Select msd.CASID,msd.IdentifierValueID, li.IdentifierValue collate SQL_Latin1_General_CP1_CI_AS as IdentifierValue, 9 as IdentifierTypeID,'EC Number' as IdentifierType,lmsd.SessionID,lmsd.TimeStamp as DisapprovedOn  from Map_CAS_ECNumber_Disapproved msd  ";
                query +=" left join Library_Identifiers li on msd.IdentifierValueID=li.IdentifierValueID and li.IdentifierTypeID=9 left join Log_Map_CAS_EC_Disapproved lmsd on lmsd.CASECNumberID_Disapproved=msd.CASECNumberID_Disapproved union all ";
                query +=" Select msd.CASID, msd.IdentifierValueID, li.IdentifierValue collate SQL_Latin1_General_CP1_CI_AS  as IdentifierValue,13 as IdentifierTypeID,'Molecular Formula' as IdentifierType,lmsd.SessionID,lmsd.TimeStamp as DisapprovedOn  from Map_CAS_MolecularFormula_Disapproved  msd  ";
                query +=" left join Library_Identifiers li on msd.IdentifierValueID=li.IdentifierValueID and li.IdentifierTypeID=13 left join Log_Map_CAS_MolecularFormula_Disapproved lmsd on lmsd.CASMolecularFormulaID_Disapprove=msd.CASMolecularFormulaID_Disapprove )t left join Library_CAS lc on lc.CASID=t.CASID left join Log_Sessions ls on ls.SessionID=t.SessionID  left join Library_Users lu on lu.VeriskID=ls.UserName";
                var SAPLibCountriesTable = objCommon.DbaseQueryReturnSqlList<MapLibDisapprovedCasIdent>(query, Win_Administrator_VM.CommonListConn);
                lstDisapprovedCasIdent = SAPLibCountriesTable;

                VisibilityDisapprovedCasIdent_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityDisapprovedCasIdent_Loader");

            });
            
        }
        

        private void NotifyMeDisapproved(PropertyChangedMessage<MapLibDisapprovedCasIdent> obj)
        {
            SelectedDisapprovedCasIdent = obj.NewValue;
            if (SelectedDisapprovedCasIdent != null)
            {
                if (SelectedDisapprovedCasIdent.IdentifierType!= "Synonyms")
                {
                    objCommentDC_AlternateCas = new CommentsUC_VM(new GenericComment_VM(SelectedDisapprovedCasIdent.CAS, SelectedDisapprovedCasIdent.IdentifierValue, SelectedDisapprovedCasIdent.CASID, SelectedDisapprovedCasIdent.IdentifierValueID, 470, false, "", false, "", 0, true, SelectedDisapprovedCasIdent.IdentifierTypeID));
                }
                else {
                    objCommentDC_AlternateCas = new CommentsUC_VM(new GenericComment_VM(SelectedDisapprovedCasIdent.CAS, "", SelectedDisapprovedCasIdent.CASID, 0, 470, false, "", true, SelectedDisapprovedCasIdent.IdentifierValue, SelectedDisapprovedCasIdent.IdentifierValueID, false, 0));
                }
                NotifyPropertyChanged("objCommentDC_AlternateCas");
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnDisapprovedCasIdent()
        {
            List<CommonDataGridColumn> listColumnCasIdent = new List<CommonDataGridColumn>();
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Id", ColBindingName = "CASID", ColType = "Textbox" });
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "CAS", ColType = "Textbox" });
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Value", ColBindingName = "IdentifierValue", ColType = "Textbox" });
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Identifier Type", ColBindingName = "IdentifierType", ColType = "Textbox" });
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Disapproved On", ColBindingName = "DisapprovedOn", ColType = "Textbox" });
            listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Disapproved by", ColBindingName = "Disapprovedby", ColType = "Textbox" });
            //listColumnCasIdent.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Map_CAS_ECNumber", ColType = "Button", ColumnWidth = "200", ContentName = "Show Comments", BackgroundColor = "#F4E06D", CommandParam = "ShowComments" });
            return listColumnCasIdent;
        }


    }
    public class MapLibDisapprovedCasIdent
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public string IdentifierValue { get; set; }
        public string IdentifierType { get; set; }
        public int? IdentifierValueID { get; set; }
        public int? IdentifierTypeID { get; set; }
        public DateTime? DisapprovedOn { get; set; }
        public string Disapprovedby { get; set; }
        public string Comments { get; set; }
    }
}
