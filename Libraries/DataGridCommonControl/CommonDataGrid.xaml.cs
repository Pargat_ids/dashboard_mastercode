﻿using System.Windows.Controls;

namespace Libraries.DataGridCommonControl
{
    /// <summary>
    /// Interaction logic for CommonDataGrid.xaml
    /// </summary>
    public partial class CommonDataGrid : UserControl
    {
        public CommonDataGrid()
        {
            InitializeComponent();
        }
    }
}
