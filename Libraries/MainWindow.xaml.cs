﻿using System.Windows;

namespace Libraries
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Libraries.ViewModel.Win_Administrator_VM CurrentSIDataViewDataContext { get; set; }
        public MainWindow()
        {
            CurrentSIDataViewDataContext = new Libraries.ViewModel.Win_Administrator_VM();
            InitializeComponent();
            CustomLibUnitControl.DataContext = CurrentSIDataViewDataContext;
        }
    }
}
