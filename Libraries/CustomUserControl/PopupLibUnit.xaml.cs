﻿using CRA_DataAccess;
using Libraries.ViewModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;

namespace Libraries.CustomUserControl
{
    /// <summary>
    /// Interaction logic for PopupLibUnit.xaml
    /// </summary>
    public partial class PopupLibUnit : Window
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public PopupLibUnit(int UnitID)
        {
            InitializeComponent();
            var vm = new PopupLibUnit_VM(UnitID);
            DataContext = vm;
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
        }
    }
}
