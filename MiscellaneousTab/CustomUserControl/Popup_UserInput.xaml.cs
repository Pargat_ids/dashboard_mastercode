﻿using MiscellaneousTab.ViewModel;
using System.Collections.Generic;
using System.Windows;
using CRA_DataAccess.ViewModel;

namespace MiscellaneousTab.CustomUserControl
{
    /// <summary>
    /// Interaction logic for Popup_UserInput.xaml
    /// </summary>
    public partial class Popup_UserInput : Window
    {

        public Popup_UserInput(List<iUserInput> lst)
        {
            InitializeComponent();
            var vm = new Popup_UserInput_VM(this,lst);
            DataContext = vm;
        }
    }
}
