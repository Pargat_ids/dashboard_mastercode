﻿using MiscellaneousTab.ViewModel;
using System.Data;
using System.Windows;

namespace MiscellaneousTab.CustomUserControl
{
    /// <summary>
    /// Interaction logic for PopupNotesConsistency.xaml
    /// </summary>
    public partial class PopupNotesConsistency : Window
    {
        public PopupNotesConsistency(string _messageHeader, DataTable dt)
        {
            InitializeComponent();
            var vm = new PopupNotesConsistency_VM(_messageHeader,dt);
            DataContext = vm;
        }
    }
}
