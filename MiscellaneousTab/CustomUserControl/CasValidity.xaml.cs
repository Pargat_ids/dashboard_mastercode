﻿using CRA_DataAccess;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;

namespace MiscellaneousTab.CustomUserControl
{
    /// <summary>
    /// Interaction logic for CasValidity.xaml
    /// </summary>
    public partial class CasValidity : UserControl
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public CasValidity()
        {
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab2.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab3.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab4.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab5.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab6.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
            foreach (var ct in Tab7.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab8.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
            foreach (var ct in Tab9.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(headerName))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                //if (!VersionControlSystem.Model.ApplicationEngine.Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                //{
                //    ((TabItem)ct).Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    ((TabItem)ct).Visibility = Visibility.Visible;
                //}
            }
        }

       
    }
}
