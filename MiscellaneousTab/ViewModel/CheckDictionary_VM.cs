﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        private List<IChkName> listChkTb = new List<IChkName>();
        public bool _IsSelectedAllChecks { get; set; }
        public bool IsSelectedAllChecks
        {
            get => _IsSelectedAllChecks;
            set
            {
                if (Equals(_IsSelectedAllChecks, value))
                {
                    return;
                }

                _IsSelectedAllChecks = value;
                listChkTableParentOne.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                listChkTableParentTwo.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                listChkTableParentThree.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                listChkTableParentFour.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                listChkTableParentFive.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                listChkTableParentSix.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                //listChkTableParentSeven.ToList().ForEach(x =>
                //{
                //    x.IsSelected = value;
                //});
                NotifyPropertyChanged("listChkTableParentOne");
                NotifyPropertyChanged("listChkTableParentTwo");
                NotifyPropertyChanged("listChkTableParentThree");
                NotifyPropertyChanged("listChkTableParentFour");
                NotifyPropertyChanged("listChkTableParentFive");
                NotifyPropertyChanged("listChkTableParentSix");
                //NotifyPropertyChanged("listChkTableParentSeven");
                commandSelectedChangesExecute(null);
            }
        }
        public List<MapChecksVM> listChkTableParentOne { get; set; }
        public List<MapChecksVM> listChkTableParentTwo { get; set; }
        public List<MapChecksVM> listChkTableParentThree { get; set; }
        public List<MapChecksVM> listChkTableParentFour { get; set; }
        public List<MapChecksVM> listChkTableParentFive { get; set; }
        public List<MapChecksVM> listChkTableParentSix { get; set; }
        //public List<MapChecksVM> listChkTableParentSeven { get; set; }
        public ICommand CheckDictionaryFile { get; set; }
        public Visibility uploadPassedCheck { get; set; } = Visibility.Collapsed;
        public Visibility uploadFailedCheck { get; set; } = Visibility.Collapsed;
        private List<ICheckPassCases> _listPassedCheck { get; set; } = new List<ICheckPassCases>();
        public List<ICheckPassCases> listPassedCheck
        {
            get { return _listPassedCheck; }
            set
            {
                if (_listPassedCheck != value)
                {
                    _listPassedCheck = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ICheckPassCases>>>(new PropertyChangedMessage<List<ICheckPassCases>>(null, _listPassedCheck, "Default List"));
                }
            }
        }
        private void BindChecks()
        {


            //Tables check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "DATA", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "DATA_DICTIONARY", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "LIST_DICTIONARY", ParentID = 1 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "NOTES", ParentID = 1 });

            //Data Field Check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "RN, CAS, PREF", ParentID = 2 });

            //RN check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "RN Should be number in DATA", ParentID = 3 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain Null entries in DATA when Omit is Null", ParentID = 3 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain duplicate entries in DATA", ParentID = 3 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Should not equal to Zero", ParentID = 3 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "No decimal values allowed", ParentID = 3 });

            //DD check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Fields listed in DATA_Dictionary are present in DATA", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "RN Should be number in Data-Dictionary", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain Null entries in Data-Dictionary", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain duplicate entries in Data-Dictionary", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "All fields in DATA Should present in DATA_Dictionary", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field sizes in DATA should same define in DATA_DICTIONARY", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Normalized FieldName not in Data-Dictionary", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field Names cannot contain Space", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "FieldNames cannot start with a number", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field Names cannot contain any characters other than [a-z][0-9]_", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Max length of a note text should be less than 2000 characters", ParentID = 4 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Not allow duplicate data in HEAD_CD", ParentID = 4 });
            //listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "All non-internal fields in QSxxx_DataDictionary", ParentID = 4 });


            //lD checks
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field PUB_DATE in mm/dd/yyyy format", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Two QSID shouldn't linked one List_FieldName", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Two List_FieldName shouldn't linked one QSID", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Short Name Length greater than 70 Chracter", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "List Phrase Length greater than 200 Chracter", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "List Phrase and List Explain not matched", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "QC check Country not present in lookup Table", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Pub-date is greater that today's date", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Validate QSID", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "NonEnglish_ListPhrase Equals to List_Phrase", ParentID = 5 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Legislation Id not exist in Library", ParentID = 5 });


            // Notes Check
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Should have columns -  RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL", ParentID = 6 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "RN Should be number in Notes", ParentID = 6 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain Null entries in Notes", ParentID = 6 });
            listChkTb.Add(new IChkName { IsSelected = false, ChkDescription = "Field RN cannot contain duplicate entries in Notes", ParentID = 6 });

            listChkTableParentOne = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 1).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            listChkTableParentTwo = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 2).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            listChkTableParentThree = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 3).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            listChkTableParentFour = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 4).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            listChkTableParentFive = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 5).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            listChkTableParentSix = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 6).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            //listChkTableParentSeven = new List<MapChecksVM>(listChkTb.Where(y => y.ParentID == 7).Select(z => new MapChecksVM { ChkDescription = z.ChkDescription, IsSelected = z.IsSelected, ParentID = z.ParentID }).ToList());
            NotifyPropertyChanged("listChkTableParentOne");
            NotifyPropertyChanged("listChkTableParentTwo");
            NotifyPropertyChanged("listChkTableParentThree");
            NotifyPropertyChanged("listChkTableParentFour");
            NotifyPropertyChanged("listChkTableParentFive");
            NotifyPropertyChanged("listChkTableParentSix");
            //NotifyPropertyChanged("listChkTableParentSeven");

        }
        private void CheckDictionaryFileExecute(object o)
        {
            if (string.IsNullOrEmpty(CommonFilepath))
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }

            uploadPassedCheck = Visibility.Visible;
            NotifyPropertyChanged("uploadPassedCheck");
            uploadFailedCheck = Visibility.Visible;
            NotifyPropertyChanged("uploadFailedCheck");
            _ = Task.Run(() =>
              {


                  List<ICheckPassCases> lstFinalPassed = new List<ICheckPassCases>();
                  List<string> chkedItems = new List<string>();
                  chkedItems.AddRange(listChkTableParentOne.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  chkedItems.AddRange(listChkTableParentTwo.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  chkedItems.AddRange(listChkTableParentThree.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  chkedItems.AddRange(listChkTableParentFour.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  chkedItems.AddRange(listChkTableParentFive.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  chkedItems.AddRange(listChkTableParentSix.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  //chkedItems.AddRange(listChkTableParentSeven.Where(x => x.IsSelected).Select(y => y.ChkDescription).ToList());
                  var lstDate1 = objCommon.DbaseQueryReturnTable("select top 1 LIST_FIELD_NAME, QSID, country,[module],  LIST_PHRASE, SHORT_NAME,  PUB_DATE,LIST_EXPLAIN,  SHORT_DESCRIPTION  from LIST_DICTIONARY", CommonFilepath);
                  if (lstDate1.Rows.Count == 0)
                  {
                      _notifier.ShowError("Unable to find columns LIST_FIELD_NAME, QSID, country,[module],  LIST_PHRASE, SHORT_NAME,  PUB_DATE,LIST_EXPLAIN,  SHORT_DESCRIPTION, contact administrator");
                      return;
                  }
                  var allColumnName = objCommon.DbaseQueryReturnTable("select top 1  *  from DATA", CommonFilepath);
                  var listFieldCateid = objCommon.DbaseQueryReturnStringSQL("select top 1 PhraseCategoryID  from Library_PhraseCategories where phrasecategory ='List Field Name'", CasValidity_VM.CommonListConn);
                  var lstQsid = lstDate1.AsEnumerable().Select(y => y.Field<string>("QSID")).FirstOrDefault();
                  var lstqsidid = objCommon.DbaseQueryReturnStringSQL("select top 1 Qsid_ID from Library_Qsids where upper(qsid) ='" + lstQsid.ToUpper() + "'", CasValidity_VM.CommonListConn);
                  if (lstqsidid == "")
                  {
                      var lst = new List<string>();
                      var resultQsidInsert = _objLibraryFunction_Service.AddQSIDInLibrary(lstQsid, Engine.CurrentUserSessionID, objCommon.ESTTime(), out lst);
                      if (resultQsidInsert != "Pass")
                      {
                          _notifier.ShowError("Unable to Insert Qsid in Library Table, contact administrator");
                          return;
                      }
                  }
                  lstqsidid = objCommon.DbaseQueryReturnStringSQL("select top 1 Qsid_ID from Library_Qsids where upper(qsid) ='" + lstQsid.ToUpper() + "'", CasValidity_VM.CommonListConn);

                  var allFieldNameofDD = objCommon.DbaseQueryReturnTable("select FIELD_NAME, DF1_REQUIRED, Head_CD, EXPLAIN,Phrase from DATA_Dictionary", CommonFilepath);
                  var noteDataTypeId = objCommon.DbaseQueryReturnStringSQL("select top 1 DataTypeID from Library_DataType where TypeDescription ='Notes'", CasValidity_VM.CommonListConn);
                  List<string> DataDicField = new List<string>();
                  allFieldNameofDD.AsEnumerable().ToList().ForEach(x =>
                  {
                      DataDicField.Add(x.Field<string>("FIELD_NAME").ToUpper());
                  });
                  for (int i = 0; i < chkedItems.Count; i++)
                  {
                      switch (chkedItems[i].ToString())
                      {
                          case "DATA":
                              var tabData = chkTableExist("DATA");
                              lstFinalPassed.Add(tabData);
                              break;
                          case "DATA_DICTIONARY":
                              var tabDDic = chkTableExist("DATA_DICTIONARY");
                              lstFinalPassed.Add(tabDDic);
                              break;
                          case "LIST_DICTIONARY":
                              var tabLDic = chkTableExist("LIST_DICTIONARY");
                              lstFinalPassed.Add(tabLDic);
                              break;
                          case "NOTES":
                              var tabNotes = chkTableExist("NOTES");
                              lstFinalPassed.Add(tabNotes);
                              break;
                          case "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY":
                              int mismatch = 0;
                              var chkdataCol = objCommon.DbaseQueryReturnTable("select top 1  * from DATA", CommonFilepath);
                              var dtdata = new DataTable();
                              dtdata.Columns.Add("FieldName");
                              dtdata.Columns.Add("RN");

                              for (int ii = 0; ii < chkdataCol.Columns.Count; ii++)
                              {
                                  dtdata.Rows.Add(chkdataCol.Columns[ii].ToString().ToUpper().Trim(), (ii + 1));
                              }
                              var chkdatadic = objCommon.DbaseQueryReturnTable("select ltrim(rtrim(ucase(Field_Name))) as FieldName, RN from DATA_DICTIONARY", CommonFilepath);
                              var diff = (from d1 in chkdatadic.AsEnumerable()
                                          join d2 in dtdata.AsEnumerable()
                                          on new
                                          {
                                              a = d1.Field<string>("FieldName"),
                                              b = Convert.ToInt32(d1.Field<dynamic>("RN"))
                                          }
                                          equals new
                                          {
                                              a = d2.Field<string>("FieldName"),
                                              b = Convert.ToInt32(d2.Field<dynamic>("RN"))
                                          }
                                          into tg
                                          from tcheck in tg.DefaultIfEmpty()
                                          where tcheck == null
                                          select d1);

                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY",
                                  Status = diff.Any() ? "Fail" : "Pass",
                                  Remarks = diff.Any() ? "Order of Fields in table DATA should not match the order of fields in table DATA_DICTIONARY" : "Order of Fields in table DATA should match the order of fields in table DATA_DICTIONARY"
                              });
                              break;
                          case "RN, CAS, PREF":
                              var chkRn = objCommon.DbaseQueryReturnTable("select top 1  RN, CAS, PREF from DATA", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN, CAS, PREF",
                                  Status = chkRn.Columns.Count == 0 ? "Fail" : "Pass",
                                  Remarks = chkRn.Columns.Count == 0 ? "Not Found RN, CAS, PREF Field in Table DATA" : "Found RN, CAS, PREF Field in Table DATA"
                              });
                              break;
                          case "Should have columns -  RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL":
                              var chkcolExs = objCommon.DbaseQueryReturnTable("select top 1  RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL from Notes", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Should have columns -  RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL in Notes",
                                  Status = chkcolExs.Columns.Count == 0 ? "Fail" : "Pass",
                                  Remarks = chkcolExs.Columns.Count == 0 ? "Not Found RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL in Notes" : "Found RN,  NOTE_CODE, NOTES_FIELD, NOTE_TEXT, NOTE_ENGL in Notes"
                              });
                              break;
                          case "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE":
                              var chkcolDDExs = objCommon.DbaseQueryReturnTable("select top 1  RN,  FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE from Data_Dictionary", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Should have columns - RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE in Data_Dictionary",
                                  Status = chkcolDDExs.Columns.Count == 0 ? "Fail" : "Pass",
                                  Remarks = chkcolDDExs.Columns.Count == 0 ? "Not Found RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE in Data_Dictionary" : "Found RN, FIELD_NAME, DF1_REQUIRED, UNITS_FLD, HEAD_CD, EXPLAIN, PHRASE in Data_Dictionary"
                              });
                              break;

                          case "Not allow duplicate data in HEAD_CD":
                              var chkDupHC = objCommon.DbaseQueryReturnTable("select ltrim(rtrim(ucase(HEAD_CD))) from Data_Dictionary where Head_CD is not null and Head_cd <> '' group by ucase(Head_CD) having count(*) > 1", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Not allow duplicate data in HEAD_CD",
                                  Status = chkDupHC.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkDupHC.Rows.Count > 0 ? "Found duplicate data in HEAD_CD -" + chkDupHC.Rows[0][0].ToString() : ""
                              });
                              break;
                          case "RN Should be number in DATA":
                              var chkRnNonNumber = objCommon.DbaseQueryReturnTable("select top 1 RN from DATA where isnumeric(RN) = 0", CommonFilepath);
                              var chkRNDataType = objCommon.GetAccessTableColumnDataType("DATA", "RN", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN Should be number in Data",
                                  Status = chkRnNonNumber.Rows.Count > 0 || !chkRNDataType.ToUpper().Contains("INT") ? "Fail" : "Pass",
                                  Remarks = chkRnNonNumber.Columns.Count == 0 ? "RN Field Not found in Data" : chkRnNonNumber.Rows.Count > 0 ? "RN is Non-Numeric in Data -" + chkRnNonNumber.Rows[0][0].ToString() : !chkRNDataType.ToUpper().Contains("INT") ? "RN DataType is Non-Numeric" : "RN is Numeric in Data",
                              });
                              break;
                          case "RN Should be number in Data-Dictionary":
                              var chkRNInteger = objCommon.DbaseQueryReturnTable("select top 1 RN from Data_Dictionary where isnumeric(RN) = 0", CommonFilepath);
                              var chkRNDataTypeDD = objCommon.GetAccessTableColumnDataType("Data_Dictionary", "RN", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN Should be number in Data-Dictionary",
                                  Status = chkRNInteger.Rows.Count == 0 && (chkRNDataTypeDD.ToUpper().Contains("INT") || chkRNDataTypeDD.ToUpper().Contains("DOUBLE") || chkRNDataTypeDD.ToUpper().Contains("DECIMAL") || chkRNDataTypeDD.ToUpper().Contains("FLOAT")) ? "Pass" : "Fail",
                                  Remarks = chkRNInteger.Columns.Count == 0 ? "RN Field Not found in Data-Dictionary" : chkRNInteger.Rows.Count > 0 ? "RN is Non-Numeric in Data-Dictionary -" + chkRNInteger.Rows[0][0].ToString() : chkRNDataTypeDD.ToUpper().Contains("INT") || chkRNDataTypeDD.ToUpper().Contains("DOUBLE") || chkRNDataTypeDD.ToUpper().Contains("DECIMAL") || chkRNDataTypeDD.ToUpper().Contains("FLOAT") ? "RN is Numeric in Data-Dictionary" : "RN DataType is Non-Numeric"
                              });
                              break;
                          case "RN Should be number in Notes":
                              var chkRNNotes = objCommon.DbaseQueryReturnTable("select top 1 RN from Notes where isnumeric(RN) = 0", CommonFilepath);
                              var chkRNDataTypeN = objCommon.GetAccessTableColumnDataType("Notes", "RN", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN Should be number in Notes",
                                  Status = chkRNNotes.Rows.Count > 0 || !chkRNDataTypeN.ToUpper().Contains("INT") ? "Fail" : "Pass",
                                  Remarks = chkRNNotes.Columns.Count == 0 ? "RN Field Not found in Notes" : chkRNNotes.Rows.Count > 0 ? "RN is Non-Numeric in Notes -" + chkRNNotes.Rows[0][0].ToString() : !chkRNDataTypeN.ToUpper().Contains("INT") ? "RN DataType is Non-Numeric" : "RN is Numeric in Notes",
                              });
                              break;
                          case "Field RN cannot contain Null entries in DATA when Omit is Null":
                              var chkRnNull = objCommon.DbaseQueryReturnTable("select top 1  RN from DATA where (omit is null or omit ='') and  RN is null or RN =''", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN Should not be Null, when Field Omit is Null in DATA",
                                  Status = chkRnNull.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRnNull.Rows.Count > 0 ? "Found Null RN in DATA" : "Not Found Null RN in DATA",

                              });
                              break;
                          case "Field RN cannot contain Null entries in Data-Dictionary":
                              var chkRnDD = objCommon.DbaseQueryReturnTable("select top 1  RN from Data_Dictionary where RN is null or RN =''", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field RN cannot contain Null entries in Data-Dictionary",
                                  Status = chkRnDD.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRnDD.Rows.Count > 0 ? "Found Null RN in Data-Dictionary" : "Not Found Null RN in Data-Dictionary",

                              });
                              break;
                          case "Field RN cannot contain Null entries in Notes":
                              var chkRnNotes = objCommon.DbaseQueryReturnTable("select top 1  RN from Notes where RN is null or RN =''", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field RN cannot contain Null entries in Notes",
                                  Status = chkRnNotes.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRnNotes.Rows.Count > 0 ? "Found Null RN in Notes" : "Not Found Null RN in Notes",

                              });
                              break;
                          case "Field RN cannot contain duplicate entries in DATA":
                              var chkRnunique = objCommon.DbaseQueryReturnTable("select RN from DATA group by RN having count(*) > 1", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field RN cannot contain duplicate entries in DATA",
                                  Status = chkRnunique.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRnunique.Rows.Count > 0 ? "Found Duplicate RN in DATA" : "Not Found Duplicate RN in DATA",
                              });
                              break;
                          case "Field RN cannot contain duplicate entries in Data-Dictionary":
                              var chkRDD = objCommon.DbaseQueryReturnTable("select RN from Data_Dictionary group by RN having count(*) > 1", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field RN cannot contain duplicate entries in Data-Dictionary",
                                  Status = chkRDD.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRDD.Rows.Count > 0 ? "Found Duplicate RN in Data-Dictionary" : "Not Found Duplicate RN in Data-Dictionary",
                              });
                              break;
                          case "Field RN cannot contain duplicate entries in Notes":
                              var chkRNotes = objCommon.DbaseQueryReturnTable("select RN from Notes group by RN having count(*) > 1", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field RN cannot contain duplicate entries in Notes",
                                  Status = chkRNotes.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRNotes.Rows.Count > 0 ? "Found Duplicate RN in Notes" : "Not Found Duplicate RN in Notes",
                              });
                              break;
                          case "Should not equal to Zero":
                              var chkRnZero = objCommon.DbaseQueryReturnTable("select top 1 RN from DATA where RN = 0", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "RN Should not equal to Zero",
                                  Status = chkRnZero.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRnZero.Rows.Count > 0 ? "Found Zero in RN" : "Not Found Zero in RN",
                              });
                              break;
                          case "No decimal values allowed":
                              var chkRndecimal = objCommon.DbaseQueryReturnTable("select top 1 RN from DATA where instr(1,RN,'.') > 0", CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "No decimal values allowed in RN",
                                  Status = chkRndecimal.Rows.Count > 0 ? "Fail" : "Pass",
                                  Remarks = chkRndecimal.Rows.Count > 0 ? "Found decimal value in RN" : "Not Found decimal value in RN",
                              });
                              break;
                          case "Fields listed in DATA_Dictionary are present in DATA":
                              string notfoundFieldsinData = string.Empty;
                              for (int ii = 0; ii < DataDicField.Count; ii++)
                              {
                                  if (!allColumnName.Columns.Contains(DataDicField[ii].ToString().ToUpper()))
                                  {

                                      notfoundFieldsinData += DataDicField[ii].ToString() + ",";
                                  }
                              }
                              if (notfoundFieldsinData != string.Empty)
                              {
                                  notfoundFieldsinData = notfoundFieldsinData.TrimEnd(',');
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Fields listed in DATA_Dictionary are present in DATA",
                                      Status = "Fail",
                                      Remarks = "Columns not Found in Data - " + notfoundFieldsinData,
                                  });
                              }
                              else
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Fields listed in DATA_Dictionary are present in DATA",
                                      Status = "Pass",
                                      Remarks = "All Data-Dictionary Fields Found in Data"
                                  });
                              }
                              break;
                          case "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE":
                              var EmptyFields = string.Empty;
                              string notfoundFieldsinDataDictionary = string.Empty;
                              var chkFrommapQsid = objCommon.DbaseQueryReturnTableSql("select FieldName from Library_FieldNames a, Map_QSID_FieldName b " +
                              " where a.FieldNameID = b.FieldNameID and b.IsInternalOnly = 0 and b.QSID_ID = " + lstqsidid + " and b.isunit != 1", CasValidity_VM.CommonListConn);
                              chkFrommapQsid.AsEnumerable().ToList().ForEach(x =>
                              {
                                  var fldName = x.Field<string>("FieldName").ToUpper();
                                  var chkDFREquiredTrue = allFieldNameofDD.AsEnumerable().FirstOrDefault(xx => xx.Field<string>("FIELD_NAME").ToUpper() == fldName);
                                  if (chkDFREquiredTrue != null)
                                  {
                                      if (chkDFREquiredTrue[1].GetType() == typeof(System.Double))
                                      {
                                          if (chkDFREquiredTrue.Field<double>("DF1_REQUIRED") == 1 && (string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("Head_CD")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("EXPLAIN")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("PHRASE"))))
                                          {
                                              EmptyFields += fldName + ",";
                                          }
                                      }
                                      if (chkDFREquiredTrue[1].GetType() == typeof(System.Boolean))
                                      {
                                          if (chkDFREquiredTrue.Field<bool>("DF1_REQUIRED") && (string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("Head_CD")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("EXPLAIN")) || string.IsNullOrEmpty(chkDFREquiredTrue.Field<string>("PHRASE"))))
                                          {
                                              EmptyFields += fldName + ",";
                                          }
                                      }
                                  }
                                  else
                                  {
                                      notfoundFieldsinDataDictionary += fldName + ",";
                                  }
                              });
                              if (EmptyFields != string.Empty)
                              {
                                  EmptyFields = EmptyFields.TrimEnd(',');
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE",
                                      Status = "Fail",
                                      Remarks = "Non-Internalonly Fields found null in - " + EmptyFields,

                                  });
                              }
                              else
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Non-Internalonly Fields should not be NULL in HEAD_CD, EXPLAIN, PHRASE",
                                      Status = notfoundFieldsinDataDictionary != string.Empty ? "Fail" : "Pass",
                                      Remarks = notfoundFieldsinDataDictionary != string.Empty ? "Non-Internal only fields not found in Data-Dictionary -" + notfoundFieldsinDataDictionary : "Non-Internalonly Fields not null in HEAD_CD, EXPLAIN, PHRASE ",
                                  });
                              }
                              break;
                          case "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION":
                              string EmptyFields1 = string.Empty;
                              for (int ii = 0; ii < lstDate1.Columns.Count; ii++)
                              {
                                  if (string.IsNullOrEmpty(lstDate1.Rows[0][ii].ToString()))
                                  {
                                      EmptyFields1 += lstDate1.Rows[0][ii].ToString() + ",";
                                  }
                              }
                              if (EmptyFields1 != string.Empty)
                              {
                                  EmptyFields1 = EmptyFields1.TrimEnd(',');
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION",
                                      Status = "Fail",
                                      Remarks = "Empty Field Found in LIST_DICTIONARY - " + EmptyFields1,
                                  });
                              }
                              else
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Should not empty: QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION",
                                      Status = lstDate1.Columns.Count == 0 ? "Fail" : "Pass",
                                      Remarks = lstDate1.Columns.Count == 0 ? "QSID,LIST_FIELD_NAME, MODULE, COUNTRY, PUB_DATE, List_Phrase,SHORT_NAME, SHORT_DESCRIPTION not found" : "Not Found Empty Field in LIST_DICTIONARY "
                                  });
                              }
                              break;
                          case "Field PUB_DATE in mm/dd/yyyy format":
                              string lstDate = Convert.ToString(lstDate1.AsEnumerable().Select(x => x.Field<dynamic>("PUB_DATE")).FirstOrDefault());
                              var result = CheckingInvalidDateFormatInListDictionary(lstDate);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field PUB_DATE in mm/dd/yyyy format",
                                  Status = result == "Pass" ? "Pass" : "Fail",
                                  Remarks = result == "Pass" ? "PUB_DATE in mm/dd/yyyy format" : "PUB_DATE in not in mm/dd/yyyy format",
                              });
                              break;
                          case "Two QSID shouldn't linked one List_FieldName":
                              var lstFldName = lstDate1.AsEnumerable().Select(y => y.Field<string>("LIST_FIELD_NAME")).FirstOrDefault();
                              var lstPhrID = objCommon.DbaseQueryReturnStringSQL("select PhraseId from Library_Phrases where languageid = 1 and phrase ='" + lstFldName + "' COLLATE SQL_Latin1_General_CP1_CS_AS ", CasValidity_VM.CommonListConn);
                              if (lstPhrID == "")
                              {
                                  lstPhrID = "0";
                              }
                              var resultNew = objCommon.DbaseQueryReturnTableSql("select distinct qsid_ID from QSxxx_ListDictionary where phrasecategoryID =" + Convert.ToInt32(listFieldCateid) + " and phraseid =" + Convert.ToInt32(lstPhrID), CasValidity_VM.CommonListConn);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Two QSID shouldn't linked one List_FieldName",
                                  Status = lstPhrID == "0" ? "Warning" : resultNew.Rows.Count <= 1 ? "Pass" : "Fail",
                                  Remarks = lstPhrID == "0" ? "Its New LIST_FIELD_NAME,  Not Normalized Yet" : resultNew.Rows.Count <= 1 ? "Pass" : "Found in these Qsid_IDs -" + string.Join(",", resultNew.AsEnumerable().Select(y => y.Field<string>("QSID_ID")))
                              });
                              break;
                          case "Two List_FieldName shouldn't linked one QSID":
                              if (lstqsidid == "")
                              {
                                  lstqsidid = "0";
                              }
                              var result1 = objCommon.DbaseQueryReturnTableSql("select distinct phrase from QSxxx_ListDictionary a, library_phrases b where phrasecategoryID =" + Convert.ToInt32(listFieldCateid) + " and qsid_id =" + Convert.ToInt32(lstqsidid) + " and a.phraseid = b.phraseid ", CasValidity_VM.CommonListConn);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Two List_FieldName shouldn't linked one QSID",
                                  Status = lstqsidid == "0" ? "Warning" : result1.Rows.Count <= 1 ? "Pass" : "Fail",
                                  Remarks = lstqsidid == "0" ? "Its New Qsid,  Not Normalized Yet" : result1.Rows.Count <= 1 ? "Pass" : "Found in these List_Field_Name -" + string.Join(",", result1.AsEnumerable().Select(y => y.Field<string>("phrase"))),
                              });
                              break;
                          case "All fields in DATA Should present in DATA_Dictionary":
                              List<string> notfoundFields = new List<string>();
                              for (int ii = 0; ii < allColumnName.Columns.Count; ii++)
                              {
                                  if (!DataDicField.Contains(allColumnName.Columns[ii].ColumnName.ToString().ToUpper()))
                                  {

                                      notfoundFields.Add(allColumnName.Columns[ii].ColumnName.ToString());
                                  }
                              }
                              if (notfoundFields.Any())
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "All fields in DATA Should present in DATA_Dictionary",
                                      Status = "Fail",
                                      Remarks = "All Data Columns not Found in Data-Dictionary- " + string.Join(",", notfoundFields),
                                  });
                              }
                              else
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "All fields in DATA Should present in DATA_Dictionary",
                                      Status = "Pass",
                                      Remarks = "All Data Columns Found in Data-Dictionary"
                                  });
                              }
                              break;
                          case "Field sizes in DATA should same define in DATA_DICTIONARY":
                              var res = _objLibraryFunction_Service.GetSchemaBtn_Click(CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field sizes in DATA should same define in DATA_DICTIONARY - " + res,
                                  Status = res == string.Empty ? "Pass" : "Fail",
                                  Remarks = res == string.Empty ? "Field sizes same in DATA as define in DATA_DICTIONARY" : "Field sizes not same in DATA as define in DATA_DICTIONARY - " + res
                              });
                              break;
                          case "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY":
                              var NonNotesFields = string.Empty;
                              var chkNoteCodeField = objCommon.DbaseQueryReturnTable("select distinct NOTES_FIELD from notes a, Data_Dictionary b where a.notes_field = b.FIELD_NAME where Data_Type <> " + noteDataTypeId, CommonFilepath);
                              for (int ii = 0; ii < chkNoteCodeField.Rows.Count; ii++)
                              {
                                  NonNotesFields += chkNoteCodeField.Rows[ii][0].ToString() + ",";
                              }
                              if (NonNotesFields != string.Empty)
                              {
                                  NonNotesFields = NonNotesFields.TrimEnd(',');
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY",
                                      Status = "Fail",
                                      Remarks = "Fields not define NOTES in DATA_DICTIONARY- " + NonNotesFields,
                                  });
                              }
                              else
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Fields listed under NOTES_FIELD in table NOTES should define NOTES in DATA_DICTIONARY",
                                      Status = "Pass",
                                      Remarks = "All notes Fields define NOTES in DATA_DICTIONARY",
                                  });
                              }
                              break;
                          case "Short Name Length greater than 70 Chracter":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Short Name Length greater than 70 Chracter",
                                  Status = lstDate1.Rows[0]["SHORT_NAME"].ToString().Trim().Length > 70 ? "Fail" : "Pass",
                                  Remarks = ""
                              });
                              break;
                          case "List Phrase Length greater than 200 Chracter":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Short Name Length greater than 70 Chracter",
                                  Status = lstDate1.Rows[0]["LIST_PHRASE"].ToString().Trim().Length > 200 ? "Fail" : "Pass",
                                  Remarks = ""
                              });
                              break;
                          case "List Phrase and List Explain not matched":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "List Phrase and List Explain not matched",
                                  Status = lstDate1.Rows[0]["LIST_PHRASE"].ToString().Trim() != lstDate1.Rows[0]["LIST_EXPLAIN"].ToString().Trim() ? "Fail" : "Pass",
                                  Remarks = ""
                              });
                              break;
                          case "QC check Country not present in lookup Table":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "QC check Country not present in lookup Table",
                                  Status = objCommon.CheckCountryCode(lstDate1),
                                  Remarks = ""
                              });
                              break;
                          case "Pub-date is greater that today's date":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Pub-date is greater that today's date",
                                  Status = objCommon.CheckingDateNotGreaterThanToday(lstDate1.Rows[0]["PUB_DATE"].ToString().Trim()),
                                  Remarks = ""
                              });
                              break;
                          case "Validate Qsid":
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Validate Qsid",
                                  Status = lstDate1.AsEnumerable().Count(x => string.IsNullOrWhiteSpace(x.Field<string>("QSID").Trim()) || (x.Field<string>("QSID").Length > 15)) > 0 ? "Fail" : "Pass",
                                  Remarks = ""
                              });
                              break;
                          case "NonEnglish_ListPhrase Equals to List_Phrase":
                              var resPh = objCommon.DbaseQueryReturnTableSql("select top 1 NonEnglish_ListPhrase, LIST_PHRASE from LIST_DICTIONARY", CommonFilepath);
                              if (resPh == null || resPh.Rows.Count == 0)
                              {
                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Legislation Id not exist in Library",
                                      Status = "Pass",
                                      Remarks = ""
                                  });
                              }
                              else
                              {
                                  if ((resPh.Rows[0]["NonEnglish_ListPhrase"] == resPh.Rows[0]["LIST_PHRASE"]) && (resPh.Rows[0]["NonEnglish_ListPhrase"] != null) && (resPh.Rows[0]["LIST_PHRASE"].ToString() != string.Empty))
                                  {
                                      lstFinalPassed.Add(new ICheckPassCases
                                      {
                                          Description = "Legislation Id not exist in Library",
                                          Status = "Fail",
                                          Remarks = ""
                                      });
                                  }
                                  else
                                  {
                                      lstFinalPassed.Add(new ICheckPassCases
                                      {
                                          Description = "Legislation Id not exist in Library",
                                          Status = "Pass",
                                          Remarks = ""
                                      });
                                  }
                              }
                              break;
                          case "Legislation Id not exist in Library":
                              var resLeg = objCommon.DbaseQueryReturnString("select top 1 LegislationID from LIST_DICTIONARY", CommonFilepath);
                              string chkLib = string.Empty;
                              if (resLeg != string.Empty)
                              {
                                  chkLib = objCommon.DbaseQueryReturnStringSQL("select top 1 LG_ID from Library_Legislations where LegislationID = '" + resLeg + "'", CommonListConn);
                              }
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Legislation Id not exist in Library",
                                  Status = resLeg == string.Empty || resLeg == null ? "Pass" : chkLib == string.Empty ? "Fail" : "Pass",
                                  Remarks = ""
                              });
                              break;
                          case "Normalized FieldName not in Data-Dictionary":
                              using (var context = new CRAModel())
                              {
                                  List<string> notExistinDD = new List<string>();
                                  var qsidid1 = Convert.ToInt32(lstqsidid);
                                  var chkMapQsid = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid1 && x.UnitID != null && x.UnitFieldNameID != null).ToList();
                                  var virtualFields = chkMapQsid.Select(y => y.UnitFieldNameID).ToList();
                                  var fieldNameInDataDic = objCommon.DbaseQueryReturnTable("select FIELD_NAME from Data_Dictionary", CommonFilepath);
                                  var fieldNames = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid1 && x.IsInternalOnly == false && x.ParentId == null).ToList()
                                                    join d2 in context.Library_FieldNames.AsNoTracking()
                                                        on d1.FieldNameID equals d2.FieldNameID
                                                    select new
                                                    {
                                                        FieldName = d2.FieldName.Replace("\n", string.Empty).Replace("\r", string.Empty).ToUpper(),
                                                        d2.FieldNameID,
                                                    }).ToList();
                                  if (virtualFields.Any())
                                  {
                                      fieldNames = fieldNames.Where(x => !virtualFields.Contains(x.FieldNameID)).ToList();
                                  }
                                  fieldNames.ToList().ForEach(x =>
                                  {
                                      if (fieldNameInDataDic.AsEnumerable().Count(xx => xx.Field<string>("FIELD_NAME").ToUpper() == x.FieldName) == 0)
                                      {
                                          notExistinDD.Add(x.FieldName);
                                      }
                                  });

                                  lstFinalPassed.Add(new ICheckPassCases
                                  {
                                      Description = "Normalized FieldName not in Data - Dictionary",
                                      Status = notExistinDD.Count() > 0 ? "Fail" : "Pass",
                                      Remarks = string.Join(",", notExistinDD.ToList()),
                                  });
                              }
                              break;
                          case "Field Names cannot contain Space":
                              var chkSpace = allFieldNameofDD.AsEnumerable().Where(x => x.Field<string>("FIELD_NAME").Contains(" ")).Select(y => y.Field<string>("FIELD_NAME")).ToList();
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field Names cannot contain Space",
                                  Status = chkSpace.Count == 0 ? "Pass" : "Fail",
                                  Remarks = string.Join(",", chkSpace)
                              });
                              break;
                          case "FieldNames cannot start with a number":
                              var numErr = new List<string>();
                              var chkNumber = allFieldNameofDD.AsEnumerable().Select(y => y.Field<string>("FIELD_NAME")).ToList();
                              foreach (var ct in chkNumber)
                              {
                                  char[] charArr = ct.ToCharArray();
                                  if (Char.IsDigit(charArr[0]))
                                  {
                                      numErr.Add(ct);
                                  }
                              }
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "FieldNames cannot start with a number",
                                  Status = numErr.Count == 0 ? "Pass" : "Fail",
                                  Remarks = string.Join(",", numErr)
                              });
                              break;
                          case "Field Names cannot contain any characters other than [a-z][0-9]_":
                              var numDig = new List<string>();
                              var chkDig = allFieldNameofDD.AsEnumerable().Select(y => y.Field<string>("FIELD_NAME")).ToList();
                              foreach (var ct in chkDig)
                              {
                                  if (ct.All(c => c >= 48 && c <= 57 || c >= 65 && c <= 90 || c >= 97 && c <= 122 || c == 95))
                                  {
                                  }
                                  else
                                  {
                                      numDig.Add(ct);
                                  }
                              }
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Field Names cannot contain any characters other than [a-z][0-9]_",
                                  Status = numDig.Count == 0 ? "Pass" : "Fail",
                                  Remarks = string.Join(",", numDig)
                              });
                              break;
                          case "Max length of a note text should be less than 2000 characters":
                              var fieldNams = objCommon.DbaseQueryReturnTable("select FIELD_NAME as FieldName from DATA_Dictionary where DATA_TYPE =6", CommonFilepath);
                              var chknoteCodeField = objCommon.DbaseQueryReturnTableSql("select FieldName as FieldName from Library_FieldNames a, Map_QSID_FieldName b " +
                              " where a.FieldNameID = b.FieldNameID and b.isNoteCodeField = 1 and b.QSID_ID = " + lstqsidid, CasValidity_VM.CommonListConn);
                              if (chknoteCodeField.Rows.Count > 0)
                              {
                                  fieldNams.Merge(chknoteCodeField);
                              }
                              string extraQuery = string.Empty;
                              if (fieldNams.Rows.Count > 0)
                              {
                                  var uniqueFields = fieldNams.AsEnumerable().Select(x => "'" + x.Field<string>("FieldName").ToUpper() + "'").Distinct().ToList();
                                  var joinAll = string.Join(",", uniqueFields);
                                  extraQuery = " and ucase(NOTES_FIELD) in (" + joinAll + ")";
                              }
                              var notesField = objCommon.DbaseQueryReturnTable("select NOTES_FIELD & '(' & NOTE_CODE & ')' as NOTES_FIELD_CODE from Notes where (len(NOTE_TEXT) > 2000 or len(NOTE_ENGL) > 2000 ) " + extraQuery, CommonFilepath);
                              lstFinalPassed.Add(new ICheckPassCases
                              {
                                  Description = "Max length of a note text should be less than 2000 characters",
                                  Status = notesField.Rows.Count == 0 ? "Pass" : "Fail",
                                  Remarks = string.Join(",", notesField.AsEnumerable().Select(y => y.Field<string>("NOTES_FIELD_CODE")).ToList()),
                              });
                              break;
                      }
                  }
                  lstFinalPassed.ForEach(x =>
                  {
                      x.NeedToAddLibrary1 = x.Status == "Pass" ? 1 : x.Status == "Fail" ? 2 : 3;
                  });

                  var chkpass = lstFinalPassed.OrderBy(z => z.Status).ToList();
                  listPassedCheck = new List<ICheckPassCases>(chkpass);
                  NotifyPropertyChanged("ComonlistPassedCheck");
                  uploadFailedCheck = Visibility.Collapsed;
                  NotifyPropertyChanged("uploadFailedCheck");
                  uploadPassedCheck = Visibility.Collapsed;
                  NotifyPropertyChanged("uploadPassedCheck");
              });
        }


        private string CheckingInvalidDateFormatInListDictionary(string lstDate)
        {
            try
            {
                if (lstDate == null)
                {
                    return "Fail";
                }

                string stringdate = lstDate.Replace("12:00:00 AM", "").Trim();
                DateTime date = Convert.ToDateTime(stringdate);
                string tempdate = date.ToString("MM/dd/yyyy");
                int monTemp = Convert.ToInt32(tempdate.Substring(0, tempdate.IndexOf("/")));
                int monStr = Convert.ToInt32(stringdate.Substring(0, stringdate.IndexOf("/")));
                int firstIndexTemp = tempdate.IndexOf("/");
                int lastIndexTemp = tempdate.LastIndexOf("/");
                int firstIndexStr = stringdate.IndexOf("/");
                int lastIndexStr = stringdate.LastIndexOf("/");
                int dayTemp = Convert.ToInt32(tempdate.Substring(firstIndexTemp + 1, lastIndexTemp - firstIndexTemp - 1));
                int dayStr = Convert.ToInt32(stringdate.Substring(firstIndexStr + 1, lastIndexStr - firstIndexStr - 1));
                int yrTemp = Convert.ToInt32(tempdate.Substring(lastIndexTemp + 1));
                int yrStr = Convert.ToInt32(stringdate.Substring(lastIndexStr + 1));

                return (monTemp == monStr) && (dayTemp == dayStr) && (yrTemp == yrStr) ? "Pass" : "Fail";
            }
            catch
            {
                return "Fail";
            }
        }
        private dynamic chkTableExist(string TableName)
        {
            ICheckPassCases result;
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1  * from " + TableName, CommonFilepath);
            result = new ICheckPassCases { Description = TableName, Status = dtRaw.Columns.Count == 0 ? "Fail" : "Pass", Remarks = dtRaw.Columns.Count == 0 ? "Not Found Table - " + TableName : "Found Table - " + TableName };
            return result;

        }
        private List<CommonDataGridColumn> GetPassedColumn()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Description", ColBindingName = "Description", ColType = "Textbox", ColumnWidth = "1500" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Remarks", ColBindingName = "Remarks", ColType = "Textbox", ColumnWidth = "1500" });

            return listColumnQsidDetail;
        }
    }
    public class ICheckPassCases
    {
        public string Description { get; set; }
        public string Status { get; set; }
        public int NeedToAddLibrary1 { get; set; }
        public string Remarks { get; set; }
    }
    public class IFieldName
    {
        public string FieldName { get; set; }
    }
    public class IChkName
    {
        public bool IsSelected { get; set; }
        public string ChkDescription { get; set; }
        public int ParentID { get; set; }
    }
}
