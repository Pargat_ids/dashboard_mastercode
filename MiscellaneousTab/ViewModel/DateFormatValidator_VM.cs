﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public List<IDelimiter> listDelimiterDateFormat { get; set; }
        private IDelimiter _SelectedDelimiterDateFormat { get; set; }
        public IDelimiter SelectedDelimiterDateFormat
        {
            get { return _SelectedDelimiterDateFormat; }
            set
            {
                _SelectedDelimiterDateFormat = value;
                NotifyPropertyChanged("SelectedDelimiterDateFormat");
            }
        }
        private string TableNameDateFormat { get; set; }
        public List<Library_DateFormat> listDateType { get; set; }
        //public StandardDataGrid_VM listDateFormat { get; set; }
        private Library_DateFormat _SelectedDateType { get; set; }
        public ICommand CheckDateFormat { get; set; }
        private string FieldNameDateFormat { get; set; }
        public string Labeldescription { get; set; }
        public Visibility uploadDateFormat { get; set; } = Visibility.Collapsed;
        public Library_DateFormat SelectedDateType
        {
            get => _SelectedDateType;
            set
            {
                if (Equals(_SelectedDateType, value))
                {
                    return;
                }

                _SelectedDateType = value;
                NotifyPropertyChanged("SelectedDateType");

                using (var context = new CRAModel())
                {
                    var res = context.Library_DateFormat.AsNoTracking().Where(x => x.DateFormatID == SelectedDateType.DateFormatID).Select(y => y.DateDescription).FirstOrDefault();
                    Labeldescription = res == null ? string.Empty : res;
                    NotifyPropertyChanged("Labeldescription");

                }

            }
        }
        private bool CheckDateFormatCanExecute(object o)
        {
            if (SelectedDateType != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private void CheckDateFormatExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameDateFormat = TabName;
            FieldNameDateFormat = string.Empty;
            if (TabName.ToString() == "Default")
            {
                TableNameDateFormat = "DATA";
                var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
                var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
                using (var context = new CRAModel())
                {
                    var mapFields = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.IsPhraseField == true && x.DateFormatID != null).ToList()
                                     join d2 in context.Library_FieldNames.AsNoTracking()
                                     on d1.FieldNameID equals d2.FieldNameID
                                     select new { d2.FieldName, d1.MultiValuePhraseDelimiterID }).Distinct().ToList();
                    if (mapFields.Count == 0)
                    {
                        _notifier.ShowError("No DateFormat field already Exist");
                        return;
                    }
                    else
                    {
                        var firstRow = mapFields[0];
                        FieldNameDateFormat = firstRow.FieldName;
                    }
                }
            }
            else
            {
                FieldNameDateFormat = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).FirstOrDefault();
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + FieldNameDateFormat + ", count(*) as cnt from [" + TableNameDateFormat + "] group by " + FieldNameDateFormat, CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(FieldNameDateFormat + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            uploadDateFormat = Visibility.Visible;
            NotifyPropertyChanged("uploadDateFormat");
            Task.Run(() =>
            {
                var dt = new DataTable();
                dt.Columns.Add(FieldNameDateFormat);
                dt.Columns.Add("IsValid", typeof(bool));
                dt.Columns.Add("Frequency", typeof(Int32));
                for (int i = 0; i < dtRaw.Rows.Count; i++)
                {
                    var strRow = dtRaw.Rows[i].Field<string>(FieldNameDateFormat);
                    if (string.IsNullOrEmpty(strRow) == false)
                    {
                        var arr = strRow.Split(SelectedDelimiterDateFormat.Delimiter).Where(y => string.IsNullOrEmpty(y) == false).Select(z=> z.Trim()).ToList();
                        bool formatChk = true;
                        for (int j = 0; j < arr.Count(); j++)
                        {
                            if (_objLibraryFunction_Service.IsValidDate(arr[j], SelectedDateType.DateFormat) == false)
                            {
                                formatChk = false;
                                break;
                            }
                        }
                        dt.Rows.Add(strRow, formatChk, dtRaw.Rows[i].Field<Int32>("cnt"));
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    //show invalid row above
                    dt = dt.AsEnumerable().OrderBy(x => x.Field<bool>("IsValid")).CopyToDataTable();
                }
                var result = new List<IDateFormat>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(new IDateFormat
                    {
                        FieldValue = dt.Rows[i].Field<string>(FieldNameDateFormat),
                        IsValid = dt.Rows[i].Field<bool>("IsValid") == false ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault(),
                        IsValid1 = dt.Rows[i].Field<bool>("IsValid"),
                        Frequency = dt.Rows[i].Field<Int32>("Frequency"),
                    });
                }
                listDateFormat = new List<IDateFormat>(result);
                NotifyPropertyChanged("comonDateFormat_VM");
                //listDateFormat = new StandardDataGrid_VM("Date Format Validator", dt);
                //NotifyPropertyChanged("listDateFormat");
                uploadDateFormat = Visibility.Collapsed;
                NotifyPropertyChanged("uploadDateFormat");
            });
        }
        private List<CommonDataGridColumn> GetListDateFormat()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldValue", ColBindingName = "FieldValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsValid", ColBindingName = "IsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Frequency", ColBindingName = "Frequency", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private List<IDateFormat> _listDateFormat { get; set; } = new List<IDateFormat>();
        public List<IDateFormat> listDateFormat
        {
            get { return _listDateFormat; }
            set
            {
                if (_listDateFormat != value)
                {
                    _listDateFormat = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IDateFormat>>>(new PropertyChangedMessage<List<IDateFormat>>(null, _listDateFormat, "Default List"));
                }
            }
        }
    }
    public class IDateFormat
    {
        public string FieldValue { get; set; }
        public bool IsValid1 { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsValid { get; set; }
        public Int32 Frequency { get; set; }
    }
}
