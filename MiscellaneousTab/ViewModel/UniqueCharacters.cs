﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;


namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public int SelectedTabIndex { get; set; }
        public ICommand BtnSameAccessFileUniqueChar { get; set; }
        public Visibility LoaderUniqueCharacters { get; set; } = Visibility.Collapsed;
        public ICommand ShowUniqueCharacters { get; set; }
        public ICommand RemInvCharacters { get; set; }
        //public StandardDataGrid_VM listUniqueCharacters { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();

        private List<IUniqueCharacter> _lisUniqueChr { get; set; } = new List<IUniqueCharacter>();
        public List<IUniqueCharacter> lisUniqueChr
        {
            get { return _lisUniqueChr; }
            set
            {
                if (_lisUniqueChr != value)
                {
                    _lisUniqueChr = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IUniqueCharacter>>>(new PropertyChangedMessage<List<IUniqueCharacter>>(null, _lisUniqueChr, "Default List"));
                }
            }
        }

        public IUniqueCharacter SelectedUniqueChrProperty { get; set; }
        private void NotifyMeUniqueChr(PropertyChangedMessage<IUniqueCharacter> obj)
        {
            SelectedUniqueChrProperty = obj.NewValue;
        }
        private void CommandButtonUniqueChrExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "AddEditName")
            {
                if (CommonFilepath == string.Empty)
                {
                    _notifier.ShowError("First choose access file to continue");
                    return;
                }
                if (TabName == null)
                {
                    _notifier.ShowError("first choose the TableName to continue");
                    return;
                }
                var tableName = TabName;
                List<string> combineFieldNames = new List<string>();
                if (TabName.ToString() == "Default")
                {
                    tableName = "DATA";

                }
                var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tableName + "]", CommonFilepath);
                if (dtRawChk == null || dtRawChk.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
                if (TabName.ToString() == "Default")
                {
                    combineFieldNames = (from DataColumn x in dtRawChk.Columns
                                         select x.ColumnName).ToList();
                }
                else
                {
                    combineFieldNames = listFields.Where(x => x.IsSelected).Select(y => y.FieldName).ToList();
                }
                var selCh = SelectedUniqueChrProperty.Character == "'" ? "''" : SelectedUniqueChrProperty.Character;
                var selCh1 = SelectedUniqueChrProperty.Character == "'" ? "''" : SelectedUniqueChrProperty.Character;

                if (selCh1.ToUpper() == "CARRIAGE RETURN")
                {
                    selCh1 = "\r\n";
                    selCh = "CR";
                }
                if (selCh1.ToUpper() == "VERTICAL TAB")
                {
                    selCh1 = "\v";
                    selCh = "VT";
                }
                if (selCh1.ToUpper() == "HORIZONTAL TAB")
                {
                    selCh1 = "\t";
                    selCh = "HT";
                }
                if (selCh1.ToUpper() == "DOUBLE SPACE")
                {
                    selCh1 = "  ";
                    selCh = "DS";
                }
                if (selCh1.ToUpper() == "LEADING SPACE" || selCh1.ToUpper() == "TRAILING SPACE")
                {
                    selCh1 = " ";
                    selCh = "LTS";
                }
                if (selCh1.ToUpper() == "NL LINE FEED, NEW LINE" || selCh1.ToUpper().Contains("LINE FEED"))
                {
                    selCh1 = "\n";
                    selCh = "NL";
                }
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnTable("UPDATE [" + tableName + "] Set TMP_RN = null ", CommonFilepath);
                for (int i = 0; i < combineFieldNames.Count; i++)
                {
                    if (selCh != "LTS" && selCh != "DS")
                    {
                        objCommon.DbaseQueryReturnTable("update [" + tableName + "]  set tmp_rn = '" + selCh + "' where InStr(1,[" + combineFieldNames[i] + "],'" + selCh1 + "', 0) > 0 ", CommonFilepath);
                    }
                    else
                    {
                        if (selCh == "LTS")
                        {
                            objCommon.DbaseQueryReturnTable("update [" + tableName + "]  set tmp_rn = '" + selCh + "' where ltrim(rtrim( " +  combineFieldNames[i] + ")) <> " + combineFieldNames[i], CommonFilepath);
                        }
                        if (selCh == "DS")
                        {
                            objCommon.DbaseQueryReturnTable("update [" + tableName + "]  set tmp_rn = '" + selCh + "' where InStr(1,[" + combineFieldNames[i] + "],'  ', 0) > 0", CommonFilepath);
                        }

                    }
                }

                _notifier.ShowSuccess("TMP_RN Flag updated successfully");
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            }
        }

        private void RemInvCharactersExecute(object o)
        {
            SelectedTabIndex = 1;
            NotifyPropertyChanged("SelectedTabIndex");
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                RemoveWhiteCharactersExecute(null);
            });
        }
        private void ShowUniqueCharactersExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tableName + "]", CommonFilepath);
            if (dtRawChk == null || dtRawChk.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            LoaderUniqueCharacters = Visibility.Visible;
            NotifyPropertyChanged("LoaderUniqueCharacters");

            Task.Run(() =>
            {

                DataTable dtFinal = new DataTable();
                List<IUniqueCharacter> lstMerge = new List<IUniqueCharacter>();
                if (TabName.ToString() == "Default")
                {
                    var dtTrans = objCommon.DbaseQueryReturnTable("select * from [" + tableName + "]", CommonFilepath);
                    var dtNotes = objCommon.DbaseQueryReturnTable("select * from Notes", CommonFilepath);
                    var dtDDic = objCommon.DbaseQueryReturnTable("select FIELD_NAME,HEAD_CD,EXPLAIN,Phrase from DATA_DICTIONARY", CommonFilepath);
                    var dtLDic = objCommon.DbaseQueryReturnTable("select * from LIST_DICTIONARY", CommonFilepath);
                    dtFinal = rowIdentifier.GetCharacterDetails(dtTrans, dtNotes, dtDDic, dtLDic, 4, tableName);

                    // get DoubleSpace, LeadingSpace & Trailing Space
                    lstMerge.AddRange(GetSpaces(dtTrans, tableName));
                    lstMerge.AddRange(GetSpaces(dtNotes, "Notes"));
                    lstMerge.AddRange(GetSpaces(dtDDic, "DATA_DICTIONARY"));
                    lstMerge.AddRange(GetSpaces(dtLDic, "LIST_DICTIONARY"));
                }
                else
                {
                    var selectedFields = string.Join(",", listFields.Where(x => x.IsSelected).Select(y => y.FieldName).ToList());
                    var dummyDt = new DataTable();
                    var dtTrans = objCommon.DbaseQueryReturnTable("select " + selectedFields + " from [" + tableName + "]", CommonFilepath);
                    dtFinal = rowIdentifier.GetCharacterDetails(dtTrans, dummyDt, dummyDt, dummyDt, 0, tableName);
                    lstMerge.AddRange(GetSpaces(dtTrans, tableName));
                }

                List<IUniqueCharacter> convertList = new List<IUniqueCharacter>();
                for (int i = 0; i < dtFinal.Rows.Count; i++)
                {
                    convertList.Add(new IUniqueCharacter
                    {
                        Character = dtFinal.Rows[i].Field<string>("Character"),
                        DecimalValue = dtFinal.Rows[i].Field<int?>("DecimalValue"),
                        OctaDecimalValue = dtFinal.Rows[i].Field<int?>("OctaDecimalValue"),
                        HexValue = dtFinal.Rows[i].Field<string>("HexValue"),
                        Frequency = dtFinal.Rows[i].Field<int?>("Frequency"),
                        IsAscii = dtFinal.Rows[i].Field<bool>("IsAscii") == true ? Engine._listCheckboxType.Where(x => x.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(x => x.Value == false).FirstOrDefault(),
                        Invalid_Character = dtFinal.Rows[i].Field<bool>("Invalid_Character") == true ? Engine._listCheckboxType.Where(x => x.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(x => x.Value == false).FirstOrDefault(),
                        NeedToAddLibraryInvalid = dtFinal.Rows[i].Field<bool>("Invalid_Character"),
                        FieldName = dtFinal.Rows[i].Field<string>("FieldName"),
                        Table = dtFinal.Rows[i].Field<string>("Table"),
                    });
                }


                var dtAccess = (from d1 in lstMerge
                                group new { d1.FieldName, d1.Table } by new { d1.Character } into g
                                select new IUniqueCharacter
                                {
                                    Character = g.Key.Character,
                                    Invalid_Character = Engine._listCheckboxType.Where(x => x.Value == true).FirstOrDefault(),
                                    NeedToAddLibraryInvalid = true,
                                    Frequency = g.Count(),
                                    FieldName = string.Join(",", g.Select(y => y.FieldName).Distinct().ToList()),
                                    Table = string.Join(",", g.Select(y => y.Table).Distinct().ToList()),
                                }).ToList();

                convertList.AddRange(dtAccess);
                convertList = convertList.OrderByDescending(x => x.NeedToAddLibraryInvalid).ToList();

                lisUniqueChr = new List<IUniqueCharacter>(convertList);
                NotifyPropertyChanged("comonUniqueChr_VM");
            });

            LoaderUniqueCharacters = Visibility.Collapsed;
            NotifyPropertyChanged("LoaderUniqueCharacters");
        }

        private List<IUniqueCharacter> GetSpaces(DataTable dtTrans, string TableName)
        {
            List<IUniqueCharacter> lstMerge = new List<IUniqueCharacter>();
            foreach (DataRow dr in dtTrans.Rows) // trim string data
            {
                foreach (DataColumn dc in dtTrans.Columns)
                {
                    object ob = dr[dc];
                    if (!Convert.IsDBNull(ob) && ob != null)
                    {
                        if (ob.ToString().Contains("  "))
                        {
                            lstMerge.Add(new IUniqueCharacter
                            {
                                Character = "Double Space",
                                FieldName = dc.ColumnName,
                                Table = TableName,
                            });

                        }
                        if (ob.ToString().TrimStart() != ob.ToString())
                        {
                            lstMerge.Add(new IUniqueCharacter
                            {
                                Character = "Leading Space",
                                FieldName = dc.ColumnName,
                                Table = TableName,
                            });

                        }
                        if (ob.ToString().TrimEnd() != ob.ToString())
                        {
                            lstMerge.Add(new IUniqueCharacter
                            {
                                Character = "Trailing Space",
                                Invalid_Character = Engine._listCheckboxType.Where(x => x.Value == true).FirstOrDefault(),
                                NeedToAddLibraryInvalid = true,
                                FieldName = dc.ColumnName,
                                Table = TableName,
                            });

                        }

                    }

                }
            }
            return lstMerge;
        }
        private List<CommonDataGridColumn> GetListGridColumnUniqueCharacters()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Character", ColBindingName = "Character", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "HexValue", ColBindingName = "HexValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DecimalValue", ColBindingName = "DecimalValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OctaDecimalValue", ColBindingName = "OctaDecimalValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Table", ColBindingName = "Table", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Frequency", ColBindingName = "Frequency", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsAscii", ColBindingName = "IsAscii", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Invalid_Character", ColBindingName = "Invalid_Character", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Update Flag in Access", ColBindingName = "Field_Value", ColType = "Button", ColumnWidth = "150", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditName" });
            return listColumnQsidDetail;
        }
        public void BtnSameAccessFileUniqueCharExecute(Object o)
        {

            var dtNew = objCommon.ConvertToDataTable<IExportUniqueCharacter>(lisUniqueChr.Select(x => new IExportUniqueCharacter
            {
                CharacterValue = x.Character,
                DecimalValue = x.DecimalValue.ToString(),
                FieldName = x.FieldName,
                Frequency = x.Frequency.ToString(),
                HexValue = x.HexValue,
                InvalidCharacter = x.Invalid_Character.Value == true ? "True" : "False",
                OctaDecimalValue = x.OctaDecimalValue.ToString(),
                TableName = x.Table,
                IsAscii = x.IsAscii.Value == true ? "True" : "False",
            }).ToList());
            var NewTableName = "UniqueCharacters_" + TabName + (listFields.Count(y => y.IsSelected) > 0 ? "_" : "") +   string.Join("_", listFields.Where(y=> y.IsSelected).Select(x => x.FieldName).Distinct().Take(2).ToList());
            CreateTabelINSameAccessFile(NewTableName, dtNew);
            _notifier.ShowSuccess(NewTableName + " Table Created successfully");
        }


    }
    public class IUniqueCharacter
    {
        public string Character { get; set; }
        public string HexValue { get; set; }
        public int? DecimalValue { get; set; }
        public int? OctaDecimalValue { get; set; }
        public string FieldName { get; set; }
        public string Table { get; set; }
        public int? Frequency { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsAscii { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel Invalid_Character { get; set; }
        public bool NeedToAddLibraryInvalid { get; set; }
    }
    public class IExportUniqueCharacter
    {
        public string CharacterValue { get; set; }
        public string HexValue { get; set; }
        public string DecimalValue { get; set; }
        public string OctaDecimalValue { get; set; }
        public string FieldName { get; set; }
        public string TableName { get; set; }
        public string Frequency { get; set; }
        public string IsAscii { get; set; }
        public string InvalidCharacter { get; set; }
    }

}
