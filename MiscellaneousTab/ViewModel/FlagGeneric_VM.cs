﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public ICommand BtnShowFlagGeneric { get; set; }
        public ICommand BtnUpdateFlagGeneric { get; set; }
        private DataTable dtfinal = new DataTable();

        public Visibility uploadFlagGeneric { get; set; } = Visibility.Collapsed;
        public StandardDataGrid_VM ListFlagGeneric { get; set; }

        private void BtnShowFlagGenericExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var TableNameFlag = TabName;
            var combineField = string.Empty;
            var FieldNameFlag = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameFlag = "DATA";
                FieldNameFlag.Add("CAS");
                FieldNameFlag.Add("Editorial");
            }
            else
            {
                FieldNameFlag = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
            }
            if (!FieldNameFlag.Contains("RN"))
            {
                FieldNameFlag.Add("RN");
            }
            combineField = string.Join(",", FieldNameFlag);
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameFlag + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate RN in this file, first fix them");
                return;
            }
            uploadFlagGeneric = Visibility.Visible;
            NotifyPropertyChanged("uploadFlagGeneric");
            Task.Run(() =>
            {
                CalculateFlagGeneric(dtRaw);
            });
        }

        private void BtnUpdateFlagGenericExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var TableNameFlag = TabName;
            var combineField = string.Empty;
            var FieldNameFlag = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameFlag = "DATA";
                FieldNameFlag.Add("CAS");
                FieldNameFlag.Add("Editorial");
            }
            else
            {
                FieldNameFlag = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
            }
            if (!FieldNameFlag.Contains("RN"))
            {
                FieldNameFlag.Add("RN");
            }
            combineField = string.Join(",", FieldNameFlag);
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameFlag + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate RN in this file, first fix them");
                return;
            }
            uploadFlagGeneric = Visibility.Visible;
            NotifyPropertyChanged("uploadFlagGeneric");

            Task.Run(() =>
            {
                CalculateFlagGeneric(dtRaw);

                var queryString = string.Empty;
                var updateQuery = string.Empty;
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                listColParam.Add("@RN");
                columnList.Add("RN");
                for (int i = 0; i < dtfinal.Columns.Count; i++)
                {
                    if (dtfinal.Columns[i].ColumnName != "RN" && dtfinal.Columns[i].ColumnName.ToUpper() != "IDENT")
                    {
                        queryString += "[" + dtfinal.Columns[i].ColumnName + "] LongText,";
                        listColParam.Add("@" + dtfinal.Columns[i].ColumnName);
                        columnList.Add(dtfinal.Columns[i].ColumnName);
                    }
                }
                queryString = queryString.TrimEnd(',');
                updateQuery = updateQuery.TrimEnd(',');
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
                objCommon.DbaseQueryReturnString("Alter Table " + TableNameFlag + " Alter Column TMP_RN Text(254)", CommonFilepath);
                objCommon.DbaseQueryReturnString("Update  DATA_DICTIONARY set DATA_SIZE = 254 where FIELD_NAME ='TMP_RN'", CommonFilepath);

                _objAccessVersion_Service.ExportAccessData(dtfinal, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnString("Update  [" + TableNameFlag + "] set Tmp_RN = null", CommonFilepath);
                objCommon.DbaseQueryReturnString("update [" + TableNameFlag + "] a inner join TMPDh b on a.RN = b.RN set Tmp_RN = b.New_TMP_RN", CommonFilepath);

                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                uploadFlagGeneric = Visibility.Hidden;
                NotifyPropertyChanged("uploadFlagGeneric");
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowSuccess("Access file updated successfully");
                });
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            });
        }

        private void CalculateFlagGeneric(DataTable dt)
        {
            dtfinal = new DataTable();
            dtfinal.Columns.Add("RN", typeof(Int32));
            dtfinal.Columns.Add("New_TMP_RN", typeof(string));
            using (var context = new CRAModel())
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName.ToUpper() != "RN")
                    {
                        if (!dtfinal.Columns.Contains(dt.Columns[i].ColumnName))
                        {
                            dtfinal.Columns.Add(dt.Columns[i].ColumnName, typeof(string));
                        }
                        var allColumnsCas = dt.AsEnumerable().Select(y => new { RN = y.Field<Int32>("RN"), Value = y.Field<string>(dt.Columns[i].ColumnName) }).Distinct().ToList();
                        var joinWithLibraryCas = (from d1 in allColumnsCas
                                                  join d2 in context.Library_CAS.AsNoTracking()
                                                  on d1.Value equals d2.CAS
                                                  select new { d1.RN, d2.CASID, d2.CAS }).Distinct().ToList();
                        var joinWithMapTable = (from d1 in joinWithLibraryCas
                                                join d2 in context.Map_Generics_Combined_Hydrates_AlternateCAS_etc.AsNoTracking()
                                                on d1.CASID equals d2.ParentCasID
                                                select new
                                                {
                                                    d1.RN,
                                                    d1.CAS,
                                                    d1.CASID,
                                                    d2.Type,
                                                    d2.ChildCasID,
                                                }).Distinct().ToList();

                        var groupBYRN = (from d1 in joinWithMapTable
                                         group new { d1.ChildCasID } by new { d1.RN, d1.CAS, d1.Type } into g
                                         select new
                                         {
                                             RN = g.Key.RN,
                                             CAS = g.Key.CAS,
                                             tmprn =  g.Key.Type == "Alternate CAS" ? "AC#" + g.Count() : g.Key.Type.Substring(0, 1) + "#" + g.Count(),
                                         }).Distinct().ToList();
                        var groupAgain = (from d1 in groupBYRN
                                          group d1.tmprn by new { d1.RN, d1.CAS } into g
                                          select new
                                          {
                                              RN = g.Key.RN,
                                              CAS = g.Key.CAS,
                                              TmpRN = dt.Columns[i].ColumnName.Substring(0, 1) + ": " + string.Join(" ", g.ToList()),
                                          }).ToList();
                        for (int j = 0; j < groupAgain.Count; j++)
                        {

                            var dtRow = dtfinal.NewRow();
                            var chkExt = dtfinal.AsEnumerable().Where(x => x.Field<Int32>("RN") == Convert.ToInt32(groupAgain[j].RN)).FirstOrDefault();
                            if (chkExt == null)
                            {
                                dtRow["New_TMP_RN"] = groupAgain[j].TmpRN;
                            }
                            else
                            {
                                dtRow = chkExt;
                                dtRow["New_TMP_RN"] = dtRow.Field<string>("New_TMP_RN") + "; " + groupAgain[j].TmpRN;
                            }
                            dtRow[dt.Columns[i].ColumnName] = groupAgain[j].CAS;
                            dtRow["RN"] = groupAgain[j].RN;
                            dtfinal.Rows.Add(dtRow.ItemArray);
                        }
                    }
                }
                if (dtfinal.Rows.Count > 0)
                {
                    dtfinal = dtfinal.AsEnumerable().Distinct(System.Data.DataRowComparer.Default).CopyToDataTable();
                }
                ListFlagGeneric = new StandardDataGrid_VM("Flag Generic", dtfinal);
                NotifyPropertyChanged("ListFlagGeneric");
                uploadFlagGeneric = Visibility.Hidden;
                NotifyPropertyChanged("uploadFlagGeneric");
            }
        }
    }
}
