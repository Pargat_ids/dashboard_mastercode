﻿using CRA_DataAccess.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public static List<IComboValue> _lstSingleValueConsiderAs { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstConsiderNullValue { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstThresholdType { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstThresholdCategoryinCombo { get; set; } = new List<IComboValue>();
        public static List<IComboValue> _lstThresholdOperatorinCombo { get; set; } = new List<IComboValue>();
        public static List<ILibraryUnits> _lstLibraryUnits { get; set; } = new List<ILibraryUnits>();
        public static List<ILibraryOperator> _lstLibraryOperators { get; set; } = new List<ILibraryOperator>();
        public static List<ILibraryDelimiters> _lstLibraryDelimiters { get; set; } = new List<ILibraryDelimiters>();
        public static List<ILibraryCategories> _lstLibraryThresholdCategories { get; set; } = new List<ILibraryCategories>();
        public ObservableCollection<GenericCriteria_VM> listGenericCriteria { get; set; } = new ObservableCollection<GenericCriteria_VM>();
        public static List<(string, string)> CombineFieldName { get; set; } = new List<(string, string)>();
        public string DefaultLableThreshold { get; set; }
        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public Visibility listAllColVisible { get; set; } = Visibility.Collapsed;
        public Visibility uploadThresholdError { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<IThresholdError> comonThresholdError_VM { get; set; }
        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            if (listAllColVisible == Visibility.Collapsed)
            {
                listAllColVisible = Visibility.Visible;
            }
            else
            {
                listAllColVisible = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllColVisible");
        }
        private void MapAdditionalInfoFields()
        {
            Task.Run(() =>
            {
                _lstLibraryThresholdCategories.AddRange(_objLibraryFunction_Service.AllLibraryThresholdCategories().ToList());
                _lstLibraryDelimiters.AddRange(_objLibraryFunction_Service.AllLibraryDelimiters().ToList());
                _lstThresholdType.AddRange(_objLibraryFunction_Service.AddThresholdTypeinCombo().ToList());
                _lstThresholdCategoryinCombo.AddRange(_objLibraryFunction_Service.AddThresholdCategoryinCombo().ToList());
                _lstThresholdOperatorinCombo.AddRange(_objLibraryFunction_Service.AddThresholdOperatorinCombo().ToList());
                _lstSingleValueConsiderAs.AddRange(_objLibraryFunction_Service.AddSingleValueConsiderinComboBox().ToList());
                _lstConsiderNullValue.AddRange(_objLibraryFunction_Service.AddConsiderNullValueinComboBox().ToList());
                _lstLibraryUnits.AddRange(_objLibraryFunction_Service.AllLibraryUnits().ToList());
                _lstLibraryOperators.AddRange(_objLibraryFunction_Service.AllLibraryOperators().ToList());
            });
        }
        public static string columnNameFound { get; set; }
        private void NotifyRefreshGrid(PropertyChangedMessage<string> obj)
        {
            if (obj != null && obj.NewValue == "abc")
            {
                columnNameFound = string.Empty;
                var splVal = obj.PropertyName.Split(';');
                if (splVal.Count() > 2)
                {
                    var visaVerss = splVal[0].ToString() == "Max Value" ? "Min Value" : "Max Value";
                    columnNameFound = splVal[3];
                    for (int i = 0; i < listGenericCriteria.Count; i++)
                    {
                        if ((splVal[0] == "Max Value" || splVal[0] == "Min Value") && listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == visaVerss && listGenericCriteria[i].SelectedCatgory != null && listGenericCriteria[i].SelectedCatgory.Category == splVal[1].ToString() && listGenericCriteria[i].SelectedUnit != null && listGenericCriteria[i].SelectedUnit.Unit == splVal[2].ToString())
                        {
                            columnNameFound = splVal[0] == "Min Value" ? listGenericCriteria[i].ColumnName : splVal[3].ToString();
                            if (splVal[0] == "Max Value")
                            {
                                if (CombineFieldName.Count(x => x.Item1 == columnNameFound + "_Unit") == 0)
                                {
                                    CombineFieldName.Add((columnNameFound + "_Unit", "VirtualUnit"));
                                }
                                var resultUnit2 = CasValidity_VM.CombineFieldName.Where(x => x.Item1 == columnNameFound + "_Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                                listGenericCriteria[i].MaxFieldName = new List<IComboValue>(resultUnit2);
                                listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == columnNameFound + "_Unit").FirstOrDefault();
                            }
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < listGenericCriteria.Count; i++)
                    {
                        if (listGenericCriteria[i].SelectedMaxFieldName != null)
                        {
                            if (listGenericCriteria[i].SelectedMaxFieldName.CboValue == splVal[0].ToString() + "_Unit" && listGenericCriteria[i].ColumnName != splVal[0].ToString())
                            {
                                if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_Unit") == 0)
                                {
                                    CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_Unit", "VirtualUnit"));
                                }
                                var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                                listGenericCriteria[i].MaxFieldName = new List<IComboValue>(resultUnit2);
                                var result = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_Unit").FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedMaxFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddUnit")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdUnitType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdUnitType.CboValue == "Unit FieldName")
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Unit")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Unit").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Unit"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Unit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].MaxFieldName = new List<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedMaxFieldName != null)
                            {
                                var result = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedMaxFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedMaxFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMax")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Max Value Operator")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Max Value Operator").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Max Value Operator"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Max Value Operator").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].OperatorFieldName = new List<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedOperatorFieldName != null)
                            {
                                var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedOperatorFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedOperatorFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMin")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null)
                    {
                        if (listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                        {
                            for (int ii = 0; ii < listGenericCriteria.Count; ii++)
                            {
                                if (listGenericCriteria[ii].SelectedThresholdType != null)
                                {
                                    if (listGenericCriteria[ii].SelectedThresholdType.CboValue == "Min Value Operator")
                                    {
                                        if (CombineFieldName.Where(x => x.Item1.ToUpper() == listGenericCriteria[ii].ColumnName.ToUpper() && x.Item2 == "Min Value Operator").Count() == 0)
                                        {
                                            CombineFieldName.Add((listGenericCriteria[ii].ColumnName, "Min Value Operator"));
                                        }
                                    }
                                }
                            }
                            var resultMin = CombineFieldName.Where(x => x.Item2 == "Min Value Operator").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            listGenericCriteria[i].OperatorFieldName = new List<IComboValue>(resultMin);
                            if (listGenericCriteria[i].SelectedOperatorFieldName != null)
                            {
                                var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].SelectedOperatorFieldName.CboValue).FirstOrDefault();
                                if (result != null)
                                {
                                    listGenericCriteria[i].SelectedOperatorFieldName = result;
                                }
                            }
                        }
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMaxManual")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                    {
                        //if (listGenericCriteria[i].SelectedOperatorFieldName.CboValue == obj.PropertyName + "_MaxOp" && listGenericCriteria[i].ColumnName != obj.PropertyName)
                        //{
                        if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MaxOp") == 0)
                        {
                            CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_MaxOp", "VirtualOperator"));
                        }
                        var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MaxOp").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                        listGenericCriteria[i].OperatorFieldName = new List<IComboValue>(resultUnit2);
                        var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_MaxOp").FirstOrDefault();
                        if (result != null)
                        {
                            listGenericCriteria[i].SelectedOperatorFieldName = result;
                        }
                        //}
                    }
                }
            }
            if (obj != null && obj.NewValue == "AddMinManual")
            {
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    if (listGenericCriteria[i].SelectedThresholdType != null && listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" && listGenericCriteria[i].ColumnName == obj.PropertyName)
                    {
                        //if (listGenericCriteria[i].SelectedOperatorFieldName.CboValue == obj.PropertyName + "_MinOp" && listGenericCriteria[i].ColumnName != obj.PropertyName)
                        //{
                        if (CombineFieldName.Count(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MinOp") == 0)
                        {
                            CombineFieldName.Add((listGenericCriteria[i].ColumnName + "_MinOp", "VirtualOperator"));
                        }
                        var resultUnit2 = CombineFieldName.Where(x => x.Item1 == listGenericCriteria[i].ColumnName + "_MinOp").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                        listGenericCriteria[i].OperatorFieldName = new List<IComboValue>(resultUnit2);
                        var result = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == listGenericCriteria[i].ColumnName + "_MinOp").FirstOrDefault();
                        if (result != null)
                        {
                            listGenericCriteria[i].SelectedOperatorFieldName = result;
                        }
                        //}
                    }
                }
            }
        }
        public ICommand BtnSaveAddInfo { get; set; }
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        public ICommand ClearAllFilter { get; set; }
        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        private bool IsNavigateProgress { get; set; }
        public List<GenericCriteria_VM> myList { get; set; } = new List<GenericCriteria_VM>();
        private void Navigate(int mode)
        {
            IsNavigateProgress = true;
            lblContent = "";
            lblTotRecords = "";
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("lblContent");
            var result = _objLibraryFunction_Service.NavigateList<GenericCriteria_VM>(mode, ref pageIndex, numberOfRecPerPage, myList, selectedItemCombo == null ? "25" : selectedItemCombo.text, lblContent, lblTotRecords, listGenericCriteria);
            lblContent = result.Item1;
            lblTotRecords = result.Item2;
            listGenericCriteria = result.Item3;
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("listGenericCriteria");
            NotifyPropertyChanged("lblContent");
            IsNavigateProgress = false;
        }
        public string lblTotRecords { get; set; }
        public string lblContent { get; set; }
        private List<selectedListItemCombo> AddList()
        {
            List<selectedListItemCombo> listSelectedCombo =

                new List<selectedListItemCombo>() {
                new selectedListItemCombo() { id = 25, text = "25" },
                new selectedListItemCombo() { id = 50, text = "50" },
                new selectedListItemCombo() { id = 100, text = "100" },
                new selectedListItemCombo() { id = 500, text = "All" },
            };
            return listSelectedCombo;

        }
        int pageIndex = 1;
        private int numberOfRecPerPage = 25;
        private enum PagingMode { First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public List<selectedListItemCombo> ListComboxItem { get; set; } = new List<selectedListItemCombo>();
        public selectedListItemCombo _selectedItemCombo { get; set; }
        public selectedListItemCombo selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    numberOfRecPerPage = (int)value.id;
                    NotifyPropertyChanged("numberOfRecPerPage");
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }
        public Visibility ColumnNameVisible { get; set; }
        public Visibility HeaderVisible { get; set; } = Visibility.Hidden;
        public Visibility ExplanationVisible { get; set; } = Visibility.Hidden;
        public Visibility Is_InternalVisible { get; set; }
        public Visibility Field_TypeVisible { get; set; }
        public Visibility Threshold_TypeVisible { get; set; }
        public Visibility Unit_TypeVisible { get; set; }
        public Visibility Operator_TypeVisible { get; set; }
        public Visibility Manual_UnitVisible { get; set; }
        public Visibility Manual_OperatorVisible { get; set; }
        public Visibility Min_Max_Unit_FieldVisible { get; set; }
        public Visibility Operator_FieldVisible { get; set; }
        public Visibility CategoryVisible { get; set; }
        public Visibility LanguageVisible { get; set; }
        public Visibility DelimiterVisible { get; set; }
        public Visibility DateFormatVisible { get; set; }
        public Visibility Allowed_True_FalseVisible { get; set; }
        public Visibility Consider_Single_ValueVisible { get; set; }
        public Visibility Consider_Null_ValueVisible { get; set; }
        public Visibility ValueField_Contain_OperatorVisible { get; set; }
        public void BtnSaveAddInfoExecute(object o)
        {
            selectedItemCombo = ListComboxItem.Where(x => x.text == "All").FirstOrDefault();
            NotifyPropertyChanged("selectedItemCombo");
            string msgFlag = string.Empty;
            string colNames = string.Empty;
            for (int i = 0; i < listGenericCriteria.Count(); i++)
            {
                if (listGenericCriteria[i].SelectedThresholdType == null)
                {
                    msgFlag += listGenericCriteria[i].ColumnName + " - need to select only one out of Minval/MaxVal/Range/Unit/Operator" + Environment.NewLine;
                }
                if (listGenericCriteria[i].SelectedThresholdType != null)
                {
                    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Unit" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
                    {
                        colNames += listGenericCriteria[i].ColumnName + ",";
                    }
                    if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range") && listGenericCriteria[i].SelectedThresholdUnitType != null && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != "No Unit" && listGenericCriteria[i].SelectedThresholdUnitType.CboValue != "Unit Delimitor")
                    {
                        int chkCountNew = listGenericCriteria[i].SelectedUnit == null ? 0 : 1;
                        chkCountNew += listGenericCriteria[i].SelectedMaxFieldName == null ? 0 : 1;
                        if (chkCountNew == 0)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - need to select Unit ID/UnitFieldName" + Environment.NewLine;
                        }
                        if (listGenericCriteria[i].SelectedThresholdOperatorType != null && listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Manual Operator")
                        {
                            if (listGenericCriteria[i].SelectedOperator == null || listGenericCriteria[i].SelectedOperator.Operator == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to select operator" + Environment.NewLine;
                            }
                        }
                        if (listGenericCriteria[i].SelectedThresholdOperatorType != null && listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName")
                        {
                            if (listGenericCriteria[i].SelectedOperatorFieldName == null || listGenericCriteria[i].SelectedOperatorFieldName.CboValue == null)
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to select Operator Field" + Environment.NewLine;
                            }
                        }
                    }
                    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range")
                    {
                        if (listGenericCriteria[i].SelectedDelimiter == null)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - need to choose Delimiter " + Environment.NewLine;
                        }
                        else
                        {
                            if (listGenericCriteria[i].SelectedDelimiter.Delimiter == "--Select--")
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to choose Delimiter" + Environment.NewLine;
                            }
                        }

                        if (listGenericCriteria[i].SelectedConsiderSingleValues == null)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - need to choose consider single value as" + Environment.NewLine;
                        }
                        else
                        {
                            if (listGenericCriteria[i].SelectedConsiderSingleValues.CboValue == "--Select--")
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - need to choose consider single value as" + Environment.NewLine;
                            }
                        }
                    }

                    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value")
                    {
                        if (listGenericCriteria[i].SelectedConsiderNullValues == null)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - Choose ConsiderNullValue As, its mandatory" + Environment.NewLine;
                        }
                        else
                        {
                            if (listGenericCriteria[i].SelectedConsiderNullValues.CboValue == "--Select--")
                            {
                                msgFlag += listGenericCriteria[i].ColumnName + " - Choose ConsiderNullValue As, its mandatory" + Environment.NewLine;
                            }
                        }
                    }
                    if ((listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Value Range") && listGenericCriteria[i].SelectedCatgory == null)
                    {
                        msgFlag += listGenericCriteria[i].ColumnName + " - should choose Threshold Category" + Environment.NewLine;
                    }

                    int assciatewithMaxMin = 0;
                    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Unit")
                    {
                        var unitColumnName = listGenericCriteria[i].ColumnName;
                        for (int j = 0; j < listGenericCriteria.Count(); j++)
                        {
                            if (listGenericCriteria[j].SelectedMaxFieldName != null && listGenericCriteria[j].SelectedMaxFieldName.CboValue == unitColumnName.Trim())
                            {
                                assciatewithMaxMin += 1;
                            }
                        }

                        if (assciatewithMaxMin == 0 )
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - Unit Field is not associate with any Min/Max/Value Range Field" + Environment.NewLine;
                        }
                        if (assciatewithMaxMin > 2)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - Unit Field associate more than 1" + Environment.NewLine;
                        }
                    }
                    assciatewithMaxMin = 0;
                    if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator" || listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
                    {
                        var unitColumnName = listGenericCriteria[i].ColumnName;
                        for (int j = 0; j < listGenericCriteria.Count(); j++)
                        {
                            if (listGenericCriteria[j].SelectedOperatorFieldName != null && listGenericCriteria[j].SelectedOperatorFieldName.CboValue == unitColumnName.Trim())
                            {
                                assciatewithMaxMin += 1;
                            }
                        }

                        if (assciatewithMaxMin == 0)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - Operator Field is not associate with any Min/Max Range Field" + Environment.NewLine;
                        }
                        if (assciatewithMaxMin > 1)
                        {
                            msgFlag += listGenericCriteria[i].ColumnName + " - Operator Field is associate more than 1" + Environment.NewLine;
                        }
                    }
                }
            }
            if (msgFlag != string.Empty)
            {
                System.Windows.Forms.MessageBox.Show("Following are the Errors" + Environment.NewLine + Environment.NewLine + msgFlag, "Error");
            }
            colNames = colNames.TrimEnd(',');
            var thresholdTable = string.Empty;
            var extraQuery = string.Empty;
            if (TabName == "Default")
            {
                thresholdTable = "DATA";

            }
            else
            {
                thresholdTable = SelectedTable.TableName;
            }
            if (TabName == "Default" || thresholdTable == "DATA")
            {
                extraQuery = " where omit is null or omit =''";
            }
            var autoCol = RetrieveAutoNumberColumn("[" + thresholdTable + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("Select " + autoCol + ", " + colNames + " from [" + thresholdTable + "]" + extraQuery, CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(autoCol + ", " + colNames + " Fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            Regex regex = new Regex(@"^[0-9.]*$");
            List<IThresholdError> lstError = new System.Collections.Generic.List<IThresholdError>();
            for (int i = 0; i < listGenericCriteria.Count(); i++)
            {
                var colName = listGenericCriteria[i].ColumnName.Trim();
                if (listGenericCriteria[i].SelectedThresholdType.CboValue.ToString() == "Min Value" || listGenericCriteria[i].SelectedThresholdType.CboValue.ToString() == "Max Value")
                {
                    var checkNonNumeric = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(colName) != null && !regex.IsMatch(x.Field<dynamic>(colName))).Select(x => new
                    {
                        AutoCol_ID = x.Field<Int32>(autoCol),
                        Field_Value = x.Field<dynamic>(colName)
                    }).Distinct().ToList();
                    checkNonNumeric = checkNonNumeric.Where(x => x.Field_Value != "GMP").ToList();
                    for (int j = 0; j < checkNonNumeric.Count; j++)
                    {
                        lstError.Add(new IThresholdError { AutoCol_ID = checkNonNumeric[j].AutoCol_ID, Field_Name = colName, Field_Value = checkNonNumeric[j].Field_Value, Error_Description = "Non-Numeric Value" });
                    }
                }
                if (listGenericCriteria[i].SelectedThresholdType.CboValue.ToString() == "Min Value")
                {
                    var catgory = listGenericCriteria[i].SelectedCatgory.Category;
                    var getMaxColName = listGenericCriteria.Where(x => x.SelectedThresholdType.CboValue == "Max Value" && x.SelectedCatgory.Category == catgory).Select(y => y.ColumnName).FirstOrDefault();
                    var dtValTemp = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(getMaxColName) != null && x.Field<dynamic>(colName) != null && regex.IsMatch(x.Field<dynamic>(colName)) && regex.IsMatch(x.Field<dynamic>(getMaxColName)));
                    var dtVal = dtValTemp.AsEnumerable().Where(x => Convert.ToDecimal(x.Field<dynamic>(colName)) > Convert.ToDecimal(x.Field<dynamic>(getMaxColName))).Select(x => new
                    {
                        AutoCol_ID = x.Field<Int32>(autoCol),
                        Field_Value = x.Field<dynamic>(colName),
                    }).Distinct().ToList();
                    for (int k = 0; k < dtVal.Count; k++)
                    {
                        lstError.Add(new IThresholdError { AutoCol_ID = dtVal[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal[k].Field_Value, Error_Description = "min value greater than max value" });
                    }
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null && listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName")
                    {
                        var OPCol = listGenericCriteria[i].SelectedOperatorFieldName.CboValue;
                        var minCol = listGenericCriteria[i].ColumnName;
                        var dtVal1 = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(OPCol) == null && x.Field<dynamic>(minCol) != null).Select(x => new
                        {
                            AutoCol_ID = x.Field<Int32>(autoCol),
                            Field_Value = x.Field<dynamic>(minCol)
                        }).Distinct().ToList();
                        for (int k = 0; k < dtVal1.Count; k++)
                        {
                            lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = minCol, Field_Value = dtVal1[k].Field_Value, Error_Description = "Min Value without Operator" });
                        }
                    }
                }

                if (listGenericCriteria[i].SelectedThresholdType.CboValue.ToString() == "Max Value")
                {
                    if (listGenericCriteria[i].SelectedThresholdOperatorType != null && listGenericCriteria[i].SelectedThresholdOperatorType.CboValue == "Operator FieldName")
                    {
                        var OPCol = listGenericCriteria[i].SelectedOperatorFieldName.CboValue;
                        var minCol = listGenericCriteria[i].ColumnName;
                        var dtVal1 = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(OPCol) == null && x.Field<dynamic>(minCol) != null).Select(x => new
                        {
                            AutoCol_ID = x.Field<Int32>(autoCol),
                            Field_Value = x.Field<dynamic>(minCol)
                        }).Distinct().ToList();
                        for (int k = 0; k < dtVal1.Count; k++)
                        {
                            lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = minCol, Field_Value = dtVal1[k].Field_Value, Error_Description = "Max Value without Operator" });
                        }
                    }
                }
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Unit")
                {
                    var getMaxMinColumnName = listGenericCriteria.Where(x => x.SelectedMaxFieldName != null && x.SelectedMaxFieldName.CboValue == colName && x.SelectedThresholdUnitType.CboValue == "Unit FieldName").ToList();
                    var maxCol = getMaxMinColumnName.Where(x => x.SelectedThresholdType.CboValue == "Max Value").Select(y => y.ColumnName).FirstOrDefault();
                    var minCol = getMaxMinColumnName.Where(x => x.SelectedThresholdType.CboValue == "Min Value").Select(y => y.ColumnName).FirstOrDefault();
                    if (minCol == string.Empty)
                    {
                        minCol = maxCol;
                    }
                    if (maxCol == string.Empty)
                    {
                        maxCol = minCol;
                    }
                    var dtVal = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(maxCol) == null && x.Field<dynamic>(minCol) == null && x.Field<dynamic>(colName) != null).Select(x => new
                    {
                        AutoCol_ID = x.Field<Int32>(autoCol),
                        Field_Value = x.Field<dynamic>(colName)
                    }).Distinct().ToList();
                    for (int k = 0; k < dtVal.Count; k++)
                    {
                        lstError.Add(new IThresholdError { AutoCol_ID = dtVal[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal[k].Field_Value, Error_Description = "units without value" });
                    }

                    string suffix = objCommon.DbaseQueryReturnString("select SUFFIX from DATA_Dictionary where FIELD_NAME='" + colName + "'", CommonFilepath);
                    DataTable allUnit = new DataTable();
                    string unt = string.Empty;
                    string[] suffixSplit;
                    if (string.IsNullOrWhiteSpace(suffix) == false)
                    {
                        suffix = suffix.Replace("<>", "<@>");
                        suffixSplit = suffix.Split('<');
                        var dtVal1 = dtRaw.AsEnumerable().Where(x => (x.Field<dynamic>(maxCol) != null || x.Field<dynamic>(minCol) != null) && x.Field<dynamic>(colName) == null).Select(x => new
                        {
                            AutoCol_ID = x.Field<Int32>(autoCol),
                            Field_Value = x.Field<dynamic>(colName) == null ? GetUnitfromDataDictionaryNewF(suffixSplit, string.Empty) : x.Field<dynamic>(colName),
                        }).Distinct().ToList();
                        dtVal1 = dtVal1.Where(x => x.Field_Value == null).ToList();
                        for (int k = 0; k < dtVal1.Count; k++)
                        {
                            lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal1[k].Field_Value, Error_Description = "values without unit" });
                        }
                    }
                    else
                    {
                        var dtVal1 = dtRaw.AsEnumerable().Where(x => (x.Field<dynamic>(maxCol) != null || x.Field<dynamic>(minCol) != null) && x.Field<dynamic>(colName) == null).Select(x => new
                        {
                            AutoCol_ID = x.Field<Int32>(autoCol),
                            Field_Value = x.Field<dynamic>(colName),
                        }).Distinct().ToList();
                        for (int k = 0; k < dtVal1.Count; k++)
                        {
                            lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal1[k].Field_Value, Error_Description = "values without unit" });
                        }
                    }

                }
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Min Value Operator")
                {
                    var dtVal1 = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(colName) == "<" || x.Field<dynamic>(colName) == "<=").Select(x => new
                    {
                        AutoCol_ID = x.Field<Int32>(autoCol),
                        Field_Value = x.Field<dynamic>(colName)
                    }).Distinct().ToList();
                    for (int k = 0; k < dtVal1.Count; k++)
                    {
                        lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal1[k].Field_Value, Error_Description = "</<= operator in MinValue Operator Field" });
                    }
                }
                if (listGenericCriteria[i].SelectedThresholdType.CboValue == "Max Value Operator")
                {
                    var dtVal1 = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>(colName) == ">" || x.Field<dynamic>(colName) == ">=").Select(x => new
                    {
                        AutoCol_ID = x.Field<Int32>(autoCol),
                        Field_Value = x.Field<dynamic>(colName)
                    }).Distinct().ToList();
                    for (int k = 0; k < dtVal1.Count; k++)
                    {
                        lstError.Add(new IThresholdError { AutoCol_ID = dtVal1[k].AutoCol_ID, Field_Name = colName, Field_Value = dtVal1[k].Field_Value, Error_Description = ">/>= operator in MaxValue Operator Field" });
                    }
                }
            }
            SelThresholdError = false;
            if (lstError.Any())
            {
                uploadThresholdError = Visibility.Visible;
                NotifyPropertyChanged("uploadThresholdError");
                listThresholdError = new List<IThresholdError>(lstError);
                NotifyPropertyChanged("comonThresholdError_VM");
                uploadThresholdError = Visibility.Collapsed;
                NotifyPropertyChanged("uploadThresholdError");
                SelThresholdError = true;
            }
            else
            {
                listThresholdError = null;
                NotifyPropertyChanged("comonThresholdError_VM");
            }
            NotifyPropertyChanged("SelThresholdError");
        }
        public string GetUnitfromDataDictionaryNewF(string[] suffixSplit, string abbr)
        {
            string abbr1 = string.Empty;
            string unit1 = string.Empty;
            string ssf = string.Empty;

            abbr1 = string.IsNullOrWhiteSpace(abbr) ? "@>" : abbr + ">";
            suffixSplit = suffixSplit.Where(x => string.IsNullOrWhiteSpace(x) == false).ToArray();
            for (int k = 0; k < suffixSplit.Count(); k++)
            {
                ssf = suffixSplit[k].Substring(0, suffixSplit[k].IndexOf('>') + 1);
                string abbrNew = abbr == null ? abbr : abbr.Trim();
                string abbr1New = abbr1 == null ? abbr1 : abbr1.Trim();
                if ((ssf.TrimEnd('.').Trim() == abbr1New) || (suffixSplit[k].TrimEnd('.').Trim() == abbrNew))
                {
                    unit1 = suffixSplit[k].Replace(abbr1, string.Empty).Replace(".", string.Empty).Trim();
                }
            }
            unit1 = objCommon.ChkGreekSmallLetter(unit1);
            return unit1;
        }
        public bool SelThresholdError { get; set; } = false;
        private List<IThresholdError> _listThresholdError { get; set; } = new List<IThresholdError>();
        public List<IThresholdError> listThresholdError
        {
            get { return _listThresholdError; }
            set
            {
                if (_listThresholdError != value)
                {
                    _listThresholdError = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IThresholdError>>>(new PropertyChangedMessage<List<IThresholdError>>(null, _listThresholdError, "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnThresholdError()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "AutoCol_ID", ColBindingName = "AutoCol_ID", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_Name", ColBindingName = "Field_Name", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_Value", ColBindingName = "Field_Value", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Error_Description", ColBindingName = "Error_Description", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }


        public ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM> ListAllColumnDG { get; set; } = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
        public DataColumnCollection defaultDataTableColumns { get; set; }
        public Visibility LoaderAddInfo { get; set; }
        private void ExecuteFunction(List<GenericCriteria_VM> lstData)
        {
            var dtTemp = new DataTable();
            dtTemp.Columns.Add("ColumnName");
            dtTemp.Columns.Add("Threshold_Type");
            dtTemp.Columns.Add("Unit_Type");
            dtTemp.Columns.Add("Manual_Unit");
            dtTemp.Columns.Add("Unit_Field");
            dtTemp.Columns.Add("Category");
            dtTemp.Columns.Add("Operator_Type");
            dtTemp.Columns.Add("Manual_Operator");
            dtTemp.Columns.Add("Operator_Field");
            dtTemp.Columns.Add("Delimiter");
            dtTemp.Columns.Add("Consider_Single_Value");
            dtTemp.Columns.Add("Consider_Null_Value");
            dtTemp.Columns.Add("ValueField_Contain_Operator");
            defaultDataTableColumns = dtTemp.Columns;
            BindListAllColumn();
            LoaderAddInfo = Visibility.Visible;
            NotifyPropertyChanged("LoaderAddInfo");
            Task.Run(() =>
            {
                myList = lstData;
                //selectedItemCombo = ListComboxItem.Where(x => x.id == 25).FirstOrDefault();
                LoaderAddInfo = Visibility.Collapsed;
                NotifyPropertyChanged("LoaderAddInfo");
                Navigate((int)PagingMode.First);
                //NotifyPropertyChanged("selectedItemCombo");
            });
        }
        public void BindListAllColumn()
        {
            var list = new List<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();

            foreach (DataColumn dt in defaultDataTableColumns)
            {
                list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { IsSelected = true, ColumnName = dt.ColumnName });
            }

            ListAllColumnDG = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>(list);
            NotifyPropertyChanged("ListAllColumnDG");
        }
        public ICommand CommandOnColSelectedChange { get; set; }
        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDG.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            switch (selectCol.ColumnName)
            {
                case "ColumnName":
                    ColumnNameVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("ColumnNameVisible");
                    break;
                case "Threshold_Type":
                    Threshold_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Threshold_TypeVisible");
                    break;
                case "Unit_Type":
                    Unit_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Unit_TypeVisible");
                    break;
                case "Operator_Type":
                    Operator_TypeVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Operator_TypeVisible");
                    break;
                case "Manual_Unit":
                    Manual_UnitVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Manual_UnitVisible");
                    break;
                case "Manual_Operator":
                    Manual_OperatorVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Manual_OperatorVisible");
                    break;
                case "Unit_Field":
                    Min_Max_Unit_FieldVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Min_Max_Unit_FieldVisible");
                    break;
                case "Operator_Field":
                    Operator_FieldVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Operator_FieldVisible");
                    break;
                case "Category":
                    CategoryVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("CategoryVisible");
                    break;
                case "Delimiter":
                    DelimiterVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("DelimiterVisible");
                    break;
                case "Consider_Single_Value":
                    Consider_Single_ValueVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Consider_Single_ValueVisible");
                    break;
                case "Consider_Null_Value":
                    Consider_Null_ValueVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("Consider_Null_ValueVisible");
                    break;
                case "ValueField_Contain_Operator":
                    ValueField_Contain_OperatorVisible = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("ValueField_Contain_OperatorVisible");
                    break;
            }
        }
    }
    public class IThresholdError
    {
        public Int32 AutoCol_ID { get; set; }
        public string Field_Name { get; set; }
        public string Field_Value { get; set; }
        public string Error_Description { get; set; }
    }
}
