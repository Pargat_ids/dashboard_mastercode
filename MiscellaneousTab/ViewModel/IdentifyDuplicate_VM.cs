﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public ICommand IdentifyDuplicate { get; set; }
        public ICommand FixDuplicate { get; set; }
        public Visibility uploadIdentifyDuplicateVis { get; set; } = Visibility.Collapsed;
        public string DefaultLableIdentifyDuplicate { get; set; }
        private DataTable newdtRaw = new DataTable();

        public CommonDataGrid_ViewModel<IIdentifyDuplicate> comonIdentifyDuplicate_VM { get; set; }

        private List<IIdentifyDuplicate> _lstIdentifyDuplicate { get; set; } = new List<IIdentifyDuplicate>();
        public List<IIdentifyDuplicate> lstIdentifyDuplicate
        {
            get { return _lstIdentifyDuplicate; }
            set
            {
                if (_lstIdentifyDuplicate != value)
                {
                    _lstIdentifyDuplicate = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IIdentifyDuplicate>>>(new PropertyChangedMessage<List<IIdentifyDuplicate>>(null, _lstIdentifyDuplicate, "Default List"));
                }
            }
        }

        private void IdentifyDuplicateExecute(object o)
        {

            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var combineField = string.Empty;
            FieldNameSubroot = new List<string>();
            using (var context = new CRAModel())
            {
                var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == lstDic).Select(y => y.QSID_ID).FirstOrDefault();
                var mapqsidFieldid = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && (x.IsPhraseField ==
                                      true) && x.MultiValuePhraseDelimiterID > 0).Select(y => new { y.FieldNameID, y.MultiValuePhraseDelimiterID }).ToList()
                                      join d2 in context.Library_FieldNames.AsNoTracking()
                                      on d1.FieldNameID equals d2.FieldNameID
                                      join d3 in context.Library_Delimiters.AsNoTracking()
                                      on d1.MultiValuePhraseDelimiterID equals d3.DelimiterID
                                      select new ImapFields { FieldName = d2.FieldName.ToUpper().Trim(), Delimiter = d3.Delimiter }).ToList();
                var onlyFieldNames = mapqsidFieldid.Select(y => y.FieldName).ToList();
                if (!onlyFieldNames.Contains("RN"))
                {
                    onlyFieldNames.Add("RN");
                }
                FieldNameSubroot.AddRange(onlyFieldNames);
                combineField = string.Join(",", onlyFieldNames);

                var dtData = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + tableName + "]", CommonFilepath);
                if (dtData.Columns.Count == 0)
                {
                    _notifier.ShowError(combineField + " - fields not found in selected file!");
                    return;
                }
                if (dtData == null || dtData.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
                var chkRNDuplicate = dtData.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
                if (chkRNDuplicate.Any())
                {
                    _notifier.ShowError("Found duplicate RN in this file, first fix them");
                    return;
                }
                uploadIdentifyDuplicateVis = Visibility.Visible;
                NotifyPropertyChanged("uploadIdentifyDuplicateVis");
                Task.Run(() =>
                {
                    var lstErr = _objLibraryFunction_Service.GetErrors(mapqsidFieldid, dtData).Item1;
                    lstIdentifyDuplicate = new List<IIdentifyDuplicate>(lstErr);
                    NotifyPropertyChanged("comonIdentifyDuplicate_VM");
                    uploadIdentifyDuplicateVis = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadIdentifyDuplicateVis");
                });
            }
        }

        private void FixDuplicateExecute(object o)
        {
            string extraQuery = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
                
            }
            if (tableName == "DATA")
            {
                extraQuery = " where omit is null or omit = ''";
            }
            var combineField = string.Empty;
            var fieldName1 = new List<string>();
            using (var context = new CRAModel())
            {
                var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == lstDic).Select(y => y.QSID_ID).FirstOrDefault();
                var mapqsidFieldid = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && (x.IsPhraseField ==
                                      true ) && x.MultiValuePhraseDelimiterID > 0).Select(y => new { y.FieldNameID, y.MultiValuePhraseDelimiterID }).ToList()
                                      join d2 in context.Library_FieldNames.AsNoTracking()
                                      on d1.FieldNameID equals d2.FieldNameID
                                      join d3 in context.Library_Delimiters.AsNoTracking()
                                      on d1.MultiValuePhraseDelimiterID equals d3.DelimiterID
                                      select new ImapFields { FieldName = d2.FieldName.ToUpper().Trim(), Delimiter = d3.Delimiter }).ToList();
                var onlyFieldNames = mapqsidFieldid.Select(y => y.FieldName).ToList();
                if (!onlyFieldNames.Contains("RN"))
                {
                    onlyFieldNames.Add("RN");
                }
                fieldName1.AddRange(onlyFieldNames);
                combineField = string.Join(",", onlyFieldNames);

                var dtData = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + tableName + "] " + extraQuery, CommonFilepath);
                if (dtData.Columns.Count == 0)
                {
                    _notifier.ShowError(combineField + " - fields not found in selected file!");
                    return;
                }
                if (dtData == null || dtData.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
                var chkRNDuplicate = dtData.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
                if (chkRNDuplicate.Any())
                {
                    _notifier.ShowError("Found duplicate RN in this file, first fix them");
                    return;
                }
                uploadIdentifyDuplicateVis = Visibility.Visible;
                NotifyPropertyChanged("uploadIdentifyDuplicateVis");
                Task.Run(() =>
                {
                    var dtVal = _objLibraryFunction_Service.GetErrors(mapqsidFieldid, dtData).Item2;
                    //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                    if (dtVal.Rows.Count > 0)
                    {
                        List<string> listColParam = new List<string>();
                        List<string> columnList = new List<string>();
                        var queryString = string.Empty;
                        var updateQuery = string.Empty;
                        listColParam.Add("@RN");
                        columnList.Add("RN");
                        for (int i = 0; i < fieldName1.Count(); i++)
                        {
                            if (fieldName1[i] != "RN" && fieldName1[i].ToUpper() != "IDENT")
                            {
                                queryString += "[" +  fieldName1[i] + "] LongText,";
                                listColParam.Add("@" + fieldName1[i]);
                                columnList.Add(fieldName1[i]);
                                updateQuery += "a." + fieldName1[i] + " = iif(len(b." + fieldName1[i] + ") = 0, null,b." + fieldName1[i] + "),";
                            }
                        }
                        queryString = queryString.TrimEnd(',');
                        updateQuery = updateQuery.TrimEnd(',');
                        
                        objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                        objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
                        _objAccessVersion_Service.ExportAccessData(dtVal, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());

                        objCommon.DbaseQueryReturnString("update [" + tableName + "] a inner join TMPDh b on a.RN = b.RN set " + updateQuery, CommonFilepath);


                        objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    }
                    
                    dtData = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + tableName + "] " + extraQuery, CommonFilepath);
                    var result = _objLibraryFunction_Service.GetErrors(mapqsidFieldid, dtData).Item1;
                    lstIdentifyDuplicate = new List<IIdentifyDuplicate>(result);
                    NotifyPropertyChanged("comonIdentifyDuplicate_VM");
                    uploadIdentifyDuplicateVis = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadIdentifyDuplicateVis");
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("Updated in access file.");
                    });
                    //if (isFileOpen)
                    //{
                    //    objCommon.OpenFile(CommonFilepath);
                    //}
                });
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnIdentifyDuplicate()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_Name", ColBindingName = "Field_Name", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldValue", ColBindingName = "OldValue", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewValue", ColBindingName = "NewValue", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }
    }
    
    
}
