﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        private string TableNameECValidator { get; set; }
        private string FieldNameECValidator { get; set; }
        public ICommand CheckECValidator { get; set; }
        public Visibility uploadECValidator { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<IECValidator> comonECValidator_VM { get; set; }
        public string DefaultLableECValidator { get; set; }
        private List<CommonDataGridColumn> GetListECValidator()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldValue", ColBindingName = "FieldValue", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsValid", ColBindingName = "IsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Remarks", ColBindingName = "Remarks", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private List<IECValidator> _listECValidator { get; set; } = new List<IECValidator>();
        public List<IECValidator> listECValidator
        {
            get { return _listECValidator; }
            set
            {
                if (_listECValidator != value)
                {
                    _listECValidator = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IECValidator>>>(new PropertyChangedMessage<List<IECValidator>>(null, _listECValidator, "Default List"));
                }
            }
        }

        private void CheckECValidatorExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameECValidator = TabName;
            FieldNameECValidator = string.Empty;
            List<string> ECNumberException = new List<string>();
            using (var context = new CRAModel())
            {
                ECNumberException = context.ECNumber_Checksum_Exceptions.AsNoTracking().Select(x => x.ECNumbers).ToList();
                if (TabName.ToString() == "Default")
                {
                    TableNameDateFormat = "DATA";
                    var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
                    var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());

                    var EcFields = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.IsIdentifierField == true && x.IdentifierTypeID == 9).Select(y => y.FieldNameID).ToList()
                                    join d2 in context.Library_FieldNames.AsNoTracking()
                                    on d1 equals d2.FieldNameID
                                    select d2.FieldName).Distinct().ToList();
                    if (EcFields.Count == 0)
                    {
                        _notifier.ShowError("No ECNumber field already Exist");
                        return;
                    }
                    else
                    {
                        FieldNameECValidator = string.Join(",", EcFields);
                    }

                }
                else
                {
                    FieldNameECValidator = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).FirstOrDefault();
                }
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select distinct " + FieldNameECValidator + " from [" + TableNameECValidator + "] ", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(FieldNameECValidator + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            uploadECValidator = Visibility.Visible;
            NotifyPropertyChanged("uploadECValidator");
            Task.Run(() =>
            {
                var result = new List<IECValidator>();
                for (int i = 0; i < dtRaw.Columns.Count; i++)
                {
                    var fldName = dtRaw.Columns[i].ColumnName;
                    for (int j = 0; j < dtRaw.Rows.Count; j++)
                    {
                        var chkValIdent = CheckIdentifierFormat(dtRaw.Rows[j][i].ToString(), ECNumberException);
                        result.Add(new IECValidator
                        {
                            FieldName = fldName,
                            FieldValue = dtRaw.Rows[j][i].ToString(),
                            IsValid = chkValIdent != "True" ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault(),
                            Remarks = chkValIdent != "True" ? chkValIdent : String.Empty,
                        });

                    }
                    listECValidator = new List<IECValidator>(result);
                    NotifyPropertyChanged("comonECValidator_VM");
                    uploadECValidator = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadECValidator");
                }
            });
        }

        private string CheckIdentifierFormat(string str, List<string> ECNumberException)
        {
            if ((str.Trim().Length != 9) || (str.Trim().Substring(0, 1) == "0") || (str.Trim().Substring(0, 1) == "1"))
            {
                return "Not Valid, Identifier shouldn't start with 0/1 and length should be 9";
            }

            bool result = Regex.IsMatch(str.Trim(), @"^[0-9\-]+$");
            if (result == false)
            {
                return "Not Valid, Identifier should be numeric";
            }

            char[] splitStr = str.Trim().ToCharArray();
            if (splitStr[3] != '-' || splitStr[7] != '-')
            {
                return "Not Valid, format should be XXX-XXX-x";
            }

            if (ECNumberException.Contains(str))
            {
                return "True";
            }

            Int32 totalValue = (Convert.ToInt32(splitStr[0].ToString()) * 1) + (Convert.ToInt32(splitStr[1].ToString()) * 2) + (Convert.ToInt32(splitStr[2].ToString()) * 3) + (Convert.ToInt32(splitStr[4].ToString()) * 4) + (Convert.ToInt32(splitStr[5].ToString()) * 5) + (Convert.ToInt32(splitStr[6].ToString()) * 6);
            return (totalValue % 11) == Convert.ToInt32(splitStr[8].ToString()) ? "True" : "Not Valid, as per European Community Centre";
        }
    }
    public class IECValidator
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsValid { get; set; }
        public string Remarks { get; set; }
    }
}
