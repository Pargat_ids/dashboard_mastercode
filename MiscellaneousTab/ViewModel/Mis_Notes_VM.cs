﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using MiscellaneousTab.CustomUserControl;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public Visibility uploadNoteTranslate { get; set; } = Visibility.Collapsed;
        public Visibility uploadNoteCodeConsistency { get; set; } = Visibility.Collapsed;
        public ICommand CheckNoteCodeConsistency { get; set; }
        public ICommand FixNoteCodeSameCell { get; set; }
        private void CheckNoteCodeConsistencyExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "Notes";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 NOTES_FIELD, NOTE_CODE, Note_Engl, Note_Text from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("NOTES_FIELD, NOTE_CODE, Note_Engl, Note_Text - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            uploadNoteCodeConsistency = Visibility.Visible;
            NotifyPropertyChanged("uploadNoteCodeConsistency");

            Task.Run(() =>
            {
                Tuple<DataTable, List<string>> result = null;
                result = _objLibraryFunction_Service.CheckErrorInNotes(CommonFilepath, tableName);

                if (result.Item1.Rows.Count == 0)
                {
                    listNotecodeConsistency = null;
                    NotifyPropertyChanged("listNotecodeConsistency");
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("No error found in selected file!");
                    });
                }
                else
                {
                    listNotecodeConsistency = new StandardDataGrid_VM("NoteCode Consistency", result.Item1);
                    NotifyPropertyChanged("listNotecodeConsistency");
                    uploadNoteCodeConsistency = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadNoteCodeConsistency");
                    return;

                }

                uploadNoteCodeConsistency = Visibility.Collapsed;
                NotifyPropertyChanged("uploadNoteCodeConsistency");
            });
        }

        private void ShowNotesExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "Notes";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 NOTES_FIELD, NOTE_CODE, Note_Engl, Note_Text from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("NOTES_FIELD, NOTE_CODE, Note_Engl, Note_Text - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            uploadNoteTranslate = Visibility.Visible;
            NotifyPropertyChanged("uploadNoteTranslate");
            Tuple<DataTable, List<iUserInputNotes>, DataTable, List<string>> result = null;
            //Task.Run(() =>
            //{
            result = _objLibraryFunction_Service.AddinTranslatedPhrases_Notes(CommonFilepath, Engine.CurrentUserSessionID, tableName);

            if (result.Item1.Rows.Count > 0)
            {
                PopupNotesConsistency objChangesAdd = new PopupNotesConsistency("NoteCode Consistency", result.Item1);
                objChangesAdd.ShowDialog();
                uploadNoteTranslate = Visibility.Collapsed;
                NotifyPropertyChanged("uploadNoteTranslate");
                return;
            }
            if (result.Item2.Count() > 1)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    var chk = result.Item2.Select(x => new iUserInput { Approve = x.Approve, EnglishPhrase = x.EnglishPhrase, GroupID = x.GroupID, LanguageID = x.LanguageID, LanguageName = x.LanguageName, Old_New = x.Old_New, Phrase = x.Phrase, PhraseID = x.PhraseID, QsidCount = x.QsidCount, TransID = x.TransID }).ToList();
                    Popup_UserInput objChangesAdd = new Popup_UserInput(chk);
                    objChangesAdd.ShowDialog();
                });
            }
            listNoteTranslate = new StandardDataGrid_VM("Map_TranslatedPhrases", result.Item3);
            NotifyPropertyChanged("listNoteTranslate");
            uploadNoteTranslate = Visibility.Collapsed;
            NotifyPropertyChanged("uploadNoteTranslate");
            //});
        }

        private void FixNoteCodeSameCellExecute(object o)
        {
            string extraQuery = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }

            using (var context = new CRAModel())
            {
                var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == lstDic).Select(y => y.QSID_ID).FirstOrDefault();
                var mapqsidFieldid = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && (x.IsNoteCodeField ==
                                      true) && x.MultiValuePhraseDelimiterID > 0).Select(y => new { y.FieldNameID, y.MultiValuePhraseDelimiterID }).ToList()
                                      join d2 in context.Library_FieldNames.AsNoTracking()
                                      on d1.FieldNameID equals d2.FieldNameID
                                      join d3 in context.Library_Delimiters.AsNoTracking()
                                      on d1.MultiValuePhraseDelimiterID equals d3.DelimiterID
                                      select new ImapFields { FieldName = d2.FieldName.ToUpper().Trim(), Delimiter = d3.Delimiter }).ToList();
                var onlyFieldName = mapqsidFieldid.Select(y => y.FieldName).ToList();
                onlyFieldName.Add("RN");
                var onlyFieldNames = string.Join(",", onlyFieldName);
                var dtData = objCommon.DbaseQueryReturnTable("select " + onlyFieldNames + " from DATA where omit is null or omit =''", CommonFilepath);
                if (dtData.Columns.Count == 0)
                {
                    _notifier.ShowError(onlyFieldNames + " - fields not found in selected file!");
                    return;
                }
                if (dtData == null || dtData.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
                var chkRNDuplicate = dtData.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
                if (chkRNDuplicate.Any())
                {
                    _notifier.ShowError("Found duplicate RN in this file, first fix them");
                    return;
                }
                uploadIdentifyDuplicateVis = Visibility.Visible;
                NotifyPropertyChanged("uploadIdentifyDuplicateVis");
                Task.Run(() =>
                {
                    var dtVal = _objLibraryFunction_Service.GetErrors(mapqsidFieldid, dtData).Item2;
                    //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                    if (dtVal.Rows.Count > 0)
                    {
                        List<string> listColParam = new List<string>();
                        List<string> columnList = new List<string>();
                        var queryString = string.Empty;
                        var updateQuery = string.Empty;
                        listColParam.Add("@RN");
                        columnList.Add("RN");
                        for (int i = 0; i < onlyFieldName.Count(); i++)
                        {
                            if (onlyFieldName[i] != "RN" && onlyFieldName[i].ToUpper() != "IDENT")
                            {
                                queryString += "[" + onlyFieldName[i] + "] LongText,";
                                listColParam.Add("@" + onlyFieldName[i]);
                                columnList.Add(onlyFieldName[i]);
                                updateQuery += "a." + onlyFieldName[i] + " = iif(len(b." + onlyFieldName[i] + ") = 0, null,b." + onlyFieldName[i] + "),";
                            }
                        }
                        queryString = queryString.TrimEnd(',');
                        updateQuery = updateQuery.TrimEnd(',');
                       
                        objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                        objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
                        _objAccessVersion_Service.ExportAccessData(dtVal, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                        objCommon.DbaseQueryReturnString("update DATA a inner join TMPDh b on a.RN = b.RN set " + updateQuery, CommonFilepath);
                        objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    }
                    listNotecodeConsistency = null;
                    NotifyPropertyChanged("listNotecodeConsistency");
                    uploadNoteCodeConsistency = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadNoteCodeConsistency");
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("update in access file");
                    });
                    //if (isFileOpen)
                    //{
                    //    objCommon.OpenFile(CommonFilepath);
                    //}
                });
            }
        }
    }
    public class iFieldGroup
    {
        public int FieldNameID { get; set; }
        public int? GroupID { get; set; }
    }
}


