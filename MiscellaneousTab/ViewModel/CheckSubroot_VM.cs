﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        private string TableNameSubroot { get; set; }
        public List<ISubrootType> listSubrootType { get; set; }
        private List<string> FieldNameSubroot { get; set; }
        public CommonDataGrid_ViewModel<ISubroot> comonSubroots_VM { get; set; }
        public CommonDataGrid_ViewModel<IDateFormat> comonDateFormat_VM { get; set; }
        public ISubroot SelectedSubroot { get; set; }
        public Visibility uploadSubroots { get; set; } = Visibility.Collapsed;
        public ICommand CheckSubroots { get; set; }
        private void CheckSubrootsExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameSubroot = TabName;
            var combineField = string.Empty;
            FieldNameSubroot = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameSubroot = "DATA";
            }
            var result = new List<ISubroot>();
            listSubroots = new List<ISubroot>(result);
            NotifyPropertyChanged("comonSubroots_VM");
            using (var context = new CRAModel())
            {
                var catid = 0;
                switch (SelectedSubrootType.SubrootType)
                {
                    case "Subroot":
                        catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Subroot").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        break;
                    case "Expressed As (Phrase)":
                        catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Expressed As (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        break;
                    case "Physical Form (Phrase)":
                        catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Physical Form (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        break;
                    case "Threshold calculation basis":
                        catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Threshold calculation basis").Select(y => y.PhraseCategoryID).FirstOrDefault();
                        break;
                }
                if (catid != 0)
                {
                    var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                    var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == lstDic).Select(y => y.QSID_ID).FirstOrDefault();
                    var mapqsidFieldid = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && x.IsPhraseField == true && x.PhraseCategoryID == catid)
                                          join d2 in context.Library_FieldNames.AsNoTracking()
                                          on d1.FieldNameID equals d2.FieldNameID
                                          select d2.FieldName).ToList();
                    FieldNameSubroot.AddRange(mapqsidFieldid);
                    combineField = string.Join(",", mapqsidFieldid);
                }
            }

            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameSubroot + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            uploadSubroots = Visibility.Visible;
            NotifyPropertyChanged("uploadSubroots");
            Task.Run(() =>
            {
                switch (SelectedSubrootType.SubrootType)
                {
                    case "Subroot":
                        BindSubroot();
                        break;
                    case "Expressed As (Phrase)":
                        BindExpressedAs();
                        break;
                    case "Physical Form (Phrase)":
                        BindPhysicalForm();
                        break;
                    case "Threshold calculation basis":
                        BindThresholdBasis();
                        break;
                }
            });
        }
        private bool CheckSubrootsCanExecute(object o)
        {
            if (SelectedSubrootType != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private void BindSubroot()
        {
            List<(string, string, string, string)> lstHashlngId = new List<(string, string, string, string)>();
            List<ISubroot> lstHashFinal = new List<ISubroot>();
            using (var context = new CRAModel())
            {
                var catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Subroot").Select(y => y.PhraseCategoryID).FirstOrDefault();
                var engLngId = context.Library_Languages.AsNoTracking().Where(x => x.LanguageName == "English").Select(y => y.LanguageID).FirstOrDefault();
                for (int i = 0; i < FieldNameSubroot.Count(); i++)
                {

                    var fName = FieldNameSubroot[i];
                    var tabData = objCommon.DbaseQueryReturnTable("select distinct " + fName + " from [" + TableNameSubroot + "] where " + fName + " is not null and " + fName + " <> ''", CommonFilepath);
                    var replcedBy = (from d1 in tabData.AsEnumerable()
                                     join d2 in context.Map_ControlPhrase_Replacement.AsNoTracking()
                                     on d1.Field<string>(fName) equals d2.Orginal_Value
                                     join d3 in context.Library_Phrases.AsNoTracking()
                                     on d2.Replace_ControlPhrase_ID equals d3.PhraseID
                                     select new
                                     {
                                         orgPhrase = d1.Field<string>(fName).Trim(),
                                         RepWith = d3.Phrase,
                                     }).Distinct().ToList();
                    for (int j = 0; j < tabData.Rows.Count; j++)
                    {
                        var phrVal = tabData.Rows[j].Field<string>(fName).Trim();
                        var chkExt = replcedBy.Where(x => x.orgPhrase == phrVal).Select(y => y.RepWith).FirstOrDefault();
                        if (!string.IsNullOrEmpty(chkExt))
                        {
                            var phrValLower = chkExt.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(chkExt);
                            lstHashlngId.Add((phraseHash, chkExt, fName, phrVal + " Replaced By -" + chkExt));
                        }
                        else
                        {
                            var phraseVal = objCommon.RemoveWhitespaceCharacter(phrVal, StripCharacters);
                            var phrValLower = phraseVal.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(phrValLower);
                            lstHashlngId.Add((phraseHash, phraseVal, fName, ""));
                        }
                    }
                }


                var chkInLibSubroot = (from d1 in lstHashlngId
                                       join d2 in context.Library_SubRoots.AsNoTracking()
                                       on d1.Item1 equals d2.PhraseHash_LowerCase
                                       into tg
                                       from tcheck in tg.DefaultIfEmpty()
                                       where tcheck == null
                                       select d1).Distinct().ToList();

                if (chkInLibSubroot.Any())
                {
                    chkInLibSubroot.ForEach(x =>
                    {
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.Item3,
                            Phrase = x.Item2,
                            PhraseID = 0,
                            Active_Lists = new List<HyperLinkDataValue>(),
                            No_of_active_lists = 0,
                            PhraseHash = x.Item1,
                            NeedToAddLibrary = true,
                            Remarks = x.Item4
                        });
                    });

                }
                var validPhrases = (from d1 in lstHashlngId
                                    join d2 in context.Library_SubRoots.AsNoTracking()
                                    on d1.Item1 equals d2.PhraseHash_LowerCase
                                    select new { d1, d2.PhraseID_Subroot }).Distinct().ToList();
                if (validPhrases.Any())
                {
                    var res = (from d1 in validPhrases
                               join d2 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.PhraseCategoryID == catid)
                               on d1.PhraseID_Subroot equals d2.PhraseID
                               select new { d1.PhraseID_Subroot, d2.QSID_ID }).Distinct().ToList();
                    var chkQsidCountValid = (from d1 in res
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID_Subroot, d3.QSID, d1.QSID_ID }).Distinct().ToList();
                    validPhrases.ForEach(x =>
                    {
                        var noofActiveList = chkQsidCountValid.Count(y => y.PhraseID_Subroot == x.PhraseID_Subroot);
                        var ActiveList = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_Subroot == x.PhraseID_Subroot).Select(z => z.QSID).ToList());
                        var activeListID = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_Subroot == x.PhraseID_Subroot).Select(z => z.QSID_ID).ToList());
                        var linkVal = ConvertInfoToHyoerlinkVal(ActiveList, activeListID);
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.d1.Item3,
                            Phrase = x.d1.Item2,
                            PhraseID = (int)x.PhraseID_Subroot,
                            Active_Lists = linkVal,
                            No_of_active_lists = noofActiveList,
                            PhraseHash = x.d1.Item1,
                            NeedToAddLibrary = false,
                            Remarks = x.d1.Item4
                        });
                    });
                }


            }
            listSubroots = new List<ISubroot>(lstHashFinal);
            NotifyPropertyChanged("comonSubroots_VM");
            uploadSubroots = Visibility.Collapsed;
            NotifyPropertyChanged("uploadSubroots");
        }
        private void BindExpressedAs()
        {
            List<(string, string, string, string)> lstHashlngId = new List<(string, string, string, string)>();
            List<ISubroot> lstHashFinal = new List<ISubroot>();
            using (var context = new CRAModel())
            {
                var catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Expressed As (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();
                var engLngId = context.Library_Languages.AsNoTracking().Where(x => x.LanguageName == "English").Select(y => y.LanguageID).FirstOrDefault();

                for (int i = 0; i < FieldNameSubroot.Count(); i++)
                {

                    var fName = FieldNameSubroot[i];
                    var tabData = objCommon.DbaseQueryReturnTable("select distinct " + fName + " from [" + TableNameSubroot + "] where " + fName + " is not null and " + fName + " <> ''", CommonFilepath);
                    var replcedBy = (from d1 in tabData.AsEnumerable()
                                     join d2 in context.Map_ControlPhrase_Replacement.AsNoTracking()
                                     on d1.Field<string>(fName) equals d2.Orginal_Value
                                     join d3 in context.Library_Phrases.AsNoTracking()
                                     on d2.Replace_ControlPhrase_ID equals d3.PhraseID
                                     select new
                                     {
                                         orgPhrase = d1.Field<string>(fName).Trim(),
                                         RepWith = d3.Phrase,
                                     }).Distinct().ToList();
                    for (int j = 0; j < tabData.Rows.Count; j++)
                    {
                        var phrVal = tabData.Rows[j].Field<string>(fName).Trim();
                        var chkExt = replcedBy.Where(x => x.orgPhrase == phrVal).Select(y => y.RepWith).FirstOrDefault();
                        if (!string.IsNullOrEmpty(chkExt))
                        {
                            var phrValLower = chkExt.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(chkExt);
                            lstHashlngId.Add((phraseHash, chkExt, fName, phrVal + " Replaced By -" + chkExt));
                        }
                        else
                        {
                            var phraseVal = objCommon.RemoveWhitespaceCharacter(phrVal, StripCharacters);
                            var phrValLower = phraseVal.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(phrValLower);
                            lstHashlngId.Add((phraseHash, phraseVal, fName, ""));
                        }
                    }
                }

                var chkInLibSubroot = (from d1 in lstHashlngId
                                       join d2 in context.Library_ExpressedAs.AsNoTracking()
                                       on d1.Item1 equals d2.PhraseHash_LowerCase
                                       into tg
                                       from tcheck in tg.DefaultIfEmpty()
                                       where tcheck == null
                                       select d1).Distinct().ToList();

                if (chkInLibSubroot.Any())
                {
                    chkInLibSubroot.ForEach(x =>
                    {
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.Item3,
                            Phrase = x.Item2,
                            PhraseID = 0,
                            Active_Lists = new List<HyperLinkDataValue>(),
                            No_of_active_lists = 0,
                            PhraseHash = x.Item1,
                            NeedToAddLibrary = true,
                            Remarks = x.Item4
                        });
                    });

                }
                var validPhrases = (from d1 in lstHashlngId
                                    join d2 in context.Library_ExpressedAs.AsNoTracking()
                                    on d1.Item1 equals d2.PhraseHash_LowerCase
                                    select new { d1, d2.PhraseID_ExpressedAs }).Distinct().ToList();
                if (validPhrases.Any())
                {
                    var res = (from d1 in validPhrases
                               join d2 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.PhraseCategoryID == catid)
                               on d1.PhraseID_ExpressedAs equals d2.PhraseID
                               select new { d1.PhraseID_ExpressedAs, d2.QSID_ID }).Distinct().ToList();
                    var chkQsidCountValid = (from d1 in res
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID_ExpressedAs, d3.QSID, d1.QSID_ID }).Distinct().ToList();
                    validPhrases.ForEach(x =>
                    {
                        var noofActiveList = chkQsidCountValid.Count(y => y.PhraseID_ExpressedAs == x.PhraseID_ExpressedAs);
                        var ActiveList = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_ExpressedAs == x.PhraseID_ExpressedAs).Select(z => z.QSID).ToList());
                        var activeListID = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_ExpressedAs == x.PhraseID_ExpressedAs).Select(z => z.QSID_ID).ToList());
                        var linkVal = ConvertInfoToHyoerlinkVal(ActiveList, activeListID);
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.d1.Item3,
                            Phrase = x.d1.Item2,
                            PhraseID = (int)x.PhraseID_ExpressedAs,
                            Active_Lists = linkVal,
                            No_of_active_lists = noofActiveList,
                            PhraseHash = x.d1.Item1,
                            NeedToAddLibrary = false,
                            Remarks = x.d1.Item4
                        });
                    });
                }


            }
            listSubroots = new List<ISubroot>(lstHashFinal);
            NotifyPropertyChanged("comonSubroots_VM");
            uploadSubroots = Visibility.Collapsed;
            NotifyPropertyChanged("uploadSubroots");
        }
        private void BindPhysicalForm()
        {
            List<(string, string, string, string)> lstHashlngId = new List<(string, string, string, string)>();
            List<ISubroot> lstHashFinal = new List<ISubroot>();
            using (var context = new CRAModel())
            {
                var catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Physical Form (Phrase)").Select(y => y.PhraseCategoryID).FirstOrDefault();
                var engLngId = context.Library_Languages.AsNoTracking().Where(x => x.LanguageName == "English").Select(y => y.LanguageID).FirstOrDefault();

                for (int i = 0; i < FieldNameSubroot.Count(); i++)
                {

                    var fName = FieldNameSubroot[i];
                    var tabData = objCommon.DbaseQueryReturnTable("select distinct " + fName + " from [" + TableNameSubroot + "] where " + fName + " is not null and " + fName + " <> ''", CommonFilepath);
                    var replcedBy = (from d1 in tabData.AsEnumerable()
                                     join d2 in context.Map_ControlPhrase_Replacement.AsNoTracking()
                                     on d1.Field<string>(fName) equals d2.Orginal_Value
                                     join d3 in context.Library_Phrases.AsNoTracking()
                                     on d2.Replace_ControlPhrase_ID equals d3.PhraseID
                                     select new
                                     {
                                         orgPhrase = d1.Field<string>(fName).Trim(),
                                         RepWith = d3.Phrase,
                                     }).Distinct().ToList();
                    for (int j = 0; j < tabData.Rows.Count; j++)
                    {
                        var phrVal = tabData.Rows[j].Field<string>(fName).Trim();
                        var chkExt = replcedBy.Where(x => x.orgPhrase == phrVal).Select(y => y.RepWith).FirstOrDefault();
                        if (!string.IsNullOrEmpty(chkExt))
                        {
                            var phrValLower = chkExt.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(chkExt);
                            lstHashlngId.Add((phraseHash, chkExt, fName, phrVal + " Replaced By -" + chkExt));
                        }
                        else
                        {
                            var phraseVal = objCommon.RemoveWhitespaceCharacter(phrVal, StripCharacters);
                            var phrValLower = phraseVal.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(phrValLower);
                            lstHashlngId.Add((phraseHash, phraseVal, fName, ""));
                        }
                    }
                }

                var chkInLibSubroot = (from d1 in lstHashlngId
                                       join d2 in context.Library_PhysicalForm.AsNoTracking()
                                       on d1.Item1 equals d2.PhraseHash_LowerCase
                                       into tg
                                       from tcheck in tg.DefaultIfEmpty()
                                       where tcheck == null
                                       select d1).Distinct().ToList();

                if (chkInLibSubroot.Any())
                {
                    chkInLibSubroot.ForEach(x =>
                    {
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.Item3,
                            Phrase = x.Item2,
                            PhraseID = 0,
                            Active_Lists = new List<HyperLinkDataValue>(),
                            No_of_active_lists = 0,
                            PhraseHash = x.Item1,
                            NeedToAddLibrary = true,
                            Remarks = x.Item4
                        });
                    });

                }
                var validPhrases = (from d1 in lstHashlngId
                                    join d2 in context.Library_PhysicalForm.AsNoTracking()
                                    on d1.Item1 equals d2.PhraseHash_LowerCase
                                    select new { d1, d2.PhraseID_PhysicalForm }).Distinct().ToList();
                if (validPhrases.Any())
                {
                    var res = (from d1 in validPhrases
                               join d2 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.PhraseCategoryID == catid)
                               on d1.PhraseID_PhysicalForm equals d2.PhraseID
                               select new { d1.PhraseID_PhysicalForm, d2.QSID_ID }).Distinct().ToList();
                    var chkQsidCountValid = (from d1 in res
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID_PhysicalForm, d3.QSID, d1.QSID_ID }).Distinct().ToList();
                    validPhrases.ForEach(x =>
                    {
                        var noofActiveList = chkQsidCountValid.Count(y => y.PhraseID_PhysicalForm == x.PhraseID_PhysicalForm);
                        var ActiveList = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_PhysicalForm == x.PhraseID_PhysicalForm).Select(z => z.QSID).ToList());
                        var activeListID = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_PhysicalForm == x.PhraseID_PhysicalForm).Select(z => z.QSID_ID).ToList());
                        var linkVal = ConvertInfoToHyoerlinkVal(ActiveList, activeListID);
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.d1.Item3,
                            Phrase = x.d1.Item2,
                            PhraseID = (int)x.PhraseID_PhysicalForm,
                            Active_Lists = linkVal,
                            No_of_active_lists = noofActiveList,
                            PhraseHash = x.d1.Item1,
                            NeedToAddLibrary = false,
                            Remarks = x.d1.Item4
                        });
                    });
                }


            }
            listSubroots = new List<ISubroot>(lstHashFinal);
            NotifyPropertyChanged("comonSubroots_VM");
            uploadSubroots = Visibility.Collapsed;
            NotifyPropertyChanged("uploadSubroots");
        }
        private void BindThresholdBasis()
        {
            List<(string, string, string, string)> lstHashlngId = new List<(string, string, string, string)>();
            List<ISubroot> lstHashFinal = new List<ISubroot>();
            using (var context = new CRAModel())
            {
                var catid = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Threshold calculation basis").Select(y => y.PhraseCategoryID).FirstOrDefault();
                var engLngId = context.Library_Languages.AsNoTracking().Where(x => x.LanguageName == "English").Select(y => y.LanguageID).FirstOrDefault();

                for (int i = 0; i < FieldNameSubroot.Count(); i++)
                {

                    var fName = FieldNameSubroot[i];
                    var tabData = objCommon.DbaseQueryReturnTable("select distinct " + fName + " from [" + TableNameSubroot + "] where " + fName + " is not null and " + fName + " <> ''", CommonFilepath);
                    var replcedBy = (from d1 in tabData.AsEnumerable()
                                     join d2 in context.Map_ControlPhrase_Replacement.AsNoTracking()
                                     on d1.Field<string>(fName) equals d2.Orginal_Value
                                     join d3 in context.Library_Phrases.AsNoTracking()
                                     on d2.Replace_ControlPhrase_ID equals d3.PhraseID
                                     select new
                                     {
                                         orgPhrase = d1.Field<string>(fName).Trim(),
                                         RepWith = d3.Phrase,
                                     }).Distinct().ToList();
                    for (int j = 0; j < tabData.Rows.Count; j++)
                    {
                        var phrVal = tabData.Rows[j].Field<string>(fName).Trim();
                        var chkExt = replcedBy.Where(x => x.orgPhrase == phrVal).Select(y => y.RepWith).FirstOrDefault();
                        if (!string.IsNullOrEmpty(chkExt))
                        {
                            var phrValLower = chkExt.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(chkExt);
                            lstHashlngId.Add((phraseHash, chkExt, fName, phrVal + " Replaced By -" + chkExt));
                        }
                        else
                        {
                            var phraseVal = objCommon.RemoveWhitespaceCharacter(phrVal, StripCharacters);
                            var phrValLower = phraseVal.ToLower();
                            var phraseHash = objCommon.GenerateSHA256String(phrValLower);
                            lstHashlngId.Add((phraseHash, phraseVal, fName, ""));
                        }
                    }
                }

                var chkInLibSubroot = (from d1 in lstHashlngId
                                       join d2 in context.Library_Threshold_Calculation_Basis.AsNoTracking()
                                       on d1.Item1 equals d2.PhraseHash_LowerCase
                                       into tg
                                       from tcheck in tg.DefaultIfEmpty()
                                       where tcheck == null
                                       select d1).Distinct().ToList();

                if (chkInLibSubroot.Any())
                {
                    chkInLibSubroot.ForEach(x =>
                    {
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.Item3,
                            Phrase = x.Item2,
                            PhraseID = 0,
                            Active_Lists = new List<HyperLinkDataValue>(),
                            No_of_active_lists = 0,
                            PhraseHash = x.Item1,
                            NeedToAddLibrary = true,
                            Remarks = x.Item4
                        });
                    });

                }
                var validPhrases = (from d1 in lstHashlngId
                                    join d2 in context.Library_Threshold_Calculation_Basis.AsNoTracking()
                                    on d1.Item1 equals d2.PhraseHash_LowerCase
                                    select new { d1, d2.PhraseID_ThresholdBasis }).Distinct().ToList();
                if (validPhrases.Any())
                {
                    var res = (from d1 in validPhrases
                               join d2 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.PhraseCategoryID == catid)
                               on d1.PhraseID_ThresholdBasis equals d2.PhraseID
                               select new { d1.PhraseID_ThresholdBasis, d2.QSID_ID }).Distinct().ToList();
                    var chkQsidCountValid = (from d1 in res
                                             join d3 in context.Library_QSIDs.AsNoTracking()
                                             on d1.QSID_ID equals d3.QSID_ID
                                             select new { d1.PhraseID_ThresholdBasis, d3.QSID, d1.QSID_ID }).Distinct().ToList();
                    validPhrases.ForEach(x =>
                    {
                        var noofActiveList = chkQsidCountValid.Count(y => y.PhraseID_ThresholdBasis == x.PhraseID_ThresholdBasis);
                        var ActiveList = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_ThresholdBasis == x.PhraseID_ThresholdBasis).Select(z => z.QSID).ToList());
                        var activeListID = string.Join(",", chkQsidCountValid.Where(y => y.PhraseID_ThresholdBasis == x.PhraseID_ThresholdBasis).Select(z => z.QSID_ID).ToList());
                        var linkVal = ConvertInfoToHyoerlinkVal(ActiveList, activeListID);
                        lstHashFinal.Add(new ISubroot
                        {
                            FieldName = x.d1.Item3,
                            Phrase = x.d1.Item2,
                            PhraseID = (int)x.PhraseID_ThresholdBasis,
                            Active_Lists = linkVal,
                            No_of_active_lists = noofActiveList,
                            PhraseHash = x.d1.Item1,
                            NeedToAddLibrary = false,
                            Remarks = x.d1.Item4
                        });
                    });
                }


            }
            listSubroots = new List<ISubroot>(lstHashFinal);
            NotifyPropertyChanged("comonSubroots_VM");
            uploadSubroots = Visibility.Collapsed;
            NotifyPropertyChanged("uploadSubroots");
        }
        private ISubrootType _SelectedSubrootType { get; set; }
        public ISubrootType SelectedSubrootType
        {
            get => _SelectedSubrootType;
            set
            {
                if (Equals(_SelectedSubrootType, value))
                {
                    return;
                }

                _SelectedSubrootType = value;
                NotifyPropertyChanged("SelectedSubrootType");
            }
        }
        private void NotifyMeSubroot(PropertyChangedMessage<ISubroot> obj)
        {
            SelectedSubroot = obj.NewValue;
        }
        private void CallbackHyperlink(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "AddToLibrary")
            {
                using (var context = new CRAModel())
                {
                    var engLngId = context.Library_Languages.AsNoTracking().Where(x => x.LanguageName == "English").Select(y => y.LanguageID).FirstOrDefault();
                    var lstFinal = new List<Library_Phrases>();

                    var phrVal = SelectedSubroot.Phrase.Trim();
                    var phraseVal = objCommon.RemoveWhitespaceCharacter(phrVal, StripCharacters);
                    var phraseHashLower = objCommon.GenerateSHA256String(phraseVal.ToLower());
                    var phraseHash = objCommon.GenerateSHA256String(phraseVal);
                    var lngId = engLngId;
                    var phId = context.Library_Phrases.AsNoTracking().Count(x => x.PhraseHash == phraseHash && x.LanguageID == lngId);
                    if (phId == 0)
                    {
                        var lib = new Library_Phrases
                        {
                            LanguageID = lngId,
                            InternalOnly = false,
                            IsActive = true,
                            Phrase = phraseVal,
                            PhraseHash = phraseHash,
                            TimeStamp = objCommon.ESTTime(),
                        };
                        lstFinal.Add(lib);
                        _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstFinal);
                    }
                    var phraseId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phraseHash && x.LanguageID == lngId).Select(y => y.PhraseID).FirstOrDefault();

                    if (SelectedSubrootType.SubrootType == "Subroot")
                    {
                        var lstSFinal = new List<Library_SubRoots>();
                        var chkExist = context.Library_SubRoots.AsNoTracking().Where(x => x.PhraseHash_LowerCase == phraseHashLower).FirstOrDefault();
                        if (chkExist == null)
                        {
                            var libS = new Library_SubRoots
                            {
                                IsActive = true,
                                PhraseID_Subroot = phraseId,
                                PhraseHash_LowerCase = phraseHashLower,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstSFinal.Add(libS);
                            _objLibraryFunction_Service.BulkIns<Library_SubRoots>(lstSFinal);
                        }
                        else
                        {
                            _notifier.ShowError("Subroot already exists");
                            return;
                        }
                    }
                    if (SelectedSubrootType.SubrootType == "Expressed As (Phrase)")
                    {
                        var lstSFinal = new List<Library_ExpressedAs>();
                        var chkExist = context.Library_ExpressedAs.AsNoTracking().Where(x => x.PhraseHash_LowerCase == phraseHashLower).FirstOrDefault();
                        if (chkExist == null)
                        {
                            var libS = new Library_ExpressedAs
                            {
                                IsActive = true,
                                PhraseID_ExpressedAs = phraseId,
                                PhraseHash_LowerCase = phraseHashLower,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstSFinal.Add(libS);
                            _objLibraryFunction_Service.BulkIns<Library_ExpressedAs>(lstSFinal);
                        }
                        else
                        {
                            _notifier.ShowError("Expressed As (Phrase) already exists");
                            return;
                        }
                    }

                    if (SelectedSubrootType.SubrootType == "Physical Form (Phrase)")
                    {
                        var lstSFinal = new List<Library_PhysicalForm>();
                        var chkExist = context.Library_PhysicalForm.AsNoTracking().Where(x => x.PhraseHash_LowerCase == phraseHashLower).FirstOrDefault();
                        if (chkExist == null)
                        {
                            var libS = new Library_PhysicalForm
                            {
                                IsActive = true,
                                PhraseID_PhysicalForm = phraseId,
                                PhraseHash_LowerCase = phraseHashLower,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstSFinal.Add(libS);
                            _objLibraryFunction_Service.BulkIns<Library_PhysicalForm>(lstSFinal);
                        }
                        else
                        {
                            _notifier.ShowError("Physical Form (Phrase) already exists");
                            return;
                        }
                    }

                    if (SelectedSubrootType.SubrootType == "Threshold calculation basis")
                    {
                        var lstSFinal = new List<Library_Threshold_Calculation_Basis>();
                        var chkExist = context.Library_Threshold_Calculation_Basis.AsNoTracking().Where(x => x.PhraseHash_LowerCase == phraseHashLower).FirstOrDefault();
                        if (chkExist == null)
                        {
                            var libS = new Library_Threshold_Calculation_Basis
                            {
                                IsActive = true,
                                PhraseID_ThresholdBasis = phraseId,
                                PhraseHash_LowerCase = phraseHashLower,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstSFinal.Add(libS);
                            _objLibraryFunction_Service.BulkIns<Library_Threshold_Calculation_Basis>(lstSFinal);
                        }
                        else
                        {
                            _notifier.ShowError("Physical Form (Phrase) already exists");
                            return;
                        }
                    }

                    uploadSubroots = Visibility.Visible;
                    NotifyPropertyChanged("uploadSubroots");
                    Task.Run(() =>
                    {
                        switch (SelectedSubrootType.SubrootType)
                        {
                            case "Subroot":
                                BindSubroot();
                                break;
                            case "Expressed As (Phrase)":
                                BindExpressedAs();
                                break;
                            case "Physical Form (Phrase)":
                                BindPhysicalForm();
                                break;
                            case "Threshold calculation basis":
                                BindThresholdBasis();
                                break;
                        }
                    });
                }
            }
            else
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");
        }
        private List<CommonDataGridColumn> GetListGridColumnLibSubroots()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No_of_active_lists", ColBindingName = "No_of_active_lists", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Active_Lists", ColBindingName = "Active_Lists", ColType = "MultipleHyperLinkCol" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Nothing", ColType = "Button", ColumnWidth = "100", ContentName = "Add to Library", BackgroundColor = "#07689f", CommandParam = "AddToLibrary" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Remarks", ColBindingName = "Remarks", ColType = "Textbox" });
            return listColumnQsidDetail;
        }
        private List<HyperLinkDataValue> ConvertInfoToHyoerlinkVal(string qsids, string qsidIds)
        {
            string[] qsidArry = qsids.Split(',');
            string[] qsid_Id_Arry = qsidIds.Split(',');
            List<HyperLinkDataValue> listHyperLinkValues = new List<HyperLinkDataValue>();
            for (int index = 0; index < qsidArry.Length; index++)
            {
                listHyperLinkValues.Add(new HyperLinkDataValue() { DisplayValue = qsidArry[index].Trim(), NavigateData = qsid_Id_Arry[index].Trim() });
            }
            return listHyperLinkValues;
        }
    }
    public class ISubroot
    {
        public string FieldName { get; set; }
        public int PhraseID { get; set; }
        public string PhraseHash { get; set; }
        public string Phrase { get; set; }
        public int No_of_active_lists { get; set; }
        public List<HyperLinkDataValue> Active_Lists { get; set; }
        public bool NeedToAddLibrary { get; set; }
        public string Remarks { get; set; }
    }
    public class ISubrootType
    {
        public string SubrootType { get; set; }
    }
}
