﻿using System.Data;
using System.Windows.Input;


namespace MiscellaneousTab.ViewModel
{
    public class PopupNotesConsistency_VM : Base_ViewModel
    {
        #region Property Member
        public string MessageHeader { get; set; }
        public ICommand CommandCancel { get; private set; }
        public string lblHeading { get; set; }

        public StandardDataGrid_VM listNotePopup { get; set; }
        #endregion

        public PopupNotesConsistency_VM(string _messageHeader, DataTable dt)
        {
            lblHeading = _messageHeader;
            NotifyPropertyChanged("lblHeading");
            listNotePopup = new StandardDataGrid_VM(_messageHeader, dt);
        }
    }
}
