﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using System.Collections;
using System.ComponentModel;
using System.Linq.Dynamic;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public bool ChkAlphaOrderOmit { get; set; } = false;
        public ICommand CheckOmit { get; set; }
        public ICommand UpdateOmit { get; set; }
        private iTableName _SelectedCaseOmit { get; set; }
        public iTableName SelectedCaseOmit
        {
            get => _SelectedCaseOmit;
            set
            {
                if (Equals(_SelectedCaseOmit, value))
                {
                    return;
                }

                _SelectedCaseOmit = value;
            }
        }
        public bool _ChkExistingUniqueCriteria { get; set; }
        public bool ChkExistingUniqueCriteria
        {
            get => _ChkExistingUniqueCriteria;
            set
            {
                if (Equals(_ChkExistingUniqueCriteria, value))
                {
                    return;
                }

                _ChkExistingUniqueCriteria = value;
                NotifyPropertyChanged("ChkExistingUniqueCriteria");
                if (value == true)
                {
                    CheckExistingCriteria();
                }
            }
        }

        public bool ChkRemoveStrip { get; set; }
        public bool ChkSquashDup { get; set; }

        private void CheckExistingCriteria()
        {
            var lstDic = objCommon.DbaseQueryReturnTable("Select top 1 * from LIST_DICTIONARY", CommonFilepath);
            if (lstDic.Columns.Count == 0)
            {
                _notifier.ShowError("Table LIST_DICTIONARY not found in selected file");
                return;
            }
            if (string.IsNullOrEmpty(lstDic.Rows[0]["QSID"].ToString()))
            {
                _notifier.ShowError("QSID not present in table LIST_DICTIONARY of selected file");
                return;
            }
            ChkRemoveStrip = false;
            ChkSquashDup = false;
            ChkAlphaOrderOmit = false;
            SelectedCaseOmit = listCaseOmit.Where(x => x.TableName == "Keep Original").FirstOrDefault();
            NotifyPropertyChanged("ChkRemoveStrip");
            NotifyPropertyChanged("ChkSquashDup");
            NotifyPropertyChanged("ChkAlphaOrderOmit");
            NotifyPropertyChanged("SelectedCaseOmit");
            var getuniqueCriteria = objCommon.DbaseQueryReturnTableSql("select distinct FieldName from Map_Unique_Criteria a " +
                    " inner join Library_FieldNames b " +
                    " on a.FieldNameID = b.FieldNameID inner join Library_QSIDs c on a.qsid_id = c.QSID_ID " +
                    " where c.QSID = '" + lstDic.Rows[0]["QSID"].ToString() + "'", CommonListConn);
            if (getuniqueCriteria.Rows.Count == 0 || getuniqueCriteria.Columns.Count == 0)
            {
                _notifier.ShowError("Uniqueness criteria not yet defined for the selected list");
                return;
            }
            var listFlds = getuniqueCriteria.AsEnumerable().Select(y => y.Field<string>("FieldName").ToUpper()).ToList();
            var combineFields = string.Join(",", listFlds);

            var chkAllFieldExists = objCommon.DbaseQueryReturnTable("Select top 1 " + combineFields + "  from DATA", CommonFilepath);
            if (chkAllFieldExists.Columns.Count == 0)
            {
                _notifier.ShowError(combineFields + "Fields not present in table Data of selected file");
                return;
            }
            if (listFields.Count == 0)
            {
                SelectedTable = listTableName.Where(x => x.TableName == "DATA").FirstOrDefault();
                NotifyPropertyChanged("SelectedTable");

            }
            listFields.ToList().ForEach(x =>
            {
                x.IsSelected = false;
            });
            listFields.Where(y => listFlds.Contains(y.FieldName.ToUpper())).ToList().ForEach(x =>
             {
                 x.IsSelected = true;
             });
            BindLabel(SelectedTable);
        }
        public ICommand FlagDuplicateTmpRN { get; set; }
        private List<string> FieldNameOmit { get; set; }
        public StandardDataGrid_VM listOmit { get; set; }
        public Visibility uploadOmit { get; set; } = Visibility.Collapsed;
        private string TableNameOmit { get; set; }
        private void CheckOmitExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameOmit = TabName;
            var combineField = string.Empty;
            FieldNameOmit = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameOmit = "DATA";
                FieldNameOmit.Add("CAS");
                combineField = "CAS";
            }
            else
            {
                FieldNameOmit = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
                combineField = string.Join(",", FieldNameOmit);
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameOmit + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (FieldNameOmit.Contains("OMIT"))
            {
                _notifier.ShowError("OMIT is not the part of unique Criteria");
                return;
            }
            var ChkOmitField = objCommon.DbaseQueryReturnTable("select top 1 OMIT from [" + TableNameOmit + "]", CommonFilepath);
            if (ChkOmitField.Columns.Count == 0)
            {
                _notifier.ShowError("OMIT field not exist in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkDup = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameOmit + "] where omit is null or omit =''", CommonFilepath);

            chkDup = ApplyChecksonDataTable(chkDup);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + combineField + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            if (chkExistingCriteria.Any())
            {
                var dt = CreateDataTableFromDynamicQuery(combineField, chkExistingCriteria, true);
                listOmit = new StandardDataGrid_VM("Omit Duplicates", dt);
                NotifyPropertyChanged("listOmit");
            }
            else
            {
                listOmit = null;
                NotifyPropertyChanged("listOmit");
                _notifier.ShowSuccess("No Duplicate Found");
                return;
            }
        }
        private void UpdateOmitExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameOmit = TabName;
            var combineField = string.Empty;
            FieldNameOmit = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameOmit = "DATA";
                FieldNameOmit.Add("CAS");
                combineField = "CAS";
            }
            else
            {
                FieldNameOmit = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
                combineField = string.Join(",", FieldNameOmit);
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameOmit + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (FieldNameOmit.Contains("OMIT"))
            {
                _notifier.ShowError("OMIT is not the part of unique Criteria");
                return;
            }
            var ChkOmitField = objCommon.DbaseQueryReturnTable("select top 1 OMIT from [" + TableNameOmit + "]", CommonFilepath);
            if (ChkOmitField.Columns.Count == 0)
            {
                _notifier.ShowError("OMIT field not exist in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameOmit + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }

            var chkDup1 = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameOmit + "] where omit is null or omit =''", CommonFilepath);
            chkDup1 = ApplyChecksonDataTable(chkDup1);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup1, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + combineField + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            //var chkDup = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameOmit + "] where omit is null or omit ='' group by " + combineField + " having count(*) > 1", CommonFilepath);
            if (!chkExistingCriteria.Any())
            {
                _notifier.ShowSuccess("No Duplicate Found");
                return;
            }
            else
            {
                uploadOmit = Visibility.Visible;
                NotifyPropertyChanged("uploadOmit");

                var chkDup = CreateDataTableFromDynamicQuery(combineField, chkExistingCriteria, false);

                chkDup.Columns.Add("CommonCol");
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    string s = string.Join(", ", chkDup.Rows[i].ItemArray.Where(x => x.ToString() != string.Empty).ToList());
                    chkDup.Rows[i]["CommonCol"] = s;
                }
                chkDup.Columns.Add("GroupID");
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    chkDup.Rows[i]["GroupID"] = "D" + (i + 1);
                }
                Task.Run(() =>
                {
                    //string omitFields = string.Empty;
                    string oFields = string.Empty;
                    for (int i = 0; i < FieldNameOmit.Count(); i++)
                    {
                        //omitFields += "a." + FieldNameOmit[i] + " = b." + FieldNameOmit[i] + " and ";
                        oFields += "a." + FieldNameOmit[i] + ",";
                    }
                    //omitFields = omitFields.Substring(0, omitFields.LastIndexOf(" and "));
                    oFields = oFields.TrimEnd(',');
                    var allRec = objCommon.DbaseQueryReturnTable("select " + autoCol + ", " + oFields + " from [" + TableNameOmit + "] a where  omit is null or omit ='' ", CommonFilepath);
                    allRec = ApplyChecksonDataTable(allRec);
                    allRec.Columns.Add("CommonCol");
                    for (int i = 0; i < allRec.Rows.Count; i++)
                    {
                        string s = string.Join(", ", allRec.Rows[i].ItemArray.Skip(1).Where(x => x.ToString() != string.Empty).ToList());
                        allRec.Rows[i]["CommonCol"] = s;
                    }
                    var getCombine = (from d1 in chkDup.AsEnumerable()
                                      join d2 in allRec.AsEnumerable()
                                      on d1.Field<string>("CommonCol") equals d2.Field<string>("CommonCol")
                                      select new INewDatatable { Ident = d2.Field<Int32>(autoCol), GroupID = d1.Field<string>("GroupID") }).ToList();
                    var groupByAndSkipFirst = getCombine.GroupBy(s => s.GroupID).SelectMany(grp => grp.Skip(1));
                    var singleGroupCount = getCombine.GroupBy(s => s.GroupID).Where(z => z.Count() == 1);
                    var finalList = groupByAndSkipFirst.ToList();
                    //finalList.AddRange(singleGroupCount.ToList());
                    var newDt = objCommon.ConvertToDataTable<INewDatatable>(finalList);

                    List<string> listColParam = new List<string>();
                    List<string> columnList = new List<string>();
                    for (int i = 0; i < newDt.Columns.Count; i++)
                    {
                        var colName = newDt.Columns[i].ColumnName;
                        listColParam.Add("@" + colName);
                        columnList.Add(colName);
                    }

                    //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                    objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh ( ident Number, GroupID LongText )", CommonFilepath);
                    _objAccessVersion_Service.ExportAccessData(newDt, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                    objCommon.DbaseQueryReturnString("update " + TableNameOmit + " a inner join TMPDh b on a." + autoCol + " = b.Ident set a.Omit = '*'", CommonFilepath);
                    objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    uploadOmit = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadOmit");
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowSuccess("Access file updated successfully");
                    });
                    //if (isFileOpen)
                    //{
                    //    objCommon.OpenFile(CommonFilepath);
                    //}
                });

            }
        }
        private void FlagDuplicateTmpRNExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameOmit = TabName;
            var combineField = string.Empty;
            FieldNameOmit = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameOmit = "DATA";
                FieldNameOmit.Add("CAS");
                combineField = "CAS";
            }
            else
            {
                FieldNameOmit = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
                combineField = string.Join(",", FieldNameOmit);
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameOmit + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (FieldNameOmit.Contains("OMIT"))
            {
                _notifier.ShowError("OMIT is not the part of unique Criteria");
                return;
            }
            var ChkOmitField = objCommon.DbaseQueryReturnTable("select top 1 OMIT from [" + TableNameOmit + "]", CommonFilepath);
            if (ChkOmitField.Columns.Count == 0)
            {
                _notifier.ShowError("OMIT field not exist in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameOmit + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var chkDup1 = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameOmit + "] where omit is null or omit =''", CommonFilepath);
            chkDup1 = ApplyChecksonDataTable(chkDup1);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup1, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + combineField + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            //var chkDup = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameOmit + "] where omit is null or omit ='' group by " + combineField + " having count(*) > 1 order by " + combineField, CommonFilepath);
            if (!chkExistingCriteria.Any())
            {
                _notifier.ShowSuccess("No Duplicate Found");
                return;
            }
            else
            {
                uploadOmit = Visibility.Visible;
                NotifyPropertyChanged("uploadOmit");
                objCommon.DbaseQueryReturnTable("UPDATE [" + TableNameOmit + "] Set TMP_RN = null ", CommonFilepath);
                var chkDup = CreateDataTableFromDynamicQuery(combineField, chkExistingCriteria, false);
                chkDup.Columns.Add("CommonCol");
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    string s = string.Join(", ", chkDup.Rows[i].ItemArray.Where(x => x.ToString() != string.Empty).ToList());
                    chkDup.Rows[i]["CommonCol"] = s;
                }
                chkDup.Columns.Add("GroupID");
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    chkDup.Rows[i]["GroupID"] = "D" + (i + 1);
                }

                var maxLength = chkDup.AsEnumerable().Max(x => x.Field<string>("GroupID").Length);
                if (maxLength > 6)
                {
                    objCommon.DbaseQueryReturnString("Alter Table DATA Alter Column TMP_RN Text(" + maxLength + ")", CommonFilepath);
                    if (GetColumnsize(CommonFilepath) < maxLength)
                    {
                        uploadOmit = Visibility.Collapsed;
                        NotifyPropertyChanged("uploadOmit");
                        _notifier.ShowSuccess("Kindly alter Tmp_RN column length to " + maxLength);
                        return;
                    }
                }
                Task.Run(() =>
                {
                    //string omitFields = string.Empty;
                    string oFields = string.Empty;
                    for (int i = 0; i < FieldNameOmit.Count(); i++)
                    {
                        //omitFields += "a." + FieldNameOmit[i] + " = iif(len(b." + FieldNameOmit[i] + ") = 0, null,b." + FieldNameOmit[i] + ") and ";
                        oFields += "a." + FieldNameOmit[i] + ",";
                    }
                    //omitFields = omitFields.Substring(0, omitFields.LastIndexOf(" and "));
                    oFields = oFields.TrimEnd(',');
                    var allRec = objCommon.DbaseQueryReturnTable("select " + autoCol + ", " + oFields + " from [" + TableNameOmit + "]  a where  omit is null or omit =''", CommonFilepath);
                    allRec = ApplyChecksonDataTable(allRec);
                    allRec.Columns.Add("CommonCol");
                    for (int i = 0; i < allRec.Rows.Count; i++)
                    {
                        string s = string.Join(", ", allRec.Rows[i].ItemArray.Skip(1).Where(x => x.ToString() != string.Empty).ToList());
                        allRec.Rows[i]["CommonCol"] = s;
                    }
                    var getCombine = (from d1 in chkDup.AsEnumerable()
                                      join d2 in allRec.AsEnumerable()
                                      on d1.Field<string>("CommonCol") equals d2.Field<string>("CommonCol")
                                      select new INewDatatable { Ident = d2.Field<Int32>(autoCol), GroupID = d1.Field<string>("GroupID") }).ToList();
                    var newDt = objCommon.ConvertToDataTable<INewDatatable>(getCombine);
                    List<string> listColParam = new List<string>();
                    List<string> columnList = new List<string>();
                    for (int i = 0; i < newDt.Columns.Count; i++)
                    {
                        var colName = newDt.Columns[i].ColumnName;
                        listColParam.Add("@" + colName);
                        columnList.Add(colName);
                    }

                    //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                    objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh ( ident Number, GroupID LongText )", CommonFilepath);
                    _objAccessVersion_Service.ExportAccessData(newDt, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                    objCommon.DbaseQueryReturnString("update " + TableNameOmit + " a inner join TMPDh b on a." + autoCol + " = b.Ident set a.Tmp_RN = b.GroupID ", CommonFilepath);
                    objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                    uploadOmit = Visibility.Collapsed;
                    NotifyPropertyChanged("uploadOmit");
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowSuccess("Access file updated successfully");
                    });
                    //if (isFileOpen)
                    //{
                    //    objCommon.OpenFile(CommonFilepath);
                    //}
                });
            }
        }
        public int GetColumnsize(string dbaseConnString)
        {
            List<string> colList = new List<string>();

            string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source = " + dbaseConnString;
            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT Tmp_RN FROM DATA", conStr);
            DataTable dtSchema = adapter.FillSchema(new DataTable(), SchemaType.Source);
            if (dtSchema != null)
            {
                return dtSchema.Columns[0].MaxLength;
            }
            return 0;
        }
        private DataTable CreateDataTableFromDynamicQuery(string combineField, IQueryable chkExistingCriteria, bool rowCount)
        {
            var colName = combineField.Split(",").ToList();
            var chkDup = new DataTable();
            for (int i = 0; i < colName.Count; i++)
            {
                chkDup.Columns.Add(colName[i].ToString());
            }
            if (rowCount)
            {
                chkDup.Columns.Add("RowCount");
                chkDup.Columns.Add("DuplicateID");
            }
            int dupId = 1;
            foreach (var ct in chkExistingCriteria)
            {
                var nM = ct.GetType().GetProperty("Name").GetValue(ct, null);
                DataRow dr = chkDup.NewRow();
                for (int i = 0; i < colName.Count; i++)
                {
                    var colVal = nM.GetType().GetProperty(colName[i].ToString()).GetValue(nM, null);
                    dr[colName[i].ToString()] = colVal;
                }
                if (rowCount)
                {
                    var tot = ct.GetType().GetProperty("Total").GetValue(ct, null);
                    dr["RowCount"] = tot;
                    dr["DuplicateID"] = dupId;
                    dupId += 1;
                }
                chkDup.Rows.Add(dr);
            }
            return chkDup;
        }
        public IList BindListFromTable(DataTable dt, Type listType)
        {
            var obj = MyTypeBuilder.CreateNewObject(dt.Columns);
            listType = typeof(List<>).MakeGenericType(new[] { obj });
            System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
            var fields = obj.GetProperties();
            foreach (DataRow dr in dt.Rows)
            {
                var ob = Activator.CreateInstance(obj);

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
                        {
                            object value = (dr[dc.ColumnName] == DBNull.Value) ? null : dr[dc.ColumnName];
                            if (value != null)
                            {
                                //if (fieldInfo.PropertyType == typeof(ListCheckBoxType_ViewModel) || fieldInfo.PropertyType == typeof(bool))
                                //{
                                //    var objVM = new ListCheckBoxType_ViewModel();
                                //    objVM.ID = (bool)value == true ? 1 : 0;
                                //    objVM.Value = (bool)value;
                                //    fieldInfo.SetValue(ob, objVM);
                                //}
                                //else
                                //{
                                var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
                                var result = converter.ConvertFrom(value.ToString());
                                fieldInfo.SetValue(ob, result);
                                //}
                            }
                            else
                            {
                                if (fieldInfo.PropertyType == typeof(DateTime))
                                {
                                    fieldInfo.SetValue(ob, (DateTime?)null);
                                }
                                else
                                {
                                    //if (fieldInfo.PropertyType == typeof(ListCheckBoxType_ViewModel) || fieldInfo.PropertyType == typeof(bool))
                                    //{
                                    //    var objVM = new ListCheckBoxType_ViewModel();
                                    //    objVM.ID = 0;
                                    //    objVM.Value = false;
                                    //    fieldInfo.SetValue(ob, objVM);
                                    //}
                                    //else
                                    //{
                                    fieldInfo.SetValue(ob, value);
                                    //}
                                }

                            }
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }
        private DataTable ApplyChecksonDataTable(DataTable chkDup)
        {
            List<IStripChar> stripChar = new List<IStripChar>();
            List<string> selectedSquashChar = new List<string>();
            using (var context = new CRAModel())
            {
                stripChar = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                selectedSquashChar = context.Library_SquashCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.Character.Trim()) == false).Select(x => x.Character.Trim()).ToList();
            }

            if (SelectedCaseOmit.TableName == "Upper")
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        chkDup.Rows[i][chkDup.Columns[j].ColumnName] = chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString().ToUpper();
                    }
                }
            }
            if (SelectedCaseOmit.TableName == "Lower")
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        chkDup.Rows[i][chkDup.Columns[j].ColumnName] = chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString().ToLower();
                    }
                }
            }
            if (ChkRemoveStrip == true)
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        var res = objCommon.RemoveWhitespaceCharacter(chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString(), stripChar);
                        if (res != chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString())
                        {
                            chkDup.Rows[i][chkDup.Columns[j].ColumnName] = res;
                        }
                    }
                }
            }
            if (ChkSquashDup == true)
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {
                        var res = SquashChar(chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString(), selectedSquashChar, ChkAlphaOrderOmit);
                        if (res != chkDup.Rows[i][chkDup.Columns[j].ColumnName].ToString())
                        {
                            chkDup.Rows[i][chkDup.Columns[j].ColumnName] = res;
                        }
                    }
                }
            }

            return chkDup;
        }
    }
    public class INewDatatable
    {
        public Int32 Ident { get; set; }
        public string GroupID { get; set; }
    }
    public class INewDatatableOmit
    {
        public Int32 Ident { get; set; }
        public string GroupID { get; set; }
        public string OmitRec { get; set; }
    }
}
