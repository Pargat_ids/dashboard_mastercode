﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;
using System.Text;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public Visibility uploadFieldSquash { get; set; } = Visibility.Collapsed;
        public ICommand BtnFieldSquash { get; set; }
        public ICommand BtnUpdateAccessFieldSquash { get; set; }
        public bool ChkAlphaOrder { get; set; } = false;
        private string FieldNameSquash { get; set; }
        public ICommand CommandAddNewStripChar { get; set; }
        public string newStrip { get; set; }
        private string TableNameFieldSquash { get; set; }
        public CommonDataGrid_ViewModel<IFieldSquash> comonFieldSquash_VM { get; set; }
        private List<IFieldSquash> _listFieldSquash { get; set; } = new List<IFieldSquash>();
        public List<IFieldSquash> listFieldSquash
        {
            get { return _listFieldSquash; }
            set
            {
                if (_listFieldSquash != value)
                {
                    _listFieldSquash = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IFieldSquash>>>(new PropertyChangedMessage<List<IFieldSquash>>(null, _listFieldSquash, "Default List"));
                }
            }
        }
        public string TxtTruncateUpto { get; set; }
        public List<CommonDataGridColumn> GetListGridColumnFieldSquash()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "AutoNumberCol", ColBindingName = "AutoNumberCol", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Original_Field", ColBindingName = "Original_Field", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LowerCased_Strip", ColBindingName = "LowerCased_Strip", ColType = "Textbox", ColumnWidth = "300" });
            return listColumnQsidDetail;
        }
        public void BtnFieldSquashExecute(Object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameFieldSquash = TabName;
            if (TabName.ToString() == "Default")
            {
                TableNameFieldSquash = "DATA";
                FieldNameSquash = "PREF";
            }
            else
            {
                FieldNameSquash = listFields.Where(x => x.IsSelected).Select(x => x.FieldName.ToUpper()).FirstOrDefault();
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameFieldSquash + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + "," + FieldNameSquash + " from [" + TableNameFieldSquash + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(autoCol + "," + FieldNameSquash + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }
            uploadFieldSquash = Visibility.Visible;
            NotifyPropertyChanged("uploadFieldSquash");

            Task.Run(() =>
            {
                var lst = CalculateSquash(dtRaw, autoCol);
                listFieldSquash = new List<IFieldSquash>(lst);
                NotifyPropertyChanged("comonFieldSquash_VM");
                uploadFieldSquash = Visibility.Collapsed;
                NotifyPropertyChanged("uploadFieldSquash");
            });

        }
        private List<IFieldSquash> CalculateSquash(DataTable dtRaw, string AutoCol)
        {
            List<IFieldSquash> lst = new List<IFieldSquash>();
            var selectedSquashChar = listSquashChar.Where(x => x.IsSelected == true && string.IsNullOrEmpty(x.SquashCharacter.Trim()) == false).Select(x => x.SquashCharacter.Trim()).ToList();
            var truncateUpto = TxtTruncateUpto == string.Empty ? 0 : Convert.ToInt32(TxtTruncateUpto);
            dtRaw.AsEnumerable().ToList().ForEach(x =>
            {
                var actualValue = x.Field<dynamic>(FieldNameSquash) == null ? string.Empty : x.Field<string>(FieldNameSquash).Trim();
                if (actualValue != string.Empty)
                {
                    switch (SelectedCase.TableName)
                    {
                        case "Upper":
                            actualValue = actualValue.ToUpper();
                            break;
                        case "Lower":
                            actualValue = actualValue.ToLower();
                            break;
                    }
                    var resRemwhite = objCommon.RemoveWhitespaceCharacter(actualValue, StripCharacters);
                    var res = SquashChar(resRemwhite, selectedSquashChar, ChkAlphaOrder);
                    if (truncateUpto != 0 && res.Length > truncateUpto)
                    {
                        res = res.Substring(0, truncateUpto);
                    }
                    
                    lst.Add(new IFieldSquash { AutoNumberCol = Convert.ToInt32(x.Field<dynamic>(AutoCol)), Original_Field = actualValue, LowerCased_Strip = res });
                }
            });
            return lst;

        }

        private void BtnUpdateAccessFieldSquashExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameFieldSquash = TabName;
            if (TabName.ToString() == "Default")
            {
                TableNameFieldSquash = "DATA";
                FieldNameSquash = "PREF";
            }
            else
            {
                FieldNameSquash = listFields.Where(x => x.IsSelected).Select(x => x.FieldName.ToUpper()).FirstOrDefault();
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameFieldSquash + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + ", " + FieldNameSquash + " from [" + TableNameFieldSquash + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(autoCol + "," + FieldNameSquash + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }
            uploadFieldSquash = Visibility.Visible;
            NotifyPropertyChanged("uploadFieldSquash");
            Task.Run(() =>
            {
                var lst = CalculateSquash(dtRaw, autoCol);

                // check if field already Exists.
                var chkRn = objCommon.DbaseQueryReturnTable("select top 1  " + FieldNameSquash + "_Squash  from [" + TableNameFieldSquash + "]", CommonFilepath);
                if (chkRn.Columns.Count == 0)
                {
                    objCommon.DbaseQueryReturnString("alter Table [" + TableNameFieldSquash + "] ADD COLUMN  " + FieldNameSquash + "_Squash LongText ", CommonFilepath);
                }
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                listColParam.Add("@" + autoCol);
                columnList.Add(autoCol);
                listColParam.Add("@Original_Field");
                columnList.Add("Original_Field");
                listColParam.Add("@LowerCased_Strip");
                columnList.Add("LowerCased_Strip");
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (" + autoCol + " Number,Original_Field LongText, LowerCased_Strip LongText)", CommonFilepath);
                var lstDt = new DataTable();
                lstDt.Columns.Add(autoCol, typeof(Int32));
                lstDt.Columns.Add("Original_Field", typeof(string));
                lstDt.Columns.Add("LowerCased_Strip", typeof(string));
                lst.ForEach(x =>
                {
                    lstDt.Rows.Add(x.AutoNumberCol, x.Original_Field, x.LowerCased_Strip);
                });
                //var lstDt = objCommon.ConvertToDataTable<IFieldSquash>(lst);
                _objAccessVersion_Service.ExportAccessData(lstDt, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnString("update [" + TableNameFieldSquash + "] set " + FieldNameSquash + "_Squash = ''", CommonFilepath);
                objCommon.DbaseQueryReturnString("update [" + TableNameFieldSquash + "] a inner join TMPDh b on a." + autoCol + " = b." + autoCol + " set a." + FieldNameSquash + "_Squash = b.LowerCased_Strip", CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);

                listFieldSquash = new List<IFieldSquash>(lst);
                NotifyPropertyChanged("comonFieldSquash_VM");
                uploadFieldSquash = Visibility.Collapsed;
                NotifyPropertyChanged("uploadFieldSquash");
                _notifier.ShowSuccess("Access file updated successfully");
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            });
        }
        public string SquashChar(string str, List<string> selectedSquashChar, bool ChkAlphaOrder)
        {
            StringBuilder strToconvert = new StringBuilder(str);
            selectedSquashChar.ForEach(x =>
            {
                strToconvert.Replace(x, string.Empty);
            });
            if (ChkAlphaOrder)
            {
                var splitWord = strToconvert.ToString().Split(' ');
                var sortArray = splitWord.OrderBy(d => d.Trim()).ToArray();
                return  string.Join(string.Empty, sortArray).Trim();
            }
            else
            {
                return strToconvert.Replace(" ", "").ToString();
            }
        }
        public void CommandAddNewStripCharExecute(object o)
        {
            if (listSquashChar.Count(x => x.SquashCharacter.ToUpper() == newStrip.ToUpper()) == 0)
            {
                listSquashChar.Add(new MapStripCharacters { IsSelected = false, SquashCharacter = newStrip });
            }
        }
    }
    public class IFieldSquash
    {
        public Int32 AutoNumberCol { get; set; }
        public string Original_Field { get; set; }
        public string LowerCased_Strip { get; set; }
    }
}
