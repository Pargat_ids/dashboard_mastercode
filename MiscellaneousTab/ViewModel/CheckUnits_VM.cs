﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        private string TableNameCheckUnits { get; set; }
        private List<string> FieldNameCheckUnits { get; set; }
        public CommonDataGrid_ViewModel<ICheckUnits> comonCheckUnits_VM { get; set; }
        public Visibility uploadCheckUnits { get; set; } = Visibility.Collapsed;
        public string DefaultLableCheckUnits { get; set; }
        public ICommand CheckUnits { get; set; }
        private List<ICheckUnits> _listCheckUnits { get; set; } = new List<ICheckUnits>();
        public List<ICheckUnits> listCheckUnits
        {
            get { return _listCheckUnits; }
            set
            {
                if (_listCheckUnits != value)
                {
                    _listCheckUnits = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ICheckUnits>>>(new PropertyChangedMessage<List<ICheckUnits>>(null, _listCheckUnits, "Default List"));
                }
            }
        }
        private bool CheckUnitsCanExecute(object o)
        {
            return true;
        }
        private void CheckUnitsExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameCheckUnits = TabName;
            var combineField = string.Empty;
            FieldNameCheckUnits = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameCheckUnits = "DATA";
                using (var context = new CRAModel())
                {
                    var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                    var qsidid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == lstDic).Select(y => y.QSID_ID).FirstOrDefault();
                    var mapVirtualFieldid = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && x.UnitFieldNameID != null && x.UnitID != null).Select(y => (int)y.UnitFieldNameID).ToList();
                    var exceptVirtual = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidid && x.IsUnit == true && !mapVirtualFieldid.Contains((int)x.FieldNameID)).ToList();
                    var mapqsidFieldid = (from d1 in exceptVirtual
                                          join d2 in context.Library_FieldNames.AsNoTracking()
                                          on d1.FieldNameID equals d2.FieldNameID
                                          select d2.FieldName).ToList();
                    FieldNameCheckUnits.AddRange(mapqsidFieldid);
                    combineField = string.Join(",", mapqsidFieldid);
                    if (combineField == string.Empty)
                    {
                        _notifier.ShowError("No unit columnName found");
                        return;
                    }
                }
            }
            else
            {
                FieldNameCheckUnits = listFields.Where(x => x.IsSelected).Select(x => x.FieldName.ToUpper()).ToList();
            }

            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameCheckUnits + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            uploadCheckUnits = Visibility.Visible;
            NotifyPropertyChanged("uploadCheckUnits");
            Task.Run(() =>
            {
                BindCheckUnits();
            });
        }
        private void BindCheckUnits()
        {
            List<ICheckUnits> lstHashFinal = new List<ICheckUnits>();
            using (var context = new CRAModel())
            {
                var libUnit = context.Library_Units.AsNoTracking().ToList();
                for (int i = 0; i < FieldNameCheckUnits.Count(); i++)
                {

                    var fName = FieldNameCheckUnits[i];
                    var tabData = objCommon.DbaseQueryReturnTable("select distinct " + fName + " from [" + TableNameCheckUnits + "] where " + fName + " is not null and " + fName + " <> ''", CommonFilepath);
                    var replcedBy = (from d1 in tabData.AsEnumerable()
                                     join d2 in context.Map_Unit_Replacement.AsNoTracking()
                                     on d1.Field<string>(fName) equals d2.Orginal_Value
                                     join d3 in context.Library_Units.AsNoTracking()
                                     on d2.Replace_Unit_ID equals d3.UnitID
                                     select new
                                     {
                                         orgUnits = d1.Field<string>(fName).Trim(),
                                         RepWith = d3.Unit,
                                     }).Distinct().ToList();
                    for (int j = 0; j < tabData.Rows.Count; j++)
                    {
                        var phrVal = tabData.Rows[j].Field<string>(fName).Trim();
                        var chkExt = replcedBy.Where(x => x.orgUnits == phrVal).Select(y => y.RepWith).FirstOrDefault();
                        if (lstHashFinal.Count(x => x.Units == phrVal && x.FieldName == fName) == 0)
                        {
                            lstHashFinal.Add(new ICheckUnits
                            {
                                FieldName = fName,
                                Units = phrVal,
                                NeedToAddLibrary = false,
                                Remarks = !string.IsNullOrEmpty(chkExt) ? phrVal + " Replaced By -" + chkExt : "",
                            });
                        }
                    }
                }

                lstHashFinal.ForEach(x =>
                {
                    var extInLib = libUnit.Where(xx => xx.Unit == x.Units).FirstOrDefault();
                    if (extInLib == null)
                    {
                        x.NeedToAddLibrary = true;
                             x.Remarks = x.Remarks == string.Empty ? "Need to add in Library" : x.Remarks + ", Need to add in Library";
                    }
                });
            }
            lstHashFinal = lstHashFinal.OrderByDescending(x => x.NeedToAddLibrary).ToList();
            listCheckUnits = new List<ICheckUnits>(lstHashFinal);
            NotifyPropertyChanged("comonCheckUnits_VM");
            uploadCheckUnits = Visibility.Collapsed;
            NotifyPropertyChanged("uploadCheckUnits");
        }
        private List<CommonDataGridColumn> GetListGridColumnCheckUnits()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Units", ColBindingName = "Units", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Remarks", ColBindingName = "Remarks", ColType = "Textbox", ColumnWidth = "500" });
            return listColumnQsidDetail;
        }
    }

    public class ICheckUnits
    {
        public string FieldName { get; set; }
        public string Units { get; set; }
        public bool NeedToAddLibrary { get; set; }
        public string Remarks { get; set; }
    }
}
