﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public Visibility uploadNameAssignmentVis { get; set; } = Visibility.Collapsed;
        public ICommand BtnNameAssignment { get; set; }
        public ICommand BtnNameAssignmentUpdateAll { get; set; }
        public ICommand BtnNameAssignmentUpdateBlank { get; set; }
        private string FieldNameAssignment { get; set; }
        public string DefaultLableNameAssignment { get; set; }
        private List<INameAssignment> _listNameAssignment { get; set; } = new List<INameAssignment>();
        public List<INameAssignment> listNameAssignment
        {
            get { return _listNameAssignment; }
            set
            {
                if (_listNameAssignment != value)
                {
                    _listNameAssignment = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<INameAssignment>>>(new PropertyChangedMessage<List<INameAssignment>>(null, _listNameAssignment, "Default List"));
                }
            }
        }
        public CommonDataGrid_ViewModel<INameAssignment> comonNameAssignment_VM { get; set; }

        private string casField = string.Empty;
        private string PrefField = string.Empty;
        private DataTable dtNameAss = new DataTable();
        private List<INameAssignment> lstNA = new List<INameAssignment>();

        private void BtnNameAssignmentExecute(object o)
        {
            var err = IntialChecks();
            if (err != string.Empty)
            {
                _notifier.ShowError(err);
                return;
            }
            uploadNameAssignmentVis = Visibility.Visible;
            NotifyPropertyChanged("uploadNameAssignmentVis");
            Task.Run(() =>
            {
                lstNA = new List<INameAssignment>();
                RunIntialValues();
                listNameAssignment = new List<INameAssignment>(lstNA);
                NotifyPropertyChanged("comonNameAssignment_VM");
                uploadNameAssignmentVis = Visibility.Collapsed;
                NotifyPropertyChanged("uploadNameAssignmentVis");
            });

        }

        private void BtnNameAssignmentUpdateBlankExecute(object o)
        {
            var err = IntialChecks();
            if (err != string.Empty)
            {
                _notifier.ShowError(err);
                return;
            }
            uploadNameAssignmentVis = Visibility.Visible;
            NotifyPropertyChanged("uploadNameAssignmentVis");
            Task.Run(() =>
            {
                if (!lstNA.Any())
                {
                    RunIntialValues();
                }
                var chgVl = lstNA.Where(x => x.IsNameField_Empty.Value == true && x.IsPrimaryName_Available.Value == true).Select(y =>
                new IupdateRecords
                {
                    RN = y.RN,
                    PrimaryName = y.PrimaryName
                }).ToList();
                UpdateRecords(chgVl);
            });
        }
        private void BtnNameAssignmentUpdateAllExecute(object o)
        {
            var err = IntialChecks();
            if (err != string.Empty)
            {
                _notifier.ShowError(err);
                return;
            }
            uploadNameAssignmentVis = Visibility.Visible;
            NotifyPropertyChanged("uploadNameAssignmentVis");
            Task.Run(() =>
            {
                if (!lstNA.Any())
                {
                    RunIntialValues();
                }
                var chgVl = lstNA.Where(x => x.IsPrimaryName_Available.Value == true).Select(y =>
                new IupdateRecords
                {
                    RN = y.RN,
                    PrimaryName = y.PrimaryName
                }).ToList();
                UpdateRecords(chgVl);
            });
        }
        private List<CommonDataGridColumn> GetListGridColumnNameAssignment()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS_Field", ColBindingName = "CAS_Field", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Name_Field", ColBindingName = "Name_Field", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PrimaryName", ColBindingName = "PrimaryName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsNameField_Empty", ColBindingName = "IsNameField_Empty", ColType = "Checkbox", ColumnWidth = "120" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsPrimaryName_Available", ColBindingName = "IsPrimaryName_Available", ColType = "Checkbox", ColumnWidth = "120" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsNameField_EqualTo_PrimaryName", ColBindingName = "IsNameField_EqualTo_PrimaryName", ColType = "Checkbox", ColumnWidth = "120" });

            return listColumnQsidDetail;
        }

        private string IntialChecks()
        {
            if (CommonFilepath == string.Empty)
            {
                return "First choose access file to continue";
            }
            if (TabName == null)
            {
                return "first choose the TableName to continue";
            }
            
            var tableName = TabName;
            FieldNameAssignment = string.Empty;
            casField = string.Empty;
            PrefField = string.Empty;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
                FieldNameAssignment = "CAS,PREF";
                casField = "CAS";
                PrefField = "PREF";
            }
            else
            {
                casField = newListFieldNameAss[0].SelectedFieldName == null ? string.Empty : newListFieldNameAss[0].SelectedFieldName.Type;
                PrefField = newListFieldNameAss[1].SelectedFieldName == null ? string.Empty : newListFieldNameAss[1].SelectedFieldName.Type;
                FieldNameAssignment = casField + "," + PrefField;
            }
            dtNameAss = objCommon.DbaseQueryReturnTable("select RN, " + FieldNameAssignment + " from [" + tableName + "]", CommonFilepath);
            if (dtNameAss.Columns.Count == 0)
            {
                return "RN," + FieldNameAssignment + " - fields not found in selected file!";
            }
            if (dtNameAss == null || dtNameAss.Rows.Count == 0)
            {
                return "No row found in selected file!";

            }
            var chkRNDuplicate = dtNameAss.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                return "Found duplicate RN in this file, first fix them";

            }
            if (casField == string.Empty || PrefField == string.Empty)
            {
                return "Choose FieldType of selected fields";
            }
            return string.Empty;
        }
        private void RunIntialValues()
        {
            //using (var context = new CRAModel())
            //{
            //var mapNames = (from d1 in dtNameAss.AsEnumerable()
            //                join d2 in context.Library_CAS.AsNoTracking()
            //                on d1.Field<string>(casField) equals d2.CAS
            //                select new { d2.CAS, d2.CASID }).Distinct().ToList();
            //IQueryable<Map_PreferredSubstanceName> mapPrefSub = context.Map_PreferredSubstanceName.AsNoTracking();
            //var mapPref = (from d1 in mapNames
            //               join d2 in mapPrefSub
            //               on d1.CASID equals d2.CASID into tg
            //               from tcheck in tg.DefaultIfEmpty()
            //               select new { d1.CAS, d1.CASID, ChemicalNameID = tcheck == null ? 0 : tcheck.ChemicalNameID }).ToList();
            //IQueryable<Library_ChemicalNames> libChem = context.Library_ChemicalNames.AsNoTracking();
            //var mapChem = (from d1 in mapPref
            //               join d2 in libChem
            //               on d1.ChemicalNameID equals d2.ChemicalNameID
            //               into tg
            //               from tcheck in tg.DefaultIfEmpty()
            //               select new { d1.CASID, d1.CAS, d1.ChemicalNameID, ChemicalName = tcheck == null ? "" : tcheck.ChemicalName }).ToList();
            var casNames = string.Join(",", dtNameAss.AsEnumerable().Select(y => "'"  + y.Field<string>(casField) + "'").Distinct().ToList());
            var mapChem = objCommon.DbaseQueryReturnTableSql("select a.CASID, a.CAS, b.ChemicalNameID, c.ChemicalName from library_cas a left join Map_PreferredSubstanceName b " +
                " on a.CASID = b.CASID left join Library_ChemicalNames c on b.ChemicalNameID = c.ChemicalNameID where a.CAS in (" + casNames + ")", CommonListConn);
            dtNameAss.AsEnumerable().ToList().ForEach(x =>
            {
                var cas = x.Field<string>(casField);
                var prefFld = x.Field<string>(PrefField);
                var chk = mapChem.AsEnumerable().Where(xx => xx.Field<string>("CAS") == cas).Select(y => y.Field<string>("ChemicalName")).FirstOrDefault();
                lstNA.Add(new INameAssignment
                {
                    RN = Convert.ToInt32(x.Field<dynamic>("RN")),
                    CAS_Field = cas,
                    Name_Field = prefFld,
                    IsNameField_Empty = prefFld == null || prefFld == string.Empty ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                    IsPrimaryName_Available = chk == null || chk == string.Empty ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault(),
                    IsNameField_EqualTo_PrimaryName = chk == null || chk == string.Empty || prefFld != chk ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault(),
                    PrimaryName = chk
                });
            });
            //}
        }

        private void UpdateRecords(List<IupdateRecords> chgVl)
        {
            if (chgVl.Count > 0)
            {
                var dtNA = new DataTable();
                dtNA.Columns.Add("RN", typeof(Int32));
                dtNA.Columns.Add(PrefField, typeof(string));
                chgVl.ForEach(x =>
                {
                    dtNA.Rows.Add(x.RN, x.PrimaryName);
                });
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                var queryString = string.Empty;
                var updateQuery = string.Empty;
                listColParam.Add("@RN");
                listColParam.Add("@" + PrefField);
                columnList.Add("RN");
                columnList.Add(PrefField);
                queryString += "[" + PrefField + "] LongText,";
                updateQuery += "a." + PrefField + " = iif(len(b." + PrefField + ") = 0, null,b." + PrefField + "),";

                queryString = queryString.TrimEnd(',');
                updateQuery = updateQuery.TrimEnd(',');

                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
                _objAccessVersion_Service.ExportAccessData(dtNA, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnString("update DATA a inner join TMPDh b on a.RN = b.RN set " + updateQuery, CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                lstNA = new List<INameAssignment>();
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("Record Updated successfully");
                });
            }
            listNameAssignment = new List<INameAssignment>(lstNA);
            NotifyPropertyChanged("comonNameAssignment_VM");
            uploadNameAssignmentVis = Visibility.Collapsed;
            NotifyPropertyChanged("uploadNameAssignmentVis");
        }
    }
    public class INameAssignment
    {
        public Int32 RN { get; set; }
        public string CAS_Field { get; set; }
        public string Name_Field { get; set; }
        public string PrimaryName { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsNameField_Empty { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsPrimaryName_Available { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsNameField_EqualTo_PrimaryName { get; set; }
    }

    public class IupdateRecords
    {
        public Int32 RN { get; set; }
        public string PrimaryName { get; set; }
    }
}
