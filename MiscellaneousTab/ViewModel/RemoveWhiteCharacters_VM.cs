﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        private List<IInvalidChar> _listInvalid { get; set; } = new List<IInvalidChar>();
        public List<IInvalidChar> listInvalid
        {
            get { return _listInvalid; }
            set
            {
                if (_listInvalid != value)
                {
                    _listInvalid = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IInvalidChar>>>(new PropertyChangedMessage<List<IInvalidChar>>(null, _listInvalid, "Default List"));
                }
            }
        }

        public Visibility uploadRemoveWhiteCharacters { get; set; } = Visibility.Collapsed;
        private void RemoveWhiteCharactersExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            List<string> fieldName1 = new List<string>();
            var combineField = string.Empty;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";

                var dtTrans = objCommon.DbaseQueryReturnTable("select * from [" + tableName + "]", CommonFilepath);
                for (int i = 0; i < dtTrans.Columns.Count; i++)
                {
                    fieldName1.Add(dtTrans.Columns[i].ColumnName.ToString());
                }
            }
            else
            {
                fieldName1 = listFields.Where(x => x.IsSelected).Select(x => x.FieldName.ToUpper()).ToList();
                
            }
            if (!fieldName1.Contains("RN"))
            {
                fieldName1.Add("RN");
                //_notifier.ShowError("fields RN Should be in selected fieldnames");
                //return;
            }
            combineField = string.Join(",", fieldName1);
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate RN in this file, first fix them");
                return;
            }

            // Add columns in dtRaw
            uploadRemoveWhiteCharacters= Visibility.Visible;
            NotifyPropertyChanged("uploadRemoveWhiteCharacters");
            var newdtRaw = dtRaw.Clone();
            List<IStripChar> StripCharacters = new List<IStripChar>();
            using (var context = new CRAModel())
            {
                StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
            }
            // removewhitechractersNow
            List<IInvalidChar> lstInvalid = new List<IInvalidChar>();
            for (int i = 0; i < dtRaw.Rows.Count; i++)
            {
                var newRow = dtRaw.NewRow();
                for (int j = 0; j < dtRaw.Columns.Count; j++)
                {
                    var oldValue = dtRaw.Rows[i][j].ToString();
                    var newValue = objCommon.RemoveWhitespaceCharacterNew(oldValue, StripCharacters);
                    newRow[j] = newValue.Item1.Trim().Replace("  "," ").Trim();
                    if (oldValue != newRow[j].ToString())
                    {
                        lstInvalid.Add(new IInvalidChar { FieldName = dtRaw.Columns[j].ColumnName, OldValue = dtRaw.Rows[i][j].ToString(), NewValue = newRow[j].ToString(), RN = Convert.ToInt32(dtRaw.Rows[i]["RN"]), InvalidCharacter = newValue.Item2 == "" ? "Double Space" : newValue.Item2 });
                    }
                }
                newdtRaw.Rows.Add(newRow.ItemArray);
            }
            var queryString = string.Empty;
            var updateQuery = string.Empty;
            List<string> listColParam = new List<string>();
            List<string> columnList = new List<string>();
            listColParam.Add("@RN");
            columnList.Add("RN");
            for (int i = 0; i < fieldName1.Count();i++)
            {
                if(fieldName1[i] != "RN" && fieldName1[i].ToUpper() != "IDENT" && fieldName1[i].ToUpper() != "ID")
                {
                    queryString += "[" + fieldName1[i] + "] LongText,";
                    listColParam.Add("@" + fieldName1[i]);
                    columnList.Add(fieldName1[i]);
                    updateQuery += "a." + fieldName1[i] + " = iif(len(b." + fieldName1[i] + ") = 0, null,b." + fieldName1[i] + "),";
                }
            }
            queryString = queryString.TrimEnd(',');
            updateQuery = updateQuery.TrimEnd(',');
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
            _objAccessVersion_Service.ExportAccessData(newdtRaw, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
            objCommon.DbaseQueryReturnString("update [" + tableName + "] a inner join TMPDh b on a.RN = b.RN set " + updateQuery, CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            listInvalid = new List<IInvalidChar>(lstInvalid);
            NotifyPropertyChanged("comonInvalid_VM");
            uploadRemoveWhiteCharacters = Visibility.Collapsed;
            NotifyPropertyChanged("uploadRemoveWhiteCharacters");
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}
        }
        private List<CommonDataGridColumn> GetListGridColumnInvalid()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "InvalidCharacter", ColBindingName = "InvalidCharacter", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RN", ColBindingName = "RN", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OldValue", ColBindingName = "OldValue", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "NewValue", ColBindingName = "NewValue", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }
    }

    public class IInvalidChar
    {
        public string InvalidCharacter { get; set; }
        public int RN { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
