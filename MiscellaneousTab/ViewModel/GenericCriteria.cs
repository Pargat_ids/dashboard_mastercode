﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CRA_DataAccess.ViewModel;
using System.Windows.Media;

namespace MiscellaneousTab.ViewModel
{
    public class GenericCriteria_VM : Base_ViewModel
    {
        public GenericCriteria_VM()
        {
        }
        public SolidColorBrush Back_Category { get; set; }
        public SolidColorBrush Back_Delimiter { get; set; }
        public SolidColorBrush Back_ThresholdType { get; set; }
        public SolidColorBrush Back_ThresholdUnitType { get; set; }
        public SolidColorBrush Back_ThresholdOperatorType { get; set; }
        public SolidColorBrush Back_NullValue { get; set; }
        public SolidColorBrush Back_SingleValue { get; set; }
        public SolidColorBrush Back_UnitID { get; set; }
        public SolidColorBrush Back_OperatorID { get; set; }
        public SolidColorBrush Back_FieldName { get; set; }
        public SolidColorBrush Back_OperatorFieldName { get; set; }
        public string ColumnName { get; set; }
       

        public List<ILibraryCategories> ListCategory { get; set; }
        private ILibraryCategories _SelectedCatgory { get; set; }
        public ILibraryCategories SelectedCatgory
        {
            get { return _SelectedCatgory; }
            set
            {
                _SelectedCatgory = value;
                NotifyPropertyChanged("SelectedCatgory");
                Back_Category = value == null && ListCategory.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_Category");
                CommonFunctionToSelectMaxField();
            }
        }




        public List<ILibraryDelimiters> ListDelimiter { get; set; }
        private ILibraryDelimiters _SelectedDelimiter { get; set; }
        public ILibraryDelimiters SelectedDelimiter
        {
            get { return _SelectedDelimiter; }
            set
            {
                _SelectedDelimiter = value;
                NotifyPropertyChanged("SelectedDelimiter");
                Back_Delimiter = value == null && ListDelimiter.Count > 0 ? new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_Delimiter");
            }
        }

        public List<IComboValue> ListThresholdType { get; set; }
        private IComboValue _SelectedThresholdType { get; set; }
        public IComboValue SelectedThresholdType
        {
            get { return _SelectedThresholdType; }
            set
            {
                _SelectedThresholdType = value;
                NotifyPropertyChanged("SelectedThresholdType");
                ListCategory = new List<ILibraryCategories>(new List<ILibraryCategories>());
                ListConsiderNullValues = new List<IComboValue>(new List<IComboValue>());
                ListConsiderSingleValues = new List<IComboValue>(new List<IComboValue>());
                ThresholdUnitType = new List<IComboValue>(new List<IComboValue>());
                ThresholdOperatorType = new List<IComboValue>(new List<IComboValue>());
                ListDelimiter = new List<ILibraryDelimiters>(new List<ILibraryDelimiters>());
                ListUnit = new List<ILibraryUnits>(new List<ILibraryUnits>());
                ListOperator = new List<ILibraryOperator>(new List<ILibraryOperator>());
                MaxFieldName = new List<IComboValue>(new List<IComboValue>());

                Back_Category = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdUnitType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_NullValue = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_UnitID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_Delimiter = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_FieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorFieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Max Value":
                            ListCategory = new List<ILibraryCategories>(CasValidity_VM._lstLibraryThresholdCategories);
                            ListConsiderNullValues = new List<IComboValue>(CasValidity_VM._lstConsiderNullValue);
                            ThresholdUnitType = new List<IComboValue>(CasValidity_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new List<IComboValue>(CasValidity_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            Back_NullValue = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //if (CasValidity_VM.CombineFieldName.Where(x => x.Item1.ToUpper() == ColumnName.ToUpper()).Count() == 0)
                            //{
                            //    CasValidity_VM.CombineFieldName.Add((ColumnName, "Max"));
                            //}
                            break;
                        case "Min Value":
                            ListCategory = new List<ILibraryCategories>(CasValidity_VM._lstLibraryThresholdCategories);
                            ThresholdUnitType = new List<IComboValue>(CasValidity_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new List<IComboValue>(CasValidity_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //if (CasValidity_VM.CombineFieldName.Where(x => x.Item1.ToUpper() == ColumnName.ToUpper()).Count() == 0)
                            //{
                            //    CasValidity_VM.CombineFieldName.Add((ColumnName, "Min"));
                            //}
                            break;
                        case "Value Range":
                            ListCategory = new List<ILibraryCategories>(CasValidity_VM._lstLibraryThresholdCategories);
                            ThresholdUnitType = new List<IComboValue>(CasValidity_VM._lstThresholdCategoryinCombo);
                            ThresholdOperatorType = new List<IComboValue>(CasValidity_VM._lstThresholdOperatorinCombo);
                            Back_Category = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_ThresholdUnitType = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            Back_NullValue = new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush;
                            ListConsiderSingleValues = new List<IComboValue>(CasValidity_VM._lstSingleValueConsiderAs);
                            ListConsiderNullValues = new List<IComboValue>(CasValidity_VM._lstConsiderNullValue);
                            Back_SingleValue = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            ListDelimiter = new List<ILibraryDelimiters>(CasValidity_VM._lstLibraryDelimiters);
                            Back_Delimiter = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            break;
                        case "Max Value Operator":
                            //CasValidity_VM.CombineFieldName = CasValidity_VM.CombineFieldName.Where(x => x.Item1 != ColumnName).ToList();
                            //var result = CasValidity_VM.CombineFieldName.Where(x => x.Item2 == "Max").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            //Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //MaxFieldName = new List<IComboValue>(result);
                            //break; ;
                        case "Min Value Operator":
                            //CasValidity_VM.CombineFieldName = CasValidity_VM.CombineFieldName.Where(x => x.Item1 != ColumnName).ToList();
                            //var resultMin = CasValidity_VM.CombineFieldName.Where(x => x.Item2 == "Min").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                            //MaxFieldName = new List<IComboValue>(resultMin);
                            //Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            //break;
                        case "Unit":
                            UpdateMaxFields();
                            break;

                    }
                }
                Back_ThresholdType = value == null && ListThresholdType.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;

                NotifyPropertyChanged("ListCategory");
                NotifyPropertyChanged("ThresholdUnitType");
                NotifyPropertyChanged("ThresholdOperatorType");
                NotifyPropertyChanged("ListConsiderNullValues");
                NotifyPropertyChanged("ListDelimiter");
                NotifyPropertyChanged("ListConsiderSingleValues");
                NotifyPropertyChanged("ListUnit");
                NotifyPropertyChanged("ListOperator");
                NotifyPropertyChanged("ListDelimiter");

                NotifyPropertyChanged("Back_UnitID");
                NotifyPropertyChanged("Back_OperatorID");
                NotifyPropertyChanged("Back_ThresholdType");
                NotifyPropertyChanged("Back_Category");
                NotifyPropertyChanged("Back_ThresholdUnitType");
                NotifyPropertyChanged("Back_ThresholdOperatorType");
                NotifyPropertyChanged("Back_NullValue");
                NotifyPropertyChanged("Back_Delimiter");
                NotifyPropertyChanged("Back_SingleValue");
                NotifyPropertyChanged("Back_Delimiter");
                NotifyPropertyChanged("Back_FieldName");
            }
        }


        public List<IComboValue> ListConsiderNullValues { get; set; }
        private IComboValue _SelectedConsiderNullValues { get; set; }
        public IComboValue SelectedConsiderNullValues
        {
            get { return _SelectedConsiderNullValues; }
            set
            {
                _SelectedConsiderNullValues = value;
                NotifyPropertyChanged("SelectedConsiderNullValues");
                if (value != null)
                {
                    Back_NullValue = value == null ? (SelectedThresholdType.CboValue == "Max Value" ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush) : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_NullValue");
                }
            }
        }


        public List<IComboValue> ListConsiderSingleValues { get; set; }
        private IComboValue _SelectedConsiderSingleValues { get; set; }
        public IComboValue SelectedConsiderSingleValues
        {
            get { return _SelectedConsiderSingleValues; }
            set
            {
                _SelectedConsiderSingleValues = value;
                NotifyPropertyChanged("SelectedConsiderSingleValues");
                Back_SingleValue = value == null && ListConsiderSingleValues.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_SingleValue");
            }
        }


        public List<ILibraryUnits> ListUnit { get; set; }
        public List<ILibraryOperator> ListOperator { get; set; }

        private ILibraryUnits _SelectedUnit { get; set; }
        public ILibraryUnits SelectedUnit
        {
            get
            {
                return _SelectedUnit;
            }
            set
            {
                _SelectedUnit = value;
                NotifyPropertyChanged("SelectedUnit");
                Back_UnitID = value == null && ListUnit.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_UnitID");
                CommonFunctionToSelectMaxField();
            }
        }
        private ILibraryOperator _SelectedOperator { get; set; }
        public ILibraryOperator SelectedOperator
        {
            get
            {
                return _SelectedOperator;
            }
            set
            {
                _SelectedOperator = value;
                NotifyPropertyChanged("SelectedOperator");
                Back_OperatorID = value == null && ListOperator.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                NotifyPropertyChanged("Back_OperatorID");
                CommonFunctionToSelectMaxField();
            }
        }


        public List<IComboValue> ThresholdUnitType { get; set; }
        public List<IComboValue> ThresholdOperatorType { get; set; }
        private IComboValue _SelectedThresholdUnitType { get; set; }
        public IComboValue SelectedThresholdUnitType
        {
            get
            {
                return _SelectedThresholdUnitType;
            }
            set
            {
                _SelectedThresholdUnitType = value;
                NotifyPropertyChanged("SelectedThresholdUnitType");
                ListUnit = new List<ILibraryUnits>(new List<ILibraryUnits>());
                MaxFieldName = new List<IComboValue>(new List<IComboValue>());
                Back_UnitID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_FieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdUnitType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Manual Unit Definition":
                            ListUnit = new List<ILibraryUnits>(CasValidity_VM._lstLibraryUnits);
                            Back_UnitID = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;
                        case "Unit FieldName":
                            Back_FieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;

                    }
                }
                Back_ThresholdUnitType = value == null && ThresholdUnitType.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;


                NotifyPropertyChanged("ListUnit");
                NotifyPropertyChanged("Back_UnitID");
                NotifyPropertyChanged("Back_ThresholdUnitType");
                NotifyPropertyChanged("Back_FieldName");

            }
        }
        private IComboValue _SelectedThresholdOperatorType { get; set; }
        public IComboValue SelectedThresholdOperatorType
        {
            get
            {
                return _SelectedThresholdOperatorType;
            }
            set
            {
                _SelectedThresholdOperatorType = value;
                NotifyPropertyChanged("SelectedThresholdOperatorType");
                ListOperator = new List<ILibraryOperator>(new List<ILibraryOperator>());
                OperatorFieldName = new List<IComboValue>(new List<IComboValue>());
                Back_OperatorID = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_OperatorFieldName = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                Back_ThresholdOperatorType = new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                if (value != null)
                {
                    switch (value.CboValue)
                    {
                        case "Manual Operator":
                            if (SelectedThresholdType.CboValue == "Max Value")
                            {
                                ListOperator = new List<ILibraryOperator>(CasValidity_VM._lstLibraryOperators.Where(x => x.Operator == "<" || x.Operator == "<=").ToList());
                            }
                            else
                            {
                                ListOperator = new List<ILibraryOperator>(CasValidity_VM._lstLibraryOperators.Where(x => x.Operator == ">" || x.Operator == ">=").ToList());
                            }
                            Back_OperatorID = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;
                        case "Operator FieldName":
                            Back_OperatorFieldName = new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush;
                            UpdateMaxFields();
                            break;

                    }
                }
                if (ThresholdOperatorType != null)
                {
                    Back_ThresholdOperatorType = value == null && ThresholdOperatorType.Count > 0 ? new BrushConverter().ConvertFromString("LightBlue") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                }
                NotifyPropertyChanged("ListOperator");
                NotifyPropertyChanged("OperatorFieldName");
                NotifyPropertyChanged("Back_OperatorID");
                NotifyPropertyChanged("Back_ThresholdOperatorType");
                NotifyPropertyChanged("Back_OperatorFieldName");
            }
        }

        private List<IComboValue> _MaxFieldName { get; set; }

        public List<IComboValue> MaxFieldName
        {
            get
            {
                return _MaxFieldName;
            }
            set
            {
                if (value != null)
                {
                    _MaxFieldName = value;
                    NotifyPropertyChanged("MaxFieldName");
                }
            }
        }
        private IComboValue _SelectedMaxFieldName { get; set; }
        public IComboValue SelectedMaxFieldName
        {
            get
            {
                return _SelectedMaxFieldName;
            }
            set
            {
                if (value != null)
                {
                    _SelectedMaxFieldName = value;
                    NotifyPropertyChanged("SelectedMaxFieldName");
                    Back_FieldName = value == null && MaxFieldName.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_FieldName");
                }
            }
        }
        private List<IComboValue> _OperatorFieldName { get; set; }

        public List<IComboValue> OperatorFieldName
        {
            get
            {
                return _OperatorFieldName;
            }
            set
            {
                if (value != null)
                {
                    _OperatorFieldName = value;
                    NotifyPropertyChanged("OperatorFieldName");
                }
            }
        }
        private IComboValue _SelectedOperatorFieldName { get; set; }
        public IComboValue SelectedOperatorFieldName
        {
            get
            {
                return _SelectedOperatorFieldName;
            }
            set
            {
                if (value != null)
                {
                    _SelectedOperatorFieldName = value;
                    NotifyPropertyChanged("SelectedOperatorFieldName");
                    Back_OperatorFieldName = value == null && OperatorFieldName.Count > 0 ? new BrushConverter().ConvertFromString("LightPink") as SolidColorBrush : new BrushConverter().ConvertFromString("White") as SolidColorBrush;
                    NotifyPropertyChanged("Back_OperatorFieldName");
                }
            }
        }
        public bool? ValueFieldContainOperator { get; set; }


        private void CommonFunctionToSelectMaxField()
        {
            if (SelectedCatgory != null && SelectedThresholdUnitType != null && SelectedThresholdUnitType.CboValue == "Manual Unit Definition" && SelectedUnit != null)
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "abc", SelectedThresholdType.CboValue + ";" + SelectedCatgory.Category + ";" + SelectedUnit.Unit + ";" + ColumnName), typeof(GenericCriteria_VM));
                var selColumn = string.Empty;
                if (CasValidity_VM.columnNameFound != string.Empty)
                {
                    selColumn = CasValidity_VM.columnNameFound + "_Unit";
                }
                else
                {
                    selColumn = ColumnName + "_Unit";
                }
                if (CasValidity_VM.CombineFieldName.Count(x => x.Item1 == selColumn) == 0)
                {
                    CasValidity_VM.CombineFieldName.Add((selColumn, "VirtualUnit"));
                }
                var resultUnit2 = CasValidity_VM.CombineFieldName.Where(x => x.Item2 == "VirtualUnit").Select(y => new IComboValue { ID = 0, CboValue = y.Item1 }).ToList();
                MaxFieldName = new List<IComboValue>(resultUnit2);
                SelectedMaxFieldName = MaxFieldName.Where(x => x.CboValue == selColumn).FirstOrDefault();
            }
        }
        private void UpdateMaxFields()
        {
            if ((SelectedThresholdType != null && SelectedThresholdType.CboValue == "Unit") || (SelectedThresholdUnitType != null && SelectedThresholdUnitType.CboValue == "Unit FieldName"))
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddUnit", ColumnName), typeof(GenericCriteria_VM));
            }
            else
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "abc", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Max Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Operator FieldName")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMax", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Min Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Operator FieldName")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMin", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Max Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Manual Operator")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMaxManual", ColumnName), typeof(GenericCriteria_VM));
            }
            if (SelectedThresholdType != null && SelectedThresholdType.CboValue == "Min Value" && SelectedThresholdOperatorType != null && SelectedThresholdOperatorType.CboValue == "Manual Operator")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(null, "AddMinManual", ColumnName), typeof(GenericCriteria_VM));
            }
        }
    }
    public static class GenericList
    {
        public static List<T> Clone<T>(this List<T> list) where T : ICloneable
        {
            return list.Select(i => (T)i.Clone()).ToList();
        }
    }
}
