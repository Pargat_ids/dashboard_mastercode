﻿using MiscellaneousTab.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using MiscellaneousTab.CustomUserControl;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public static List<IAccessData> FilterRows { get; set; }
        public string UniqueCASCount { get; set; }
        public string ValidCASCount { get; set; }
        public string ECAScount { get; set; }
        public string InvalidCAScount { get; set; }
        public string AlternateCASCount { get; set; }
        public string NonOmittedrowswithoutCAS { get; set; }

        public CommonFunctions objCommon;
        public string TabName { get; set; }
        public string DefaultLableUniqueChar { get; set; }
        public string DefaultLableNotes { get; set; }
        public string DefaultLablePhrase { get; set; }
        public string DefaultLableReorder { get; set; }
        public string DefaultLableCAS { get; set; }
        public string DefaultLableNoteCodeConsistency { get; set; }
        public string DefaultLableRemoveWhiteCharacters { get; set; }
        public string DefaultLableSubroots { get; set; }
        public string DefaultLableDateFormat { get; set; }
        public string DefaultLableOmit { get; set; }
        public string DefaultLableAccess { get; set; }
        public string DefaultLableGreek { get; set; }
        public string DefaultLableFlagGeneric { get; set; }
        public string DefaultFieldSquash { get; set; }
        public Visibility IsSelectedAllColumnVisible { get; set; } = Visibility.Collapsed;
        public ICommand commandSelectedChanges { get; set; }

        public Visibility isPhraseChooseVisible { get; set; } = Visibility.Collapsed;
        public List<iTableName> listTableName { get; set; }
        public List<iTableName> listCase { get; set; }
        public List<iTableName> listCaseOmit { get; set; }
        public bool _IsSelectedAllColumn { get; set; }
        public bool IsSelectedAllColumn
        {
            get => _IsSelectedAllColumn;
            set
            {
                if (Equals(_IsSelectedAllColumn, value))
                {
                    return;
                }

                _IsSelectedAllColumn = value;
                listFields.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                NotifyPropertyChanged("listFields");
                commandSelectedChangesExecute(null);
            }
        }
        public bool _IsSelectedAllSquash { get; set; }
        public bool IsSelectedAllSquash
        {
            get => _IsSelectedAllSquash;
            set
            {
                if (Equals(_IsSelectedAllSquash, value))
                {
                    return;
                }

                _IsSelectedAllSquash = value;
                listSquashChar.ToList().ForEach(x =>
                {
                    x.IsSelected = value;
                });
                NotifyPropertyChanged("listSquashChar");
                commandSelectedChangesExecute(null);
            }
        }
        public iTableName SelectedTable
        {
            get => _SelectedTable;
            set
            {
                if (Equals(_SelectedTable, value))
                {
                    return;
                }

                _SelectedTable = value;
                TabName = value != null ? _SelectedTable.TableName : string.Empty;
                BindfieldNames(value);
                BindLabel(value);
                if (TabName == "Default")
                {
                    isPhraseChooseVisible = Visibility.Visible;
                    NotifyPropertyChanged("isPhraseChooseVisible");
                    IsSelectedAllColumnVisible = Visibility.Collapsed;
                    NotifyPropertyChanged("IsSelectedAllColumnVisible");
                    DefaultDelimiterDateFormat();
                }
                else
                {
                    isPhraseChooseVisible = Visibility.Collapsed;
                    NotifyPropertyChanged("isPhraseChooseVisible");
                    IsSelectedAllColumnVisible = Visibility.Visible;
                    NotifyPropertyChanged("IsSelectedAllColumnVisible");
                }
                OnTabChange();
                newListFieldNameAss = null;
                var resultNameAss = new List<MapFieldListNameAssVM>();
                if (TabName == "Default")
                {
                    resultNameAss.Add(new MapFieldListNameAssVM { FieldType = "CAS_Field", SelectedFieldName = listType.Where(x => x.Type == "CAS").FirstOrDefault() });
                    resultNameAss.Add(new MapFieldListNameAssVM { FieldType = "Name_Field", SelectedFieldName = listType.Where(x => x.Type == "PREF").FirstOrDefault() });
                }
                else
                {
                    resultNameAss.Add(new MapFieldListNameAssVM { FieldType = "CAS_Field", SelectedFieldName = new IType() });
                    resultNameAss.Add(new MapFieldListNameAssVM { FieldType = "Name_Field", SelectedFieldName = new IType() });
                }
                newListFieldNameAss = new List<MapFieldListNameAssVM>(resultNameAss);
                NotifyPropertyChanged("newListFieldNameAss");
            }
        }

        public TabItem _SelectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get { return _SelectedTabItem; }
            set
            {
                if (_SelectedTabItem != value)
                {
                    _SelectedTabItem = value;
                    OnTabChange();
                }

            }
        }
        public void OnTabChange()
        {
            switch (SelectedTabItem.Header.ToString())
            {
                case "Field Validator":
                    RunThresholdValidator();
                    break;
                case "Consolidate Rows":
                    BindConcatenateFields();
                    BindMatchingFields();
                    break;
                case "Phrases":
                    BindSelectedPhraseField();
                    break;
            }
        }

        private void RunThresholdValidator()
        {
            if (TabName == "Default")
            {
                // here i need to fetch previous addinfo of that file
                listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();
                var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
                var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
                LoaderAddInfo = Visibility.Visible;
                NotifyPropertyChanged("LoaderAddInfo");
                Task.Run(() =>
                {
                    CheckExistingValues(QsidID);
                    NotifyPropertyChanged("listGenericCriteria");
                    ExecuteFunction(listGenericCriteria.ToList());
                    LoaderAddInfo = Visibility.Collapsed;
                    NotifyPropertyChanged("LoaderAddInfo");
                });
            }
        }
        private iTableName _SelectedCase { get; set; }
        public iTableName SelectedCase
        {
            get => _SelectedCase;
            set
            {
                if (Equals(_SelectedCase, value))
                {
                    return;
                }

                _SelectedCase = value;
            }
        }
        private void BindLabel(iTableName tableName)
        {
            if (tableName != null)
            {
                if (tableName.TableName.ToString() == "Default")
                {
                    DefaultLableUniqueChar = "Data - All Columns (Except ID, RowID, Ident)" + Environment.NewLine + "Notes - All Columns(Except ID,RowID,Ident)" + Environment.NewLine + "Data_Dictionary - FIELD_NAME, HEAD_CD, EXPLAIN, Phrase " + Environment.NewLine + "List_Dictionary - All Columns(Except ID, RowID, Ident)";
                    DefaultLableNotes = "Notes - NOTES_FIELD, NOTE_CODE, NOTE_TEXT, NOTE_ENGL" + Environment.NewLine + "DATA - All Notes Fields";
                    DefaultLablePhrase = "Data - All Phrase Columns with English Language (Update Table Translate__Phrases)" + Environment.NewLine + " Translations_phrases - All Valid Language Columns (Update Table in SQL)";
                    DefaultLableReorder = "DATA - RN, TMP_RN";
                    DefaultLableCAS = "DATA - INV_CAS, RN, CAS, Editorial, OMIT (Check MDB File)" + Environment.NewLine + "DATA - CAS, Editorial (Added Dashes, Remove Dashes)";
                    DefaultLableNoteCodeConsistency = "Notes - NOTES_FIELD, NOTE_CODE, NOTE_TEXT, NOTE_ENGL";
                    DefaultLableRemoveWhiteCharacters = "DATA - All Fields";
                    DefaultLableSubroots = "DATA - (Fields from Additionalinfo) ";
                    DefaultLableOmit = "DATA - CAS";
                    DefaultLableAccess = "DATA - PREF";
                    DefaultLableGreek = "DATA - All Fields";
                    DefaultLableIdentifyDuplicate = "DATA - (Pref Fields with delimiter from Additionalinfo)";
                    DefaultLableFlagGeneric = "DATA - CAS, Editorial";
                    DefaultFieldSquash = "DATA - PREF";
                    DefaultLableDateFormat = "DATA-Already Exist Pharse-DateFormat(only first)  ";
                    DefaultLableStripCharacter = "DATA-CAS";
                    DefaultLableThreshold = "DATA-Already Exists mapping for this list";
                    DefaultLableECValidator = "DATA-Already Exist Identifier Ec-Number";
                    DefaultLableCheckUnits = "DATA-Already Exist Unit columns";
                    DefaultLableNameAssignment = "DATA-CAS,PREF(if available)";
                }
                else
                {
                    DefaultLableUniqueChar = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray()) + "(Except ID,RowID,Ident)";
                    DefaultLableNotes = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray()) + Environment.NewLine + "DATA - All Notes Fields";
                    DefaultLablePhrase = tableName.TableName + " - All Phrase Columns with English Language (Update Table Translate__Phrases)" + Environment.NewLine + tableName.TableName +
                        "-  All Valid Language Columns (Update Table in SQL)";
                    DefaultLableReorder = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableCAS = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableNoteCodeConsistency = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableRemoveWhiteCharacters = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableSubroots = "DATA - (Fields from Additionalinfo) ";
                    DefaultLableOmit = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableAccess = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableGreek = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableIdentifyDuplicate = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableFlagGeneric = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultFieldSquash = tableName.TableName + " - " + listFields.Where(x => x.IsSelected).Select(x => x.FieldName).FirstOrDefault();
                    DefaultLableDateFormat = tableName.TableName + " - " + listFields.Where(x => x.IsSelected).Select(x => x.FieldName).FirstOrDefault();
                    DefaultLableStripCharacter = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableThreshold = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableECValidator = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableCheckUnits = tableName.TableName + " - " + string.Join(", ", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToArray());
                    DefaultLableNameAssignment = tableName.TableName + " - CAS," + listFields.Where(x => x.IsSelected).Select(x => x.FieldName).FirstOrDefault();
                }
                NotifyPropertyChanged("DefaultLableUniqueChar");
                NotifyPropertyChanged("DefaultLableNotes");
                NotifyPropertyChanged("DefaultLablePhrase");
                NotifyPropertyChanged("DefaultLableReorder");
                NotifyPropertyChanged("DefaultLableCAS");
                NotifyPropertyChanged("DefaultLableNoteCodeConsistency");
                NotifyPropertyChanged("DefaultLableRemoveWhiteCharacters");
                NotifyPropertyChanged("DefaultLableSubroots");
                NotifyPropertyChanged("DefaultLableOmit");
                NotifyPropertyChanged("DefaultLableAccess");
                NotifyPropertyChanged("DefaultLableGreek");
                NotifyPropertyChanged("DefaultLableIdentifyDuplicate");
                NotifyPropertyChanged("DefaultLableFlagGeneric");
                NotifyPropertyChanged("DefaultFieldSquash");
                NotifyPropertyChanged("DefaultLableDateFormat");
                NotifyPropertyChanged("DefaultLableECValidator");
                NotifyPropertyChanged("DefaultLableStripCharacter");
                NotifyPropertyChanged("DefaultLableThreshold");
                NotifyPropertyChanged("DefaultLableCheckUnits");
                NotifyPropertyChanged("DefaultLableNameAssignment");
            }
            newListField = null;
            var result = new List<MapFieldListVM>();
            listType = new List<IType>();
            if (TabName != "Default")
            {
                var FirstOne = listFields.Where(x => x.IsSelected).FirstOrDefault();
                if (FirstOne != null)
                {
                    result.Add(
                        new MapFieldListVM
                        {
                            FieldName = FirstOne.FieldName,
                            IsSelected = false,
                            SelectedDelimiter = new IDelimiter()
                        });
                }

                listFields.ForEach(x =>
                {
                    listType.Add(new IType { Type = x.FieldName });
                });
                NotifyPropertyChanged("listType");
            }
            else
            {
                result.Add(
                        new MapFieldListVM
                        {
                            FieldName = "PREF",
                            IsSelected = false,
                            SelectedDelimiter = new IDelimiter()
                        });

                listType.Add(new IType { Type = "CAS" });
                listType.Add(new IType { Type = "PREF" });
            }
            if (result.Any())
            {
                newListField = new List<MapFieldListVM>(result);
            }
            NotifyPropertyChanged("newListField");
            NotifyPropertyChanged("listType");

        }
        private void BindfieldNames(iTableName tableName)
        {
            var listRes = new List<MapFieldListVM>();
            if (tableName != null)
            {
                var dt = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tableName.TableName + "]", CommonFilepath);

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    listRes.Add(new MapFieldListVM { IsSelected = false, FieldName = dt.Columns[i].ColumnName });
                }
            }
            listFields = new List<MapFieldListVM>(listRes);
            NotifyPropertyChanged("listFields");
            NotifyPropertyChanged("listDelimiter");
        }
        private void BindMatchingFields()
        {
            var tabNm = TabName == "Default" ? "Data" : TabName;
            var listRes = new List<MapFieldListMatchingCriteriaVM>();
            if (string.IsNullOrEmpty(TabName) == false)
            {
                var dt = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tabNm + "]", CommonFilepath);

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName.ToUpper() != "IDENT" && dt.Columns[i].ColumnName.ToUpper() != "OMIT")
                    {
                        listRes.Add(new MapFieldListMatchingCriteriaVM { IsSelected = false, FieldName = dt.Columns[i].ColumnName });
                    }
                }
            }
            listFieldsMatchingCriteria = new List<MapFieldListMatchingCriteriaVM>(listRes);
            NotifyPropertyChanged("listFieldsMatchingCriteria");

        }
        private void BindConcatenateFields()
        {
            var tabNm = TabName == "Default" ? "Data" : TabName;

            var listRes = new List<MapFieldListConcatenateCriteriaVM>();
            if (string.IsNullOrEmpty(TabName) == false)
            {
                var dt = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tabNm + "]", CommonFilepath);

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName.ToUpper() != "IDENT" && dt.Columns[i].ColumnName.ToUpper() != "OMIT")
                    {
                        listRes.Add(new MapFieldListConcatenateCriteriaVM { IsSelected = false, FieldName = dt.Columns[i].ColumnName });
                    }
                }
            }
            listFieldsConcatenateCriteria = new List<MapFieldListConcatenateCriteriaVM>(listRes);
            NotifyPropertyChanged("listFieldsConcatenateCriteria");
        }
        private void BindSquashCharacters()
        {
            using (var context = new CRAModel())
            {
                var res = context.Library_SquashCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.Character.Trim()) == false).Select(x => new MapStripCharacters { IsSelected = false, SquashCharacter = x.Character.Trim() }).ToList();
                listSquashChar = new List<MapStripCharacters>(res);
                NotifyPropertyChanged("listSquashChar");
            }
        }
        private void BindSelectedPhraseField()
        {
            if (TabName == "Default")
            {
                var listRes = new List<MapPhraseListVM>();
                var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
                var getPhraseFields = objCommon.DbaseQueryReturnTableSql("select distinct a.FieldNameID, FieldName, Delimiter, f.PhraseCategory as Category, " +
                   " (select top 1 Phrase from QSxxx_DataDictionary x, Library_PhraseCategories y, Library_Phrases z " +
                   "  where x.PhraseCategoryID = y.PhraseCategoryID and " +
                   "  y.PhraseCategory = 'Field Explanation' and x.PhraseID = z.PhraseID and x.FieldNameID = a.FieldNameID " +
                   "  and x.QSID_ID = a.QSID_ID) as Description " +
                   "  from Map_QSID_FieldName a " +
                   "  inner " +
                   "  join Library_FieldNames b " +
                   " on a.FieldNameID = b.FieldNameID inner " +
                   "  join Library_QSIDs c  on a.QSID_ID = c.QSID_ID " +
                   " inner " +
                   "  join Library_Languages d  on a.LanguageID = d.LanguageID " +
                   " left " +
                   "  join Library_Delimiters e  on a.MultiValuePhraseDelimiterID = e.DelimiterID " +
                   " left " +
                   "  join Library_PhraseCategories f on f.PhraseCategoryID = a.PhraseCategoryID " +
                   "  where a.IsPhraseField = 1 and c.QSID = '" + lstDic + "' " +
                   "  and d.LanguageName = 'English' ", CommonListConn);
                string combineFieldNames = string.Empty;
                for (int i = 0; i < getPhraseFields.Rows.Count; i++)
                {
                    var fldName = getPhraseFields.Rows[i].Field<string>("FieldName").Trim().ToUpper();
                    var valueTrueFalse = true;
                    if (fldName == "PUBLICATION DATE" || fldName == "GENERIC DATE" || fldName == "EFFECTIVE DATE" || fldName == "EXPIRATION DATE" || fldName == "SOURCE SECTION" || fldName == "REFERENCE NO" || fldName == "BIBLIOGRAPHY")
                    {
                        valueTrueFalse = false;
                    }
                    listRes.Add(new MapPhraseListVM { IsPhraseSelected = valueTrueFalse, FieldNamePhrase = fldName + " ", Category = "[" + getPhraseFields.Rows[i].Field<string>("Category") + "] ", Description = "(" + getPhraseFields.Rows[i].Field<string>("Description") + ")" });
                }
                listPhraseFields = new List<MapPhraseListVM>(listRes);
                NotifyPropertyChanged("listPhraseFields");
            }
        }

        public List<MapFieldListVM> listFields { get; set; }
        public List<MapStripCharacters> listSquashChar { get; set; }
        public List<MapFieldListVM> newListField { get; set; }
        public List<MapFieldListNameAssVM> newListFieldNameAss { get; set; }
        public List<MapPhraseListVM> listPhraseFields { get; set; }
        public List<MapUniqueFieldListVM> listUniqueFields { get; set; }
        private iTableName _SelectedTable { get; set; }
        public string CasValid { get; set; } = string.Empty;

        public DataTable dtRaw = new DataTable();
        public Visibility uploadCASVis { get; set; } = Visibility.Collapsed;
        public Visibility uploadReorderVis { get; set; } = Visibility.Collapsed;
        public Visibility uploadTranslate { get; set; } = Visibility.Collapsed;
        public ICommand CheckValidityCommand { get; set; }
        public ICommand UploadMdb { get; set; }
        public ICommand AddDashes { get; set; }
        public ICommand RemoveDashes { get; set; }
        public ICommand BtnRemoveWhiteCharacters { get; set; }
        public ICommand UploadExcel { get; set; }
        public ICommand UploadMdbCommon { get; set; }
        public ICommand ResetMsTool { get; set; }
        public ICommand TranslateShow { get; set; }
        public ICommand ReorderMdb { get; set; }
        public ICommand TranslateNotesUpdateinSQL { get; set; }
        public ICommand TranslateUpdateinSQL { get; set; }
        public ICommand CopyRNToTmpRN { get; set; }
        public ICommand BtnFillRNFromScratch { get; set; }
        public ICommand BtnFillEmptyRN { get; set; }
        public string CommonFilepath { get; set; }
        public StandardDataGrid_VM listTranslate { get; set; }
        public StandardDataGrid_VM listNoteTranslate { get; set; }
        public StandardDataGrid_VM listNotecodeConsistency { get; set; }
        public CommonDataGrid_ViewModel<IUploadCAS> comonUpload_VM { get; set; }
        public CommonDataGrid_ViewModel<IReorder> comonReorder_VM { get; set; }
        public CommonDataGrid_ViewModel<Property_EncryptHexa_VM> commonGrd_EncryptHexa { get; set; }
        public CommonDataGrid_ViewModel<Property_DecryptHexa_VM> commonGrd_DecryptHexa { get; set; }
        public CommonDataGrid_ViewModel<EchaBusinessKey_VM> commonGrd_EchaBusinessKey_VM { get; set; }
        private List<Property_EncryptHexa_VM> _listPropertyEncryptHexa { get; set; } = new();
        public List<Property_EncryptHexa_VM> listPropertyEncryptHexa
        {
            get { return _listPropertyEncryptHexa; }
            set
            {
                if (_listPropertyEncryptHexa != value)
                {

                    _listPropertyEncryptHexa = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Property_EncryptHexa_VM>>>(new PropertyChangedMessage<List<Property_EncryptHexa_VM>>(null, _listPropertyEncryptHexa, "Default List"));
                }
            }
        }
        private List<EchaBusinessKey_VM> _listEchaBusinessKey_VM { get; set; } = new();
        public List<EchaBusinessKey_VM> listEchaBusinessKey_VM
        {
            get { return _listEchaBusinessKey_VM; }
            set
            {
                if (_listEchaBusinessKey_VM != value)
                {

                    _listEchaBusinessKey_VM = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<EchaBusinessKey_VM>>>(new PropertyChangedMessage<List<EchaBusinessKey_VM>>(null, _listEchaBusinessKey_VM, "Default List"));
                }
            }
        }
        private EchaBusinessKey_VM SelectedEchaBusinessKey_VM { get; set; }
        private List<Property_DecryptHexa_VM> _listPropertyDecryptHexa { get; set; } = new();
        public List<Property_DecryptHexa_VM> listPropertyDecryptHexa
        {
            get { return _listPropertyDecryptHexa; }
            set
            {
                if (_listPropertyDecryptHexa != value)
                {

                    _listPropertyDecryptHexa = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Property_DecryptHexa_VM>>>(new PropertyChangedMessage<List<Property_DecryptHexa_VM>>(null, _listPropertyDecryptHexa, "Default List"));
                }
            }
        }

        private List<EchaListEntryBusinessKey_VM> _listEchaBusinessKey { get; set; } = new();
        public List<EchaListEntryBusinessKey_VM> listEchaBusinessKey
        {
            get { return _listEchaBusinessKey; }
            set
            {
                if (_listEchaBusinessKey != value)
                {

                    _listEchaBusinessKey = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<EchaListEntryBusinessKey_VM>>>(new PropertyChangedMessage<List<EchaListEntryBusinessKey_VM>>(null, _listEchaBusinessKey, "Default List"));
                }
            }
        }


        private List<ISubroot> _listSubroots { get; set; } = new List<ISubroot>();
        public List<ISubroot> listSubroots
        {
            get { return _listSubroots; }
            set
            {
                if (_listSubroots != value)
                {
                    _listSubroots = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ISubroot>>>(new PropertyChangedMessage<List<ISubroot>>(null, _listSubroots, "Default List"));
                }
            }
        }

        public CommonDataGrid_ViewModel<IInvalidChar> comonInvalid_VM { get; set; }
        // public CommonDataGrid_ViewModel<ITranslate> comonTranslate_VM { get; set; }

        private bool _checkBoxInvalid { get; set; }
        public static string CommonListConn;

        private List<IUploadCAS> lstFinal = new List<IUploadCAS>();
        public bool checkBoxInvalid
        {
            get { return _checkBoxInvalid; }
            set
            {
                if (_checkBoxInvalid != value)
                {
                    _checkBoxInvalid = value;
                }
                NotifyPropertyChanged("checkBoxInvalid");
            }
        }

        public string inputRowID { get; set; }
        public string inputQsid { get; set; }

        public string inputID { get; set; }
        public string outputHexaVal { get; set; }
        public ICommand CommandConvertIdToHexa { get; set; }
        public string inputIDDecrypt { get; set; }
        public string outputIDVal { get; set; }
        public ICommand CommandConvertHexaToID { get; set; }

        public List<Map_Qsid_Legislation_VM> listMapQsidLG { get; set; } = new();
        private Map_Qsid_Legislation_VM _selectedQsidLegislationID { get; set; }
        public Map_Qsid_Legislation_VM SelectedQsidLegislationID
        {
            get
            {
                return _selectedQsidLegislationID;
            }
            set
            {
                _selectedQsidLegislationID = value;
                BindEchaListBusinessKey(_selectedQsidLegislationID.Qsid_ID);
                NotifyPropertyChanged("SelectedQsidLegislationID");
            }
        }
        public CommonDataGrid_ViewModel<EchaListEntryBusinessKey_VM> commonGrd_EchaListEntryBusinessKey_VM { get; set; }

        public Visibility visibilityEchaListBusinessKey { get; set; } = Visibility.Collapsed;
        public Visibility visibilityEchaBusinessKey { get; set; } = Visibility.Collapsed;
        public ICommand CommandGoToGenerics { get; set; }
        public bool IsSelectEchaListEntryKey { get; set; }
        public CasValidity_VM()
        {
            objCommon = new CommonFunctions();
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            commonGrd_EncryptHexa = new CommonDataGrid_ViewModel<Property_EncryptHexa_VM>(GetListGridColumnEncryptHexa(), "InputBaseVal", "Encrypt To Hexa");
            commonGrd_DecryptHexa = new CommonDataGrid_ViewModel<Property_DecryptHexa_VM>(GetListGridColumnDecryptHexa(), "InputBaseVal", "Decrypt To Hexa");
            commonGrd_EchaListEntryBusinessKey_VM = new CommonDataGrid_ViewModel<EchaListEntryBusinessKey_VM>(GetListGridColumnEchaBusinessKey(), "", "");
            commonGrd_EchaBusinessKey_VM = new CommonDataGrid_ViewModel<EchaBusinessKey_VM>(GetColumnEchaBusinessKey(), "Qsid", "Echa List Business Key");
            BindChecks();
            var listSubrootTypeTemp = new List<ISubrootType>();
            listSubrootTypeTemp.Add(new ISubrootType { SubrootType = "Subroot" });
            listSubrootTypeTemp.Add(new ISubrootType { SubrootType = "Expressed As (Phrase)" });
            listSubrootTypeTemp.Add(new ISubrootType { SubrootType = "Physical Form (Phrase)" });
            listSubrootTypeTemp.Add(new ISubrootType { SubrootType = "Threshold calculation basis" });
            listSubrootType = new List<ISubrootType>(listSubrootTypeTemp);

            using (var context = new CRAModel())
            {
                listDateType = new List<Library_DateFormat>(context.Library_DateFormat.AsNoTracking().ToList());
                var resultDelimiter = context.Library_Delimiters.AsNoTracking().Select(x => new IDelimiter { ID = x.DelimiterID, Delimiter = x.Delimiter }).ToList();
                listDelimiter = new List<IDelimiter>(resultDelimiter);
                listDelimiterDateFormat = new List<IDelimiter>(resultDelimiter);
                StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
            }
            BindMapLegislationQsid();
            BindEchaBusinessKey();
            CheckValidityCommand = new RelayCommand(CommandCasSearchExecute, CommandCasSearchCanExecute);
            commandSelectedChanges = new RelayCommand(commandSelectedChangesExecute, CommandCasCanExecute);

            UploadMdbCommon = new RelayCommand(UploadMdbCommonExecute, CommandCasCanExecute);
            ResetMsTool = new RelayCommand(ResetMsToolCommonExecute, CommandCasCanExecute);
            UploadExcel = new RelayCommand(UploadExcelExecute, CommandCasCanExecute);

            ShowUniqueCharacters = new RelayCommand(ShowUniqueCharactersExecute, CommandCasCanExecute);
            RemInvCharacters = new RelayCommand(RemInvCharactersExecute, CommandCasCanExecute);
            TranslateNotesUpdateinSQL = new RelayCommand(ShowNotesExecute, CommandCasCanExecute);
            TranslateShow = new RelayCommand(TranslatePhraseExecute, CommandCasCanExecute);
            TranslateUpdateinSQL = new RelayCommand(TranslateUpdateinSQLExecute, CommandCasCanExecute);
            CopyRNToTmpRN = new RelayCommand(CopyRNToTmpRNExecute, CommandCasCanExecute);
            BtnFillRNFromScratch = new RelayCommand(BtnFillRNFromScratchExecute, CommandCasCanExecute);
            BtnFillEmptyRN = new RelayCommand(BtnFillEmptyRNExecute, CommandCasCanExecute);
            IdentifyDuplicate = new RelayCommand(IdentifyDuplicateExecute, CommandCasCanExecute);
            AddDashes = new RelayCommand(AddDashesExecute, CommandCasCanExecute);
            RemoveDashes = new RelayCommand(RemoveDashesExecute, CommandCasCanExecute);
            BtnRemoveWhiteCharacters = new RelayCommand(RemoveWhiteCharactersExecute, CommandCasCanExecute);
            BtnGreekConversion = new RelayCommand(GreekConvert, CommandCasCanExecute);
            BtnIdentifyGreekConversion = new RelayCommand(GreekIdentifyConvert, CommandCasCanExecute);
            ReorderMdb = new RelayCommand(ReorderMdbExecute, CommandCasCanExecute);
            FixDuplicate = new RelayCommand(FixDuplicateExecute, CommandCasCanExecute);
            FixNoteCodeSameCell = new RelayCommand(FixNoteCodeSameCellExecute, CommandCasCanExecute);
            UploadMdb = new RelayCommand(UploadMdbExecute, CommandCasCanExecute);

            CheckNoteCodeConsistency = new RelayCommand(CheckNoteCodeConsistencyExecute, CommandCasCanExecute);
            BtnNameAssignment = new RelayCommand(BtnNameAssignmentExecute, CommandCasCanExecute);
            BtnNameAssignmentUpdateAll = new RelayCommand(BtnNameAssignmentUpdateAllExecute, CommandCasCanExecute);
            BtnNameAssignmentUpdateBlank = new RelayCommand(BtnNameAssignmentUpdateBlankExecute, CommandCasCanExecute);
            CheckSubroots = new RelayCommand(CheckSubrootsExecute, CheckSubrootsCanExecute);
            CheckUnits = new RelayCommand(CheckUnitsExecute, CheckUnitsCanExecute);
            CheckDateFormat = new RelayCommand(CheckDateFormatExecute, CheckDateFormatCanExecute);
            CheckStripCharacter = new RelayCommand(CheckStripCharacterExecute, CommandCasCanExecute);
            CheckStripCharacterAnalyze = new RelayCommand(CheckStripCharacterAnalyzeExecute, CommandCasCanExecute);
            CheckOmit = new RelayCommand(CheckOmitExecute, CommandCasCanExecute);
            FlagDuplicateTmpRN = new RelayCommand(FlagDuplicateTmpRNExecute, CommandCasCanExecute);
            UpdateOmit = new RelayCommand(UpdateOmitExecute, CommandCasCanExecute);
            CheckDictionaryFile = new RelayCommand(CheckDictionaryFileExecute, CommandCasCanExecute);

            comonUpload_VM = new CommonDataGrid_ViewModel<IUploadCAS>(GetListGridColumnUploadCAS(), "RN", "Check CASValidity");
            comonReorder_VM = new CommonDataGrid_ViewModel<IReorder>(GetListGridColumnReorder(), "RN", "ReOrder RN");
            comonInvalid_VM = new CommonDataGrid_ViewModel<IInvalidChar>(GetListGridColumnInvalid(), "RN", "Invalid Characters");
            comonGreek_VM = new CommonDataGrid_ViewModel<IInvalidGreek>(GetListGridColumnInvalidGreek(), "RN", "HTML Conversion Characters");
            comonIdentifyDuplicate_VM = new CommonDataGrid_ViewModel<IIdentifyDuplicate>(GetListGridColumnIdentifyDuplicate(), "RN", "Identify Duplicate Phrases/NoteCodes");

            commandOpenCurrentDataViewNewAddList = new RelayCommand(commandOpenCurrentDataViewNewAddListExected, commandOpenCurrentDataViewNewAddListCanExecte);
            comonSubroots_VM = new CommonDataGrid_ViewModel<ISubroot>(GetListGridColumnLibSubroots(), "RN", "Control Phrases");
            comonCheckUnits_VM = new CommonDataGrid_ViewModel<ICheckUnits>(GetListGridColumnCheckUnits(), "Units", "Units");
            comonNameAssignment_VM = new CommonDataGrid_ViewModel<INameAssignment>(GetListGridColumnNameAssignment(), "NameAssignment", "NameAssignment");

            MessengerInstance.Register<PropertyChangedMessage<ISubroot>>(this, NotifyMeSubroot);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ISubroot), CallbackHyperlink);

            ShowAccessData = new RelayCommand(ShowAccessDataExecute, CommandCasCanExecute);

            comonAccess_VM = new CommonDataGrid_ViewModel<IAccessData>(GetListGridColumnAccess(), "RN", "UniqueValues in AccessFile");
            comonUniqueChr_VM = new CommonDataGrid_ViewModel<IUniqueCharacter>(GetListGridColumnUniqueCharacters(), "RN", "Unique Characters");

            ComonlistPassedCheck = new CommonDataGrid_ViewModel<ICheckPassCases>(GetPassedColumn(), "RN", "Check Dictionary");

            UpdateAccessData = new RelayCommand(CommandUpdateAccessExecute, CommandCasCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<IUniqueCharacter>>(this, NotifyMeUniqueChr);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(IUniqueCharacter), CommandButtonUniqueChrExecuted);

            BtnShowFlagGeneric = new RelayCommand(BtnShowFlagGenericExecute, CommandCasCanExecute);
            BtnUpdateFlagGeneric = new RelayCommand(BtnUpdateFlagGenericExecute, CommandCasCanExecute);
            BtnSameAccessFile = new RelayCommand(BtnSameAccessFileExecute, CommandCasCanExecute);
            BtnSameAccessFileUniqueChar = new RelayCommand(BtnSameAccessFileUniqueCharExecute, CommandCasCanExecute);

            CommandConvertIdToHexa = new RelayCommand(CommandConvertIdToHexaExecute);
            CommandConvertHexaToID = new RelayCommand(CommandConvertHexaToIDExecute);
            CommandGoToGenerics = new RelayCommand(CommandGoToGenericsExecute);

            comonFieldSquash_VM = new CommonDataGrid_ViewModel<IFieldSquash>(GetListGridColumnFieldSquash(), "FieldSquash", "Field Squash");
            BtnFieldSquash = new RelayCommand(BtnFieldSquashExecute, CommandCasCanExecute);
            BtnUpdateAccessFieldSquash = new RelayCommand(BtnUpdateAccessFieldSquashExecute, CommandCasCanExecute);
            BindSquashCharacters();
            CommandAddNewStripChar = new RelayCommand(CommandAddNewStripCharExecute, CommandCasCanExecute);

            var resultCase = new List<iTableName>();
            resultCase.Add(new iTableName { TableName = "Upper" });
            resultCase.Add(new iTableName { TableName = "Lower" });
            resultCase.Add(new iTableName { TableName = "Keep Original" });
            listCase = new List<iTableName>(resultCase);
            NotifyPropertyChanged("listCase");
            SelectedCase = listCase.Where(x => x.TableName == "Lower").FirstOrDefault();
            NotifyPropertyChanged("SelectedCase");

            listCaseOmit = new List<iTableName>(resultCase);
            NotifyPropertyChanged("listCaseOmit");
            SelectedCaseOmit = listCaseOmit.Where(x => x.TableName == "Lower").FirstOrDefault();
            NotifyPropertyChanged("SelectedCaseOmit");



            BtnShowConsolidate = new RelayCommand(BtnShowConsolidateExecute, CommandCasCanExecute);
            BtnUpdateAccessConsolidate = new RelayCommand(BtnUpdateAccessConsolidateExecute, CommandCasCanExecute);
            BtnPreviewConsolidate = new RelayCommand(BtnPreviewConsolidateExecute, CommandCasCanExecute);
            MessengerInstance.Register<NotificationMessageAction<EchaBusinessKey_VM>>(this, CallBackRefreshEchaBusinessKeyExecute);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(EchaBusinessKey_VM), ButtonClickExecuted);
            MessengerInstance.Register<PropertyChangedMessage<EchaBusinessKey_VM>>(this, NotifyMe);

            comonDateFormat_VM = new CommonDataGrid_ViewModel<IDateFormat>(GetListDateFormat(), "Date_Format", "Date_Format");
            // comonStripCharacter_VM = new CommonDataGrid_ViewModel<IDateFormat>(GetListDateFormat(), "Strip Character", "Strip Character");
            ListComboxItem = new List<selectedListItemCombo>(AddList());
            comonThresholdError_VM = new CommonDataGrid_ViewModel<IThresholdError>(GetListGridColumnThresholdError(), "ThresholdError", "ThresholdError");
            BtnSaveAddInfo = new RelayCommand(BtnSaveAddInfoExecute, CommandCasCanExecute);
            commandFirst = new RelayCommand(CommandFirstExecute, CommandCasCanExecute);
            commandPrev = new RelayCommand(CommandPrevExecute, CommandCasCanExecute);
            commandNext = new RelayCommand(CommandNextExecute, CommandCasCanExecute);
            commandLast = new RelayCommand(CommandLastExecute, CommandCasCanExecute);
            CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute, CommandCasCanExecute);
            CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute, CommandCasCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(GenericCriteria_VM), NotifyRefreshGrid);
            MapAdditionalInfoFields();
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            CheckECValidator = new RelayCommand(CheckECValidatorExecute, CommandCasCanExecute);
            comonECValidator_VM = new CommonDataGrid_ViewModel<IECValidator>(GetListECValidator(), "ECValidator", "ECValidator");
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        private void NotifyMe(PropertyChangedMessage<EchaBusinessKey_VM> obj)
        {
            SelectedEchaBusinessKey_VM = obj.NewValue;
        }

        private void ButtonClickExecuted(NotificationMessageAction<object> notificationMessageAction)
        {
            string notification = (string)notificationMessageAction.Notification;
            if (notification == "GoToDataGenericsCount")
            {
                GoToCurrentDataView(SelectedEchaBusinessKey_VM.QSID_ID, SelectedEchaBusinessKey_VM.QSID);
            }
            else if (notification == "GoToEchaListEntry")
            {
                SelectedQsidLegislationID = listMapQsidLG.Where(x => x.Qsid_ID == SelectedEchaBusinessKey_VM.QSID_ID).FirstOrDefault();
                IsSelectEchaListEntryKey = true;
                NotifyPropertyChanged("IsSelectEchaListEntryKey");
            }

        }
        private void CommandGoToGenericsExecute(object obj)
        {
            GoToCurrentDataView(SelectedQsidLegislationID.Qsid_ID, SelectedQsidLegislationID.Qsid);
        }
        private void GoToCurrentDataView(int qsid_ID, string qsid)
        {
            Engine.IsOpenCurrentDataViewFromVersionHis = true;
            Engine.CurrentDataViewQsid = qsid;
            Engine.CurrentDataViewQsidID = qsid_ID;
            MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenDataGenerics", NotifyMe));

        }
        private void CallBackRefreshEchaBusinessKeyExecute(NotificationMessageAction<EchaBusinessKey_VM> obj)
        {
            BindEchaBusinessKey();
        }

        private bool commandOpenCurrentDataViewNewAddListCanExecte(object obj)
        {
            return true;
        }
        public void NotifyMe(string callAction)
        {

        }
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public RelayCommand commandOpenCurrentDataViewNewAddList { get; set; }
        private void commandOpenCurrentDataViewNewAddListExected(object obj)
        {

            //Task.Run(() =>
            //{
            try
            {
                Engine.IsOpenCurrentDataViewFromVersionHis = true;
                Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

            }
            catch (Exception ex)
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);
                });
            }
            //});
        }

        List<IStripChar> StripCharacters = new List<IStripChar>();

        private List<IReorder> _lstReorder { get; set; } = new List<IReorder>();
        public List<IReorder> lstReorder
        {
            get { return _lstReorder; }
            set
            {
                if (_lstReorder != value)
                {
                    _lstReorder = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IReorder>>>(new PropertyChangedMessage<List<IReorder>>(null, _lstReorder, "Default List"));
                }
            }
        }

        private void AddDashesExecute(object o)
        {
            UniqueCASCount = "";
            ValidCASCount = "";
            ECAScount = "";
            InvalidCAScount = "";
            AlternateCASCount = "";
            NotifyPropertyChanged("UniqueCASCount");
            NotifyPropertyChanged("ValidCASCount");
            NotifyPropertyChanged("ECAScount");
            NotifyPropertyChanged("InvalidCAScount");
            NotifyPropertyChanged("AlternateCASCount");
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            List<string> fieldName1 = new List<string>();
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
                fieldName1.Add("CAS");
                fieldName1.Add("Editorial");
            }
            else
            {
                fieldName1 = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
            }
            for (int i = 0; i < fieldName1.Count; i++)
            {
                var dtRawChk = objCommon.DbaseQueryReturnTable("select top 1 " + fieldName1[i].ToString() + "  from [" + tableName + "]", CommonFilepath);
                if (dtRawChk.Columns.Count == 0)
                {
                    _notifier.ShowError(fieldName1 + " field not found in selected file!");
                    return;
                }
                if (dtRawChk == null || dtRawChk.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
            }
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            for (int j = 0; j < fieldName1.Count; j++)
            {
                objCommon.DbaseQueryReturnString("update [" + tableName + "] set " + fieldName1[j].ToString() + " = replace(" + fieldName1[j].ToString() + ", '-', '') where isnumeric(replace(" + fieldName1[j].ToString() + ", '-','')) = true and " + fieldName1[j].ToString() + " is not null ", CommonFilepath);

                var dtRaw = objCommon.DbaseQueryReturnTable("Select distinct " + fieldName1[j].ToString() + " from [" + tableName + "] where isnumeric(" + fieldName1[j].ToString() + ") = true and " + fieldName1[j].ToString() + " is not null ", CommonFilepath);
                List<string> invalidCAS = new List<string>();
                for (int i = 0; i < dtRaw.Rows.Count; i++)
                {
                    var chkCAS = objCommon.CheckIsValidCRN(dtRaw.Rows[i].Field<string>(fieldName1[j].ToString()));
                    if (chkCAS == false)
                    {
                        invalidCAS.Add("'" + dtRaw.Rows[i].Field<string>(fieldName1[j].ToString()) + "'");
                    }
                }
                var excludeCas = string.Join(",", invalidCAS);

                if (invalidCAS.Any())
                {
                    objCommon.DbaseQueryReturnString("update [" + tableName + "] set " + fieldName1[j].ToString() + " = mid(" + fieldName1[j].ToString() + ", 1,len(cas)-3) + '-' + mid(" + fieldName1[j].ToString() + ", len(" + fieldName1[j].ToString() + ") -2,2) + '-' +  mid(" + fieldName1[j].ToString() + ", len(" + fieldName1[j].ToString() + "),1) " +
                    " where isnumeric(" + fieldName1[j].ToString() + " ) = true and " + fieldName1[j].ToString() + " is not null and " + fieldName1[j].ToString() + " not in ( " + excludeCas + " )", CommonFilepath);
                }
                else
                {
                    objCommon.DbaseQueryReturnString("update [" + tableName + "] set " + fieldName1[j].ToString() + " = mid(" + fieldName1[j].ToString() + ", 1,len(" + fieldName1[j].ToString() + " ) -3) + '-' + mid(" + fieldName1[j].ToString() + ", len(" + fieldName1[j].ToString() + ") -2,2) + '-' +  mid(cas, len(" + fieldName1[j].ToString() + " ),1) " +
                    " where " + fieldName1[j].ToString() + " is not null and isnumeric(" + fieldName1[j].ToString() + " ) = true ", CommonFilepath);
                }
            }
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}

        }
        private void RemoveDashesExecute(object o)
        {
            UniqueCASCount = "";
            ValidCASCount = "";
            ECAScount = "";
            InvalidCAScount = "";
            AlternateCASCount = "";
            NotifyPropertyChanged("UniqueCASCount");
            NotifyPropertyChanged("ValidCASCount");
            NotifyPropertyChanged("ECAScount");
            NotifyPropertyChanged("InvalidCAScount");
            NotifyPropertyChanged("AlternateCASCount");
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            List<string> fieldName1 = new List<string>();
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
                fieldName1.Add("CAS");
                fieldName1.Add("Editorial");
            }
            else
            {
                fieldName1 = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
            }
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            for (int i = 0; i < fieldName1.Count(); i++)
            {
                var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + fieldName1[i].ToString() + " from [" + tableName + "]", CommonFilepath);
                if (dtRaw.Columns.Count == 0)
                {
                    _notifier.ShowError(fieldName1[i].ToString() + " field not found in selected file!");
                    return;
                }
                if (dtRaw == null || dtRaw.Rows.Count == 0)
                {
                    _notifier.ShowError("No row found in selected file!");
                    return;
                }
                objCommon.DbaseQueryReturnString("update [" + tableName + "] set " + fieldName1[i].ToString() + " = replace(" + fieldName1[i].ToString() + ", '-', '') where isnumeric(replace(" + fieldName1[i].ToString() + ", '-','')) = true and " + fieldName1[i].ToString() + " is not null ", CommonFilepath);
            }
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}

        }
        private void BtnFillEmptyRNExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("Select Ident,RN from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("Ident and RN field not found in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("Ident")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate Ident in this file, first fix them");
                return;
            }
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            var maxNumberStarFrom = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>("RN") != null).Max(x => x.Field<Int32>("RN"));
            maxNumberStarFrom += 1;
            var dtTab = objCommon.DbaseQueryReturnTable("Select (SELECT COUNT(t1.Ident) + " + maxNumberStarFrom + " FROM " + tableName + " t1 WHERE (rn is null) and t1.Ident < t2.Ident) AS NewRN, ident FROM " + tableName + " AS t2 where (rn is null) ORDER BY t2.Ident ", CommonFilepath);
            List<string> listColParam = new List<string>();
            List<string> columnList = new List<string>();
            listColParam.Add("@Ident");
            listColParam.Add("@NewRN");
            columnList.Add("Ident");
            columnList.Add("NewRN");
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (Ident Number, NewRN Number)", CommonFilepath);
            _objAccessVersion_Service.ExportAccessData(dtTab, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
            objCommon.DbaseQueryReturnString("update [" + tableName + "] a inner join TMPDh on cstr(a.Ident) = cstr(TMPDh.Ident) set a.RN = cdbl(TMPDh.NewRN)", CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}
        }
        private void BtnFillRNFromScratchExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("Select Ident,RN from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("Ident and RN field not found in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("Ident")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate Ident in this file, first fix them");
                return;
            }
            var dtTab = objCommon.DbaseQueryReturnTable("Select (SELECT COUNT(t1.Ident) +1 FROM " + tableName + " t1 WHERE t1.Ident < t2.Ident) AS NewRN, ident FROM " + tableName + " AS t2 ORDER BY t2.Ident ", CommonFilepath);
            List<string> listColParam = new List<string>();
            List<string> columnList = new List<string>();
            listColParam.Add("@Ident");
            listColParam.Add("@NewRN");
            columnList.Add("Ident");
            columnList.Add("NewRN");
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (Ident Number, NewRN Number)", CommonFilepath);
            _objAccessVersion_Service.ExportAccessData(dtTab, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
            objCommon.DbaseQueryReturnString("update [" + tableName + "] a inner join TMPDh on cstr(a.Ident) = cstr(TMPDh.Ident) set a.RN = cdbl(TMPDh.NewRN)", CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}
        }
        private void CopyRNToTmpRNExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("Select TMP_RN,RN from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("TMP_RN and RN field not found in this table");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            objCommon.DbaseQueryReturnString("update [" + tableName + "] set TMP_RN = RN", CommonFilepath);
            _notifier.ShowSuccess("Updated in access file.");
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}
        }
        private void ReorderMdbExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("Select TMP_RN,RN from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("TMP_RN and RN fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkTMPRNNull = dtRaw.AsEnumerable().Where(x => x.Field<dynamic>("TMP_RN") == null).ToList();
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("TMP_RN")).Where(y => y.Count() > 1).ToList();
            if (chkTMPRNNull.Any())
            {
                _notifier.ShowError("Found null value in TMP_RN in this file, first fix them");
                return;
            }
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate TMP_RN in this file, first fix them");
                return;
            }

            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("No such fields found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            uploadReorderVis = Visibility.Visible;
            NotifyPropertyChanged("uploadReorderVis");
            Task.Run(() =>
            {
                List<IReorder> lst = new List<IReorder>();
                for (int i = 0; i < dtRaw.Rows.Count; i++)
                {
                    decimal j = 0;
                    string s = Convert.ToString(dtRaw.Rows[i].Field<dynamic>("TMP_RN"));
                    bool result = decimal.TryParse(s, out j);
                    if (result == false)
                    {
                        _notifier.ShowError("non-numeric data in field TMP_RN : " + s);
                        break;
                    }
                    var lstval = new IReorder
                    {
                        OriginalRN = Convert.ToInt32(dtRaw.Rows[i].Field<dynamic>("RN")),
                        TMP_RN = Convert.ToDecimal(dtRaw.Rows[i].Field<dynamic>("TMP_RN")),
                    };
                    lst.Add(lstval);
                }
                int k = 1;
                List<IReorder> lstFinal = new List<IReorder>();
                var onlyOneD = lst.Where(x => x.TMP_RN.ToString().IndexOf(".") < 0).Select(y => Convert.ToInt32(y.TMP_RN.ToString())).ToList();

                var OnlyOneDigit = lst.Where(x => x.TMP_RN.ToString().IndexOf(".") >= 0).Select(y => Convert.ToInt32(y.TMP_RN.ToString().Substring(0, y.TMP_RN.ToString().IndexOf(".")))).Distinct().ToList();
                OnlyOneDigit.AddRange(onlyOneD);
                OnlyOneDigit = OnlyOneDigit.Distinct().ToList();

                OnlyOneDigit.OrderBy(x => x).ToList().ForEach(x =>
                 {
                     var getOnlyDigitValue = lst.Where(y => y.TMP_RN.ToString().IndexOf(".") < 0 && y.TMP_RN == x).FirstOrDefault();
                     if (getOnlyDigitValue != null)
                     {
                         var lstval = new IReorder
                         {
                             OriginalRN = getOnlyDigitValue.OriginalRN,
                             TMP_RN = getOnlyDigitValue.TMP_RN,
                             RevisedRN = k,
                         };
                         lstFinal.Add(lstval);
                         k += 1;
                     }
                     var getEx = lst.Where(y => y.TMP_RN > x && y.TMP_RN < (x + 1)
                     && y.TMP_RN.ToString().IndexOf(".") > 0).ToList();
                     var getEx1 = getEx.Select(z => new
                     {
                         z.OriginalRN,
                         z.RevisedRN,
                         z.TMP_RN,
                         SortValue = Convert.ToInt32(z.TMP_RN.ToString().Substring(z.TMP_RN.ToString().IndexOf(".") + 1))
                     }).ToList();

                     getEx1.OrderBy(y => y.SortValue).ToList().ForEach(P =>
                        {
                            var lstval1 = new IReorder
                            {
                                OriginalRN = P.OriginalRN,
                                TMP_RN = P.TMP_RN,
                                RevisedRN = k,
                            };
                            lstFinal.Add(lstval1);
                            k += 1;
                        });
                 });
                var getMaxRevisedRN = lstFinal.Max(x => x.OriginalRN);
                lstReorder = new List<IReorder>(lstFinal);
                NotifyPropertyChanged("comonReorder_VM");
                var dtTab = objCommon.ConvertToDataTable<IReorder>(lstFinal);
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();

                listColParam.Add("@TMP_RN");
                listColParam.Add("@RevisedRN");
                columnList.Add("TMP_RN");
                columnList.Add("RevisedRN");
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (TMP_RN LongText, RevisedRN Number)", CommonFilepath);
                objCommon.DbaseQueryReturnString("update [" + tableName + "] set RN = rn + " + getMaxRevisedRN, CommonFilepath);
                _objAccessVersion_Service.ExportAccessData(dtTab, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnString("update [" + tableName + "] a inner join TMPDh on cstr(a.TMP_RN) = cstr(TMPDh.TMP_RN) set a.RN = cdbl(TMPDh.RevisedRN)", CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                uploadReorderVis = Visibility.Collapsed;
                NotifyPropertyChanged("uploadReorderVis");
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("Updated in access file.");
                });
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            });
        }
        private bool CommandCasSearchCanExecute(object o)
        {
            if (String.IsNullOrEmpty(CasValid))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool CommandCasCanExecute(object o)
        {

            return true;

        }
        private void DefaultDelimiterDateFormat()
        {
            if (SelectedTable != null && SelectedTable.TableName == "Default")
            {
                var dtRawChk1 = objCommon.DbaseQueryReturnTable("select top 1 * from List_DICTIONARY", CommonFilepath);
                var QsidID = (int)_objLibraryFunction_Service.GetQsid_IDFromLibrary(dtRawChk1.Rows[0]["QSID"].ToString());
                using (var context = new CRAModel())
                {
                    var mapFields = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == QsidID && x.IsPhraseField == true && x.DateFormatID != null).ToList()
                                     join d2 in context.Library_FieldNames.AsNoTracking()
                                     on d1.FieldNameID equals d2.FieldNameID
                                     select new { d2.FieldName, d1.MultiValuePhraseDelimiterID }).Distinct().ToList();

                    if (mapFields.Count == 0)
                    {
                        SelectedDelimiterDateFormat = null;
                        NotifyPropertyChanged("SelectedDelimiterDateFormat");
                    }
                    else
                    {
                        var firstRow = mapFields[0];
                        FieldNameDateFormat = firstRow.FieldName;
                        if (firstRow.MultiValuePhraseDelimiterID != null)
                        {
                            SelectedDelimiterDateFormat = listDelimiterDateFormat.Where(x => x.ID == firstRow.MultiValuePhraseDelimiterID).FirstOrDefault();
                            NotifyPropertyChanged("SelectedDelimiterDateFormat");
                        }
                    }
                }
            }
        }
        private void commandSelectedChangesExecute(object o)
        {
            if (SelectedTable != null && SelectedTable.TableName != "Default")
            {
                BindLabel(SelectedTable);
                listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();
                listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList().ForEach(x =>
                {
                    // add data in listGeneric here
                    listGenericCriteria.Add(new GenericCriteria_VM()
                    {
                        ColumnName = x.ToUpper(),
                        ValueFieldContainOperator = false,
                        ListThresholdType = new List<IComboValue>(_lstThresholdType),
                    });
                });
                NotifyPropertyChanged("listGenericCriteria");
                ExecuteFunction(listGenericCriteria.ToList());
            }
        }
        private void CommandCasSearchExecute(object o)
        {
            //CheckIsValidText = string.Empty;
            //NotifyPropertyChanged("CheckIsValidText");
            //var chk = objCommon.CheckIsValidCRN(CasValid);
            //CheckIsValidText = chk == true ? "Valid CAS" : "InValid CAS";
            //NotifyPropertyChanged("CheckIsValidText");
            //ForegroundColCAS = chk ? "Green" : "Red";
            //NotifyPropertyChanged("ForegroundColCAS");
            BindUploadSingle();
        }

        private void TranslateUpdateinSQLExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "Translations_phrases";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 * from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("No fields found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            uploadTranslate = Visibility.Visible;
            NotifyPropertyChanged("uploadTranslate");
            Tuple<string, string, List<iUserInput>, DataTable> result = null;
            //Task.Run(() =>
            //{
            result = _objLibraryFunction_Service.AddinTranslatedPhrases_Phrases(CommonFilepath, Engine.CurrentUserSessionID, tableName);
            //});
            if (result.Item2 != string.Empty)
            {
                _notifier.ShowError(result.Item2);
                uploadTranslate = Visibility.Collapsed;
                NotifyPropertyChanged("uploadTranslate");
                return;
            }
            if (result.Item3.Count() > 0)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            Popup_UserInput objChangesAdd = new Popup_UserInput(result.Item3);
                            objChangesAdd.ShowDialog();
                        });
            }
            listTranslate = new StandardDataGrid_VM("Map_TranslatedPhrases", result.Item4);
            NotifyPropertyChanged("listTranslate");
            uploadTranslate = Visibility.Collapsed;
            NotifyPropertyChanged("uploadTranslate");
        }
        private void TranslatePhraseExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }

            var lstDic = objCommon.DbaseQueryReturnString("Select top 1 QSID from LIST_DICTIONARY", CommonFilepath);
            var getPhraseFields = objCommon.DbaseQueryReturnTableSql("select distinct a.FieldNameID, FieldName, Delimiter from Map_QSID_FieldName a " +
            " inner join Library_FieldNames b " +
            " on a.FieldNameID = b.FieldNameID inner join Library_QSIDs c " +
            " on a.QSID_ID = c.QSID_ID inner join Library_Languages d " +
            " on a.LanguageID = d.LanguageID left join Library_Delimiters e " +
            " on a.MultiValuePhraseDelimiterID = e.DelimiterID " +
            " where a.IsPhraseField = 1 and c.QSID = '" + lstDic + "' and d.LanguageName = 'English'", CommonListConn);
            string combineFieldNames = string.Empty;
            if (getPhraseFields.Rows.Count > 0)
            {
                var onlyFieldNames = getPhraseFields.AsEnumerable().Select(x => x.Field<string>("FieldName")).ToList();
                combineFieldNames = string.Join(",", onlyFieldNames);
            }
            if (getPhraseFields.Rows.Count == 0)
            {
                _notifier.ShowError("No phrase field found for this QSID");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
                combineFieldNames = string.Join(",", listPhraseFields.Where(x => x.IsPhraseSelected).Select(x => x.FieldNamePhrase).ToList());
            }
            dtRaw = objCommon.DbaseQueryReturnTable("Select " + combineFieldNames + " from [" + tableName + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineFieldNames + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            if (dtRaw.Rows.Count > 0)
            {
                uploadTranslate = Visibility.Visible;
                NotifyPropertyChanged("uploadTranslate");

                Task.Run(() =>
                {

                    List<ITranslate> lstEN = new List<ITranslate>();
                    List<iLibPhr> lstLibPhrases = new List<iLibPhr>();
                    List<IStripChar> StripCharacters = new List<IStripChar>();
                    using (var context = new CRAModel())
                    {
                        StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                    }
                    for (int col = 0; col < dtRaw.Columns.Count; col++)
                    {
                        var fieldName = dtRaw.Columns[col].ColumnName;
                        var chkDelimiter = getPhraseFields.AsEnumerable().Where(x => x.Field<string>("FieldName") == fieldName && x.Field<dynamic>("Delimiter") != null).Select(y => y.Field<string>("Delimiter")).FirstOrDefault();
                        for (int i = 0; i < dtRaw.Rows.Count; i++)
                        {
                            var lstChk = new List<string>();
                            var getPhraseOnly = dtRaw.Rows[i][fieldName] == null ? string.Empty : dtRaw.Rows[i][fieldName].ToString();
                            if (string.IsNullOrEmpty(chkDelimiter))
                            {
                                lstChk.Add(getPhraseOnly);
                            }
                            else
                            {
                                lstChk.AddRange(getPhraseOnly.Split(char.Parse(chkDelimiter)));
                            }
                            lstChk = lstChk.Where(x => string.IsNullOrEmpty(x) == false).ToList();
                            for (int j = 0; j < lstChk.Count; j++)
                            {
                                var First = System.Text.RegularExpressions.Regex.Replace(this.objCommon.RemoveWhitespaceCharacter(lstChk[j].Trim(), StripCharacters), @"\s+", " ").Trim();
                                var FirstHash = objCommon.GenerateSHA256String(First);
                                lstLibPhrases.Add(new iLibPhr
                                {
                                    Phrase = First,
                                    LanguageID = 1,
                                    PhraseHash = FirstHash,
                                });
                                lstEN.Add(new ITranslate
                                {
                                    FieldName = fieldName,
                                    Phrase = First,
                                    PhraseHash = FirstHash,
                                });
                            }
                        }
                    }

                    lstLibPhrases = lstLibPhrases.GroupBy(oo => oo.PhraseHash).Select(x => x.FirstOrDefault()).ToList();

                    // check if Translations_phrases exists or not
                    var chkTP = objCommon.DbaseQueryReturnTable("select top 1 * from Translations_phrases", CommonFilepath);
                    if (chkTP.Columns.Count == 0)
                    {

                        objCommon.DbaseQueryReturnString("CREATE TABLE Translations_phrases (EN LongText, IsActive Yesno, DateFirstAdded DateTime, Frequency Number, FieldNames LongText )", CommonFilepath);
                        List<string> listColParam = new List<string>();
                        listColParam.Add("@EN");
                        listColParam.Add("@IsActive");
                        listColParam.Add("@DateFirstAdded");
                        listColParam.Add("@Frequency");
                        listColParam.Add("@FieldNames");
                        List<string> columnList = new List<string>();
                        columnList.Add("EN");
                        columnList.Add("IsActive");
                        columnList.Add("DateFirstAdded");
                        columnList.Add("Frequency");
                        columnList.Add("FieldNames");
                        DataTable dt = new DataTable();
                        dt.Columns.Add("EN");
                        dt.Columns.Add("IsActive");
                        dt.Columns.Add("DateFirstAdded");
                        dt.Columns.Add("Frequency");
                        dt.Columns.Add("FieldNames");
                        lstLibPhrases.ForEach(x =>
                        {
                            var frequency = lstEN.Count(z => z.PhraseHash == x.PhraseHash);
                            var fieldNames = string.Join(",", lstEN.Where(z => z.PhraseHash == x.PhraseHash).Select(y => y.FieldName).Distinct().ToList());
                            dt.Rows.Add(x.Phrase, 1, objCommon.ESTTime(), frequency, fieldNames);
                        });
                        _objAccessVersion_Service.ExportAccessData(dt, CommonFilepath, "[Translations_phrases]", listColParam.ToArray(), columnList.ToArray());
                    }
                    else
                    {
                        List<string> listColParam = new List<string>();
                        List<string> columnList = new List<string>();
                        DataTable dt = new DataTable();
                        for (int i = 0; i < chkTP.Columns.Count; i++)
                        {
                            listColParam.Add("@" + chkTP.Columns[i].ColumnName);
                            columnList.Add(chkTP.Columns[i].ColumnName);
                            dt.Columns.Add(chkTP.Columns[i].ColumnName);
                        }
                        var recOfAccessFile = objCommon.DbaseQueryReturnTable("select * from Translations_phrases", CommonFilepath);

                        lstLibPhrases.ForEach(x =>
                        {
                            var frequency = lstEN.Count(z => z.PhraseHash == x.PhraseHash);
                            var fieldNames = string.Join(",", lstEN.Where(z => z.PhraseHash == x.PhraseHash).Select(y => y.FieldName).Distinct().ToList());
                            DataRow dtrow = dt.NewRow();
                            dtrow["EN"] = x.Phrase;
                            dtrow["IsActive"] = 1;
                            dtrow["DateFirstAdded"] = objCommon.ESTTime();
                            dtrow["Frequency"] = frequency;
                            dtrow["FieldNames"] = fieldNames;
                            dt.Rows.Add(dtrow.ItemArray);
                        });

                        var addRec = (from d1 in dt.AsEnumerable()
                                      join d2 in recOfAccessFile.AsEnumerable()
                                      on d1.Field<string>("EN")
                                      equals d2.Field<string>("EN")
                                      into tg
                                      from tcheck in tg.DefaultIfEmpty()
                                      where tcheck == null
                                      select d1);
                        DataTable addRecdt = new DataTable();
                        if (addRec.Any())
                        {
                            addRecdt = addRec.CopyToDataTable();
                            ExportAccessData(addRecdt, CommonFilepath, "[Translations_phrases]", listColParam.ToArray(), columnList.ToArray());
                        }
                    }

                    var dtTrans = objCommon.DbaseQueryReturnTable("select * from Translations_phrases", CommonFilepath);
                    if (dtTrans.Rows.Count > 0)
                    {
                        listTranslate = new StandardDataGrid_VM("Translations_phrases", dtTrans);
                        NotifyPropertyChanged("listTranslate");
                    }
                    else
                    {
                        _notifier.ShowError("no row found in Table Translations_phrases");
                    }

                });


                uploadTranslate = Visibility.Collapsed;
                NotifyPropertyChanged("uploadTranslate");
                _notifier.ShowSuccess("Updated in Table Map_TranslatedPhrases");
            }
            else
            {
                _notifier.ShowError("No row found in access file");
            }
        }

        private void ExportAccessData(DataTable list_CasId, string fileName, string TableName, string[] paramterName, string[] paramterVal)
        {
            InserttoDatabase(paramterName, paramterVal, paramterVal, TableName, list_CasId, fileName);
        }
        private static void InserttoDatabase(string[] parameterName, string[] valuePar, string[] datatableFields, string tableName, DataTable dtFromList, string path)
        {
            //DataTable dtFromList = ConvertToDataTable<T>(ListInput);
            var dtDefault = dtFromList.DefaultView.ToTable(false, datatableFields);
            var dtResult = dtDefault.DefaultView.ToTable(false, datatableFields);
            var valueFields = string.Join(", ", parameterName).Replace("-", "");
            var valueParameter = string.Join(", ", valuePar.Select(x => "[" + x + "]").ToList());
            var sql = "SELECT * FROM " + tableName;
            var insertQuery = "INSERT INTO " + tableName + " ( " + valueParameter + " ) VALUES (" + valueFields + " )";
            AddParametersInsertData(parameterName, datatableFields, sql, insertQuery, dtResult, path);
        }
        private static void AddParametersInsertData(string[] parameterName, string[] datatableFields, string sql, string insertQuery, DataTable dtNew, string path)
        {
            var oleConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path);
            var oleAdp = new OleDbDataAdapter(sql, oleConn) { InsertCommand = new OleDbCommand(insertQuery) };
            for (var i = 0; i <= parameterName.GetUpperBound(0); i++)
            {
                oleAdp.InsertCommand.Parameters.Add(parameterName[i], OleDbType.LongVarChar, 3000, datatableFields[i]);
            }
            oleAdp.InsertCommand.Connection = oleConn;
            oleAdp.InsertCommand.Connection.Open();
            oleAdp.Update(dtNew);
            oleAdp.InsertCommand.Connection.Close();
        }
        private void UploadExcelExecute(object o)
        {
            UniqueCASCount = "";
            ValidCASCount = "";
            ECAScount = "";
            InvalidCAScount = "";
            AlternateCASCount = "";
            NotifyPropertyChanged("UniqueCASCount");
            NotifyPropertyChanged("ValidCASCount");
            NotifyPropertyChanged("ECAScount");
            NotifyPropertyChanged("InvalidCAScount");
            NotifyPropertyChanged("AlternateCASCount");
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.DefaultExt = ".mdb";
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                var SelectedMdbPath = SelectedPath[0];
                dtRaw = ConvertExcelToDataTable(SelectedMdbPath);
                lstFinal = CalculateRowsNow();

                UniqueCASCount = "Unique CAS Count - " + lstFinal.Select(x => x.CAS).Distinct().Count();
                ValidCASCount = "Valid CAS Count - " + lstFinal.Where(x => x.Remarks == string.Empty).Select(y => y.CAS).Count();
                ECAScount = "3E CAS Count - " + lstFinal.Where(x => x.New_TMP_RN == "3E_CAS").Select(y => y.CAS).Count();
                InvalidCAScount = "Invalid CAS Count - " + lstFinal.Where(x => x.Remarks != string.Empty).Select(y => y.CAS).Count();
                AlternateCASCount = "Alternate CAS Count - " + lstFinal.Where(x => x.IsAlternateCAS.Value == true).Select(y => y.CAS).Count();
                NotifyPropertyChanged("UniqueCASCount");
                NotifyPropertyChanged("ValidCASCount");
                NotifyPropertyChanged("ECAScount");
                NotifyPropertyChanged("InvalidCAScount");
                NotifyPropertyChanged("AlternateCASCount");
            }
        }

        private void ResetMsToolCommonExecute(object o)
        {
            CommonFilepath = string.Empty;
            NotifyPropertyChanged("CommonFilepath");
            listTableName = null;
            NotifyPropertyChanged("listTableName");
            //blank all grids
            var blankOrder = new List<IReorder>();
            lstReorder = new List<IReorder>(blankOrder);
            NotifyPropertyChanged("comonReorder_VM");
            var blankUpload = new List<IUploadCAS>();
            lstUpload = new List<IUploadCAS>();
            NotifyPropertyChanged("comonUpload_VM");
            var blankSubroot = new List<ISubroot>();
            listSubroots = new List<ISubroot>(blankSubroot);
            NotifyPropertyChanged("comonSubroots_VM");
            var blankAccess = new List<IAccessData>();
            listAccess = new List<IAccessData>(blankAccess);
            NotifyPropertyChanged("comonAccess_VM");
            var blankGreek = new List<IInvalidGreek>();
            listInvalidGreek = new List<IInvalidGreek>(blankGreek);
            NotifyPropertyChanged("comonInvalid_VM");
            var blankIInvalidChar = new List<IInvalidChar>();
            listInvalid = new List<IInvalidChar>(blankIInvalidChar);
            NotifyPropertyChanged("comonInvalid_VM");
            var blankUniqueChar = new List<IUniqueCharacter>();
            lisUniqueChr = new List<IUniqueCharacter>(blankUniqueChar);
            NotifyPropertyChanged("comonUniqueChr_VM");
            listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();
            NotifyPropertyChanged("comonThresholdError_VM");
            listThresholdError = new List<IThresholdError>();
            NotifyPropertyChanged("comonThresholdError_VM");
            listTranslate = null;
            listNotecodeConsistency = null;
            listNoteTranslate = null;
            listOmit = null;
            listStripCharacter = null;
            listDateFormat = null;
            NotifyPropertyChanged("listTranslate");
            NotifyPropertyChanged("listNotecodeConsistency");
            NotifyPropertyChanged("listNoteTranslate");
            NotifyPropertyChanged("listOmit");
            NotifyPropertyChanged("listDateFormat");
            NotifyPropertyChanged("listStripCharacter");
            DefaultLableUniqueChar = string.Empty;
            DefaultLableNotes = string.Empty;
            DefaultLablePhrase = string.Empty;
            DefaultLableReorder = string.Empty;
            DefaultLableCAS = string.Empty;
            DefaultLableNoteCodeConsistency = string.Empty;
            DefaultLableRemoveWhiteCharacters = string.Empty;
            DefaultLableSubroots = string.Empty;
            DefaultLableOmit = string.Empty;
            DefaultLableAccess = string.Empty;
            DefaultLableGreek = string.Empty;
            DefaultLableIdentifyDuplicate = string.Empty;
            DefaultLableFlagGeneric = string.Empty;
            DefaultFieldSquash = string.Empty;
            DefaultLableDateFormat = string.Empty;
            DefaultLableStripCharacter = string.Empty;
            DefaultLableThreshold = string.Empty;
            DefaultLableECValidator = string.Empty;
            DefaultLableCheckUnits = string.Empty;
            DefaultLableNameAssignment = string.Empty;
            NotifyPropertyChanged("DefaultLableUniqueChar");
            NotifyPropertyChanged("DefaultLableNotes");
            NotifyPropertyChanged("DefaultLablePhrase");
            NotifyPropertyChanged("DefaultLableReorder");
            NotifyPropertyChanged("DefaultLableCAS");
            NotifyPropertyChanged("DefaultLableNoteCodeConsistency");
            NotifyPropertyChanged("DefaultLableRemoveWhiteCharacters");
            NotifyPropertyChanged("DefaultLableSubroots");
            NotifyPropertyChanged("DefaultLableOmit");
            NotifyPropertyChanged("DefaultLableAccess");
            NotifyPropertyChanged("DefaultLableGreek");
            NotifyPropertyChanged("DefaultLableIdentifyDuplicate");
            NotifyPropertyChanged("DefaultLableFlagGeneric");
            NotifyPropertyChanged("DefaultFieldSquash");
            NotifyPropertyChanged("DefaultLableDateFormat");
            NotifyPropertyChanged("DefaultLableStripCharacter");
            NotifyPropertyChanged("DefaultLableThreshold");
            NotifyPropertyChanged("DefaultLableECValidator");
            NotifyPropertyChanged("DefaultLableCheckUnits");
            NotifyPropertyChanged("DefaultLableNameAssignment");
        }
        private void UploadMdbCommonExecute(object o)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.DefaultExt = ".mdb";
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                CommonFilepath = SelectedPath[0];
                NotifyPropertyChanged("CommonFilepath");
            }
            var result = objCommon.GetDataTablesNameFromAccess(CommonFilepath);
            var result1 = new List<iTableName>();
            result1.Add(new iTableName { TableName = "Default" });
            result.ForEach(x =>
            {
                result1.Add(new iTableName { TableName = x });
            });
            listTableName = new List<iTableName>(result1);
            NotifyPropertyChanged("listTableName");


            IsSelectedAllChecks = true;
            NotifyPropertyChanged("IsSelectedAllChecks");

            //blank all grids
            var blankOrder = new List<IReorder>();
            lstReorder = new List<IReorder>(blankOrder);
            NotifyPropertyChanged("comonReorder_VM");
            var blankUpload = new List<IUploadCAS>();
            lstUpload = new List<IUploadCAS>();
            NotifyPropertyChanged("comonUpload_VM");
            var blankSubroot = new List<ISubroot>();
            listSubroots = new List<ISubroot>(blankSubroot);
            NotifyPropertyChanged("comonSubroots_VM");
            var blankAccess = new List<IAccessData>();
            listAccess = new List<IAccessData>(blankAccess);
            NotifyPropertyChanged("comonAccess_VM");
            var blankGreek = new List<IInvalidGreek>();
            listInvalidGreek = new List<IInvalidGreek>(blankGreek);
            NotifyPropertyChanged("comonInvalid_VM");
            var blankIInvalidChar = new List<IInvalidChar>();
            listInvalid = new List<IInvalidChar>(blankIInvalidChar);
            NotifyPropertyChanged("comonInvalid_VM");
            var blankUniqueChar = new List<IUniqueCharacter>();
            lisUniqueChr = new List<IUniqueCharacter>(blankUniqueChar);
            NotifyPropertyChanged("comonUniqueChr_VM");
            listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();
            NotifyPropertyChanged("comonThresholdError_VM");
            listThresholdError = new List<IThresholdError>();
            NotifyPropertyChanged("comonThresholdError_VM");
            listTranslate = null;
            listNotecodeConsistency = null;
            listNoteTranslate = null;
            listOmit = null;
            listStripCharacter = null;
            listDateFormat = null;
            NotifyPropertyChanged("listTranslate");
            NotifyPropertyChanged("listNotecodeConsistency");
            NotifyPropertyChanged("listNoteTranslate");
            NotifyPropertyChanged("listOmit");
            NotifyPropertyChanged("listDateFormat");
            NotifyPropertyChanged("listStripCharacter");
            SelectedTable = listTableName.Where(x => x.TableName == "Default").FirstOrDefault();
            NotifyPropertyChanged("SelectedTable");

        }
        private void CheckExistingValues(Int32 qsidID)
        {
            using (CRAModel context = new CRAModel())
            {
                var virtualUnitField = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID && x.UnitFieldNameID != null && x.UnitID != null).Select(y => y.UnitFieldNameID).ToList();
                var virtualUnitField1 = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID && x.OperatorID != null && x.OperatorFieldNameID != null).Select(y => y.OperatorFieldNameID).Distinct().ToList();
                virtualUnitField.AddRange(virtualUnitField1);
                var chkInMapQSIDInternalOnly = (from d1 in context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsidID && x.IsInternalOnly == false && !virtualUnitField.Contains(x.FieldNameID) && (x.IsMinValue == true || x.IsMaxValue == true || x.IsMaxValOp == true || x.IsMinValOp == true || x.IsUnit == true || x.ValueFieldContainsRanges == true)).ToList()
                                                join d2 in context.Library_FieldNames.AsNoTracking().ToList()
                                                on d1.FieldNameID equals d2.FieldNameID
                                                select new { d1, d2.FieldName }).ToList();
                for (int i = 0; i < chkInMapQSIDInternalOnly.Count(); i++)
                {
                    listGenericCriteria.Add(new GenericCriteria_VM()
                    {
                        ColumnName = chkInMapQSIDInternalOnly[i].FieldName.ToUpper(),
                        ValueFieldContainOperator = false,
                        ListThresholdType = new List<IComboValue>(_lstThresholdType),
                    });
                }
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    var fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    listGenericCriteria[i].ValueFieldContainOperator = false;
                    if (fieldDet != null)
                    {
                        if (fieldDet.IsMinValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            //NotifyPropertyChanged("MaxFieldName");
                            NotifyPropertyChanged("SelectedCatgory");

                        }
                        if (fieldDet.IsMaxValue)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            //NotifyPropertyChanged("MaxFieldName");
                            NotifyPropertyChanged("SelectedCatgory");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");

                        }
                        if (fieldDet.IsUnit)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Unit").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                            //NotifyPropertyChanged("MaxFieldName");
                        }
                        if (fieldDet.ValueFieldContainsRanges)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Value Range").FirstOrDefault();
                            var CatId = fieldDet.ThresholdCategory == 0 || fieldDet.ThresholdCategory == null ? 0 : fieldDet.ThresholdCategory;
                            listGenericCriteria[i].SelectedCatgory = listGenericCriteria[i].ListCategory.Where(x => x.ID == CatId).FirstOrDefault();
                            NotifyPropertyChanged("SelectedCatgory");
                            var rangeDel = fieldDet.RangeDelimiterID == 0 || fieldDet.RangeDelimiterID == null ? 0 : fieldDet.RangeDelimiterID;
                            listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.DelimiterID == rangeDel).FirstOrDefault();
                            NotifyPropertyChanged("SelectedDelimiter");
                            listGenericCriteria[i].SelectedConsiderSingleValues = listGenericCriteria[i].ListConsiderSingleValues.Where(x => x.CboValue == fieldDet.ConsiderSingleValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderSingleValues");
                            listGenericCriteria[i].SelectedConsiderNullValues = listGenericCriteria[i].ListConsiderNullValues.Where(x => x.CboValue == fieldDet.ConsiderNullValueAs).FirstOrDefault();
                            NotifyPropertyChanged("SelectedConsiderNullValues");
                        }
                        if (fieldDet.IsMinValOp)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Min Value Operator").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                        }
                        if (fieldDet.IsMaxValOp)
                        {
                            listGenericCriteria[i].SelectedThresholdType = listGenericCriteria[i].ListThresholdType.Where(x => x.CboValue == "Max Value Operator").FirstOrDefault();
                            NotifyPropertyChanged("SelectedThresholdType");
                        }
                    }
                }
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    var fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    if (fieldDet != null)
                    {
                        if (fieldDet.IsMaxValue || fieldDet.ValueFieldContainsRanges)
                        {
                            if ((fieldDet.UnitID != 0) && (fieldDet.UnitID != null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Manual Unit Definition").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedUnit = listGenericCriteria[i].ListUnit.Where(x => x.UnitID == fieldDet.UnitID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedUnit");
                            }
                            if ((fieldDet.UnitFieldNameID != 0) && (fieldDet.UnitFieldNameID != null) && ((fieldDet.UnitID == 0) || (fieldDet.UnitID == null)))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.UnitFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                                listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedMaxFieldName");
                            }

                            if (fieldDet.UnitDelimiterID != null)
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit Delimitor").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.Delimiter == " ").FirstOrDefault();
                                NotifyPropertyChanged("SelectedDelimiter");
                            }
                            if ((fieldDet.UnitID == null) && (fieldDet.UnitFieldNameID == null) && (fieldDet.UnitDelimiterID == null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "No Unit").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                            }
                            if (fieldDet.ValueFieldContainsOperators)
                            {
                                listGenericCriteria[i].ValueFieldContainOperator = fieldDet.ValueFieldContainsOperators;
                                NotifyPropertyChanged("ValueFieldContainOperator");
                            }
                            if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMinValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperator");
                            }
                            if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMaxValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperator");
                            }
                            if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMinValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperatorFieldName");
                            }
                            if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMaxValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperatorFieldName");
                            }
                        }
                        //else
                        //{
                        //    ////if ((fieldDet.Operator_ValueFieldID != 0) && (fieldDet.Operator_ValueFieldID != null))
                        //    ////{
                        //    ////    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.Operator_ValueFieldID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                        //    ////    listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                        //    ////    NotifyPropertyChanged("SelectedMaxFieldName");
                        //    ////}
                        //}

                    }
                }
                for (int i = 0; i < listGenericCriteria.Count; i++)
                {
                    var fldName = listGenericCriteria[i].ColumnName;
                    var fieldDet = chkInMapQSIDInternalOnly.Where(x => x.FieldName == fldName).Select(y => y.d1).FirstOrDefault();
                    if (fieldDet != null)
                    {
                        if (fieldDet.IsMinValue)
                        {
                            if ((fieldDet.UnitID != 0) && (fieldDet.UnitID != null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Manual Unit Definition").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedUnit = listGenericCriteria[i].ListUnit.Where(x => x.UnitID == fieldDet.UnitID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedUnit");
                            }
                            if ((fieldDet.UnitFieldNameID != 0) && (fieldDet.UnitFieldNameID != null) && ((fieldDet.UnitID == 0) || (fieldDet.UnitID == null)))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.UnitFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                                listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedMaxFieldName");
                            }

                            if (fieldDet.UnitDelimiterID != null)
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "Unit Delimitor").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                                listGenericCriteria[i].SelectedDelimiter = listGenericCriteria[i].ListDelimiter.Where(x => x.Delimiter == " ").FirstOrDefault();
                                NotifyPropertyChanged("SelectedDelimiter");
                            }
                            if ((fieldDet.UnitID == null) && (fieldDet.UnitFieldNameID == null) && (fieldDet.UnitDelimiterID == null))
                            {
                                listGenericCriteria[i].SelectedThresholdUnitType = listGenericCriteria[i].ThresholdUnitType.Where(x => x.CboValue == "No Unit").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdUnitType");
                            }
                            if (fieldDet.ValueFieldContainsOperators)
                            {
                                listGenericCriteria[i].ValueFieldContainOperator = fieldDet.ValueFieldContainsOperators;
                                NotifyPropertyChanged("ValueFieldContainOperator");
                            }
                            if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMinValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperator");
                            }
                            if (fieldDet.OperatorID != 0 && fieldDet.OperatorID != null && fieldDet.IsMaxValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Manual Operator").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                listGenericCriteria[i].SelectedOperator = listGenericCriteria[i].ListOperator.Where(x => x.OperatorID == fieldDet.OperatorID).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperator");
                            }
                            if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMinValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperatorFieldName");
                            }
                            if ((fieldDet.OperatorFieldNameID != 0) && (fieldDet.OperatorFieldNameID != null) && (fieldDet.OperatorID == 0 || fieldDet.OperatorID == null) && fieldDet.IsMaxValue == true)
                            {
                                listGenericCriteria[i].SelectedThresholdOperatorType = listGenericCriteria[i].ThresholdOperatorType.Where(x => x.CboValue == "Operator FieldName").FirstOrDefault();
                                NotifyPropertyChanged("SelectedThresholdOperatorType");
                                var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.OperatorFieldNameID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();
                                listGenericCriteria[i].SelectedOperatorFieldName = listGenericCriteria[i].OperatorFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                                NotifyPropertyChanged("SelectedOperatorFieldName");
                            }
                        }
                        //else
                        //{
                        //    ////if ((fieldDet.Operator_ValueFieldID != 0) && (fieldDet.Operator_ValueFieldID != null))
                        //    ////{
                        //    ////    var fieldNameid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldNameID == fieldDet.Operator_ValueFieldID).Select(y => y.FieldName.ToUpper()).FirstOrDefault();

                        //    ////    listGenericCriteria[i].SelectedMaxFieldName = listGenericCriteria[i].MaxFieldName.Where(x => x.CboValue == fieldNameid).FirstOrDefault();
                        //    ////    NotifyPropertyChanged("SelectedMaxFieldName");
                        //    ////}
                        //}

                    }
                }
            }
        }
        private void UploadMdbExecute(object o)
        {
            UniqueCASCount = "";
            ValidCASCount = "";
            ECAScount = "";
            InvalidCAScount = "";
            AlternateCASCount = "";
            NotifyPropertyChanged("UniqueCASCount");
            NotifyPropertyChanged("ValidCASCount");
            NotifyPropertyChanged("ECAScount");
            NotifyPropertyChanged("InvalidCAScount");
            NotifyPropertyChanged("AlternateCASCount");
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            var tableName = TabName;
            if (TabName.ToString() == "Default")
            {
                tableName = "DATA";
            }

            if (checkBoxInvalid)
            {
                dtRaw = objCommon.DbaseQueryReturnTable("Select CAS,RN,INV_CAS,Editorial,Omit from [" + tableName + "] where omit is null or omit =''", CommonFilepath);
                if (dtRaw.Columns.Contains("CAS") && dtRaw.Columns.Contains("Editorial") && dtRaw.Columns.Contains("RN") && dtRaw.Columns.Contains("INV_CAS") && dtRaw.Columns.Contains("Omit"))
                {
                }
                else
                {
                    _notifier.ShowError("CAS,RN,INV_CAS,Editorial,OMIT column not found in selected file!");
                    return;
                }
            }
            else
            {
                dtRaw = objCommon.DbaseQueryReturnTable("Select CAS,Omit from [" + tableName + "] where omit is null or omit =''", CommonFilepath);
                if (!dtRaw.Columns.Contains("CAS") && dtRaw.Columns.Contains("Omit"))
                {
                    _notifier.ShowError("CAS column not found in selected file!");
                    return;
                }
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            var chkNUllCAs = dtRaw.AsEnumerable().Count(x => string.IsNullOrEmpty(x.Field<string>("CAS")));
            if (chkNUllCAs > 0)
            {
                _notifier.ShowError(chkNUllCAs.ToString() + " rows without CAS in table");
                return;
            }
            dtRaw.Columns.Remove("Omit");

            uploadCASVis = Visibility.Visible;
            NotifyPropertyChanged("uploadCASVis");
            Task.Run(() =>
            {
                var lst = CalculateRowsNow();
                if (checkBoxInvalid != false)
                {
                    //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                    for (int i = 0; i < lst.Count; i++)
                    {
                        string extraQuery = string.Empty;
                        if (!string.IsNullOrEmpty(lst[i].New_TMP_RN))
                        {
                            extraQuery += ", TMP_RN ='" + lst[i].New_TMP_RN + "'";
                        }

                        if (dtRaw.Columns.Contains("INV_CAS") && !string.IsNullOrEmpty(lst[i].INV_CAS))
                        {
                            extraQuery += ", INV_CAS ='" + lst[i].INV_CAS + "'";
                        }
                        if (dtRaw.Columns.Contains("Editorial") && !string.IsNullOrEmpty(lst[i].EDITORIAL))
                        {
                            extraQuery += ", Editorial ='" + lst[i].EDITORIAL + "'";
                        }
                        objCommon.DbaseQueryReturnString("update [" + tableName + "] set CAS = '" + lst[i].CAS + "'" + extraQuery + " where RN = " + lst[i].RN, CommonFilepath);

                    }
                    //if (isFileOpen)
                    //{
                    //    objCommon.OpenFile(CommonFilepath);
                    //}
                }
                UniqueCASCount = "Unique CAS Count - " + lst.Select(x => x.CAS).Distinct().Count();
                ValidCASCount = "Valid CAS Count - " + lst.Where(x => x.Remarks == string.Empty).Select(y => y.CAS).Count();
                ECAScount = "3E CAS Count - " + lst.Where(x => x.New_TMP_RN == "3E_CAS").Select(y => y.CAS).Count();
                InvalidCAScount = "Invalid CAS Count - " + lst.Where(x => x.Remarks != string.Empty).Select(y => y.CAS).Count();
                AlternateCASCount = "Alternate CAS Count - " + lst.Where(x => x.IsAlternateCAS.Value == true).Select(y => y.CAS).Count();
                NotifyPropertyChanged("UniqueCASCount");
                NotifyPropertyChanged("ValidCASCount");
                NotifyPropertyChanged("ECAScount");
                NotifyPropertyChanged("InvalidCAScount");
                NotifyPropertyChanged("AlternateCASCount");

                uploadCASVis = Visibility.Collapsed;
                NotifyPropertyChanged("uploadCASVis");
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowSuccess("Updated in access file.");
                });

            });
        }
        private List<IUploadCAS> CalculateRowsNow()
        {
            if (dtRaw.Columns.Contains("RN"))
            {
                var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
                if (chkRNDuplicate.Any())
                {
                    _notifier.ShowError("Found duplicate RN in this file, first fix them");
                    return null;
                }
            }
            string fieldNames = string.Empty;
            if (dtRaw == null)
            {
                _notifier.ShowError("No row found in selected file!");
                return null;
            }
            if (dtRaw.Columns.Contains("INV_CAS"))
            {
                fieldNames = fieldNames + "'INV_CAS'" + ",";
            }
            if (dtRaw.Columns.Contains("EDITORIAL"))
            {
                fieldNames = fieldNames + "'EDITORIAL'" + ",";
            }
            var SAPLibUnitTable = new List<IUploadCAS>();

            using (var context = new CRAModel())
            {
                var chklibCAS = (from d1 in dtRaw.AsEnumerable()
                                 join d2 in context.Library_CAS.AsNoTracking()
                                 on d1.Field<string>("CAS").Replace("-", "") equals d2.CAS.Replace("-", "")
                                 select new { d2.CASID, d2.CAS }).Distinct().ToList();
                var chkAlternateCAS = (from d1 in chklibCAS
                                       join d2 in context.Map_AlternateCAS.AsNoTracking()
                                       on d1.CASID equals d2.AlternateCASID
                                       select new { d1.CASID, d1.CAS }).Distinct().ToList();

                dtRaw.AsEnumerable().Where(x => x.Field<string>("CAS") != null).ToList().ForEach(x =>
                 {
                     var CheckValid = objCommon.CheckIsValidCRN(x.Field<string>("CAS").Replace("-", ""));
                     var checkDigitONly = IsDigitsOnly(x.Field<string>("CAS").Replace("-", ""));
                     var NewCASCheck = _objLibraryFunction_Service.NewCASInvalidCheck(x.Field<string>("CAS"));
                     SAPLibUnitTable.Add(new IUploadCAS
                     {
                         RN = dtRaw.Columns.Contains("RN") ? Convert.ToInt32(x.Field<dynamic>("RN")) : null,
                         CAS = x.Field<string>("CAS").Replace("-", ""),
                         EDITORIAL = fieldNames.Contains("EDITORIAL") ? x.Field<string>("EDITORIAL") != null ? x.Field<string>("EDITORIAL").Replace("-", "") : string.Empty : string.Empty,
                         IsValid = CheckValid ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                         Remarks = CheckValid ? string.Empty : NewCASCheck && !checkDigitONly ? string.Empty : "Invalid CAS",

                         INV_CAS = fieldNames.Contains("INV_CAS") && !CheckValid && checkDigitONly ? x.Field<string>("CAS").Replace("-", "") : string.Empty,

                         New_TMP_RN = !CheckValid ? NewCASCheck && !checkDigitONly ? "3E_CAS" : "INVCAS" : string.Empty,

                         Is_3E_CAS = checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                         IsAlternateCAS = chkAlternateCAS.Count(xx => xx.CAS.Replace("-", "") == x.Field<string>("CAS").Replace("-", "")) > 0 ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                     });
                 });
                SAPLibUnitTable.ForEach(x =>
                {
                    var CheckValid = x.EDITORIAL != string.Empty ? objCommon.CheckIsValidCRN(x.EDITORIAL.Replace("-", "")) : true;
                    var checkDigitONly = IsDigitsOnly(x.EDITORIAL.Replace("-", ""));
                    var NewCASCheck = _objLibraryFunction_Service.NewCASInvalidCheck(x.EDITORIAL);
                    if (CheckValid == false)
                    {
                        x.Is_3E_CAS = checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault();
                        x.New_TMP_RN = !CheckValid ? NewCASCheck && !checkDigitONly ? "3E_CAS" : "INVCAS" : string.Empty;
                        x.IsValid = CheckValid ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault();
                        x.Remarks += CheckValid ? string.Empty : NewCASCheck && !checkDigitONly ? string.Empty : "Invalid Editorial";
                        x.IsAlternateCAS = chkAlternateCAS.Count(xx => xx.CAS.Replace("-", "") == x.EDITORIAL.Replace("-", "")) > 0 ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault();
                    }
                });
            }
            lstUpload = new List<IUploadCAS>(SAPLibUnitTable);
            NotifyPropertyChanged("comonUpload_VM");
            return SAPLibUnitTable;
        }
        public static DataTable ConvertExcelToDataTable(string FileName)
        {
            DataTable dtResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            using (OleDbConnection objConn = new OleDbConnection(
            "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
                return dtResult; //Returning Dattable  
            }
        }
        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
        private List<IUploadCAS> _lstUpload { get; set; } = new List<IUploadCAS>();
        public List<IUploadCAS> lstUpload
        {
            get { return _lstUpload; }
            set
            {
                if (_lstUpload != value)
                {
                    _lstUpload = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IUploadCAS>>>(new PropertyChangedMessage<List<IUploadCAS>>(null, _lstUpload, "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnUploadCAS()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RN", ColBindingName = "RN", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "INV_CAS", ColBindingName = "INV_CAS", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EDITORIAL", ColBindingName = "EDITORIAL", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsValid", ColBindingName = "IsValid", ColType = "Checkbox", ColumnWidth = "120" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Remarks", ColBindingName = "Remarks", ColType = "Textbox", ColumnWidth = "250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is_3E_CAS", ColBindingName = "Is_3E_CAS", ColType = "Checkbox", ColumnWidth = "120" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsAlternateCAS", ColBindingName = "IsAlternateCAS", ColType = "Checkbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "New_TMP_RN", ColBindingName = "New_TMP_RN", ColType = "Textbox", ColumnWidth = "200" });

            return listColumnQsidDetail;
        }

        private List<CommonDataGridColumn> GetListGridColumnReorder()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "OriginalRN", ColBindingName = "OriginalRN", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TMP_RN", ColBindingName = "TMP_RN", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RevisedRN", ColBindingName = "RevisedRN", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }
        public List<CommonDataGridColumn> GetListGridColumnEncryptHexa()
        {
            List<CommonDataGridColumn> listColumnEncryptHexa = new();
            listColumnEncryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Input Base Val", ColBindingName = "InputBaseVal", ColType = "Textbox", ColumnWidth = "200" });
            listColumnEncryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Output Hexa Val", ColBindingName = "OutputHexaVal", ColType = "Textbox", ColumnWidth = "200" });
            listColumnEncryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Property Name", ColBindingName = "PropertyName", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnEncryptHexa;
        }

        public List<CommonDataGridColumn> GetListGridColumnDecryptHexa()
        {
            List<CommonDataGridColumn> listColumnDecryptHexa = new();
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Input Hexa Val", ColBindingName = "InputHexaVal", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Output Base Val", ColBindingName = "OutputBaseVal", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Property Name", ColBindingName = "PropertyName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RowID", ColBindingName = "QsCasIDDetail.RowID", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "QsCasIDDetail.Qsid", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RN", ColBindingName = "QsCasIDDetail.RN", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnDecryptHexa;
        }

        public List<CommonDataGridColumn> GetListGridColumnEchaBusinessKey()
        {
            List<CommonDataGridColumn> listColumnDecryptHexa = new();
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ECHA list entry business key", ColBindingName = "EchaListEntryBusinessKey", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSCASID", ColBindingName = "QSCASID", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "RowID", ColBindingName = "RowID", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ECHA List business key", ColBindingName = "EchaListBusinessKey", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CASID", ColBindingName = "CasId", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "Cas", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EnglishNameOnList", ColBindingName = "EnglishNameOnList", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Access file RN", ColBindingName = "AccessFileRN", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnDecryptHexa;
        }
        public List<CommonDataGridColumn> GetColumnEchaBusinessKey()
        {
            List<CommonDataGridColumn> listColumnDecryptHexa = new();
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColFilterMemberPath = "QSID", ColType = "Button", ContentName = "None", ColumnWidth = "200", BackgroundColor = "#95D1CC", CommandParam = "GoToEchaListEntry" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ECHA Legislation List Business Key", ColBindingName = "EchaLegislationListBusinessKey", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ECHA Legislation Business Key", ColBindingName = "EchaLegislationBusinessKey", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Field Name", ColBindingName = "ListFieldName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Short Name", ColBindingName = "ListShortName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "List Phrase", ColBindingName = " ListPhrase", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Legislation Short Name", ColBindingName = "LegislationShortName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Legislation Phrase", ColBindingName = "LegislationPhrase", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDecryptHexa.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSID_ID", ColType = "Button", ContentName = "Data + Generics Counts", BackgroundColor = "#B91646", CommandParam = "GoToDataGenericsCount", ColumnWidth = "300" });


            return listColumnDecryptHexa;
        }
        private void BindUploadSingle()
        {
            using (var context = new CRAModel())
            {
                var chklibCAS = context.Library_CAS.AsNoTracking().Where(y => y.CAS.Replace("-", "") == CasValid.Replace("-", "")).Select(z => new { z.CASID, z.CAS }).Distinct().ToList();
                var chkAlternateCAS = (from d1 in chklibCAS
                                       join d2 in context.Map_AlternateCAS.AsNoTracking()
                                       on d1.CASID equals d2.AlternateCASID
                                       select new { d1.CASID, d1.CAS }).Distinct().ToList();
                Task.Run(() =>
            {
                var SAPLibUnitTable = new List<IUploadCAS>();
                var CheckValid = objCommon.CheckIsValidCRN(CasValid.Replace("-", ""));
                var checkDigitONly = IsDigitsOnly(CasValid.Replace("-", ""));
                var NewCASCheck = _objLibraryFunction_Service.NewCASInvalidCheck(CasValid);
                SAPLibUnitTable.Add(new IUploadCAS
                {
                    RN = null,
                    CAS = CasValid.Replace("-", ""),
                    EDITORIAL = "",
                    INV_CAS = "",
                    IsValid = CheckValid ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                    Remarks = CheckValid ? string.Empty : NewCASCheck && !checkDigitONly ? string.Empty : "Invalid CAS",
                    New_TMP_RN = !CheckValid ? NewCASCheck && !checkDigitONly ? "3E_CAS" : "INVCAS" : string.Empty,
                    Is_3E_CAS = checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault() : NewCASCheck && !checkDigitONly ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() :
                    Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),
                    IsAlternateCAS = chkAlternateCAS.Count(xx => xx.CAS.Replace("-", "") == CasValid.Replace("-", "")) > 0 ? Engine._listCheckboxType.Where(xx => xx.Value == true).FirstOrDefault() : Engine._listCheckboxType.Where(xx => xx.Value == false).FirstOrDefault(),

                });
                lstUpload = new List<IUploadCAS>(SAPLibUnitTable);
                UniqueCASCount = "";
                ValidCASCount = "";
                ECAScount = "";
                InvalidCAScount = "";
                AlternateCASCount = "";
                NotifyPropertyChanged("UniqueCASCount");
                NotifyPropertyChanged("ValidCASCount");
                NotifyPropertyChanged("ECAScount");
                NotifyPropertyChanged("InvalidCAScount");
                NotifyPropertyChanged("AlternateCASCount");
            });

                NotifyPropertyChanged("comonUpload_VM");
            }
        }



        private void CommandConvertHexaToIDExecute(object obj)
        {
            int decValue = 0;
            try
            {
                decValue = Convert.ToInt32(inputIDDecrypt, 16);
            }
            catch (Exception ex)
            {
                decValue = decode(inputIDDecrypt, 36);
                decValue = decValue - 967458816;
                decValue = decValue + 16777215;
            }
            var listDetail = _objLibraryFunction_Service.getDetailEchaByQsCasId(decValue);

            var list = listPropertyDecryptHexa;
            list.Add(new Property_DecryptHexa_VM { InputHexaVal = inputIDDecrypt, OutputBaseVal = decValue.ToString(), PropertyName = listDetail.Count > 0 ? "QsCasID" : "", QsCasIDDetail = listDetail.Count > 0 ? listDetail.First() : new() });
            listPropertyDecryptHexa = new List<Property_DecryptHexa_VM>(list);
            inputIDDecrypt = string.Empty;
            NotifyPropertyChanged("inputIDDecrypt");
        }
        public int decode(string sIn, int nBase)
        {
            int n = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".IndexOf(sIn[^1]);
            string s = sIn[..^1];
            return s != "" ? decode(s, nBase) * nBase + n : n;
        }


        private void CommandConvertIdToHexaExecute(object obj)
        {
            if (!string.IsNullOrEmpty(inputID))
            {
                string output = ConvertHexa(inputID);
                var list = listPropertyEncryptHexa;
                list.Add(new Property_EncryptHexa_VM { InputBaseVal = inputID, OutputHexaVal = output.ToString(), PropertyName = "" });
                listPropertyEncryptHexa = new List<Property_EncryptHexa_VM>(list);
                inputID = string.Empty;
                NotifyPropertyChanged("inputID");
            }
            else if (!string.IsNullOrEmpty(inputQsid) && !string.IsNullOrEmpty(inputRowID))
            {
                var listQsCasId = _objLibraryFunction_Service.GetQsCasIDList(inputQsid, Convert.ToInt32(inputRowID));
                List<Property_EncryptHexa_VM> listHexa = new List<Property_EncryptHexa_VM>();
                foreach (var item in listQsCasId)
                {
                    string output = ConvertHexa(item.ToString());
                    listHexa.Add(new Property_EncryptHexa_VM { InputBaseVal = item.ToString(), OutputHexaVal = output.ToString(), PropertyName = "QsCasId" });
                }
                listPropertyEncryptHexa = new List<Property_EncryptHexa_VM>(listHexa);
            }
            else
            {
                var listSelectedFields = listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList();
                if (listSelectedFields.Count > 0 && TabName != "Default")
                {
                    List<Property_EncryptHexa_VM> listHexa = new List<Property_EncryptHexa_VM>();
                    foreach (var item in listSelectedFields)
                    {
                        var dtRaw = objCommon.DbaseQueryReturnTable("Select distinct " + item.ToString() + " from [" + TabName + "]", CommonFilepath);
                        for (var i = 0; i < dtRaw.Rows.Count; i++)
                        {
                            string output = ConvertHexa(dtRaw.Rows[i][0].ToString());
                            listHexa.Add(new Property_EncryptHexa_VM { InputBaseVal = dtRaw.Rows[i][0].ToString(), OutputHexaVal = output.ToString(), PropertyName = item });
                        }

                    }
                    listPropertyEncryptHexa = new List<Property_EncryptHexa_VM>(listHexa);
                }
            }
        }
        private string ConvertHexa(string inputval)
        {

            int intValue = Convert.ToInt32(inputval);
            string hexValue = intValue.ToString("X");

            int existingvalue = 16777215;
            string output = string.Empty;
            if (intValue <= existingvalue)
            {
                output = Convert.ToString(intValue, 16);
            }
            else
            {
                int temp1 = intValue - existingvalue;
                int tmp2 = temp1 + 967458816;
                output = ConvertToBase36(tmp2);
            }
            return "VSK-" + output.PadLeft(6, '0').ToUpper();
        }


        public string ConvertToBase36(int value)
        {
            int Base = 36;
            string Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = "";

            while (value > 0)
            {
                result = Chars[value % Base] + result;
                value /= Base;
            }

            return result;
        }

        public void BindMapLegislationQsid()
        {
            Task.Run(() =>
            {
                listMapQsidLG = new List<Map_Qsid_Legislation_VM>(_objLibraryFunction_Service.GetMapLegislationList());
                NotifyPropertyChanged("listMapQsidLG");
            });
        }
        public void BindEchaListBusinessKey(int qsid)
        {
            visibilityEchaListBusinessKey = Visibility.Visible;
            NotifyPropertyChanged("visibilityEchaListBusinessKey");
            Task.Run(() =>
            {
                var listQsCasID = _objLibraryFunction_Service.GetEchaLegislationEntryKey_QscasID_List(qsid);
                List<EchaListEntryBusinessKey_VM> listHexa = new List<EchaListEntryBusinessKey_VM>();
                foreach (var item in listQsCasID)
                {
                    string output = ConvertHexa(item.QSCASID.ToString());
                    listHexa.Add(new EchaListEntryBusinessKey_VM()
                    {
                        QSCASID = item.QSCASID,
                        AccessFileRN = item.AccessFileRN,
                        Cas = item.Cas,
                        CasId = item.CasId,
                        EnglishNameOnList = item.EnglishNameOnList,
                        RowID = item.RowID,
                        EchaListEntryBusinessKey = output,
                        EchaListBusinessKey = item.ECHAListBusinessKey
                    });
                }
                listEchaBusinessKey = new List<EchaListEntryBusinessKey_VM>(listHexa);
                visibilityEchaListBusinessKey = Visibility.Collapsed;
                NotifyPropertyChanged("visibilityEchaListBusinessKey");
            });
        }
        public void BindEchaBusinessKey()
        {
            visibilityEchaBusinessKey = Visibility.Visible;
            NotifyPropertyChanged("visibilityEchaBusinessKey");
            Task.Run(() =>
            {
                var listQsid = _objLibraryFunction_Service.GetEchaBusinessKey_List();

                listEchaBusinessKey_VM = listQsid.Select(x => new EchaBusinessKey_VM()
                {
                    EchaLegislationBusinessKey = x.EchaLegislationBusinessKey,
                    EchaLegislationListBusinessKey = x.EchaLegislationListBusinessKey,
                    QSID = x.QSID,
                    QSID_ID = x.QSID_ID,
                    LegislationPhrase = x.LegislationPhrase,
                    LegislationShortName = x.LegislationShortName,
                    ListFieldName = x.ListFieldName,
                    ListPhrase = x.ListPhrase,
                    ListShortName = x.ListShortName
                }).ToList();
                visibilityEchaBusinessKey = Visibility.Collapsed;
                NotifyPropertyChanged("visibilityEchaBusinessKey");
            });
        }


    }

    public class IUploadCAS
    {
        public int? RN { get; set; }
        public string INV_CAS { get; set; }
        public string EDITORIAL { get; set; }
        public string CAS { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsValid { get; set; }
        public string Remarks { get; set; }

        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel Is_3E_CAS { get; set; }
        public string New_TMP_RN { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsAlternateCAS { get; set; }
    }
    public class IReorder
    {
        public int? OriginalRN { get; set; }
        public decimal? TMP_RN { get; set; }
        public int? RevisedRN { get; set; }
    }
    public class ILibLang
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
    }
    public class ITranslate
    {
        public string FieldName { get; set; }
        public int PhraseID { get; set; }
        public string Phrase { get; set; }
        public string PhraseHash { get; set; }

    }
    public class iLibPhr
    {
        public string Phrase { get; set; }
        public int LanguageID { get; set; }
        public string PhraseHash { get; set; }

    }
    public class IMapTrans
    {
        public string Phrase { get; set; }
        public int PhraseID { get; set; }
        public int? GroupID { get; set; }
        public int LangID { get; set; }
    }
    public class GenericJoinSqlTables
    {
        private readonly CommonFunctions objCommon;
        public static string CommonListConn;

        public GenericJoinSqlTables()
        {
            objCommon = new CommonFunctions();
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
        }

        public List<dynamic> InsertTempBulk(List<string> createTableAFields, DataTable lst, string sqlQuery, string tempTableName)
        {
            string str = "CREATE TABLE " + tempTableName + "( " + string.Join(",", createTableAFields) + " )";
            SqlConnection con = new SqlConnection(CommonListConn);
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            DataTable dt = new DataTable();

            using (SqlCommand createCommand = new SqlCommand(str, con))
            {
                using (SqlCommand dCommand = new SqlCommand("DROP table " + tempTableName, con))
                {
                    createCommand.ExecuteNonQuery();
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.UseInternalTransaction, null)
                    {
                        DestinationTableName = tempTableName,
                    };
                    bulkCopy.WriteToServer(lst);
                    SqlCommand adp = new SqlCommand(sqlQuery, con) { CommandTimeout = 0 };
                    using (SqlDataReader dr = adp.ExecuteReader())
                    {
                        dt = new DataTable();
                        dt.BeginLoadData();
                        dt.Load(dr);
                        dt.EndLoadData();
                    }

                    dCommand.ExecuteNonQuery();
                }

            }

            List<string> convertList = (from DataColumn x in dt.Columns
                                        select x.ColumnName).ToList();
            List<dynamic> newLst = objCommon.ConvertToDynamicList(dt, convertList);
            return newLst;
        }

    }
    public class iTableName
    {
        public string TableName { get; set; }
    }

}
