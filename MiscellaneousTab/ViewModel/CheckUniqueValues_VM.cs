﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public List<IDelimiter> listDelimiter { get; set; }
        public List<IType> listType { get; set; } = new List<IType>();

        public ICommand ShowAccessData { get; set; }
        public ICommand BtnSameAccessFile { get; set; }

        List<IAccessData> dtAccess = new List<IAccessData>();
        public Visibility uploadAccess { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<IAccessData> comonAccess_VM { get; set; }

        public CommonDataGrid_ViewModel<ICheckPassCases> ComonlistPassedCheck { get; set; }

        public CommonDataGrid_ViewModel<IUniqueCharacter> comonUniqueChr_VM { get; set; }
        private List<string> FieldNameAccess { get; set; }
        private string TableNameAccess { get; set; }

        private void ShowAccessDataExecute(object o)
        {

            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameAccess = TabName;
            var combineField = string.Empty;
            FieldNameAccess = new List<string>();
            if (TabName.ToString() == "Default")
            {
                TableNameAccess = "DATA";
                FieldNameAccess.Add("PREF");
                //combineField = "PREF";
            }
            else
            {
                FieldNameAccess.Add(listFields.Where(x => x.IsSelected).FirstOrDefault().FieldName);
                //combineField = string.Join(",", FieldNameAccess);
            }
            if (!FieldNameAccess.Contains("RN"))
            {
                FieldNameAccess.Add("RN");
            }
            combineField = string.Join(",", FieldNameAccess);

            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 " + combineField + " from [" + TableNameAccess + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(combineField + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>("RN")).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate RN in this file, first fix them");
                return;
            }

            uploadAccess = Visibility.Visible;
            NotifyPropertyChanged("uploadAccess");
            dtAccess = new List<IAccessData>();


            Task.Run(() =>
            {
                var result = chkUniqueValue(combineField);
                listAccess = new List<IAccessData>(dtAccess);
                NotifyPropertyChanged("comonAccess_VM");
                uploadAccess = Visibility.Collapsed;
                NotifyPropertyChanged("uploadAccess");
            });
            comonAccess_VM.CommandDataGridClearFilterExecute(null);
        }

        private List<IAccessData> chkUniqueValue(string combineField)
        {
            char delimiter;
            dtAccess = new List<IAccessData>();
            var chkDup = objCommon.DbaseQueryReturnTable("select " + combineField + " from [" + TableNameAccess + "]", CommonFilepath);
            if (chkDup.Rows.Count > 0)
            {
                for (int i = 0; i < chkDup.Rows.Count; i++)
                {
                    for (int j = 0; j < chkDup.Columns.Count; j++)
                    {

                        var colName = chkDup.Columns[j].ColumnName;
                        if (colName != "RN")
                        {
                            var delimiterName = newListField.Where(x => x.FieldName == colName).Select(y => y.SelectedDelimiter.Delimiter).FirstOrDefault();
                            if (!string.IsNullOrEmpty(chkDup.Rows[i][j].ToString()))
                            {
                                List<string> splitVal = new List<string>();
                                if (!string.IsNullOrEmpty(delimiterName))
                                {
                                    delimiter = Convert.ToChar(delimiterName);
                                    splitVal = chkDup.Rows[i][j].ToString().Split(delimiter).ToList();
                                }
                                else
                                {
                                    splitVal.Add(chkDup.Rows[i][j].ToString());
                                }

                                for (int k = 0; k < splitVal.Count(); k++)
                                {
                                    var val = objCommon.RemoveWhitespaceCharacter(splitVal[k].ToString().Trim(), StripCharacters);
                                    dtAccess.Add(new IAccessData
                                    {
                                        Field_Name = colName,
                                        OriginalValue = chkDup.Rows[i]["RN"].ToString() + "." + (k + 1).ToString() + "-" + splitVal[k].ToString(),
                                        Field_Value = val,
                                        Field_length = val.Length,
                                        ChangeTo = "",
                                        Alpha_Squash = objCommon.FullSquash(val),
                                        Frequency = null,
                                        DuplicateID = null,
                                        RN = chkDup.Rows[i]["RN"].ToString()
                                    });
                                }
                            }
                        }
                    }
                }

                dtAccess = (from d1 in dtAccess
                            group new { d1.OriginalValue, d1.RN } by new { d1.Field_Value, d1.Field_length, d1.Alpha_Squash, d1.Field_Name } into g
                            select new IAccessData
                            {
                                Field_Name = g.Key.Field_Name,
                                Field_Value = g.Key.Field_Value,
                                Field_length = g.Key.Field_length,
                                ChangeTo = "",
                                Alpha_Squash = g.Key.Alpha_Squash,
                                Frequency = g.Count(),
                                DuplicateID = null,
                                RN = string.Join("@", g.Select(y => y.RN).Distinct().ToList()),
                                OriginalValueList = g.Select(y => y.OriginalValue).ToList(),
                            }).ToList();
                var cnt = 1;
                dtAccess.ForEach(x =>
                {
                    var freq = dtAccess.Where(y => y.Alpha_Squash == x.Alpha_Squash).ToList();
                    if (freq.Count() > 1 && x.DuplicateID == null)
                    {
                        freq.ForEach(z =>
                        {
                            z.DuplicateID = cnt;
                        });
                        cnt += 1;
                    }
                });
                dtAccess = dtAccess.OrderByDescending(x => x.DuplicateID).ToList();
            }
            return dtAccess;
        }
        private List<IAccessData> _listAccess { get; set; } = new List<IAccessData>();
        public List<IAccessData> listAccess
        {
            get { return _listAccess; }
            set
            {
                if (_listAccess != value)
                {
                    _listAccess = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IAccessData>>>(new PropertyChangedMessage<List<IAccessData>>(null, _listAccess, "Default List"));
                }
            }
        }

        public bool _IsUpdateChangeTo { get; set; }
        public bool IsUpdateChangeTo
        {
            get => _IsUpdateChangeTo;
            set
            {
                if (Equals(_IsUpdateChangeTo, value))
                {
                    return;
                }

                _IsUpdateChangeTo = value;
                if (IsUpdateChangeTo == true)
                {
                    if (FilterRows.Count() == 0)
                    {
                        dtAccess.ForEach(x =>
                        {
                            x.ChangeTo = x.Field_Value;
                        });

                    }
                    else
                    {
                        var filterRN = FilterRows.Select(x => x.RN).Distinct().ToList();
                        dtAccess.ForEach(x =>
                        {
                            if (filterRN.Contains(x.RN))
                            {
                                x.ChangeTo = x.Field_Value;
                            }
                        });
                    }
                    listAccess = new List<IAccessData>(dtAccess);
                    NotifyPropertyChanged("comonAccess_VM");
                }
                else
                {
                    dtAccess.ForEach(x =>
                    {
                        x.ChangeTo = "";
                    });
                    listAccess = new List<IAccessData>(dtAccess);
                    NotifyPropertyChanged("comonAccess_VM");
                }

            }
        }

        public ICommand UpdateAccessData { get; set; }


        private void CommandUpdateAccessExecute(object obj)
        {
            uploadAccess = Visibility.Visible;
            NotifyPropertyChanged("uploadAccess");
            Task.Run(() =>
            {
                List<iChangeValues> chngLst = new List<iChangeValues>();

                var dt = new DataTable();
                dt.Columns.Add("RN");

                var fieldNm = dtAccess.Where(x => x.Field_Name != "RN").Select(x => x.Field_Name).FirstOrDefault();
                dt.Columns.Add(fieldNm);
                var del = newListField.Select(y => y.SelectedDelimiter.Delimiter).FirstOrDefault() == null ? string.Empty : newListField.Select(y => y.SelectedDelimiter.Delimiter).FirstOrDefault();

                List<string> onlyChangedRows = new List<string>();
                List<IUpdate> orgChangeValues = new List<IUpdate>();

                dtAccess.ForEach(x =>
                {
                    var rowIDSplit = x.RN.Split('@');
                    if (x.ChangeTo != string.Empty)
                    {
                        onlyChangedRows.AddRange(rowIDSplit.ToList());
                    }
                    for (int i = 0; i < rowIDSplit.Count(); i++)
                    {
                        orgChangeValues.AddRange(x.OriginalValueList.Where(z => z.StartsWith(rowIDSplit[i] + ".")).Select(y => new IUpdate
                        {
                            Org_Value = y,
                            ChangeValue = x.ChangeTo == string.Empty ? y.Substring(y.IndexOf("-") + 1) : x.ChangeTo,
                            DecValue = Convert.ToDecimal(y.Substring(0, y.IndexOf("-"))),
                        }).ToList());
                    }
                });

                onlyChangedRows = onlyChangedRows.Select(x => x).Distinct().ToList();
                for (int i = 0; i < onlyChangedRows.Count(); i++)
                {
                    var getAllRowsofSpecificRN = orgChangeValues.Where(z => z.Org_Value.StartsWith(onlyChangedRows[i] + ".")).OrderBy(z => z.DecValue).Select(y => y.ChangeValue).ToList();
                    var makeNewValue = string.Join(del, getAllRowsofSpecificRN);
                    if (makeNewValue.Length > 1000)
                    {
                        var abc = string.Empty;
                    }
                    dt.Rows.Add(onlyChangedRows[i], makeNewValue);

                }
                dt = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<string>(fieldNm))).OrderBy(x => x.Field<string>("RN")).CopyToDataTable();
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                var queryString = string.Empty;
                var updateQuery = string.Empty;
                listColParam.Add("@RN");
                columnList.Add("RN");
                queryString += "[" + fieldNm + "] LongText,";
                listColParam.Add("@" + fieldNm);
                columnList.Add(fieldNm);
                updateQuery += "a." + fieldNm + " = iif(len(b." + fieldNm + ") = 0, null,b." + fieldNm + "),";

                queryString = queryString.TrimEnd(',');
                updateQuery = updateQuery.TrimEnd(',');
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (RN Number, " + queryString + ")", CommonFilepath);
                _objAccessVersion_Service.ExportAccessData(dt, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());

                objCommon.DbaseQueryReturnTable("update [" + TableNameAccess + "] a inner join TMPDh b on cdbl(a.RN) = cdbl(b.RN) set " + updateQuery, CommonFilepath);


                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);


                var combineField = string.Join(",", FieldNameAccess);
                var result = chkUniqueValue(combineField);
                listAccess = new List<IAccessData>(dtAccess);
                NotifyPropertyChanged("comonAccess_VM");
                uploadAccess = Visibility.Collapsed;
                NotifyPropertyChanged("uploadAccess");

                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowSuccess("Access file updated successfully");
                });
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            });


        }

        private List<CommonDataGridColumn> GetListGridColumnAccess()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "RN", ColBindingName = "RN", ColType = "Textbox" });

            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_Name", ColBindingName = "Field_Name", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_Value", ColBindingName = "Field_Value", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChangeTo", ColBindingName = "ChangeTo", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Field_length", ColBindingName = "Field_length", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Frequency", ColBindingName = "Frequency", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Alpha_Squash", ColBindingName = "Alpha_Squash", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "DuplicateID", ColBindingName = "DuplicateID", ColType = "Textbox" });
            return listColumnQsidDetail;
        }

        public void BtnSameAccessFileExecute(Object o)
        {

            var dtNew = objCommon.ConvertToDataTable<IExportData>(listAccess.Select(x => new IExportData
            {
                FieldName = x.Field_Name,
                Fieldlength = x.Field_length.ToString(),
                ChangeTo = string.IsNullOrEmpty(x.ChangeTo) ? " " : x.ChangeTo,
                AlphaSquash = x.Alpha_Squash,
                DuplicateID = string.IsNullOrEmpty(x.DuplicateID.ToString()) ? " " : x.DuplicateID.ToString(),
                FieldValue = x.Field_Value,
                Frequency = string.IsNullOrEmpty(x.Frequency.ToString()) ? " " : x.Frequency.ToString(),
                RN = x.RN
            }).ToList());
            var NewTableName = "UniqueValues_" + string.Join("_", listAccess.Select(x => x.Field_Name).Distinct().ToList());
            CreateTabelINSameAccessFile(NewTableName, dtNew);
            _notifier.ShowSuccess(NewTableName + " Table Created successfully");
        }

        private void CreateTabelINSameAccessFile(string NewTableName, DataTable dtExport)
        {
            var queryString = string.Empty;
            var updateQuery = string.Empty;
            List<string> listColParam = new List<string>();
            List<string> columnList = new List<string>();
            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                queryString += "[" + dtExport.Columns[i].ColumnName + "] LongText,";
                listColParam.Add("@" + dtExport.Columns[i].ColumnName);
                columnList.Add(dtExport.Columns[i].ColumnName);
            }
            queryString = queryString.TrimEnd(',');
            updateQuery = updateQuery.TrimEnd(',');
            //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
            objCommon.DbaseQueryReturnString("Drop Table  " + NewTableName, CommonFilepath);
            objCommon.DbaseQueryReturnString("CREATE TABLE " + NewTableName + "(" + queryString + ")", CommonFilepath);
            _objAccessVersion_Service.ExportAccessData(dtExport, CommonFilepath, NewTableName, listColParam.ToArray(), columnList.ToArray());
            //if (isFileOpen)
            //{
            //    objCommon.OpenFile(CommonFilepath);
            //}
        }


    }

    public class IUpdate
    {
        public string Org_Value { get; set; }
        public string ChangeValue { get; set; }
        public decimal DecValue { get; set; }
    }
    public class IAccessData
    {
        public string Field_Name { get; set; }
        public string OriginalValue { get; set; }
        public string Field_Value { get; set; }
        public string ChangeTo { get; set; }
        public int Field_length { get; set; }
        public int? Frequency { get; set; }
        public string Alpha_Squash { get; set; }
        public int? DuplicateID { get; set; }
        public string RN { get; set; }
        public List<string> OriginalValueList { get; set; }
    }
    public class IExportData
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public string ChangeTo { get; set; }
        public string Fieldlength { get; set; }
        public string Frequency { get; set; }
        public string AlphaSquash { get; set; }
        public string DuplicateID { get; set; }
        public string RN { get; set; }
    }
    public class DelimiterCombo
    {
        public string FieldName { get; set; }
        public IDelimiter DelCombo { get; set; }
    }

    public class iChangeValues
    {
        public string FieldName { get; set; }
        public int Count { get; set; }
        public string RowID { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string SortOrder { get; set; }
        public int RN { get; set; }
    }
}
