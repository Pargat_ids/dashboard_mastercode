﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;
using System.Linq.Dynamic;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public Visibility uploadConsolidate { get; set; } = Visibility.Collapsed;
        public string TxtDefaultDel { get; set; } = string.Empty;
        public bool ChkLeading { get; set; }
        public bool ChkTrailing { get; set; }
        public ICommand BtnShowConsolidate { get; set; }
        public ICommand BtnUpdateAccessConsolidate { get; set; }
        public ICommand BtnPreviewConsolidate { get; set; }
        private string TableNameConsolidata { get; set; }
        public StandardDataGrid_VM listConsolidateRows { get; set; }
        public List<MapFieldListConcatenateCriteriaVM> listFieldsConcatenateCriteria { get; set; }
        public List<MapFieldListMatchingCriteriaVM> listFieldsMatchingCriteria { get; set; }
        public string ConsolidateError { get; set; }
        public void BtnShowConsolidateExecute(Object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameConsolidata = TabName;
            if (TabName.ToString() == "Default")
            {
                TableNameConsolidata = "DATA";
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select top 1 * from [" + TableNameConsolidata + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("Fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            var selMatchingCriteria = listFieldsMatchingCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            var selConcatenateCriteria = listFieldsConcatenateCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            if (selMatchingCriteria.Count() == 0 || selConcatenateCriteria.Count() == 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Empty");
                return;
            }
            // Matching Fields and ConcatenateFields can't be same
            var common = (from d1 in selMatchingCriteria
                          join d2 in selConcatenateCriteria
                          on d1 equals d2
                          select d1).ToList();
            if (common.Count() > 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Same");
                return;
            }

            uploadConsolidate = Visibility.Visible;
            NotifyPropertyChanged("uploadConsolidate");
            // Check unique Criteria
            var strMatCriteria = string.Join(",", selMatchingCriteria);

            var chkDup = objCommon.DbaseQueryReturnTable("select " + strMatCriteria + " from [" + TableNameConsolidata + "] where omit is null or omit =''", CommonFilepath);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + strMatCriteria + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            //var chkCriteria = objCommon.DbaseQueryReturnTable(
            //        "SELECT " + strMatCriteria + " , COUNT(*) AS RowsCount FROM [" + TableNameConsolidata + 
            //        "] where (omit is null or omit = '' )" +
            //        " GROUP BY " + strMatCriteria + " HAVING COUNT(*) > 1 ", CommonFilepath);
            var dtError = new DataTable();
            ConsolidateError = string.Empty;
            if (chkExistingCriteria.Any())
            {
                var dt = CreateDataTableFromDynamicQuery(strMatCriteria, chkExistingCriteria, true);
                ConsolidateError = "Total Duplicates Founds";
                listConsolidateRows = new StandardDataGrid_VM("Duplicate Rows", dt);
            }
            else
            {
                ConsolidateError = "No Duplicates Founds";
                listConsolidateRows = null;
            }
            NotifyPropertyChanged("ConsolidateError");
            NotifyPropertyChanged("listConsolidateRows");
            uploadConsolidate = Visibility.Collapsed;
            NotifyPropertyChanged("uploadConsolidate");
        }
        public void BtnUpdateAccessConsolidateExecute(Object o)
        {
            ConsolidateError = string.Empty;
            ConsolidateError = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameConsolidata = TabName;
            if (TabName.ToString() == "Default")
            {
                TableNameConsolidata = "DATA";
            }


            var selMatchingCriteria = listFieldsMatchingCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            var selConcatenateCriteria = listFieldsConcatenateCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            if (selMatchingCriteria.Count() == 0 || selConcatenateCriteria.Count() == 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Empty");
                return;
            }
            // Matching Fields and ConcatenateFields can't be same
            var common = (from d1 in selMatchingCriteria
                          join d2 in selConcatenateCriteria
                          on d1 equals d2
                          select d1).ToList();
            if (common.Count() > 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Same");
                return;
            }

            var strMatCriteria = string.Join(",", selMatchingCriteria);
            var strcon = string.Join(",", selConcatenateCriteria);
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameConsolidata + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + ", OMIT, " + strMatCriteria + "," + strcon + " from [" + TableNameConsolidata + "] where omit is null or omit = ''", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(autoCol + ", OMIT, " + strMatCriteria + ", " + strcon + " Fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkDup = objCommon.DbaseQueryReturnTable("select " + strMatCriteria + " from [" + TableNameConsolidata + "] where omit is null or omit =''", CommonFilepath);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + strMatCriteria + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            //var chkCriteria = objCommon.DbaseQueryReturnTable(
            //       "SELECT " + strMatCriteria + " , COUNT(*) AS RowsCount FROM [" + TableNameConsolidata +
            //       "] where (omit is null or omit = '' )" +
            //       " GROUP BY " + strMatCriteria + " HAVING COUNT(*) > 1 ", CommonFilepath);
            var dtError = new DataTable();
            ConsolidateError = string.Empty;
            if (!chkExistingCriteria.Any())
            {
                _notifier.ShowError("No Duplicate Found so can't run the process");
                return;
            }

            var chkIdentDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkIdentDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }

            Task.Run(() =>
            {
                uploadConsolidate = Visibility.Visible;
                NotifyPropertyChanged("uploadConsolidate");

                var strUnqCriteria = string.Join(" & '/' & ", selMatchingCriteria.Select(x => " Cstr(IIF(" + x + " Is Null,''," + x + "))").ToList());
                var strAllUnqCriteria = string.Join(",", selMatchingCriteria.Select(x => "IIF(" + x + " Is Null,''," + x + ") as " + x + "1").ToList());
                var strConCriteria = string.Join(",", selConcatenateCriteria.Select(x => "IIF(" + x + " Is Null,''," + x + ") as " + x + "1").ToList());
                var dtAllRows = objCommon.DbaseQueryReturnTable("select " + autoCol + ", OMIT, " + strUnqCriteria + " as UniqueCriteria," + strAllUnqCriteria + "," + strConCriteria + " from [" + TableNameConsolidata + "] where omit is null or omit = '' order by " + autoCol, CommonFilepath);
                var dtOnlyUnique = objCommon.DbaseQueryReturnTable("select distinct " + strUnqCriteria + " as UniqueCriteria from [" + TableNameConsolidata + "] where omit is null or omit = ''", CommonFilepath);

                for (int i = 0; i < dtAllRows.Columns.Count; i++)
                {
                    if (dtAllRows.Columns[i].ColumnName != "UniqueCriteria" && dtAllRows.Columns[i].ColumnName.Contains("1"))
                    {
                        dtAllRows.Columns[i].ColumnName = dtAllRows.Columns[i].ColumnName.Remove(dtAllRows.Columns[i].ColumnName.Length - 1);
                    }
                }
                var del = TxtDefaultDel == null || TxtDefaultDel.Trim() == string.Empty ? ";" : TxtDefaultDel.Trim();
                del = ChkLeading == true ? " " + del : del;
                del = ChkTrailing == true ? del + " " : del;
                for (int i = 0; i < dtOnlyUnique.Rows.Count; i++)
                {
                    var getMatchRows = dtAllRows.AsEnumerable().Where(x => x.Field<string>("UniqueCriteria") == dtOnlyUnique.Rows[i].Field<string>("UniqueCriteria")).OrderBy(y => y.Field<Int32>(autoCol)).ToList();
                    for (int k = 0; k < selConcatenateCriteria.Count; k++)
                    {
                        var joinVal = string.Join(del, getMatchRows.Where(y => !string.IsNullOrEmpty(y.Field<string>(selConcatenateCriteria[k]))).Select(x => x.Field<string>(selConcatenateCriteria[k])).ToList());
                        for (int j = 0; j < getMatchRows.Count(); j++)
                        {
                            if (j == 0)
                            {
                                getMatchRows[j][selConcatenateCriteria[k]] = joinVal;
                            }
                            getMatchRows[j]["Omit"] = j == 0 ? string.Empty : "*";
                        }
                    }
                }
                dtAllRows.Columns.Remove("UniqueCriteria");
                dtAllRows = dtAllRows.AsEnumerable().OrderBy(x => x.Field<Int32>(autoCol)).CopyToDataTable();
                for (int i = 0; i < selMatchingCriteria.Count; i++)
                {
                    dtAllRows.Columns.Remove(selMatchingCriteria[i]);

                }
                var queryString = string.Empty;
                var updateQuery = string.Empty;
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                for (int i = 0; i < dtAllRows.Columns.Count; i++)
                {
                    var fName = dtAllRows.Columns[i].ColumnName;
                    queryString += "[" + fName + "] LongText,";
                    listColParam.Add("@" + fName);
                    columnList.Add(fName);
                    if (fName.ToUpper() != autoCol)
                    {
                        updateQuery += "a." + fName + " = iif(len(b." + fName + ") = 0, null,b." + fName + "),";
                    }
                }
                queryString = queryString.TrimEnd(',');
                updateQuery = updateQuery.TrimEnd(',');
                //var isFileOpen = objCommon.CloseAlreadyOpenedFile(CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (" + queryString + ")", CommonFilepath);
                _objAccessVersion_Service.ExportAccessData(dtAllRows, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnTable("update [" + TableNameConsolidata + "] a inner join TMPDh b on cdbl(a." + autoCol + ") = cdbl(b." + autoCol + ") set " + updateQuery, CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                _notifier.ShowSuccess("Access File Updated Successfully");
                listConsolidateRows = null;
                NotifyPropertyChanged("ConsolidateError");
                NotifyPropertyChanged("listConsolidateRows");
                uploadConsolidate = Visibility.Collapsed;
                NotifyPropertyChanged("uploadConsolidate");
                //if (isFileOpen)
                //{
                //    objCommon.OpenFile(CommonFilepath);
                //}
            });


        }
        public void BtnPreviewConsolidateExecute(object o)
        {
            ConsolidateError = string.Empty;
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameConsolidata = TabName;
            if (TabName.ToString() == "Default")
            {
                TableNameConsolidata = "DATA";
            }


            var selMatchingCriteria = listFieldsMatchingCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            var selConcatenateCriteria = listFieldsConcatenateCriteria.Where(x => x.IsSelected == true).Select(y => y.FieldName).ToList();
            if (selMatchingCriteria.Count() == 0 || selConcatenateCriteria.Count() == 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Empty");
                return;
            }
            // Matching Fields and ConcatenateFields can't be same
            var common = (from d1 in selMatchingCriteria
                          join d2 in selConcatenateCriteria
                          on d1 equals d2
                          select d1).ToList();
            if (common.Count() > 0)
            {
                _notifier.ShowError("Matching Field & Concatenate Fields can't be Same");
                return;
            }

            var strMatCriteria = string.Join(",", selMatchingCriteria);
            var strcon = string.Join(",", selConcatenateCriteria);
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameConsolidata + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + ", OMIT, " + strMatCriteria + "," + strcon + " from [" + TableNameConsolidata + "] where omit is null or omit = ''", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError(autoCol + ", OMIT, " + strMatCriteria + ", " + strcon + " Fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }

            var chkDup = objCommon.DbaseQueryReturnTable("select " + strMatCriteria + " from [" + TableNameConsolidata + "] where omit is null or omit =''", CommonFilepath);
            var listType = typeof(System.String);
            var list = BindListFromTable(chkDup, listType);
            var chkExistingCriteria = list.AsQueryable().GroupBy("new(" + strMatCriteria + ")", "it").Where("Count() > 1").Select("new(Key as Name, Count() as Total)");
            //var chkCriteria = objCommon.DbaseQueryReturnTable(
            //       "SELECT " + strMatCriteria + " , COUNT(*) AS RowsCount FROM [" + TableNameConsolidata +
            //       "] where (omit is null or omit = '' )" +
            //       " GROUP BY " + strMatCriteria + " HAVING COUNT(*) > 1 ", CommonFilepath);
            var dtError = new DataTable();
            ConsolidateError = string.Empty;
            if (!chkExistingCriteria.Any())
            {
                _notifier.ShowError("No Duplicate Found so can't run the process");
                return;
            }

            var chkIdentDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkIdentDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }

            Task.Run(() =>
            {
                uploadConsolidate = Visibility.Visible;
                NotifyPropertyChanged("uploadConsolidate");

                var strUnqCriteria = string.Join(" & '/' & ", selMatchingCriteria.Select(x => " Cstr(IIF(" + x + " Is Null,''," + x + "))").ToList());
                var strAllUnqCriteria = string.Join(",", selMatchingCriteria.Select(x => "IIF(" + x + " Is Null,''," + x + ") as " + x + "1").ToList());
                var strConCriteria = string.Join(",", selConcatenateCriteria.Select(x => "IIF(" + x + " Is Null,''," + x + ") as " + x + "1").ToList());
                var dtAllRows = objCommon.DbaseQueryReturnTable("select " + autoCol + ", OMIT, " + strUnqCriteria + " as UniqueCriteria," + strAllUnqCriteria + "," + strConCriteria + " from [" + TableNameConsolidata + "] where omit is null or omit = '' order by " + autoCol, CommonFilepath);
                var dtOnlyUnique = objCommon.DbaseQueryReturnTable("select distinct " + strUnqCriteria + " as UniqueCriteria from [" + TableNameConsolidata + "] where omit is null or omit = ''", CommonFilepath);

                for (int i = 0; i < dtAllRows.Columns.Count; i++)
                {
                    if (dtAllRows.Columns[i].ColumnName != "UniqueCriteria" && dtAllRows.Columns[i].ColumnName.Contains("1"))
                    {
                        dtAllRows.Columns[i].ColumnName = dtAllRows.Columns[i].ColumnName.Remove(dtAllRows.Columns[i].ColumnName.Length - 1);
                    }
                }
                var del = TxtDefaultDel == null || TxtDefaultDel.Trim() == string.Empty ? ";" : TxtDefaultDel.Trim();
                del = ChkLeading == true ? " " + del : del;
                del = ChkTrailing == true ? del + " " : del;
                for (int i = 0; i < dtOnlyUnique.Rows.Count; i++)
                {
                    var getMatchRows = dtAllRows.AsEnumerable().Where(x => x.Field<string>("UniqueCriteria") == dtOnlyUnique.Rows[i].Field<string>("UniqueCriteria")).OrderBy(y => y.Field<Int32>(autoCol)).ToList();
                    for (int k = 0; k < selConcatenateCriteria.Count; k++)
                    {
                        var joinVal = string.Join(del, getMatchRows.Where(y => !string.IsNullOrEmpty(y.Field<string>(selConcatenateCriteria[k]))).Select(x => x.Field<string>(selConcatenateCriteria[k])).ToList());
                        for (int j = 0; j < getMatchRows.Count(); j++)
                        {
                            if (j == 0)
                            {
                                getMatchRows[j][selConcatenateCriteria[k]] = joinVal;
                            }
                            getMatchRows[j]["Omit"] = j == 0 ? string.Empty : "*";
                        }
                    }
                }
                dtAllRows.Columns.Remove("UniqueCriteria");
                dtAllRows = dtAllRows.AsEnumerable().OrderBy(x => x.Field<Int32>(autoCol)).CopyToDataTable();
                listConsolidateRows = new StandardDataGrid_VM("Consolidate Rows", dtAllRows);
                NotifyPropertyChanged("ConsolidateError");
                NotifyPropertyChanged("listConsolidateRows");
                uploadConsolidate = Visibility.Collapsed;
                NotifyPropertyChanged("uploadConsolidate");
            });


        }
        public string RetrieveAutoNumberColumn(string TableName)
        {
            string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source = " + CommonFilepath;
            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + TableName, conStr);
            DataTable dtSchema = adapter.FillSchema(new DataTable(), SchemaType.Source);
            if (dtSchema == null)
            {
            }
            string columnName = null;
            for (int i = 0; i < dtSchema.Columns.Count; i++)
            {
                if (dtSchema.Columns[i].AutoIncrement)
                {
                    columnName = dtSchema.Columns[i].ColumnName;
                    break;
                }
            }
            return columnName;
        }
    }
}
