﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public partial class CasValidity_VM : Base_ViewModel
    {
        public ICommand CheckStripCharacter { get; set; }
        public ICommand CheckStripCharacterAnalyze { get; set; }
        private string TableNameStripCharacter { get; set; }
        private string FieldNameStripCharacter { get; set; }
        public CommonDataGrid_ViewModel<IDateFormat> comonStripCharacter_VM { get; set; }
        private void CheckStripCharacterAnalyzeExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameStripCharacter = TabName;
            FieldNameStripCharacter = string.Empty;
            if (TabName.ToString() == "Default")
            {
                TableNameStripCharacter = "DATA";
                FieldNameStripCharacter = "CAS";
            }
            else
            {
                FieldNameStripCharacter = string.Join(",", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList());
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameStripCharacter + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + ", " + FieldNameStripCharacter + " from [" + TableNameStripCharacter + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("Ident and " + FieldNameStripCharacter + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }
            uploadStripCharacter = Visibility.Visible;
            NotifyPropertyChanged("uploadStripCharacter");
            Task.Run(() =>
            {
                List<IStripChar> StripCharacters = new List<IStripChar>();
                using (var context = new CRAModel())
                {
                    StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                }

                var dt = new DataTable();
                dt.Columns.Add(autoCol, typeof(int));
                dt.Columns.Add("FieldName", typeof(string));
                dt.Columns.Add("OldValue", typeof(string));
                dt.Columns.Add("NewValue", typeof(string));
                dt.Columns.Add("RemovedCharacters", typeof(string));

                for (int i = 0; i < dtRaw.Rows.Count; i++)
                {
                    for (int j = 0; j < dtRaw.Columns.Count; j++)
                    {
                        var orFld = dtRaw.Rows[i][dtRaw.Columns[j].ColumnName].ToString();
                        var NewFld = objCommon.RemoveWhitespaceCharacterNew(dtRaw.Rows[i][dtRaw.Columns[j].ColumnName].ToString(), StripCharacters);
                        var NewFldItem = NewFld.Item1.Trim().Replace("  ", " ").Trim();
                        if (orFld != NewFldItem)
                        {
                            dt.Rows.Add(dtRaw.Rows[i][autoCol], dtRaw.Columns[j].ColumnName, orFld, NewFldItem, NewFld.Item2 == "" ? "Leading / Trailing / Double Spaces" : NewFld.Item2);
                        }
                    }
                }

                if (dt.Rows.Count > 0)
                {

                    listStripCharacter = new StandardDataGrid_VM("Strip Character", dt);
                    NotifyPropertyChanged("listStripCharacter");
                }
                else
                {
                    listStripCharacter = null;
                    NotifyPropertyChanged("listStripCharacter");
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowSuccess("No Discrepancy Found");
                    });
                }
                uploadStripCharacter = Visibility.Collapsed;
                NotifyPropertyChanged("uploadStripCharacter");
            });
        }
        public string DefaultLableStripCharacter { get; set; }
        public Visibility uploadStripCharacter { get; set; } = Visibility.Collapsed;

        private void CheckStripCharacterExecute(object o)
        {
            if (CommonFilepath == string.Empty)
            {
                _notifier.ShowError("First choose access file to continue");
                return;
            }
            if (TabName == null)
            {
                _notifier.ShowError("first choose the TableName to continue");
                return;
            }
            TableNameStripCharacter = TabName;
            FieldNameStripCharacter = string.Empty;
            if (TabName.ToString() == "Default")
            {
                TableNameStripCharacter = "DATA";
                FieldNameStripCharacter = "CAS";
            }
            else
            {
                FieldNameStripCharacter = string.Join(",", listFields.Where(x => x.IsSelected).Select(x => x.FieldName).ToList());
            }
            var autoCol = RetrieveAutoNumberColumn("[" + TableNameStripCharacter + "]");
            if (autoCol == string.Empty || autoCol == null)
            {
                _notifier.ShowError("No AutoNumber Column Found in this file!");
                return;
            }
            var dtRaw = objCommon.DbaseQueryReturnTable("select " + autoCol + ", " + FieldNameStripCharacter + " from [" + TableNameStripCharacter + "]", CommonFilepath);
            if (dtRaw.Columns.Count == 0)
            {
                _notifier.ShowError("Ident and " + FieldNameStripCharacter + " - fields not found in selected file!");
                return;
            }
            if (dtRaw == null || dtRaw.Rows.Count == 0)
            {
                _notifier.ShowError("No row found in selected file!");
                return;
            }
            var chkRNDuplicate = dtRaw.AsEnumerable().GroupBy(x => x.Field<dynamic>(autoCol)).Where(y => y.Count() > 1).ToList();
            if (chkRNDuplicate.Any())
            {
                _notifier.ShowError("Found duplicate " + autoCol + " in this file, first fix them");
                return;
            }
            uploadStripCharacter = Visibility.Visible;
            NotifyPropertyChanged("uploadStripCharacter");
            Task.Run(() =>
            {
                List<IStripChar> StripCharacters = new List<IStripChar>();
                using (var context = new CRAModel())
                {
                    StripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
                }
                var queryString = string.Empty;
                var updateQuery = string.Empty;
                List<string> listColParam = new List<string>();
                List<string> columnList = new List<string>();
                listColParam.Add("@" + autoCol);
                columnList.Add(autoCol);

                var lstToShow = dtRaw.Clone();

                for (int i = 0; i < dtRaw.Columns.Count; i++)
                {
                    if (dtRaw.Columns[i].ColumnName.ToUpper().Trim() != autoCol.ToUpper().Trim())
                    {
                        queryString += "[" + dtRaw.Columns[i].ColumnName + "] LongText,";
                        listColParam.Add("@" + dtRaw.Columns[i].ColumnName);
                        columnList.Add(dtRaw.Columns[i].ColumnName);
                        updateQuery += "a." + dtRaw.Columns[i].ColumnName + " = iif(len(b." + dtRaw.Columns[i].ColumnName + ") = 0, null,b." + dtRaw.Columns[i].ColumnName + "),";
                    }
                }


                queryString = queryString.TrimEnd(',');
                updateQuery = updateQuery.TrimEnd(',');
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);
                objCommon.DbaseQueryReturnString("CREATE TABLE TMPDh (" + autoCol + " Number, " + queryString + ")", CommonFilepath);

                var newdtRaw = dtRaw.Clone();
                for (int i = 0; i < dtRaw.Rows.Count; i++)
                {
                    var newRow = dtRaw.NewRow();
                    var NeedtoUpdate = false;
                    for (int j = 0; j < dtRaw.Columns.Count; j++)
                    {
                        var orFld = dtRaw.Rows[i][dtRaw.Columns[j].ColumnName].ToString();
                        var NewFld = objCommon.RemoveWhitespaceCharacterNew(dtRaw.Rows[i][dtRaw.Columns[j].ColumnName].ToString(), StripCharacters);
                        var NewFldItem = NewFld.Item1.Trim().Replace("  ", " ").Trim();
                        newRow[j] = orFld;
                        if (orFld != NewFldItem)
                        {
                            newRow[j] = NewFldItem;
                            NeedtoUpdate = true;
                        }
                    }

                    if (NeedtoUpdate)
                    {
                        newdtRaw.Rows.Add(newRow.ItemArray);
                    }
                }

                _objAccessVersion_Service.ExportAccessData(newdtRaw, CommonFilepath, "[TMPDh]", listColParam.ToArray(), columnList.ToArray());
                objCommon.DbaseQueryReturnString("update " + TableNameStripCharacter + " a inner join TMPDh b on a." + autoCol + " = b." + autoCol + " set " + updateQuery, CommonFilepath);
                objCommon.DbaseQueryReturnString("Drop Table TMPDh ", CommonFilepath);

                for (int i = 0; i < dtRaw.Columns.Count; i++)
                {
                    if (dtRaw.Columns[i].ColumnName.ToUpper().Trim() != autoCol.ToUpper().Trim())
                    {
                        objCommon.DbaseQueryReturnString("update " + TableNameStripCharacter + " set " + dtRaw.Columns[i].ColumnName + " =  null where " + dtRaw.Columns[i].ColumnName + " = ''", CommonFilepath);
                    }
                }

                listStripCharacter = null;
                NotifyPropertyChanged("listStripCharacter");
                uploadStripCharacter = Visibility.Collapsed;
                NotifyPropertyChanged("uploadStripCharacter");
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    _notifier.ShowSuccess("Update in Access File");
                });
            });
        }
        public StandardDataGrid_VM listStripCharacter { get; set; }
    }
    public class IStripCharacter
    {
        public string Ident { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel IsChanged { get; set; }
    }
}

