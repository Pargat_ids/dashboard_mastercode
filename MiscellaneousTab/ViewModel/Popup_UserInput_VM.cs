﻿using MiscellaneousTab.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using MiscellaneousTab.CustomUserControl;
using CRA_DataAccess.ViewModel;

namespace MiscellaneousTab.ViewModel
{
    public class Popup_UserInput_VM : Base_ViewModel
    {
        public CommonFunctions objCommon;
        public CommonDataGrid_ViewModel<iUserInput> comonuserinput_VM { get; set; }
        public ICommand CommandSaveInput { get; set; }
        public Popup_UserInput popWindow { get; set; }
        public Popup_UserInput_VM(Popup_UserInput _popWindow, List<iUserInput> lst)
        {
            objCommon = new CommonFunctions();
            CommandSaveInput = new RelayCommand(CommandSaveInputExecute, CommandSaveInputCanExecute);
            comonuserinput_VM = new CommonDataGrid_ViewModel<iUserInput>(GetPopupField(), "QSID_ID", "Conflict in native language phrase/notes, please review :");
            ListUserInput = new List<iUserInput>(lst);
            popWindow = _popWindow;
        }
        private bool CommandSaveInputCanExecute(object o)
        {
            return true;
        }
        private void CommandSaveInputExecute(object o)
        {
            List<int> lstDel = new List<int>();
            List<Map_TranslatedPhrases> lstAdd = new List<Map_TranslatedPhrases>();
            List<Log_Map_TranslatedPhrases> lstLogAdd = new List<Log_Map_TranslatedPhrases>();
            var chkDuplicateVal = ListUserInput.GroupBy(x => new { x.GroupID, x.LanguageID, x.Approve }).Where(y => y.Count() > 1).ToList();
            if (chkDuplicateVal.Any())
            {
                _notifier.ShowError("One value require Approve or Disapprove " + Environment.NewLine +
                    string.Join(",", chkDuplicateVal.Select(y => y.Key.GroupID).ToList()));
                return;
            }
            var maxtranId = objCommon.GetMaxAutoField("Map_TranslatedPhrases");
            for (int i = 0; i < ListUserInput.Count; i++)
            {
                if (ListUserInput[i].Approve == false && ListUserInput[i].Old_New == "Old Value")
                {
                    lstDel.Add(ListUserInput[i].TransID);
                    lstLogAdd.Add(new Log_Map_TranslatedPhrases { TranslatedPhraseGroupID = ListUserInput[i].GroupID, PhraseID_CRA = ListUserInput[i].PhraseID, Status = "D", TimeStamp = objCommon.ESTTime(), SessionID = Engine.CurrentUserSessionID, TranslatedPhraseID = ListUserInput[i].TransID });
                }
                if (ListUserInput[i].Approve == true && ListUserInput[i].Old_New == "New Value")
                {
                    lstLogAdd.Add(new Log_Map_TranslatedPhrases { TranslatedPhraseGroupID = ListUserInput[i].GroupID, PhraseID_CRA = ListUserInput[i].PhraseID, Status = "A", TimeStamp = objCommon.ESTTime(), SessionID = Engine.CurrentUserSessionID, TranslatedPhraseID = maxtranId });
                    lstAdd.Add(new Map_TranslatedPhrases { TranslatedPhraseGroupID = ListUserInput[i].GroupID, PhraseID_CRA = ListUserInput[i].PhraseID });
                    maxtranId += 1;
                }
            }
            if (lstDel.Any())
            {
                _objLibraryFunction_Service.BulkDel(lstDel, "Map_TranslatedPhrases");
            }
            _objLibraryFunction_Service.BulkIns<Log_Map_TranslatedPhrases>(lstLogAdd);
            _objLibraryFunction_Service.BulkIns<Map_TranslatedPhrases>(lstAdd);
            _notifier.ShowSuccess("Update Sucessfully");
            ListUserInput = new List<iUserInput>(null);
            NotifyPropertyChanged("ListUserInput");
            popWindow.Close();

        }
        private List<CommonDataGridColumn> GetPopupField()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GroupID", ColBindingName = "GroupID", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EnglishPhrase", ColBindingName = "EnglishPhrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Old_New", ColBindingName = "Old_New", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QsidCount", ColBindingName = "QsidCount", ColType = "Textbox" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approve", ColBindingName = "Approve", ColType = "CheckboxEnable" });
            return listColumn;
        }
        public List<iUserInput> _ListUserInput { get; set; }
        public List<iUserInput> ListUserInput
        {
            get { return _ListUserInput; }
            set
            {
                if (_ListUserInput != value)
                {
                    _ListUserInput = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<iUserInput>>>(new PropertyChangedMessage<List<iUserInput>>(_ListUserInput, _ListUserInput.ToList(), "Default List"));

                }
            }
        }
    }
}
