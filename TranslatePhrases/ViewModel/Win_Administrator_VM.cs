﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using TranslatePhrases.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;

namespace TranslatePhrases.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private CommonFunctions objCommon;
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }

        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {

                case "Units":
                    RunTPhrases();
                    break;

            }
        }

        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommandTabChanged { get; set; }

        public static string CommonListConn;
        public ICommand SearchCommand { get; set; }
        private void SearchCommandExecute(object obj)
        {
            SelectedTabItemChanged();
        }
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public bool IsSelectedTabData { get; set; }
        public List<IStripChar> stripCharacter = new List<IStripChar>();
        public Win_Administrator_VM()
        {
            objCommon = new CommonFunctions();
            comonTPhrases_VM = new CommonDataGrid_ViewModel<ITranslatePhrase>(GetListGridColumnTPhrases(), "Translated Phrases", "");

            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);

            //listFlavor.Add(new LibYesNo { Value = "No" });
            //listFlavor.Add(new LibYesNo { Value = "Yes" });
            //listFema.Add(new LibYesNo { Value = "No" });
            //listFema.Add(new LibYesNo { Value = "Yes" });
            using (var context = new CRAModel())
            {
                stripCharacter = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();

            }

            IsSelectedTabData = true;

            CommandShowPopUpAddTPhrases = new RelayCommand(CommandShowPopUpAddTPhrasesExecute);
            CommandClosePopUpTPhrases = new RelayCommand(CommandClosePopUpExecuteTPhrases);
            CommandAddTPhrases = new RelayCommand(CommandAddTPhrasesExecute, CommandAddTPhrasesCanExecute);
            CommandAddMoreTPhrases = new RelayCommand(CommandAddMoreTPhrasesExecute, CommandAddTPhrasesCanExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
            MessengerInstance.Register<PropertyChangedMessage<ITranslatePhrase>>(this, NotifyMeTPhrases);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ITranslatePhrase), CommandButtonExecutedTPhrases);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGridTPhrases);
            MessengerInstance.Register<PropertyChangedMessage<DataGridCellInfo>>(this, UpdateSourceTriggerCurrentCell);
            RunTPhrases();
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
    }
}
