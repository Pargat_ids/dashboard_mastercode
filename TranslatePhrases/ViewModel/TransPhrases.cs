﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using TranslatePhrases.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using TranslatePhrases.CustomUserControl;

namespace TranslatePhrases.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<ITranslatePhrase> comonTPhrases_VM { get; set; }
        public Visibility VisibilityTPhrasesDashboard_Loader { get; set; }
        public ICommand CommandClosePopUpTPhrases { get; private set; }
        public ICommand CommandShowPopUpAddTPhrases { get; private set; }
        public bool IsEditTPhrases { get; set; } = true;
        public Visibility PopUpContentAddTPhrasesVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<ITranslatePhrase> _lstTPhrases { get; set; } = new ObservableCollection<ITranslatePhrase>();
        public ObservableCollection<ITranslatePhrase> lstTPhrases
        {
            get { return _lstTPhrases; }
            set
            {
                if (_lstTPhrases != value)
                {
                    _lstTPhrases = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<ITranslatePhrase>>>(new PropertyChangedMessage<List<ITranslatePhrase>>(null, _lstTPhrases.ToList(), "Default List"));
                }
            }
        }
        private void BindTPhrases()
        {
            VisibilityTPhrasesDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityTPhrasesDashboard_Loader");
            Task.Run(() =>
            {
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<ITranslatePhrase>(
                " select b.PhraseID, b.Phrase, TranslatedPhraseGroupID as GroupID, LanguageName from " +
                " Map_TranslatedPhrases a, Library_Phrases b, " +
                " Library_Languages c " +
                "  where a.PhraseID_CRA = b.PhraseID and b.LanguageID = c.LanguageID " +
                " order by TranslatedPhraseID desc", Win_Administrator_VM.CommonListConn);
                lstTPhrases = new ObservableCollection<ITranslatePhrase>(SAPTPhrasesTable);
            });
            NotifyPropertyChanged("comonTPhrases_VM");
            VisibilityTPhrasesDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityTPhrasesDashboard_Loader");
        }
        private bool _IsShowPopUpTPhrases { get; set; }
        public bool IsShowPopUpTPhrases
        {
            get { return _IsShowPopUpTPhrases; }
            set
            {
                if (_IsShowPopUpTPhrases != value)
                {
                    _IsShowPopUpTPhrases = value;
                    NotifyPropertyChanged("IsShowPopUpTPhrases");
                }
            }
        }
        private void RunTPhrases()
        {
            BindTPhrases();

        }

        private void RefreshGridTPhrases(PropertyChangedMessage<string> flag)
        {
            BindTPhrases();
            if (IsShowPopUpTPhrases)
            {
                NotifyPropertyChanged("IsShowPopUpTPhrases");
            }
        }
        public ICommand CommandAddTPhrases { get; private set; }
        private void CommandAddTPhrasesExecute(object obj)
        {
            SaveTPhrases();
            IsShowPopUpTPhrases = false;
            IsEditTPhrases = true;
            NotifyPropertyChanged("IsEditTPhrases");
            ClearPopUpVariableTPhrases();
            PopUpContentAddTPhrasesVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddTPhrasesVisibility");
        }
        private bool CommandAddTPhrasesCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(newUnit.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
      
        public void SaveTPhrases()
        {
            Map_TranslatedPhrases obj = new Map_TranslatedPhrases();
            using (var context = new CRAModel())
            {
                var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('Map_TranslatedPhrases')", Win_Administrator_VM.CommonListConn));
                obj.TranslatedPhraseID = mx + 1;
                var val = objCommon.RemoveWhitespaceCharacter(newUnit, stripCharacter);
                var phrHash = objCommon.GenerateSHA256String(val.Trim());
                int phrId = 0;
                var chkinLIbPHr = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).FirstOrDefault();
                if (chkinLIbPHr == null)
                {
                    List<Library_Phrases> lstMain = new List<Library_Phrases>();
                    var lib = new Library_Phrases
                    {
                        InternalOnly = false,
                        Phrase = val.Trim(),
                        PhraseHash = phrHash,
                        IsActive = true,
                        LanguageID = 1,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstMain.Add(lib);
                    _objLibraryFunction_Service.BulkIns<Library_Phrases>(lstMain);
                    phrId = context.Library_Phrases.AsNoTracking().Where(x => x.PhraseHash == phrHash && x.LanguageID == 1).Select(y => y.PhraseID).FirstOrDefault();
                }
                else
                {
                    phrId = chkinLIbPHr.PhraseID;

                }
                obj.PhraseID_CRA = phrId;


                var chkAlreadyExist = context.Map_TranslatedPhrases.Count(x => x.PhraseID_CRA == obj.PhraseID_CRA);
                if (chkAlreadyExist > 0)
                {
                    _notifier.ShowError("PhraseID already exists");
                }
                else
                {
                    var maxGroupId = objCommon.DbaseQueryReturnStringSQL("select max(TranslatedPhraseGroupID) from Map_TranslatedPhrases", Win_Administrator_VM.CommonListConn);
                    obj.TranslatedPhraseGroupID = Convert.ToInt32(maxGroupId) + 1;
                    context.Map_TranslatedPhrases.Add(obj);
                    context.SaveChanges();
                    _notifier.ShowSuccess("New PhraseID Saved Successfully.");
                }
            }

            ClearPopUpVariableTPhrases();
            BindTPhrases();

        }
        public ICommand CommandAddMoreTPhrases { get; private set; }
        private void CommandAddMoreTPhrasesExecute(object obj)
        {
            SaveTPhrases();
        }
        private void CommandClosePopUpExecuteTPhrases(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpTPhrases = false;

        }
        private void CommandShowPopUpAddTPhrasesExecute(object obj)
        {
            ClearPopUpVariableTPhrases();
            IsEditTPhrases = true;
            NotifyPropertyChanged("IsEditTPhrases");
            IsShowPopUpTPhrases = true;
            PopUpContentAddTPhrasesVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddTPhrasesVisibility");
        }

        private ITranslatePhrase _SelectedTPhrases { get; set; }
        public ITranslatePhrase SelectedTPhrases
        {
            get => (ITranslatePhrase)_SelectedTPhrases;
            set
            {
                if (Equals(_SelectedTPhrases, value))
                {
                    return;
                }
                _SelectedTPhrases = value;
                
            }
        }

       
        private void NotifyMeTPhrases(PropertyChangedMessage<ITranslatePhrase> obj)
        {
            SelectedTPhrases = obj.NewValue;
        }

        public void ClearPopUpVariableTPhrases()
        {
            newUnit = "";
            NotifyPropertyChanged("newUnit");
        }
        private void CommandButtonExecutedTPhrases(NotificationMessageAction<object> obj)
        {

        }
        public string newUnit { get; set; } = string.Empty;

        private List<CommonDataGridColumn> GetListGridColumnTPhrases()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseID", ColBindingName = "PhraseID", ColType = "Textbox", ColumnWidth="150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth="1000" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GroupID", ColBindingName = "GroupID", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox", ColumnWidth = "200" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapTPhrases" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNameTPhrases" });

            return listColumnQsidDetail;
        }
        private DataGridCellInfo _cellInfo_AddNew { get; set; }
        public DataGridCellInfo CellInfo_AddNew
        {
            get { return _cellInfo_AddNew; }
            set
            {
                _cellInfo_AddNew = value;
            }
        }
        private void UpdateSourceTriggerCurrentCell(PropertyChangedMessage<DataGridCellInfo> obj)
        {
            if (obj.Sender.ToString() == "ITranslatePhrase")
            {
                CellInfo_AddNew = obj.NewValue;
            }
        }

    }
    public class ITranslatePhrase
    {
        public int PhraseID { get; set; }
        public string Phrase { get; set; }
        public int GroupID { get; set; }
        public string LanguageName { get; set; }
    }
}
