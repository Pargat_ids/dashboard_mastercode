﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess;

namespace TranslatePhrases.ViewModel
{
    public class ExportFile
    {
        private RowIdentifier rowIdentifier = new RowIdentifier();
        private string SelectedPath;
        public ExportFile(string selectedPath)
        {
            SelectedPath = selectedPath;
            Task.Run(() => ExportAccess());
        }
        private void ExportAccess()
        {

        }
    }
    public class ColumnSelectionDataGrid : INotifyPropertyChanged
    {
        private bool _IsNull { get; set; }
        public bool IsNull
        {
            get { return _IsNull; }
            set
            {
                _IsNull = value;
                if (_IsNull)
                {
                    IsNotNull = false;
                }
                NotifyPropertyChanged("IsNull");
            }
        }

        private bool _IsNotNull { get; set; }
        public bool IsNotNull
        {
            get { return _IsNotNull; }
            set
            {
                _IsNotNull = value;
                if (IsNotNull)
                {
                    IsNull = false;
                }
                NotifyPropertyChanged("IsNotNull");
            }
        }

        public string ColumnName { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
