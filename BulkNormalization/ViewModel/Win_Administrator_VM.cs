﻿using BulkNormalization.Library;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base;
using TableDependency.SqlClient.Base.EventArgs;
using ToastNotifications.Messages;
using CRA_DataAccess;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace BulkNormalization.ViewModel
{
    public class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }

        private CommonFunctions objCommonFunc;
        private string stringSqlCon;
        public CommonDataGrid_ViewModel<IBulkNormalization> comonBulkNormalization_VM { get; set; }
        public CommonDataGrid_ViewModel<IBulkNormalizationStatus> comonBulkNormalizationStatus_VM { get; set; }
        public Visibility LoadBulkNormalizationChanges { get; set; } = Visibility.Collapsed;
        public Visibility LoadBulkNormalizationStatusChanges { get; set; } = Visibility.Collapsed;
        public ICommand CommandRunBulk { get; set; }
       

        public Win_Administrator_VM()
        {

            objCommonFunc = new CommonFunctions();
            stringSqlCon = ConfigurationManager.ConnectionStrings["CRAModel"].ConnectionString;
            comonBulkNormalization_VM = new CommonDataGrid_ViewModel<IBulkNormalization>(GetBulkNormalizationChanges(), "QSID_ID", "Bulk Normalization :");
            comonBulkNormalizationStatus_VM = new CommonDataGrid_ViewModel<IBulkNormalizationStatus>(GetBulkStatusChanges(), "QSID_ID", "Bulk Normalization Status :");
            BindBulkNormalizationChanges();
            CommandRunBulk = new RelayCommand(CommandRunBulkExecute, CommandAddNewListCanExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }
        private bool CommandAddNewListCanExecute(object o)
        {
            return true;
        }
        private static SqlTableDependency<Log_Errors_Warnings> _tableDependency;
        private void CommandRunBulkExecute(object o)
        {
            listSelected = new List<IBulkNormalizationStatus>();
            listSelectedNew = new List<IBulkNormalizationStatus>();
            objCommonFunc.DbaseQueryReturnStringSQL("Delete from Log_Bulk_Normalization where timestamp is null", stringSqlCon);
            objCommonFunc.DbaseQueryReturnStringSQL("update Log_Sessions set SessionEnd_TimeStamp = SessionBegin_TimeStamp where SessionEnd_TimeStamp is null and username ='" + Environment.UserName + "'", stringSqlCon);
            using (var context = new CRAModel())
            {

                List<Log_Bulk_Normalization> lst = new List<Log_Bulk_Normalization>();
                for (int i = 0; i < ListBulkNormalizationChanges.Count(); i++)
                {
                    if (ListBulkNormalizationChanges[i].IsSelected == true)
                    {
                        var filePath = ListBulkNormalizationChanges[i].FilePath;
                        var chkExist = context.Log_Bulk_Normalization.AsNoTracking().FirstOrDefault(y => y.FilePath == filePath && y.TimeStamp == null);
                        if (chkExist == null)
                        {
                            var result = new Log_Bulk_Normalization()
                            {
                                FilePath = filePath,
                                TimeStamp = null,
                            };
                            lst.Add(result);
                            listSelected.Add(new IBulkNormalizationStatus { QSID = ListBulkNormalizationChanges[i].QSID, QSID_ID = ListBulkNormalizationChanges[i].QSID_ID });
                        }
                    }

                }
                _objLibraryFunction_Service.insertintoDataInput_Version(lst);
            }
            BindBulkNormalizationStatusChanges();

            _tableDependency = new SqlTableDependency<Log_Errors_Warnings>(stringSqlCon, "Log_Errors_Warnings");
            _tableDependency.OnChanged += SqlTableDependency_Changed;
            _tableDependency.Start();

            if (File.Exists(@"C:\Ariel\Common\COM Add-ins\upstreamDatanormalization\upstreamDatanormalization.exe"))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.Arguments = "Bulk";
                //startInfo.FileName = @"C:\Debug\upstreamDatanormalization.exe";
                startInfo.FileName = @"C:\Ariel\Common\COM Add-ins\upstreamDatanormalization\upstreamDatanormalization.exe";
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + @"C:\Ariel\Common\COM Add-ins\upstreamDatanormalization\upstreamDatanormalization.exe");
            }
        }
        private void SqlTableDependency_Changed(object sender, RecordChangedEventArgs<Log_Errors_Warnings> e)
        {
            for (int i = 0; i < listSelected.Count(); i++)
            {
                var qsidid = listSelected[i].QSID_ID;
                var qsid = listSelected[i].QSID;
                var chkExst = objCommonFunc.DbaseQueryReturnTableSql("select top 1 SessionID  from Log_Sessions where QSID_ID =" + qsidid + " and IsBulkProcess =  1 and sessionEnd_Timestamp is null and sessionBegin_Timestamp >='" + System.DateTime.Now.AddDays(-1) + "' order by SessionID desc", stringSqlCon);
                if (chkExst.Rows.Count != 0)
                {
                    //listSelected = listSelected.Where(x => x.QSID_ID != qsidid && string.IsNullOrEmpty(x.CheckDescription)).ToList();
                    var dt = objCommonFunc.DbaseQueryReturnTableSql("select CheckType, CheckDescription, 'Fail' as Status from Log_Errors_Warnings a,Library_Errors_Warnings b where a.LibraryErrorWarningID = b.LibraryErrorWarningID and a.SessionID =  " + Convert.ToInt32(chkExst.Rows[0][0].ToString()) + " and a.CheckPassed = 0", stringSqlCon);

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        var chkDesc = dt.Rows[j]["CheckDescription"].ToString();
                        var chkTyp = dt.Rows[j]["CheckType"].ToString(); ;
                        var chkAlreadyExist = listSelectedNew.Count(x => x.CheckDescription == chkDesc && x.QSID_ID == qsidid);
                        if (chkAlreadyExist == 0)
                        {
                            listSelectedNew.Add(new IBulkNormalizationStatus { QSID_ID = qsidid, QSID = qsid, CheckDescription = chkDesc, CheckType = chkTyp, Status = "Fail" });
                        }
                    }
                }

            }
            ListBulkNormalizationStatusChanges = new ObservableCollection<IBulkNormalizationStatus>(listSelectedNew);
        }
        private List<CommonDataGridColumn> GetBulkNormalizationChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Selected", ColBindingName = "IsSelected", ColType = "CheckboxEnable", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColType = "Textbox", ColumnWidth = "200" });
            return listColumn;
        }
        private List<CommonDataGridColumn> GetBulkStatusChanges()
        {
            List<CommonDataGridColumn> listColumn = new List<CommonDataGridColumn>();
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID_ID", ColBindingName = "QSID_ID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CheckType", ColBindingName = "CheckType", ColType = "Textbox", ColumnWidth = "200" });
            listColumn.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CheckDescription", ColBindingName = "CheckDescription", ColType = "Textbox", ColumnWidth = "500" });
            return listColumn;
        }
        public ObservableCollection<IBulkNormalization> _ListBulkNormalizationChanges { get; set; }
        public ObservableCollection<IBulkNormalization> ListBulkNormalizationChanges
        {
            get { return _ListBulkNormalizationChanges; }
            set
            {
                if (_ListBulkNormalizationChanges != value)
                {
                    _ListBulkNormalizationChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IBulkNormalization>>>(new PropertyChangedMessage<List<IBulkNormalization>>(_ListBulkNormalizationChanges.ToList(), _ListBulkNormalizationChanges.ToList(), "Default List"));

                }
            }
        }

        public ObservableCollection<IBulkNormalizationStatus> _ListBulkNormalizationStatusChanges { get; set; }
        public ObservableCollection<IBulkNormalizationStatus> ListBulkNormalizationStatusChanges
        {
            get { return _ListBulkNormalizationStatusChanges; }
            set
            {
                if (_ListBulkNormalizationStatusChanges != value)
                {
                    _ListBulkNormalizationStatusChanges = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IBulkNormalizationStatus>>>(new PropertyChangedMessage<List<IBulkNormalizationStatus>>(_ListBulkNormalizationStatusChanges.ToList(), _ListBulkNormalizationStatusChanges.ToList(), "Default List"));

                }
            }
        }
        public void BindBulkNormalizationChanges()
        {
            LoadBulkNormalizationChanges = Visibility.Visible;
            NotifyPropertyChanged("LoadBulkNormalizationChanges");
            Task.Run(() =>
            {
                var result = objCommonFunc.DbaseQueryReturnSqlList<IBulkNormalization>("select 'false' as IsSelected, a.QSID_ID, y.QSID, max " +
                    " (version) as MaxVersion, max(filepath) as FilePath from Log_VersionControl_History a, " +
                    " library_qsids y where a.QSID_ID not in " +
                    " (select distinct QSID_ID from Log_VersionControl_History where Currently_Checked_Out = 1) " +
                    " and a.QSID_ID = y.QSID_ID and y.isactive = 1" +
                    " group by a.QSID_ID, y.QSID ", stringSqlCon);
                
                ListBulkNormalizationChanges = new ObservableCollection<IBulkNormalization>(result);
                LoadBulkNormalizationChanges = Visibility.Collapsed;
                NotifyPropertyChanged("LoadBulkNormalizationChanges");
            });

        }

        List<IBulkNormalizationStatus> listSelected = new List<IBulkNormalizationStatus>();
        List<IBulkNormalizationStatus> listSelectedNew = new List<IBulkNormalizationStatus>();
        public void BindBulkNormalizationStatusChanges()
        {
            LoadBulkNormalizationStatusChanges = Visibility.Visible;
            NotifyPropertyChanged("LoadBulkNormalizationStatusChanges");
            Task.Run(() =>
            {
                ListBulkNormalizationStatusChanges = new ObservableCollection<IBulkNormalizationStatus>(listSelected);
                LoadBulkNormalizationStatusChanges = Visibility.Collapsed;
                NotifyPropertyChanged("LoadBulkNormalizationStatusChanges");
            });

        }


    }
    public class IBulkNormalization
    {
        public bool IsSelected { get; set; }
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string FilePath { get; set; }
    }
    public class IBulkNormalizationStatus
    {
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string Status { get; set; }
        public string CheckType { get; set; }
        public string CheckDescription { get; set; }
    }
    public class libError
    {
        public int LibraryErrorWarningID { get; set; }
        public string CheckDescription { get; set; }
        public string CheckType { get; set; }
    }
}

