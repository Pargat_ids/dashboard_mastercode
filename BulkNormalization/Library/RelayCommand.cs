﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BulkNormalization.Library
{
    public class RelayCommand<T> : RelayCommand where T : class
    {
        public RelayCommand(Action<object> execute) : base(execute)
        { }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute) : base(execute, canExecute)
        { }
    }
    public class RelayCommand : ICommand
    {
        #region Constants and Fields

        private readonly Predicate<object> _canExecute;

        private readonly Action<object> _execute;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// </summary>
        /// <param name = "execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        ///   Creates a new command.
        /// </summary>
        /// <param name = "execute">The execution logic.</param>
        /// <param name = "canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException($"execute");
            _canExecute = canExecute;
        }

        #endregion

        #region Events

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        #endregion

        #region Implemented Interfaces

        #region ICommand

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            //if (CanExecuteChanged != null)
            //    CanExecuteChanged(this, new EventArgs());
            CommandManager.InvalidateRequerySuggested();
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }

        #endregion

        #endregion
    }
}
