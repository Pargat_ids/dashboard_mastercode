﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class WorkFeedDisapprovedSug_ViewModel
    {
        public string ParentCas { get; set; }
        public string ChildCas { get; set; }
        public string ReviewedUser { get; set; }
        public string SuggestedUser { get; set; }
        public int? ParentCasID { get; set; }
        public int? ChildCasid { get; set; }
        public DateTime? ReviewedOn { get; set; }
        public DateTime? SuggestedOn { get; set; }
    }
}
