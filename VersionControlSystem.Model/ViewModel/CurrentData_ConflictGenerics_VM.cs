﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class CurrentData_ConflictGenerics_VM
    {
        public int ParentCasID { get; set; }
        public string ParentCas { get; set; }
        public int ChildCasID { get; set; }
        public string ChildCas { get; set; }
        public string ParentChemicalName { get; set; }
        public string ChemicalName { get; set; }
        public string ConflictingGroup { get; set; }
    }
    public class CurrentDataGenerics_VM : CurrentData_ConflictGenerics_VM
    {
        public string Type { get; set; }
    }
}
