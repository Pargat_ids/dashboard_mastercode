﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class Map_GenericCalculatedDetail_NewAdd
    {
        public string Cas { get; set; }
        public int? CasID { get; set; }
        public string ChildName { get; set; }
    }
    public class Map_GenericCalculatedDetail_Delete
    {
        public string Cas { get; set; }
        public int? CasID { get; set; }
        public string ChildName { get; set; }
    }
    public class Map_GenericCalculatedDetail_Existing
    {
        public string Cas { get; set; }
        public int? CasID { get; set; }
        public string ChildName { get; set; }
    }
}
