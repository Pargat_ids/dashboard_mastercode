﻿using System;

namespace VersionControlSystem.Model.ViewModel
{
    public class Access_QSID_Data_VM
    {

        public int VersionControl_ID { get; set; }
        public int? ListVersionChangeTypeID { get; set; }
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string Version { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public Nullable<System.DateTime> Check_In_TimeStamp { get; set; }
        public Nullable<System.DateTime> Check_Out_TimeStamp { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> DateFirstAdded { get; set; }
        public string AddNote { get; set; }
        public Nullable<System.DateTime> ProductTimeStamp { get; set; }
        public string UniqueSubstance { get; set; }
        public string TotalCount { get; set; }
        public System.Collections.Generic.List<HyperLinkDataValue> WorkOrderNumber { get; set; }
        public string TypeChange { get; set; }
        public string EditorialChangeType { get; set; }
        public Nullable<bool> CheckedOut { get; set; }

        public int? TimeSpentValue { get; set; }
        public string TimeSpentUnit { get; set; }
        public System.Collections.Generic.List<HyperLinkDataValue> TicketNumber { get; set; }
        //public Nullable<bool> IsLocked { get; set; }        

    }
}
