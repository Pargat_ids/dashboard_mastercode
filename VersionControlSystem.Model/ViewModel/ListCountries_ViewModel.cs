﻿using System;
using System.ComponentModel;

namespace VersionControlSystem.Model.ViewModel
{
    public class ListCountries_ViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ListCheckBoxType_ViewModel
    {
        public string Content { get; set; }
        public bool Value { get; set; }
    }
    public class ListCommonComboBox_ViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Id_String { get; set; }
    }
    public class ListParentChild_ViewModel
    {

        public int ParentQSIDID { get; set; }
        public int ChildQSIDID { get; set; }
        public string ParentQsid { get; set; }
        public string ChildQsid { get; set; }
    }    
}
