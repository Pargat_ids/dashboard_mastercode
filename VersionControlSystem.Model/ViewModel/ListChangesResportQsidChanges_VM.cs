﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class ListChangesResportQsidChanges_VM
    {
        public int QSID_ID { get; set; }
        public string Qsid { get; set; }
        public ListVersionChangeTypes_VM ListVersionChangeType { get; set; }
        public List<HyperLinkDataValue> WoNumber { get; set; }
        public string Versions { get; set; }
        public string ListFieldName { get; set; }
        public string NewListPhrase { get; set; }
        public string OldListPhrase { get; set; }
        public string IsMatch { get; set; }
        public int RowAdded { get; set; }
        public int RowChanged { get; set; }
        public int RowDeleted { get; set; }
        public int FieldsAdded { get; set; }
        public int FieldsDeleted { get; set; }
        public int ListDictionaryChanges { get; set; }
        public int CasAdded { get; set; }
        public int CasDeleted { get; set; }
        public DateTime? ProductDateMin { get; set; }
        public DateTime? ProductDateMax { get; set; }
        public int CountNOL { get; set; }
        public int FieldExplainChanges { get; set; }
    }
    public class ListVersionChangeTypes_VM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ListChangesReportQsidAdd_VM
    {
        public int QSID_ID { get; set; }
        public string Qsid { get; set; }
        public string ShortName { get; set; }
        public string ListPhrase { get; set; }
        public string ListFieldName { get; set; }
        public List<HyperLinkDataValue> WoNumber { get; set; }
        public string Versions { get; set; }
        public int RowAdded { get; set; }
        public int NormalizedFields { get; set; }
        public int CasAdded { get; set; }
        public DateTime? ProductDateMin { get; set; }
        public DateTime? ProductDateMax { get; set; }
        public int CountNOL { get; set; }

    }
    public class ListChangesReportQsidDelete_VM
    {
        public int QSID_ID { get; set; }
        public string Qsid { get; set; }
        public string ShortName { get; set; }
        public string ListPhrase { get; set; }
        public string ListFieldName { get; set; }
        public List<HyperLinkDataValue> WoNumber { get; set; }
        public string Versions { get; set; }
        public DateTime? ProductDateMin { get; set; }
        public DateTime? ProductDateMax { get; set; }

    }
    public class ListChangesReportAddQsidFieldPopUP
    {
        public string Qsid { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayHeader { get; set; }
        public string FieldExplanation { get; set; }
        public DateTime DateAddedDeleted { get; set; }
    }
    public class ListChangesReportDeleteFieldPopUP
    {
        public string Qsid { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayHeader { get; set; }
        public string FieldExplanation { get; set; }
        public DateTime DateAddedDeleted { get; set; }
    }
    public class ListChangesReportListDictionaryPopUP
    {        
        public string PhraseCategory { get; set; }
        public string NewValue { get; set; }
        public string OldValue { get; set; }
    }

    public class ListChangesReportNOLPopUP
    {
        public string FieldName { get; set; }
        public string LanguageName { get; set; }
    }

}

