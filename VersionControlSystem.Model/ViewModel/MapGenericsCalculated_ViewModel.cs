﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class MapGenericsCalculated_ViewModel
    {
        public List<HyperLinkDataValue> Qsid { get; set; }

        public string QsidFilter { get; set; }
        public string Cas
        {
            get;
            set;
        }

        public int? CasID
        {
            get;
            set;
        }

        public string ParentName
        {
            get;
            set;
        }

        public int Qsid_ID
        {
            get;
            set;
        }

        public bool IsUniqueCas
        {
            get;
            set;
        }

        public string FieldName
        {
            get;
            set;
        }

        public int? FieldName_ID
        {
            get;
            set;
        }

        public string CriteriaType
        {
            get;
            set;
        }

        public string CriteriaValue
        {
            get;
            set;
        }

        public string NextCondition
        {
            get;
            set;
        }

        public int? GroupCriteriaNo
        {
            get;
            set;
        }
    }
}
