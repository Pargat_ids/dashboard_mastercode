﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class Log_Map_QSID_ERC_EHSListCode_VM : INotifyPropertyChanged
    {
        private string _EHS_ListCode { get; set; }
        public string EHS_ListCode
        {
            get { return _EHS_ListCode; }
            set
            {
                if (_EHS_ListCode != value)
                {
                    _EHS_ListCode = value;
                    OnPropertyChanged("EHS_ListCode");
                }
            }
        }
        public DateTime CreatedDate { get; set; }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
