﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class GenericSuggestionTree_ViewModel : INotifyPropertyChanged
    {
        public int GenericSuggestion_ID { get; set; }
        public string RowID { get; set; }
        private bool? _IsExclude { get; set; }
        public bool? IsExclude
        {
            get { return _IsExclude; }
            set
            {
                if (value != _IsExclude)
                {
                    _IsExclude = value;
                    NotifyPropertyChanged("IsExclude");
                }
            }
        }
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int? ChildCasID { get; set; }
        private string _status { get; set; }
        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    NotifyPropertyChanged("Status");
                }
            }
        }
        public string UserID { get; set; }
        public string Note { get; set; }
        public string ParentName { get; set; }
        public string ChildName { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        public string XPath { get; set; }
        public Map_Generic_Detail_VM Parent_SubstanceInfo { get; set; }
        public Map_Generic_Detail_VM Child_SubstanceInfo { get; set; }
        public int FlatGenericsCount { get; set; }
        public int Level { get; set; }
        private bool? _isParentGenCalculated { get; set; }
        public bool? IsParentGenCalculated
        {
            get { return _isParentGenCalculated; }
            set
            {
                _isParentGenCalculated = value;
                NotifyPropertyChanged("IsParentGenCalculated");
            }
        }

        private bool? _isCasMarkedClosed { get; set; }
        public bool? IsCasMarkedClosed
        {
            get { return _isCasMarkedClosed; }
            set
            {
                _isCasMarkedClosed = value;
                NotifyPropertyChanged("IsCasMarkedClosed");
            }
        }
    }
    public class GenericSuggestionFilterModel
    {
        public int? ParentCasID { get; set; }
        public string Path { get; set; }
        public int? ChildCasID { get; set; }
    }
    public class GenericSuggestionClosedGroup
    {
        public int ParentCASID { get; set; }
        public string ParentCAS { get; set; }
        public string ParentCASName { get; set; }
        public int NoDirectChildren { get; set; }
        public string ClosedBy { get; set; }
        public DateTime ClosedOn { get; set; }
    }

}
