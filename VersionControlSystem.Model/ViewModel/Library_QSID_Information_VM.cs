﻿using System;
using System.ComponentModel;

namespace VersionControlSystem.Model.ViewModel
{
    public class Library_QSID_Information_VM : INotifyPropertyChanged
    {
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string ListFieldName { get; set; }
        public string ListId { get; set; }
        public string ShortName { get; set; }
        public DateTime ListPublicationDate { get; set; }
        public ListCountries_ViewModel Country { get; set; }
        public ListModule_ViewModel Module { get; set; }
        public ListTopic_ViewModel Topic { get; set; }
        public string ListType { get; set; }
        public string ListIndustryVertical { get; set; }
        public ListCommonComboBox_ViewModel ListObligationType { get; set; }
        public string ListPhrase { get; set; }
        public string UniqueSubstances { get; set; }
        public string TotalRecords { get; set; }
        public ListCheckBoxType_ViewModel IsDeleted { get; set; }

        public int? DirectHitCount { get; set; }
        public int? GenericHitCount { get; set; }
        public string ProductName { get; set; }
        public string EHS_ListCodes { get; set; }
        public string NoteUrl { get; set; }
        public string ShortDescription { get; set; }

        public string ParentQsid { get; set; }

        public string ChildQsid { get; set; }
        public int? GenericsCount { get; set; }

        private ListCommonComboBox_ViewModel _currentlyCheckedOut { get; set; }
        public ListCommonComboBox_ViewModel CurrentlyCheckedOut
        {
            get { return _currentlyCheckedOut; }
            set
            {
                if (_currentlyCheckedOut != value)
                {
                    _currentlyCheckedOut = value;
                    OnPropertyChanged("CurrentlyCheckedOut");
                }
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class CompareQsidListDetail : Library_QSID_Information_VM, INotifyPropertyChanged
    {

        private bool _isCheckedForCompare { get; set; }
        public bool IsCheckedForCompare
        {
            get { return _isCheckedForCompare; }
            set
            {
                _isCheckedForCompare = value;
                NotifyPropertyChanged("IsCheckedForCompare");
            }
        }


        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChangedEvent != null)
            {
                PropertyChangedEvent(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChangedEvent;
    }

    public class QcChecksDetail_VM : INotifyPropertyChanged
    {
        public int QcCheckID { get; set; }
        public string QcCheckName { get; set; }
        private bool _isCompleted { get; set; }

        public bool IsCompleted
        {
            get { return _isCompleted; }
            set
            {
                _isCompleted = value;
                NotifyPropertyChanged("IsCompleted");
            }
        }
        private bool _isNotApplicable { get; set; }
        public string QcCheckDescription { get; set; }
        public bool IsNotApplicable
        {
            get
            {
                return _isNotApplicable;
            }
            set
            {
                _isNotApplicable = value;
                if (_isNotApplicable)
                {
                    IsEnabled = false;
                }
                else
                {
                    IsEnabled = true;
                }
                NotifyPropertyChanged("IsNotApplicable");
            }
        }
        public string UserName { get; set; }
        public string DateAdded { get; set; }
        private bool? _isEnabled { get; set; }
        public bool? IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                NotifyPropertyChanged("IsEnabled");
            }
        }
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
