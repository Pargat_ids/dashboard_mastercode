﻿using System.ComponentModel;

namespace VersionControlSystem.Model.ViewModel
{
    public class ListEditorialChangeTypes_VM : INotifyPropertyChanged
    {
        public int ListEditorialChangeTypeID { get; set; }
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string ListEditorialChangeType { get; set; }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
