﻿using CRA_DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class Property_EncryptHexa_VM
    {
        public string InputBaseVal { get; set; }
        public string OutputHexaVal { get; set; }
        public string PropertyName { get; set; }

    }
    public class Property_DecryptHexa_VM
    {
        public string InputHexaVal { get; set; }
        public string OutputBaseVal { get; set; }
        public string PropertyName { get; set; }

        public EchaListDetail_Decrypt QsCasIDDetail { get; set; }

    }
    public class EchaListEntryBusinessKey_VM
    {
        public string EchaListEntryBusinessKey { get; set; }
        public int QSCASID { get; set; }
        public int RowID { get; set; }
        public string EchaListBusinessKey { get; set; }
        public int CasId { get; set; }
        public string Cas { get; set; }
        public string EnglishNameOnList { get; set; }
        public string AccessFileRN { get; set; }

    }
    public class EchaBusinessKey_VM
    {
        public string QSID { get; set; }
        public int QSID_ID { get; set; }     
        public string EchaLegislationListBusinessKey { get; set; }
        public string EchaLegislationBusinessKey { get; set; }
        public string ListFieldName { get; set; }
        public string ListShortName { get; set; }
        public string ListPhrase { get; set; }
        public string LegislationShortName { get; set; }
        public string LegislationPhrase { get; set; }

    }
}
