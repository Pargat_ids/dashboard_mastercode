﻿using System;
using System.ComponentModel;
using System.Windows;

namespace VersionControlSystem.Model.ViewModel
{
    public class MapProductListVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string ProductName { get; set; }
        public string ProductInternalCode { get; set; }
        public int ProductID { get; set; }
        public int Qsid_ID { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class MapDateFormat : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string DateFormat { get; set; }
        public int DateFormatID { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class MapParentChildVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string Qsid { get; set; }
        public int Qsid_ID { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class MapFieldListVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string FieldName { get; set; }
        private IDelimiter _SelectedDelimiter { get; set; }
        public IDelimiter SelectedDelimiter
        {
            get => _SelectedDelimiter;
            set
            {
                if (Equals(_SelectedDelimiter, value))
                {
                    return;
                }

                _SelectedDelimiter = value;
            }
        }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class MapFieldListNameAssVM : ICloneable, INotifyPropertyChanged
    {
        public string FieldType { get; set; }
        private IType _SelectedFieldName { get; set; }
        public IType SelectedFieldName
        {
            get => _SelectedFieldName;
            set
            {
                if (Equals(_SelectedFieldName, value))
                {
                    return;
                }

                _SelectedFieldName = value;
            }
        }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class MapFieldListMatchingCriteriaVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string FieldName { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class MapFieldListConcatenateCriteriaVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string FieldName { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class MapStripCharacters : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string SquashCharacter { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }

    public class MapChecksVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string ChkDescription { get; set; }
        public int ParentID { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class MapColumnsVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string ColumnName { get; set; }
        public string ActualColumnName { get; set; }
        public bool IsInternalOnly { get; set; }
        public bool IsPrevCriteria { get; set; }
        private Visibility _IsVisible { get; set; }
        public Visibility IsVisible
        {
            get { return _IsVisible; }
            set
            {
                if (value != _IsVisible)
                {
                    _IsVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }
        }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class IDelimiter
    {
        public int ID { get; set; }
        public string Delimiter { get; set; }
    }
    public class IType
    {
        //public int ID { get; set; }
        public string Type { get; set; }
    }
    public class MapPhraseListVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsPhraseSelected { get; set; }
        public bool IsPhraseSelected
        {
            get { return _IsPhraseSelected; }
            set
            {
                if (value != _IsPhraseSelected)
                {
                    _IsPhraseSelected = value;
                    OnPropertyChanged("IsPhraseSelected");
                }
            }
        }
        public string FieldNamePhrase { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }

    public class MapUniqueFieldListVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsUniqueFieldSelected { get; set; }
        public bool IsUniqueFieldSelected
        {
            get { return _IsUniqueFieldSelected; }
            set
            {
                if (value != _IsUniqueFieldSelected)
                {
                    _IsUniqueFieldSelected = value;
                    OnPropertyChanged("IsUniqueFieldSelected");
                }
            }
        }
        public string FieldNameUnique { get; set; }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class IRelationShip
    {
        public int ID { get; set; }
        public string ListRelationship_Name { get; set; }

    }

    public class MapFieldListRelationVM : ICloneable, INotifyPropertyChanged
    {
        private bool _IsSelected { get; set; }
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        public string Qsid { get; set; }
        public int Qsid_ID { get; set; }
        private IRelationShip _SelectedRelation { get; set; }
        public IRelationShip SelectedRelation
        {
            get => _SelectedRelation;
            set
            {
                if (Equals(_SelectedRelation, value))
                {
                    return;
                }

                _SelectedRelation = value;
            }
        }
        public int ListRelationshipID { get; set; }
        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
