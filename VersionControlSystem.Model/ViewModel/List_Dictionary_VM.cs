﻿namespace VersionControlSystem.Model.ViewModel
{
    public class List_Dictionary_VM
    {
        public string QSID { get; set; }
        public string List_Field_Name { get; set; }
        public int Country { get; set; }
        public string Module { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string ShortDescription { get; set; }
    }
}
