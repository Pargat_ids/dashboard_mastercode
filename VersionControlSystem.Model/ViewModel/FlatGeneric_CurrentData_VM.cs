﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class FlatGeneric_CurrentData_VM
    {
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string ChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ParentChemicalNameID { get; set; }
    }
    public class CombinedGeneric_CurrentData_VM
    {
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string ChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ParentChemicalNameID { get; set; }
        public string Type { get; set; }
    }
}
