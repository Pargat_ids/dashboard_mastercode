﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class GenericSuggestionComments_VM
    {
        public int CommentID { get; set; }

        public int? ParentCasID { get; set; }

        public int? ChildCasID { get; set; }


        public string Comments { get; set; }

        public string Action { get; set; }

        public string CommentBy { get; set; }

        public DateTime? Timestamp { get; set; }
        public bool  Additional_Generics_research_flag{get;set;}
    }
}
