﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class GroupThreshold_ListVM
    {
        public int ChemicalNameID { get; set; }
        public int LibGroupThresholdNameID { get; set; }
        public string GroupThresholdName { get; set; }
        public string comments { get; set; }
        public int CountMembers { get; set; }
        public int CountAppName { get; set; }

    }
   
}
