﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class CommonDataGridColumn
    {
        public bool IsSelected { get; set; }
        public string ColName { get; set; }
        public string ColBindingName { get; set; }
        public string ColType { get; set; }
        public string ColFilterMemberPath { get; set; }
        public string ColumnWidth { get; set; }
        public string ContentName { get; set; } //If Column Type Button
        public string BackgroundColor { get; set; } //If Column Type Button
        public string CommandParam { get; set; } //If Column Type Button
        public string CustomStyleClass { get; set; } //If Column Type Button       

    }
}
