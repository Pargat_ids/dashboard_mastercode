﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class ColumnSelectionDataGrid_VM
    {
        public bool IsSelected { get; set; }

        public string ColumnName { get; set; }
    }
}
