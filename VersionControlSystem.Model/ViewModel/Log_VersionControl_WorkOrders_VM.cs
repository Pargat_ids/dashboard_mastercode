﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace VersionControlSystem.Model.ViewModel
{
    public class Log_VersionControl_WorkOrders_VM : INotifyPropertyChanged
    {
        public int Log_VersionControl_WorkOrderID { get; set; }
        public int? VersionControl_HistoryID { get; set; }
        private int? workOrder_Number { get; set; }
        public int? WO_Number
        {
            get { return workOrder_Number; }
            set
            {
                if (workOrder_Number != value)
                {
                    workOrder_Number = value;
                    OnPropertyChanged("WO_Number");
                }
            }
        }
        public DateTime CreatedDate { get; set; }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class Log_VersionControl_Tickets_VM : INotifyPropertyChanged
    {            
        private string _ticket_Number { get; set; }
        public string Ticket_Number
        {
            get { return _ticket_Number; }
            set
            {
                if (_ticket_Number != value)
                {
                    _ticket_Number = value;
                    OnPropertyChanged("Ticket_Number");
                }
            }
        }
        private string _ticket_Url { get; set; }
        public string Ticket_Url
        {
            get { return _ticket_Url; }
            set
            {
                if (_ticket_Url != value)
                {
                    _ticket_Url = value;
                    OnPropertyChanged("Ticket_Url");
                }
            }
        }
        
        public DateTime CreatedDate { get; set; }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
    public class HyperLinkDataValue
    {
        public object NavigateData { get; set; }
        public string DisplayValue { get; set; }
        public bool? IsValid { get; set; }
    }
    public class WorkOrderValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            bool isValid = false;
            if (value != null)
            {
                int woNumber = 0;
                isValid = int.TryParse(value.ToString(), out woNumber);
            }
            else
            {
                isValid = true;
            }
            return isValid
                ? ValidationResult.ValidResult
                : new ValidationResult(false, "value should be numeric");
        }
    }
    public class NoteURLValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            bool isValid = false;
            if (value != null)
            {
                string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
                Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                isValid = reg.IsMatch(value.ToString());
            }
            else
            {
                isValid = true;
            }
            return isValid
                ? ValidationResult.ValidResult
                : new ValidationResult(false, "Url not valid");
        }
    }
}
