﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    //public class SubstanceSearchCasVariant_VM
    // {
    //     public string Qsid { get; set; }
    //     public string Cas { get; set; }

    // }
    public class SubstanceSearchIdentifier_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string QsidFilter { get; set; }
        public string Cas { get; set; }
        public string IdentifierType { get; set; }
        public string IdentifierValue { get; set; }
        public string SubstanceName { get; set; }
        public int ListCount { get; set; }

    }
    public class SubstanceSearchPhrase_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string QsidFilter { get; set; }
        public string Cas { get; set; }
        public string PhraseCategory { get; set; }
        public string Phrase { get; set; }
        public string SubstanceName { get; set; }
        public int ListCount { get; set; }

    }
    public class SubstanceSearchFieldName_VM
    {
        public string Qsid { get; set; }
        public int Qsid_ID { get; set; }
        public int FieldNameID { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayHeader { get; set; }
        public string FieldExplanation { get; set; }
        public string FieldReportPhrase { get; set; }
        public string List_Products { get; set; }
        public string Field_Products { get; set; }

    }

}
