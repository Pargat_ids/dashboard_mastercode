﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class Log_Map_EasiList_Qsid_VM : INotifyPropertyChanged
    {

        private int? _easiPro_ListID { get; set; }
        public int? EasiPro_ListID
        {
            get { return _easiPro_ListID; }
            set
            {
                if (_easiPro_ListID != value)
                {
                    _easiPro_ListID = value;
                    OnPropertyChanged("EasiPro_ListID");
                }
            }
        }
        public DateTime CreatedDate { get; set; }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
