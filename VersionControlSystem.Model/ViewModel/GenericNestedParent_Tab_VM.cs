﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
   public class GenericNestedParent_Tab_VM
    {
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string ParentName { get; set; }
        public int? ParentNameID { get; set; }
        public string ChildName { get; set; }
        public int? ChildNameID { get; set; }
        public int NestingLevel { get; set; }
        public int NestingGroupNo { get; set; }
        public string Status { get; set; }
        public bool? IsExclude { get; set; }
        public DateTime DateAdded { get; set; }
    }
    public class GenericRejected_VM
    {
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string ParentName { get; set; }
        public string Action { get; set; }
        
        public int? ParentNameID { get; set; }
        public string ChildName { get; set; }
        public int? ChildNameID { get; set; }
        public ListCheckBoxType_ViewModel IsExclude { get; set; }
        public ListCommonComboBox_ViewModel UserName { get; set; }
        public DateTime DateAdded { get; set; }
        public ListCommonComboBox_ViewModel SuggestedUserName { get; set; }
        public DateTime? SuggestedDateAdded { get; set; }
    }
    public class GenericDelete_VM
    {
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int ChildCasID { get; set; }
        public string ParentName { get; set; }
        public int? ParentNameID { get; set; }
        public string ChildName { get; set; }
        public int? ChildNameID { get; set; }
        public ListCheckBoxType_ViewModel IsExclude { get; set; }
        public ListCommonComboBox_ViewModel UserName { get; set; }
        public DateTime DateAdded { get; set; }
        public ListCommonComboBox_ViewModel SuggestedUserName { get; set; }
        public DateTime? SuggestedDateAdded { get; set; }
    }
    public class GenericResearchComment_VM
    {
        public int CommentID { get; set; }
        public string ParentCas { get; set; }
        public int ParentCasId { get; set; }
        public string ChildCas { get; set; }
        public string ParentName { get; set; }
        public int? ChildCasId { get; set; }
        public string Comments { get; set; }
        public ListCommonComboBox_ViewModel UserName { get; set; }
        public ListCommonComboBox_ViewModel AssignedUserName { get; set; }
        public DateTime? AdditionalResearchDeadline { get; set; }
        public DateTime Timestamp { get; set; }
        public string Type { get; set; }

    }
}
