﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class SubstanceSearchTab_VM
    {

        public List<HyperLinkDataValue> Qsid { get; set; }
        public string QsidFilter { get; set; }
        public string Cas { get; set; }
        public string Type { get; set; }
        public string IdentifierValue_SubstanceName { get; set; }
        public string SubstanceName_Language { get; set; }        
        public int SubstanceLength { get; set; }
        public int ListCount { get; set; }
        public string SubstanceType { get; set; }
        public string SubstanceCategory { get; set; }
        public int? ChildCount { get; set; }

    }
}
