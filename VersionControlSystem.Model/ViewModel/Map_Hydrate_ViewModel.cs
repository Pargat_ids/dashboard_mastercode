﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class Map_Hydrate_ViewModel
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int HydrateID { get; set; }
        public string Anhydrous_CAS { get; set; }
        public int? Anhydrous_CAS_ID { get; set; }
        public int? AnhydrousChemicalNameID { get; set; }
        public int? HydrousChemicalNameID { get; set; }
        public string Anhydrous_Name { get; set; }
        public string Hydrous_CAS { get; set; }
        public int? Hydrous_CAS_ID { get; set; }
        public string Hydrous_Name { get; set; }
        public string Status { get; set; }
        public DateTime TimeStamp { get; set; }
        public ListCommonComboBox_ViewModel UserName { get; set; }
        public string Comments { get; set; }
        public int? SessionID { get; set; }
        public int QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
    public class Map_AlternateCas_ViewModel{
        public List<HyperLinkDataValue> Qsid { get; set; }
        public int AlternateID { get; set; }
        public int PrimaryCASID { get; set; }
        public string PrimaryCAS { get; set; }
        public int AlternateCASID { get; set; }
        public string AlternateCAS { get; set; }
        public string Status { get; set; }
        public DateTime TimeStamp { get; set; }
        public ListCommonComboBox_ViewModel UserName { get; set; }
        public string Comments { get; set; }
        public int? SessionID { get; set; }
        public int QsidCount { get; set; }
        public string QsidFilter { get; set; }
    }
}
