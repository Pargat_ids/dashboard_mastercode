﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControlSystem.Model.ViewModel
{
    public class GenericComment_VM : INotifyPropertyChanged
    {
        private string parent_Cas { get; set; }
        private string child_Cas { get; set; }
        public string ParentCas
        {
            get { return parent_Cas; }
            set
            {
                parent_Cas = value;
                NotifyPropertyChanged("ParentCas");
            }
        }
        public string ChildCas
        {
            get { return child_Cas; }
            set
            {
                child_Cas = value;
                NotifyPropertyChanged("ChildCas");
            }
        }
        public int? ParentCasId { get; set; } = 0;
        public int? ChildCasId { get; set; } = 0;
        public double CustomScreenHeight { get; set; }
        public bool IsParentChildView { get; set; } 
        public bool IsChemicalCommentView { get; set; }
        public string ChemicalName { get; set; }
        public int? ChemicalNameId { get; set; }
        public string  CommentType { get; set; }
        public bool IsIdentifierCommentView { get; set; }
        public int? IdentifierTypeID { get; set; }

        public GenericComment_VM(string _parentCas="",string _childCas="",int? _parentCasId=0,int?_childCasId=0,double _customScreenHeight = 370,bool _isParentChildView = true, string _commentType = "", bool _isChemicalCommentView=false,string _chemicalName="",int? _chemicalNameId=0,bool _isIdentifierCommentView=false,int? _identifierTypeID=null)
        {
            ParentCas = _parentCas;
            ChildCas = _childCas;
            ParentCasId = _parentCasId;
            ChildCasId = _childCasId;
            CustomScreenHeight = _customScreenHeight;
            IsParentChildView = _isParentChildView;
            IsChemicalCommentView = _isChemicalCommentView;
            ChemicalName = _chemicalName;
            ChemicalNameId = _chemicalNameId;
            CommentType = _commentType;
            IsIdentifierCommentView = _isIdentifierCommentView;
            IdentifierTypeID = _identifierTypeID;
            NotifyPropertyChanged("ParentCasId");
            NotifyPropertyChanged("ChildCasId");
            NotifyPropertyChanged("CustomScreenHeight"); NotifyPropertyChanged("IsParentChildView");
            NotifyPropertyChanged("IsChemicalCommentView");
            NotifyPropertyChanged("ChemicalName");
            NotifyPropertyChanged("ChemicalNameId");
            NotifyPropertyChanged("IsIdentifierCommentView");
            NotifyPropertyChanged("IdentifierTypeID");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
