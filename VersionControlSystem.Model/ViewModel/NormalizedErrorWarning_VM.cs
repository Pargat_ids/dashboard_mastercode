﻿namespace VersionControlSystem.Model.ViewModel
{
    public class NormalizedErrorWarning_VM
    {
        public string CheckType { get; set; }
        public string Description { get; set; }
        public string StatusImagePath { get; set; }
    }
    public class LogNormalizedAddQsidField {
        public string Qsid { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayHeader { get; set; }
        public string FieldExplanation { get; set; }
    }
    public class LogNormalizedDeleteQsidField
    {
        public string Qsid { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayHeader { get; set; }
        public string FieldExplanation { get; set; }
    }

}
