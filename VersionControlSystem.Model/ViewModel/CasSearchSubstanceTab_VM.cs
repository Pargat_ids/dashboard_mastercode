﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess.ViewModel;

namespace VersionControlSystem.Model.ViewModel
{
   public class CasSearchSubstanceTab_VM
    {
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string QsidFilter { get; set; }
        // public string ListFieldName { get; set; }
        public CasType Cas { get; set; }
        public string Type { get; set; }
        public string IdentifierValue_SubstanceName { get; set; }
        public string SubstanceName_Language { get; set; }
        //public string ChemicalName { get; set; }
        //public string ChemicalNameSquash { get; set; }
        //public int? Frequency { get; set; }
        //public string ParentCAS { get; set; }
        //public string IdentifierValue { get; set; }
        //public string Phrase { get; set; }
        //public string PhraseCategory { get; set; }
        //public int SubstanceLength { get; set; }
        public int ListCount { get; set; }
    }

    public class DateSearchTab_VM
    {
        public string QsidFilter { get; set; }
        public List<HyperLinkDataValue> Qsid { get; set; }
        public string FieldName { get; set; }
        public string Phrase { get; set; }
        public string Harmonized_Date { get; set; }
        public int No_of_ListCount { get; set; }
    }
}
