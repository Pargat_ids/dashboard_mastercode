﻿namespace VersionControlSystem.Model.ViewModel
{
    public class QSID_VM
    {
        public int QSID_ID { get; set; }
        public string QSID { get; set; }
        public string Module { get; set; }
    }
    public class filterList
    {
        public string ColName { get; set; }
        public string ColValue { get; set; }
    }

    public class SelectListItem
    {
        public string Text { get; set; }
    }


}
