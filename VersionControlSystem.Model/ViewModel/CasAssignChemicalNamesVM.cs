﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace VersionControlSystem.Model.ViewModel
{
   
    public class CasDetailVM : INotifyPropertyChanged
    {
        public int? CasID { get; set; }
        public string Cas { get; set; }
        public int QsidCount { get; set; }
        public bool IsValid { get; set; }
        public Visibility ListOpenVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<string> _listQsidInMultiCas { get; set; }
        public ObservableCollection<string> listQsidInMultiCas
        {
            get { return _listQsidInMultiCas; }
            set
            {
                if (value != null)
                {
                    if (value != _listQsidInMultiCas)
                    {
                        _listQsidInMultiCas = value;
                        OnPropertyChanged("listQsidInMultiCas");
                        ListOpenVisibility = Visibility.Visible;
                        OnPropertyChanged("ListOpenVisibility");
                    }
                }
                else
                {
                    _listQsidInMultiCas = value;
                    ListOpenVisibility = Visibility.Collapsed;
                    OnPropertyChanged("ListOpenVisibility");
                }
            }
        }

        public int? SubstanceTypeID { get; set; }
        public int? SubstanceCategoryID { get; set; }
        public bool IsInCorrect_Cas { get; set; }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class CasAssignmentSummary
    {
        public string SummaryCountDetail { get; set; }
        public string CasSummary { get; set; }
        public int? count { get; set; }
        public string ForegroundCol { get; set; }
    }
    public class Cas_MIC_ING_Seed
    {

        public string Code { get; set; }
        public string CatCode { get; set; }
        public string CatName { get; set; }
        public string Prefix { get; set; }
        public int? NextSeed { get; set; }
    }
    public class CasValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return string.IsNullOrWhiteSpace((value ?? "").ToString())
                ? ValidationResult.ValidResult
                : (value.ToString().Length < 3 || value.ToString().Length > 255) ? new ValidationResult(false, "Cas length should be > 3 and < 15.") : ValidationResult.ValidResult;
        }
    }
    public class SubstanceType_VM
    {
        public int SubstanceTypeID { get; set; }
        public string SubstanceTypeName { get; set; }
    }

    public class SubstanceCategories_VM
    {
        public int SubstanceCategoryID { get; set; }
        public string SubstanceCategoryName { get; set; }
    }
    public class Map_Incorrect_Cas_ChemicalName_VM : INotifyPropertyChanged
    {
        public List<HyperLinkDataValue> ListQsid { get; set; }
        public int Map_IncorrectID { get; set; }
        public int? CasID { get; set; }
        public int? ChemicalNameId { get; set; }
        public int? Qsid_Id { get; set; }
        public bool? CasIsValid { get; set; }
        public string Cas { get; set; }
        public string LanguageName { get; set; }
        public string ChemicalName { get; set; }
        public string Qsid { get; set; }
        public int ActiveListsCount { get; set; }
        public bool _activeListsQsidVisibility { get; set; } = false;
        public bool ActiveListsQsidVisibility
        {
            get { return _activeListsQsidVisibility; }
            set
            {
                if (_activeListsQsidVisibility != value)
                {
                    _activeListsQsidVisibility = value;
                    OnPropertyChanged("ActiveListsQsidVisibility");
                }
            }
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
