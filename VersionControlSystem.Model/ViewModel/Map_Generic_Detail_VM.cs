﻿using System;

namespace VersionControlSystem.Model.ViewModel
{
    public class Map_Generic_Detail_VM
    {
        public int? GenericsCategoryID { get; set; }
        public string GenericsCategoryName { get; set; }
        public int? SubstanceTypeID { get; set; }
        public string SubstanceTypeName { get; set; }
        public int? SubstanceCategoryID { get; set; }
        public string SubstanceCategoryName { get; set; }
    }
    public class GenericSuggestion_Review_VM
    {
        public int GenericSuggestion_ID { get; set; }   
        public bool SelectedGenSuggestion { get; set; }

        public ListCheckBoxType_ViewModel IsExclude
        {
            get;set;
        }
        public ListCheckBoxType_ViewModel ParentCasIsValid
        {
            get; set;
        }
        public ListCheckBoxType_ViewModel ChildCasIsValid
        {
            get; set;
        }
        public string ParentCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ChildCas { get; set; }
        public int? ChildCasID { get; set; }      
        public string Status
        {
            get; set;
        }
        public string Note { get; set; }
        public string UserID { get; set; }
        public ListCommonComboBox_ViewModel SuggestedBy { get; set; }      
        public string ParentName { get; set; }
        public string ChildName { get; set; }        
        public Map_Generic_Detail_VM Parent_SubstanceInfo { get; set; }
        public Map_Generic_Detail_VM Child_SubstanceInfo { get; set; }
        public string DeletedAlreadyFlag { get; set; }
        public DateTime? DateSuggest  { get; set; }
    }
    public class GenericSuggestion_Add_VM
    {      
        public int? orderLevel { get; set; }
        public string NodeType { get; set; }
        public DateTime Timestamp { get; set; }
        public int? ActiveListCount { get; set; }
        public int ChildrenCount { get; set; }
        public int? ParentChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }
        public Map_Generic_Detail_VM ParentMapGenericIDs { get; set; }
        public ListCheckBoxType_ViewModel IsExclude { get; set; }
        public ListCheckBoxType_ViewModel ParentCasIsValid { get; set; }
        public ListCheckBoxType_ViewModel ChildCasIsValid { get; set; }        
        public string Note { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        public int ChildCasID { get; set; }
        public string ChildCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ParentCas { get; set; }
        public string RowID { get; set; }
        public int GenericSuggestion_ID { get; set; }
        public string ParentComment { get; set; }
        public bool? IsExistInGeneric { get; set; }
        public Map_Generic_Detail_VM ChildMapGenericIDs { get; set; }

    }
    public class GenericSuggestion_Reject_VM
    {
       
        public string ParentChemicalName { get; set; }
   
        public string ChemicalName { get; set; }
    
        public string ChildCas { get; set; }
      
        public string ParentCas { get; set; }

        public string ReasonRejected { get; set; }
    }
    public class Generic_CircularRef_VM
    {        
        public DateTime Timestamp { get; set; }        
        public int? ParentChemicalNameID { get; set; }
        public string ParentChemicalName { get; set; }
        public int? ChemicalNameID { get; set; }
        public string ChemicalName { get; set; }
        public Map_Generic_Detail_VM ParentMapGenericIDs { get; set; }
        public ListCheckBoxType_ViewModel IsExclude { get; set; }
        public string Note { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        public int ChildCasID { get; set; }
        public string ChildCas { get; set; }
        public int? ParentCasID { get; set; }
        public string ParentCas { get; set; }
        

    }
}