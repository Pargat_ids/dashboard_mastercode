﻿using CRA_DataAccess.ViewModel;
using System.Collections.Generic;
using VersionControlSystem.Model.DataModel;
using VersionControlSystem.Model.ViewModel;

namespace VersionControlSystem.Model.ApplicationEngine
{
    public static class Engine
    {
        public static string CommonAccessFolderPath = @"\\mdfs02.3ecorp.com\cralists\";
        //public static string CommonAccessFolderPath =  @"P:\Pargat\Projects\Current Development\AccessOutputFolder\"; 
        public static string CommonExportOldFolderPath = @"P:\Pargat\Projects\ExportOldPath\";
        public static string CommonCheckoutFolderPath = @"C:\CraWork\VersionControl\";
        public static string CommonAccessExportFolderPath = @"C:\CraWork\VersionControl\";
        public static string CommonDocumentPath = @"\\canfs01.can.local\CAN_Data\Dashboard_Documents\";
        public static string PrevDocumentPath = @"\\canfs01.can.local\CAN_Data\Dashboard_Documents_Archive\";
        public static string CurrentSelectedTabItem { get; set; }

        public static int CurrentUserSessionID { get; set; }
        public static string CurrentUserName { get; set; }
        public static bool IsApproveRejectGenericSuggestion { get; set; }
        public static List<ListCountries_ViewModel> _listCountry { get; set; }
        public static List<ListModule_ViewModel> _listModule { get; set; }
        public static List<ListTopic_ViewModel> _listTopic { get; set; }
        public static bool IsOpenCasAssignmentFromAccess { get; set; }
        public static string CasAssignmentAccessDBFilePath { get; set; }
        public static bool IsOpenExistingInstance { get; set; }
        public static string ToolVersion { get; set; }
        public static string DelimiterForQsidCasAssignmentTool { get; set; }
        public static bool IsOpenCurrentDataViewFromVersionHis { get; set; }
        public static bool IsOpenDataDictionaryFromVersionHis { get; set; }
        public static bool IsOpenListDictionaryFromVersionHis { get; set; }
        public static bool IsOpenAdditionalInfoFromVersionHis { get; set; }
        public static bool IsOpenUniquenessCriteriaFromVersionHis { get; set; }

        public static string DataDictionaryListDicPath { get; set; }
        public static bool IsOpenAdditionalInfoViewFromVersionHis { get; set; }
        public static string CurrentDataViewQsid { get; set; }
        public static int CurrentDataViewQsidID { get; set; }
        public static List<ListCheckBoxType_ViewModel> _listCheckboxType { get; set; }
        public static List<ListCommonComboBox_ViewModel> _listCheckedOutUser { get; set; }
        public static List<ListCommonComboBox_ViewModel> _listProducts { get; set; }
        public static List<ListCommonComboBox_ViewModel> _listObligationType { get; set; }
        public static List<ListCommonComboBox_ViewModel> _listTypeCategory { get; set; }
        public static List<ListVersionChangeTypes_VM> _listVersionChangeType { get; set; }
        public static string ToolDbType { get; set; }
        public static bool UploadChemPhrase { get; set; }
        public static List<MapPermission_VM> listAllPermissionDashboard { get; set; }

    }

}
