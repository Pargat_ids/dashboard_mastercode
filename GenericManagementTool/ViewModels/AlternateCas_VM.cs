﻿using GalaSoft.MvvmLight.Messaging;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using CommonGenericUIView.ViewModel;
using CommonGenericUIView.CustomUserControl;

namespace GenericManagementTool.ViewModels
{
    public class AlternateCas_VM : Base_ViewModel
    {
        public Visibility VisibilityListDetail_Loader { get; set; }
        public CommonDataGrid_ViewModel<Map_AlternateCas_ViewModel> comonDG_VM { get; set; }
        private bool _IsShowPopUp { get; set; }
        public bool IsShowPopUp
        {
            get { return _IsShowPopUp; }
            set
            {
                if (_IsShowPopUp != value)
                {
                    _IsShowPopUp = value;
                    NotifyPropertyChanged("IsShowPopUp");
                }
            }
        }
        public Map_AlternateCas_ViewModel SelectedMapAlternateCas { get; set; }
        private List<Map_AlternateCas_ViewModel> _listAlternateCas_VM { get; set; } = new List<Map_AlternateCas_ViewModel>();
        public List<Map_AlternateCas_ViewModel> ListAlternateCas_VM
        {
            get { return _listAlternateCas_VM; }
            set
            {
                if (_listAlternateCas_VM != value)
                {
                    _listAlternateCas_VM = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Map_AlternateCas_ViewModel>>>(new PropertyChangedMessage<List<Map_AlternateCas_ViewModel>>(null, _listAlternateCas_VM, "Default List"));
                }
            }
        }
        public ICommand CommandShowPopUpAddAlternateCas { get; private set; }
        public ICommand CommandAddAlternateCas { get; private set; }
        public ICommand CommandDeleteAlternateCas { get; private set; }
        public ICommand CommandAddMoreAlternateCas { get; private set; }
        public ICommand CommandClosePopUp { get; private set; }
        public Visibility PopUpContentAddAlternateVisibility { get; set; } = Visibility.Collapsed;
        public Visibility PopUpContentDeleteAlternateVisibility { get; set; } = Visibility.Collapsed;

        public string AlternateCAS { get; set; }
        public string PrimaryCAS { get; set; }
        public string ActionComment { get; set; }
        public bool IsEdit { get; set; } = true;
        public string CasSearchSubstanceIdentity { get; set; }
        public ObservableCollection<SelectListItem> listItemSearchTypeForGeneric { get; set; }
        private SelectListItem _selectedSearch_AlternateCas { get; set; }
        public SelectListItem selectedSearch_AlternateCas
        {
            get { return _selectedSearch_AlternateCas; }
            set
            {
                _selectedSearch_AlternateCas = value;
                NotifyPropertyChanged("selectedSearch_AlternateCas");
            }
        }
        public string searchCas_AlternateCas { get; set; } = string.Empty;
        public ICommand CommandSearchCasAlternateTab { get; set; }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public CommentsUC_VM objCommentDC_AlternateCas { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public Visibility IsSubstanceIdentityView { get; set; }
        public AlternateCas_VM(string _cas = null, bool flag = false)
        {
            if (_cas != null)
            {
                CasSearchSubstanceIdentity = _cas;
                IsSubstanceIdentityView = Visibility.Collapsed;
            }
            else
            {
                CasSearchSubstanceIdentity = null;
                IsSubstanceIdentityView = Visibility.Visible;
            }
            BindListSearchTypeFilterQSID();
            comonDG_VM = new CommonDataGrid_ViewModel<Map_AlternateCas_ViewModel>(GetListGridColumn(), "AlternateCAS", "Alternate Cas:");
            BindListAlternateCas();
            CommandShowPopUpAddAlternateCas = new RelayCommand(CommandShowPopUpAddAlternateCasExecute);
            CommandAddAlternateCas = new RelayCommand(CommandAddAlternateCasExecute, CommandAddAlternateCasCanExecute);
            CommandAddMoreAlternateCas = new RelayCommand(CommandAddMoreAlternateCasExecute, CommandAddAlternateCasCanExecute);
            CommandClosePopUp = new RelayCommand(CommandClosePopUpExecute);
            CommandSearchCasAlternateTab = new RelayCommand(CommandSearchCasAlternateTabExecute);
            CommandDeleteAlternateCas = new RelayCommand(CommandDeleteAlternateCasExecute, CommandDeleteAlternateCasCanExecute);
            if (flag)
            {
                MessengerInstance.Register<PropertyChangedMessage<Map_AlternateCas_ViewModel>>(this, NotifyMe);
                MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Map_AlternateCas_ViewModel), CommandButtonExecuted);
                MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(AlternateCas_VM), RefreshGrid);                
            }
            NotifyPropertyChanged("IsSubstanceIdentityView");
        }

        private bool CommandDeleteAlternateCasCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ActionComment) && !string.IsNullOrWhiteSpace(ActionComment))
            {
                return true;
            }
            else {
                return false;
            }
        }

        private void CommandDeleteAlternateCasExecute(object obj)
        {
            bool resultDel = _objGeneric_Service.DeleteMap_AlternateCas(SelectedMapAlternateCas.AlternateID, Engine.CurrentUserSessionID, "");
            if (resultDel)
            {
                IsShowPopUp = false;
                BindListAlternateCas();
                ClearPopUpVariable();
                _notifier.ShowSuccess("Selected Alternate Cas deleted successfully");
            }
            else
            {
                _notifier.ShowError("Selected Alternate Cas not deleted due to internal server error!");
            }
        }

        
        private void CommandSearchCasAlternateTabExecute(object obj)
        {
            BindListAlternateCas();
        }

        public void BindListSearchTypeFilterQSID()
        {
            var srchTypeList = new List<SelectListItem>();
            srchTypeList.Add(new SelectListItem { Text = "Cas" });
            srchTypeList.Add(new SelectListItem { Text = "Cas ID" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid ID" });
            listItemSearchTypeForGeneric = new ObservableCollection<SelectListItem>(srchTypeList);
            selectedSearch_AlternateCas = listItemSearchTypeForGeneric.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchTypeForGeneric");
        }
        private void RefreshGrid(PropertyChangedMessage<string> flag)
        {
            BindListAlternateCas();
            if (IsShowPopUp)
            {
                NotifyPropertyChanged("IsShowPopUp");
            }
        }
        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapAlternateCas")
            {
                Task.Run(() =>
                {

                    AlternateCAS = SelectedMapAlternateCas.AlternateCAS;
                    NotifyPropertyChanged("AlternateCAS");
                    PrimaryCAS = SelectedMapAlternateCas.PrimaryCAS;
                    NotifyPropertyChanged("PrimaryCAS");
                    PopUpContentAddAlternateVisibility = Visibility.Collapsed;
                    NotifyPropertyChanged("PopUpContentAddAlternateVisibility");
                    IsShowPopUp = true;
                    PopUpContentDeleteAlternateVisibility = Visibility.Visible;
                    NotifyPropertyChanged("PopUpContentDeleteAlternateVisibility");
                });
            }
            else {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }
        }

        private void NotifyMe(PropertyChangedMessage<Map_AlternateCas_ViewModel> obj)
        {
            SelectedMapAlternateCas = obj.NewValue;
            if (SelectedMapAlternateCas != null)
            {
                objCommentDC_AlternateCas = new CommentsUC_VM(new GenericComment_VM(SelectedMapAlternateCas.PrimaryCAS, SelectedMapAlternateCas.AlternateCAS, SelectedMapAlternateCas.PrimaryCASID, SelectedMapAlternateCas.AlternateCASID, 370, true, "Primary-Alternate CAS relationship"));
                NotifyPropertyChanged("objCommentDC_AlternateCas");
            }
        }

        private bool CommandAddAlternateCasCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(AlternateCAS) && !string.IsNullOrEmpty(PrimaryCAS))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandAddMoreAlternateCasExecute(object obj)
        {
            SaveAlternateCas();
        }

        private void CommandAddAlternateCasExecute(object obj)
        {
            SaveAlternateCas();
            IsShowPopUp = false;
            ClearPopUpVariable();
            PopUpContentAddAlternateVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddAlternateVisibility");
        }

        private void CommandShowPopUpAddAlternateCasExecute(object obj)
        {

            ClearPopUpVariable();
            if (!string.IsNullOrEmpty(CasSearchSubstanceIdentity))
            {
                PrimaryCAS = CasSearchSubstanceIdentity;
                NotifyPropertyChanged("PrimaryCAS");
            }
            IsShowPopUp = true;
            PopUpContentAddAlternateVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddAlternateVisibility");
        }
        public void SaveAlternateCas()
        {
            Map_AlternateCas_VM objAlternateCas_VM = new Map_AlternateCas_VM();
            objAlternateCas_VM.AlternateCAS = AlternateCAS;
            objAlternateCas_VM.PrimaryCAS = PrimaryCAS;
            objAlternateCas_VM.SessionID = Engine.CurrentUserSessionID;
            objAlternateCas_VM.UserName = Environment.UserName;
            objAlternateCas_VM.Comments = ActionComment;

            if (_objLibraryFunction_Service.CheckLogAlternateCasForDeleted(AlternateCAS, PrimaryCAS))
            {
                ConfirmDialog_UC objDialog = new ConfirmDialog_UC("This relationship was previously deleted, please confirm if you want to proceed with re-adding this relationship?");
                objDialog.ShowDialog();
                if (!objDialog.IsAccepted)
                {
                    return;
                }
            }

            var response = _objGeneric_Service.AddNewMap_AlternateCas(objAlternateCas_VM);
            if (response.Type == "Error")
            {
                _notifier.ShowError("Error: " + response.Description);
            }
            else if (response.Type == "Warning")
            {
                _notifier.ShowWarning("Warning: " + response.Description);
            }
            else
            {
                if (!IsEdit)
                { _notifier.ShowSuccess("Existing Alternate CAS Saved Successfully."); }
                else
                {
                    _notifier.ShowSuccess("New Alternate CAS added successfully.");
                }
                ClearPopUpVariable();
                BindListAlternateCas();
            }
        }
        public void ClearPopUpVariable()
        {
            AlternateCAS = string.Empty;
            PrimaryCAS = string.Empty;
            ActionComment = string.Empty;
            NotifyPropertyChanged("ActionComment");
            NotifyPropertyChanged("AlternateCAS");
            NotifyPropertyChanged("PrimaryCAS");
        }

        public void BindListAlternateCas()
        {
            VisibilityListDetail_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDetail_Loader");
            Task.Run(() =>
            {
                List<Map_AlternateCas_VM> listalternateCas_DB = new List<Map_AlternateCas_VM>();
                if (CasSearchSubstanceIdentity == null)
                {
                    if (string.IsNullOrEmpty(searchCas_AlternateCas))
                    {
                        listalternateCas_DB = _objGeneric_Service.GetListAllMapAlternateCas();
                    }
                    else
                    {
                        searchCas_AlternateCas = searchCas_AlternateCas.Replace("-", "");
                        listalternateCas_DB = _objGeneric_Service.GetListAllMapAlternateCasSearch(searchCas_AlternateCas, selectedSearch_AlternateCas.Text);
                    }
                }
                else
                {
                    // your code here 
                    listalternateCas_DB = _objGeneric_Service.GetListAllMapAlternateCasSingleCAS(CasSearchSubstanceIdentity);
                }

                ListAlternateCas_VM = listalternateCas_DB.GroupBy(x => new { x.AlternateCAS, x.AlternateCASID, x.AlternateID, x.Comments, x.PrimaryCAS, x.PrimaryCASID, x.SessionID, x.Status, x.UserName }).Select(x => new Map_AlternateCas_ViewModel
                {
                    Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => new HyperLinkDataValue() { DisplayValue = y.Qsid + " ( " + y.ListFieldName + " )", NavigateData = y.Qsid_Id }).Distinct().ToList(),
                    QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Count(),
                    QsidFilter = String.Join(",", x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => y.Qsid).ToArray()),
                    AlternateCAS = x.Key.AlternateCAS,
                    AlternateCASID = x.Key.AlternateCASID,
                    AlternateID = x.Key.AlternateID,
                    Comments = x.Key.Comments,
                    PrimaryCAS = x.Key.PrimaryCAS,
                    PrimaryCASID = x.Key.PrimaryCASID,
                    SessionID = x.Key.SessionID,
                    Status = x.Key.Status,
                    TimeStamp = x.Select(y => y.TimeStamp).FirstOrDefault(),
                    UserName = !string.IsNullOrEmpty(x.Key.UserName) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.Key.UserName.ToLower().Replace("i", ""))) && Engine._listCheckedOutUser != null ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.Key.UserName.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                }).ToList();

                VisibilityListDetail_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityListDetail_Loader");
            });
        }
        private List<CommonDataGridColumn> GetListGridColumn()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Alternate CAS ID", ColBindingName = "AlternateCASID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Alternate CAS", ColBindingName = "AlternateCAS", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Primary CAS ID", ColBindingName = "PrimaryCASID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Primary CAS", ColBindingName = "PrimaryCAS", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "Comments", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested By", ColBindingName = "UserName", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Added", ColBindingName = "TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "AlternateID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapAlternateCas" });
            return listColumnQsidDetail;
        }

        private void CommandClosePopUpExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
    }
}
