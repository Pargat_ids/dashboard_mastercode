﻿using EntityClass;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VersionControlSystem.Model.ViewModel;
using ToastNotifications.Messages;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;

namespace GenericManagementTool.ViewModels
{
    public class UpdateGenericCriteria_VM : Base_ViewModel
    {
        public ICommand CommandDeleteCriteria { get; set; }
        public ICommand CommandAddCriteria { get; set; }
        public ICommand CommandShowCriteriaByCas { get; set; }
        public ICommand CommandSaveCriteria { get; set; }
        public ICommand CommandCancelCriteria { get; set; }
        public ICommand CommandGenericCalculatedImpact { get; set; }
        public ICommand CommandClosePopup { get; set; }
        private bool _isSelectAddEditCalGen { get; set; }
        public bool IsSelectAddEditCalGen
        {
            get { return _isSelectAddEditCalGen; }
            set
            {
                _isSelectAddEditCalGen = value;
                if (_isSelectAddEditCalGen)
                {
                    NotifyPropertyChanged("IsShowPopUp");
                }
                NotifyPropertyChanged("IsSelectAddEditCalGen");
            }
        }
        private bool _isSelectListExistCalGen { get; set; }
        public bool IsSelectListExistCalGen
        {
            get { return _isSelectListExistCalGen; }
            set
            {
                _isSelectListExistCalGen = value;
                if (_isSelectListExistCalGen)
                {
                    BindListGenericsCalculated();
                }
                NotifyPropertyChanged("IsSelectListExistCalGen");
            }
        }

        public Visibility visibilityGenercCriteriaList { get; set; } = Visibility.Collapsed;
        public Visibility genCalculatedLoaderVisibility { get; set; } = Visibility.Collapsed;
        public string _searchCas { get; set; }
        private int CasID { get; set; }
        public string SearchCas
        {
            get { return _searchCas; }
            set
            {
                _searchCas = value;
                NotifyPropertyChanged("SearchCas");
            }
        }
        public CommonDataGrid_ViewModel<MapGenericsCalculated_ViewModel> commonDG_GenCalculated_VM { get; set; }
        private List<MapGenericsCalculated_ViewModel> _listGenCalculated { get; set; }
        public List<MapGenericsCalculated_ViewModel> listGenCalculated
        {
            get { return _listGenCalculated; }
            set
            {
                _listGenCalculated = value;
                MessengerInstance.Send<PropertyChangedMessage<List<MapGenericsCalculated_ViewModel>>>(new PropertyChangedMessage<List<MapGenericsCalculated_ViewModel>>(null, _listGenCalculated, "Default List"));
            }
        }

        public MapGenericsCalculated_ViewModel SelectedGenCalculated { get; set; }
        public ObservableCollection<GenericCriteria_VM> listGenericCriteria { get; set; } = new ObservableCollection<GenericCriteria_VM>();
        public List<EntityClass.Library_QSIDs> listQsids { get; set; }
        public ObservableCollection<Library_FieldNames> listFieldNames { get; set; }
        public ObservableCollection<SelectListItem> listCriteria { get; set; }
        public ObservableCollection<SelectListItem> listAdditionalCheck { get; set; }
        public ObservableCollection<SelectListItem> listCondition { get; set; }
        private List<Map_GenericCalculatedDetail_NewAdd> _listNewAddChildren { get; set; }
        private List<Map_GenericCalculatedDetail_Delete> _listDeleteChildren { get; set; }
        private List<Map_GenericCalculatedDetail_Existing> _listExistingChildren { get; set; }
        public List<Map_GenericCalculatedDetail_NewAdd> listNewAddChildren
        {
            get { return _listNewAddChildren; }
            set
            {
                _listNewAddChildren = value;
                MessengerInstance.Send<PropertyChangedMessage<List<Map_GenericCalculatedDetail_NewAdd>>>(new PropertyChangedMessage<List<Map_GenericCalculatedDetail_NewAdd>>(null, _listNewAddChildren, "Default List"));
                NotifyPropertyChanged("listNewAddChildren");
            }
        }
        public List<Map_GenericCalculatedDetail_Delete> listDeleteChildren
        {
            get { return _listDeleteChildren; }
            set
            {
                _listDeleteChildren = value;
                MessengerInstance.Send<PropertyChangedMessage<List<Map_GenericCalculatedDetail_Delete>>>(new PropertyChangedMessage<List<Map_GenericCalculatedDetail_Delete>>(null, _listDeleteChildren, "Default List"));
                NotifyPropertyChanged("listDeleteChildren");
            }
        }
        public List<Map_GenericCalculatedDetail_Existing> listExistingChildren
        {
            get { return _listExistingChildren; }
            set
            {
                _listExistingChildren = value;
                MessengerInstance.Send<PropertyChangedMessage<List<Map_GenericCalculatedDetail_Existing>>>(new PropertyChangedMessage<List<Map_GenericCalculatedDetail_Existing>>(null, _listExistingChildren, "Default List"));
                NotifyPropertyChanged("listExistingChildren");
            }
        }

        public CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_NewAdd> CommonDGNewAddChildren { get; set; }
        public CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_Delete> CommonDGDeleteChildren { get; set; }
        public CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_Existing> CommonDGExistingChildren { get; set; }
        public bool IsShowPopUp { get; set; } = false;
        public string NotificationCalGenResult { get; set; }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public UpdateGenericCriteria_VM()
        {
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Generics_VM), NotifyMe);
            commonDG_GenCalculated_VM = new CommonDataGrid_ViewModel<MapGenericsCalculated_ViewModel>(BindColumnForGrid(), "", "Generics Calculated");
            CommonDGNewAddChildren = new CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_NewAdd>(BindColumnForGenericCalculate(), "Cas", "");
            CommonDGDeleteChildren = new CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_Delete>(BindColumnForGenericCalculate(), "Cas", "");
            CommonDGExistingChildren = new CommonDataGrid_ViewModel<Map_GenericCalculatedDetail_Existing>(BindColumnForGenericCalculate(), "Cas", "");
            CommandDeleteCriteria = new RelayCommand(CommandDeleteCriteriaExecute);
            CommandAddCriteria = new RelayCommand(CommandAddCriteriaExecute);
            CommandShowCriteriaByCas = new RelayCommand(CommandShowCriteriaByCasExecute, CommandShowCriteriaByCasCanExecute);
            CommandSaveCriteria = new RelayCommand(CommandSaveCriteriaExecute);
            CommandCancelCriteria = new RelayCommand(CommandCancelCriteriaExecute);
            CommandGenericCalculatedImpact = new RelayCommand(CommandGenericCalculatedImpactExecute);
            CommandClosePopup = new RelayCommand(CommandClosePopupExecute);
            MessengerInstance.Register<PropertyChangedMessage<MapGenericsCalculated_ViewModel>>(this, NotifyMe);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapGenericsCalculated_ViewModel), CommandButtonExecuted);
            BindAllLibraryQsid();
            BindCriteriaCombo();
            BindConditionCombo();
            BindAdditionalCheck();
            BindListGenericsCalculated();
            MessengerInstance.Register<NotificationMessageAction<MapGenericsCalculated_ViewModel>>(this, CallBackBindListDetailGridExecute);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(SubstanceSearchTab_VM), CommandButtonExecuted);
        }

        private void CallBackBindListDetailGridExecute(NotificationMessageAction<MapGenericsCalculated_ViewModel> obj)
        {
            BindListGenericsCalculated();
        }

        private void NotifyMe(PropertyChangedMessage<string> obj)
        {
            NotifyPropertyChanged("IsShowPopUp");
        }

        private bool CriteriaCanAddNewItem()
        {
            bool retFlag = true;
            if (listGenericCriteria.Any(x => x.IsUniqueCas == false && (x.Qsid_ID == 0 || x.FieldNameID == 0 || string.IsNullOrEmpty(x.CriteriaType) || string.IsNullOrEmpty(x.CriteriaValue) || string.IsNullOrEmpty(x.NextCondition) || x.GroupCriteriaNo == null || x.GroupCriteriaNo == 0)))
            {
                retFlag = false;
            }
            if (listGenericCriteria.Any(x => x.IsUniqueCas == true && x.Qsid_ID == 0))
            {
                retFlag = false;
            }
            return retFlag;

        }

        private List<CommonDataGridColumn> BindColumnForGenericCalculate()
        {
            List<CommonDataGridColumn> listGenCalculated = new List<CommonDataGridColumn>();
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "Cas", ColType = "Textbox" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChildName", ColType = "Textbox", ColumnWidth = "250" });
            return listGenCalculated;
        }

        private void CommandClosePopupExecute(object obj)
        {
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }



        private bool ValidateExistingCriteriaItem()
        {
            bool retFlag = true;
            if (listGenericCriteria.Any(x => x.IsUniqueCas == false && (x.Qsid_ID == 0 || x.FieldNameID == 0 || string.IsNullOrEmpty(x.CriteriaType) || string.IsNullOrEmpty(x.CriteriaValue))))
            {
                retFlag = false;
            }
            if (listGenericCriteria.Any(x => x.IsUniqueCas == true && x.Qsid_ID == 0))
            {
                retFlag = false;
            }
            return retFlag;

        }

        private void CommandGenericCalculatedImpactExecute(object obj)
        {
            if (!ValidateExistingCriteriaItem())
            {
                _notifier.ShowError("Please fill all criteria correctly!");
                return;
            }

            var listCalculatedLogic = MapGenericCalculatedLogicToBusinessVM();
            var newListGeneric = _objLibraryFunction_Service.GetCalculatedGenericsQueryByCas(listCalculatedLogic);
            var existingListGen = _objLibraryFunction_Service.GetExistingGenericNestedCas(CasID);
            listNewAddChildren = newListGeneric.Where(x => !existingListGen.Any(y => x.CasID == y.CasID)).Select(x => new Map_GenericCalculatedDetail_NewAdd
            {
                Cas = x.Cas,
                CasID = x.CasID,
                ChildName = x.ChildName
            }).ToList();
            listDeleteChildren = existingListGen.Where(x => !newListGeneric.Any(y => x.CasID == y.CasID)).Select(x => new Map_GenericCalculatedDetail_Delete
            {
                Cas = x.Cas,
                CasID = x.CasID,
                ChildName = x.ChildName
            }).ToList();
            listExistingChildren = newListGeneric.Where(x => existingListGen.Any(y => x.CasID == y.CasID)).Select(x => new Map_GenericCalculatedDetail_Existing
            {
                Cas = x.Cas,
                CasID = x.CasID,
                ChildName = x.ChildName
            }).ToList();
            NotifyPropertyChanged("listNewAddChildren");
            NotifyPropertyChanged("listDeleteChildren");
            NotifyPropertyChanged("listExistingChildren");
            IsShowPopUp = true;
            NotifyPropertyChanged("IsShowPopUp");

        }

        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "ShowLogicGeneric")
            {
                SearchCas = SelectedGenCalculated.Cas;
                CommandShowCriteriaByCas.Execute(null);
                IsSelectAddEditCalGen = true;
                NotifyPropertyChanged("IsSelectAddEditCalGen");
            }
            else if (obj.Notification == "ShowCalculateImpact")
            {
                SearchCas = SelectedGenCalculated.Cas;
                CommandShowCriteriaByCas.Execute(null);
                IsSelectAddEditCalGen = true;
                NotifyPropertyChanged("IsSelectAddEditCalGen");
                CommandGenericCalculatedImpact.Execute(null);
            }
            else if (obj.Notification == "CommitLatestCalculatedGenerics")
            {
                Task.Run(() =>
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowInformation("Process Calculated Generics Nested Started");
                        if (_objLibraryFunction_Service.AddCalculatedGenericAftterSaveCriteria((int)SelectedGenCalculated.CasID, Environment.UserName))
                        {
                            _notifier.ShowSuccess("Process Calculated Generics Nested Completed");
                        }
                        else
                        {
                            _notifier.ShowError("Internal server issue in Process Calculated Generics Nested");
                        }
                    });

                });
            }
            else
            {
                int qsid_Id = 0; ;
                int.TryParse(obj.Notification, out qsid_Id);

                if (qsid_Id != 0)
                {
                    Task.Run(() => FetchQsidDetail(qsid_Id));
                }
            }
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
        private void NotifyMe(PropertyChangedMessage<MapGenericsCalculated_ViewModel> obj)
        {
            SelectedGenCalculated = obj.NewValue;
        }

        public void BindListGenericsCalculated()
        {
            Task.Run(() =>
            {
                listGenCalculated = _objGeneric_Service.GetListDetailGenericsCalculated_ViewModel();
            });
        }
        private List<CommonDataGridColumn> BindColumnForGrid()
        {
            List<CommonDataGridColumn> listGenCalculated = new List<CommonDataGridColumn>();
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColFilterMemberPath = "QsidFilter" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "Cas", ColType = "Textbox" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox", ColumnWidth = "250" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Detail", ColBindingName = "CasID", ColType = "Button", ContentName = "View Existing Logic", BackgroundColor = "#ffc93c", CommandParam = "ShowLogicGeneric" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Detail", ColBindingName = "CasID", ColType = "Button", ContentName = "Calculate Changes", BackgroundColor = "#30475e", CommandParam = "ShowCalculateImpact" });
            listGenCalculated.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Commit", ColBindingName = "CasID", ColType = "Button", ContentName = "Commit Changes", BackgroundColor = "#79d70f", CommandParam = "CommitLatestCalculatedGenerics" });
            return listGenCalculated;
        }

        private void CommandCancelCriteriaExecute(object obj)
        {
            SearchCas = string.Empty;
            visibilityGenercCriteriaList = Visibility.Collapsed;
            NotifyPropertyChanged("visibilityGenercCriteriaList");
            NotificationCalGenResult = "";
            NotifyPropertyChanged("NotificationCalGenResult");
        }

        private void CommandSaveCriteriaExecute(object obj)
        {
            if (!ValidateExistingCriteriaItem())
            {
                _notifier.ShowError("Please fill all criteria correctly!");
                return;
            }
            var listMapGenericCal = MapGenericCalculatedLogicToBusinessVM();
            _objLibraryFunction_Service.InsertListMap_Generic_CalculatedCritera(listMapGenericCal, CasID, Engine.CurrentUserSessionID);
            _notifier.ShowSuccess("Map Generics calculate saved successfully.");
            Task.Run(() =>
            {
                _notifier.ShowInformation("Process Calculated Generics Nested Started");
                if (_objLibraryFunction_Service.AddCalculatedGenericAftterSaveCriteria(CasID, Environment.UserName))
                {
                    _notifier.ShowSuccess("Process Calculated Generics Nested Completed");
                }
                else
                {
                    _notifier.ShowError("Internal server issue in Process Calculated Generics Nested");
                }

            });

        }

        public List<Map_Generics_Calculated> MapGenericCalculatedLogicToBusinessVM()
        {
            var listAddCriteria = listGenericCriteria.Where(x => x.Qsid_ID != 0).ToList();
            List<Map_Generics_Calculated> listMapGenericCal = listAddCriteria.Select(x => new Map_Generics_Calculated()
            {
                Qsid_ID = x.Qsid_ID,
                CasID = CasID,
                IsSelectUniqueCas = x.IsUniqueCas,
                FieldNameID = x.FieldNameID,
                CriteriaType = x.CriteriaType,
                CriteriaValue = x.CriteriaValue,
                GroupingCriteriaNumber = x.GroupCriteriaNo,
                NextCondition = x.NextCondition
            }).OrderBy(x => x.GroupingCriteriaNumber).ToList();
            return listMapGenericCal;
        }


        private bool CommandShowCriteriaByCasCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(SearchCas))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandShowCriteriaByCasExecute(object obj)
        {
            var libCas = _objLibraryFunction_Service.GetLibraryCasByCas(SearchCas);
            if (libCas != null)
            {
                CasID = libCas.CASID;
                visibilityGenercCriteriaList = Visibility.Visible;
                NotifyPropertyChanged("visibilityGenercCriteriaList");
                BindExistingCriteria();
                NotificationCalGenResult = "Calculated Generics For Cas: " + SearchCas;
                NotifyPropertyChanged("NotificationCalGenResult");
            }
            else
            {

            }
        }

        private void CommandAddCriteriaExecute(object obj)
        {
            AddNewItemInCriteriaList();
        }

        private void CommandDeleteCriteriaExecute(object obj)
        {
            int crietr = (int)obj;
            DeleteItemInCriteriaList(crietr);
        }


        public void BindAllLibraryQsid()
        {
            listQsids = _objLibraryFunction_Service.GetLibrary_QsidList();
            NotifyPropertyChanged("listQsids");
        }
        public void BindCriteriaCombo()
        {
            var listCrit = new List<SelectListItem>();
            listCrit.Add(new SelectListItem() { Text = "Equal" });
            listCrit.Add(new SelectListItem() { Text = "Contains" });
            listCrit.Add(new SelectListItem() { Text = "Begins with" });
            listCrit.Add(new SelectListItem() { Text = "Ends with" });
            listCriteria = new ObservableCollection<SelectListItem>(listCrit);
            NotifyPropertyChanged("listCriteria");
        }
        public void BindConditionCombo()
        {
            var listCond = new List<SelectListItem>();
            listCond.Add(new SelectListItem() { Text = "And" });
            listCond.Add(new SelectListItem() { Text = "Or" });
            listCondition = new ObservableCollection<SelectListItem>(listCond);
            NotifyPropertyChanged("listCondition");
        }
        public void BindAdditionalCheck()
        {
            var listAddCheck = new List<SelectListItem>();
            listAddCheck.Add(new SelectListItem() { Text = "Greater than" });
            listAddCheck.Add(new SelectListItem() { Text = "Greater than equal to" });
            listAddCheck.Add(new SelectListItem() { Text = "Less than" });
            listAddCheck.Add(new SelectListItem() { Text = "Less than equal to" });
            listAdditionalCheck = new ObservableCollection<SelectListItem>(listAddCheck);
            NotifyPropertyChanged("listAdditionalCheck");
        }
        public void AddNewItemInCriteriaList()
        {
            if (listGenericCriteria.Count > 0)
            {
                var lastCriteria = listGenericCriteria.Last();
                if (lastCriteria.SelectedQsid == null && lastCriteria.Qsid_ID == 0)
                {
                    _notifier.ShowWarning("Fill last Item first!");
                    return;
                }
                else
                {
                    if (!CriteriaCanAddNewItem())
                    {
                        _notifier.ShowWarning("Fill last Item first!");
                        return;
                    }
                }
            }
            int criteriaNo = listGenericCriteria.Count + 1;
            listGenericCriteria.Add(new GenericCriteria_VM()
            {
                CriteriaNumber = criteriaNo,
            });
            listGenericCriteria = new ObservableCollection<GenericCriteria_VM>(listGenericCriteria);
            NotifyPropertyChanged("listGenericCriteria");

        }
        public void DeleteItemInCriteriaList(int criteriaNo)
        {
            var delCri = listGenericCriteria.Where(x => x.CriteriaNumber == criteriaNo).FirstOrDefault();
            listGenericCriteria.Remove(delCri);
            int startCriteriaNo = 0;
            listGenericCriteria.ToList().ForEach(x => { startCriteriaNo += 1; x.CriteriaNumber = startCriteriaNo; });
            listGenericCriteria = new ObservableCollection<GenericCriteria_VM>(listGenericCriteria);
            NotifyPropertyChanged("listGenericCriteria");

        }
        public void BindExistingCriteria()
        {
            if (CasID > 0)
            {
                var listExisting = _objLibraryFunction_Service.GetList_Map_Generic_CalculatedCritera(CasID);
                if (listExisting.Count > 0)
                {
                    listGenericCriteria = new ObservableCollection<GenericCriteria_VM>(Map_GenericCalculate_ViemModel(listExisting));
                }
                else
                {
                    listGenericCriteria = new ObservableCollection<GenericCriteria_VM>();
                    AddNewItemInCriteriaList();
                }
                NotifyPropertyChanged("listGenericCriteria");
            }
        }
        public List<GenericCriteria_VM> Map_GenericCalculate_ViemModel(List<Map_Generics_Calculated> listExistingCal)
        {
            List<GenericCriteria_VM> listGenCriteria = new List<GenericCriteria_VM>();
            int criteriaNo = 0;
            foreach (var item in listExistingCal)
            {
                criteriaNo = criteriaNo + 1;
                GenericCriteria_VM objGenericCri = new GenericCriteria_VM();
                objGenericCri.SelectedQsid = listQsids.Where(x => x.QSID_ID == item.Qsid_ID).FirstOrDefault();
                objGenericCri.IsUniqueCas = item.IsSelectUniqueCas;
                objGenericCri.SelectFieldName = objGenericCri.listFieldNames.Where(x => x.FieldNameID == item.FieldNameID).FirstOrDefault();
                if (!string.IsNullOrEmpty(item.CriteriaType))
                {
                    if (item.CriteriaType.Contains("than"))
                    {
                        objGenericCri.SelectedAdditional_Generics = listAdditionalCheck.Where(x => x.Text == item.CriteriaType).FirstOrDefault();
                    }
                    else
                    {
                        objGenericCri.SelectedCriteria_Generics = listCriteria.Where(x => x.Text == item.CriteriaType).FirstOrDefault();
                    }
                }

                objGenericCri.SelectedNextCondition_Generics = listCondition.Where(x => x.Text == item.NextCondition).FirstOrDefault();
                objGenericCri.CriteriaValue = item.CriteriaValue;
                objGenericCri.GroupCriteriaNo = item.GroupingCriteriaNumber;
                objGenericCri.CriteriaNumber = criteriaNo;
                listGenCriteria.Add(objGenericCri);
            }
            return listGenCriteria;
        }


    }
}
