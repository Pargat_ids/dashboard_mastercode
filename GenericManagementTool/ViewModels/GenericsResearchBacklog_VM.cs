﻿using CommonGenericUIView.ViewModel;
using CRA_DataAccess.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using GenericManagementTool.CustomUserControl;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace GenericManagementTool.ViewModels
{
    public partial class Generics_VM : Base_ViewModel
    {
        public bool IsShowPopUpGenericRes { get; set; }
        public GenericResearchComment_VM SelectedMapGenericResearchComment_VM { get; set; }
        public CommonDataGrid_ViewModel<GenericResearchComment_VM> commonDG_GenericResearchComment_VM { get; set; }
        public List<GenericResearchComment_VM> _listGenericResearchComment { get; set; }
        public List<GenericResearchComment_VM> ListGenericResearchComment
        {
            get { return _listGenericResearchComment; }
            set
            {
                if (_listGenericResearchComment != value)
                {
                    _listGenericResearchComment = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericResearchComment_VM>>>(new PropertyChangedMessage<List<GenericResearchComment_VM>>(null, _listGenericResearchComment, "Default List"));
                }
            }
        }
        public string TitleCommentHeaderGenResearch { get; set; }
        public Visibility GenResearchGridLoaderVisibility { get; set; } = Visibility.Collapsed;
        public ICommand CommandAddAdditionalComment_Research { get; set; }
        public ICommand CommandDisableResearchInBackLog { get; set; }


        private List<CommonDataGridColumn> GetListGridColumn_ResearchComment()
        {
            List<CommonDataGridColumn> listColumnResearchComment = new List<CommonDataGridColumn>();
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasId", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasId", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Assigned To", ColBindingName = "AssignedUserName", ColType = "Combobox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "UserName", ColBindingName = "UserName", ColType = "Combobox" });            
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Additional Research Detail", ColBindingName = "AdditionalResearchDeadline", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Timestamp", ColBindingName = "Timestamp", ColType = "Textbox" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Disable", ColBindingName = "CasId", ColType = "Button", ColumnWidth = "600", ContentName = "Disable Need More Research Flag", BackgroundColor = "#FF5151", CommandParam = "DeleteGenResearch" });
            listColumnResearchComment.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "CasId", ColType = "Button", ColumnWidth = "600", ContentName = "View/Assign", BackgroundColor = "#85C88A", CommandParam = "ViewAssignUser" });

            return listColumnResearchComment;
        }

        public void BindGenericResearchComment()
        {
            GenResearchGridLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("GenResearchGridLoaderVisibility");
            Task.Run(() =>
            {
                ListGenericResearchComment = _objLibraryFunction_Service.GetAllAdditionalResearch().Select(x => new GenericResearchComment_VM
                {
                    CommentID = x.CommentID,
                    AdditionalResearchDeadline = x.AdditionalResearchDeadline,
                    Type = x.ActionType,
                    ParentCas = x.ParentCas,
                    ParentName= x.ParentName,
                    ParentCasId = x.ParentCasId,
                    ChildCas = x.ChildCas,
                    ChildCasId = x.ChildCasId,
                    Timestamp = Convert.ToDateTime(x.Timestamp),
                    Comments = x.Comments,
                    UserName = GetSelectedUserNameFromCombo(x.UserID),
                    AssignedUserName = GetSelectedUserNameFromCombo(x.AssignedUserId),
                }).ToList();
                GenResearchGridLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("GenResearchGridLoaderVisibility");
            });
        }
        public ListCommonComboBox_ViewModel GetSelectedUserNameFromCombo(string userID)
        {
            var objUser = new ListCommonComboBox_ViewModel();
            if (!string.IsNullOrEmpty(userID))
            {
                objUser = Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(userID.ToLower().Replace("i", ""))).FirstOrDefault();
            }
            return objUser;
        }
        private void NotifyMe(PropertyChangedMessage<GenericResearchComment_VM> obj)
        {

            SelectedMapGenericResearchComment_VM = obj.NewValue;
            NotifyPropertyChanged("SelectedMapGenericResearchComment_VM");
            if (SelectedMapGenericResearchComment_VM != null)
            {
                objCommentDC_GenericsBacklog = new CommentsUC_VM(new GenericComment_VM(SelectedMapGenericResearchComment_VM.ParentCas, SelectedMapGenericResearchComment_VM.ChildCas, SelectedMapGenericResearchComment_VM.ParentCasId, SelectedMapGenericResearchComment_VM.ChildCasId, 370, true, "Generics Parent-Child"));
                NotifyPropertyChanged("objCommentDC_GenericsBacklog");
            }
            //BindListItemParentComments_Research();

        }
        private void CallBackBindListDetailGridExecute(NotificationMessageAction<GenericResearchComment_VM> notificationMessageAction)
        {
            BindGenericResearchComment();
            BindListItemParentComments_Research();
        }
        public void BindListItemParentComments_Research()
        {
            if (SelectedMapGenericResearchComment_VM != null)
            {
                Task.Run(() =>
            {
                List<GenericSuggestionComments_VM> listParentComment = new();
                if (SelectedMapGenericResearchComment_VM.Type == "Parent")
                {
                    TitleCommentHeaderGenResearch = "Additional Research Comments Parent Cas :" + SelectedMapGenericResearchComment_VM.ParentCas;
                    listParentComment = _objGeneric_Service.GetListGenericsParentComments(SelectedMapGenericResearchComment_VM.ParentCasId);
                }
                else
                {
                    TitleCommentHeaderGenResearch = "Additional Research Comments Parent Cas :" + SelectedMapGenericResearchComment_VM.ParentCas + " And Child Cas: " + SelectedMapGenericResearchComment_VM.ChildCas;
                    listParentComment = _objGeneric_Service.GetListGenericsComments(SelectedMapGenericResearchComment_VM.ParentCasId, SelectedMapGenericResearchComment_VM.ChildCasId);
                }
                NotifyPropertyChanged("TitleCommentHeaderGenResearch");
                listItemComments_Research = new ObservableCollection<GenericSuggestionComments_VM>(listParentComment);
                NotifyPropertyChanged("listItemComments_Research");
            });
            }
        }
        public void CommandAddAdditionalComment_ResearchExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction))
            {
                MapGenericResearchComment_VM objGenericComment = new MapGenericResearchComment_VM();
                objGenericComment.ParentCasId = SelectedMapGenericResearchComment_VM.ParentCasId;
                objGenericComment.ChildCasId = SelectedMapGenericResearchComment_VM.ChildCasId;
                objGenericComment.Comments = CommentGenericSuggestionAction;
                objGenericComment.UserID = Environment.UserName;
                objGenericComment.ActionType = SelectedMapGenericResearchComment_VM.Type;
                _objLibraryFunction_Service.SaveCommentsGenericResearchBackLog(objGenericComment);
                BindListItemParentComments_Research();
                CommentGenericSuggestionAction = string.Empty;
                NotifyPropertyChanged("CommentGenericSuggestionAction");
            }
            else
            {
                _notifier.ShowError("comment mandatory to add addition comment!");
            }
        }


        private void CommandShowPopUPGenericResExecute(object obj)
        {
            ShowPopGenericRes(new GenericResearchComment_VM(), "Add");
        }
        private void CommandButtonExecuted_GenericResearchComment(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteGenResearch")
            {
                IsShowPopUpGenericRes = true;
                NotifyPropertyChanged("IsShowPopUpGenericRes");
            }
            else if (obj.Notification == "ViewAssignUser")
            {
                ShowPopGenericRes(SelectedMapGenericResearchComment_VM, "Edit");
            }
        }
        private void CommandDisableResearchInBackLogExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) && SelectedMapGenericResearchComment_VM != null)
            {
                _objLibraryFunction_Service.PutFlagDisableAdditionalResearch(SelectedMapGenericResearchComment_VM.ParentCasId, SelectedMapGenericResearchComment_VM.ChildCasId, SelectedMapGenericResearchComment_VM.Type);
                CommandAddAdditionalComment_Research.Execute(obj);
                CommentGenericSuggestionAction = string.Empty;                
                IsShowPopUpGenericRes = false;
                NotifyPropertyChanged("IsShowPopUpGenericRes");
            }
            else
            {
                _notifier.ShowError("comment mandatory to add addition comment!");
            }
        }
        public void ShowPopGenericRes(GenericResearchComment_VM _objGeneric, string action)
        {
            var objContext = new GenericResearch_WindowPopUP_VM(_objGeneric, action);
            GenericResearch_WindowPopUP objPopUp = new GenericResearch_WindowPopUP();
            objPopUp.DataContext = objContext;
            objPopUp.Show();
        }

    }
}
