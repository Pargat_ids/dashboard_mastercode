﻿using GalaSoft.MvvmLight.Messaging;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using ToastNotifications.Messages;
using CommonGenericUIView.ViewModel;
using CommonGenericUIView.CustomUserControl;

namespace GenericManagementTool.ViewModels
{
    public class Hydrate_VM : Base_ViewModel
    {
        public Visibility VisibilityListDetailDashboard_Loader { get; set; }
        public CommonDataGrid_ViewModel<Map_Hydrate_ViewModel> comonDG_VM { get; set; }
        private bool _IsShowPopUp { get; set; }
        public bool IsShowPopUp
        {
            get { return _IsShowPopUp; }
            set
            {
                if (_IsShowPopUp != value)
                {
                    _IsShowPopUp = value;
                    NotifyPropertyChanged("IsShowPopUp");
                }
            }
        }
        public Map_Hydrate_ViewModel SelectedMapHydrate { get; set; }
        private List<Map_Hydrate_ViewModel> _listHydrate_VM { get; set; } = new List<Map_Hydrate_ViewModel>();
        public List<Map_Hydrate_ViewModel> ListHydrate_VM
        {
            get { return _listHydrate_VM; }
            set
            {
                if (_listHydrate_VM != value)
                {
                    _listHydrate_VM = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Map_Hydrate_ViewModel>>>(new PropertyChangedMessage<List<Map_Hydrate_ViewModel>>(null, _listHydrate_VM, "Default List"));
                }
            }
        }

        // public List<GenericChemicalName_VM> listAllChemicalName_Backend { get; set; }
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Anhydus { get; set; }
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Hydrus { get; set; }
        private GenericChemicalName_VM _selectedChemName_Anhydrus { get; set; }
        public GenericChemicalName_VM SelectedChemName_Anhydrus
        {
            get { return _selectedChemName_Anhydrus; }
            set
            {
                if (_selectedChemName_Anhydrus != value)
                {
                    _selectedChemName_Anhydrus = value;
                    if (_selectedChemName_Anhydrus != null)
                    {
                        Anhydrus_ChemName = _selectedChemName_Anhydrus.ChemicalName;
                        Anhydrus_ChemNameID = _selectedChemName_Anhydrus.ChemicalNameID;
                        ListChemicalAnHydrusVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemicalAnHydrusVisibility");
                    }
                    NotifyPropertyChanged("SelectedChemName_Anhydrus");
                }
            }
        }
        private GenericChemicalName_VM _selectedChemName_Hydrus { get; set; }
        public GenericChemicalName_VM SelectedChemName_Hydrus
        {
            get { return _selectedChemName_Hydrus; }
            set
            {
                if (_selectedChemName_Hydrus != value)
                {
                    _selectedChemName_Hydrus = value;
                    if (_selectedChemName_Hydrus != null)
                    {
                        Hydrus_ChemName = _selectedChemName_Hydrus.ChemicalName;
                        Hydrus_ChemNameID = _selectedChemName_Hydrus.ChemicalNameID;
                        ListChemicalHydrusVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemicalHydrusVisibility");
                    }
                    NotifyPropertyChanged("SelectedChemName_Hydrus");
                }
            }
        }
        private string _anhydrus_ChemName { get; set; }
        private int? Anhydrus_ChemNameID { get; set; }
        public string Anhydrus_ChemName
        {
            get { return _anhydrus_ChemName; }
            set
            {
                if (value != _anhydrus_ChemName)
                {
                    Anhydrus_ChemNameID = 0;
                    _anhydrus_ChemName = value;
                    FilterChemicalNameForAnHydrus(_anhydrus_ChemName);
                    NotifyPropertyChanged("Anhydrus_ChemName");
                }

            }
        }
        private string _hydrus_ChemName { get; set; }
        private int? Hydrus_ChemNameID { get; set; }
        public string Hydrus_ChemName
        {
            get { return _hydrus_ChemName; }
            set
            {
                if (value != _hydrus_ChemName)
                {
                    Hydrus_ChemNameID = 0;
                    _hydrus_ChemName = value;
                    FilterChemicalNameForHydrus(_hydrus_ChemName);
                    NotifyPropertyChanged("Hydrus_ChemName");
                }

            }
        }

        public ICommand CommandShowPopUpAddHydrate { get; private set; }
        public ICommand CommandAddHydrate { get; private set; }
        public ICommand CommandDeleteHydrate { get; private set; }
        public ICommand CommandAddMoreHydrate { get; private set; }
        public ICommand CommandLostFocusAnHydrateCas { get; private set; }
        public ICommand CommandLostFocusHydrateCas { get; private set; }
        public ICommand CommandClosePopUp { get; private set; }
        public Visibility PopUpContentAddHydrateVisibility { get; set; } = Visibility.Collapsed;
        public Visibility PopUpContentDeleteHydrateVisibility { get; set; } = Visibility.Collapsed;
        public Visibility PopUpContentAddEditNameVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalHydrusVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalAnHydrusVisibility { get; set; } = Visibility.Collapsed;
        public string Hydrous_CAS { get; set; }
        public string Anhydrous_CAS { get; set; }
        public string ActionComment { get; set; }
        public bool IsEdit { get; set; } = true;
        public string CasSearchSubstanceIdentity { get; set; }
        public string searchCasHydrate { get; set; } = string.Empty;
        public ObservableCollection<SelectListItem> listItemSearchTypeForGeneric { get; set; }
        private SelectListItem _selectedSearch_Hydrate { get; set; }
        public SelectListItem selectedSearch_Hydrate
        {
            get { return _selectedSearch_Hydrate; }
            set
            {
                _selectedSearch_Hydrate = value;
                NotifyPropertyChanged("selectedSearch_Hydrate");
            }
        }
        public ICommand CommandSearchCasHydrateTab { get; set; }
        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo
        {
            get; set;
        }
        public CommentsUC_VM objCommentDC_Hydrate { get; set; } = new CommentsUC_VM(new GenericComment_VM("", "", 0, 0, 400));
        public Visibility IsSubstanceIdentityView { get; set; }
        public Hydrate_VM(string _cas = null, bool flag = false)
        {
            if (_cas != null)
            {
                CasSearchSubstanceIdentity = _cas;
                IsSubstanceIdentityView = Visibility.Collapsed;
            }
            else
            {
                CasSearchSubstanceIdentity = null;
                IsSubstanceIdentityView = Visibility.Visible;
            }
            BindListSearchTypeFilterQSID();
            comonDG_VM = new CommonDataGrid_ViewModel<Map_Hydrate_ViewModel>(GetListGridColumn(), "Anhydrous_CAS", "");
            BindListHydrate();
            CommandShowPopUpAddHydrate = new RelayCommand(CommandShowPopUpAddHydrateExecute);
            CommandAddHydrate = new RelayCommand(CommandAddHydrateExecute, CommandAddHydrateCanExecute);
            CommandDeleteHydrate = new RelayCommand(CommandDeleteHydrateExecute, CommandDeleteCanHydrateExecute);
            CommandAddMoreHydrate = new RelayCommand(CommandAddMoreHydrateExecute, CommandAddHydrateCanExecute);
            CommandLostFocusAnHydrateCas = new RelayCommand(CommandLostFocusAnHydrateCasExecute);
            CommandLostFocusHydrateCas = new RelayCommand(CommandLostFocusHydrateCasExecute);
            CommandClosePopUp = new RelayCommand(CommandClosePopUpExecute);
            CommandSearchCasHydrateTab = new RelayCommand(CommandSearchCasHydrateTabExecute);
            NotifyPropertyChanged("IsSubstanceIdentityView");
            if (flag)
            {
                MessengerInstance.Register<PropertyChangedMessage<Map_Hydrate_ViewModel>>(this, NotifyMe);
                MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Map_Hydrate_ViewModel), CommandButtonExecuted);
                MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Hydrate_VM), RefreshGrid);
            }
            // MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Map_Hydrate_ViewModel), CallbackHyperlink);
        }

        private bool CommandDeleteCanHydrateExecute(object obj)
        {
            if (SelectedMapHydrate != null && !string.IsNullOrEmpty(ActionComment) && !string.IsNullOrWhiteSpace(ActionComment))
            {
                return true;
            }
            else {
                return false;
            }
        }

        private void CommandDeleteHydrateExecute(object obj)
        {
            bool resultDel = _objGeneric_Service.DeleteMap_Hydrate(SelectedMapHydrate.HydrateID, Engine.CurrentUserSessionID, ActionComment);
            if (resultDel)
            {
                IsShowPopUp= false;
                ClearPopUpVariable();
                BindListHydrate();
                _notifier.ShowSuccess("Selected hydrate deleted successfully");
            }
            else
            {
                _notifier.ShowError("Selected hydrate not deleted due to internal server error!");
            }
        }

        private void CommandSearchCasHydrateTabExecute(object obj)
        {
            BindListHydrate();
        }

        public void BindListSearchTypeFilterQSID()
        {
            var srchTypeList = new List<SelectListItem>();
            srchTypeList.Add(new SelectListItem { Text = "Cas" });
            srchTypeList.Add(new SelectListItem { Text = "Cas ID" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid ID" });
            listItemSearchTypeForGeneric = new ObservableCollection<SelectListItem>(srchTypeList);
            selectedSearch_Hydrate = listItemSearchTypeForGeneric.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchTypeForGeneric");
        }
        private void CommandClosePopUpExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
        }
        private void CommandLostFocusHydrateCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(Hydrous_CAS))
            {
                int? ChemNameID = _objGeneric_Service.GetChildChemicalNameIDByCas(Hydrous_CAS);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();
                    if (foundRes != null)
                    {
                        SelectedChemName_Hydrus = foundRes;
                    }
                }
                else
                {
                    Hydrus_ChemName = string.Empty;
                    Hydrus_ChemNameID = 0;
                    SelectedChemName_Hydrus = null;
                }
            }
        }

        private void CommandLostFocusAnHydrateCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(Anhydrous_CAS))
            {
                int? ChemNameID = _objGeneric_Service.GetChildChemicalNameIDByCas(Anhydrous_CAS);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();
                    if (foundRes != null)
                    {
                        SelectedChemName_Anhydrus = foundRes;
                    }
                }
                else
                {
                    Anhydrus_ChemName = string.Empty;
                    Anhydrus_ChemNameID = 0;
                    SelectedChemName_Anhydrus = null;
                }
            }
        }

        private void RefreshGrid(PropertyChangedMessage<string> flag)
        {
            BindListHydrate();
            if (IsShowPopUp)
            {
                NotifyPropertyChanged("IsShowPopUp");
            }
        }

        private void FilterChemicalNameForAnHydrus(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Anhydus = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Anhydus");
                });
                ListChemicalAnHydrusVisibility = Visibility.Visible;
            }
            else
            {
                listAllChemicalName_Anhydus = new ObservableCollection<GenericChemicalName_VM>();
                ListChemicalAnHydrusVisibility = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllChemicalName_Anhydus");
            NotifyPropertyChanged("ListChemicalAnHydrusVisibility");


        }
        private void FilterChemicalNameForHydrus(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Hydrus = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Hydrus");
                });
                ListChemicalHydrusVisibility = Visibility.Visible;

            }
            else
            {
                listAllChemicalName_Hydrus = new ObservableCollection<GenericChemicalName_VM>();
                ListChemicalHydrusVisibility = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllChemicalName_Hydrus");
            NotifyPropertyChanged("ListChemicalHydrusVisibility");


        }

        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapHydrate")
            {
                Task.Run(() =>
                {

                    Anhydrous_CAS = SelectedMapHydrate.Anhydrous_CAS;
                    NotifyPropertyChanged("Anhydrous_CAS");
                    Hydrous_CAS = SelectedMapHydrate.Hydrous_CAS;
                    NotifyPropertyChanged("Hydrous_CAS");
                    PopUpContentAddHydrateVisibility = Visibility.Collapsed;
                    NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
                    IsShowPopUp = true;
                    PopUpContentDeleteHydrateVisibility = Visibility.Visible;
                    NotifyPropertyChanged("PopUpContentDeleteHydrateVisibility");
                });
            }
            else if (obj.Notification == "AddEditName")
            {
                PopUpContentDeleteHydrateVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("PopUpContentDeleteHydrateVisibility");
                IsEdit = false;
                NotifyPropertyChanged("IsEdit");
                Anhydrous_CAS = SelectedMapHydrate.Anhydrous_CAS;
                NotifyPropertyChanged("Anhydrous_CAS");
                Hydrous_CAS = SelectedMapHydrate.Hydrous_CAS;
                NotifyPropertyChanged("Hydrous_CAS");
                Anhydrus_ChemName = SelectedMapHydrate.Anhydrous_Name;
                //   Anhydrus_ChemNameID = SelectedMapHydrate.AnhydrousChemicalNameID;
                Hydrus_ChemName = SelectedMapHydrate.Hydrous_Name;
                // Hydrus_ChemNameID = SelectedMapHydrate.AnhydrousChemicalNameID;
                IsShowPopUp = true;
                PopUpContentAddHydrateVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
                ListChemicalAnHydrusVisibility = Visibility.Collapsed;
                ListChemicalHydrusVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemicalAnHydrusVisibility");
                NotifyPropertyChanged("ListChemicalHydrusVisibility");
            }
            else if (obj.Notification != "DeleteMapHydrate" && obj.Notification != "AddEditName")
            {
                int qsid_Id = Convert.ToInt32(obj.Notification);
                Task.Run(() => FetchQsidDetail(qsid_Id));
            }

        }

        private void NotifyMe(PropertyChangedMessage<Map_Hydrate_ViewModel> obj)
        {
            SelectedMapHydrate = obj.NewValue;
            if (SelectedMapHydrate != null)
            {
                objCommentDC_Hydrate = new CommentsUC_VM(new GenericComment_VM(SelectedMapHydrate.Anhydrous_CAS, SelectedMapHydrate.Hydrous_CAS, SelectedMapHydrate.Anhydrous_CAS_ID, SelectedMapHydrate.Hydrous_CAS_ID, 400, true, "Hydrate-Anhydrous relationship"));
                NotifyPropertyChanged("objCommentDC_Hydrate");
            }
        }
        private void CommandAddMoreHydrateExecute(object obj)
        {
            SaveHydrate();
        }
        private void CommandAddHydrateExecute(object obj)
        {
            SaveHydrate();
            IsShowPopUp = false;
            IsEdit = true;
            NotifyPropertyChanged("IsEdit");
            ClearPopUpVariable();
            PopUpContentAddHydrateVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
        }

        public void SaveHydrate()
        {
            Map_Hydrates_VM objHydrate_VM = new Map_Hydrates_VM();
            if (!IsEdit)
            {
                objHydrate_VM.HydrateID = SelectedMapHydrate.HydrateID;
                objHydrate_VM.Hydrous_CAS_ID = SelectedMapHydrate.Hydrous_CAS_ID;
                objHydrate_VM.Anhydrous_CAS_ID = SelectedMapHydrate.Anhydrous_CAS_ID;
            }
            objHydrate_VM.Anhydrous_CAS = Anhydrous_CAS;
            objHydrate_VM.Hydrous_CAS = Hydrous_CAS;
            objHydrate_VM.Anhydrous_Name = Anhydrus_ChemName;
            objHydrate_VM.AnhydrousChemicalNameID = Anhydrus_ChemNameID;
            objHydrate_VM.Hydrous_Name = Hydrus_ChemName;
            objHydrate_VM.HydrousChemicalNameID = Hydrus_ChemNameID;
            objHydrate_VM.SessionID = Engine.CurrentUserSessionID;
            objHydrate_VM.UserName = Environment.UserName;
            objHydrate_VM.Comments = ActionComment;
            if (_objLibraryFunction_Service.CheckLogHydrateForDeleted(Anhydrous_CAS, Hydrous_CAS))
            {
                ConfirmDialog_UC objDialog = new ConfirmDialog_UC("This relationship was previously deleted, please confirm if you want to proceed with re-adding this relationship?");
                objDialog.ShowDialog();
                if (!objDialog.IsAccepted)
                {
                    return;
                }
            }
            var response = _objGeneric_Service.AddNewMap_Hydrates(objHydrate_VM);
            if (response.Type == "Error")
            {
                _notifier.ShowError("Error: " + response.Description);
            }
            else if (response.Type == "Warning")
            {
                _notifier.ShowWarning("Warning: " + response.Description);
            }
            else
            {
                if (!IsEdit)
                { _notifier.ShowSuccess("Existing Hydrates Saved Successfully."); }
                else
                {
                    _notifier.ShowSuccess("New Hydrates Saved Successfully.");
                }
                ClearPopUpVariable();
                BindListHydrate();
            }
        }
        public void ClearPopUpVariable()
        {
            Anhydrous_CAS = string.Empty;
            Hydrous_CAS = string.Empty;
            Anhydrus_ChemName = string.Empty;
            Anhydrus_ChemNameID = 0;
            Hydrus_ChemName = string.Empty;
            Hydrus_ChemNameID = 0;
            ActionComment = string.Empty;
            NotifyPropertyChanged("ActionComment");
            NotifyPropertyChanged("Anhydrous_CAS");
            NotifyPropertyChanged("Hydrous_CAS");
            NotifyPropertyChanged("Anhydrus_ChemName");
            NotifyPropertyChanged("Hydrus_ChemName");
        }
        private bool CommandAddHydrateCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(Hydrous_CAS) || string.IsNullOrEmpty(Anhydrous_CAS))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void CommandShowPopUpAddHydrateExecute(object obj)
        {
            ClearPopUpVariable();
            if (!string.IsNullOrEmpty(CasSearchSubstanceIdentity))
            {
                Anhydrous_CAS = CasSearchSubstanceIdentity;
                NotifyPropertyChanged("Anhydrous_CAS");
            }
            IsEdit = true;
            NotifyPropertyChanged("IsEdit");
            IsShowPopUp = true;
            PopUpContentAddHydrateVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
        }

        public void BindListHydrate()
        {
            VisibilityListDetailDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
            Task.Run(() =>
            {
                List<Map_Hydrates_VM> listhydrate_DB = new List<Map_Hydrates_VM>();
                if (CasSearchSubstanceIdentity == null)
                {
                    if (string.IsNullOrEmpty(searchCasHydrate))
                    {
                        listhydrate_DB = _objGeneric_Service.GetListAllMapHydrates();
                    }
                    else
                    {
                        searchCasHydrate = searchCasHydrate.Replace("-", "");
                        listhydrate_DB = _objGeneric_Service.GetListAllMapHydratesSearch(searchCasHydrate, selectedSearch_Hydrate.Text);
                    }
                }
                else
                {
                    // your code here 
                    listhydrate_DB = _objGeneric_Service.GetListAllMapHydratesSingleCAS(CasSearchSubstanceIdentity);
                }
                ListHydrate_VM = listhydrate_DB.GroupBy(y => new { y.HydrousChemicalNameID, y.Hydrous_Name, y.Status, y.UserName, y.SessionID, y.Anhydrous_Name, y.AnhydrousChemicalNameID, y.Hydrous_CAS_ID, y.Anhydrous_CAS_ID, y.HydrateID, y.Anhydrous_CAS, y.Hydrous_CAS, y.Comments }).Select(x => new Map_Hydrate_ViewModel
                {
                    AnhydrousChemicalNameID = x.Key.AnhydrousChemicalNameID,
                    Anhydrous_CAS = x.Key.Anhydrous_CAS,
                    Anhydrous_CAS_ID = x.Key.Anhydrous_CAS_ID,
                    Anhydrous_Name = x.Key.Anhydrous_Name,
                    Comments = x.Key.Comments,
                    HydrateID = x.Key.HydrateID,
                    HydrousChemicalNameID = x.Key.HydrousChemicalNameID,
                    Hydrous_CAS = x.Key.Hydrous_CAS,
                    Hydrous_CAS_ID = x.Key.Hydrous_CAS_ID,
                    Hydrous_Name = x.Key.Hydrous_Name,
                    SessionID = x.Key.SessionID,
                    Status = x.Key.Status,
                    TimeStamp = x.Select(y => y.TimeStamp).FirstOrDefault(),
                    Qsid = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => new HyperLinkDataValue() { DisplayValue = y.Qsid + " ( " + y.ListFieldName + " )", NavigateData = y.Qsid_Id }).ToList(),
                    QsidCount = x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Distinct().Count(),
                    QsidFilter = String.Join(",", x.ToList().Where(y => !string.IsNullOrEmpty(y.Qsid)).Select(y => y.Qsid).ToList()),
                    UserName = !string.IsNullOrEmpty(x.Key.UserName) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.Key.UserName.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.Key.UserName.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                }).ToList();
                VisibilityListDetailDashboard_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
            });
        }
        private List<CommonDataGridColumn> GetListGridColumn()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();


            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Qsid", ColBindingName = "Qsid", ColType = "MultipleHyperLinkCol", ColumnWidth = "150", ColFilterMemberPath = "QsidFilter" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Anhydrous CAS ID", ColBindingName = "Anhydrous_CAS_ID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Anhydrous CAS", ColBindingName = "Anhydrous_CAS", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Anhydrous Name", ColBindingName = "Anhydrous_Name", ColType = "Textbox", ColumnWidth = "500" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Hydrous CAS ID", ColBindingName = "Hydrous_CAS_ID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Hydrate CAS", ColBindingName = "Hydrous_CAS", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Hydrate Name", ColBindingName = "Hydrous_Name", ColType = "Textbox", ColumnWidth = "500" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "Comments", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No of List", ColBindingName = "QsidCount", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested By", ColBindingName = "UserName", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Added", ColBindingName = "TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "HydrateID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapHydrate" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "HydrateID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditName" });

            return listColumnQsidDetail;
        }
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");


        }
    }
}
