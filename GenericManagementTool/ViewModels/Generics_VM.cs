﻿using DocumentFormat.OpenXml.Spreadsheet;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
//using GenericManagementTool.DataAccess;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using System.Diagnostics;
using GenericManagementTool.CustomUserControl;
using CommonGenericUIView.ViewModel;
using DataGridFilterLibrary.Support;

namespace GenericManagementTool.ViewModels
{
    public partial class Generics_VM : Base_ViewModel
    {
        public AlternateCas_VM dataContextAlternateCase { get; set; } = new AlternateCas_VM(null, true);
        public Hydrate_VM dataContextHydrate { get; set; } = new Hydrate_VM(null, true);
        public ObservableCollection<SelectListItem> listStatusCombo { get; set; }
        public List<GenericSuggestionVM> ListGenericSuggestion { get; set; } = new List<GenericSuggestionVM>();
        private bool IsInProgressBulkSuggestionAdd { get; set; } = false;
        private List<GenericSuggestion_Add_VM> _collection { get; set; }
        public List<GenericSuggestion_Add_VM> observableCollectionGenericSuggestion
        {
            get { return _collection; }
            set
            {
                if (value != _collection)
                {
                    _collection = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericSuggestion_Add_VM>>>(new PropertyChangedMessage<List<GenericSuggestion_Add_VM>>(null, _collection, "Default List"));
                }
            }
        }
        private List<GenericSuggestion_Reject_VM> _collectionRejectSuggestion { get; set; }
        public List<GenericSuggestion_Reject_VM> ListRejectGenericSuggestion
        {
            get { return _collectionRejectSuggestion; }
            set
            {
                if (value != _collectionRejectSuggestion)
                {
                    _collectionRejectSuggestion = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericSuggestion_Reject_VM>>>(new PropertyChangedMessage<List<GenericSuggestion_Reject_VM>>(null, _collectionRejectSuggestion, "Default List"));
                }
            }
        }

        private ObservableCollection<GenericSuggestion_Review_VM> _listGenericForConfirm { get; set; }
        public ObservableCollection<GenericSuggestion_Review_VM> listGenericForConfirm
        {
            get { return _listGenericForConfirm; }
            set
            {
                //if (_listGenericForConfirm != value)
                //{
                _listGenericForConfirm = value;
                MessengerInstance.Send<PropertyChangedMessage<List<GenericSuggestion_Review_VM>>>(new PropertyChangedMessage<List<GenericSuggestion_Review_VM>>(null, _listGenericForConfirm.ToList(), "Default List"));
                //}
            }
        }
        public ObservableCollection<GenericTreeView> ListGenericTree { get; set; }
        public List<GenericTreeView> ListGenericTree_Backend { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemComments { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemParentComments { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemComments_Review { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemParentComments_Review { get; set; }
        public ObservableCollection<GenericSuggestionComments_VM> listItemComments_Research { get; set; }


        public ObservableCollection<SelectListItem> ListItemNodeType { get; set; }
        public SelectListItem SelectedItemNodeType { get; set; }
        public ObservableCollection<SelectListItem> listItemSearchType { get; set; }
        public ObservableCollection<SelectListItem> listItemSearchFor { get; set; }
        public SelectListItem SelectedItemSearchType { get; set; }
        public SelectListItem SelectedItemSearchFor { get; set; }
        public List<FilterGenericSuggestion> listSearchedCas { get; set; } = new List<FilterGenericSuggestion>();
        public List<GenericSuggestionVM> ListSearchCasDetail { get; set; }

        public ObservableCollection<SelectListItem> listItemSearchType_Deleted { get; set; }
        public SelectListItem SelectItem_Deleted { get; set; }
        public string casSearch_Delete
        {
            get { return _casSearch_Delete; }
            set
            {
                if (_casSearch_Delete != value)
                {
                    _casSearch_Delete = value;
                    NotifyPropertyChanged("casSearch_Delete");
                }
            }
        }
        private string _casSearch_Delete { get; set; }

        public ICommand CommandApproveGenericSuggestion { get; private set; }
        public ICommand CommandDisApproveGenericSuggestion { get; private set; }
        public ICommand CommandSaveCommentGeneric { get; private set; }
        public ICommand CommandSaveConfirmedGenericSuggestion { get; private set; }
        public ICommand CommandUndoConfirmedGenericSuggestion { get; private set; }
        public ICommand CommandShowPopAddGeneric { get; private set; }
        public ICommand CommandShowPopRemoveGeneric { get; private set; }
        public ICommand CommandShowPopApproveGeneric { get; private set; }
        public ICommand CommandShowPopRejectGeneric { get; private set; }
        public ICommand CommandAddGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandAddMultipleGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandRemoveGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandApproveGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandRejectGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandShowPopAddParentGeneric { get; private set; }
        public ICommand CommandAddParentGenericSuggestionInTreeView { get; private set; }
        public ICommand CommandAddAdditionalComment { get; private set; }
        public ICommand CommandAddAdditionalComment_Review { get; private set; }
        public ICommand CommandTreeViewItemExpanded { get; private set; }
        public ICommand CommandSearchCasTree { get; private set; }
        public ICommand CommandFindNextFoundResult { get; private set; }
        public ICommand CommandFindPrevFoundResult { get; private set; }
        public ICommand CommandSelectedTreeViewItem { get; private set; }
        public ICommand CommandClearSearch { get; private set; }
        public ICommand CommandAddMainParent { get; private set; }
        public ICommand CommandShowPopUpAddMainParent { get; private set; }
        public ICommand CommandShowAllFilterResult { get; private set; }
        public ICommand CommandShowPopUpAddCasName { get; private set; }
        public ICommand CommandSaveAddEditCasName { get; private set; }
        public ICommand CommandIsExcludeChange { get; private set; }
        public ICommand CommandCopyTreeDataToClipboard { get; private set; }
        public ICommand CommandRefreshTreeView { get; private set; }
        public ICommand CommandLostFocusChildCas { get; private set; }
        public ICommand CommandLostFocusParentCas { get; private set; }
        //Add Generic Suggestion Tab
        public ICommand CommandShowImportBulkPopUp { get; private set; }
        public ICommand CommandShowPopUpAddGeneric { get; private set; }
        public ICommand CommandAddMultipleGenericSuggestion { get; private set; }
        public ICommand CommandSaveGenericSuggestion { get; private set; }
        public ICommand CommandClearGenericSuggestion { get; private set; }
        public ICommand CommandImportBulkSuggestion { get; private set; }
        public ICommand CommandAddGenericSuggestion { get; private set; }
        public ICommand CommandDownloadTemplateFile { get; private set; }
        public ICommand CommandSaveAddEditChemicalName { get; private set; }
        public ICommand CommandPopUpActiveList { get; private set; }
        public ICommand CommandCloseQsidInfoPopUp { get; private set; }
        public ICommand CommandOpenCurrentDataView { get; private set; }
        public ICommand CommandClosePopUp { get; private set; }
        public ICommand CommandShowNestedParent { get; private set; }
        public ICommand CommandShowSubstanceInfoPopUp { get; private set; }
        public ICommand CommandCloseSubstanceInfoPopUp { get; private set; }
        public ICommand CommandUpdateSubstanceInfo { get; private set; }
        public ICommand CommandShowCommentForMultiSelect { get; private set; }
        public ICommand CommandSaveCommentForMultiGeneric { get; private set; }

        public ICommand CommandSearchCasUniqueCircularRefTab { get; private set; }
        public ICommand CommandRunRefreshFlatExe { get; private set; }
        public ICommand CommandGoToSubstanceIdentity { get; private set; }
        public ICommand CommandMarkGroupAsClosed { get; private set; }
        public ICommand CommandRevertClosedGroup { get; private set; }

        public Visibility CircularRefGridLoaderVisibility { get; set; } = Visibility.Collapsed;
        public string ParentCas { get; set; }
        private string _childCas { get; set; } = "";
        public string ChildCas
        {
            get { return _childCas; }
            set
            {
                _childCas = value;
                NotifyPropertyChanged("ChildCas");
            }
        }
        public string ParentCasInvalid { get; set; } = string.Empty;
        public string ChildCasInvalid { get; set; } = string.Empty;
        public SelectListItem SelectedItemStatus { get; set; }
        public bool IsImportExcel { get; set; } = true;
        public bool IsImportAccess { get; set; }
        public bool IsShowPopUp { get; set; }
        private bool _isShowPopUpTreeView { get; set; }
        public bool IsShowPopUpTreeView
        {
            get { return _isShowPopUpTreeView; }
            set
            {
                _isShowPopUpTreeView = value;
                NotifyPropertyChanged("IsShowPopUpTreeView");
            }
        }

        public string SearchCas { get; set; }
        public int CurentFilterIndex { get; set; }

        public Visibility contentApproveVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentRejectVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentRemoveVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentAddVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentCommentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentAddParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentAddMainParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentAddEditCasNameVisibility { get; set; } = Visibility.Collapsed;
        public Visibility Visibility_LoaderTreeView { get; set; } = Visibility.Collapsed;
        public Visibility contentImportVisibility { get; set; } = Visibility.Collapsed;
        public Visibility GenericSuggestionGridLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility RejectGenericSuggestionGridLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ReviewSuggestionGridLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility NestedParentLoaderVisibility { get; set; } = Visibility.Collapsed;
        public bool IsShowPopUpReviewGeneric { get; set; }
        public bool IsShowPopUpDeleteGeneric { get; set; }
        public bool IsShowPopUpDisApproveGeneric { get; set; }

        //public bool IsShowPopUpGenericRes { get; set; }
        public bool IsConfirmed { get; set; }
        private string _commentGenericSuggestionAction { get; set; }
        public string CommentGenericSuggestionAction
        {
            get { return _commentGenericSuggestionAction; }
            set
            {
                _commentGenericSuggestionAction = value;
                NotifyPropertyChanged("CommentGenericSuggestionAction");
            }
        }
        private string _commentSuggestionParent { get; set; }
        public string CommentSuggestionParent
        {
            get { return _commentSuggestionParent; }
            set
            {
                _commentSuggestionParent = value;
                NotifyPropertyChanged("CommentSuggestionParent");
            }
        }
        public string CommentGenericSuggestionAction_Review { get; set; }
        public bool _CheckedSelectGenReview { get; set; }
        public List<GenericSuggestion_Review_VM> listGenericReview_Filter { get; set; }
        public string SelectedGenReview { get; set; }
        public bool IsCheckedSelectGenReview
        {
            get { return _CheckedSelectGenReview; }
            set
            {
                _CheckedSelectGenReview = value;
                if (_CheckedSelectGenReview)
                {
                    foreach (var item in listGenericReview_Filter)
                    {
                        var selectGen = listGenericForConfirm.Where(x => x.GenericSuggestion_ID == item.GenericSuggestion_ID).First();
                        selectGen.SelectedGenSuggestion = _CheckedSelectGenReview;
                    }
                    SelectedGenReview = "Selected Row: " + listGenericForConfirm.Where(x => x.SelectedGenSuggestion).Count();
                }
                else
                {
                    listGenericReview_Filter.ForEach(x => x.SelectedGenSuggestion = false);
                    SelectedGenReview = string.Empty;
                }
                NotifyPropertyChanged("SelectedGenReview");
                listGenericForConfirm = new ObservableCollection<GenericSuggestion_Review_VM>(listGenericForConfirm);
            }
        }
        public bool IsProgressMultiSelect_Enable_Review { get; set; } = true;
        public int _GenericSuggestionID { get; set; }
        public string strFoundResult { get; set; }
        public GenericSuggestionVM objGenericSuggestionTree_VM { get; set; }
        public GenericSuggestionVM SelectedTreeViewItem { get; set; }
        public ObservableCollection<SelectListItem> ListItemMatchCondition { get; set; }
        public SelectListItem SelectedItemMatchCondition { get; set; }


        public Visibility SearchResultVisibility { get; set; } = Visibility.Collapsed;

        public string TitleCommentHeader_Review { get; set; }

        //  public List<GenericChemicalName_VM> listAllChemicalName_Backend { get; set; }
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Parent { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        private string _parentName { get; set; }
        public string ParentName
        {
            get { return _parentName; }
            set
            {
                if (value != _parentName)
                {
                    ParentNameID = 0;
                    _parentName = value;
                    ListChemNameParentVisibility = Visibility.Visible;
                    NotifyPropertyChanged("ListChemNameParentVisibility");
                    FilterChemicalName_Parent(_parentName);
                    NotifyPropertyChanged("ParentName");
                }
            }
        }
        public int ParentNameID { get; set; }
        private GenericChemicalName_VM _selected_Chemical_ParentName { get; set; }
        public GenericChemicalName_VM Selected_Chemical_ParentName
        {
            get { return _selected_Chemical_ParentName; }
            set
            {
                if (_selected_Chemical_ParentName != value)
                {
                    _selected_Chemical_ParentName = value;
                    if (_selected_Chemical_ParentName != null)
                    {
                        ParentName = _selected_Chemical_ParentName.ChemicalName;
                        ParentNameID = _selected_Chemical_ParentName.ChemicalNameID;
                        ListChemNameParentVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameParentVisibility");
                    }
                    NotifyPropertyChanged("Selected_Chemical_ParentName");
                }
            }
        }
        public Visibility ListChemNameParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemNameChildVisibility { get; set; } = Visibility.Collapsed;
        public ObservableCollection<GenericChemicalName_VM> listAllChemicalName_Child { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        private string _ChildName { get; set; }
        public string ChildName
        {
            get { return _ChildName; }
            set
            {
                if (value != _ChildName)
                {
                    ChildNameID = 0;
                    _ChildName = value;
                    ListChemNameChildVisibility = Visibility.Visible;
                    NotifyPropertyChanged("ListChemNameChildVisibility");
                    FilterChemicalName_Child(_ChildName);
                    NotifyPropertyChanged("ChildName");
                }
            }
        }
        public int ChildNameID { get; set; }
        private GenericChemicalName_VM _selected_Chemical_ChildName { get; set; }
        public GenericChemicalName_VM Selected_Chemical_ChildName
        {
            get { return _selected_Chemical_ChildName; }
            set
            {
                if (_selected_Chemical_ChildName != value)
                {
                    _selected_Chemical_ChildName = value;
                    if (_selected_Chemical_ChildName != null)
                    {
                        ChildName = _selected_Chemical_ChildName.ChemicalName;
                        ChildNameID = _selected_Chemical_ChildName.ChemicalNameID;
                        ListChemNameChildVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameChildVisibility");
                    }
                    NotifyPropertyChanged("Selected_Chemical_ChildName");
                }
            }
        }

        public bool IsEdit { get; set; } = true;
        public GenericParentChildCasName_VM objGenericParentChildCasName { get; set; }
        private GenericTreeView TreeViewItem_Generic_Backend { get; set; }
        public bool? IsExclude { get; set; } = false;
        public bool? IsNeedMoreResearch { get; set; } = false;
        public bool? IsNeedMoreResearch_Review { get; set; } = false;
        public bool? IsNeedMoreResearchParentChild { get; set; } = false;

        public bool IsEnableNeedMoreRes { get; set; }
        public ObservableCollection<MapProductListVM> listProductsToMap { get; set; }
        private MapProductListVM _SelectedProduct { get; set; }
        private bool IsSearchByProduct { get; set; }
        public MapProductListVM SelectedProduct
        {
            get { return _SelectedProduct; }
            set
            {
                if (_SelectedProduct != value)
                {
                    _SelectedProduct = value;
                    NotifyPropertyChanged("SelectedProduct");
                    TreeSearchByProduct();
                }


            }
        }
        public bool IsParentSuggestionAction { get; private set; } = false;
        public bool IsChildSuggestionAction { get; private set; } = false;
        public bool IsMainSuggestionAction { get; private set; } = false;
        private bool _isDisplaySearchDrawer { get; set; } = false;
        public bool IsDisplaySearchDrawer
        {
            get { return _isDisplaySearchDrawer; }
            set
            {
                if (_isDisplaySearchDrawer != value)
                {
                    _isDisplaySearchDrawer = value;
                    NotifyPropertyChanged("IsDisplaySearchDrawer");
                }
            }
        }
        public CommonDataGrid_ViewModel<GenericSuggestion_Add_VM> commonDG_GenericSuggestion_VM { get; set; }
        public GenericSuggestion_Add_VM SelectedGenericSuggestionVM { get; set; }

        public CommonDataGrid_ViewModel<GenericSuggestion_Review_VM> commonDG_GenericSuggestionReview_VM { get; set; }
        public CommonDataGrid_ViewModel<Map_Generic_Flat_VM> commonDG_GenericFlat_VM { get; set; }
        public CommonDataGrid_ViewModel<Map_Combined_Generic_VM> commonDG_CombinedGeneric_VM { get; set; }

        public GenericSuggestion_Review_VM SelectedGenericSuggestionReviewVM { get; set; }
        public CommonDataGrid_ViewModel<GenericeNestedParent> commonDG_NestedParent_VM { get; set; }
        public CommonDataGrid_ViewModel<GenericSuggestion_Reject_VM> commonDG_RejectGenericSuggestion_VM { get; set; }

        public GenericSuggestion_Reject_VM SelectedRejectGenericSuggestionVM { get; set; }
        public bool IsApprove { get; set; } = false;
        public bool IsReject { get; set; } = false;
        public Visibility contentVisibilityAddEditName_Review { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibiltyComment_Review { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibilityListComments_Review { get; set; } = Visibility.Collapsed;
        public Visibility contentNestedParentVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentFilterSortVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibiltyCommentMultiSelect_Review { get; set; } = Visibility.Collapsed;
        public Visibility ContentVisibilityLoadingMultiSelectReview { get; set; } = Visibility.Collapsed;
        public Visibility contentDisableResearchCommentVisibility { get; set; } = Visibility.Collapsed;
        private bool _isSelectedTreeTab { get; set; }
        public bool IsSelectedTreeTab
        {
            get { return _isSelectedTreeTab; }
            set
            {
                _isSelectedTreeTab = value;
                if (_isSelectedTreeTab)
                {
                    NotifyPropertyChanged("IsShowPopUpTreeView");
                }
                NotifyPropertyChanged("IsSelectedTreeTab");
            }
        }

        private bool _isSelectedReviewSuggestTab { get; set; }
        public bool IsSelectedReviewSuggestTab
        {
            get
            {
                return _isSelectedReviewSuggestTab;
            }
            set
            {
                if (_isSelectedReviewSuggestTab != value)
                {
                    _isSelectedReviewSuggestTab = value;
                    if (_isSelectedReviewSuggestTab)
                    {
                        if (string.IsNullOrEmpty(progressText))
                        {
                            BindGenericSuggestionForReviewGrid();
                        }

                    }
                }

                if (_isSelectedReviewSuggestTab)
                {
                    NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                    if (!string.IsNullOrEmpty(progressText))
                    {

                        ContentVisibilityLoadingMultiSelectReview = Visibility.Visible;
                        NotifyPropertyChanged("ContentVisibilityLoadingMultiSelectReview");
                    }
                }
            }
        }

        private bool _isSelectedHydrateTab { get; set; }
        public bool IsSelectedHydrateTab
        {
            get { return _isSelectedHydrateTab; }
            set
            {

                _isSelectedHydrateTab = value;
                if (_isSelectedHydrateTab)
                {
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", "RefreshGrid", "BindDataGrid"), typeof(Hydrate_VM));
                }

                if (_isSelectedHydrateTab)
                {
                    NotifyPropertyChanged("IsSelectedHydrateTab");
                }
            }
        }
        private bool _isSelectedAlternateCasTab { get; set; }
        public bool IsSelectedAlternateCasTab
        {
            get { return _isSelectedAlternateCasTab; }
            set
            {

                _isSelectedAlternateCasTab = value;
                if (_isSelectedAlternateCasTab)
                {
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", "RefreshGrid", "BindDataGrid"), typeof(AlternateCas_VM));
                }

                if (_isSelectedAlternateCasTab)
                {
                    NotifyPropertyChanged("IsSelectedAlternateCasTab");
                }
            }
        }

        private bool _isSelectedAddSuggestTab { get; set; }
        public bool IsSelectedAddSuggestTab
        {
            get
            {
                return _isSelectedAddSuggestTab;
            }
            set
            {
                _isSelectedAddSuggestTab = value;
                if (_isSelectedAddSuggestTab)
                {
                    NotifyPropertyChanged("IsShowPopUp");
                }
            }
        }
        private bool _isSelectedFlatGenericTab { get; set; }
        public bool IsSelectedFlatGenericTab
        {
            get
            {
                return _isSelectedFlatGenericTab;
            }
            set
            {
                _isSelectedFlatGenericTab = value;

                NotifyPropertyChanged("IsSelectedFlatGenericTab");

            }
        }
        public bool _isSelectedGenericResearchTab { get; set; }
        public bool IsSelectedGenericResearchTab
        {
            get
            {
                return _isSelectedGenericResearchTab;
            }
            set
            {
                _isSelectedGenericResearchTab = value;

                NotifyPropertyChanged("IsSelectedGenericResearchTab");
                BindGenericResearchComment();

            }
        }
        private bool _isSelectedDisapproveSuggestTab { get; set; }
        public bool IsSelectedDisapproveSuggestTab
        {
            get
            {
                return _isSelectedDisapproveSuggestTab;
            }
            set
            {
                _isSelectedDisapproveSuggestTab = value;
                if (_isSelectedDisapproveSuggestTab)
                {
                    BindLogGenericReject();
                }
                NotifyPropertyChanged("IsSelectedDisapproveSuggestTab");

            }
        }
        private bool _isSelectedDeletedGeneTab { get; set; }
        public bool IsSelectedDeletedGeneTab
        {
            get
            {
                return _isSelectedDeletedGeneTab;
            }
            set
            {
                _isSelectedDeletedGeneTab = value;
                if (_isSelectedDeletedGeneTab)
                {
                    BindLogGenericDelete();
                }
                NotifyPropertyChanged("IsSelectedDeletedGeneTab");

            }
        }

        public bool QsidInfoPopUpIsOpen { get; set; } = false;
        public bool ActiveListDetailIsOpen { get; set; } = false;
        public LibraryInformation_QSID_VM QsidDetailInfo { get; set; }
        public ObservableCollection<Library_QSIDs> listActiveQsids { get; set; }
        private Library_QSIDs _selectedActiveQsids { get; set; }
        public Library_QSIDs SelectedActiveQsids
        {
            get { return _selectedActiveQsids; }
            set
            {
                _selectedActiveQsids = value;
                if (_selectedActiveQsids != null)
                {
                    FetchQsidDetail(_selectedActiveQsids.QSID_ID);
                    //  NotifyPropertyChanged("SelectedActiveQsids");
                }
            }
        }
        private ObservableCollection<GenericeNestedParent> _listNestedParent { get; set; }
        public ObservableCollection<GenericeNestedParent> ListNestedParent
        {
            get { return _listNestedParent; }
            set
            {
                if (value != _listNestedParent)
                {
                    _listNestedParent = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericeNestedParent>>>(new PropertyChangedMessage<List<GenericeNestedParent>>(null, _listNestedParent.ToList(), "Default List"));
                }
            }
        }

        private string _titleSuggestionPopUpHeader { get; set; }
        public string TitleSuggestionPopUpHeader
        {
            get { return _titleSuggestionPopUpHeader; }
            set
            {
                _titleSuggestionPopUpHeader = value;
                NotifyPropertyChanged("TitleSuggestionPopUpHeader");
            }
        }
        public ICommand CommandDetailNestedParent { get; private set; }
        public bool SubstanceInfoPopUpIsOpen { get; set; } = false;
        public ObservableCollection<Library_GenericsCategories> listGenericsCategory { get; set; }
        public Library_GenericsCategories selectedParentItemCategory { get; set; }
        public Library_GenericsCategories selectedChildItemCategory { get; set; }

        public ObservableCollection<Library_SubstanceType> listSubstanceType { get; set; }
        public ObservableCollection<Library_SubstanceType> listChildSubstanceType { get; set; }
        private Library_SubstanceType _selectedParentSubstanceType { get; set; }

        public Library_SubstanceType selectedParentSubstanceType
        {
            get { return _selectedParentSubstanceType; }
            set
            {
                if (value == null)
                {
                    _selectedParentSubstanceType = value;
                    return;
                }
                if (value.SubstanceTypeID != 1)
                {
                    IsEnableNeedMoreRes = true;
                    _selectedParentSubstanceType = value;
                }
                else
                {
                    ValidateSubstanceTypeInFlat(ParentCas);
                    IsEnableNeedMoreRes = false;
                    _selectedParentSubstanceType = value;
                    _notifier.ShowError("Substance Type not valid for Parent");
                }
                NotifyPropertyChanged("IsEnableNeedMoreRes");
            }
        }
        private Library_SubstanceType _selectedChildSubstanceType { get; set; }
        public Library_SubstanceType selectedChildSubstanceType
        {
            get { return _selectedChildSubstanceType; }
            set
            {
                _selectedChildSubstanceType = value;
                if (_selectedChildSubstanceType == null)
                {
                    return;
                }
                if (_selectedChildSubstanceType.SubstanceTypeID == 1)
                {
                    ValidateSubstanceTypeInFlat(ChildCas);
                    IsEnableChildGenericCate = false;

                }
                else
                {
                    IsEnableChildGenericCate = true;
                }
                NotifyPropertyChanged("IsEnableChildGenericCate");
            }
        }
        public bool IsEnableChildGenericCate { get; set; } = false;
        private bool _isEditSubInfo { get; set; } = false;
        public bool IsEditSubInfo
        {
            get { return _isEditSubInfo; }
            set
            {
                _isEditSubInfo = value;
                NotifyPropertyChanged("IsEditSubInfo");
            }
        }
        public ObservableCollection<Library_SubstanceCategories> listSubstanceCategory { get; set; }
        public Library_SubstanceCategories selectedParentSubstanceCategory { get; set; }
        public Library_SubstanceCategories selectedChildSubstanceCategory { get; set; }

        public bool IsSearchFromReviewTab { get; set; } = false;

        #region Variable Tab Nested Generic Suggestion
        public bool IsShowPopup_Nested { get; set; }
        public ICommand CommandSearchCasNestedParentTab { get; private set; }
        public ICommand CommandAddGenericSuggestion_Nested { get; private set; }
        public ICommand CommandRemoveGenericSuggestion_Nested { get; private set; }
        public ICommand CommandAddAdditionalComment_Nested { get; private set; }

        public string searchCasNestedParent { get; set; }
        private bool _isSelectedNestedGenericTab { get; set; }
        public bool IsSelectedNestedGenericTab
        {
            get
            {
                return _isSelectedNestedGenericTab;
            }
            set
            {
                _isSelectedNestedGenericTab = value;
                if (_isSelectedNestedGenericTab)
                {
                    //   NotifyPropertyChanged("IsShowPopUpReviewGeneric");

                }
                NotifyPropertyChanged("IsSelectedNestedGenericTab");
            }
        }
        public Visibility contentVisibilityListComments_Nested { get; set; } = Visibility.Collapsed;
        public Visibility NestedParentTabLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility contentAddVisibility_Nested { get; set; } = Visibility.Collapsed;
        public Visibility contentRemoveVisibility_Nested { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibiltyComment_Nested { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<GenericNestedParent_Tab_VM> commonDG_NestedParent_Tab { get; set; }
        private ObservableCollection<GenericNestedParent_Tab_VM> _listNestedParent_Tab { get; set; }
        public ObservableCollection<GenericNestedParent_Tab_VM> ListNestedParent_Tab
        {
            get { return _listNestedParent_Tab; }
            set
            {
                _listNestedParent_Tab = value;
                MessengerInstance.Send<PropertyChangedMessage<List<GenericNestedParent_Tab_VM>>>(new PropertyChangedMessage<List<GenericNestedParent_Tab_VM>>(null, _listNestedParent_Tab.ToList(), "Default List"));

            }
        }
        public GenericNestedParent_Tab_VM SelectedNestedParent_VM { get; set; }
        public Generic_CircularRef_VM SelectedGenericCircularRef_VM { get; set; }


        public int CasIDForNestedDetail { get; set; }
        #endregion

        public ObservableCollection<Map_Generic_Flat_VM> _listFlatGenerics { get; set; }
        public ObservableCollection<Map_Generic_Flat_VM> ListFlatGenerics
        {
            get { return _listFlatGenerics; }
            set
            {
                if (_listFlatGenerics != value)
                {
                    _listFlatGenerics = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Map_Generic_Flat_VM>>>(new PropertyChangedMessage<List<Map_Generic_Flat_VM>>(null, _listFlatGenerics.ToList(), "Default List"));
                }
            }
        }
        public ICommand CommandGotoFlatGenerics { get; private set; }
        public ICommand CommandSearchCasFlatGenericsTab { get; private set; }
        public ICommand CommandSearchCasCombinedGenericsTab { get; private set; }
        public ICommand CommandSearchCas_DeletedGenerics { get; private set; }
        public string searchCasFlatGenerics { get; set; }
        public string searchQsidFlatGenerics { get; set; }
        public string searchCasCombinedGenerics { get; set; }
        public Visibility GenericRejectLoaderVisibility { get; set; }
        public Visibility GenericDeleteLoaderVisibility { get; set; }
        public CommonDataGrid_ViewModel<GenericDelete_VM> commonDG_GenericDelete { get; set; }
        public ObservableCollection<GenericRejected_VM> _listGenericReject { get; set; }
        public ObservableCollection<GenericRejected_VM> ListGenericReject
        {
            get { return _listGenericReject; }
            set
            {
                if (_listGenericReject != value)
                {
                    _listGenericReject = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericRejected_VM>>>(new PropertyChangedMessage<List<GenericRejected_VM>>(null, _listGenericReject.ToList(), "Default List"));
                }
            }
        }
        public List<GenericDelete_VM> _listGenericDelete { get; set; }
        public List<GenericDelete_VM> ListGenericDelete
        {
            get { return _listGenericDelete; }
            set
            {
                if (_listGenericDelete != value)
                {
                    _listGenericDelete = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<GenericDelete_VM>>>(new PropertyChangedMessage<List<GenericDelete_VM>>(null, _listGenericDelete, "Default List"));
                }
            }
        }
        public GenericDelete_VM SelectedGenericDeleteVM { get; set; }
        public GenericRejected_VM SelectedGenericRejectedVM { get; set; }
        public CommonDataGrid_ViewModel<GenericRejected_VM> commonDG_GenericReject { get; set; }
        public ObservableCollection<Map_Combined_Generic_VM> _listCombinedGenerics { get; set; }
        public ObservableCollection<Map_Combined_Generic_VM> ListCombinedGenerics
        {
            get { return _listCombinedGenerics; }
            set
            {
                if (_listCombinedGenerics != value)
                {
                    _listCombinedGenerics = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Map_Combined_Generic_VM>>>(new PropertyChangedMessage<List<Map_Combined_Generic_VM>>(null, _listCombinedGenerics.ToList(), "Default List"));
                }
            }
        }

        public List<Generic_CircularRef_VM> _listUniqueCircularRef { get; set; }
        public List<Generic_CircularRef_VM> ListUniqueCircularRef
        {
            get { return _listUniqueCircularRef; }
            set
            {
                if (_listUniqueCircularRef != value)
                {
                    _listUniqueCircularRef = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Generic_CircularRef_VM>>>(new PropertyChangedMessage<List<Generic_CircularRef_VM>>(null, _listUniqueCircularRef, "Default List"));
                }
            }
        }



        public ICommand CommandClearSearchCas_DeletedGenerics { get; private set; }
        public string casForSearch_Filter { get; set; }
        public string searchFor_Filter { get; set; }
        public string searchType_Filter { get; set; }
        public string matchCond_Filter { get; set; }
        public ICommand CommandShowPopFilterSortNode { get; private set; }
        public ObservableCollection<SelectListItem> listSortFieldType { get; set; }
        public SelectListItem SelectedSortFieldItem { get; set; }
        public ObservableCollection<SelectListItem> listSortType { get; set; }
        public SelectListItem SelectedSortTypeItem { get; set; }
        private string _searchCasNodeLevel { get; set; }
        private string _searchChemicalNameNodeLevel { get; set; }
        public string SearchCasNodeLevel
        {
            get
            {

                return _searchCasNodeLevel;
            }
            set
            {
                _searchCasNodeLevel = value;
                NotifyPropertyChanged("SearchCasNodeLevel");
            }
        }
        public string SearchChemicalNameNodeLevel
        {
            get
            {
                return _searchChemicalNameNodeLevel;
            }
            set
            {
                _searchChemicalNameNodeLevel = value;
                NotifyPropertyChanged("SearchChemicalNameNodeLevel");
            }
        }
        public ICommand CommandApplyNodeLevelFilter { get; set; }
        public ICommand CommandClearNodeLevelFilter { get; set; }
        public string progressText { get; set; }
        public ICommand CommandShowGenericCalculated { get; set; }
        public ObservableCollection<SelectListItem> listItemSearchTypeForGeneric { get; set; }
        private SelectListItem _selectedSearchItemGeneric { get; set; }
        public SelectListItem selectedSearchItemGeneric
        {
            get { return _selectedSearchItemGeneric; }
            set
            {
                _selectedSearchItemGeneric = value;
                NotifyPropertyChanged("selectedSearchItemGeneric");
            }
        }
        private SelectListItem _selectedSearch_Combine { get; set; }
        public SelectListItem selectedSearch_Combine
        {
            get { return _selectedSearch_Combine; }
            set
            {
                _selectedSearch_Combine = value;
                NotifyPropertyChanged("selectedSearch_Combine");
            }
        }
        private SelectListItem _selectedSearch_UniqueCircularRef { get; set; }
        public SelectListItem selectedSearch_UniqueCircularRef
        {
            get { return _selectedSearch_UniqueCircularRef; }
            set
            {
                _selectedSearch_UniqueCircularRef = value;
                NotifyPropertyChanged("selectedSearch_UniqueCircularRef");
            }
        }

        public string searchCas_UniqueCircularRef { get; set; }

        public ICommand CommandCalculateRefreshFlats { get; set; }
        public CommonDataGrid_ViewModel<Generic_CircularRef_VM> commonDG_UniqueCircularRef_VM { get; set; }
        public Visibility IsGeneResearchChangeVisibility { get; set; } = Visibility.Collapsed;
        public Visibility IsGeneResearchChangeVisibility_Review { get; set; } = Visibility.Collapsed;
        public ICommand CommandIsGeneResearchChange { get; private set; }

        public ICommand CommandShowPopUPGenericRes { get; set; }
        public string genericResearchCommentAction { get; set; }

        public ICommand CommandDisableResearchInTreeView { get; set; }
        public ICommand CommandOpenPopUpCscTicket { get; set; }
        public bool AddCSCTicketIsOpenPopUp { get; set; } = false;
        public bool IsNeedAddCSCTickets_TreeView { get; set; }
        public CommentsUC_VM objCommentDC { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public CommentsUC_VM objCommentDC_Review { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public CommentsUC_VM objCommentDC_Disapprove { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public CommentsUC_VM objCommentDC_DeleteGenerics { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public CommentsUC_VM objCommentDC_GenericsBacklog { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public Generics_VM()
        {
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Generics_VM), NotifyMe);
            commonDG_GenericSuggestion_VM = new CommonDataGrid_ViewModel<GenericSuggestion_Add_VM>(GetListGridColumn(), "RowId", "");
            commonDG_GenericSuggestionReview_VM = new CommonDataGrid_ViewModel<GenericSuggestion_Review_VM>(GetListGridColumn_Review(), "GenericSuggestion_ID", "");
            commonDG_NestedParent_VM = new CommonDataGrid_ViewModel<GenericeNestedParent>(GetListGridColumn_Nested(false), "ParentCas", "Generic Nested Parent List:");
            commonDG_NestedParent_Tab = new CommonDataGrid_ViewModel<GenericNestedParent_Tab_VM>(GetListGridColumn_Nested(true), "", "Generic Nested Parent List:");
            commonDG_GenericFlat_VM = new CommonDataGrid_ViewModel<Map_Generic_Flat_VM>(GetListGridColumn_Flat(), "", "Flat Generic List:");
            commonDG_GenericReject = new CommonDataGrid_ViewModel<GenericRejected_VM>(GetListGridColumn_Reject(), "", "Disapproved Suggestions List:");
            commonDG_GenericDelete = new CommonDataGrid_ViewModel<GenericDelete_VM>(GetListGridColumn_Delete(), "", "Deleted Generic List:");
            commonDG_CombinedGeneric_VM = new CommonDataGrid_ViewModel<Map_Combined_Generic_VM>(GetListGridColumn_CombinedGenerics(), "", "Combined Generic List:");
            commonDG_UniqueCircularRef_VM = new CommonDataGrid_ViewModel<Generic_CircularRef_VM>(GetListGridColumn_UniqueRef(), "", "Unique Circular Reference Data");
            commonDG_GenericResearchComment_VM = new CommonDataGrid_ViewModel<GenericResearchComment_VM>(GetListGridColumn_ResearchComment(), "CasID", "Unique Generic Research Comment:");
            commonDG_RejectGenericSuggestion_VM = new CommonDataGrid_ViewModel<GenericSuggestion_Reject_VM>(GetListGridColumnRejectSuggestion(), "ParentCas", "");
            commonDataGrid_ClosedGroup = new CommonDataGrid_ViewModel<GenericSuggestionClosedGroup>(GetListGridColumnClosedGroup(), "ParentCas", "");

            BindSortTypeListNodelevel();
            BindListSearchTypeGenericDelete();
            BindListProduct();
            BindListItemNodeType();
            BindGenericSuggestionTreeView();
            BindListItemSearchFor();
            BindListItemSearchType();
            BindListItemMatchCondition();
            BindGenericsCategories();
            BindSubstanceType();
            BindSubstanceCategories();
            BindListSearchTypeFilterQSID();
            DefaultSelectionSearchType();
            BindGenericUniqueCircularRef();
            BindGenericResearchComment();

            CommandSaveGenericSuggestion = new RelayCommand(CommandSaveGenericSuggestionExecute, CommandSaveGenericSuggestionCanExecute);
            CommandImportBulkSuggestion = new RelayCommand(CommandImportBulkSuggestionExecute);
            CommandAddGenericSuggestion = new RelayCommand(CommandAddGenericSuggestionExecute, CommandAddGenericSuggestionCanExecute);
            CommandAddMultipleGenericSuggestion = new RelayCommand(CommandAddMultipleGenericSuggestionExecute, CommandAddGenericSuggestionCanExecute);
            CommandSaveCommentGeneric = new RelayCommand(CommandSaveCommentGenericExecute);
            CommandShowPopAddGeneric = new RelayCommand(CommandShowPopAddGenericExecute, CommandShowPopAddGenericCanExecute);
            CommandShowPopRemoveGeneric = new RelayCommand(CommandShowPopRemoveGenericExecute);
            CommandShowPopApproveGeneric = new RelayCommand(CommandShowPopApproveGenericExecute, CommandApproveRejectGenericSuggestionCanExecute);
            CommandShowPopRejectGeneric = new RelayCommand(CommandShowPopRejectGenericExecute, CommandApproveRejectGenericSuggestionCanExecute);
            CommandAddGenericSuggestionInTreeView = new RelayCommand(CommandAddGenericSuggestionInTreeViewExecute, CommandAddGenericSuggestionInTreeViewCanExecute);
            CommandAddMultipleGenericSuggestionInTreeView = new RelayCommand(CommandAddMultipleGenericSuggestionInTreeViewExecute, CommandAddGenericSuggestionInTreeViewCanExecute);
            CommandRemoveGenericSuggestionInTreeView = new RelayCommand(CommandRemoveGenericSuggestionInTreeViewExecute);
            CommandApproveGenericSuggestionInTreeView = new RelayCommand(CommandApproveGenericSuggestionInTreeViewExecute, CommandApproveRejectGenericSuggestionCanExecute);
            CommandRejectGenericSuggestionInTreeView = new RelayCommand(CommandRejectGenericSuggestionInTreeViewExecute, CommandApproveRejectGenericSuggestionCanExecute);
            CommandShowPopAddParentGeneric = new RelayCommand(CommandShowPopAddParentGenericExecute);
            CommandAddAdditionalComment = new RelayCommand(CommandAddAdditionalCommentExecute, CommandAddAdditionalCommentCanExecute);
            CommandSearchCasTree = new RelayCommand(CommandSearchCasTreeExecute, CommandSearchCasTreeCanExecute);
            CommandFindNextFoundResult = new RelayCommand(CommandFindNextFoundResulteExecute, CommandFindNextFoundResultCanExecute);
            CommandFindPrevFoundResult = new RelayCommand(CommandFindPrevFoundResultExecute, CommandFindPrevFoundResultCanExecute);
            CommandSelectedTreeViewItem = new RelayCommand(CommandSelectedTreeViewItemExecute);
            CommandClearSearch = new RelayCommand(CommandClearSearchExecute);
            // CommandAddMainParent = new RelayCommand(CommandAddMainParentExecute, CommandAddMainParentCanExecute);
            CommandShowPopUpAddMainParent = new RelayCommand(CommandShowPopUpAddMainParentExecute);
            CommandShowAllFilterResult = new RelayCommand(CommandShowAllFilterResultExecute, CommandShowAllFilterResultCanExecute);
            CommandShowPopUpAddCasName = new RelayCommand(CommandShowPopUpAddCasNameExecute);
            CommandSaveAddEditCasName = new RelayCommand(CommandSaveAddEditCasNameExecute, CommandSaveAddEditCasNameCanExecute);
            CommandIsExcludeChange = new RelayCommand(CommandIsExcludeChangeExecute);
            CommandCopyTreeDataToClipboard = new RelayCommand(CommandCopyCellDataToClipboardExecute);
            CommandRefreshTreeView = new RelayCommand(CommandRefreshTreeViewExecute);
            CommandLostFocusChildCas = new RelayCommand(CommandLostFocusChildCasExecute);
            CommandLostFocusParentCas = new RelayCommand(CommandLostFocusParentCasExecute);
            CommandShowImportBulkPopUp = new RelayCommand(CommandShowImportBulkPopUpExecute);
            CommandShowPopUpAddGeneric = new RelayCommand(CommandShowPopUpAddGenericExecute);
            CommandDownloadTemplateFile = new RelayCommand(CommandDownloadTemplateFileExecute);
            CommandSaveAddEditChemicalName = new RelayCommand(CommandSaveAddEditChemicalName_Review_Execute);
            CommandPopUpActiveList = new RelayCommand(CommandPopUpActiveListExecute);
            CommandCloseQsidInfoPopUp = new RelayCommand(CommandCloseQsidInfoPopUpExecute);
            CommandOpenCurrentDataView = new RelayCommand(CommandOpenCurrentDataViewExecute);
            CommandClearGenericSuggestion = new RelayCommand(CommandClearGenericSuggestionExecute, CommandSaveGenericSuggestionCanExecute);
            CommandClosePopUp = new RelayCommand(CommandClosePopUpExecute);
            CommandAddAdditionalComment_Review = new RelayCommand(CommandAddAdditionalReviewCommentExecute, CommandAddAdditionalReviewCommentCanExecute);
            CommandShowNestedParent = new RelayCommand(CommandShowNestedParentExecute);
            CommandDetailNestedParent = new RelayCommand(CommandDetailNestedParentExecute);
            CommandSearchCasNestedParentTab = new RelayCommand(CommandSearchCasNestedParentTabExecute, CommandSearchCasNestedParentTabCanExecute);
            CommandShowSubstanceInfoPopUp = new RelayCommand(CommandShowSubstanceInfoPopUpExecute);
            CommandCloseSubstanceInfoPopUp = new RelayCommand(CommandCloseSubstanceInfoPopUpExecute);
            //  CommandCloseSubstanceInfoPopUp = new RelayCommand(CommandCloseSubstanceInfoPopUpExecute);
            CommandUpdateSubstanceInfo = new RelayCommand(CommandUpdateSubstanceInfoExecute);
            CommandRemoveGenericSuggestion_Nested = new RelayCommand(CommandRemoveGenericSuggestion_NestedExecute);
            CommandAddGenericSuggestion_Nested = new RelayCommand(CommandAddGenericSuggestion_NestedExecute, CommandAddGenericSuggestion_NestedCanExecute);
            CommandAddAdditionalComment_Nested = new RelayCommand(CommandAddAdditionalComment_NestedExecute, CommandAddAdditionalReviewCommentCanExecute);
            CommandGotoFlatGenerics = new RelayCommand(CommandGotoFlatGenericsExecute);
            CommandSearchCasFlatGenericsTab = new RelayCommand(CommandSearchCasFlatGenericsTabExecute);
            CommandSearchCasCombinedGenericsTab = new RelayCommand(CommandSearchCasCombinedGenericsTabExecute);
            CommandSearchCas_DeletedGenerics = new RelayCommand(CommandSearchCas_DeletedGenericsExecute);
            CommandClearSearchCas_DeletedGenerics = new RelayCommand(CommandClearSearchCas_DeletedGenericsExecute);
            CommandShowPopFilterSortNode = new RelayCommand(CommandShowPopFilterSortNodeExecute);
            CommandApplyNodeLevelFilter = new RelayCommand(CommandApplyNodeLevelFilterExecute, CommandApplyNodeLevelFilterCanExecute);
            CommandClearNodeLevelFilter = new RelayCommand(CommanClearNodeLevelFilterExecute);
            CommandShowCommentForMultiSelect = new RelayCommand(CommandShowCommentForMultiSelectExecute, CommandShowCommentForMultiSelectCanExecute);
            CommandSaveCommentForMultiGeneric = new RelayCommand(CommandSaveCommentForMultiGenericExecute);
            CommandShowGenericCalculated = new RelayCommand(CommandShowGenericCalculatedExecuted);
            CommandCalculateRefreshFlats = new RelayCommand(CommandCalculateRefreshFlatsExecute);
            CommandSearchCasUniqueCircularRefTab = new RelayCommand(CommandSearchCasUniqueCircularRefTabExecute);
            CommandIsGeneResearchChange = new RelayCommand(CommandIsGeneResearchChangeExecute);
            CommandShowPopUPGenericRes = new RelayCommand(CommandShowPopUPGenericResExecute);

            CommandAddAdditionalComment_Research = new RelayCommand(CommandAddAdditionalComment_ResearchExecute);
            CommandDisableResearchInTreeView = new RelayCommand(CommandDisableResearchInTreeViewExecute);
            CommandRunRefreshFlatExe = new RelayCommand(CommandRunRefreshFlatExeExecute);
            CommandGoToSubstanceIdentity = new RelayCommand(CommandGoToSubstanceIdentityExecute);
            CommandOpenPopUpCscTicket = new RelayCommand(CommandOpenPopUpCscTicketExecute);
            CommandDisableResearchInBackLog = new RelayCommand(CommandDisableResearchInBackLogExecute);
            CommandMarkGroupAsClosed = new RelayCommand(CommandMarkGroupAsClosedExecute);
            CommandRevertClosedGroup = new RelayCommand(CommandRevertClosedGroupExecute);

            MessengerInstance.Register<PropertyChangedMessage<GenericSuggestion_Add_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GenericSuggestion_Review_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GenericNestedParent_Tab_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GenericRejected_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GenericDelete_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<Generic_CircularRef_VM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GenericResearchComment_VM>>(this, NotifyMe);


            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericSuggestion_Add_VM), CommandButtonExecuted);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericSuggestion_Review_VM), CommandButtonExecuted_Review);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericNestedParent_Tab_VM), CommandButtonExecuted_NestedParent);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericDelete_VM), CommandButtonExecuted_DeletedGeneric);
            //MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericRejected_VM), CommandButtonExecuted_DisApproveGeneric);
            MessengerInstance.Register<NotificationMessageAction<GenericSuggestion_Review_VM>>(this, CallBackBindListDetailGridExecute);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Generics_VM), SearchCasInTreeEvent);
            MessengerInstance.Register<PropertyChangedMessage<List<GenericSuggestion_Review_VM>>>(this, typeof(GenericSuggestion_Review_VM), NotifyMeFilterList);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Generic_CircularRef_VM), CommandButtonExecuted_CircularRef);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Generics_VM), TabChangedEvent);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GenericResearchComment_VM), CommandButtonExecuted_GenericResearchComment);
            MessengerInstance.Register<NotificationMessageAction<GenericResearchComment_VM>>(this, CallBackBindListDetailGridExecute);
            MessengerInstance.Register<NotificationMessageAction<Generic_CircularRef_VM>>(this, CallBackBindRefreshCirculateExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
        }

      

        private bool CommandShowPopAddGenericCanExecute(object obj)
        {
            return true;
        }

        private void CommandGoToSubstanceIdentityExecute(object obj)
        {
            string cas = (string)obj;
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", cas, "ActiveSubstanceIdentityWithCas"), typeof(Generics_VM));
        }

        private void CommandRunRefreshFlatExeExecute(object obj)
        {
            string exePath = Environment.CurrentDirectory + "\\UpdateFlatGenerics.exe";
            if (File.Exists(exePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.FileName = exePath;
                Process.Start(startInfo);
            }
            else
            {
                _notifier.ShowError("Unable to run file from below path: " + System.Environment.NewLine + exePath);
            }
        }

        private void CallBackBindRefreshCirculateExecute(NotificationMessageAction<Generic_CircularRef_VM> obj)
        {
            BindGenericUniqueCircularRef();
        }


        private void CommandButtonExecuted_CircularRef(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "GoToTreeView")
            {
                SwitchTabFindCurrentGenericSuggestion_CircularRef();
            }
        }

        private void CommandSearchCasUniqueCircularRefTabExecute(object obj)
        {
            BindGenericUniqueCircularRef();
        }


        private void CommandButtonExecuted_DeletedGeneric(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "ShowComments")
            {
                BindListItemComments_Review(SelectedGenericDeleteVM.ParentCasID, SelectedGenericDeleteVM.ChildCasID);
                BindListItemParentComments_Review(SelectedGenericDeleteVM.ParentCasID);

                IsShowPopUpDeleteGeneric = true;
                NotifyPropertyChanged("IsShowPopUpDeleteGeneric");
                CommentGenericSuggestionAction_Review = string.Empty;
                NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
                TitleCommentHeader_Review = "Generic Comments for Parent = '" + SelectedGenericDeleteVM.ParentCas + "' and Child = '" + SelectedGenericDeleteVM.ChildCas + "' :";
                NotifyPropertyChanged("TitleCommentHeader_Review");
            }
        }

        private void CommandShowGenericCalculatedExecuted(object obj)
        {

        }

        private void NotifyMeFilterList(PropertyChangedMessage<List<GenericSuggestion_Review_VM>> obj)
        {
            if (obj.PropertyName == "Filter List")
            {
                listGenericReview_Filter = obj.NewValue;
            }
        }

        private void SearchCasInTreeEvent(PropertyChangedMessage<string> objParam)
        {
            if (objParam.PropertyName == "SearchCasInTreeFromCurrentDataView")
            {

                IsSearchFromReviewTab = false;
                string casForSearch = objParam.NewValue;

                SearchCasInTreeView(casForSearch, "Cas", "Both Child and Parent", "Exact Match");//FilterReviewGenericSuggestion
                IsSelectedTreeTab = true;
            }
            else if (objParam.PropertyName == "FilterAddSuggestionReview_Generic")
            {
                IsSelectedReviewSuggestTab = true;
                var dateFrom = objParam.NewValue.Split("@")[0].ToString();
                var dateTo = objParam.NewValue.Split("@")[1].ToString();
                if (!string.IsNullOrEmpty(dateFrom) && !string.IsNullOrEmpty(dateTo))
                {
                    var filterdata = new FilterData(FilterOperator.Between, FilterType.DateTimeBetween, "DateSuggest", typeof(DateTime), dateFrom, dateTo, true, false);
                    commonDG_GenericSuggestionReview_VM.OverrideQueryController.ColumnFilterData = filterdata;
                    //commonDG_GenericSuggestionReview_VM.DataGridFilteringStarted(null, null);
                }
            }
        }

        private void CommandClearSearchCas_DeletedGenericsExecute(object obj)
        {
            casSearch_Delete = string.Empty;
            SelectItem_Deleted = listItemSearchType_Deleted.Where(x => x.Text == "Only Parent").FirstOrDefault();
            NotifyPropertyChanged("SelectItem_Deleted");
        }

        private void CommandSearchCas_DeletedGenericsExecute(object obj)
        {
            BindLogGenericDelete();
        }

        public void BindFlatGenerics()
        {
            if (selectedSearchItemGeneric.Text == "Cas" || selectedSearchItemGeneric.Text == "Cas ID")
            {
                if (!string.IsNullOrEmpty(searchCasFlatGenerics))
                {
                    searchCasFlatGenerics = searchCasFlatGenerics.Replace("-", "");
                }
                ListFlatGenerics = new ObservableCollection<Map_Generic_Flat_VM>(_objGeneric_Service.GetAllFlatGenerics(searchCasFlatGenerics, selectedSearchItemGeneric.Text));
            }
            else if (selectedSearchItemGeneric.Text == "Qsid" || selectedSearchItemGeneric.Text == "Qsid ID")
            {
                ListFlatGenerics = new ObservableCollection<Map_Generic_Flat_VM>(_objGeneric_Service.GetAllFlatGenericsByQsid(searchQsidFlatGenerics, selectedSearchItemGeneric.Text));
            }
        }
        private void CommandCalculateRefreshFlatsExecute(object obj)
        {
            _notifier.ShowInformation("Background process of generate flat and combine generics start, system update while it completed!");
            Task.Run(() =>
            {


                if (obj != null)
                {
                    _objGeneric_Service.Refresh_RecalculateFlatGenerics(Convert.ToString(obj), "Cas ID");
                }
                else
                {
                    if (selectedSearchItemGeneric.Text == "Cas" || selectedSearchItemGeneric.Text == "Cas ID")
                    {
                        if (!string.IsNullOrEmpty(searchCasFlatGenerics))
                        {
                            searchCasFlatGenerics = searchCasFlatGenerics.Replace("-", "");
                        }
                        _objGeneric_Service.Refresh_RecalculateFlatGenerics(searchCasFlatGenerics, selectedSearchItemGeneric.Text);
                    }
                    else if (selectedSearchItemGeneric.Text == "Qsid" || selectedSearchItemGeneric.Text == "Qsid ID")
                    {
                        _objGeneric_Service.Refresh_RecalculateFlatGenerics(searchQsidFlatGenerics, selectedSearchItemGeneric.Text);
                    }
                }
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("All Flats and combined generics created successfully!");
                });
            });
        }
        public void BindCombinedGenerics()
        {
            if (selectedSearch_Combine.Text == "Cas" || selectedSearch_Combine.Text == "Cas ID")
            {

                if (!string.IsNullOrEmpty(searchCasCombinedGenerics))
                {
                    searchCasCombinedGenerics = searchCasCombinedGenerics.Replace("-", "");
                }
                ListCombinedGenerics = new ObservableCollection<Map_Combined_Generic_VM>(_objGeneric_Service.GetAllCombinedGenerics(searchCasCombinedGenerics, selectedSearch_Combine.Text));
            }
            else if (selectedSearch_Combine.Text == "Qsid" || selectedSearch_Combine.Text == "Qsid ID")
            {
                ListCombinedGenerics = new ObservableCollection<Map_Combined_Generic_VM>(_objGeneric_Service.GetAllCombinedGenericsByQsid(searchCasCombinedGenerics, selectedSearch_Combine.Text));
            }
        }

        public void BindLogGenericReject()
        {
            GenericRejectLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("GenericRejectLoaderVisibility");
            Task.Run(() =>
            {
                ListGenericReject = new ObservableCollection<GenericRejected_VM>(_objGeneric_Service.GetLogGenericsSuggestionRejected().Select(x => new GenericRejected_VM
                {
                    ChildCas = x.ChildCas,
                    ChildCasID = x.ChildCasID,
                    DateAdded = Convert.ToDateTime(x.Timestamp),
                    ParentCas = x.ParentCas,
                    ParentCasID = x.ParentCasID,
                    ParentName = x.ParentChemicalName,
                    Action = x.Status,
                    ChildName = x.ChemicalName,
                    IsExclude = x.IsExclude == true ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                    UserName = !string.IsNullOrEmpty(x.UserID) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                    SuggestedUserName = !string.IsNullOrEmpty(x.SuggestedUserID) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.SuggestedUserID.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.SuggestedUserID.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                    SuggestedDateAdded = x.SuggestedTimestamp
                }).ToList());
                GenericRejectLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("GenericRejectLoaderVisibility");
            });

        }
        public void BindLogGenericDelete()
        {
            GenericDeleteLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("GenericDeleteLoaderVisibility");
            Task.Run(() =>
            {
                if (!string.IsNullOrEmpty(casSearch_Delete)) { casSearch_Delete = casSearch_Delete.Replace("-", ""); }
                ListGenericDelete = _objGeneric_Service.GetLogGenericsDeleted(casSearch_Delete, SelectItem_Deleted.Text).Select(x => new GenericDelete_VM
                {
                    ChildCas = x.ChildCas,
                    ChildCasID = x.ChildCasID,
                    DateAdded = Convert.ToDateTime(x.Timestamp),
                    ParentCas = x.ParentCas,
                    ParentCasID = x.ParentCasID,
                    ParentName = x.ParentChemicalName,
                    ChildName = x.ChemicalName,
                    IsExclude = x.IsExclude == true ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                    UserName = !string.IsNullOrEmpty(x.UserID) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                    SuggestedUserName = !string.IsNullOrEmpty(x.SuggestedUserID) && x.SuggestedUserID.ToLower().Any(x => x == 'i') && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.SuggestedUserID.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.SuggestedUserID.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                    SuggestedDateAdded = x.SuggestedTimestamp
                }).ToList();
                NotifyPropertyChanged("GenericDeleteLoaderVisibility");
                GenericDeleteLoaderVisibility = Visibility.Collapsed;
            });
        }

        public void BindListSearchTypeGenericDelete()
        {
            var listItemNode = new List<SelectListItem>();
            listItemNode.Add(new SelectListItem { Text = "Only Parent" });
            listItemNode.Add(new SelectListItem { Text = "Only Child" });
            listItemNode.Add(new SelectListItem { Text = "Both Child and Parent" });
            listItemSearchType_Deleted = new ObservableCollection<SelectListItem>(listItemNode);
            SelectItem_Deleted = listItemSearchType_Deleted.Where(x => x.Text == "Only Parent").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchType_Deleted");
            NotifyPropertyChanged("SelectItem_Deleted");
        }
        private void CommandButtonExecuted_NestedParent(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "GoToTreeView_Nested")
            {
                SwitchTabFindCurrentGenericSuggestion_Nested();
            }
            else if (obj.Notification == "AddDeleteSuggestion_Nested")
            {
                if (SelectedNestedParent_VM.Status == "Confirm")
                {
                    ShowPopAddDeleteSuggestion_Nested();
                }
                else
                {
                    _notifier.ShowWarning("First Approve/Reject this suggestion.");
                }
            }
            else if (obj.Notification == "AddSuggestion_Nested")
            {
                if (SelectedNestedParent_VM.Status == "Confirm")
                {
                    ShowPopAddSuggestion_Nested();
                }
                else
                {
                    _notifier.ShowWarning("First Approve/Reject this suggestion.");
                }

            }
            else if (obj.Notification == "ShowComments")
            {
                ShowCommentParentAndChild_Nested();
            }
            else if (obj.Notification == "ApproveGenericSuggestion_Nested")
            {
                if (SelectedNestedParent_VM.Status != "Confirm")
                {
                    ShowApproveRejectGenericsSuggestion_Nested(true, false);
                }
            }
            else if (obj.Notification == "RejectGenericSuggestion_Nested")
            {
                if (SelectedNestedParent_VM.Status != "Confirm")
                {
                    ShowApproveRejectGenericsSuggestion_Nested(false, true);
                }
            }
        }


        private void NotifyMe(PropertyChangedMessage<GenericDelete_VM> obj)
        {
            SelectedGenericDeleteVM = obj.NewValue;
            NotifyPropertyChanged("SelectedGenericDeleteVM");
            if (SelectedGenericDeleteVM != null)
            {
                objCommentDC_DeleteGenerics = new CommentsUC_VM(new GenericComment_VM(SelectedGenericDeleteVM.ParentCas, SelectedGenericDeleteVM.ChildCas, SelectedGenericDeleteVM.ParentCasID, SelectedGenericDeleteVM.ChildCasID, 370, true, "Generics Parent-Child"));
                NotifyPropertyChanged("objCommentDC_DeleteGenerics");
            }
        }

        private void NotifyMe(PropertyChangedMessage<GenericRejected_VM> obj)
        {
            SelectedGenericRejectedVM = obj.NewValue;
            NotifyPropertyChanged("SelectedGenericRejectedVM");
            if (SelectedGenericRejectedVM != null)
            {
                objCommentDC_Disapprove = new CommentsUC_VM(new GenericComment_VM(SelectedGenericRejectedVM.ParentCas, SelectedGenericRejectedVM.ChildCas, SelectedGenericRejectedVM.ParentCasID, SelectedGenericRejectedVM.ChildCasID, 370, true, "Generics Parent-Child"));
                NotifyPropertyChanged("objCommentDC_Disapprove");
            }
        }

        private void NotifyMe(PropertyChangedMessage<GenericNestedParent_Tab_VM> obj)
        {
            SelectedNestedParent_VM = obj.NewValue;
            NotifyPropertyChanged("SelectedNestedParent_VM");
        }

        private void NotifyMe(PropertyChangedMessage<Generic_CircularRef_VM> obj)
        {
            SelectedGenericCircularRef_VM = obj.NewValue;
            NotifyPropertyChanged("SelectedNestedParent_VM");
        }



        private void NotifyMe(PropertyChangedMessage<string> obj)
        {
            if (IsSelectedTreeTab)
            {

                NotifyPropertyChanged("IsShowPopUpTreeView");
            }
            else if (IsSelectedAddSuggestTab)
            {
                NotifyPropertyChanged("IsShowPopUp");
            }
            else if (IsSelectedReviewSuggestTab)
            {
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
            }
            else if (IsSelectedHydrateTab)
            {
                IsSelectedHydrateTab = true;
            }
            else if (IsSelectedAlternateCasTab)
            {
                IsSelectedAlternateCasTab = true;
            }
        }

        private void CommandClosePopUpExecute(object obj)
        {
            string Currenttab = (string)obj;
            if (Currenttab == "Tree")
            {
                IsShowPopUpTreeView = false;
                ClearVariableAddSuggestion();
            }
            else if (Currenttab == "AddSuggest")
            {
                IsShowPopUp = false;
                NotifyPropertyChanged("IsShowPopUp");
            }
            else if (Currenttab == "ReviewSuggest")
            {
                IsShowPopUpReviewGeneric = false;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
            }
            else if (Currenttab == "Nested")
            {
                IsShowPopup_Nested = false;
                NotifyPropertyChanged("IsShowPopup_Nested");
            }
            else if (Currenttab == "DeleteGenerics")
            {
                IsShowPopUpDeleteGeneric = false;
                NotifyPropertyChanged("IsShowPopUpDeleteGeneric");
            }
            else if (Currenttab == "DisapproveGenerics")
            {
                IsShowPopUpDisApproveGeneric = false;
                NotifyPropertyChanged("IsShowPopUpDisApproveGeneric");
            }
            else if (Currenttab == "GenericResearch")
            {
                IsShowPopUpGenericRes = false;
                NotifyPropertyChanged("IsShowPopUpGenericRes");
                CommentGenericSuggestionAction = string.Empty;
            }
        }

        private void CallBackBindListDetailGridExecute(NotificationMessageAction<GenericSuggestion_Review_VM> notificationMessageAction)
        {
            BindGenericSuggestionForReviewGrid();
        }
        private void NotifyMe(PropertyChangedMessage<GenericSuggestion_Review_VM> obj)
        {
            SelectedGenericSuggestionReviewVM = obj.NewValue;
            NotifyPropertyChanged("SelectedGenericSuggestionReviewVM");
            if (SelectedGenericSuggestionReviewVM != null)
            {
                objCommentDC_Review = new CommentsUC_VM(new GenericComment_VM(SelectedGenericSuggestionReviewVM.ParentCas, SelectedGenericSuggestionReviewVM.ChildCas, SelectedGenericSuggestionReviewVM.ParentCasID, SelectedGenericSuggestionReviewVM.ChildCasID, 370, true, "Generics Parent-Child"));
                NotifyPropertyChanged("objCommentDC_Review");
            }
        }

        private List<CommonDataGridColumn> GetListGridColumn_Review()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Select", ColBindingName = "SelectedGenSuggestion", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChildName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exclude", ColBindingName = "IsExclude", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent Generic Category", ColBindingName = "Parent_SubstanceInfo.GenericsCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent Substance Category", ColBindingName = "Parent_SubstanceInfo.SubstanceCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent Substance Type", ColBindingName = "Parent_SubstanceInfo.SubstanceTypeName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child Generic Category", ColBindingName = "Child_SubstanceInfo.GenericsCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child Substance Category", ColBindingName = "Child_SubstanceInfo.SubstanceCategoryName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child Substance Type", ColBindingName = "Child_SubstanceInfo.SubstanceTypeName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Suggested", ColBindingName = "DateSuggest", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested By", ColBindingName = "SuggestedBy", ColType = "Combobox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Cas InValid", ColBindingName = "ParentCasIsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Cas InValid", ColBindingName = "ChildCasIsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approve", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#79d70f", CommandParam = "ApproveGenericSuggestionReview" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Reject", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "100", ContentName = "Reject", BackgroundColor = "#e8505b", CommandParam = "RejectGenericSuggestionReview" });
            // listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Show", BackgroundColor = "#931a25", CommandParam = "ShowComments" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditName_Review" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Go To Tree View", BackgroundColor = "#ffc93c", CommandParam = "GoToTreeView_Review" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Flag Already Delete", ColBindingName = "DeletedAlreadyFlag", ColType = "Button", ColumnWidth = "150", ContentName = "None", BackgroundColor = "#583d72", CommandParam = "GoToHistory_Review" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggestion Delete", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Delete", BackgroundColor = "#c84b31", CommandParam = "DeleteSuggestion_Review" });

            return listColumnQsidDetail;
        }

        private List<CommonDataGridColumn> GetListGridColumn_Nested(bool IsActiveCustomCol)
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = IsActiveCustomCol, ColName = "Nesting Group No", ColBindingName = "NestingGroupNo", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Nesting Level", ColBindingName = "NestingLevel", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = IsActiveCustomCol, ColName = "Child Name", ColBindingName = "ChildName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exclude", ColBindingName = "IsExclude", ColType = "Textbox" });

            if (IsActiveCustomCol)
            {
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Date Added", ColBindingName = "DateAdded", ColType = "Textbox" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add Suggestion", ColBindingName = "ParentCAS", ColType = "Button", ColumnWidth = "150", ContentName = "Add", BackgroundColor = "#79d70f", CommandParam = "AddSuggestion_Nested", CustomStyleClass = "CustomAddRemoveButton" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Delete Suggestion", ColBindingName = "ParentCAS", ColType = "Button", ColumnWidth = "150", ContentName = "Delete", BackgroundColor = "#e8505b", CommandParam = "AddDeleteSuggestion_Nested", CustomStyleClass = "CustomAddRemoveButton" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Show", BackgroundColor = "#931a25", CommandParam = "ShowComments" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approve", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#335d2d", CommandParam = "ApproveGenericSuggestion_Nested", CustomStyleClass = "CustomApproveRejectButton" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Reject", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "100", ContentName = "Reject", BackgroundColor = "#fe7171", CommandParam = "RejectGenericSuggestion_Nested", CustomStyleClass = "CustomApproveRejectButton" });
                listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "ParentCAS", ColType = "Button", ColumnWidth = "150", ContentName = "Go To Tree View", BackgroundColor = "#ffc93c", CommandParam = "GoToTreeView_Nested" });
            }
            return listColumnQsidDetail;
        }
        private void NotifyMe(PropertyChangedMessage<GenericSuggestion_Add_VM> obj)
        {
            SelectedGenericSuggestionVM = obj.NewValue;
        }

        private List<CommonDataGridColumn> GetListGridColumnRejectSuggestion()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Reason for rejection", ColBindingName = "ReasonRejected", ColType = "Textbox" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "RowID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteGenericSuggestionImport" });
            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetListGridColumn()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Generic Category", ColBindingName = "ParentMapGenericIDs.GenericsCategoryName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Substance Category", ColBindingName = "ParentMapGenericIDs.SubstanceCategoryName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Substance Type", ColBindingName = "ParentMapGenericIDs.SubstanceTypeName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Generic Category", ColBindingName = "ChildMapGenericIDs.GenericsCategoryName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Substance Category", ColBindingName = "ChildMapGenericIDs.SubstanceCategoryName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Substance Type", ColBindingName = "ChildMapGenericIDs.SubstanceTypeName", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Comments", ColBindingName = "ParentComment", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Child Comments", ColBindingName = "Note", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exclude", ColBindingName = "IsExclude", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Cas InValid", ColBindingName = "ParentCasIsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Cas InValid", ColBindingName = "ChildCasIsValid", ColType = "Checkbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "User ID", ColBindingName = "UserID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exist In Generics", ColBindingName = "IsExistInGeneric", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "RowID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "UpdateNameSuggestionImport" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Comment", ColBindingName = "RowID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#931a25", CommandParam = "UpdateCommentSuggestionImport" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "RowID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteGenericSuggestionImport" });
            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetListGridColumn_Flat()
        {
            List<CommonDataGridColumn> listColumnFlatGenerics = new List<CommonDataGridColumn>();
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnFlatGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnFlatGenerics;
        }
        private List<CommonDataGridColumn> GetListGridColumn_CombinedGenerics()
        {
            List<CommonDataGridColumn> listColumnCombinedGenerics = new List<CommonDataGridColumn>();
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnCombinedGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox", ColumnWidth = "150" });
            return listColumnCombinedGenerics;
        }
        private List<CommonDataGridColumn> GetListGridColumn_Reject()
        {
            List<CommonDataGridColumn> listColumnRejectGenerics = new List<CommonDataGridColumn>();
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChildName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "Action", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exclude", ColBindingName = "IsExclude", ColType = "Checkbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested By", ColBindingName = "SuggestedUserName", ColType = "Combobox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Reviewed By", ColBindingName = "UserName", ColType = "Combobox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested On", ColBindingName = "SuggestedDateAdded", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Reviewed On", ColBindingName = "DateAdded", ColType = "Textbox" });
            listColumnRejectGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Show", BackgroundColor = "#931a25", CommandParam = "ShowComments" });

            return listColumnRejectGenerics;
        }
        private List<CommonDataGridColumn> GetListGridColumn_Delete()
        {
            List<CommonDataGridColumn> listColumnDeleteGenerics = new List<CommonDataGridColumn>();
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Parent CAS ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "Child CAS ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChildName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Exclude", ColBindingName = "IsExclude", ColType = "Checkbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested By", ColBindingName = "SuggestedUserName", ColType = "Combobox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approved By", ColBindingName = "UserName", ColType = "Combobox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Suggested On", ColBindingName = "SuggestedDateAdded", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approved On", ColBindingName = "DateAdded", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Show", BackgroundColor = "#931a25", CommandParam = "ShowComments" });
            return listColumnDeleteGenerics;
        }
        private List<CommonDataGridColumn> GetListGridColumn_UniqueRef()
        {
            List<CommonDataGridColumn> listColumnDeleteGenerics = new List<CommonDataGridColumn>();
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "CAS 1 ID", ColBindingName = "ParentCasID", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS 1", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = false, ColName = "CAS 2 ID", ColBindingName = "ChildCasID", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS 2", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Name 1", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas Name 2", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });

            listColumnDeleteGenerics.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "GenericSuggestion_ID", ColType = "Button", ColumnWidth = "150", ContentName = "Go To Tree View", BackgroundColor = "#931a25", CommandParam = "GoToTreeView" });
            return listColumnDeleteGenerics;
        }

        public Visibility contentVisibilityAddEditName_Add { get; set; } = Visibility.Collapsed;
        public Visibility contentVisibiltyComment_Add { get; set; } = Visibility.Collapsed;
        private void FilterChemicalName_Parent(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Parent = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Parent");
                    if (listAllChemicalName_Parent.Count == 0)
                    {
                        ListChemNameParentVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameParentVisibility");
                    }
                });
            }
            else
            {
                listAllChemicalName_Parent = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllChemicalName_Parent");
                ListChemNameParentVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemNameParentVisibility");
            }


        }
        private void FilterChemicalName_Child(string cas)
        {

            if (!string.IsNullOrEmpty(cas))
            {
                Task.Run(() =>
                {
                    listAllChemicalName_Child = new ObservableCollection<GenericChemicalName_VM>(_objGeneric_Service.FilterChemicalNameForGeneric(cas.ToLower()));
                    NotifyPropertyChanged("listAllChemicalName_Child");
                    if (listAllChemicalName_Child.Count == 0)
                    {
                        ListChemNameChildVisibility = Visibility.Collapsed;
                        NotifyPropertyChanged("ListChemNameChildVisibility");
                    }
                });
            }
            else
            {
                listAllChemicalName_Child = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllChemicalName_Child");
                ListChemNameChildVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemNameChildVisibility");
            }


        }
        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteGenericSuggestionImport")
            {
                var result = MessageBox.Show("Are you sure do you want to delete this Generic Suggestion?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {

                    var itemDel = ListGenericSuggestion.Where(x => x.ParentCas == SelectedGenericSuggestionVM.ParentCas && x.ChildCas == SelectedGenericSuggestionVM.ChildCas).FirstOrDefault();
                    ListGenericSuggestion.Remove(itemDel);
                    BindGenericSuggestion();
                }
            }
            else if (obj.Notification == "UpdateNameSuggestionImport")
            {
                AddEditChemicalName_Add();

            }
            else if (obj.Notification == "UpdateCommentSuggestionImport")
            {
                AddCommentInSuggestion();
            }
            else if (obj.Notification == "ApproveGenericSuggestionReview")
            {
                ApproveSuggestion_Review();
            }
            else if (obj.Notification == "RejectGenericSuggestionReview")
            {
                RejectSuggestion_Review();
            }
            else if (obj.Notification == "AddEditName_Review")
            {
                AddEditChemicalName_Review();
            }
            else if (obj.Notification == "GoToTreeView_Review")
            {
                SwitchTabFindCurrentGenericSuggestion();
            }
            else if (obj.Notification == "ShowComments")
            {
                BindListItemComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID, SelectedGenericSuggestionReviewVM.ChildCasID);
                BindListItemParentComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID);
                ResetContentVisibility_Review();
                contentVisibilityListComments_Review = Visibility.Visible;
                NotifyPropertyChanged("contentVisibilityListComments_Review");
                IsShowPopUpReviewGeneric = true;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                CommentGenericSuggestionAction_Review = string.Empty;
                NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
                TitleCommentHeader_Review = "Generic Comments for Parent = '" + SelectedGenericSuggestionReviewVM.ParentCas + "' and Child = '" + SelectedGenericSuggestionReviewVM.ChildCas + "' :";
                NotifyPropertyChanged("TitleCommentHeader_Review");
            }
            else if (obj.Notification == "GoToHistory_Review")
            {
                SwitchTabFindDeleted();
            }
            else if (obj.Notification == "DeleteSuggestion_Review")
            {
                if (SelectedGenericSuggestionReviewVM.UserID.ToLower() == Environment.UserName.ToLower())
                {
                    DeleteGenericSuggestionOnly();
                }
                else
                {
                    _notifier.ShowWarning("You are not authorize to perform this action!");
                }
            }
        }
        private void CommandButtonExecuted_Review(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteGenericSuggestionImport")
            {
                var result = MessageBox.Show("Are you sure do you want to delete this Generic Suggestion?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {

                    var itemDel = ListGenericSuggestion.Where(x => x.ParentCas == SelectedGenericSuggestionVM.ParentCas && x.ChildCas == SelectedGenericSuggestionVM.ChildCas).FirstOrDefault();
                    ListGenericSuggestion.Remove(itemDel);
                    BindGenericSuggestion();
                }
            }
            else if (obj.Notification == "UpdateNameSuggestionImport")
            {
                AddEditChemicalName_Add();

            }
            else if (obj.Notification == "UpdateCommentSuggestionImport")
            {
                AddCommentInSuggestion();
            }
            else if (obj.Notification == "ApproveGenericSuggestionReview")
            {
                ApproveSuggestion_Review();
            }
            else if (obj.Notification == "RejectGenericSuggestionReview")
            {
                RejectSuggestion_Review();
            }
            else if (obj.Notification == "AddEditName_Review")
            {
                AddEditChemicalName_Review();
            }
            else if (obj.Notification == "GoToTreeView_Review")
            {
                SwitchTabFindCurrentGenericSuggestion();
            }
            else if (obj.Notification == "ShowComments")
            {
                BindListItemComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID, SelectedGenericSuggestionReviewVM.ChildCasID);
                BindListItemParentComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID);
                ResetContentVisibility_Review();
                contentVisibilityListComments_Review = Visibility.Visible;
                NotifyPropertyChanged("contentVisibilityListComments_Review");
                IsShowPopUpReviewGeneric = true;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                CommentGenericSuggestionAction_Review = string.Empty;
                NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
                TitleCommentHeader_Review = "Generic Comments for Parent = '" + SelectedGenericSuggestionReviewVM.ParentCas + "' and Child = '" + SelectedGenericSuggestionReviewVM.ChildCas + "' :";
                NotifyPropertyChanged("TitleCommentHeader_Review");
            }
            else if (obj.Notification == "GoToHistory_Review")
            {
                SwitchTabFindDeleted();
            }
            else if (obj.Notification == "DeleteSuggestion_Review")
            {
                if (SelectedGenericSuggestionReviewVM.UserID.ToLower() == Environment.UserName.ToLower())
                {
                    DeleteGenericSuggestionOnly();
                }
                else
                {
                    _notifier.ShowWarning("You are not authorize to perform this action!");
                }
            }
        }
        public void DeleteGenericSuggestionOnly()
        {
            _objLibraryFunction_Service.DeleteGenericSuggestion((int)SelectedGenericSuggestionReviewVM.ParentCasID, (int)SelectedGenericSuggestionReviewVM.ChildCasID);
        }
        public void BindGenericSuggestion()
        {
            var listSuggestion_Add = new List<GenericSuggestion_Add_VM>();
            var listSuggestion_Reject = new List<GenericSuggestion_Reject_VM>();
            foreach (var item in ListGenericSuggestion)
            {
                if (!string.IsNullOrEmpty(item.ParentCas) && !string.IsNullOrEmpty(item.ChildCas) && string.IsNullOrEmpty(item.Note))
                {
                    listSuggestion_Reject.Add(CovertSuggestionToRejectList(item, "Parent and Child comment is mandatory!"));
                    continue;
                }
                if (!string.IsNullOrEmpty(item.ParentCas) && string.IsNullOrEmpty(item.ChildCas) && string.IsNullOrEmpty(item.ParentComment))
                {
                    listSuggestion_Reject.Add(CovertSuggestionToRejectList(item, "Parent comment is mandatory!"));
                    continue;
                }

                var response = _objLibraryFunction_Service.ValidateAddSuggestionImportFile(item.ParentCas, item.ChildCas);
                if (response.Type == "Success")
                {
                    listSuggestion_Add.Add(CovertSuggestionToAddList(item));
                }
                else
                {
                    listSuggestion_Reject.Add(CovertSuggestionToRejectList(item, response.Description));
                }
            }
            if (listSuggestion_Reject.Count > 0)
            {
                _notifier.ShowInformation("System reject " + listSuggestion_Reject.Count + " suggestions. See Reject Suggestion tab for detail.");
            }


            observableCollectionGenericSuggestion = listSuggestion_Add;

            ListRejectGenericSuggestion = listSuggestion_Reject;

        }
        public GenericSuggestion_Add_VM CovertSuggestionToAddList(GenericSuggestionVM item)
        {
            var objAddSuggestion = new GenericSuggestion_Add_VM()
            {
                ParentCas = item.ParentCas,
                ChildCas = item.ChildCas,
                ParentChemicalName = item.ParentChemicalName,
                ChemicalName = item.ChemicalName,
                Note = item.Note,
                ParentComment = item.ParentComment,
                IsExclude = item.IsExclude == true ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                RowID = item.RowID,
                Timestamp = Convert.ToDateTime(item.Timestamp),
                UserID = item.UserID,
                Status = item.Status,
                ParentMapGenericIDs = new Map_Generic_Detail_VM()
                {
                    GenericsCategoryName = item.ParentMapGenericIDs.GenericsCategoryName,
                    SubstanceCategoryName = item.ParentMapGenericIDs.SubstanceCategoryName,
                    SubstanceTypeName = item.ParentMapGenericIDs.SubstanceTypeName,
                },
                ChildMapGenericIDs = new Map_Generic_Detail_VM()
                {
                    GenericsCategoryName = item.ChildMapGenericIDs.GenericsCategoryName,
                    SubstanceCategoryName = item.ChildMapGenericIDs.SubstanceCategoryName,
                    SubstanceTypeName = item.ChildMapGenericIDs.SubstanceTypeName,
                },
                ParentCasIsValid = CheckCasInValid(item.ParentCas) == false ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                ChildCasIsValid = CheckCasInValid(item.ChildCas) == false ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault())
                        ,
                IsExistInGeneric = _objLibraryFunction_Service.ValidateRelationIfExistInFlat(item.ParentCas, item.ChildCas) ? true : (bool?)null

            };
            return objAddSuggestion;
        }
        public GenericSuggestion_Reject_VM CovertSuggestionToRejectList(GenericSuggestionVM item, string reason)
        {
            var objAddSuggestion = new GenericSuggestion_Reject_VM()
            {
                ParentCas = item.ParentCas,
                ChildCas = item.ChildCas,
                ParentChemicalName = item.ParentChemicalName,
                ChemicalName = item.ChemicalName,
                ReasonRejected = reason
            };
            return objAddSuggestion;
        }
        public void BindListItemNodeType()
        {
            var listItemNode = new List<SelectListItem>();
            listItemNode.Add(new SelectListItem { Text = "Nested" });
            listItemNode.Add(new SelectListItem { Text = "Separate" });
            ListItemNodeType = new ObservableCollection<SelectListItem>(listItemNode);
            SelectedItemNodeType = ListItemNodeType.Where(x => x.Text == "Nested").First();
            NotifyPropertyChanged("ListItemNodeType");
            NotifyPropertyChanged("SelectedItemNodeType");
        }
        public void BindListItemSearchType()
        {
            var listItemNode = new List<SelectListItem>();
            listItemNode.Add(new SelectListItem { Text = "Only First Level Parent" });
            listItemNode.Add(new SelectListItem { Text = "Only Child" });
            listItemNode.Add(new SelectListItem { Text = "Review Generics Suggestion" });
            listItemNode.Add(new SelectListItem { Text = "Both Child and Parent" });
          
            listItemSearchType = new ObservableCollection<SelectListItem>(listItemNode);
            SelectedItemSearchType = listItemSearchType.Where(x => x.Text == "Both Child and Parent").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchType");
            NotifyPropertyChanged("SelectedItemSearchType");
        }
        public void BindListItemSearchFor()
        {
            var listItemSearch = new List<SelectListItem>();
            listItemSearch.Add(new SelectListItem { Text = "Cas" });
            listItemSearch.Add(new SelectListItem { Text = "Chemical Name" });
            listItemSearch.Add(new SelectListItem { Text = "CSCTicket Number" });
            listItemSearch.Add(new SelectListItem { Text = "ClosedGroup" });
            listItemSearchFor = new ObservableCollection<SelectListItem>(listItemSearch);
            SelectedItemSearchFor = listItemSearchFor.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchFor");
            NotifyPropertyChanged("SelectedItemSearchFor");
        }
        public void BindListItemMatchCondition()
        {
            var listItemMatchCond = new List<SelectListItem>();
            listItemMatchCond.Add(new SelectListItem { Text = "Partial Match" });
            listItemMatchCond.Add(new SelectListItem { Text = "Exact Match" });
            listItemMatchCond.Add(new SelectListItem { Text = "Begin with" });
            listItemMatchCond.Add(new SelectListItem { Text = "End with" });
            ListItemMatchCondition = new ObservableCollection<SelectListItem>(listItemMatchCond);
            SelectedItemMatchCondition = ListItemMatchCondition.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("ListItemMatchCondition");
            NotifyPropertyChanged("SelectedItemMatchCondition");
        }
        public void BindSubstanceType()
        {
            listSubstanceType = new ObservableCollection<Library_SubstanceType>(_objGeneric_Service.GetAllSubstanceType());
            NotifyPropertyChanged("listSubstanceType");
            listChildSubstanceType = new ObservableCollection<Library_SubstanceType>(_objGeneric_Service.GetAllSubstanceType());
            NotifyPropertyChanged("listChildSubstanceType");
        }
        public void BindSubstanceCategories()
        {
            listSubstanceCategory = new ObservableCollection<Library_SubstanceCategories>(_objGeneric_Service.GetAllSubstanceCategories());
            NotifyPropertyChanged("listSubstanceCategory");
        }
        public void BindGenericsCategories()
        {
            listGenericsCategory = new ObservableCollection<Library_GenericsCategories>(_objGeneric_Service.GetAllGenericsCategories());
            NotifyPropertyChanged("listGenericsCategory");
        }
        public void SwitchTabFindDeleted()
        {
            IsSelectedDeletedGeneTab = true;
            NotifyPropertyChanged("IsSelectedDeletedGeneTab");
            casSearch_Delete = SelectedGenericSuggestionReviewVM.ParentCas;
            SelectItem_Deleted = listItemSearchType_Deleted.Where(x => x.Text == "Only Parent").FirstOrDefault();
        }

        #region Tab Add Generic Suggestion
        public void ResetContentVisibility_AddSuggestionTab()
        {
            contentImportVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentImportVisibility");
            contentAddVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentAddVisibility");
            contentVisibilityAddEditName_Add = Visibility.Collapsed;
            NotifyPropertyChanged("contentVisibilityAddEditName_Add");
            contentVisibiltyComment_Add = Visibility.Collapsed;
            NotifyPropertyChanged("contentVisibiltyComment_Add");
        }
        private void AddEditChemicalName_Add()
        {
            ResetContentVisibility_AddSuggestionTab();
            IsShowPopUp = true;
            contentVisibilityAddEditName_Add = Visibility.Visible;

            ParentCas = SelectedGenericSuggestionVM.ParentCas;
            NotifyPropertyChanged("ParentCas");
            ParentName = SelectedGenericSuggestionVM.ParentChemicalName;
            ChildCas = SelectedGenericSuggestionVM.ChildCas;
            NotifyPropertyChanged("ChildCas");
            ChildName = SelectedGenericSuggestionVM.ChemicalName;
            ListChemNameParentVisibility = Visibility.Collapsed;
            ListChemNameChildVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("ListChemNameParentVisibility");
            NotifyPropertyChanged("ListChemNameChildVisibility");
            NotifyPropertyChanged("contentVisibilityAddEditName_Add");
            NotifyPropertyChanged("IsShowPopUp");

        }
        private void AddCommentInSuggestion()
        {
            ResetContentVisibility_AddSuggestionTab();
            IsShowPopUp = true;
            CommentGenericSuggestionAction = SelectedGenericSuggestionVM.Note;
            CommentSuggestionParent = SelectedGenericSuggestionVM.ParentComment;
            contentVisibiltyComment_Add = Visibility.Visible;
            NotifyPropertyChanged("contentVisibiltyComment_Add");
            NotifyPropertyChanged("IsShowPopUp");
        }
        private void CommandShowPopUpAddGenericExecute(object obj)
        {
            ClearVariable();
            TreeViewItem_Generic_Backend = null;
            ResetContentVisibility_AddSuggestionTab();
            IsShowPopUp = true;
            contentAddVisibility = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUp");
            NotifyPropertyChanged("contentAddVisibility");
        }
        private bool CommandAddGenericSuggestionCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(ParentCas) || string.IsNullOrEmpty(ChildCas))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void CommandAddGenericSuggestionExecute(object obj)
        {
            if (ValidationAddSuggestion(true, true))
            {
                return;
            }
            AddGenericSuggestion();
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }
        private void CommandAddMultipleGenericSuggestionExecute(object obj)
        {
            if (ValidationAddSuggestion(true, true))
            {
                return;
            }
            AddGenericSuggestion();
            ClearVariable();
        }
        private void CommandShowImportBulkPopUpExecute(object obj)
        {
            ResetContentVisibility_AddSuggestionTab();
            IsShowPopUp = true;
            contentImportVisibility = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUp");
            NotifyPropertyChanged("contentImportVisibility");
        }
        private void CommandImportBulkSuggestionExecute(object obj)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                for (int count = 0; count < SelectedPath.Count(); count++)
                {
                    if (!Common.CommonGeneralFunction.IsFileLocked(SelectedPath[0]))
                    {
                        ImportBulkGenericSuggestion(SelectedPath[0]);
                        BindGenericSuggestion();
                    }
                    else
                    {
                        _notifier.ShowError("File is already in use, kindly close it.");
                    }
                }
            }
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }
        public void ImportBulkGenericSuggestion(string path)
        {
            DataTable dt = new DataTable();
            if (IsImportExcel)
            {
                dt = Common.CommonGeneralFunction.ImportBulkExcel(path);

            }
            if (IsImportAccess)
            {
                dt = _objGeneric_Service.GetListGenericSuggesionImport_Access(path);
            }
            ListGenericSuggestion = BindListSuggestionFromImportData(dt);
        }
        public List<GenericSuggestionVM> BindListSuggestionFromImportData(DataTable dt)
        {
            List<GenericSuggestionVM> listSuggestionImport = new List<GenericSuggestionVM>();
            if (dt.Rows.Count > 0)
            {
                foreach (var dtRow in dt.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace(x.Field<string>("ParentCas")) || !string.IsNullOrWhiteSpace(x.Field<string>("ChildCas"))).ToList())
                {
                    if (!listSuggestionImport.Any(x => x.ParentCas == dtRow.Field<string>("ParentCas") && x.ChildCas == dtRow.Field<string>("ChildCas")))
                    {
                        listSuggestionImport.Add(new GenericSuggestionVM
                        {
                            ParentCas = dtRow.Field<string>("ParentCas"),
                            ChildCas = dtRow.Field<string>("ChildCas"),
                            ParentChemicalName = dtRow.Field<string>("ParentName"),
                            ChemicalName = dtRow.Field<string>("ChildName"),
                            Note = dtRow.Field<string>("ParentChildComment"),
                            ParentComment = dtRow.Field<string>("ParentComment"),
                            IsExclude = IsImportAccess ? (!string.IsNullOrEmpty(dtRow.Field<string>("Exclude")) && dtRow.Field<string>("Exclude").ToLower() == "-1" ? true : false) : (!string.IsNullOrEmpty(dtRow.Field<string>("IsExclude")) && dtRow.Field<string>("IsExclude").ToLower() == "1" ? true : false),
                            RowID = Guid.NewGuid().ToString(),
                            Timestamp = DateTime.Now,
                            UserID = Engine.CurrentUserName,
                            Status = "Add",
                            ParentMapGenericIDs = FetchSubstanceInfo(dtRow.Field<string>("ParentSubstanceType"), dtRow.Field<string>("ParentSubstanceCategory"), dtRow.Field<string>("ParentGenericCategory")),
                            ChildMapGenericIDs = FetchSubstanceInfo(dtRow.Field<string>("ChildSubstanceType"), dtRow.Field<string>("ChildSubstanceCategory"), dtRow.Field<string>("ChildGenericCategory")),
                            //ParentCasIsValid = _objGeneric_Service.CheckCasValid(dtRow.Field<string>("ParentCas")),
                            //ChildCasIsValid = _objGeneric_Service.CheckCasValid(dtRow.Field<string>("ChildCas")),
                        });
                    }
                }
            }
            return listSuggestionImport;
        }
        public Map_Generic_Detail FetchSubstanceInfo(string substanceType, string substanceCategory, string genericCategory)
        {
            Map_Generic_Detail objMapGenericDetail = new Map_Generic_Detail();
            if (!string.IsNullOrEmpty(substanceType))
            {
                var objSelSubType = listSubstanceType.Where(x => x.SubstanceTypeName.ToLower() == substanceType.ToLower()).FirstOrDefault();
                if (objSelSubType != null)
                {
                    objMapGenericDetail.SubstanceTypeName = objSelSubType.SubstanceTypeName;
                    objMapGenericDetail.SubstanceTypeID = objSelSubType.SubstanceTypeID;
                }
            }
            if (!string.IsNullOrEmpty(substanceCategory))
            {
                var objSelSubCate = listSubstanceCategory.Where(x => x.SubstanceCategoryName.ToLower() == substanceCategory.ToLower()).FirstOrDefault();
                if (objSelSubCate != null)
                {
                    objMapGenericDetail.SubstanceCategoryName = objSelSubCate.SubstanceCategoryName;
                    objMapGenericDetail.SubstanceCategoryID = objSelSubCate.SubstanceCategoryID;
                }
            }
            if (!string.IsNullOrEmpty(genericCategory))
            {
                var objSelGenericCate = listGenericsCategory.Where(x => x.GenericsCategoryName.ToLower() == genericCategory.ToLower()).FirstOrDefault();
                if (objSelGenericCate != null)
                {
                    objMapGenericDetail.GenericsCategoryName = objSelGenericCate.GenericsCategoryName;
                    objMapGenericDetail.GenericsCategoryID = objSelGenericCate.GenericsCategoryID;
                }
            }
            return objMapGenericDetail;
        }
        private bool CommandSaveGenericSuggestionCanExecute(object obj)
        {
            if (ListGenericSuggestion != null && ListGenericSuggestion.Count > 0 && !IsInProgressBulkSuggestionAdd)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandSaveGenericSuggestionExecute(object obj)
        {
            IsInProgressBulkSuggestionAdd = true;
            GenericSuggestionGridLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("GenericSuggestionGridLoaderVisibility");
            Task.Run(() =>
            {
                InsertAddChemicalNameBulkData();
                ListGenericSuggestion.ForEach(x => { x.UserID = Environment.UserName; x.NodeType = "Nested"; });
                var dicOutRes = _objGeneric_Service.InsertGenericSuggestion(ListGenericSuggestion, Engine.CurrentUserSessionID, "AddGenericSuggestion");
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (dicOutRes.Any(x => x.Type == "Error"))
                    {
                        foreach (var item in dicOutRes.Where(x => x.Type == "Error").ToList())
                        {
                            _notifier.ShowError("Error: " + item.Description);
                        }
                    }
                    else
                    {
                        _notifier.ShowSuccess("Generic Suggestion saved successfully");
                    }
                    foreach (var item in dicOutRes.Where(x => x.Type == "Warning").ToList())
                    {
                        _notifier.ShowWarning("Warning: " + item.Description);
                    }
                    ListGenericSuggestion = new List<GenericSuggestionVM>();
                    observableCollectionGenericSuggestion = new List<GenericSuggestion_Add_VM>(); ;
                    BindGenericSuggestionForReviewGrid();

                    GenericSuggestionGridLoaderVisibility = Visibility.Collapsed;
                    NotifyPropertyChanged("GenericSuggestionGridLoaderVisibility");
                    IsInProgressBulkSuggestionAdd = false;
                });
            });
        }
        private void InsertAddChemicalNameBulkData()
        {

            foreach (var itemParent in ListGenericSuggestion.GroupBy(x => x.ParentChemicalName).Select(x => x.Key).ToList())
            {
                if (!string.IsNullOrEmpty(itemParent))
                {
                    int chemicalNameID = _objGeneric_Service.InsertIfNotExistChemicalName(itemParent);
                    ListGenericSuggestion.Where(x => x.ParentChemicalName == itemParent).ToList().ForEach(x => x.ParentChemicalNameID = chemicalNameID);
                }
            }
            foreach (var itemChemical in ListGenericSuggestion.GroupBy(x => x.ChemicalName).Select(x => x.Key).ToList())
            {
                if (!string.IsNullOrEmpty(itemChemical))
                {
                    int chemicalNameID = _objGeneric_Service.InsertIfNotExistChemicalName(itemChemical);
                    ListGenericSuggestion.Where(x => x.ChemicalName == itemChemical).ToList().ForEach(x => x.ChemicalNameID = chemicalNameID);
                }
            }
        }
        private void CommandDownloadTemplateFileExecute(object obj)
        {
            string filename = string.Empty;
            if ((string)obj == "Access")
            {
                filename = "GenericSuggestionFormat_Access.accdb";
            }
            else
            {
                filename = "GenericSuggestionFormat_Excel.xlsx";
            }
            string templatePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\FileTemplate\\" + filename;
            string destinationFile = Engine.CommonAccessExportFolderPath + "\\" + filename;
            if (File.Exists(destinationFile))
            {
                File.Delete(destinationFile);
            }
            File.Copy(templatePath, destinationFile);
            _notifier.ShowSuccess("Template File downloaded on below path." + Environment.NewLine + destinationFile);
        }
        private void CommandClearGenericSuggestionExecute(object obj)
        {
            ListGenericSuggestion = new List<GenericSuggestionVM>();
            BindGenericSuggestion();
        }

        #endregion

        //Review Generic Tab 
        #region Review Generic Suggestion
        public GenericSuggestionVM MapVMToBusinessVM_ReviewSuggestion(GenericSuggestion_Review_VM objReviewVM)
        {
            GenericSuggestionVM genericSuggestionVM = new GenericSuggestionVM();
            genericSuggestionVM.ChildCas = objReviewVM.ChildCas;
            genericSuggestionVM.Note = objReviewVM.Note;
            genericSuggestionVM.ParentCas = objReviewVM.ParentCas;
            genericSuggestionVM.ChildCasID = (int)objReviewVM.ChildCasID;
            genericSuggestionVM.ParentCasID = objReviewVM.ParentCasID;
            genericSuggestionVM.GenericSuggestion_ID = objReviewVM.GenericSuggestion_ID;
            genericSuggestionVM.IsExclude = objReviewVM.IsExclude.Value;
            genericSuggestionVM.UserID = objReviewVM.UserID;
            genericSuggestionVM.Status = objReviewVM.Status;
            return genericSuggestionVM;
        }

        public void BindGenericSuggestionForReviewGrid()
        {
            ReviewSuggestionGridLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("ReviewSuggestionGridLoaderVisibility");
            Task.Run(() =>
            {
                var listGeneric = _objGeneric_Service.GetListGenericSuggestionForConfirm();
                listGenericForConfirm = new ObservableCollection<GenericSuggestion_Review_VM>(listGeneric.Select(x => new GenericSuggestion_Review_VM
                {
                    GenericSuggestion_ID = x.GenericSuggestion_ID,
                    SelectedGenSuggestion = false,
                    ChildCas = x.ChildCas,
                    Note = x.Note,
                    UserID = x.UserID,
                    ParentCas = x.ParentCas,
                    ParentName = x.ParentChemicalName,
                    ChildName = x.ChemicalName,
                    ChildCasID = x.ChildCasID,
                    ParentCasID = x.ParentCasID,
                    IsExclude = x.IsExclude == true ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                    SuggestedBy = !string.IsNullOrEmpty(x.UserID) && Engine._listCheckedOutUser.Any(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))) ? Engine._listCheckedOutUser.Where(y => y.Id == Convert.ToInt32(x.UserID.ToLower().Replace("i", ""))).FirstOrDefault() : new ListCommonComboBox_ViewModel(),
                    Status = x.Status,
                    Parent_SubstanceInfo = new Map_Generic_Detail_VM()
                    {
                        GenericsCategoryName = x.ParentMapGenericIDs.GenericsCategoryName,
                        SubstanceCategoryName = x.ParentMapGenericIDs.SubstanceCategoryName,
                        SubstanceTypeName = x.ParentMapGenericIDs.SubstanceTypeName,
                    },
                    Child_SubstanceInfo = new Map_Generic_Detail_VM()
                    {
                        GenericsCategoryName = x.ChildMapGenericIDs.GenericsCategoryName,
                        SubstanceCategoryName = x.ChildMapGenericIDs.SubstanceCategoryName,
                        SubstanceTypeName = x.ChildMapGenericIDs.SubstanceTypeName,
                    },
                    ParentCasIsValid = CheckCasInValid(x.ParentCas) == false ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                    ChildCasIsValid = CheckCasInValid(x.ChildCas) == false ? (Engine._listCheckboxType.Where(y => y.Value == true).FirstOrDefault()) : (Engine._listCheckboxType.Where(y => y.Value == false).FirstOrDefault()),
                    DeletedAlreadyFlag = x.DeleteAlready == true ? "Yes" : "No",
                    DateSuggest = x.Timestamp

                }).ToList()); ;
                ReviewSuggestionGridLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ReviewSuggestionGridLoaderVisibility");
            });
        }

        public void BindListItemComments_Review(int? ParentCasID, int? ChildCasID)
        {
            listItemComments_Review = new ObservableCollection<GenericSuggestionComments_VM>(_objGeneric_Service.GetListGenericsComments(ParentCasID, ChildCasID));
            NotifyPropertyChanged("listItemComments_Review");

        }
        public void BindListItemParentComments_Review(int? ParentCasID)
        {
            Task.Run(() =>
            {
                var listParentComment = _objGeneric_Service.GetListGenericsParentComments(ParentCasID);
                if (listParentComment.Any(x => x.Additional_Generics_research_flag))
                {
                    IsGeneResearchChangeVisibility_Review = Visibility.Visible;
                }
                else
                {
                    IsGeneResearchChangeVisibility_Review = Visibility.Collapsed;
                }
                NotifyPropertyChanged("IsGeneResearchChangeVisibility_Review");
                listItemParentComments_Review = new ObservableCollection<GenericSuggestionComments_VM>(listParentComment);
                NotifyPropertyChanged("listItemParentComments_Review");
            });
        }
        private void RejectSuggestion_Review()
        {
            if (Engine.IsApproveRejectGenericSuggestion == true)
            {
                CommentGenericSuggestionAction = string.Empty;
                IsReject = true;
                IsApprove = false;
                IsShowPopUpReviewGeneric = true;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                ResetContentVisibility_Review();
                contentVisibiltyComment_Review = Visibility.Visible;
                NotifyPropertyChanged("contentVisibiltyComment_Review");
            }
            else
            {
                _notifier.ShowError("User not authorized to perform this action!");
            }
        }

        public void ResetContentVisibility_Review()
        {
            contentVisibilityAddEditName_Review = Visibility.Collapsed;
            contentVisibiltyComment_Review = Visibility.Collapsed;
            contentVisibilityListComments_Review = Visibility.Collapsed;
            contentVisibiltyCommentMultiSelect_Review = Visibility.Collapsed;
            ContentVisibilityLoadingMultiSelectReview = Visibility.Collapsed;
            NotifyPropertyChanged("contentVisibilityAddEditName_Review");
            NotifyPropertyChanged("contentVisibiltyComment_Review");
            NotifyPropertyChanged("contentVisibilityListComments_Review");
            NotifyPropertyChanged("contentVisibiltyCommentMultiSelect_Review");
            NotifyPropertyChanged("ContentVisibilityLoadingMultiSelectReview");
        }

        private void ApproveSuggestion_Review()
        {
            if (Engine.IsApproveRejectGenericSuggestion == true)
            {
                CommentGenericSuggestionAction = string.Empty;
                IsReject = false;
                IsApprove = true;
                IsShowPopUpReviewGeneric = true;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                ResetContentVisibility_Review();
                contentVisibiltyComment_Review = Visibility.Visible;
                NotifyPropertyChanged("contentVisibiltyComment_Review");
            }
            else
            {
                _notifier.ShowError("User not authorized to perform this action!");
            }
        }

        private void CommandSaveCommentGenericExecute(object obj)
        {
            if ((string)obj == "Review")
            {
                IsShowPopUpReviewGeneric = false;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                var objGenericItem = listGenericForConfirm.Where(x => x.GenericSuggestion_ID == SelectedGenericSuggestionReviewVM.GenericSuggestion_ID).FirstOrDefault();
                objGenericItem.Note = CommentGenericSuggestionAction;
                bool result = false;
                if (IsApprove)
                {
                    var objGenericSuggToApprove = MapVMToBusinessVM_ReviewSuggestion(objGenericItem);
                    result = _objGeneric_Service.ApproveGenericSuggestion(objGenericSuggToApprove, "ReviewTab");
                    var listFlatMaskVM = new List<Flat_Mask_VM>();
                    listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = objGenericSuggToApprove.ParentCasID != null ? (int)objGenericSuggToApprove.ParentCasID : 0, ChildCasId = objGenericSuggToApprove.ChildCasID, Type = objGenericSuggToApprove.Status });
                    UpdateGenericFlats(listFlatMaskVM);
                }
                else if (IsReject)
                {
                    result = _objGeneric_Service.RejectGenericSuggestion(MapVMToBusinessVM_ReviewSuggestion(objGenericItem), "ReviewTab");
                }
                if (result)
                {
                    _notifier.ShowSuccess("Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " successfully");
                }
                else
                {
                    _notifier.ShowError("Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " failed. Due to internal server error!");
                }
                BindGenericSuggestionForReviewGrid();

            }
            else if ((string)obj == "Add")
            {
                var listSuggestion = ListGenericSuggestion.Where(x => x.ParentCas == SelectedGenericSuggestionVM.ParentCas && x.ChildCas == SelectedGenericSuggestionVM.ChildCas).FirstOrDefault();
                listSuggestion.Note = CommentGenericSuggestionAction;
                listSuggestion.ParentComment = CommentSuggestionParent;
                CommentGenericSuggestionAction = string.Empty;
                CommentSuggestionParent = string.Empty;
                BindGenericSuggestion();
                contentVisibiltyComment_Add = Visibility.Collapsed;
                IsShowPopUp = false;
                NotifyPropertyChanged("contentVisibiltyComment_Add");
                NotifyPropertyChanged("IsShowPopUp");
            }
            else if ((string)obj == "Nested")
            {
                IsShowPopup_Nested = false;
                NotifyPropertyChanged("IsShowPopup_Nested");

                bool result = false;
                if (IsApprove)
                {
                    var objGenericSuggToApprove = MapVMToBusinessVM_Nested(SelectedNestedParent_VM);
                    result = _objGeneric_Service.ApproveGenericSuggestion(objGenericSuggToApprove, "NestedTab");
                    var listFlatMaskVM = new List<Flat_Mask_VM>();
                    listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = objGenericSuggToApprove.ParentCasID != null ? (int)objGenericSuggToApprove.ParentCasID : 0, ChildCasId = objGenericSuggToApprove.ChildCasID, Type = objGenericSuggToApprove.Status });
                    UpdateGenericFlats(listFlatMaskVM);
                }
                else if (IsReject)
                {
                    result = _objGeneric_Service.RejectGenericSuggestion(MapVMToBusinessVM_Nested(SelectedNestedParent_VM), "NestedTab");
                }
                if (result)
                {
                    _notifier.ShowSuccess("Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " successfully");
                }
                else
                {
                    _notifier.ShowError("Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " failed. Due to internal server error!");
                }
                CommandSearchCasNestedParentTab.Execute(obj);
                BindGenericSuggestionForReviewGrid();

            }
            CommentGenericSuggestionAction = string.Empty;
        }

        private void AddEditChemicalName_Review()
        {
            ResetContentVisibility_Review();
            ParentCas = SelectedGenericSuggestionReviewVM.ParentCas;
            NotifyPropertyChanged("ParentCas");
            ParentName = SelectedGenericSuggestionReviewVM.ParentName;
            ChildCas = SelectedGenericSuggestionReviewVM.ChildCas;
            NotifyPropertyChanged("ChildCas");
            ChildName = SelectedGenericSuggestionReviewVM.ChildName;
            ListChemNameParentVisibility = Visibility.Collapsed;
            ListChemNameChildVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("ListChemNameParentVisibility");
            NotifyPropertyChanged("ListChemNameChildVisibility");
            contentVisibilityAddEditName_Review = Visibility.Visible;
            NotifyPropertyChanged("contentVisibilityAddEditName_Review");
            IsShowPopUpReviewGeneric = true;
            NotifyPropertyChanged("IsShowPopUpReviewGeneric");

        }
        private void CommandSaveAddEditChemicalName_Review_Execute(object obj)
        {
            string type = (string)obj;
            if (type == "Review")
            {
                GenericParentChildCasName_VM objGenericSuggestion = new GenericParentChildCasName_VM();
                objGenericSuggestion.ParentName = ChildName;
                objGenericSuggestion.ParentNameID = ChildNameID;
                objGenericSuggestion.CasID = (int)SelectedGenericSuggestionReviewVM.ChildCasID;
                objGenericSuggestion.CasType = "Child";
                objGenericSuggestion.SessionID = Engine.CurrentUserSessionID;
                _objGeneric_Service.SaveParentChildCasName(objGenericSuggestion);

                objGenericSuggestion = new GenericParentChildCasName_VM();
                objGenericSuggestion.ParentName = ParentName;
                objGenericSuggestion.ParentNameID = ParentNameID;
                objGenericSuggestion.CasID = (int)SelectedGenericSuggestionReviewVM.ParentCasID;
                objGenericSuggestion.CasType = "Parent";
                objGenericSuggestion.SessionID = Engine.CurrentUserSessionID;
                _objGeneric_Service.SaveParentChildCasName(objGenericSuggestion);

                _notifier.ShowSuccess(@"Parent\Child updated succesfully");
                IsShowPopUpReviewGeneric = false;
                NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                BindGenericSuggestionForReviewGrid();
            }
            else if (type == "Add")
            {
                var listSuggestion = ListGenericSuggestion.Where(x => x.ParentCas == SelectedGenericSuggestionVM.ParentCas && x.ChildCas == SelectedGenericSuggestionVM.ChildCas).FirstOrDefault();
                listSuggestion.ParentChemicalName = ParentName;
                listSuggestion.ChemicalName = ChildName;
                BindGenericSuggestion();
                contentVisibilityAddEditName_Add = Visibility.Collapsed;
                IsShowPopUp = false;
                NotifyPropertyChanged("contentVisibilityAddEditName_Add");
                NotifyPropertyChanged("IsShowPopUp");
            }
        }
        public void SwitchTabFindCurrentGenericSuggestion()
        {
            IsSearchFromReviewTab = true;
            string casForSearch = string.Empty;
            casForSearch = SelectedGenericSuggestionReviewVM.ParentCas + "@" + SelectedGenericSuggestionReviewVM.ChildCas;
            string searchType = string.IsNullOrEmpty(SelectedGenericSuggestionReviewVM.ParentCas) || SelectedGenericSuggestionReviewVM.ParentCas == "0" ? "FilterReviewGenericSuggestion_OnlyTopParent" : "FilterReviewGenericSuggestion";
            SearchCasInTreeView(casForSearch, "Cas", searchType, "Exact Match");//FilterReviewGenericSuggestion
            IsSelectedTreeTab = true;
        }

        private bool CommandAddAdditionalReviewCommentCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction_Review))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandAddAdditionalReviewCommentExecute(object obj)
        {
            string type = (string)obj;
            GenericSuggestionVM objGenericComment = new GenericSuggestionVM();
            bool IsResultMoreRes = true;
            //objGenericComment.Note = CommentGenericSuggestionAction_Review;
            if (type == "ParentChild")
            {
                objGenericComment.ParentCasID = SelectedGenericSuggestionReviewVM.ParentCasID;
                objGenericComment.ChildCasID = SelectedGenericSuggestionReviewVM.ChildCasID ?? 0;
                objGenericComment.Note = CommentGenericSuggestionAction_Review;
                IsResultMoreRes = ValidateCasForNeedMoreResearch(IsNeedMoreResearch_Review, objGenericComment.ParentCasID);
            }
            else if (type == "Parent")
            {

                objGenericComment.ChildCasID = (int)SelectedGenericSuggestionReviewVM.ParentCasID;
                objGenericComment.ParentComment = CommentGenericSuggestionAction_Review;
                IsResultMoreRes = ValidateCasForNeedMoreResearch(IsNeedMoreResearch_Review, objGenericComment.ChildCasID);
            }
            if (!IsResultMoreRes)
            {
                return;
            }

            objGenericComment.UserID = Environment.UserName;
            objGenericComment.IsAdditionalGenericsResearchFlag = IsNeedMoreResearch_Review == true ? true : false;
            _objGeneric_Service.InsertAdditionalComment(objGenericComment, type);
            BindListItemComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID, SelectedGenericSuggestionReviewVM.ChildCasID);
            BindListItemParentComments_Review(SelectedGenericSuggestionReviewVM.ParentCasID);
            _notifier.ShowSuccess("Additonal Comment Added Successfully");
            CommentGenericSuggestionAction_Review = string.Empty;
            NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
            IsNeedMoreResearch_Review = false;
            NotifyPropertyChanged("IsNeedMoreResearch_Review");
        }
        public bool ValidateCasForNeedMoreResearch(bool? needMoreResearch, int? casID)
        {
            bool result = true;
            if (needMoreResearch == true && !CheckCasSubstanceIsGroup(casID))
            {
                _notifier.ShowError("Add research flag faild due to cas substance type is not 'Group'");
                result = false;
            }
            return result;
        }

        private bool CommandShowCommentForMultiSelectCanExecute(object obj)
        {
            bool result = false;
            if (listGenericForConfirm != null && listGenericForConfirm.Any(x => x.SelectedGenSuggestion) && Engine.IsApproveRejectGenericSuggestion == true && IsProgressMultiSelect_Enable_Review)
            {
                result = true;
            }
            return result;

        }
        private void CommandShowCommentForMultiSelectExecute(object obj)
        {
            string objActive = (string)obj;
            if (objActive == "Approve")
            {
                IsReject = false;
                IsApprove = true;
            }
            else
            {
                IsReject = true;
                IsApprove = false;
            }
            CommentGenericSuggestionAction = string.Empty;
            IsShowPopUpReviewGeneric = true;
            NotifyPropertyChanged("IsShowPopUpReviewGeneric");
            ResetContentVisibility_Review();
            contentVisibiltyCommentMultiSelect_Review = Visibility.Visible;
            NotifyPropertyChanged("contentVisibiltyCommentMultiSelect_Review");

        }
        private void CommandSaveCommentForMultiGenericExecute(object obj)
        {
            ResetContentVisibility_Review();
            IsProgressMultiSelect_Enable_Review = false;
            NotifyPropertyChanged("IsProgressMultiSelect_Enable_Review");
            //ReviewSuggestionGridLoaderVisibility = Visibility.Visible;
            //NotifyPropertyChanged("ReviewSuggestionGridLoaderVisibility");
            ContentVisibilityLoadingMultiSelectReview = Visibility.Visible;
            NotifyPropertyChanged("ContentVisibilityLoadingMultiSelectReview");
            IsShowPopUpReviewGeneric = true;
            NotifyPropertyChanged("IsShowPopUpReviewGeneric");
            List<Flat_Mask_VM> listGenericForFlat = new List<Flat_Mask_VM>();
            Task.Run(() =>
            {
                string infoMessageSuccess = string.Empty;
                string infoMessageError = string.Empty;
                var selectedListGenericSug_Review = listGenericForConfirm.Where(x => x.SelectedGenSuggestion == true).ToList();
                for (var count = 0; count < selectedListGenericSug_Review.Count; count++)
                {
                    try
                    {
                        //ContentVisibilityLoadingMultiSelectReview = Visibility.Visible;
                        //NotifyPropertyChanged("ContentVisibilityLoadingMultiSelectReview");
                        progressText = "processing " + (count + 1) + " of " + selectedListGenericSug_Review.Count;
                        NotifyPropertyChanged("progressText");
                        var objGenericItem = selectedListGenericSug_Review[count];
                        objGenericItem.Note = CommentGenericSuggestionAction;
                        bool result = false;
                        if (IsApprove)
                        {
                            result = _objGeneric_Service.ApproveGenericSuggestion_Bulk(MapVMToBusinessVM_ReviewSuggestion(objGenericItem), "ReviewTabBulk");
                        }
                        else if (IsReject)
                        {
                            result = _objGeneric_Service.RejectGenericSuggestion_Bulk(MapVMToBusinessVM_ReviewSuggestion(objGenericItem), "ReviewTabBulk");
                        }
                        if (result)
                        {
                            infoMessageSuccess += "Parent= " + objGenericItem.ParentCas + " & Child= " + objGenericItem.ChildCas + " Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " successfully. " + Environment.NewLine;
                        }
                        else
                        {
                            infoMessageError += "Parent= " + objGenericItem.ParentCas + " & Child= " + objGenericItem.ChildCas + " Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " failed. Due to internal server error! " + Environment.NewLine;
                        }
                        if (result && IsApprove)
                        {
                            listGenericForFlat.Add(new Flat_Mask_VM() { ParentCasId = objGenericItem.ParentCasID != null ? (int)objGenericItem.ParentCasID : 0, ChildCasId = (int)objGenericItem.ChildCasID });
                        }
                    }
                    catch (Exception x)
                    {
                        infoMessageError += "Parent= " + selectedListGenericSug_Review[count].ParentCas + " & Child= " + selectedListGenericSug_Review[count].ChildCas + " Generic Suggestion " + (IsApprove ? "approved" : "rejected") + " failed. " + x.Message + Environment.NewLine;
                    }
                }
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    ContentVisibilityLoadingMultiSelectReview = Visibility.Collapsed;
                    NotifyPropertyChanged("ContentVisibilityLoadingMultiSelectReview");
                    IsShowPopUpReviewGeneric = false;
                    NotifyPropertyChanged("IsShowPopUpReviewGeneric");
                    progressText = "";
                    NotifyPropertyChanged("progressText");
                    if (!string.IsNullOrEmpty(infoMessageSuccess))
                    {
                        _notifier.ShowSuccess(infoMessageSuccess);
                    }
                    if (!string.IsNullOrEmpty(infoMessageError))
                    {
                        _notifier.ShowError(infoMessageError);
                    }
                });
                CommentGenericSuggestionAction = string.Empty;
                IsProgressMultiSelect_Enable_Review = true;
                NotifyPropertyChanged("IsProgressMultiSelect_Enable_Review");
                BindGenericSuggestionForReviewGrid();
                UpdateGenericFlats(listGenericForFlat);


            });
        }

        public void UpdateGenericFlats(List<Flat_Mask_VM> listGenericForFlat)
        {
            _notifier.ShowInformation("Generate flats are working in background, please do not shutdown the application until complete!");
            Task.Run(() =>
            {
                _objLibraryFunction_Service.ApplyAddDeleteFlatGenerics_New(listGenericForFlat, Environment.UserName);
                //App.Current.Dispatcher.Invoke((Action)delegate
                //{                                       
                _notifier.ShowInformation("Generate flats are completed!");
                //});
            });

        }
        #endregion

        #region Tab Tree View

        #region Comon Function Tree View
        public void BindSortTypeListNodelevel()
        {
            var listItem = new List<SelectListItem>();
            listItem.Add(new SelectListItem { Text = "Cas" });
            listItem.Add(new SelectListItem { Text = "Chemical Name" });
            listItem.Add(new SelectListItem { Text = "Date Of Approved" });
            listSortFieldType = new ObservableCollection<SelectListItem>(listItem);
            NotifyPropertyChanged("listSortFieldType");

            var listItemSort = new List<SelectListItem>();
            listItemSort.Add(new SelectListItem { Text = "Ascending" });
            listItemSort.Add(new SelectListItem { Text = "Descending" });

            listSortType = new ObservableCollection<SelectListItem>(listItemSort);
            NotifyPropertyChanged("listSortType");

        }
        public void BindGenericSuggestionTreeView()
        {
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                ListGenericTree_Backend = GenericTreeOperation.GetGenericManagementTree((int?)null, (int?)null, "", "", "");
                ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend);
                NotifyPropertyChanged("ListGenericTree");
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
            });
        }
        public void BindListProduct()
        {
            listProductsToMap = new ObservableCollection<MapProductListVM>(Common.CommonGeneralFunction.ConvertMapProductToViewModel(_objGeneric_Service.GetAllLibraryProductLists()));
            NotifyPropertyChanged("listProductsToMap");
        }
        public void BindListItemComments()
        {
            listItemComments = new ObservableCollection<GenericSuggestionComments_VM>(_objGeneric_Service.GetListGenericsComments(SelectedTreeViewItem.ParentCasID ?? 0, SelectedTreeViewItem.ChildCasID));
            NotifyPropertyChanged("listItemComments");
        }
        public void BindListItemParentComments()
        {
            Task.Run(() =>
            {
                if (SelectedTreeViewItem != null)
                {
                    var listParentComment = _objGeneric_Service.GetListGenericsParentComments(SelectedTreeViewItem.ChildCasID);
                    if (listParentComment.Any(x => x.Additional_Generics_research_flag))
                    {
                        IsGeneResearchChangeVisibility = Visibility.Visible;
                    }
                    else
                    {
                        IsGeneResearchChangeVisibility = Visibility.Collapsed;
                    }
                    NotifyPropertyChanged("IsGeneResearchChangeVisibility");
                    listItemParentComments = new ObservableCollection<GenericSuggestionComments_VM>(listParentComment);
                    NotifyPropertyChanged("listItemParentComments");
                }
            });
        }
        public void ClearChildVariable()
        {
            ChildCas = string.Empty;
            ChildName = string.Empty;
            ChildNameID = 0;
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            NotifyPropertyChanged("ChildCas");
            IsExclude = false;
            NotifyPropertyChanged("IsExclude");
            IsNeedMoreResearch = false;
            NotifyPropertyChanged("IsNeedMoreResearch");
            IsNeedMoreResearchParentChild = false;
            NotifyPropertyChanged("IsNeedMoreResearchParentChild");
            selectedChildItemCategory = null;
            selectedChildSubstanceCategory = null;
            selectedChildSubstanceType = null;
            ChildCasInvalid = string.Empty;
            NotifyPropertyChanged("ChildCasInvalid");
            NotifyPropertyChanged("selectedChildItemCategory");
            NotifyPropertyChanged("selectedChildSubstanceCategory");
            NotifyPropertyChanged("selectedChildSubstanceType");
        }
        public void ClearParentVariable()
        {
            ParentCas = string.Empty;
            ParentName = string.Empty;
            ParentNameID = 0;
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            NotifyPropertyChanged("ParentCas");
            IsExclude = false;
            NotifyPropertyChanged("IsExclude");
            IsNeedMoreResearch = false;
            NotifyPropertyChanged("IsNeedMoreResearch");
            IsNeedMoreResearchParentChild = false;
            NotifyPropertyChanged("IsNeedMoreResearchParentChild");
            selectedParentItemCategory = null;
            selectedParentSubstanceCategory = null;
            selectedParentSubstanceType = null;
            ParentCasInvalid = string.Empty;

            NotifyPropertyChanged("ParentCasInvalid");

            NotifyPropertyChanged("selectedParentItemCategory");
            NotifyPropertyChanged("selectedParentSubstanceCategory");
            NotifyPropertyChanged("selectedParentSubstanceType");
        }
        public void ClearVariableAddSuggestion()
        {
            ClearParentVariable();
            ClearChildVariable();
        }
        public void ClearVariable()
        {
            ParentCas = string.Empty;
            ChildCas = string.Empty;
            SelectedItemStatus = null;
            ParentCasInvalid = string.Empty;
            ChildCasInvalid = string.Empty;
            NotifyPropertyChanged("ParentCasInvalid");
            NotifyPropertyChanged("ChildCasInvalid");
            NotifyPropertyChanged("ParentCas");
            NotifyPropertyChanged("ChildCas");
            NotifyPropertyChanged("SelectedItemStatus");
        }
        public void ResetVisibilityPopUpTreeView()
        {
            contentAddEditCasNameVisibility = Visibility.Collapsed;
            contentAddParentVisibility = Visibility.Collapsed;
            contentApproveVisibility = Visibility.Collapsed;
            contentRejectVisibility = Visibility.Collapsed;
            contentRemoveVisibility = Visibility.Collapsed;
            contentAddVisibility = Visibility.Collapsed;
            contentCommentVisibility = Visibility.Collapsed;
            contentAddMainParentVisibility = Visibility.Collapsed;
            contentNestedParentVisibility = Visibility.Collapsed;
            contentFilterSortVisibility = Visibility.Collapsed;
            contentDisableResearchCommentVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentAddEditCasNameVisibility");
            NotifyPropertyChanged("contentApproveVisibility");
            NotifyPropertyChanged("contentRejectVisibility");
            NotifyPropertyChanged("contentRemoveVisibility");
            NotifyPropertyChanged("contentAddVisibility");
            NotifyPropertyChanged("contentCommentVisibility");
            NotifyPropertyChanged("contentAddParentVisibility");
            NotifyPropertyChanged("contentAddMainParentVisibility");
            NotifyPropertyChanged("contentNestedParentVisibility");
            NotifyPropertyChanged("contentFilterSortVisibility");
            NotifyPropertyChanged("contentDisableResearchCommentVisibility");

            objGenericSuggestionTree_VM = new GenericSuggestionVM();
        }
        public Map_Generic_Detail MapGenericCatIDsVMFromLocalVar(Library_GenericsCategories selectedGenericCate, Library_SubstanceType selectedSubType, Library_SubstanceCategories selectedSubCate)
        {
            Map_Generic_Detail objMapGeneric = new Map_Generic_Detail();
            objMapGeneric.GenericsCategoryID = selectedGenericCate != null ? selectedGenericCate.GenericsCategoryID : (int?)null;
            objMapGeneric.SubstanceTypeID = selectedSubType != null ? selectedSubType.SubstanceTypeID : (int?)null;
            objMapGeneric.SubstanceCategoryID = selectedSubCate != null ? selectedSubCate.SubstanceCategoryID : (int?)null;
            return objMapGeneric;
        }
        public GenericSuggestionVM MapSuggestionVMFromLocalVar(string _parentCas, string _childCas, string _parentName, string _childName, int _parentNameID, int _childNameID, string _nodetype, string _comment, bool? _isExclude, string _parentComment, bool? _isNeedMoreResearch)
        {
            GenericSuggestionVM objGenericSugg = new GenericSuggestionVM();
            objGenericSugg.ParentCas = _parentCas;
            objGenericSugg.ChildCas = _childCas;
            objGenericSugg.IsExclude = _isExclude;
            objGenericSugg.Status = "Add";
            objGenericSugg.ChemicalName = _childName;
            objGenericSugg.ChemicalNameID = _childNameID;
            objGenericSugg.ParentChemicalName = _parentName;
            objGenericSugg.ParentChemicalNameID = _parentNameID;
            objGenericSugg.NodeType = _nodetype;
            objGenericSugg.UserID = Environment.UserName;
            objGenericSugg.Timestamp = DateTime.Now;
            objGenericSugg.Note = _comment;
            objGenericSugg.ParentComment = _parentComment;
            objGenericSugg.IsAdditionalGenericsResearchFlag = _isNeedMoreResearch == true ? true : false;
            // objGenericSugg.GenericsCategoryID = objSelectCat != null ? objSelectCat.GenericsCategoryID : (int?)null;
            return objGenericSugg;
        }
        public GenericSuggestionVM ConvertViewModelToGenericSuggestionBusinessVM(GenericSuggestionTree_ViewModel objGSTree_VM)
        {
            GenericSuggestionVM objGenericBusinessVM = new GenericSuggestionVM();
            objGenericBusinessVM.ParentCasID = objGSTree_VM.ParentCasID ?? 0;
            objGenericBusinessVM.ChildCas = objGSTree_VM.ChildCas;
            objGenericBusinessVM.ChildCasID = objGSTree_VM.ChildCasID ?? 0;
            objGenericBusinessVM.ParentCas = objGSTree_VM.ParentCas;
            objGenericBusinessVM.Status = objGSTree_VM.Status;
            objGenericBusinessVM.UserID = Environment.UserName;
            objGenericBusinessVM.IsExclude = objGSTree_VM.IsExclude;
            //objGenericBusinessVM.NodeType = objGSTree_VM.;
            objGenericBusinessVM.Note = CommentGenericSuggestionAction;
            return objGenericBusinessVM;
        }
        #endregion

        #region Add new Generic Suggestion Tree View
        private void CommandShowPopAddGenericExecute(object obj)
        {
            ClearVariableAddSuggestion();
            ResetVisibilityPopUpTreeView();
            IsShowPopUpTreeView = true;
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            ParentCas = ((GenericTreeView)obj).ParentGeneric.ChildCas;
            NotifyPropertyChanged("ParentCas");
            FindChemNameAndSelectIfExist(((GenericManagementTool.ViewModels.GenericTreeView)obj).ParentCasNameID, "Parent");
            Task.Run(() =>
            {
                Map_SubstanceType_CategoryByCasID(ParentCas, "parent");
            });
            contentAddVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentAddVisibility");
            TitleSuggestionPopUpHeader = "Add New Child";
            IsParentSuggestionAction = false;
            IsChildSuggestionAction = true;
            IsMainSuggestionAction = false;
            NotifyPropertyChanged("IsParentSuggestionAction");
            NotifyPropertyChanged("IsChildSuggestionAction");
            NotifyPropertyChanged("IsMainSuggestionAction");
            //ListChemNameParentVisibility = Visibility.Collapsed;
            //NotifyPropertyChanged("ListChemNameParentVisibility");
        }
        private bool CommandAddGenericSuggestionInTreeViewCanExecute(object obj)
        {
            bool flag = true;
            if (IsMainSuggestionAction)
            {
                if (string.IsNullOrEmpty(ParentCas))
                {
                    flag = false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(ParentCas) || string.IsNullOrEmpty(ChildCas))
                {
                    flag = false;
                }
            }
            return flag;
        }
        private void CommandAddGenericSuggestionInTreeViewExecute(object obj)
        {


            if (IsMainSuggestionAction)
            {
                if (ValidationAddSuggestion(true, true))
                {
                    return;
                }
                AddGenericSuggestion();
            }
            else
            {
                if (IsChildSuggestionAction)
                {
                    if (ValidationAddSuggestion(false, true))
                    {
                        return;
                    }
                    AddGenericSuggestion();
                }
                else if (IsParentSuggestionAction)
                {
                    if (ValidationAddSuggestion(true, false))
                    {
                        return;
                    }
                    AddParentGenericSuggestion();
                }
            }
            BindGenericSuggestionForReviewGrid();
            IsShowPopUpTreeView = false;

        }
        public bool ValidationAddSuggestion(bool? IsParentCheck, bool? IsChildCheck)
        {
            bool result = false;
            if (IsParentCheck == true)
            {
                if (!string.IsNullOrEmpty(ParentCas) && (string.IsNullOrEmpty(ParentName) || string.IsNullOrWhiteSpace(ParentName)))
                {
                    result = true;
                    _notifier.ShowWarning("Parent Name is mandatory!");
                }
            }
            if (IsChildCheck == true)
            {
                if (!string.IsNullOrEmpty(ChildCas) && (string.IsNullOrEmpty(ChildName) || string.IsNullOrWhiteSpace(ChildName)))
                {
                    result = true;
                    _notifier.ShowWarning("Child Name is mandatory!");
                }
            }
            if(_objLibraryFunction_Service.GetGenericClosedGorup(ParentCas))
            {
                result = true;
                _notifier.ShowError("Error: This cas marked as closed group, child cas seems not to be added!");
            }
            if (ParentCas == ChildCas)
            {
                result = true;
                _notifier.ShowError("Error: ChildCAS cannot be same as ParentCAS!");
            }
            if (!string.IsNullOrEmpty(ParentCas) && !string.IsNullOrEmpty(ChildCas) && (string.IsNullOrEmpty(CommentGenericSuggestionAction) || string.IsNullOrWhiteSpace(CommentGenericSuggestionAction)))
            {
                result = true;
                _notifier.ShowError("Error: Parent Child Comment is mandatory!");
            }
            if (!string.IsNullOrEmpty(ParentCas) && string.IsNullOrEmpty(ChildCas) && (string.IsNullOrEmpty(CommentSuggestionParent) || string.IsNullOrWhiteSpace(CommentSuggestionParent)))
            {
                result = true;
                _notifier.ShowError("Error: Parent Comment is mandatory!");
            }
            return result;
        }
        public void AddGenericSuggestion()
        {
            List<GenericSuggestionVM> listGenericSuggestionVM = new List<GenericSuggestionVM>();
            GenericSuggestionVM objGenericSugg = MapSuggestionVMFromLocalVar(ParentCas, ChildCas, ParentName, ChildName, ParentNameID, ChildNameID, SelectedItemNodeType.Text, CommentGenericSuggestionAction, IsExclude, CommentSuggestionParent, IsNeedMoreResearch);
            objGenericSugg.ParentMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedParentItemCategory, selectedParentSubstanceType, selectedParentSubstanceCategory);
            objGenericSugg.ChildMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedChildItemCategory, selectedChildSubstanceType, selectedChildSubstanceCategory);
            listGenericSuggestionVM.Add(objGenericSugg);
            var dicOutRes = _objGeneric_Service.InsertGenericSuggestion(listGenericSuggestionVM, Engine.CurrentUserSessionID, "TreeView");
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            if (dicOutRes.Any(x => x.Type == "Error"))
            {
                foreach (var item in dicOutRes.Where(x => x.Type == "Error").ToList())
                {
                    _notifier.ShowError("Error: " + item.Description);
                }
            }
            else
            {
                if (TreeViewItem_Generic_Backend != null)
                {
                    RefreshTreeFromXPath(TreeViewItem_Generic_Backend.ParentGeneric.XPath + "/" + objGenericSugg.ChildCas);
                }
                _notifier.ShowSuccess("Generic Suggestion saved successfully");
                if (IsNeedAddCSCTickets_TreeView)       //call CSC Tickets pop if flag true in popup
                {
                    ShowPopUpCSCTicketsModel(ParentCas, ChildCas);
                }

            }
            foreach (var item in dicOutRes.Where(x => x.Type == "Warning").ToList())
            {
                _notifier.ShowWarning("Warning: " + item.Description);
            }
        }
        private void CommandAddMultipleGenericSuggestionInTreeViewExecute(object obj)
        {
            if (ValidationAddSuggestion(true, true))
            {
                return;
            }
            AddGenericSuggestion();
            ClearChildVariable();
            BindGenericSuggestionTreeView();
        }
        private void CommandShowPopAddParentGenericExecute(object obj)
        {
            ClearVariableAddSuggestion();
            ResetVisibilityPopUpTreeView();
            IsShowPopUpTreeView = true;
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            objGenericSuggestionTree_VM = ConvertViewModelToGenericSuggestionBusinessVM(((GenericManagementTool.ViewModels.GenericTreeView)obj).ParentGeneric);
            ChildCas = objGenericSuggestionTree_VM.ChildCas;

            NotifyPropertyChanged("ChildCas");
            FindChemNameAndSelectIfExist(((GenericManagementTool.ViewModels.GenericTreeView)obj).ParentCasNameID, "Child");
            Task.Run(() =>
            {
                Map_SubstanceType_CategoryByCasID(ChildCas, "child");
            });
            contentAddVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentAddVisibility");
            TitleSuggestionPopUpHeader = "Add New Parent";
            IsParentSuggestionAction = true;
            IsChildSuggestionAction = false;
            IsMainSuggestionAction = false;
            NotifyPropertyChanged("IsMainSuggestionAction");
            NotifyPropertyChanged("IsParentSuggestionAction");
            NotifyPropertyChanged("IsChildSuggestionAction");
        }
        private void AddParentGenericSuggestion()
        {
            List<GenericSuggestionVM> listGenericSuggestionVM = new List<GenericSuggestionVM>();

            GenericSuggestionVM objGenericSugg = MapSuggestionVMFromLocalVar(ParentCas, ChildCas, ParentName, ChildName, ParentNameID, ChildNameID, SelectedItemNodeType.Text, CommentGenericSuggestionAction, IsExclude, CommentSuggestionParent, IsNeedMoreResearch);
            objGenericSugg.ParentMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedParentItemCategory, selectedParentSubstanceType, selectedParentSubstanceCategory);
            objGenericSugg.ChildMapGenericIDs = MapGenericCatIDsVMFromLocalVar(selectedChildItemCategory, selectedChildSubstanceType, selectedChildSubstanceCategory);
            listGenericSuggestionVM.Add(objGenericSugg);
            GenericSuggestionVM objParentGenericSugg = MapSuggestionVMFromLocalVar("", ParentCas, "", "", 0, 0, SelectedItemNodeType.Text, "", false, "", false);
            objParentGenericSugg.ParentCasID = objGenericSuggestionTree_VM.ParentCasID;
            listGenericSuggestionVM.Add(objParentGenericSugg);
            var dicOutRes = _objGeneric_Service.InsertGenericSuggestion(listGenericSuggestionVM, Engine.CurrentUserSessionID, "TreeView");
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            if (dicOutRes.Any(x => x.Type == "Error"))
            {
                foreach (var item in dicOutRes.Where(x => x.Type == "Error").ToList())
                {
                    _notifier.ShowError("Error: " + item.Description);
                }
            }
            else
            {
                if (TreeViewItem_Generic_Backend != null)
                {
                    RefreshTreeFromXPath(TreeViewItem_Generic_Backend.ParentGeneric.XPath, true, ParentCas);
                    //TreeViewItem_Generic_Backend.IsExpanded = true;
                }
                _notifier.ShowSuccess("Generic Suggestion saved successfully");

            }
            foreach (var item in dicOutRes.Where(x => x.Type == "Warning").ToList())
            {
                _notifier.ShowWarning("Warning: " + item.Description);
            }
            BindGenericSuggestionForReviewGrid();
            IsShowPopUpTreeView = false;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            //  BindGenericSuggestionTreeView();
        }

        private void CommandShowPopUpAddMainParentExecute(object obj)
        {
            ClearVariableAddSuggestion();
            ResetVisibilityPopUpTreeView();
            IsMainSuggestionAction = true;
            IsParentSuggestionAction = true;
            IsChildSuggestionAction = true;
            TitleSuggestionPopUpHeader = "Add New Main Parent";
            NotifyPropertyChanged("IsMainSuggestionAction");
            NotifyPropertyChanged("IsParentSuggestionAction");
            NotifyPropertyChanged("IsChildSuggestionAction");
            contentAddVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentAddVisibility");
            IsShowPopUpTreeView = true;
        }
        #endregion

        #region Approve/Reject Tree View
        private void CommandShowPopRemoveGenericExecute(object obj)
        {
            var result = MessageBox.Show("Are you sure do you want to remove this node?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                TreeViewItem_Generic_Backend = (GenericTreeView)obj;
                CommentGenericSuggestionAction = string.Empty;
                ResetVisibilityPopUpTreeView();
                contentRemoveVisibility = Visibility.Visible;
                NotifyPropertyChanged("contentRemoveVisibility");
                IsShowPopUpTreeView = true;
                objGenericSuggestionTree_VM = ConvertViewModelToGenericSuggestionBusinessVM(((GenericTreeView)obj).ParentGeneric);
            }
        }
        private void CommandShowPopRejectGenericExecute(object obj)
        {
            CommentGenericSuggestionAction = string.Empty;
            ResetVisibilityPopUpTreeView();
            contentRejectVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentRejectVisibility");
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            IsShowPopUpTreeView = true;
            objGenericSuggestionTree_VM = ConvertViewModelToGenericSuggestionBusinessVM(((GenericTreeView)obj).ParentGeneric);
        }
        private void CommandShowPopApproveGenericExecute(object obj)
        {
            CommentGenericSuggestionAction = string.Empty;
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            ResetVisibilityPopUpTreeView();
            contentApproveVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentApproveVisibility");
            IsShowPopUpTreeView = true;
            objGenericSuggestionTree_VM = ConvertViewModelToGenericSuggestionBusinessVM(((GenericTreeView)obj).ParentGeneric);
        }
        private bool CommandApproveRejectGenericSuggestionCanExecute(object obj)
        {
            if (Engine.IsApproveRejectGenericSuggestion == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandApproveGenericSuggestionInTreeViewExecute(object obj)
        {
            objGenericSuggestionTree_VM.Note = CommentGenericSuggestionAction;
            List<int> listTopMainCasId = new();
            if (TreeViewItem_Generic_Backend.ParentGeneric.Status == "Delete")
            {
                listTopMainCasId = _objLibraryFunction_Service.GetMainTopFirstLabelCas(objGenericSuggestionTree_VM.ChildCasID);
            }
            var result = _objGeneric_Service.ApproveGenericSuggestion(objGenericSuggestionTree_VM, "Tree");
            CommentGenericSuggestionAction = string.Empty;
            if (result)
            {
                var listFlatMaskVM = new List<Flat_Mask_VM>();
                _notifier.ShowSuccess("Generic Suggestion Approved successfully");
                if (TreeViewItem_Generic_Backend != null)
                {
                    if (TreeViewItem_Generic_Backend.ParentGeneric.Status == "Add")
                    {
                        //  RefreshTreeFromXPathApproveReject(TreeViewItem_Generic_Backend.ParentGeneric.XPath);
                        TreeViewItem_Generic_Backend.ParentGeneric.Status = "Confirm";
                        listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = objGenericSuggestionTree_VM.ParentCasID != null ? (int)objGenericSuggestionTree_VM.ParentCasID : 0, ChildCasId = objGenericSuggestionTree_VM.ChildCasID, Type = TreeViewItem_Generic_Backend.ParentGeneric.Status });
                    }
                    else if (TreeViewItem_Generic_Backend.ParentGeneric.Status == "Delete")
                    {
                        //TreeViewItem_Generic_Backend.ChildCount = TreeViewItem_Generic_Backend.ChildCount - 1;
                        if (listTopMainCasId.Count > 0)
                        {
                            foreach (var itemcas in listTopMainCasId)
                            {
                                listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = 0, ChildCasId = itemcas, Type = TreeViewItem_Generic_Backend.ParentGeneric.Status });
                            }
                        }
                        else
                        {
                            if (TreeViewItem_Generic_Backend.ParentGeneric.XPath.Split('/').Count() > 0)
                            {
                                listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = 0, ChildCasId = Convert.ToInt32(TreeViewItem_Generic_Backend.ParentGeneric.XPath.Split('/')[0]), Type = TreeViewItem_Generic_Backend.ParentGeneric.Status });
                            }
                            else
                            {
                                listFlatMaskVM.Add(new Flat_Mask_VM() { ParentCasId = objGenericSuggestionTree_VM.ParentCasID != null ? (int)objGenericSuggestionTree_VM.ParentCasID : 0, ChildCasId = objGenericSuggestionTree_VM.ChildCasID, Type = TreeViewItem_Generic_Backend.ParentGeneric.Status });
                            }
                        }

                        RefreshTreeFromXPathApproveReject(TreeViewItem_Generic_Backend.ParentGeneric.XPath, true);

                    }
                }
                UpdateGenericFlats(listFlatMaskVM);
            }
            else
            {
                _notifier.ShowError("Error:Action not completed due to internal server error!");
            }
            IsShowPopUpTreeView = false;

        }

        private void CommandRejectGenericSuggestionInTreeViewExecute(object obj)
        {
            objGenericSuggestionTree_VM.Note = CommentGenericSuggestionAction;
            var result = _objGeneric_Service.RejectGenericSuggestion(objGenericSuggestionTree_VM, "Tree");
            CommentGenericSuggestionAction = string.Empty;
            if (result)
            {
                _notifier.ShowSuccess("Generic Suggestion Rejected successfully");
                if (TreeViewItem_Generic_Backend != null)
                {
                    if (TreeViewItem_Generic_Backend.ParentGeneric.Status == "Add")
                    {
                        TreeViewItem_Generic_Backend.ChildCount = TreeViewItem_Generic_Backend.ChildCount - 1;
                        TreeViewItem_Generic_Backend.IsDeleted = true;
                        RefreshTreeFromXPathApproveReject(TreeViewItem_Generic_Backend.ParentGeneric.XPath, true);
                    }
                    else if (TreeViewItem_Generic_Backend.ParentGeneric.Status == "Delete")
                    {
                        TreeViewItem_Generic_Backend.ParentGeneric.Status = "Confirm";
                    }
                }
            }
            else
            {
                _notifier.ShowError("Error:Action not completed due to internal server error!");
            }
            IsShowPopUpTreeView = false;
        }
        private void RefreshTreeFromXPath(string xPath, bool IsParent = false, string _parentCas = "")
        {
            char delimiter = '/';
            string[] arrayXPath = xPath.TrimEnd(delimiter).Split(delimiter);
            int arrylength = 0;
            if (IsParent)
            {
                arrylength = arrayXPath.Length - 1;
            }
            else
            {
                arrylength = arrayXPath.Length;
            }
            if (arrayXPath.Length <= 2)
            {
                ListGenericTree_Backend = GenericTreeOperation.GetGenericManagementTree((int?)null, (int?)null, "", "", "");
                ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend);
            }
            var listGenericSuggestionSearch = ListGenericTree;
            for (int counter = 0; counter <= arrylength - 1; counter++)
            {
                if (counter == arrayXPath.Length - 1)
                {
                    var objSelectedSuggest = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCas == arrayXPath[counter]).FirstOrDefault();
                    if (objSelectedSuggest != null)
                    {
                        objSelectedSuggest.IsSelected = true;
                    }
                }
                else
                {
                    var itemSearchSelected = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                    itemSearchSelected.IsExpanded = true;
                    if (IsSearchFromReviewTab || !string.IsNullOrEmpty(SearchCas) || IsSearchByProduct)
                    {
                        if ((counter + 1) == arrayXPath.Length - 1)
                        {
                            var subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.ChildCas == arrayXPath[counter + 1]).ToList());
                            itemSearchSelected.SubGeneric = subChildren;
                        }
                        else
                        {
                            var subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter + 1])).ToList());
                            itemSearchSelected.SubGeneric = subChildren;
                        }
                    }
                    listGenericSuggestionSearch = itemSearchSelected.SubGeneric;
                }
            }
            if (IsParent)
            {
                var objSelectedSuggest = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCas == _parentCas).FirstOrDefault();
                if (objSelectedSuggest != null)
                {
                    objSelectedSuggest.IsSelected = true;
                }
            }
            NotifyPropertyChanged("ListGenericTree");
        }
        private void RefreshTreeFromXPathApproveReject(string xPath, bool IsDelete = false)
        {
            char delimiter = '/';
            string[] arrayXPath = xPath.TrimEnd(delimiter).Split(delimiter);
            int arrylength = 0;
            if (IsDelete)
            {
                arrylength = arrayXPath.Length - 1;
            }
            else
            {
                arrylength = arrayXPath.Length;
            }

            if (arrayXPath.Length <= 2)
            {
                ListGenericTree_Backend = GenericTreeOperation.GetGenericManagementTree((int?)null, (int?)null, "", "", "");
                ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend);
            }
            var listGenericSuggestionSearch = ListGenericTree;

            for (int counter = 0; counter <= arrylength - 1; counter++)
            {
                if (counter == arrylength - 1)
                {
                    var objSelectedSuggest = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                    if (objSelectedSuggest != null)
                    {
                        objSelectedSuggest.IsSelected = true;
                    }
                }
                else
                {
                    var itemSearchSelected = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                    if (itemSearchSelected != null)
                    {
                        itemSearchSelected.IsExpanded = true;
                        if (IsSearchFromReviewTab || !string.IsNullOrEmpty(SearchCas) || IsSearchByProduct)
                        {
                            var subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter + 1])).ToList());
                            itemSearchSelected.SubGeneric = subChildren;
                        }
                        listGenericSuggestionSearch = itemSearchSelected.SubGeneric;
                    }
                }
            }
            NotifyPropertyChanged("ListGenericTree");
        }

        #endregion

        #region Remove And Add Additional Comment
        private void CommandRemoveGenericSuggestionInTreeViewExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) && !string.IsNullOrWhiteSpace(CommentGenericSuggestionAction))
            {
                objGenericSuggestionTree_VM.Note = CommentGenericSuggestionAction;
                _objGeneric_Service.RemoveGenericSuggestion(objGenericSuggestionTree_VM);
                _notifier.ShowSuccess("Generic Suggestion Removed successfully");
                IsShowPopUpTreeView = false;
                NotifyPropertyChanged("IsShowPopUpTreeView");

                SelectedTreeViewItem = new GenericSuggestionVM();
                SelectedTreeViewItem.ParentCasID = objGenericSuggestionTree_VM.ParentCasID;
                SelectedTreeViewItem.ChildCasID = objGenericSuggestionTree_VM.ChildCasID;
                BindListItemComments();
                BindListItemParentComments();
                NotifyPropertyChanged("SelectedTreeViewItem");
                CommentGenericSuggestionAction = string.Empty;
                CommentSuggestionParent = string.Empty;
                if (TreeViewItem_Generic_Backend != null)
                {
                    TreeViewItem_Generic_Backend.ParentGeneric.Status = "Delete";
                }
            }
            else
            {

                _notifier.ShowWarning("Comment is mandatory for perform this action.");
            }
        }
        private bool CommandAddAdditionalCommentCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) || !string.IsNullOrEmpty(CommentSuggestionParent))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandAddAdditionalCommentExecute(object obj)
        {
            if (IsNeedMoreResearch == true && !CheckCasSubstanceIsGroup(SelectedTreeViewItem.ChildCasID))
            {
                _notifier.ShowError("Add research flag faild due to cas substance type is not 'Group'");
                return;
            }
            string type = (string)obj;
            GenericSuggestionVM objGenericComment = new GenericSuggestionVM();
            objGenericComment.ParentCasID = SelectedTreeViewItem.ParentCasID;
            objGenericComment.ChildCasID = SelectedTreeViewItem.ChildCasID;
            if (type == "ParentChild")
            {
                objGenericComment.Note = CommentGenericSuggestionAction;
            }
            else if (type == "Parent")
            {
                objGenericComment.ParentComment = CommentGenericSuggestionAction;
            }
            objGenericComment.UserID = Environment.UserName;
            objGenericComment.IsAdditionalGenericsResearchFlag = IsNeedMoreResearch == true ? true : false;
            _objGeneric_Service.InsertAdditionalComment(objGenericComment, type);
            _notifier.ShowSuccess("Additonal Comment Added Successfully");
            BindListItemComments();
            BindListItemParentComments();
            CommentGenericSuggestionAction = string.Empty;
            CommentSuggestionParent = string.Empty;
            IsNeedMoreResearch = false;
            NotifyPropertyChanged("IsNeedMoreResearch");
        }
        private void CommandSelectedTreeViewItemExecute(object obj)
        {
            if (obj != null)
            {
                var objGenericTreeParent = ((GenericTreeView)obj).ParentGeneric;
                SelectedTreeViewItem = new GenericSuggestionVM();
                SelectedTreeViewItem.ParentCasID = objGenericTreeParent.ParentCasID;
                SelectedTreeViewItem.ChildCasID = objGenericTreeParent.ChildCasID ?? 0;
                if (objGenericTreeParent.Level > 1)
                {
                    objCommentDC = new CommentsUC_VM(new GenericComment_VM(objGenericTreeParent.ParentCas, objGenericTreeParent.ChildCas, objGenericTreeParent.ParentCasID, objGenericTreeParent.ChildCasID, 370, true, "Generics Parent-Child"));

                }
                else
                {
                    objCommentDC = new CommentsUC_VM(new GenericComment_VM(objGenericTreeParent.ChildCas, "", objGenericTreeParent.ChildCasID, 0, 370, true, "Parent Comment from Tree"));
                }
                NotifyPropertyChanged("objCommentDC");

            }


        }
        #endregion

        #region  Search Functionality Tree View
        public void TreeSearchByProduct()
        {
            IsSearchFromReviewTab = false;
            if (SelectedProduct != null)
            {
                IsSearchByProduct = true;
                Visibility_LoaderTreeView = Visibility.Visible;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
                Task.Run(() =>
                {
                    listSearchedCas = _objGeneric_Service.SearchGenericSuggestionCasList(SelectedProduct.ProductID.ToString(), "Product", SelectedItemSearchType.Text, "");
                    CurentFilterIndex = 1;
                    FilterCasSearchResult("Product");
                    Visibility_LoaderTreeView = Visibility.Collapsed;
                    NotifyPropertyChanged("Visibility_LoaderTreeView");
                    SearchResultVisibility = Visibility.Visible;
                    NotifyPropertyChanged("SearchResultVisibility");
                });
            }
            else
            {
                IsSearchByProduct = false;
                BindGenericSuggestionTreeView();
            }
            IsDisplaySearchDrawer = false;
            NotifyPropertyChanged("IsDisplaySearchDrawer");
        }
        private bool CommandSearchCasTreeCanExecute(object obj)
        {
            if (SelectedItemSearchType != null)//!string.IsNullOrEmpty(SearchCas)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandSearchCasTreeExecute(object obj)
        {
            SearchCasInTreeView(SearchCas, SelectedItemSearchFor.Text, SelectedItemSearchType.Text, SelectedItemMatchCondition.Text);
        }
        private void SearchCasInTreeView(string casForSearch, string searchFor, string searchType, string matchCond)
        {

            IsSearchByProduct = false;
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                listSearchedCas = _objGeneric_Service.SearchGenericSuggestionCasList((string.IsNullOrEmpty(casForSearch) ? "" : casForSearch.Replace("-", "")), searchFor, searchType, matchCond);
                CurentFilterIndex = 1;
                FilterCasSearchResult(IsSearchByProduct ? "Product" : searchType);
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
                SearchResultVisibility = Visibility.Visible;
                NotifyPropertyChanged("SearchResultVisibility");
                casForSearch_Filter = casForSearch;
                searchFor_Filter = searchFor;
                searchType_Filter = searchType;
                matchCond_Filter = matchCond;
                NotifyPropertyChanged("casForSearch_Filter");
                NotifyPropertyChanged("searchFor_Filter");
                NotifyPropertyChanged("searchType_Filter");
                NotifyPropertyChanged("matchCond_Filter");
            });
            IsDisplaySearchDrawer = false;
            NotifyPropertyChanged("IsDisplaySearchDrawer");
        }

        public int GetUniqueResultCount()
        {
            int resultCount = 0;
            if (listSearchedCas.Count > 0)
            {
                var listString = listSearchedCas.Select(x => !string.IsNullOrEmpty(x.XPath) && x.XPath.Count(y => y == '/') >= 2 ? x.XPath.Substring(0, x.XPath.IndexOf('/', x.XPath.IndexOf('/') + 1)) : x.XPath).ToList();
                resultCount = listString.GroupBy(x => x).Count();
            }
            return resultCount;
        }
        public void FilterCasSearchResult(string SearchType)
        {
            if (listSearchedCas.Count > 0)
            {
                int uniqueSearchCount = GetUniqueResultCount();
                strFoundResult = "Results: " + CurentFilterIndex + " of " + listSearchedCas.Count + " (Unique Search Result: " + uniqueSearchCount + ")";
                NotifyPropertyChanged("strFoundResult");
                if (SelectedItemSearchType != null)
                {
                    if (SearchType == "Only First Level Parent")//SelectedItemSearchType.Text
                    {
                        FilterParentCasSearchResult();
                    }
                    else if (SearchType == "Only Child" || SearchType == "Review Generics Suggestion" || SearchType == "Both Child and Parent" || SearchType == "Product" || SearchType == "FilterReviewGenericSuggestion" || SearchType == "FilterReviewGenericSuggestion_OnlyTopParent")
                    {
                        FilterChildCasSearchResult();
                    }
                }
            }
            else
            {
                strFoundResult = "Results: Not Found";
                NotifyPropertyChanged("strFoundResult");
            }
        }
        public void FilterChildCasSearchResult()
        {
            var objSearchCas = listSearchedCas.Skip(CurentFilterIndex - 1).Take(CurentFilterIndex).FirstOrDefault();

            char delimiter = '/';
            string[] arrayXPath = objSearchCas.XPath.TrimEnd(delimiter).Split(delimiter).Where(x => x != "0").ToArray();

            ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[arrayXPath.Length - 1])).ToList());
            var listGenericSuggestionSearch = ListGenericTree;
            listGenericSuggestionSearch.Where(x => x.IsExpanded).ToList().ForEach(x => x.IsExpanded = false);
            listGenericSuggestionSearch.Where(x => x.IsSelected).ToList().ForEach(x => { x.IsSelected = false; x.IsFiltered = false; });

            for (int counter = arrayXPath.Length - 1; counter >= 0; counter--)
            {
                if (counter == 0)
                {
                    var objSelectedSuggest = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                    if (objSelectedSuggest != null)
                    {
                        objSelectedSuggest.IsSelected = true;
                        objSelectedSuggest.IsFiltered = true;
                    }
                    else
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            _notifier.ShowError("Error: Filtered Item not found!");
                        });
                    }
                }
                else
                {
                    var itemSearchSelected = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();

                    if (itemSearchSelected != null)
                    {
                        itemSearchSelected.IsExpanded = true;
                        if (IsSearchFromReviewTab && (counter - 1 == 0))
                        {
                            var subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.Status == "Add" || x.ParentGeneric.Status == "Delete").ToList());
                            if (subChildren.Count == 0)
                            {
                                subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter - 1])).ToList());
                            }
                            itemSearchSelected.SubGeneric = subChildren;
                            listGenericSuggestionSearch = subChildren;
                        }
                        else
                        {
                            var subChildren = new ObservableCollection<GenericTreeView>(itemSearchSelected.SubGeneric.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter - 1])).ToList());
                            itemSearchSelected.SubGeneric = subChildren;
                            listGenericSuggestionSearch = subChildren;
                        }
                    }
                    else
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            _notifier.ShowError("Error: Filtered Item not found!");
                        });
                    }
                }
            }
            NotifyPropertyChanged("ListGenericTree");
        }
        public void FilterParentCasSearchResult()
        {
            var objSearchCas = listSearchedCas.Skip(CurentFilterIndex - 1).Take(CurentFilterIndex).FirstOrDefault();
            if (objSearchCas != null)
            {
                ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend.Where(x => x.ParentGeneric.ChildCasID == objSearchCas.ParentCasID).ToList());
                var listGenericSuggestionSearch = ListGenericTree;
                listGenericSuggestionSearch.Where(x => x.IsSelected).ToList().ForEach(x => { x.IsSelected = false; x.IsFiltered = false; });
                var itemSearchSelected = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == objSearchCas.ParentCasID).FirstOrDefault();
                if (itemSearchSelected != null)
                {
                    itemSearchSelected.IsSelected = true;
                    itemSearchSelected.IsFiltered = true;
                }
                NotifyPropertyChanged("ListGenericTree");
            }

        }
        private bool CommandFindPrevFoundResultCanExecute(object obj)
        {
            if (listSearchedCas.Count > 1 && CurentFilterIndex > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandFindPrevFoundResultExecute(object obj)
        {
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                CurentFilterIndex = CurentFilterIndex - 1;
                FilterCasSearchResult(IsSearchByProduct ? "Product" : SelectedItemSearchType.Text);
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
            });
        }
        private bool CommandFindNextFoundResultCanExecute(object obj)
        {
            if (listSearchedCas.Count > 1 && (listSearchedCas.Count) > CurentFilterIndex)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandFindNextFoundResulteExecute(object obj)
        {
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                CurentFilterIndex = CurentFilterIndex + 1;
                FilterCasSearchResult(IsSearchByProduct ? "Product" : SelectedItemSearchType.Text);
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
            });
        }
        private void CommandClearSearchExecute(object obj)
        {
            IsSearchByProduct = false;
            IsSearchFromReviewTab = false;
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                SearchResultVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("SearchResultVisibility");
                SearchCas = string.Empty;
                NotifyPropertyChanged("SearchCas");
                ListGenericTree_Backend.Where(x => x.IsExpanded || x.IsFiltered || x.IsSelected).ToList().ForEach(x => { x.IsExpanded = false; x.IsFiltered = false; x.IsSelected = false; });
                ListGenericTree = new ObservableCollection<GenericTreeView>(ListGenericTree_Backend);
                NotifyPropertyChanged("ListGenericTree");
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
                casForSearch_Filter = "";
                searchFor_Filter = "";
                searchType_Filter = "";
                matchCond_Filter = "";
                NotifyPropertyChanged("casForSearch_Filter");
                NotifyPropertyChanged("searchFor_Filter");
                NotifyPropertyChanged("searchType_Filter");
                NotifyPropertyChanged("matchCond_Filter");
            });
        }
        private bool CommandShowAllFilterResultCanExecute(object obj)
        {
            if (listSearchedCas != null && listSearchedCas.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandShowAllFilterResultExecute(object obj)
        {
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            Task.Run(() =>
            {
                int uniqueSearchCount = GetUniqueResultCount();
                strFoundResult = "Results: Showing All " + listSearchedCas.Count + " of " + listSearchedCas.Count + " (Unique Search Result: " + uniqueSearchCount + ")"; ;
                NotifyPropertyChanged("strFoundResult");
                string searchType = IsSearchByProduct ? "Product" : SelectedItemSearchType.Text;
                if (searchType != null)
                {
                    if (searchType == "Only First Level Parent")
                    {
                        FilterParentShowingAllResult();
                    }
                    else if (searchType == "Only Child" || searchType == "Review Generics Suggestion" || searchType == "Both Child and Parent" || searchType == "Product")
                    {
                        FilterChildShowingAllResult();
                    }
                }
                Visibility_LoaderTreeView = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_LoaderTreeView");
            });
        }

        public void FilterParentShowingAllResult()
        {

            var listFilterParentCas = new ObservableCollection<GenericTreeView>();
            foreach (var objSearchCas in listSearchedCas)
            {
                listFilterParentCas.Add(ListGenericTree_Backend.Where(x => x.ParentGeneric.ChildCasID == objSearchCas.ParentCasID).FirstOrDefault());
            }
            ListGenericTree = listFilterParentCas;
            NotifyPropertyChanged("ListGenericTree");
        }
        public void FilterChildShowingAllResult()
        {
            var listFilterParentCas = new ObservableCollection<GenericTreeView>();
            ListGenericTree_Backend.Where(x => x.IsExpanded).ToList().ForEach(x => x.IsExpanded = false);
            foreach (var objSearchCas in listSearchedCas)
            {
                char delimiter = '/';
                string[] arrayXPath = objSearchCas.XPath.TrimEnd(delimiter).Split(delimiter);
                if (!listFilterParentCas.Any(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[arrayXPath.Length - 1])))
                {
                    listFilterParentCas.Add(ListGenericTree_Backend.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[arrayXPath.Length - 1])).FirstOrDefault());
                }
                var listGenericSuggestionSearch = listFilterParentCas;
                for (int counter = arrayXPath.Length - 1; counter >= 0; counter--)
                {
                    if (counter == 0)
                    {
                        var objSelectedSuggest = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                        if (objSelectedSuggest != null)
                        {
                            objSelectedSuggest.IsFiltered = true;
                        }
                        else
                        {
                            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                            {
                                _notifier.ShowError("Error: Filtered Item not found!");
                            });
                        }
                    }
                    else
                    {
                        var itemSearchSelected = listGenericSuggestionSearch.Where(x => x.ParentGeneric.ChildCasID == Convert.ToInt32(arrayXPath[counter])).FirstOrDefault();
                        if (!itemSearchSelected.IsExpanded)
                        {
                            itemSearchSelected.IsExpanded = true;
                        }
                        listGenericSuggestionSearch = itemSearchSelected.SubGeneric;
                    }
                }
            }
            ListGenericTree = listFilterParentCas;
            NotifyPropertyChanged("ListGenericTree");
        }
        #endregion

        #region Add Edit Cas Name
        private void CommandShowPopUpAddCasNameExecute(object obj)
        {
            ResetVisibilityPopUpTreeView();
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            objGenericParentChildCasName = new GenericParentChildCasName_VM();
            objGenericParentChildCasName.CasID = ((GenericTreeView)obj).ParentGeneric.ChildCasID ?? 0;
            ChildName = ((GenericTreeView)obj).ParentCasName;
            objGenericParentChildCasName.CasType = ((GenericTreeView)obj).ChildCount == 0 ? "Child" : "Parent";
            objGenericParentChildCasName.SessionID = VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID;
            ParentCas = ((GenericTreeView)obj).ParentGeneric.ChildCas;
            NotifyPropertyChanged("ParentCas");
            IsShowPopUpTreeView = true;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            contentAddEditCasNameVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentAddEditCasNameVisibility");

        }
        private bool CommandSaveAddEditCasNameCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ChildName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CommandSaveAddEditCasNameExecute(object obj)
        {
            _notifier.ShowInformation("Add Parent Name InProgress...");
            Task.Run(() =>
            {
                objGenericParentChildCasName.ParentName = ChildName;
                objGenericParentChildCasName.ParentNameID = ChildNameID;
                _objGeneric_Service.SaveParentChildCasName(objGenericParentChildCasName);
                _notifier.ShowSuccess("Add Parent Name Successfully");
                TreeViewItem_Generic_Backend.ParentCasName = ChildName;
                ParentCas = string.Empty;
                NotifyPropertyChanged("ParentCas");
            });
            IsShowPopUpTreeView = false;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            contentAddEditCasNameVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentAddEditCasNameVisibility");
        }
        #endregion

        #region Tree Other functionalities
        private void CommandIsExcludeChangeExecute(object obj)
        {
            bool IsUpdated = _objGeneric_Service.UpdateIsExcludeInGenericNested(ConvertViewModelToGenericSuggestionBusinessVM((GenericSuggestionTree_ViewModel)obj));
            if (IsUpdated)
            {
                _notifier.ShowSuccess("Node Is Exclude update successfully.");
            }
            else
            {
                _notifier.ShowError("Node Is Exclude not updated due to internal server error!");
            }
        }

        private void CommandCopyCellDataToClipboardExecute(object obj)
        {

            string cas = (string)obj;
            if (!string.IsNullOrEmpty(cas))
            {

                Clipboard.SetDataObject(cas);
            }


        }

        private void CommandRefreshTreeViewExecute(object obj)
        {
            IsSearchByProduct = false;
            IsSearchFromReviewTab = false;
            Visibility_LoaderTreeView = Visibility.Visible;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            SearchResultVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("SearchResultVisibility");
            SearchCas = string.Empty;
            NotifyPropertyChanged("SearchCas");
            ListGenericTree_Backend.Where(x => x.IsExpanded || x.IsFiltered || x.IsSelected).ToList().ForEach(x => { x.IsExpanded = false; x.IsFiltered = false; x.IsSelected = false; });
            Visibility_LoaderTreeView = Visibility.Collapsed;
            NotifyPropertyChanged("Visibility_LoaderTreeView");
            casForSearch_Filter = "";
            searchFor_Filter = "";
            searchType_Filter = "";
            matchCond_Filter = "";
            NotifyPropertyChanged("casForSearch_Filter");
            NotifyPropertyChanged("searchFor_Filter");
            NotifyPropertyChanged("searchType_Filter");
            NotifyPropertyChanged("matchCond_Filter");
            objCommentDC = new CommentsUC_VM(new GenericComment_VM("", "", 0, 0));
            NotifyPropertyChanged("objCommentDC");
            BindGenericSuggestionTreeView();
        }

        private void CommandLostFocusChildCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ChildCas))
            {
                Task.Run(() =>
                {
                    Map_SubstanceType_CategoryByCasID(ChildCas, "child");
                    var IsValid = CheckCasInValid(ChildCas);
                    //if (casTypes == "parent")
                    //{
                    //    ParentCasInvalid = IsValid ? string.Empty : "InValid";
                    //    NotifyPropertyChanged("ParentCasInvalid");
                    //}
                    //else if (casTypes == "child")
                    //{
                    ChildCasInvalid = IsValid ? string.Empty : "InValid";
                    NotifyPropertyChanged("ChildCasInvalid");
                    //}
                });

                int? ChemNameID = _objGeneric_Service.GetChildChemicalNameIDByCas(ChildCas);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();// listAllChemicalName_Backend.Find(x => x.ChemicalNameID == ChemNameID);
                    if (foundRes != null)
                    {
                        Selected_Chemical_ChildName = foundRes;
                    }
                }
                else
                {
                    ChildName = string.Empty;
                    ChildNameID = 0;
                    Selected_Chemical_ChildName = null;
                }
            }
        }
        public void Map_SubstanceType_CategoryByCasID(string cas, string type)
        {
            var objCas = _objGeneric_Service.GetLibraryCasByCas(cas);
            if (objCas != null)
            {
                var existingsubstanceTypeID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceType", objCas.CASID);
                var existingsubstanceCategoryID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceCategory", objCas.CASID);
                var existinggenericCategoryID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("genericCategory", objCas.CASID);
                if (type == "child")
                {
                    if (existingsubstanceTypeID != 0)
                    {
                        selectedChildSubstanceType = listChildSubstanceType.Where(x => x.SubstanceTypeID == existingsubstanceTypeID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildSubstanceType");
                    }
                    //else
                    //{
                    //    selectedChildSubstanceType = null;// new Library_SubstanceType();
                    //    NotifyPropertyChanged("selectedChildSubstanceType");
                    //}
                    if (existingsubstanceCategoryID != 0)
                    {
                        selectedChildSubstanceCategory = listSubstanceCategory.Where(x => x.SubstanceCategoryID == existingsubstanceCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildSubstanceCategory");
                    }
                    //else
                    //{
                    //    selectedChildSubstanceCategory = null;// new Library_SubstanceType();
                    //    NotifyPropertyChanged("selectedChildSubstanceCategory");
                    //}
                    if (existinggenericCategoryID != 0)
                    {
                        selectedChildItemCategory = listGenericsCategory.Where(x => x.GenericsCategoryID == existinggenericCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedChildItemCategory");
                    }
                }
                else
                {
                    if (existingsubstanceTypeID != 0)
                    {
                        selectedParentSubstanceType = listSubstanceType.Where(x => x.SubstanceTypeID == existingsubstanceTypeID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentSubstanceType");
                    }
                    if (existingsubstanceCategoryID != 0)
                    {
                        selectedParentSubstanceCategory = listSubstanceCategory.Where(x => x.SubstanceCategoryID == existingsubstanceCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentSubstanceCategory");
                    }
                    if (existinggenericCategoryID != 0)
                    {
                        selectedParentItemCategory = listGenericsCategory.Where(x => x.GenericsCategoryID == existinggenericCategoryID).FirstOrDefault();
                        NotifyPropertyChanged("selectedParentItemCategory");
                    }
                }
            }
        }
        private void CommandLostFocusParentCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ParentCas))
            {
                Task.Run(() =>
                {
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        Map_SubstanceType_CategoryByCasID(ParentCas, "parent");
                        var IsValid = CheckCasInValid(ParentCas);
                        ParentCasInvalid = IsValid ? string.Empty : "InValid";
                        NotifyPropertyChanged("ParentCasInvalid");
                    });
                });
                int? ChemNameID = _objGeneric_Service.GetParentChemicalNameIDByCas(ParentCas);
                if (ChemNameID != null && ChemNameID != 0)
                {
                    var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();
                    if (foundRes != null)
                    {
                        Selected_Chemical_ParentName = foundRes;
                    }
                }
                else
                {
                    ParentName = string.Empty;
                    ParentNameID = 0;
                    Selected_Chemical_ParentName = null;
                }
            }
        }
        public void FindChemNameAndSelectIfExist(int? ChemNameID, string Type)
        {
            if (ChemNameID != null && ChemNameID != 0)
            {
                var foundRes = _objGeneric_Service.FilterChemicalNameForGeneric("", ChemNameID).FirstOrDefault();
                if (foundRes != null)
                {
                    if (Type == "Child")
                    {
                        Selected_Chemical_ChildName = foundRes;
                    }
                    else
                    {
                        Selected_Chemical_ParentName = foundRes;
                    }
                }
            }
        }
        public bool CheckCasInValid(string cas)
        {
            bool IsValid = false;
            if (!string.IsNullOrEmpty(cas))
            {
                cas = _objGeneric_Service.RemoveStripCharcterFromCas(cas);
                var regex = new System.Text.RegularExpressions.Regex(@"^\d+$");
                if (regex.IsMatch(cas.Replace("-", "")))
                {
                    if (_objGeneric_Service.CheckCasValid(cas))
                    {
                        IsValid = true;
                    }
                }
                else
                {
                    IsValid = true;
                }
            }
            return IsValid;

        }
        #endregion

        #region Tree View Badge 
        public void FetchQsidDetail(int qsid_Id)
        {
            QsidInfoPopUpIsOpen = true;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
            QsidDetailInfo = _objAccessVersion_Service.GetQsidDetailInfoByQsid_ID(qsid_Id);
            NotifyPropertyChanged("QsidDetailInfo");
        }
        private void CommandPopUpActiveListExecute(object obj)
        {
            int childCasID = (int)obj;
            listActiveQsids = new ObservableCollection<Library_QSIDs>(_objGeneric_Service.GetActiveQsidsListDetailByCasID(childCasID));
            NotifyPropertyChanged("listActiveQsids");
            ActiveListDetailIsOpen = true;
            NotifyPropertyChanged("ActiveListDetailIsOpen");
        }
        private void CommandCloseQsidInfoPopUpExecute(object obj)
        {
            QsidInfoPopUpIsOpen = false;
            NotifyPropertyChanged("QsidInfoPopUpIsOpen");
        }
        private void CommandOpenCurrentDataViewExecute(object obj)
        {

            Task.Run(() =>
            {
                try
                {
                    Engine.IsOpenCurrentDataViewFromVersionHis = true;
                    Engine.CurrentDataViewQsid = QsidDetailInfo.QSID;
                    Engine.CurrentDataViewQsidID = QsidDetailInfo.QSID_ID;
                    MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("OpenCurrentDataView", NotifyMe));

                }
                catch (Exception ex)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        _notifier.ShowError("Unable to open CurrentDataViewTab due to following error:" + Environment.NewLine + ex.Message);
                    });
                }
            });
        }

        private void NotifyMe(string obj)
        {

        }

        #endregion

        #region Tree View Nested Parent
        private void CommandShowNestedParentExecute(object obj)
        {
            CasIDForNestedDetail = (int)((GenericSuggestionTree_ViewModel)obj).ChildCasID;
            BindGenericNestedParent(CasIDForNestedDetail, ((GenericSuggestionTree_ViewModel)obj).XPath);
            ResetVisibilityPopUpTreeView();
            contentNestedParentVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentNestedParentVisibility");
            IsShowPopUpTreeView = true;
        }
        public void BindGenericNestedParent(int ChildCasID, string casXPath)
        {
            NestedParentLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("NestedParentLoaderVisibility");
            Task.Run(() =>
            {
                ListNestedParent = new ObservableCollection<GenericeNestedParent>(_objGeneric_Service.GetGenericNestedParent(ChildCasID, casXPath, "Tree"));
                NestedParentLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("NestedParentLoaderVisibility");
            });
        }
        private void CommandDetailNestedParentExecute(object obj)
        {
            BindGenericSuggestionNestedParent(CasIDForNestedDetail);
            IsSelectedNestedGenericTab = true;

        }
        #endregion

        #region Tree View Substance Info
        private void CommandCloseSubstanceInfoPopUpExecute(object obj)
        {

            SubstanceInfoPopUpIsOpen = false;
            NotifyPropertyChanged("SubstanceInfoPopUpIsOpen");
        }

        private void CommandShowSubstanceInfoPopUpExecute(object obj)
        {
            ChildCas = (string)obj;
            SubstanceInfoPopUpIsOpen = true;
            NotifyPropertyChanged("SubstanceInfoPopUpIsOpen");
            selectedChildItemCategory = null;
            selectedChildSubstanceCategory = null;
            selectedChildSubstanceType = null;

            NotifyPropertyChanged("selectedChildItemCategory");
            NotifyPropertyChanged("selectedChildSubstanceCategory");
            NotifyPropertyChanged("selectedChildSubstanceType");

            Map_SubstanceType_CategoryByCasID((string)(obj), "child");
            IsEditSubInfo = false;
            NotifyPropertyChanged("IsEditSubInfo");
        }
        private void CommandUpdateSubstanceInfoExecute(object obj)
        {
            var objGenericModel = MapGenericCatIDsVMFromLocalVar(selectedChildItemCategory, selectedChildSubstanceType, selectedChildSubstanceCategory);
            var resultFlag = _objGeneric_Service.UpdateSubstanceInfo(objGenericModel, ChildCas, Engine.CurrentUserSessionID);
            if (resultFlag)
            {
                ChildCas = string.Empty;
                _notifier.ShowSuccess("Substance Info updated successfully.");
            }
            else
            {
                _notifier.ShowError("Error: Internal Server Error!");
            }
        }
        #endregion

        #region Tree View Filter / Sort Option 
        private void CommandShowPopFilterSortNodeExecute(object obj)
        {
            TreeViewItem_Generic_Backend = (GenericTreeView)obj;
            string sortField = TreeViewItem_Generic_Backend.ChildNodeSortField;
            string sortType = TreeViewItem_Generic_Backend.ChildNodeSortType;
            SelectedSortFieldItem = listSortFieldType.Where(x => x.Text == sortField).FirstOrDefault();
            SelectedSortTypeItem = listSortType.Where(x => x.Text == sortType).FirstOrDefault();
            SearchCasNodeLevel = TreeViewItem_Generic_Backend.ChildNodeSearchCas;
            SearchChemicalNameNodeLevel = TreeViewItem_Generic_Backend.ChildNodeSearchCheicalName;
            NotifyPropertyChanged("SearchChemicalNameNodeLevel");
            NotifyPropertyChanged("SearchCasNodeLevel");
            NotifyPropertyChanged("SelectedSortTypeItem");
            NotifyPropertyChanged("SelectedSortFieldItem");
            ResetVisibilityPopUpTreeView();
            contentFilterSortVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentFilterSortVisibility");
            IsShowPopUpTreeView = true;
        }
        private void CommanClearNodeLevelFilterExecute(object obj)
        {
            TreeViewItem_Generic_Backend.ChildNodeSortField = string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSortType = string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSearchCas = string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSearchCheicalName = string.Empty;
            TreeViewItem_Generic_Backend.IsExpanded = true;
            contentFilterSortVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentFilterSortVisibility");
            IsShowPopUpTreeView = false;
        }

        private bool CommandApplyNodeLevelFilterCanExecute(object obj)
        {
            bool flag = false;
            if ((SelectedSortTypeItem != null && SelectedSortFieldItem != null) || !string.IsNullOrEmpty(SearchCasNodeLevel) || !string.IsNullOrEmpty(SearchChemicalNameNodeLevel))
            {
                flag = true;
            }
            return flag;
        }

        private void CommandApplyNodeLevelFilterExecute(object obj)
        {
            TreeViewItem_Generic_Backend.ChildNodeSortField = SelectedSortFieldItem != null ? SelectedSortFieldItem.Text : string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSortType = SelectedSortTypeItem != null ? SelectedSortTypeItem.Text : string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSearchCas = !string.IsNullOrEmpty(SearchCasNodeLevel) && !string.IsNullOrWhiteSpace(SearchCasNodeLevel) ? SearchCasNodeLevel : string.Empty;
            TreeViewItem_Generic_Backend.ChildNodeSearchCheicalName = !string.IsNullOrEmpty(SearchChemicalNameNodeLevel) && !string.IsNullOrWhiteSpace(SearchChemicalNameNodeLevel) ? SearchChemicalNameNodeLevel : string.Empty;
            TreeViewItem_Generic_Backend.IsExpanded = true;
            contentFilterSortVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("contentFilterSortVisibility");
            IsShowPopUpTreeView = false;
        }



        #endregion
        #endregion
        #region Generic Nested Parent Tab

        public void BindGenericSuggestionNestedParent(int childCasID)
        {
            NestedParentTabLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("NestedParentTabLoaderVisibility");
            Task.Run(() =>
            {
                ListNestedParent_Tab = new ObservableCollection<GenericNestedParent_Tab_VM>(_objGeneric_Service.GetGenericNestedParent(childCasID, "", "Tab").Select(x => new GenericNestedParent_Tab_VM
                {
                    ChildCas = x.ChildCas,
                    ChildCasID = x.ChildCasID,
                    ChildName = x.ChildName,
                    NestingGroupNo = x.NestingGroupNo,
                    NestingLevel = x.NestingLevel,
                    ParentCas = x.ParentCas,
                    ParentCasID = x.ParentCasID,
                    ParentName = x.ParentName,
                    IsExclude = x.IsExclude,
                    Status = x.Status,
                    DateAdded = x.DateAdded
                }).ToList());
                NestedParentTabLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("NestedParentTabLoaderVisibility");
            });
        }
        private bool CommandSearchCasNestedParentTabCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(searchCasNestedParent))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandSearchCasNestedParentTabExecute(object obj)
        {
            var lib_Cas = _objGeneric_Service.GetLibraryCasByCas(searchCasNestedParent);
            if (lib_Cas != null)
            {
                BindGenericSuggestionNestedParent(lib_Cas.CASID);
            }
            else
            {
                _notifier.ShowError("Error: " + searchCasNestedParent + " is not existing DB");
            }
        }

        public void SwitchTabFindCurrentGenericSuggestion_Nested()
        {
            ResetVisibilityPopUpTreeView();
            IsShowPopUpTreeView = false;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            IsSearchFromReviewTab = false;
            //if (_objGeneric_Service.ValidateIsParentSearch((int)SelectedNestedParent_VM.ChildCasID))
            //{
            //    SearchCas = SelectedNestedParent_VM.ParentCas;
            //}
            //else
            //{
            SearchCas = SelectedNestedParent_VM.ChildCas;
            //}
            NotifyPropertyChanged("SearchCas");
            SelectedItemMatchCondition = ListItemMatchCondition.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemMatchCondition");
            SelectedItemSearchFor = listItemSearchFor.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemSearchFor");
            SelectedItemSearchType = listItemSearchType.Where(x => x.Text == "Only Child").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemSearchType");
            CommandSearchCasTree.Execute("");
            IsSelectedTreeTab = true;
            NotifyPropertyChanged("IsSelectedTreeTab");
        }
        public void ResetVisibilityPopUpNested()
        {
            contentVisibilityListComments_Nested = Visibility.Collapsed;
            contentAddVisibility_Nested = Visibility.Collapsed;
            contentRemoveVisibility_Nested = Visibility.Collapsed;
            contentVisibiltyComment_Nested = Visibility.Collapsed;
            NotifyPropertyChanged("contentVisibiltyComment_Nested");
            NotifyPropertyChanged("contentAddVisibility_Nested");
            NotifyPropertyChanged("contentRemoveVisibility_Nested");
            NotifyPropertyChanged("contentVisibilityListComments_Nested");
        }
        public void ShowPopAddSuggestion_Nested()
        {
            ClearVariableAddSuggestion();
            ResetVisibilityPopUpNested();
            IsShowPopup_Nested = true;
            ParentCas = SelectedNestedParent_VM.ParentCas;
            NotifyPropertyChanged("ParentCas");
            FindChemNameAndSelectIfExist(SelectedNestedParent_VM.ParentNameID, "Parent");
            Task.Run(() =>
            {
                Map_SubstanceType_CategoryByCasID(ParentCas, "parent");
            });
            contentAddVisibility_Nested = Visibility.Visible;
            NotifyPropertyChanged("contentAddVisibility_Nested");
            IsParentSuggestionAction = false;
            IsChildSuggestionAction = true;
            IsMainSuggestionAction = false;
            NotifyPropertyChanged("IsParentSuggestionAction");
            NotifyPropertyChanged("IsChildSuggestionAction");
            NotifyPropertyChanged("IsMainSuggestionAction");
            NotifyPropertyChanged("IsShowPopup_Nested");
        }
        public void ShowPopAddDeleteSuggestion_Nested()
        {
            var result = MessageBox.Show("Are you sure do you want to remove this?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                CommentGenericSuggestionAction = string.Empty;
                ResetVisibilityPopUpNested();
                contentRemoveVisibility_Nested = Visibility.Visible;
                NotifyPropertyChanged("contentRemoveVisibility_Nested");
                IsShowPopup_Nested = true;
                NotifyPropertyChanged("IsShowPopup_Nested");
                //objGenericSuggestionTree_VM = ConvertViewModelToGenericSuggestionBusinessVM(((GenericTreeView)obj).ParentGeneric);
            }
        }
        private bool CommandAddGenericSuggestion_NestedCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ParentCas) && !string.IsNullOrEmpty(ChildCas))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandAddGenericSuggestion_NestedExecute(object obj)
        {
            if (ValidationAddSuggestion(false, true))
            {
                return;
            }
            AddGenericSuggestion();
            IsShowPopup_Nested = false;
            NotifyPropertyChanged("IsShowPopup_Nested");
            ClearVariableAddSuggestion();
        }

        private void CommandRemoveGenericSuggestion_NestedExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction) && !string.IsNullOrWhiteSpace(CommentGenericSuggestionAction))
            {
                var objGenericSuggestionDel = new GenericSuggestionVM();
                objGenericSuggestionDel.ParentCasID = SelectedNestedParent_VM.ParentCasID;
                objGenericSuggestionDel.ChildCas = SelectedNestedParent_VM.ChildCas;
                objGenericSuggestionDel.ChildCasID = SelectedNestedParent_VM.ChildCasID;
                objGenericSuggestionDel.ParentCas = SelectedNestedParent_VM.ParentCas;
                objGenericSuggestionDel.UserID = Environment.UserName;
                objGenericSuggestionDel.IsExclude = SelectedNestedParent_VM.IsExclude;
                objGenericSuggestionDel.Note = CommentGenericSuggestionAction;

                _objGeneric_Service.RemoveGenericSuggestion(objGenericSuggestionDel);
                _notifier.ShowSuccess("Generic Suggestion Removed successfully");
                IsShowPopup_Nested = false;
                NotifyPropertyChanged("IsShowPopup_Nested");
                ClearVariableAddSuggestion();
                BindGenericSuggestionForReviewGrid();
            }
            else
            {

                _notifier.ShowWarning("Comment is mandatory for perform this action.");
            }
        }
        private void ShowCommentParentAndChild_Nested()
        {
            BindListItemComments_Review(SelectedNestedParent_VM.ParentCasID, SelectedNestedParent_VM.ChildCasID);
            BindListItemParentComments_Review(SelectedNestedParent_VM.ParentCasID);
            ResetVisibilityPopUpNested();
            contentVisibilityListComments_Nested = Visibility.Visible;
            NotifyPropertyChanged("contentVisibilityListComments_Nested");
            IsShowPopup_Nested = true;
            NotifyPropertyChanged("IsShowPopup_Nested");
            CommentGenericSuggestionAction_Review = string.Empty;
            NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
            TitleCommentHeader_Review = "Generic Comments for Parent = '" + SelectedNestedParent_VM.ParentCas + "' and Child = '" + SelectedNestedParent_VM.ChildCas + "'";
            NotifyPropertyChanged("TitleCommentHeader_Review");
        }

        private void CommandAddAdditionalComment_NestedExecute(object obj)
        {
            string type = (string)obj;
            GenericSuggestionVM objGenericComment = new GenericSuggestionVM();
            if (type == "ParentChild")
            {
                objGenericComment.ParentCasID = SelectedNestedParent_VM.ParentCasID;
                objGenericComment.ChildCasID = SelectedNestedParent_VM.ChildCasID;
                objGenericComment.Note = CommentGenericSuggestionAction_Review;
            }
            else if (type == "Parent")
            {

                objGenericComment.ChildCasID = (int)SelectedNestedParent_VM.ParentCasID;
                objGenericComment.ParentComment = CommentGenericSuggestionAction_Review;
            }
            objGenericComment.UserID = Environment.UserName;
            _objGeneric_Service.InsertAdditionalComment(objGenericComment, type);
            BindListItemComments_Review(SelectedNestedParent_VM.ParentCasID, SelectedNestedParent_VM.ChildCasID);
            BindListItemParentComments_Review(SelectedNestedParent_VM.ParentCasID);
            _notifier.ShowSuccess("Additonal Comment Added Successfully");
            CommentGenericSuggestionAction_Review = string.Empty;
            NotifyPropertyChanged("CommentGenericSuggestionAction_Review");
        }
        public void ShowApproveRejectGenericsSuggestion_Nested(bool isApprove, bool isReject)
        {
            if (Engine.IsApproveRejectGenericSuggestion == true)
            {
                ResetVisibilityPopUpNested();
                CommentGenericSuggestionAction = string.Empty;
                IsReject = isReject;
                IsApprove = isApprove;
                IsShowPopup_Nested = true;
                contentVisibiltyComment_Nested = Visibility.Visible;
                NotifyPropertyChanged("contentVisibiltyComment_Nested");
                NotifyPropertyChanged("IsShowPopup_Nested");
            }
            else
            {
                _notifier.ShowError("User not authorized to perform this action!");
            }
        }
        public GenericSuggestionVM MapVMToBusinessVM_Nested(GenericNestedParent_Tab_VM objReviewVM)
        {
            GenericSuggestionVM genericSuggestionVM = new GenericSuggestionVM();
            genericSuggestionVM.ChildCas = objReviewVM.ChildCas;
            genericSuggestionVM.ParentCas = objReviewVM.ParentCas;
            genericSuggestionVM.ChildCasID = (int)objReviewVM.ChildCasID;
            genericSuggestionVM.ParentCasID = objReviewVM.ParentCasID;
            genericSuggestionVM.IsExclude = objReviewVM.IsExclude;
            genericSuggestionVM.UserID = Environment.UserName;
            genericSuggestionVM.Status = objReviewVM.Status;
            genericSuggestionVM.Note = CommentGenericSuggestionAction;
            return genericSuggestionVM;
        }






        #endregion

        #region Rest Generic function
        private void CommandSearchCasFlatGenericsTabExecute(object obj)
        {
            BindFlatGenerics();
        }
        private void CommandGotoFlatGenericsExecute(object obj)
        {
            string casSearchFlatGenerics = (string)obj;
            searchCasFlatGenerics = casSearchFlatGenerics;
            NotifyPropertyChanged("searchCasFlatGenerics");
            selectedSearchItemGeneric = listItemSearchTypeForGeneric.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("listItemSearchTypeForGeneric");
            CommandSearchCasFlatGenericsTab.Execute(obj);
            IsSelectedFlatGenericTab = true;
        }
        private void CommandSearchCasCombinedGenericsTabExecute(object obj)
        {
            BindCombinedGenerics();
        }
        public void BindListSearchTypeFilterQSID()
        {
            //listSearchType=
            var srchTypeList = new List<SelectListItem>();
            srchTypeList.Add(new SelectListItem { Text = "Cas" });
            srchTypeList.Add(new SelectListItem { Text = "Cas ID" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid" });
            srchTypeList.Add(new SelectListItem { Text = "Qsid ID" });
            listItemSearchTypeForGeneric = new ObservableCollection<SelectListItem>(srchTypeList);
            NotifyPropertyChanged("listItemSearchTypeForGeneric");
        }
        public void DefaultSelectionSearchType()
        {
            selectedSearchItemGeneric = listItemSearchTypeForGeneric.Where(x => x.Text == "Cas").FirstOrDefault();
            selectedSearch_Combine = listItemSearchTypeForGeneric.Where(x => x.Text == "Cas").FirstOrDefault();
        }
        public void BindGenericUniqueCircularRef()
        {
            CircularRefGridLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("CircularRefGridLoaderVisibility");
            Task.Run(() =>
            {
                if (!string.IsNullOrEmpty(searchCas_UniqueCircularRef))
                {
                    searchCas_UniqueCircularRef = searchCas_UniqueCircularRef.Replace("-", "");
                }

                ListUniqueCircularRef = _objGeneric_Service.GetAllUniqueCircularRef(selectedSearch_UniqueCircularRef != null ? selectedSearch_UniqueCircularRef.Text : "", searchCas_UniqueCircularRef);
                CircularRefGridLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("CircularRefGridLoaderVisibility");
            });


        }

        public void SwitchTabFindCurrentGenericSuggestion_CircularRef()
        {
            ResetVisibilityPopUpTreeView();
            IsShowPopUpTreeView = false;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            IsSearchFromReviewTab = false;
            SearchCas = SelectedGenericCircularRef_VM.ChildCas;
            NotifyPropertyChanged("SearchCas");
            SelectedItemMatchCondition = ListItemMatchCondition.Where(x => x.Text == "Exact Match").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemMatchCondition");
            SelectedItemSearchFor = listItemSearchFor.Where(x => x.Text == "Cas").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemSearchFor");
            SelectedItemSearchType = listItemSearchType.Where(x => x.Text == "Only Child").FirstOrDefault();
            NotifyPropertyChanged("SelectedItemSearchType");
            CommandSearchCasTree.Execute("");
            IsSelectedTreeTab = true;
            NotifyPropertyChanged("IsSelectedTreeTab");
        }
        private void CommandIsGeneResearchChangeExecute(object obj)
        {
            ResetVisibilityPopUpTreeView();
            IsShowPopUpTreeView = true;
            NotifyPropertyChanged("IsShowPopUpTreeView");
            contentDisableResearchCommentVisibility = Visibility.Visible;
            NotifyPropertyChanged("contentDisableResearchCommentVisibility");

        }
        private void TabChangedEvent(PropertyChangedMessage<string> obj)
        {
            if (!string.IsNullOrEmpty(obj.NewValue) && obj.PropertyName == "SearchCasInGenericTreeTab")
            {

                ResetVisibilityPopUpTreeView();
                IsShowPopUpTreeView = false;
                NotifyPropertyChanged("IsShowPopUpTreeView");
                IsSearchFromReviewTab = false;
                SearchCas = obj.NewValue;
                NotifyPropertyChanged("SearchCas");
                SelectedItemMatchCondition = ListItemMatchCondition.Where(x => x.Text == "Exact Match").FirstOrDefault();
                NotifyPropertyChanged("SelectedItemMatchCondition");
                SelectedItemSearchFor = listItemSearchFor.Where(x => x.Text == "Cas").FirstOrDefault();
                NotifyPropertyChanged("SelectedItemSearchFor");
                SelectedItemSearchType = listItemSearchType.Where(x => x.Text == "Both Child and Parent").FirstOrDefault();
                NotifyPropertyChanged("SelectedItemSearchType");
                CommandSearchCasTree.Execute("");
                IsSelectedTreeTab = true;
                NotifyPropertyChanged("IsSelectedTreeTab");
            }
        }

        public void CommandDisableResearchInTreeViewExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CommentGenericSuggestionAction))
            {
                _objLibraryFunction_Service.PutFlagDisableAdditionalResearch(SelectedTreeViewItem.ChildCasID, 0, "Parent");
                //AddAdditionalComment_ResearchGenTab(SelectedTreeViewItem.ChildCasID, "");
                MapGenericResearchComment_VM objGenericComment = new MapGenericResearchComment_VM();
                objGenericComment.ParentCasId = SelectedTreeViewItem.ChildCasID;
                // objGenericComment.ChildCasId = SelectedMapGenericResearchComment_VM.ChildCasId;
                objGenericComment.Comments = CommentGenericSuggestionAction;
                objGenericComment.UserID = Environment.UserName;
                objGenericComment.ActionType = "Parent";
                _objLibraryFunction_Service.SaveCommentsGenericResearchBackLog(objGenericComment);
                CommentGenericSuggestionAction = string.Empty;
                CommandClosePopUp.Execute("Tree");
            }
            else
            {
                _notifier.ShowError("comment mandatory to add addition comment!");
            }

        }
        private bool CheckCasSubstanceIsGroup(int? casID)
        {
            bool result = false;
            if (casID != null)
            {
                var existingsubstanceTypeID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceType", (int)casID);
                if (existingsubstanceTypeID == 2)
                {
                    result = true;
                }
            }
            return result;
        }
        private void ValidateSubstanceTypeInFlat(string cas)
        {
            if (_objLibraryFunction_Service.ValidateSubstanceTypeInFlatAndHydrate(cas))
            {
                _notifier.ShowError("Substance Type can not change to Single because it active in Generics or Hydrate");
            }
        }
        private void CommandOpenPopUpCscTicketExecute(object obj)
        {
            var treeView = (GenericTreeView)obj;
            ShowPopUpCSCTicketsModel(treeView.ParentGeneric.ParentCas, treeView.ParentGeneric.ChildCas);
        }
        private void ShowPopUpCSCTicketsModel(string _parentCas, string _childCas)
        {
            if (!string.IsNullOrEmpty(_parentCas) && !string.IsNullOrEmpty(_childCas))
            {
                var ticketModelPop = new CSCTickets_ViewModel(_parentCas, _childCas); ;
                CSCTickets_WindowPopUP objWindow = new CSCTickets_WindowPopUP();
                objWindow.DataContext = ticketModelPop;
                objWindow.Show();
            }
            else
            {
                _notifier.ShowError("Parent Cas :" + _parentCas + " and Child Cas: " + _childCas + " should not empty!");
            }
        }
        private void CommandMarkGroupAsClosedExecute(object obj)
        {
            try
            {
                var treeView = (GenericTreeView)obj;
                _objLibraryFunction_Service.MarkGenericClosedGorup((int)treeView.ParentGeneric.ChildCasID);
                _notifier.ShowSuccess(treeView.ParentGeneric.ChildCas + " marked as a closed group sucessfully!");
                treeView.ParentGeneric.IsCasMarkedClosed = true;
            }
            catch (Exception ex)
            {
                _notifier.ShowError("Error: " + ex.Message);
            }
        }
        private void CommandRevertClosedGroupExecute(object obj)
        {
            try
            {
                var treeView = (GenericTreeView)obj;
                _objLibraryFunction_Service.DeleteMarkGenericClosedGorup((int)treeView.ParentGeneric.ChildCasID);
                _notifier.ShowSuccess(treeView.ParentGeneric.ChildCas + " revert marked as a closed group sucessfully!");
                treeView.ParentGeneric.IsCasMarkedClosed = false;
            }
            catch (Exception ex)
            {
                _notifier.ShowError("Error: " + ex.Message);
            }
        }
        #endregion

        #region Closed Group 
        public Visibility ClosedGroupLoaderVisibility { get; set; } = Visibility.Collapsed;
        public CommonDataGrid_ViewModel<GenericSuggestionClosedGroup> commonDataGrid_ClosedGroup;
        private List<CommonDataGridColumn> GetListGridColumnClosedGroup()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS Id", ColBindingName = "ParentCASID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "No Direct Children", ColBindingName = "NoDirectChildren", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ClosedBy", ColBindingName = "ClosedBy", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ClosedOn", ColBindingName = "ClosedOn", ColType = "Textbox", ColumnWidth = "200" });


            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "RowID", ColType = "Button", ColumnWidth = "100", ContentName = "Revert", BackgroundColor = "#ff414d", CommandParam = "" });
            return listColumnQsidDetail;
        }
        public void GetAllClosedGroup()
        { 
        
        }


        #endregion
    }
}
