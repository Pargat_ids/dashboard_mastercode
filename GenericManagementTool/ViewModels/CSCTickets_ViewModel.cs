﻿using EntityClass;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ViewModel;

namespace GenericManagementTool.ViewModels
{
    public class CSCTickets_ViewModel : Base_ViewModel
    {
        public ObservableCollection<Log_VersionControl_Tickets_VM> listGenericCSCTickets { get; set; } = new ObservableCollection<Log_VersionControl_Tickets_VM>();
   
        public ICommand CommandAddTicketNumber { get; private set; }
        public ICommand CommandDeleteTicketNumber { get; private set; }
        public ICommand CommandCloseCSCTicketsPopUp { get; private set; }
        public ICommand CommandInsertCSCTickets { get; private set; }
        public bool AddCSCTicketIsOpenPopUp { get; private set; }
        public string ParentCas { get; private set; }
        public string ChildCas { get; private set; }

        public CSCTickets_ViewModel(string _parentCas, string _childCas)
        {
            ParentCas = _parentCas;
            ChildCas = _childCas;
           
            CommandAddTicketNumber = new RelayCommand(CommandAddTicketsExecute);
            CommandDeleteTicketNumber = new RelayCommand(CommandDeleteTicketExecute);
            CommandCloseCSCTicketsPopUp = new RelayCommand(CommandCloseCSCTicketsExecute);
            CommandInsertCSCTickets = new RelayCommand(CommandInsertCSCTicketsExecute);
            BindExistingTicketList(ParentCas, ChildCas);
        }
       

        public void BindExistingTicketList(string parentCas, string Cas)
        {
            var listTickets = _objLibraryFunction_Service.GetGenericTickets(parentCas, ChildCas);
            listGenericCSCTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(listTickets.Select(x => new Log_VersionControl_Tickets_VM { Ticket_Number = x.CSC_Ticket_Number, Ticket_Url = x.CSC_Ticket_Url, CreatedDate = DateTime.Now }).ToList());
            BindTicketList();
        }

        public void AddGenericsCscTickets(string parentCas, string childCas)
        {
            var listTickets = listGenericCSCTickets.Where(x => !string.IsNullOrWhiteSpace(x.Ticket_Url) && !string.IsNullOrEmpty(x.Ticket_Url)).Select(x =>
            new Log_Map_Generics_CSCTickets { CSC_Ticket_Number = x.Ticket_Number, CSC_Ticket_Url = x.Ticket_Url }).ToList();
            _objLibraryFunction_Service.AddUpdateLog_Generics_CSCTickets(listTickets, parentCas, childCas);
            _notifier.ShowSuccess("Add generics csc tickets successfully");
        }
        private void CommandDeleteTicketExecute(object obj)
        {
            string tNum = (string)obj;
            listGenericCSCTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(listGenericCSCTickets.Where(x => x.Ticket_Number != tNum).ToList());
            BindTicketList();
        }
        private void CommandAddTicketsExecute(object obj)
        {
            if (!listGenericCSCTickets.Any(x => x.Ticket_Number == null))
            {
                if (!listGenericCSCTickets.GroupBy(x => x.Ticket_Number).Any(grp => grp.Count() > 1))
                {
                    listGenericCSCTickets.Add(new Log_VersionControl_Tickets_VM { Ticket_Number = null, Ticket_Url = String.Empty, CreatedDate = DateTime.Now });
                    BindTicketList();
                }
                else
                {
                    _notifier.ShowWarning("New ticket not added due existing ticket is duplicate!");
                }
            }
            else
            {
                _notifier.ShowWarning("New work order not added due existing work order is empty or null!");
            }
        }

        public void BindTicketList()
        {
            if (listGenericCSCTickets.Count == 0)
            {
                listGenericCSCTickets.Add(new Log_VersionControl_Tickets_VM { Ticket_Number = null, Ticket_Url = String.Empty, CreatedDate = DateTime.Now });
            }
            listGenericCSCTickets = new ObservableCollection<Log_VersionControl_Tickets_VM>(listGenericCSCTickets.OrderByDescending(x => x.CreatedDate));
            NotifyPropertyChanged("listGenericCSCTickets");

        }
        private void CommandCloseCSCTicketsExecute(object obj)
        {
            AddCSCTicketIsOpenPopUp = false;
            NotifyPropertyChanged("AddCSCTicketIsOpenPopUp");
            
        }
        private void CommandInsertCSCTicketsExecute(object obj)
        {
            InsertUpdateCSCTicket_Generics();
           
           
        }

        private void InsertUpdateCSCTicket_Generics()
        {
            var listMapTickets = listGenericCSCTickets.Where(x => !string.IsNullOrEmpty(x.Ticket_Number) && !string.IsNullOrWhiteSpace(x.Ticket_Number))
                .Select(x => new Log_Map_Generics_CSCTickets()
                {
                    CSC_Ticket_Number = x.Ticket_Number,
                    CSC_Ticket_Url = x.Ticket_Url,
                    CreatedBy = Environment.UserName
                }).ToList();
            _objLibraryFunction_Service.AddUpdateLog_Generics_CSCTickets(listMapTickets, ParentCas, ChildCas);
            _notifier.ShowSuccess("Generics CSC tickets updated successfully!");
        }
    }
}
