﻿using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using CRA_DataAccess;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace GenericManagementTool.ViewModels
{
    public class GenericTreeView : Base_ViewModel
    {
        public GenericSuggestionTree_ViewModel ParentGeneric { get; set; }
        private string _parentCasName { get; set; }
        public string ParentCasName
        {
            get { return _parentCasName; }
            set
            {
                if (_parentCasName != value)
                {
                    _parentCasName = value;
                    NotifyPropertyChanged("ParentCasName");
                }
            }
        }

        public int? ParentCasNameID { get; set; }
        private int? _ChildCount { get; set; }
        public int? ChildCount { get { return _ChildCount; } set { _ChildCount = value; NotifyPropertyChanged("ChildCount"); } }
        public int? ActiveListCount { get; set; }
        public ObservableCollection<GenericTreeView> SubGeneric { get; set; }
        private bool _IsExpanded { get; set; }
        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set
            {              
                _IsExpanded = value;
                if (_IsExpanded)
                {
                    this.SubGeneric = new ObservableCollection<GenericTreeView>(GenericTreeOperation.GetGenericManagementTree(this.ParentGeneric.ChildCasID, this.ParentGeneric.ParentCasID, "", "", this.ParentGeneric.XPath,this.ChildNodeSortField,this.ChildNodeSortType,this.ChildNodeSearchCas,this.ChildNodeSearchCheicalName));
                }
                else
                {
                    if (this.ChildCount > 0)
                    {
                        this.SubGeneric = new ObservableCollection<GenericTreeView>(GenericTreeOperation.GetDummyChildren());
                    }
                }
                NotifyPropertyChanged("SubGeneric");
                NotifyPropertyChanged("IsExpanded");
            }
        }
        private bool _isSelected { get; set; }
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }
        private bool isFiltered { get; set; } = false;
        public bool IsFiltered
        {
            get { return isFiltered; }
            set
            {
                if (value != isFiltered)
                {
                    isFiltered = value;
                    NotifyPropertyChanged("IsFiltered");
                }
            }
        }
        public bool IsCircularRef { get; set; } = false;
        private bool _isDeleted { get; set; }
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (_isDeleted != value)
                {
                    _isDeleted = value;
                    NotifyPropertyChanged("IsDeleted");
                }
            }
        }
        private string _ChildNodeSortField { get; set; } = "";
        private string _ChildNodeSortType { get; set; } = "";
        private string _ChildNodeSearchCas { get; set; } = "";
        private string _ChildNodeSearchCheicalName { get; set; } = "";
        public string ChildNodeSortField { get { return _ChildNodeSortField; } set {
                _ChildNodeSortField = value;
                NotifyPropertyChanged("ChildNodeSortField");
            } }
        public string ChildNodeSortType { get { return _ChildNodeSortType; } set
            {
                _ChildNodeSortType = value;
                NotifyPropertyChanged("ChildNodeSortType");
            }
        }
        public string ChildNodeSearchCas { get { return _ChildNodeSearchCas; } set
            {
                _ChildNodeSearchCas = value;
                NotifyPropertyChanged("ChildNodeSearchCas");
            }
        }
        public string ChildNodeSearchCheicalName
        {
            get { return _ChildNodeSearchCheicalName; }
            set
            {
                _ChildNodeSearchCheicalName = value;
                NotifyPropertyChanged("ChildNodeSearchCheicalName");
            }
        }
    }

    public static class GenericTreeOperation
    {
        public static ILibraryFunction _objLibraryFunction_Service { get; private set; } = LibraryFunction.GetInstance;
        public static List<GenericTreeView> GetGenericManagementTree(int? childCasID, int? parentCasID, string searchKeyword, string searchType, string existingXPath,string SortField="",string SortType="",string casSearchChild="",string casSearchChildByName="")
        {
            List<GenericTreeView> listGenericTree = new List<GenericTreeView>();
            var confirmedGenericSuggestion = _objLibraryFunction_Service.GetCollectionGenericSuggestionTreeView(childCasID, searchKeyword, searchType);
            listGenericTree = GetChildren(confirmedGenericSuggestion, searchKeyword, searchType, childCasID, parentCasID, existingXPath,SortField,SortType,casSearchChild, casSearchChildByName);
            return listGenericTree;
        }
        public static List<GenericTreeView> GetChildren(List<GenericSuggestionVM> confirmedGenericSuggestion, string searchKeyword, string searchType, int? parentId = null, int? oldParentId = null, string existingXPath = "", string SortField = "", string SortType = "", string casSearchChild = "", string casSearchChildByName = "")
        {
            List<GenericTreeView> listGenericTreeView = new List<GenericTreeView>();
            List<GenericSuggestionVM> listChildOfParent = new List<GenericSuggestionVM>();
            if (!string.IsNullOrEmpty(SortType) && !string.IsNullOrEmpty(SortField))
            {
                if (SortType == "Descending")
                {
                    if (SortField == "Date Of Approved")
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderByDescending(x => x.Timestamp).ToList();
                    }
                   else if (SortField == "Chemical Name")
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderByDescending(x => x.ChemicalName).ToList();
                    }
                    else
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderByDescending(x => x.ChildCas).ToList();
                    }
                }
                else
                {
                    if (SortField == "Date Of Approved")
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderBy(x => x.Timestamp).ToList();
                    }
                    else if (SortField == "Chemical Name")
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderBy(x => x.ChemicalName).ToList();
                    }
                    else
                    {
                        listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderBy(x => x.ChildCas).ToList();
                    }
                }
            }
            else
            {
                listChildOfParent = confirmedGenericSuggestion.Where(c => c.ParentCasID == parentId).OrderBy(x => x.ChildCas).ToList();
            }
            if (!string.IsNullOrEmpty(casSearchChild)) {
                listChildOfParent = listChildOfParent.Where(x => x.ChildCas.Contains(casSearchChild)).ToList();
            }
            if (!string.IsNullOrEmpty(casSearchChildByName))
            {
                listChildOfParent = listChildOfParent.Where(x => x.ChemicalName.ToLower().Contains(casSearchChildByName.ToLower())).ToList();
            }
            foreach (var item in listChildOfParent)
            {
                var ParentGeneric = GetParent(item, existingXPath);
                var SubGeneric = new ObservableCollection<GenericTreeView>();
                if (item.ChildrenCount > 0)
                {
                    SubGeneric = new ObservableCollection<GenericTreeView>(GetDummyChildren());
                }

                listGenericTreeView.Add(new GenericTreeView
                {
                    ParentGeneric = ParentGeneric,
                    SubGeneric = SubGeneric,
                    ChildCount = item.ChildrenCount,
                    ParentCasName = item.ChemicalName,
                    ParentCasNameID = item.ChemicalNameID,
                    ActiveListCount = (item.ActiveListCount == 0 ? (int?)null : item.ActiveListCount),
                    IsCircularRef = (oldParentId != null && oldParentId == item.ChildCasID) ? true : false
                });
            }

            return listGenericTreeView;
        }
        public static List<GenericTreeView> GetDummyChildren()
        {
            var listTreeNode = new List<GenericTreeView>();
            GenericTreeView objTree = new GenericTreeView();
            listTreeNode.Add(objTree);
            return listTreeNode;
        }
        public static GenericSuggestionChildDetail_VM GetChildrenCount(string searchKeyword, string searchType, int? parentCasId = null)
        {
            return _objLibraryFunction_Service.GetCollectionChildrenCount(parentCasId, searchKeyword, searchType);

        }
        private static GenericSuggestionTree_ViewModel GetParent(GenericSuggestionVM genericSugesstion, string existingXPath)
        {
            GenericSuggestionTree_ViewModel objGeneric = new GenericSuggestionTree_ViewModel();
            objGeneric.ChildCas = genericSugesstion.ChildCas;
            objGeneric.ParentCas = genericSugesstion.ParentCas;
            objGeneric.ChildCasID = genericSugesstion.ChildCasID;
            objGeneric.ParentCasID = genericSugesstion.ParentCasID;
            objGeneric.Status = genericSugesstion.Status;
            objGeneric.IsExclude = genericSugesstion.IsExclude ?? false;
            objGeneric.XPath = (!string.IsNullOrEmpty(existingXPath) ? existingXPath + "/" : "") + genericSugesstion.ChildCasID;
            objGeneric.FlatGenericsCount = genericSugesstion.FlatGenericsCount;
            objGeneric.Level = (!string.IsNullOrEmpty(existingXPath) ? (existingXPath.Split('/').Length + 1) : 1);
            objGeneric.IsParentGenCalculated = genericSugesstion.IsCasGenCalculated;
            objGeneric.IsCasMarkedClosed = genericSugesstion.IsCasMarkedClosed;
            return objGeneric;
        }

    }
}
