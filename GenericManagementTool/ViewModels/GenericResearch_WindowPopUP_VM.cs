﻿using CRA_DataAccess.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace GenericManagementTool.ViewModels
{
    public class GenericResearch_WindowPopUP_VM : Base_ViewModel
    {
        private string _parentCasGenericRes { get; set; }
        private string _childCasGenericRes { get; set; }
        public string ParentCasGenericRes { get { return _parentCasGenericRes; } set { _parentCasGenericRes = value; NotifyPropertyChanged("ParentCasGenericRes"); } }
        public string ChildCasGenericRes { get { return _childCasGenericRes; } set { _childCasGenericRes = value; NotifyPropertyChanged("ChildCasGenericRes"); } }
        public string CommentGenericSuggestionAction { get; set; }
        public string genericResearchCommentAction { get; set; }
        public DateTime? additionalResearchDeadline { get; set; }
        public DateTime displayStartDate { get; set; } = DateTime.Now;
        public ObservableCollection<ListCommonComboBox_ViewModel> listUser_Res { get; set; } = new ObservableCollection<ListCommonComboBox_ViewModel>();
        public ListCommonComboBox_ViewModel selectedUser_Res { get; set; }
        public int? existingCommentID { get; set; }
        public ICommand CommandAddNewGenericResCas { get; set; }
        public GenericResearch_WindowPopUP_VM(GenericResearchComment_VM _objGeneric,string _researchAction)
        {
            ParentCasGenericRes = _objGeneric.ParentCas;
            ChildCasGenericRes = _objGeneric.ChildCas;
            CommentGenericSuggestionAction = _objGeneric.Comments;
            additionalResearchDeadline = _objGeneric.AdditionalResearchDeadline;

            genericResearchCommentAction = _researchAction;
            CommandAddNewGenericResCas = new RelayCommand(CommandAddNewGenericResCasExecute);
            BindListUser();
            if (_objGeneric.AssignedUserName != null)
            {
                selectedUser_Res = listUser_Res.Where(x => x.Id == _objGeneric.AssignedUserName.Id).FirstOrDefault();
                NotifyPropertyChanged("selectedUser_Res");
            }
            existingCommentID = _objGeneric.CommentID;
        }
        public void BindListUser()
        {
            listUser_Res = new ObservableCollection<ListCommonComboBox_ViewModel>(
                Engine._listCheckedOutUser.Select(x => new ListCommonComboBox_ViewModel()
                {
                    Id = x.Id,
                    Id_String = x.Id_String,
                    Name = x.Name,

                }).ToList()
                );
        }
        private void CommandAddNewGenericResCasExecute(object obj)
        {
            if (!string.IsNullOrEmpty(ParentCasGenericRes) && !string.IsNullOrEmpty(CommentGenericSuggestionAction))
            {                
                AddAdditionalComment_ResearchGenTabNew(ParentCasGenericRes, ChildCasGenericRes, CommentGenericSuggestionAction);
                MessengerInstance.Send<NotificationMessageAction<GenericResearchComment_VM>>(new NotificationMessageAction<GenericResearchComment_VM>("notification message", CallbackExecute));
            }
            else
            {
                _notifier.ShowError("cas and comment mandatory to add addition comment!");
            }
        }

        private void CallbackExecute(GenericResearchComment_VM obj)
        {
            
        }

        public void AddAdditionalComment_ResearchGenTabNew(string parentCas, string childCas, string isAdditionalFlag)
        {
            string actionType = string.Empty;
            int? parentCasID = _objLibraryFunction_Service.GetCasIdByCas(parentCas);
            int? childCasID = _objLibraryFunction_Service.GetCasIdByCas(childCas);
            if (string.IsNullOrEmpty(parentCas))
            {
                _notifier.ShowError("Add research flag faild due to parent cas is mandatory!");
                return;
            }
            if (!string.IsNullOrEmpty(parentCas) && parentCasID == null)
            {
                _notifier.ShowError("Add research flag faild due to parent cas is not exist in DB!");
                return;
            }
            if (!string.IsNullOrEmpty(childCas)&& childCasID==null)
            {
                _notifier.ShowError("Add research flag faild due to child cas is not exist in DB!");
                return;
            }
            if (isAdditionalFlag == "Add" && !CheckCasSubstanceIsGroup(parentCasID))
            {
                _notifier.ShowError("Add research flag faild due to cas substance type is not 'Group'");
                return;
            }
            MapGenericResearchComment_VM objGenericComment = new MapGenericResearchComment_VM();
            if (!string.IsNullOrEmpty( childCas))
            {
                objGenericComment.ParentCasId = (int)parentCasID;
                objGenericComment.ChildCasId = childCasID;
                actionType = "ParentChild";
            }
            else
            {
                objGenericComment.ParentCasId = (int)parentCasID;
                actionType = "Parent";
            }
            objGenericComment.Comments = CommentGenericSuggestionAction;
            objGenericComment.UserID = Environment.UserName;
            objGenericComment.AssignedUserId = selectedUser_Res.Id_String;
            objGenericComment.IsNeedMoreResearch = true;
            objGenericComment.AdditionalResearchDeadline = additionalResearchDeadline;
            objGenericComment.ActionType = actionType;
            objGenericComment.CommentID = existingCommentID!=null?(int)existingCommentID:0;
            _objLibraryFunction_Service.SaveCommentsGenericResearchBackLog(objGenericComment);
            _notifier.ShowSuccess("Additonal Comment Added Successfully");
            CommentGenericSuggestionAction = string.Empty;
            ParentCasGenericRes = string.Empty;
            NotifyPropertyChanged("ParentCasGenericRes");
            ChildCasGenericRes = string.Empty;
            NotifyPropertyChanged("ParentCasGenericRes");
        }
        private bool CheckCasSubstanceIsGroup(int? casID)
        {
            bool result = false;
            if (casID != null)
            {
                var existingsubstanceTypeID = _objGeneric_Service.Get_Map_Generic_Detail_IDs("substanceType", (int)casID);
                if (existingsubstanceTypeID == 2)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
