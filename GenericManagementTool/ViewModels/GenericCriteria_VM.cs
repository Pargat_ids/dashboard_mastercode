﻿using EntityClass;
using GenericManagementTool.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ViewModel;

namespace GenericManagementTool.ViewModels
{
   public class GenericCriteria_VM: Base_ViewModel
    {
        int _criteriaNumber { get; set; }
        public int CriteriaNumber { get { return _criteriaNumber; } set {
                _criteriaNumber = value;
                NotifyPropertyChanged("CriteriaNumber");
            } }
        public int Qsid_ID { get; set; }

         Library_QSIDs _selectedQsid { get; set; }
        public Library_QSIDs SelectedQsid { get {
                return _selectedQsid;
            } set {
                if (value != null)
                {
                    _selectedQsid = value;
                    Qsid_ID = _selectedQsid.QSID_ID;
                    NotifyPropertyChanged("SelectedQsid");
                    GetAllNormalizedFieldName(Qsid_ID);
                }
                
            } }
        public bool IsUniqueCas { get { return _isUniqueCas; } set {
                _isUniqueCas = value;
                NotifyPropertyChanged("IsUniqueCas");
            } }
        private bool _isUniqueCas { get; set; } = true;
        public bool IsAdditionalCheck { get; set; } = false;
        public int FieldNameID { get; set; }
        public ObservableCollection<LibraryFieldName_Generic> listFieldNames { get; set; }
        public LibraryFieldName_Generic SelectFieldName { get {
                return _selectFieldNames;
            } set {
                if (value != null)
                {
                    _selectFieldNames = value;
                    FieldNameID = _selectFieldNames.FieldNameID;
                    NotifyPropertyChanged("SelectFieldName");
                    IsAdditionalCheck = _selectFieldNames.IsThresholdCat;
                    NotifyPropertyChanged("IsAdditionalCheck");
                }
            } }
        LibraryFieldName_Generic _selectFieldNames { get; set; }
         SelectListItem _selectedCriteria_Generics { get; set; }
        public SelectListItem SelectedCriteria_Generics { get {
                return _selectedCriteria_Generics;
            } set {
                if (value != null)
                {
                    _selectedCriteria_Generics = value;
                    CriteriaType = _selectedCriteria_Generics.Text;
                    NotifyPropertyChanged("SelectedCriteria_Generics");
                }
            } }
        SelectListItem _selectedAdditional_Generics { get; set; }
        public SelectListItem SelectedAdditional_Generics
        {
            get
            {
                return _selectedAdditional_Generics;
            }
            set
            {
                if (value != null)
                {
                    _selectedAdditional_Generics = value;
                    CriteriaType = _selectedAdditional_Generics.Text;
                    NotifyPropertyChanged("SelectedAdditional_Generics");
                }
            }
        }
        SelectListItem _selectedNextCondition_Generics { get; set; }
        public SelectListItem SelectedNextCondition_Generics
        {
            get
            {
                return _selectedNextCondition_Generics;
            }
            set
            {
                if (value != null)
                {
                    _selectedNextCondition_Generics = value;
                    NextCondition = _selectedNextCondition_Generics.Text;
                    NotifyPropertyChanged("SelectedNextCondition_Generics");
                }
            }
        }
        public string CriteriaType { get; set; }
        public string CriteriaValue { get; set; }
        public string NextCondition { get; set; }
        public int? GroupCriteriaNo { get; set; }

        private void GetAllNormalizedFieldName(int qsid_Id) {
            listFieldNames = new ObservableCollection<LibraryFieldName_Generic>(_objLibraryFunction_Service.GetAllNormalizedFieldName(qsid_Id));
            NotifyPropertyChanged("listFieldNames");
        }
    }
    public class GenericCalculated {
        public string Cas { get; set; }
        public int Qsid { get; set; }
        public bool IsUniqueCas { get; set; }
        public string FieldName { get; set; }
        public string CriteriaType { get; set; }
        public string CriteriaValue { get; set; }
        public string NextCondition { get; set; }
        public int? GroupCriteriaNo { get; set; }
        
    }
}
