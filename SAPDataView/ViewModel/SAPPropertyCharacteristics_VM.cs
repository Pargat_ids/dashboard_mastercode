﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;


namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string SAP_CharacteristicCode { get; set; }
        public string SAP_PropertyCodePC { get; set; }
        public int PropertyID { get; set; }
        public int CharacteristicsID { get; set; }
        public int? SortOrderInProperty { get; set; }
        public List<LibProperty> listTypeHeader { get; set; }
        public List<LibCharacteristic> listTypeHeaderChar { get; set; }

        private LibProperty _SelectedTypeHeader { get; set; }
        public LibProperty SelectedTypeHeader
        {
            get => _SelectedTypeHeader;
            set
            {
                if (Equals(_SelectedTypeHeader, value))
                {
                    return;
                }

                _SelectedTypeHeader = value;
                NotifyPropertyChanged("SelectedTypeHeader");
            }
        }
        private LibCharacteristic _SelectedTypeHeaderChar { get; set; }
        public LibCharacteristic SelectedTypeHeaderChar
        {
            get => _SelectedTypeHeaderChar;
            set
            {
                if (Equals(_SelectedTypeHeaderChar, value))
                {
                    return;
                }

                _SelectedTypeHeaderChar = value;
                NotifyPropertyChanged("SelectedTypeHeaderChar");
            }
        }
        public Visibility VisibilityPropCharDashboard_Loader { get; set; }
        public CommonDataGrid_ViewModel<MapPropChar> comonPropChar_VM { get; set; }
        public ICommand CommandClosePopUpPropChar { get; private set; }
        public ICommand CommandShowPopUpAddPropChar { get; private set; }
        public MapPropChar SelectedPropChar { get; set; }

        public bool IsEditPropChar { get; set; } = true;
        public Visibility PopUpContentAddPropCharVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalPropCharVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalAnPropCharVisibility { get; set; } = Visibility.Collapsed;

        private ObservableCollection<MapPropChar> _lstPropChar { get; set; } = new ObservableCollection<MapPropChar>();
        public ObservableCollection<MapPropChar> lstPropChar
        {
            get { return _lstPropChar; }
            set
            {
                if (_lstPropChar != value)
                {
                    _lstPropChar = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapPropChar>>>(new PropertyChangedMessage<List<MapPropChar>>(null, _lstPropChar.ToList(), "Default List"));
                }
            }
        }
        private void RunPropChar()
        {
            BindPropChar();

        }

        private List<CommonDataGridColumn> GetListGridColumnPropChar()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAP Property Code", ColBindingName = "SAP_Property_Code", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAP Characteristic Code", ColBindingName = "SAP_Characteristic_Code", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SortOrderInProperty", ColBindingName = "SortOrderInProperty", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapPropChar" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNamePropChar" });

            return listColumnQsidDetail;
        }


        private void BindPropChar()
        {
            VisibilityPropCharDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityPropCharDashboard_Loader");
            Task.Run(() =>
            {
                var SAPPropCharTable = objCommon.DbaseQueryReturnSqlList<MapPropChar>("select a.MapPropertyCharacteristicID, a.PropertyID, a.CharacteristicID, b.SAP_Property_Code,c.SAP_Characteristic_Code, a.SortOrderInProperty from SAP.Map_Property_Characteristics a, SAP.Library_SAP_Properties b, SAP.Library_SAP_Characteristics c where a.PropertyID = b.PropertyID and a.CharacteristicID = c.CharacteristicID", Win_Administrator_VM.CommonListConn);
                lstPropChar = new ObservableCollection<MapPropChar>(SAPPropCharTable);
            });
            NotifyPropertyChanged("comonPropChar_VM");
            VisibilityPropCharDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityPropCharDashboard_Loader");
        }

        private void CommandClosePopUpExecutePropChar(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpPropChar = false;

        }
        private void CommandShowPopUpAddPropCharExecute(object obj)
        {
            ClearPopUpVariablePropChar();
            IsEditPropChar = true;
            NotifyPropertyChanged("IsEditPropChar");
            IsShowPopUpPropChar = true;
            PopUpContentAddPropCharVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddPropCharVisibility");
        }
        private void NotifyMePropChar(PropertyChangedMessage<MapPropChar> obj)
        {
            SelectedPropChar = obj.NewValue;
        }

        public void ClearPopUpVariablePropChar()
        {
            SAP_PropertyCodePC = string.Empty;
            SAP_CharacteristicCode = string.Empty;
            PropertyID = 0;
            CharacteristicsID = 0;
            NotifyPropertyChanged("SAP_PropertyCodePC");
            NotifyPropertyChanged("SAP_CharacteristicCode");
            NotifyPropertyChanged("PropertyID");
            NotifyPropertyChanged("CharacteristicsID");
            SelectedTypeHeader = null;
            SelectedTypeHeaderChar = null;
            NotifyPropertyChanged("SelectedTypeHeader");
            NotifyPropertyChanged("SelectedTypeHeaderChar");
            SortOrderInProperty = null;
            NotifyPropertyChanged("SortOrderInProperty");

        }
        private void CommandButtonExecutedPropChar(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapPropChar")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this Property-Characateristics?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    objCommon.DbaseQueryReturnStringSQL("Delete from SAP.Map_Property_Characteristics where MapPropertyCharacteristicID =" + SelectedPropChar.MapPropertyCharacteristicID, Win_Administrator_VM.CommonListConn);
                    BindPropChar();
                    _notifier.ShowSuccess("Selected  Property-Characateristics deleted successfully");

                }
            }
            else if (obj.Notification == "AddEditNamePropChar")
            {
                IsEditPropChar = false;
                NotifyPropertyChanged("IsEditPropChar");
                IsShowPopUpPropChar = true;
                PopUpContentAddPropCharVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddPropCharVisibility");
                ListChemicalAnPropCharVisibility = Visibility.Collapsed;
                ListChemicalPropCharVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemicalAnPropCharVisibility");
                NotifyPropertyChanged("ListChemicalPropCharVisibility");
                SelectedTypeHeader = listTypeHeader.Where(x => x.PropertyID == SelectedPropChar.PropertyID).FirstOrDefault();
                SelectedTypeHeaderChar = listTypeHeaderChar.Where(x => x.CharacteristicID == SelectedPropChar.CharacteristicID).FirstOrDefault(); 
                SortOrderInProperty = SelectedPropChar.SortOrderInProperty;
                NotifyPropertyChanged("SortOrderInProperty");
            }
        }
        private bool _IsShowPopUpPropChar { get; set; }
        public bool IsShowPopUpPropChar
        {
            get { return _IsShowPopUpPropChar; }
            set
            {
                if (_IsShowPopUpPropChar != value)
                {
                    _IsShowPopUpPropChar = value;
                    NotifyPropertyChanged("IsShowPopUpPropChar");
                }
            }
        }
        private void RefreshGridPropChar(PropertyChangedMessage<string> flag)
        {
            BindPropChar();
            if (IsShowPopUpPropChar)
            {
                NotifyPropertyChanged("IsShowPopUpPropChar");
            }
        }
        public ICommand CommandAddPropChar { get; private set; }
        private void CommandAddPropCharExecute(object obj)
        {
            SavePropChar();
            IsShowPopUpPropChar = false;
            IsEditPropChar = true;
            NotifyPropertyChanged("IsEditPropChar");
            ClearPopUpVariablePropChar();
            PopUpContentAddPropCharVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddPropCharVisibility");
        }
        private bool CommandAddPropCharCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(PropertyID.ToString()) || string.IsNullOrEmpty(CharacteristicsID.ToString()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SavePropChar()
        {
            Map_Property_Characteristics objPropChar_VM = new Map_Property_Characteristics();
            using (var context = new CRAModel())
            {
                if (!IsEditPropChar)
                {
                    objPropChar_VM = context.Map_Property_Characteristics.Where(x => x.MapPropertyCharacteristicID == SelectedPropChar.MapPropertyCharacteristicID).FirstOrDefault();
                }
                else
                {
                    var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('SAP.Map_Property_Characteristics')", Win_Administrator_VM.CommonListConn));
                    objPropChar_VM.MapPropertyCharacteristicID = mx + 1;
                }
                objPropChar_VM.CharacteristicID = SelectedTypeHeaderChar.CharacteristicID;
                objPropChar_VM.PropertyID = SelectedTypeHeader.PropertyID;
                objPropChar_VM.SortOrderInProperty = SortOrderInProperty;
                if (!IsEditPropChar)
                {
                    var chkAlreadyExist = context.Map_Property_Characteristics.Count(x => x.MapPropertyCharacteristicID != SelectedPropChar.MapPropertyCharacteristicID && x.PropertyID == SelectedTypeHeader.PropertyID && x.CharacteristicID == SelectedTypeHeaderChar.CharacteristicID);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("Property Characteristic Name already exists");
                    }
                    else
                    {
                        context.SaveChanges();
                        _notifier.ShowSuccess("Existing Property Characteristic Saved Successfully.");
                    }
                }
                else
                {
                    var chkAlreadyExist = context.Map_Property_Characteristics.Count(x => x.PropertyID == SelectedTypeHeader.PropertyID && x.CharacteristicID == SelectedTypeHeaderChar.CharacteristicID);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("Property Characteristic Name already exists");
                    }
                    else
                    {
                        context.Map_Property_Characteristics.Add(objPropChar_VM);
                        context.SaveChanges();
                        _notifier.ShowSuccess("New Property Characteristic Saved Successfully.");
                    }
                }
            }
            ClearPopUpVariablePropChar();
            BindPropChar();

        }
        public ICommand CommandAddMorePropChar { get; private set; }
        private void CommandAddMorePropCharExecute(object obj)
        {
            SavePropChar();
        }

    }
    public class MapPropChar
    {
        public int MapPropertyCharacteristicID { get; set; }
        public string SAP_Characteristic_Code { get; set; }
        public string SAP_Property_Code { get; set; }
        public int PropertyID { get; set; }
        public int CharacteristicID { get; set; }
        public int? SortOrderInProperty { get; set; }
    }

    public class LibProperty
    {
        public int PropertyID { get; set; }
        public string SAP_Property_Code { get; set; }
    }

    public class LibCharacteristic
    {
        public int CharacteristicID { get; set; }
        public string SAP_Characteristic_Code { get; set; }
    }
}
