﻿using SAPDataView.Library;
using DataGridFilterLibrary;
using DataGridFilterLibrary.Querying;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DocumentFormat.OpenXml.Packaging;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using SAPDataView.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Input;
using CRA_DataAccess;
using VersionControlSystem.Model.ApplicationEngine;
using System.Diagnostics;

namespace SAPDataView.ViewModel
{
    public class StandardDataGrid_VM : Base_ViewModel
    {
        public bool listAllColVisibleNull { get; set; } = false;
        public ICommand CommmandChangeVisiblityListColNull { get; set; }
        public ObservableCollection<ColumnSelectionDataGrid> ListAllColumnDGNull { get; set; } = new ObservableCollection<ColumnSelectionDataGrid>();
        public ICommand CommandOnColSelectedChangeNull { get; set; }


        private List<ListCheckBoxType_ViewModel> listCheckBox = new List<ListCheckBoxType_ViewModel>();

        public TabItem selectedTabItem { get; set; }
        private CommonFunctions objCommon;
        private RowIdentifier objCurrentView = new RowIdentifier();
        private ObservableCollection<DataGridColumn> _columnCollection = new ObservableCollection<DataGridColumn>(); // generate column of datagrid
        public IList myList { get; set; }
        private DataTable dtDefault; // Access Export and Excel Export Variable
        public IList myListTotal { get; set; }
        public IList myListDataTable { get; set; }
        public DataColumnCollection defaultDataTableColumns { get; set; }
        public Visibility VisibleDataList { get; set; }
        public Visibility VisibleLoder { get; set; }
        public Type listType { get; set; } = typeof(System.String);
        public Action<DataRow> GenericCallbackAction { get; set; }

        public DataRow _objSelectedItem { get; set; }
        public DataRow objSelectedItem
        {
            get { return _objSelectedItem; }
            set
            {
                if (_objSelectedItem != value)
                {
                    _objSelectedItem = value;
                    if (GenericCallbackAction != null)
                    {
                        GenericCallbackAction.Invoke(_objSelectedItem);
                    }
                }
            }
        }
        public object _selectedRowItem { get; set; }
        public object selectedRowItem
        {
            get { return _selectedRowItem; }
            set
            {

                if (value != _selectedRowItem)
                {
                    _selectedRowItem = value;
                    MessengerInstance.Send<PropertyChangedMessage<object>>(new PropertyChangedMessage<object>(selectedRowItem, selectedRowItem, PaginationTitle));
                }
            }
        }
        public string PaginationTitle { get; set; }
        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get
            {
                return this._columnCollection;
            }
            set
            {
                _columnCollection = value;
                NotifyPropertyChanged("ColumnCollection");
            }
        }

        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public ICommand CommandOnColSelectedChange { get; set; }
        public Visibility listAllColVisible { get; set; } = Visibility.Collapsed;
        public ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM> ListAllColumnDG { get; set; } = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
        public ICommand CommandDataGridSorting { get; set; }
        private ListSortDirection? lastSortDirection { get; set; }
        public ICommand CommmandProductField { get; set; }

        public ICommand CommmandDelete { get; set; }
        private string lastSortMemberPath { get; set; }
        public Type CallbackClassType { get; set; }
        public bool IsCreateSaveBtn { get; set; }

        public bool IsCreateDeleteBtn { get; set; }


        public StandardDataGrid_VM(string selectedTestCase, DataTable defaultDataTable, bool _isCreateSaveBtn = false, bool _IsCreateDeleteBtn = false, Type _callbackClassType = null)
        {
            GenericCallbackAction = null;
            if (defaultDataTable != null && defaultDataTable.Rows.Count > 0)
            {
                listCheckBox = new List<ListCheckBoxType_ViewModel>();
                //listCheckBox.Add(new ListCheckBox_ViewModel { Name = (bool?)null });
                listCheckBox.Add(new ListCheckBoxType_ViewModel { ID = 1, Value = true });
                listCheckBox.Add(new ListCheckBoxType_ViewModel { ID = 0, Value = false });

                defaultDataTableColumns = defaultDataTable.Columns;
                BindListAllColumn();
                commandFirst = new RelayCommand(CommandFirstExecute, CommandFirstCanExecute);
                commandPrev = new RelayCommand(CommandPrevExecute, CommandFirstCanExecute);
                commandNext = new RelayCommand(CommandNextExecute, CommandFirstCanExecute);
                commandLast = new RelayCommand(CommandLastExecute, CommandFirstCanExecute);
                CommmandProductField = new RelayCommand(CommmandProductFieldExecute, CommandFirstCanExecute);
                CommmandDelete = new RelayCommand(CommmandDeleteExecute, CommandFirstCanExecute);
                CommGenerateAccessFile = new RelayCommand(CommandAccessExport, CommandFirstCanExecute);
                CommGenerateExcelFile = new RelayCommand(CommandExcelExport, CommandFirstCanExecute);
                ClearAllFilter = new RelayCommand(CommandClearAllFilter, CommandFirstCanExecute);
                CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute, CommandFirstCanExecute);
                CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute, CommandFirstCanExecute);
                CommandDataGridSorting = new RelayCommand(CommandDataGridSortingExecute, CommandDataGridSortingCanExecute);
                BtnCopy = new RelayCommand(CommandCopy, CommandFirstCanExecute);

                CommmandChangeVisiblityListColNull = new RelayCommand(CommmandChangeVisiblityListColNullExecute, CommandFirstCanExecute);
                CommandOnColSelectedChangeNull = new RelayCommand(CommandOnColSelectedChangeNullExecute, CommandFirstCanExecute);

                VisibleLoder = Visibility.Visible;
                NotifyPropertyChanged("VisibleLoder");
                VisibleDataList = Visibility.Collapsed;
                NotifyPropertyChanged("VisibleDataList");
                dtDefault = defaultDataTable;
                Init(selectedTestCase, defaultDataTable);
                PaginationTitle = selectedTestCase;
                IsCreateSaveBtn = _isCreateSaveBtn;
                IsCreateDeleteBtn = _IsCreateDeleteBtn;
                CallbackClassType = _callbackClassType;
                CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;

            }
        }




        private void CommmandChangeVisiblityListColNullExecute(object obj)
        {
            listAllColVisibleNull = !listAllColVisibleNull;
            NotifyPropertyChanged("listAllColVisibleNull");
        }


        private List<string> queryNull { get; set; } = new List<string>();
        private string queryStringForNUll { get; set; }
        private void CommandOnColSelectedChangeNullExecute(object obj)
        {
            ExecuteClearFilter();
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDGNull.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            Type Proptype = null;
            if (selectColName == null)
            {
                Proptype = typeof(string);
            }
            else
            {
                foreach (DataColumn dc in defaultDataTableColumns)
                {
                    if (selectColName.ToLower() == dc.ColumnName.ToLower())
                    {
                        Proptype = dc.DataType;
                        break;
                    }
                }
            }
            if (Proptype != null)
            {
                if (selectCol != null)
                {
                    if (Proptype == typeof(string))
                    {
                        if (selectCol.IsNull)
                        {
                            queryNull.Add(" " + selectColName + ".Length = 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectColName + ".Length = 0 ");
                        }
                        if (selectCol.IsNotNull)
                        {
                            queryNull.Add(" " + selectColName + ".Length > 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectColName + ".Length > 0 ");
                        }
                    }
                    else
                    {
                        if (Proptype == typeof(Boolean))
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectColName + ".Value == false ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + ".Value == false ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectColName + ".Value == true ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + ".Value == true ");
                            }
                        }
                        else
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectColName + " = null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + " = null ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectColName + " != null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + " != null ");
                            }
                        }
                    }
                }
                var result = string.Join("&&", queryNull.ToList());
                queryStringForNUll = result;
                SearchCriteria = result;
                NotifyPropertyChanged("SearchCriteria");
                if (result == string.Empty)
                {
                    queryStringForNUll = string.Empty;
                    var list = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(list.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                    myList = myListTotal;
                }
                else
                {
                    var list = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(list.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(result.Trim()), listType, defaultDataTableColumns, listCheckBox);
                    myList = myListTotal;
                }
                if (myList.Count == 0)
                {
                    myListDataTable = null;
                    lblContent = "";
                    lblTotRecords = "";
                    NotifyPropertyChanged("lblTotRecords");
                    NotifyPropertyChanged("lblContent");
                    NotifyPropertyChanged("myListDataTable");
                }
                else
                {
                    Navigate(0);
                }
            }
        }





        private bool CommandDataGridSortingCanExecute(object obj)
        {
            return true;
        }

        private void CommandDataGridSortingExecute(object obj)
        {
            var e = (DataGridSortingEventArgs)obj;
            e.Handled = true;

            if (e.Column.SortMemberPath == lastSortMemberPath)
            {
                if (lastSortDirection == ListSortDirection.Ascending)
                    e.Column.SortDirection = ListSortDirection.Descending;
                else
                    e.Column.SortDirection = ListSortDirection.Ascending;
            }
            else
                e.Column.SortDirection = ListSortDirection.Ascending;

            string dir = (e.Column.SortDirection == ListSortDirection.Ascending) ? "ASC" : "DESC";
            lastSortDirection = e.Column.SortDirection;
            lastSortMemberPath = e.Column.SortMemberPath;
            SortDefaultList(e.Column.SortMemberPath, dir);

            // In my opinion, this last bit is redundant since data should come already sorted
            //CollectionView.SortDescriptions.Clear();
            //CollectionView.SortDescriptions.Add(new SortDescription(e.Column.SortMemberPath, e.Column.SortDirection));
        }

        private void SortDefaultList(string sortMemberPath, string dir)
        {
            var sortedList = myList.AsQueryable().OrderBy(sortMemberPath + " " + dir);
            var lst = _objLibraryFunction_Service.BindListFromIQuerable(sortedList, listType, defaultDataTableColumns, listCheckBox);
            myList = (IList)lst;
            Navigate(0);
        }

        public void BindListAllColumn()
        {

            var list = new List<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();

            foreach (DataColumn dt in defaultDataTableColumns)
            {
                list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { IsSelected = true, ColumnName = dt.ColumnName });
            }

            ListAllColumnDG = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>(list);
            NotifyPropertyChanged("ListAllColumnDG");

            var listNull = new List<ColumnSelectionDataGrid>();
            foreach (DataColumn dt in defaultDataTableColumns)
            {
                listNull.Add(new ColumnSelectionDataGrid { ColumnName = dt.ColumnName, IsNull = false, IsNotNull = false });
            }

            ListAllColumnDGNull = new ObservableCollection<ColumnSelectionDataGrid>(listNull);
            NotifyPropertyChanged("ListAllColumnDGNull");
        }

        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDG.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            //selectCol.IsSelected = !selectCol.IsSelected;
            var selectColInGrid = ColumnCollection.Where(x => x.Header.ToString() == selectColName).FirstOrDefault();
            if (selectCol.IsSelected)
            {
                selectColInGrid.Visibility = Visibility.Visible;
            }
            else
            {
                selectColInGrid.Visibility = Visibility.Collapsed;
            }
            NotifyPropertyChanged("ColumnCollection");
            CustomQueryController = new QueryController();
            CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;

            // NotifyPropertyChanged("listSelectedColumn");
        }

        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            if (listAllColVisible == Visibility.Collapsed)
            {
                listAllColVisible = Visibility.Visible;
            }
            else
            {
                listAllColVisible = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllColVisible");
        }

        public void Init(string selectedTestCase, DataTable defaultDataTable)
        {
            try
            {
                ExecuteClearFilter();
                objCommon = new CommonFunctions();
                Task.Run(() =>
                {
                    var list = _objLibraryFunction_Service.BindListFromTable(defaultDataTable, listType);
                    //var list2 = _objLibraryFunction_Service.BindListFromTable(defaultDataTable, listType);

                    ExecuteFunction(list);
                });
            }
            catch (Exception ex)
            {
                var abc = ex.Message;
            }
        }
        //public IList BindListFromTable(DataTable dt, Type listType)
        //{
        //    var obj = MyTypeBuilder.CreateNewObject(dt.Columns);
        //    listType = typeof(List<>).MakeGenericType(new[] { obj });
        //    System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
        //    var fields = obj.GetProperties();
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        var ob = Activator.CreateInstance(obj);

        //        foreach (var fieldInfo in fields)
        //        {
        //            foreach (DataColumn dc in dt.Columns)
        //            {
        //                if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
        //                {
        //                    object value = (dr[dc.ColumnName] == DBNull.Value) ? null : dr[dc.ColumnName];
        //                    if (value != null)
        //                    {
        //                        var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
        //                        var result = converter.ConvertFrom(value.ToString());
        //                        fieldInfo.SetValue(ob, result);
        //                    }
        //                    else
        //                    {
        //                        if (fieldInfo.PropertyType == typeof(DateTime))
        //                        {
        //                            fieldInfo.SetValue(ob, (DateTime?)null);
        //                        }
        //                        else
        //                        {
        //                            fieldInfo.SetValue(ob, value);
        //                        }

        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //        lst.Add(ob);
        //    }

        //    return lst;
        //}

        //public IList BindListFromIQuerable(IQueryable listQuerable, Type listType, DataColumnCollection defaultDataTableColumns)
        //{
        //    var obj = MyTypeBuilder.CreateNewObject(defaultDataTableColumns);
        //    listType = typeof(List<>).MakeGenericType(new[] { obj });
        //    IList lst = (IList)Activator.CreateInstance(listType);
        //    var fields = obj.GetProperties();
        //    foreach (var dr in listQuerable)
        //    {
        //        var ob = Activator.CreateInstance(obj);

        //        foreach (var fieldInfo in fields)
        //        {
        //            foreach (DataColumn dc in defaultDataTableColumns)
        //            {
        //                if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
        //                {
        //                    var Proptype = dr.GetType().GetProperty(fieldInfo.Name).PropertyType;
        //                    object value = (dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null) == DBNull.Value) ? null : dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null);
        //                    if (value != null)
        //                    {
        //                        if (Proptype == typeof(string))
        //                        {
        //                            fieldInfo.SetValue(ob, value.ToString());
        //                        }
        //                        else
        //                        {
        //                            var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
        //                            var result = converter.ConvertFrom(value.ToString());
        //                            fieldInfo.SetValue(ob, result);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (Proptype == typeof(string))
        //                        {
        //                            fieldInfo.SetValue(ob, "");
        //                        }
        //                        else
        //                        {
        //                            if (Proptype == typeof(DateTime))
        //                            {
        //                                fieldInfo.SetValue(ob, (DateTime?)null);
        //                            }
        //                            else
        //                            {
        //                                fieldInfo.SetValue(ob, value);
        //                            }
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //        lst.Add(ob);
        //    }

        //    return lst;
        //}
        private void ExecuteFunction(IList lstData)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                myList = lstData;
                var lst = _objLibraryFunction_Service.BindListFromIQuerable(myList.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                myListTotal = (IList)lst;
                if (myList.Count > 0)
                {
                    BindDataGridCas_ComparedByQSID();
                }
            });
            selectedItemCombo = new selectedListItemCombo() { id = 25, text = "25" };
            NotifyPropertyChanged("selectedItemCombo");
            ListComboxItem = new ObservableCollection<selectedListItemCombo>(AddList());
            NotifyPropertyChanged("ListComboxItem");
            VisibleLoder = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleLoder");
            VisibleDataList = Visibility.Visible;
            NotifyPropertyChanged("VisibleDataList");
        }


        //public IList BindListFromIQuerable(IQueryable listQuerable)
        //{
        //    var obj = MyTypeBuilder.CreateNewObject(defaultDataTableColumns);
        //    listType = typeof(List<>).MakeGenericType(new[] { obj });
        //    IList lst = (IList)Activator.CreateInstance(listType);
        //    var fields = obj.GetProperties();
        //    foreach (var dr in listQuerable)
        //    {
        //        var ob = Activator.CreateInstance(obj);

        //        foreach (var fieldInfo in fields)
        //        {
        //            foreach (DataColumn dc in defaultDataTableColumns)
        //            {
        //                if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
        //                {
        //                    var Proptype = dr.GetType().GetProperty(fieldInfo.Name).PropertyType;
        //                    object value = (dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null) == DBNull.Value) ? null : dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null);
        //                    if (value != null)
        //                    {
        //                        var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
        //                        var result = converter.ConvertFrom(value.ToString());
        //                        fieldInfo.SetValue(ob, result);
        //                    }
        //                    else
        //                    {
        //                        if (Proptype == typeof(string))
        //                        {
        //                            fieldInfo.SetValue(ob, "");
        //                        }
        //                        else
        //                        {
        //                            fieldInfo.SetValue(ob, value);
        //                        }
        //                    }
        //                    break;
        //                }
        //            }
        //        }

        //        lst.Add(ob);
        //    }

        //    return lst;
        //}
        //public void BindDataGridCas_ComparedByQSID()
        //{
        //    ColumnCollection = new ObservableCollection<DataGridColumn>();
        //    foreach (DataColumn dtCol in defaultDataTableColumns)
        //    {
        //        System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(dtCol.ColumnName);

        //        if (dtCol.DataType.Name != "Boolean")
        //        {
        //            DataGridTextColumn colText = new DataGridTextColumn();
        //            colText.Header = dtCol.ColumnName;
        //            colText.Binding = bindings;
        //            Type typePro = dtCol.DataType;
        //            if (typePro == typeof(int) || typePro == typeof(decimal))
        //            {
        //                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, false);
        //            }
        //            if (typePro == typeof(string))
        //            {
        //                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, true);
        //                DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colText, false);
        //            }
        //            if (typePro == typeof(DateTime))
        //            {
        //                //DataGridFilterLibrary.DataGridColumnExtensions.SetIsBetweenFilterControl(colText, true);
        //            }

        //            var style = new Style(typeof(TextBlock));
        //            style.Setters.Add(new Setter(TextBlock.TextWrappingProperty, TextWrapping.WrapWithOverflow));
        //            style.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
        //            colText.ElementStyle = style;
        //            ColumnCollection.Add(colText);
        //        }
        //        else
        //        {
        //            DataGridCheckBoxColumn colText = new DataGridCheckBoxColumn();
        //            colText.Header = dtCol.ColumnName;
        //            colText.Binding = bindings;
        //            colText.IsReadOnly = false;
        //            colText.IsThreeState = false;
        //            var style = new Style(typeof(System.Windows.Controls.CheckBox));
        //            style.Setters.Add(new Setter(System.Windows.Controls.DataGridCell.IsEditingProperty, true));
        //            //style.Setters.Add(new Setter(System.Windows.Controls.DataGridCell.BackgroundProperty, Brushes.White));
        //            colText.ElementStyle = style;
        //            colText.Width = 80;
        //            ColumnCollection.Add(colText);
        //        }
        //    }

        //}


        public void BindDataGridCas_ComparedByQSID()
        {
            ColumnCollection = new ObservableCollection<DataGridColumn>();
            int i = 0;
            foreach (DataColumn dtCol in defaultDataTableColumns)
            {
                switch (dtCol.DataType.Name)
                {
                    case "Boolean":
                    case "ListCheckBoxType_ViewModel":
                        ColumnCollection.Add(CreateDataGridCheckboxColumnNew(dtCol.ColumnName, dtCol.ColumnName));
                        //ColumnCollection.Add(createDataGridCheckBoxColumn(dtCol.ColumnName, dtCol.ColumnName));
                        //ColumnCollection.Add(createDataGridComboBoxColumn(dtCol.ColumnName, dtCol.ColumnName, i));
                        break;
                    default:
                        ColumnCollection.Add(createDataGridTextColumn(dtCol.ColumnName, dtCol.ColumnName, dtCol.DataType));
                        break;
                }
                i++;
            }
            if (IsCreateSaveBtn)
            {
                ColumnCollection.Add(CreateButtonTemplateColumn());
            }
            if (IsCreateDeleteBtn)
            {
                ColumnCollection.Add(CreateButtonTemplateColumnDelete());
            }

        }
        public DataGridTemplateColumn CreateButtonTemplateColumn()
        {
            DataGridTemplateColumn templateCol = new DataGridTemplateColumn();
            templateCol.Header = "Action";
            DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(templateCol, true);
            FrameworkElementFactory elementBtn = new FrameworkElementFactory(typeof(System.Windows.Controls.Button));
            elementBtn.SetValue(System.Windows.Controls.Button.ContentProperty, "Save");
            elementBtn.SetValue(System.Windows.Controls.Button.CommandProperty, CommmandProductField);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementBtn;
            templateCol.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            return templateCol;
        }

        public DataGridTemplateColumn CreateButtonTemplateColumnDelete()
        {
            DataGridTemplateColumn templateCol = new DataGridTemplateColumn();
            templateCol.Header = "Action";
            DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(templateCol, true);
            FrameworkElementFactory elementBtn = new FrameworkElementFactory(typeof(System.Windows.Controls.Button));
            elementBtn.SetValue(System.Windows.Controls.Button.ContentProperty, "Delete");
            elementBtn.SetValue(System.Windows.Controls.Button.CommandProperty, CommmandDelete);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementBtn;
            templateCol.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            return templateCol;
        }
        public DataGridComboBoxColumn CreateDataGridCheckboxColumnNew(string colHeaderName, string colBindingName)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(colBindingName);
            bindings.Mode = System.Windows.Data.BindingMode.TwoWay;
            bindings.UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged;
            DataGridComboBoxColumn comboBoxCol = new DataGridComboBoxColumn();
            comboBoxCol.Header = colHeaderName;
            comboBoxCol.SelectedItemBinding = bindings;
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetUserCanEnterText(comboBoxCol, true);
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetIsTextFilter(comboBoxCol, false);
            comboBoxCol.DisplayMemberPath = "Value";
            comboBoxCol.SelectedValuePath = "ID";
            comboBoxCol.SortMemberPath = colBindingName;
            comboBoxCol.ClipboardContentBinding = bindings;
            comboBoxCol.ItemsSource = listCheckBox;
            if (colHeaderName == "I4C" || colHeaderName == "I4F")
            {
                comboBoxCol.IsReadOnly = true;
            }

            System.Windows.Style myStyle = (System.Windows.Style)System.Windows.Application.Current.Resources["ListCheckBoxCombo"] as System.Windows.Style;
            comboBoxCol.ElementStyle = myStyle;
            return comboBoxCol;

            //System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(colBindingName);
            //DataGridTemplateColumn colCheckBox = new DataGridTemplateColumn();
            //colCheckBox.Header = colHeaderName;
            //DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(colCheckBox, true);
            //FrameworkElementFactory elementImage = new FrameworkElementFactory(typeof(System.Windows.Controls.CheckBox));
            //bindings.Mode = System.Windows.Data.BindingMode.TwoWay;
            //bindings.UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged;
            //elementImage.SetValue(System.Windows.Controls.CheckBox.IsCheckedProperty, bindings);
            //DataTemplate cellTemplate = new DataTemplate();
            //cellTemplate.VisualTree = elementImage;
            //colCheckBox.CellTemplate = cellTemplate;
            //cellTemplate.Seal();
            //return colCheckBox;
        }
        public DataGridTextColumn createDataGridTextColumn(string colHeaderName, string colBindingName, System.Type modelProp)
        {

            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(colBindingName);
            DataGridTextColumn colText = new DataGridTextColumn();
            colText.Header = colHeaderName;
            colText.Binding = bindings;
            colText.SortMemberPath = colBindingName;
            Type typePro = modelProp; //modelProperties.Where(x => x.Name.ToLower() == colBindingName.ToLower()).Select(x => x.PropertyType).FirstOrDefault();
            if (typePro == typeof(int))
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, false);
            }
            else if (typePro == typeof(DateTime))
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetIsBetweenFilterControl(colText, true);
            }
            else
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, true);
                DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colText, false);
            }
            var style = new System.Windows.Style(typeof(TextBlock));
            style.Setters.Add(new Setter(TextBlock.TextWrappingProperty, TextWrapping.WrapWithOverflow));
            style.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
            colText.ElementStyle = style;
            return colText;
        }
        #region Singlecell Copyin Clipboard
        private DataGridCellInfo _currentCellData { get; set; }

        public DataGridCellInfo CurrentCellData
        {
            get { return _currentCellData; }
            set
            {
                if (value != _currentCellData)
                {
                    _currentCellData = value;
                    //if (_currentCellData.Column != null)
                    //{
                    //    if (_currentCellData.Column.Header.ToString() == "I4C")
                    //    {
                    //        var fieldId = Convert.ToInt32(((dynamic)value.Item).FieldNameID);
                    //        foreach (var ct in myListDataTable)
                    //        {
                    //            if (Convert.ToInt32(((dynamic)ct).FieldNameID) == fieldId)
                    //            {
                    //                ((dynamic)ct).I4C.Value = !Convert.ToBoolean(((dynamic)ct).I4C.Value);
                    //                NotifyPropertyChanged("myListDataTable");
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}

                }
            }
        }

        private void CommandCopy(object obj)
        {
            if (_currentCellData.Column != null)
            {

                //if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                //{
                string header = ((System.Windows.Data.Binding)((System.Windows.Controls.DataGridBoundColumn)_currentCellData.Column).Binding).Path.Path;
                if (_currentCellData.Item.GetType().GetProperty(header).GetValue(_currentCellData.Item) != null)
                {
                    System.Windows.Clipboard.SetDataObject(_currentCellData.Item.GetType().GetProperty(header).GetValue(_currentCellData.Item).ToString());
                }
                //}
            }
        }
        #endregion

        #region CustomQuery and Show in SerachCriteria
        public List<filterList> _listfilter { get; set; } = new List<filterList>();
        public QueryController _queryController { get; set; } = new QueryController();
        public QueryController CustomQueryController
        {
            get { return _queryController; }
            set
            {
                if (_queryController != value)
                {
                    _queryController = value;
                }
                NotifyPropertyChanged("CustomQueryController");
            }
        }
        private void CustomQueryController_FilteringStarted(object sender, EventArgs e)
        {
            if (CustomQueryController.ColumnFilterData.Operator != DataGridFilterLibrary.Support.FilterOperator.Undefined)
            {
                string colName = CustomQueryController.ColumnFilterData.ValuePropertyBindingPath;
                CustomQueryController.ColumnFilterData.QueryString = colName == "CAS" ? CustomQueryController.ColumnFilterData.QueryString.Replace("-", "") : CustomQueryController.ColumnFilterData.QueryString;
                // string valueofCol = colName == "CAS" ? CustomQueryController.ColumnFilterData.QueryString.Replace("-","") : CustomQueryController.ColumnFilterData.QueryString;
                string valueofCol = CustomQueryController.ColumnFilterData.QueryString;
                string valueofColTo = CustomQueryController.ColumnFilterData.QueryStringTo;
                Type valueType = CustomQueryController.ColumnFilterData.ValuePropertyType;
                string filterOpeartor = CustomQueryController.ColumnFilterData.Operator.ToString();
                if ((valueofCol != null && valueofCol != "") || (valueofColTo != null && valueofColTo != ""))
                {
                    if (!_listfilter.Any(x => x.ColName == colName && x.ColValue == valueofCol && x.ColOperator == filterOpeartor && x.ColValueTo == valueofColTo) || colName == "")
                    {
                        if (_listfilter.Any(x => x.ColName == colName))
                        {
                            var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                            objFilter.ColValue = valueofCol;
                            objFilter.ColOperator = filterOpeartor;
                            objFilter.ColValueTo = valueofColTo;
                        }
                        else
                        {
                            _listfilter.Add(new filterList { ColName = colName, ColValue = valueofCol, ColType = valueType, ColOperator = filterOpeartor, ColValueTo = valueofColTo });
                        }
                        SearchCriteria = string.Empty;
                        GetFilterQuery();
                    }
                }
                else
                {
                    if (IsNavigateProgress)
                    {
                        var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                        if (objFilter != null)
                        {
                            objFilter.ColValue = valueofCol;
                            objFilter.ColValueTo = valueofColTo;
                            SearchCriteria = string.Empty;
                            GetFilterQuery();
                        }
                    }
                    //if (commandFilterExecute == true)
                    //{
                    //    myList = myListTotal;
                    //    SearchCriteria = string.Empty;
                    //    Navigate(0);
                    //}
                }
            }
        }

        public void GetFilterQuery()
        {
            string queryString = string.Empty;
            string searchQueryString = string.Empty;
            List<string> arryOfColValue = new List<string>();
            _listfilter = _listfilter.Where(x => x.ColValue != "").ToList();
            int cnter = 0;
            for (var counter = 0; counter < _listfilter.Count; counter++)
            {
                if (_listfilter[counter].ColType == typeof(Int32) || _listfilter[counter].ColType == typeof(int) || _listfilter[counter].ColType == typeof(Int16))
                {
                    queryString += counter > 0 ? " &&" : "";
                    if (Int32.TryParse(_listfilter[counter].ColValue, out int res))
                    {
                        switch (_listfilter[counter].ColOperator)
                        {
                            case "Equals":
                                queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThan":
                                queryString += " " + _listfilter[counter].ColName + " < " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThan":
                                queryString += " " + _listfilter[counter].ColName + " > " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " <= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " >= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                        }
                        searchQueryString += " " + _listfilter[counter].ColName + " " + _listfilter[counter].ColOperator + " '" + _listfilter[counter].ColValue + "',";
                    }
                }
                else
                {
                    if (_listfilter[counter].ColType.FullName.Contains("DateTime"))
                    {
                        queryString += counter > 0 ? " &&" : "";
                        if ((_listfilter[counter].ColValue != null && _listfilter[counter].ColValue != string.Empty) && (_listfilter[counter].ColValueTo != null && _listfilter[counter].ColValueTo != string.Empty))
                        {
                            var stringDateTime = Convert.ToDateTime(_listfilter[counter].ColValue).Year + ", " + Convert.ToDateTime(_listfilter[counter].ColValue).Month + ", " + Convert.ToDateTime(_listfilter[counter].ColValue).Day;
                            var stringDateTimeTo = Convert.ToDateTime(_listfilter[counter].ColValueTo).Year + ", " + Convert.ToDateTime(_listfilter[counter].ColValueTo).Month + ", " + Convert.ToDateTime(_listfilter[counter].ColValueTo).Day; ;
                            queryString += " " + _listfilter[counter].ColName + " >= DateTime(" + stringDateTime + ") && " + _listfilter[counter].ColName + " <= DateTime(" + stringDateTimeTo + ")";
                            searchQueryString += " " + _listfilter[counter].ColName + " between " + _listfilter[counter].ColValue + " and " + _listfilter[counter].ColValueTo + "',";
                        }
                        //switch (_listfilter[counter].ColOperator)
                        //{
                        //    case "Equal":
                        //        queryString += " " + _listfilter[counter].ColName + " = DateTime(" + stringDateTime + ")";
                        //        break;
                        //    case "LessThan":
                        //        queryString += " " + _listfilter[counter].ColName + " < DateTime(" + stringDateTime + ")"; ;
                        //        break;
                        //    case "GreaterThan":
                        //        queryString += " " + _listfilter[counter].ColName + " > DateTime(" + stringDateTime + ")";
                        //        break;
                        //    case "LessThanOrEqual":
                        //        queryString += " " + _listfilter[counter].ColName + " <= DateTime(" + stringDateTime + ")";
                        //        break;
                        //    case "GreaterThanOrEqual":
                        //        queryString += " " + _listfilter[counter].ColName + " >= DateTime(" + stringDateTime + ")";
                        //        break;
                        //}

                        //queryString += " " + _listfilter[counter].ColName + " >= DateTime(" + stringDateTime + ")";
                        //queryString += " and " + _listfilter[counter].ColName + " <= DateTime(" + stringDateTimeTo + ")";
                        ////searchQueryString += " " + _listfilter[counter].ColName + " between '" + _listfilter[counter].ColValue + "',";
                        //searchQueryString += " " + _listfilter[counter].ColName + " " + _listfilter[counter].ColOperator + " '" + _listfilter[counter].ColValue + "',";
                        //arryOfColValue.Add(_listfilter[counter].ColValue.ToString());
                    }
                    else
                    //if (!_listfilter[counter].ColType.FullName.Contains("DateTime"))
                    {
                        if (_listfilter[counter].ColType == typeof(bool))
                        {
                            queryString += counter > 0 ? " && " : "";
                            queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToBoolean(_listfilter[counter].ColValue);
                            searchQueryString += " " + _listfilter[counter].ColName + " Equal '" + _listfilter[counter].ColValue + "',";
                            //arryOfColValue.Add(_listfilter[counter].ColValue.ToString());
                        }
                        else
                        {
                            queryString += counter > 0 ? " && " : "";
                            if (_listfilter[counter].ColName.Contains("CAS"))
                            {
                                queryString += " " + _listfilter[counter].ColName + ".Replace(\"-\",\"\").ToLower().Contains(@" + cnter + ")";
                                //queryString += " " + _listfilter[counter].ColName + ".ToLower().Contains(@" + counter + ")";
                                searchQueryString += " " + _listfilter[counter].ColName + " Like '" + _listfilter[counter].ColValue.Replace("-", "") + "',";
                                arryOfColValue.Add(_listfilter[counter].ColValue.ToString().ToLower().Replace("-", ""));
                                cnter += 1;
                            }
                            else
                            {
                                queryString += " " + _listfilter[counter].ColName + ".ToLower().Contains(@" + cnter + ")";
                                searchQueryString += " " + _listfilter[counter].ColName + " Like '" + _listfilter[counter].ColValue + "',";
                                arryOfColValue.Add(_listfilter[counter].ColValue.ToString().ToLower());
                                cnter += 1;
                            }

                        }
                    }
                }

            }
            if (queryString != "")
            {
                if (arryOfColValue.Count == 0)
                {
                    var lst = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(queryString.Trim()), listType, defaultDataTableColumns, listCheckBox);
                    myList = (IList)lst;
                }
                else
                {
                    var lst = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(queryString.Trim(), arryOfColValue.ToArray()), listType, defaultDataTableColumns, listCheckBox);
                    myList = (IList)lst;
                    //var splitQuery = queryString.Trim().Split('&').ToList();
                    //var totValue = myListTotal;
                    //for (int i = 0; i < arryOfColValue.Count();i++)
                    //{
                    //    var lst1 = _objLibraryFunction_Service.BindListFromIQuerable(totValue.AsQueryable().Where(splitQuery[i].Replace("&","").Trim(), arryOfColValue[i]), listType, defaultDataTableColumns);
                    //    totValue = (IList)lst1;

                    //}
                    //myList = (IList)totValue;
                }
            }
            else
            {
                myList = myListTotal;
            }
            if (string.IsNullOrEmpty(queryStringForNUll))
            {
                SearchCriteria = "";
                Navigate(0);
            }
            else
            {
                //SearchCriteria = queryStringForNUll;
                SearchCriteria = queryStringForNUll + ", " + searchQueryString.TrimEnd(',');
                Navigate(0);
            }
            //SearchCriteria = string.Empty;
            //SearchCriteria += searchQueryString.TrimEnd(',');

            commandFilterExecute = false;
        }

        #endregion

        #region ClearFilter Button Click
        public bool commandFilterExecute { get; set; }
        public DataGridFilterCommand _commandDataGridClearFilter { get; set; }
        public DataGridFilterCommand CommandDataGridClearFilter
        {
            get { return _commandDataGridClearFilter; }
            set
            {
                _commandDataGridClearFilter = new DataGridFilterCommand(new Action<object>(CommandDataGridClearFilterExecute));
                NotifyPropertyChanged("CommandDataGridClearFilter");
            }
        }
        public void CommandDataGridClearFilterExecute(object sender)
        {
            ExecuteClearFilter();
        }
        private void ExecuteClearFilter()
        {
            if (CustomQueryController != null && CustomQueryController.ColumnFilterData != null)
            {
                //if (CustomQueryController.ColumnFilterData.QueryString.Length > 0)
                //{
                CustomQueryController.ClearFilter();
                _listfilter.Clear();
                myList = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                if (string.IsNullOrEmpty(queryStringForNUll))
                {
                    SearchCriteria = "";
                }
                else
                {
                    SearchCriteria = queryStringForNUll;
                }
                //SearchCriteria = string.Empty;
                NotifyPropertyChanged("SearchCriteria");
                Navigate(0);
                //}
            }
        }
        #endregion

        #region Excel Export
        public ICommand CommGenerateExcelFile { get; set; }
        public ICommand ClearAllFilter { get; set; }

        public ICommand BtnCopy { get; set; }

        private void CommandClearAllFilter(object obj)
        {
            commandFilterExecute = true;
            commandFilterExecute = true;
            ListAllColumnDGNull.ToList().ForEach(x =>
            {
                x.IsNotNull = false;
                x.IsNull = false;
            });
            NotifyPropertyChanged("ListAllColumnDGNull");
            queryNull = new List<string>();
            CommandOnColSelectedChangeNullExecute(null);
            ExecuteClearFilter();
        }

        private void CommandExcelExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var filePath = folderDlg.SelectedPath + "\\Test.Xlsx";
                RowIdentifier.ExportExcelFile(dtDefault, filePath);
                Task.Run(() => { OpenFileInAccessAsync(filePath); });
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        public async Task OpenFileInAccessAsync(string resultCopyPath)
        {
            try
            {
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(resultCopyPath)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("System not open the file in access due to below error:" + Environment.NewLine + ex.Message, "Warning");

            }
        }
        #endregion

        private void releaseObject(object o) { try { while (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0) { } } catch { } finally { o = null; } }

        #region Access Export
        public ICommand CommGenerateAccessFile { get; set; }
        private void CommandAccessExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var filePath = folderDlg.SelectedPath + "\\Test.mdb";
                objCurrentView.ExportAccessFileLocal(filePath, dtDefault, "TEST");
                Task.Run(() => { OpenFileInAccessAsync(filePath); });
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        #endregion

        #region Paging
        int pageIndex = 1;
        private int numberOfRecPerPage = 25;
        private enum PagingMode { First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public string lblContent { get; set; }
        public string lblTotRecords { get; set; }
        public ObservableCollection<selectedListItemCombo> ListComboxItem { get; set; } = new ObservableCollection<selectedListItemCombo>();
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        private void CallbackExecute(object obj)
        {

        }
        private void CommmandProductFieldExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<object>>(new NotificationMessageAction<object>("Save Action", CallbackExecute), CallbackClassType);
        }
        private void CommmandDeleteExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<object>>(new NotificationMessageAction<object>("Delete Action", CallbackExecute), CallbackClassType);
        }
        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }
        private bool IsNavigateProgress { get; set; }
        private void Navigate(int mode)
        {
            IsNavigateProgress = true;
            lblContent = "";
            lblTotRecords = "";
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("lblContent");
            var result = _objLibraryFunction_Service.NavigateIList(mode, ref pageIndex, numberOfRecPerPage, myList, selectedItemCombo.text, lblContent, lblTotRecords, myListDataTable, listType, defaultDataTableColumns, listCheckBox);
            lblContent = result.Item1;
            lblTotRecords = result.Item2;
            myListDataTable = result.Item3;
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("myListDataTable");
            NotifyPropertyChanged("lblContent");
            IsNavigateProgress = false;

            //if (myList.Count > 0)
            //{
            //    int count;
            //    switch (mode)
            //    {
            //        case (int)PagingMode.Next:
            //            if (myList.Count >= (pageIndex * numberOfRecPerPage))
            //            {
            //                if (myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count() == 0)
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip((pageIndex * numberOfRecPerPage) - numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = (pageIndex * numberOfRecPerPage) + myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count();
            //                    NotifyPropertyChanged("myListDataTable");
            //                }
            //                else
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = (pageIndex * numberOfRecPerPage) + myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count();
            //                    pageIndex++;
            //                    NotifyPropertyChanged("myListDataTable");
            //                }


            //                var lastCount = (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage);
            //                lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (lastCount > myList.Count ? myList.Count : lastCount) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //        case (int)PagingMode.Previous:
            //            if (pageIndex > 1)
            //            {
            //                pageIndex -= 1;

            //                if (pageIndex == 1)
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = myList.AsQueryable().Take(numberOfRecPerPage).Count();
            //                    lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                    lblTotRecords = "Total Records : " + myList.Count;
            //                    NotifyPropertyChanged("lblTotRecords");
            //                    NotifyPropertyChanged("myListDataTable");
            //                    NotifyPropertyChanged("lblContent");
            //                }
            //                else
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = Math.Min(pageIndex * numberOfRecPerPage, myList.Count);
            //                    lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage) + " of " + myList.Count;
            //                    lblTotRecords = "Total Records : " + myList.Count;
            //                    NotifyPropertyChanged("lblTotRecords");
            //                    NotifyPropertyChanged("myListDataTable");
            //                    NotifyPropertyChanged("lblContent");
            //                }
            //            }
            //            break;

            //        case (int)PagingMode.First:
            //            pageIndex = 2;
            //            Navigate((int)PagingMode.Previous);
            //            break;
            //        case (int)PagingMode.Last:
            //            pageIndex = (myList.Count / numberOfRecPerPage);
            //            Navigate((int)PagingMode.Next);
            //            break;

            //        case (int)PagingMode.PageCountChange:

            //            pageIndex = 1;
            //            if (selectedItemCombo.text == "All")
            //            {
            //                numberOfRecPerPage = myList.Count;
            //                myListDataTable = myList.Count > 0 ? myList : null;
            //                count = myList.Count;
            //                lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("myListDataTable");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            else
            //            {
            //                numberOfRecPerPage = Convert.ToInt32(selectedItemCombo.text);
            //                //System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
            //                var lst = BindListFromIQuerable(myList.AsQueryable().Take(numberOfRecPerPage));
            //                myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                count = myList.AsQueryable().Take(numberOfRecPerPage).Count();
            //                lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("myListDataTable");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //        default:
            //            if (myList.Count >= 1)
            //            {
            //                var lst = BindListFromIQuerable(myList.AsQueryable().Skip(0 * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                count = 25;
            //                NotifyPropertyChanged("myListDataTable");
            //                lblContent = 1 + " of " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //    }
            //    // ExecuteClearFilter();
            //}
        }
        private List<selectedListItemCombo> AddList()
        {
            List<selectedListItemCombo> listSelectedCombo =

                new List<selectedListItemCombo>() {
                new selectedListItemCombo() { id = 25, text = "25" },
                new selectedListItemCombo() { id = 50, text = "50" },
                new selectedListItemCombo() { id = 100, text = "100" },
                new selectedListItemCombo() { id = 101, text = "All" },
            };
            return listSelectedCombo;

        }
        public selectedListItemCombo _selectedItemCombo { get; set; }
        public selectedListItemCombo selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }
        #endregion

        #region SearchCriteria
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (_searchCriteria != value)
                {
                    _searchCriteria = value;
                    NotifyPropertyChanged("SearchCriteria");
                    if (String.IsNullOrEmpty(_searchCriteria))
                    {
                        IsSearchCriteriaVisible = Visibility.Collapsed;
                    }
                    else
                    {
                        IsSearchCriteriaVisible = Visibility.Visible;
                    }
                    NotifyPropertyChanged("IsSearchCriteriaVisible");
                }
            }
        }
        public Visibility IsSearchCriteriaVisible { get; set; } = Visibility.Collapsed;
        public string _searchCriteria { get; set; }
        #endregion
    }

    #region OtherClasses
    public class selectedListItemCombo
    {
        public int? id { get; set; }
        public string text { get; set; }
    }
    public class filterList
    {
        public string ColName { get; set; }
        public Type ColType { get; set; }
        public string ColOperator { get; set; }
        public string ColValue { get; set; }
        public string ColValueTo { get; set; }
    }
    #endregion
}
