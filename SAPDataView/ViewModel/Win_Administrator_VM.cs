﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;

namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        private CommonFunctions objCommon;
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public static int Qsid_ID { get; set; }
        private static string _Qsid { get; set; }
        public static string Qsid
        {
            get { return _Qsid; }
            set
            {
                if (value != _Qsid)
                {
                    _Qsid = value;

                }
            }
        }
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }

        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {

                case "List Dictionary":
                    RunSAPData();
                    break;
                case "Data":
                    RunSAPDataMain();
                    break;
                case "Properties":
                    RunSAPProperties();
                    break;
                case "Characteristics":
                    RunSAPCharacteristics();
                    break;
                case "SAP-Lists":
                    RunListDic();
                    break;
                case "Prop-Characteristics":
                    BindPropChar();
                    break;
                //case "Unique Phrases":
                //    CallUniquePhrase();
                //    break;
            }
        }
        //private ObservableCollection<CurrentPh> _lstCurrentPhSAP { get; set; } = new ObservableCollection<CurrentPh>();
        //public ObservableCollection<CurrentPh> lstCurrentPh
        //{
        //    get { return _lstCurrentPhSAP; }
        //    set
        //    {
        //        if (_lstCurrentPhSAP != value)
        //        {
        //            _lstCurrentPhSAP = value;
        //            MessengerInstance.Send<PropertyChangedMessage<List<CurrentPh>>>(new PropertyChangedMessage<List<CurrentPh>>(null, _lstCurrentPhSAP.ToList(), "Default List"));
        //        }
        //    }
        //}
        //private List<CommonDataGridColumn> GetListGridColumnUniquePhrase()
        //{
        //    List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseID", ColBindingName = "PhraseID", ColType = "Textbox", ColumnWidth = "150" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth = "500" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Language", ColBindingName = "languageName", ColType = "Textbox", ColumnWidth = "150" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Frequency", ColBindingName = "PhraseFrequency", ColType = "Textbox", ColumnWidth = "150" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldNames", ColType = "Textbox", ColumnWidth = "250" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ARCode", ColBindingName = "ARCode", ColType = "Textbox", ColumnWidth = "150" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseKey", ColBindingName = "PhraseKey", ColType = "Textbox", ColumnWidth = "150" });
        //    listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Length", ColBindingName = "PhraseLength", ColType = "Textbox", ColumnWidth = "150" });
        //    //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapPropChar" });
        //    //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNamePropChar" });
        //    return listColumnQsidDetail;
        //}
        //public Visibility VisibleLodeUniquePhSAP { get; set; }
        //public CommonDataGrid_ViewModel<CurrentPh> CurrentUniquePhraseSAP_VM { get; set; }
        //public void CallUniquePhrase()
        //{
        //    Task.Run(() =>
        //    {
        //        VisibleLodeUniquePhSAP = Visibility.Visible;
        //        NotifyPropertyChanged("VisibleLodeUniquePhSAP");
        //        int saplistId = 0;
        //        var sapList = objCommon.DbaseQueryReturnStringSQL("select top 1 SAP_ListID from SAP.Library_SAP_lists where SAP_listName ='" + Qsid + "'", Win_Administrator_VM.CommonListConn);
        //        if (sapList != "")
        //        {
        //            saplistId = Convert.ToInt32(sapList);
        //        }
        //        var dt = objCommon.DbaseQueryReturnSqlList<CurrentPh>("select y.phraseid,  y.PhraseFrequency, y.FieldNames,c.languageName, " +  
        //                                                            " b.Phrase, b.ARCode, b.PhraseKey, " + 
        //                                                            " len(b.Phrase) as PhraseLength from " +
        //                                                            " (select x.PhraseID, x.PhraseFrequency, stuff(( " +
        //                                                            " select distinct ',' + b.SAP_Characteristic_Code " +
        //                                                            " from SAP.QSxxx_Phrases_SAP u, SAP.Library_SAP_Characteristics b " +
        //                                                            " where u.CharacteristicID = b.CharacteristicID and u.SAP_ListID = x.SAP_ListID and u.PhraseID = x.PhraseID " +
        //                                                            " for xml path('')  " +
        //                                                            " ),1,1,'') as FieldNames from " +
        //                                                            " (select a.SAP_ListID, a.PhraseID, count(*) as PhraseFrequency  from SAP.QSxxx_Phrases_SAP a " +
        //                                                            " where a.SAP_ListID =  " + saplistId +
        //                                                            " group by a.SAP_ListID, a.PhraseID) as x) as y, Library_Phrases b, " +
        //                                                            " Library_Languages c where y.PhraseID = b.PhraseID and b.LanguageID = c.LanguageID order by y.PhraseID", Win_Administrator_VM.CommonListConn);
        //        //lstCurrentPh = new ObservableCollection<CurrentPh>(dt);
        //        VisibleLodeUniquePhSAP = Visibility.Collapsed;
        //        NotifyPropertyChanged("VisibleLodeUniquePhSAP");
        //    });

        //}
        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommandTabChanged { get; set; }
       
        public static string CommonListConn;
        public ICommand SearchCommand { get; set; }
        private void SearchCommandExecute(object obj)
        {
            SelectedTabItemChanged();
        }
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        public bool IsSelectedTabData { get; set; }
       
        public Win_Administrator_VM()
        {
            objCommon = new CommonFunctions();
            comonDG_VM = new CommonDataGrid_ViewModel<Library_SAP_Properties>(GetListGridColumnSAPProperty(), "Properties", "");
            comonCharacteristics_VM = new CommonDataGrid_ViewModel<Library_SAP_Characteristics>(GetListGridColumnSAPCharacteristics(), "Characteristics", "");
            comonPropChar_VM = new CommonDataGrid_ViewModel<MapPropChar>(GetListGridColumnPropChar(), "Property-Characteristics", "");
            comonListDic_VM = new CommonDataGrid_ViewModel<MapListDic>(GetListGridColumnListDic(), "List-Dictionary", "");

            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            listTypeHeader = objCommon.DbaseQueryReturnSqlList<LibProperty>("select PropertyID, SAP_Property_Code from SAP.Library_SAP_Properties b order by SAP_Property_Code", Win_Administrator_VM.CommonListConn);
            listTypeHeaderChar = objCommon.DbaseQueryReturnSqlList<LibCharacteristic>("select CharacteristicID, SAP_Characteristic_Code from SAP.Library_SAP_Characteristics b order by SAP_Characteristic_Code", Win_Administrator_VM.CommonListConn);

           
            listTypeHeaderCat = objCommon.DbaseQueryReturnSqlList<LibCat>("select PhraseCategoryID, PhraseCategory from Library_PhraseCategories b order by PhraseCategory", Win_Administrator_VM.CommonListConn);
           

            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);

            IsSelectedTabData = true;
            CommandClosePopUp = new RelayCommand(CommandClosePopUpExecute);
            CommandShowPopUpAddHydrate = new RelayCommand(CommandShowPopUpAddHydrateExecute);
            CommandAddHydrate = new RelayCommand(CommandAddHydrateExecute, CommandAddHydrateCanExecute);
            CommandAddMoreHydrate = new RelayCommand(CommandAddMoreHydrateExecute, CommandAddHydrateCanExecute);

            CommandShowPopUpAddCharacteristics = new RelayCommand(CommandShowPopUpAddCharacteristicsExecute);
            CommandClosePopUpCharacteristics = new RelayCommand(CommandClosePopUpExecuteCharacteristics);
            CommandAddCharacteristics = new RelayCommand(CommandAddCharacteristicsExecute, CommandAddCharacteristicsCanExecute);
            CommandAddMoreCharacteristics = new RelayCommand(CommandAddMoreCharacteristicsExecute, CommandAddCharacteristicsCanExecute);

            CommandShowPopUpAddPropChar = new RelayCommand(CommandShowPopUpAddPropCharExecute);
            CommandClosePopUpPropChar = new RelayCommand(CommandClosePopUpExecutePropChar);
            CommandAddPropChar = new RelayCommand(CommandAddPropCharExecute, CommandAddPropCharCanExecute);
            CommandAddMorePropChar = new RelayCommand(CommandAddMorePropCharExecute, CommandAddPropCharCanExecute);

            CommandShowPopUpAddListDic = new RelayCommand(CommandShowPopUpAddListDicExecute);
            CommandClosePopUpListDic = new RelayCommand(CommandClosePopUpExecuteListDic);
            CommandAddListDic = new RelayCommand(CommandAddListDicExecute, CommandAddListDicCanExecute);
            CommandAddMoreListDic = new RelayCommand(CommandAddMoreListDicExecute, CommandAddListDicCanExecute);
            //CurrentUniquePhraseSAP_VM = new CommonDataGrid_ViewModel<CurrentPh>(GetListGridColumnUniquePhrase(), "Unique-Phrase", "");

            MessengerInstance.Register<PropertyChangedMessage<Library_SAP_Properties>>(this, NotifyMe);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Library_SAP_Properties), CommandButtonExecuted);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGrid);

            MessengerInstance.Register<PropertyChangedMessage<Library_SAP_Characteristics>>(this, NotifyMeChar);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(Library_SAP_Characteristics), CommandButtonExecutedChar);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGridChar);

            MessengerInstance.Register<PropertyChangedMessage<MapPropChar>>(this, NotifyMePropChar);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapPropChar), CommandButtonExecutedPropChar);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGridPropChar);

            MessengerInstance.Register<PropertyChangedMessage<MapListDic>>(this, NotifyMeListDic);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapListDic), CommandButtonExecutedListDic);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGridListDic);

            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);
        }
        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }

    }
    //public class CurrentPh
    //{
    //    public int PhraseID { get; set; }
    //    public string Phrase { get; set; }
    //    public string languageName { get; set; }
    //    public int PhraseFrequency { get; set; }
    //    public string FieldNames { get; set; }
    //    public string ARCode { get; set; }
    //    public string PhraseKey { get; set; }
    //    public int PhraseLength { get; set; }
    //}
}
