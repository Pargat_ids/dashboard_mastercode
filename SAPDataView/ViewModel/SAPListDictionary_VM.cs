﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using System.Text.RegularExpressions;
using CRA_DataAccess.ViewModel;

namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string PhraseListDic { get; set; }
        public string SAPListDicName { get; set; }

        public List<LibCat> listTypeHeaderCat { get; set; }

        private LibCat _SelectedTypeHeaderCat { get; set; }
        public LibCat SelectedTypeHeaderCat
        {
            get => _SelectedTypeHeaderCat;
            set
            {
                if (Equals(_SelectedTypeHeaderCat, value))
                {
                    return;
                }

                _SelectedTypeHeaderCat = value;
                NotifyPropertyChanged("SelectedTypeHeaderCat");
            }
        }


        public Visibility VisibilityListDicDashboard_Loader { get; set; }
        public CommonDataGrid_ViewModel<MapListDic> comonListDic_VM { get; set; }
        public CommonDataGrid_ViewModel<MapListDic> comonListDic1_VM { get; set; }
        public ICommand CommandClosePopUpListDic { get; private set; }
        public ICommand CommandShowPopUpAddListDic { get; private set; }
        public MapListDic SelectedListDic { get; set; }

        public bool IsEditListDic { get; set; } = true;
        public Visibility PopUpContentAddListDicVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalListDicVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalAnListDicVisibility { get; set; } = Visibility.Collapsed;

        private ObservableCollection<MapListDic> _lstListDic { get; set; } = new ObservableCollection<MapListDic>();
        public ObservableCollection<MapListDic> lstListDic
        {
            get { return _lstListDic; }
            set
            {
                if (_lstListDic != value)
                {
                    _lstListDic = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapListDic>>>(new PropertyChangedMessage<List<MapListDic>>(null, _lstListDic.ToList(), "Default List"));
                }
            }
        }
        private void RunListDic()
        {
            BindListDic();

        }

        private List<CommonDataGridColumn> GetListGridColumnListDic()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAP ListName", ColBindingName = "SAP_ListName", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Category", ColBindingName = "PhraseCategory", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapListDic" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSxxx_ListDictionary_ID_SAP", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNameListDic" });

            return listColumnQsidDetail;
        }


        private void BindListDic()
        {
            VisibilityListDicDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDicDashboard_Loader");
            Task.Run(() =>
            {
                var SAPListDicTable = objCommon.DbaseQueryReturnSqlList<MapListDic>("select a.QSxxx_ListDictionary_ID_SAP, a.SAP_ListID, a.PhraseID, a.PhraseCategoryID,b.SAP_ListName, c.PhraseCategory, d.Phrase from SAP.QSxxx_ListDictionary_SAP a, Library_PhraseCategories c, Library_Phrases d, SAP.Library_SAP_lists b where a.sap_listid = b.sap_listid and a.PhraseCategoryid = c.PhraseCategoryid and a.phraseid = d.phraseid", Win_Administrator_VM.CommonListConn);
                lstListDic = new ObservableCollection<MapListDic>(SAPListDicTable);
            });
            NotifyPropertyChanged("comonListDic_VM");
            VisibilityListDicDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityListDicDashboard_Loader");
        }

        private void CommandClosePopUpExecuteListDic(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpListDic = false;

        }
        private void CommandShowPopUpAddListDicExecute(object obj)
        {
            ClearPopUpVariableListDic();
            IsEditListDic = true;
            NotifyPropertyChanged("IsEditListDic");
            IsShowPopUpListDic = true;
            PopUpContentAddListDicVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddListDicVisibility");
        }
        private void NotifyMeListDic(PropertyChangedMessage<MapListDic> obj)
        {
            SelectedListDic = obj.NewValue;
        }

        public void ClearPopUpVariableListDic()
        {
            SelectedTypeHeaderCat = null;
            NotifyPropertyChanged("SelectedTypeHeaderCat");
            SAPListDicName = null;
            NotifyPropertyChanged("SAPListDicName");
            PhraseListDic = null;
            NotifyPropertyChanged("PhraseListDic");
        }
        private void CommandButtonExecutedListDic(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapListDic")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this List Dictionary?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    objCommon.DbaseQueryReturnStringSQL("Delete from SAP.QSxxx_ListDictionary_SAP where QSxxx_ListDictionary_ID_SAP =" + SelectedListDic.QSxxx_ListDictionary_ID_SAP, Win_Administrator_VM.CommonListConn);
                    BindListDic();
                    _notifier.ShowSuccess("Selected  List Dictionary deleted successfully");

                }
            }
            else if (obj.Notification == "AddEditNameListDic")
            {
                IsEditListDic = false;
                NotifyPropertyChanged("IsEditListDic");
                IsShowPopUpListDic = true;
                PopUpContentAddListDicVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddListDicVisibility");
                ListChemicalAnListDicVisibility = Visibility.Collapsed;
                ListChemicalListDicVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemicalAnListDicVisibility");
                NotifyPropertyChanged("ListChemicalListDicVisibility");
                SelectedTypeHeaderCat = listTypeHeaderCat.Where(x => x.PhraseCategoryID == SelectedListDic.PhraseCategoryID).FirstOrDefault();
                PhraseListDic = SelectedListDic.Phrase;
                NotifyPropertyChanged("PhraseListDic");
                SAPListDicName = SelectedListDic.SAP_ListName;
                NotifyPropertyChanged("SAPListDicName");

            }
        }
        private bool _IsShowPopUpListDic { get; set; }
        public bool IsShowPopUpListDic
        {
            get { return _IsShowPopUpListDic; }
            set
            {
                if (_IsShowPopUpListDic != value)
                {
                    _IsShowPopUpListDic = value;
                    NotifyPropertyChanged("IsShowPopUpListDic");
                }
            }
        }
        private void RefreshGridListDic(PropertyChangedMessage<string> flag)
        {
            BindListDic();
            if (IsShowPopUpListDic)
            {
                NotifyPropertyChanged("IsShowPopUpListDic");
            }
        }
        public ICommand CommandAddListDic { get; private set; }
        private void CommandAddListDicExecute(object obj)
        {
            SaveListDic();
            IsShowPopUpListDic = false;
            IsEditListDic = true;
            NotifyPropertyChanged("IsEditListDic");
            ClearPopUpVariableListDic();
            PopUpContentAddListDicVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddListDicVisibility");
        }
        private bool CommandAddListDicCanExecute(object obj)
        {
            //if (string.IsNullOrEmpty(SAP_ListID.ToString()) || string.IsNullOrEmpty(PhraseCategoryID.ToString()) || string.IsNullOrEmpty(PhraseID.ToString()))
            //{
            //    return false;
           // }
            //else
            //{
                return true;
            //}
        }
        public void SaveListDic()
        {
            QSxxx_ListDictionary_SAP objListDic_VM = new QSxxx_ListDictionary_SAP();
            using (var context = new CRAModel())
            {
                if (!IsEditListDic)
                {
                    objListDic_VM = context.QSxxx_ListDictionary_SAP.Where(x => x.QSxxx_ListDictionary_ID_SAP == SelectedListDic.QSxxx_ListDictionary_ID_SAP).FirstOrDefault();
                }
                else
                {
                    var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('SAP.QSxxx_ListDictionary_SAP')", Win_Administrator_VM.CommonListConn));
                    objListDic_VM.QSxxx_ListDictionary_ID_SAP = mx + 1;
                }
                var phrIds = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == PhraseListDic).Select(y => new { y.Phrase, y.PhraseID }).ToList();
                var saplst = context.Library_SAP_lists.AsNoTracking().Where(x => x.SAP_ListName == SAPListDicName).Select(y => new { y.SAP_ListID, y.SAP_ListName }).ToList();
                if(phrIds.Count ==0)
                {
                    // add in lib_phrase
                    var lstStripChar = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();

                    PhraseListDic = this.objCommon.RemoveWhitespaceCharacter(PhraseListDic, lstStripChar);
                    PhraseListDic = Regex.Replace(PhraseListDic, @"\s+", " ").Trim();
                    _objLibraryFunction_Service.AddDataToPhareLibrary(PhraseListDic, 1, objCommon.ESTTime());
                    phrIds = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == PhraseListDic).Select(y => new { y.Phrase, y.PhraseID }).ToList();
                }
                if(saplst.Count == 0)
                {
                    // add in lib sap list
                    var objSAP = new Library_SAP_lists();
                    objSAP.SAP_ListName = SAPListDicName;
                    objSAP.IsActive = true;
                    objSAP.TimeStamp = objCommon.ESTTime();
                    context.Library_SAP_lists.Add(objSAP);
                    context.SaveChanges();
                    saplst = context.Library_SAP_lists.AsNoTracking().Where(x => x.SAP_ListName == SAPListDicName).Select(y => new { y.SAP_ListID, y.SAP_ListName }).ToList();
                }
                objListDic_VM.SAP_ListID = saplst.Where(x => x.SAP_ListName == SAPListDicName).Select(y => y.SAP_ListID).FirstOrDefault();
                objListDic_VM.PhraseID = phrIds.Where(x => x.Phrase == PhraseListDic).Select(y => y.PhraseID).FirstOrDefault();
                objListDic_VM.PhraseCategoryID = SelectedTypeHeaderCat.PhraseCategoryID;
                if (!IsEditListDic)
                {
                    var chkAlreadyExist = context.QSxxx_ListDictionary_SAP.Count(x => x.QSxxx_ListDictionary_ID_SAP != SelectedListDic.QSxxx_ListDictionary_ID_SAP && x.SAP_ListID == objListDic_VM.SAP_ListID && x.PhraseID == objListDic_VM.PhraseID && x.PhraseCategoryID == SelectedTypeHeaderCat.PhraseCategoryID);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("List Dictionary Name already exists");
                    }
                    else
                    {
                        context.SaveChanges();
                        _notifier.ShowSuccess("Existing List Dictionary Saved Successfully.");
                    }
                }
                else
                {
                    var chkAlreadyExist = context.QSxxx_ListDictionary_SAP.Count(x => x.SAP_ListID == objListDic_VM.SAP_ListID && x.PhraseID == objListDic_VM.PhraseID && x.PhraseCategoryID == SelectedTypeHeaderCat.PhraseCategoryID);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("List Dictionary already exists");
                    }
                    else
                    {
                        context.QSxxx_ListDictionary_SAP.Add(objListDic_VM);
                        context.SaveChanges();
                        _notifier.ShowSuccess("New List Dictionary Saved Successfully.");
                    }
                }
            }
            ClearPopUpVariableListDic();
            BindListDic();

        }
        public ICommand CommandAddMoreListDic { get; private set; }
        private void CommandAddMoreListDicExecute(object obj)
        {
            SaveListDic();
        }

    }
    public class MapListDic
    {
        public int QSxxx_ListDictionary_ID_SAP { get; set; }
        public string SAP_ListName { get; set; }
        public string PhraseCategory { get; set; }
        public string Phrase { get; set; }
        public int SAP_ListID { get; set; }
        public int PhraseID { get; set; }
        public int PhraseCategoryID { get; set; }
    }

    public class LibPhrase
    {
        public int PhraseID { get; set; }
        public string Phrase { get; set; }
    }

    public class LibCat
    {
        public int PhraseCategoryID { get; set; }
        public string PhraseCategory { get; set; }
    }
    public class LibList
    {
        public int SAP_ListID { get; set; }
        public string SAP_ListName { get; set; }
    }
   
}

