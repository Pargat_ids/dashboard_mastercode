﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string CharacteristicsDescription { get; set; }
        public string SAP_CharacteristicsCode { get; set; }
        public string Verisk_CharacteristicsCode { get; set; }
        public string CommentCharacteristics { get; set; }
        public bool? IsActCharacteristics { get; set; }
        public bool? IsStandardCharacteristicsC { get; set; }
        public bool? IsPhraseTypeC { get; set; }

        public bool? MultiplePhrasesAllowedC { get; set; }
        public string MultiPhraseDelimiterC { get; set; }
        public bool? IsFreeTextTypeC { get; set; }

        public bool? IsValueTypeC { get; set; }
        public int? TotalValueLengthAllowedC { get; set; }

        public int? DecimalValueLengthAllowedC { get; set; }
        public bool IsNegativeValueAllowedC { get; set; }
        public bool? IsValue0AllowedC { get; set; }
        public bool? IsValue100AllowedC { get; set; }
        public bool? IsValueOver100AllowedC { get; set; }
        public bool? IsExponentialAllowedC { get; set; }
        public string UnitsAllowedC { get; set; }


        public Visibility VisibilityCharacteristicsDashboard_Loader { get; set; }
        public CommonDataGrid_ViewModel<Library_SAP_Characteristics> comonCharacteristics_VM { get; set; }
        public ICommand CommandClosePopUpCharacteristics { get; private set; }
        public ICommand CommandShowPopUpAddCharacteristics { get; private set; }
        public Library_SAP_Characteristics SelectedCharacteristics { get; set; }
        
        public bool IsEditCharacteristics { get; set; } = true;
        public Visibility PopUpContentAddCharacteristicsVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalCharacteristicsVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalAnCharacteristicsVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<Library_SAP_Characteristics> _lstSAPCharacteristics { get; set; } = new ObservableCollection<Library_SAP_Characteristics>();
        public ObservableCollection<Library_SAP_Characteristics> lstSAPCharacteristics
        {
            get { return _lstSAPCharacteristics; }
            set
            {
                if (_lstSAPCharacteristics != value)
                {
                    _lstSAPCharacteristics = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Library_SAP_Characteristics>>>(new PropertyChangedMessage<List<Library_SAP_Characteristics>>(null, _lstSAPCharacteristics.ToList(), "Default List"));
                }
            }
        }
        private void RunSAPCharacteristics()
        {
            BindCharacteristics();

        }

        private List<CommonDataGridColumn> GetListGridColumnSAPCharacteristics()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAP Characteristics Code", ColBindingName = "SAP_Characteristic_Code", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Characteristics Description", ColBindingName = "Characteristic_Description", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Verisk Characteristics Code", ColBindingName = "Verisk_Characteristic_Code", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "Comments", ColType = "Textbox", ColumnWidth = "350" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsActive", ColBindingName = "IsActive", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsPhraseType", ColBindingName = "IsPhraseType", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Multiple PhrasesAllowed", ColBindingName = "MultiplePhrasesAllowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Multi PhraseDelimiter", ColBindingName = "MultiPhraseDelimiter", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is FreeTextType", ColBindingName = "IsFreeTextType", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is ValueType", ColBindingName = "IsValueType", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Total ValueLengthAllowed", ColBindingName = "TotalValueLengthAllowed", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Decimal ValueLengthAllowed", ColBindingName = "DecimalValueLengthAllowed", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Units Allowed", ColBindingName = "UnitsAllowed", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is NegativeValueAllowed", ColBindingName = "IsNegativeValueAllowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Value0Allowed", ColBindingName = "IsValue0Allowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Value100Allowed", ColBindingName = "IsValue100Allowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is ValueOver100Allowed", ColBindingName = "IsValueOver100Allowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is ExponentialAllowed", ColBindingName = "IsExponentialAllowed", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Is Standard Characteristics", ColBindingName = "IsStandardCharacteristic", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TimeStamp", ColBindingName = "TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "CharacteristicsID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNameChar" });

            return listColumnQsidDetail;
        }

        private void BindCharacteristics()
        {
            VisibilityCharacteristicsDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityCharacteristicsDashboard_Loader");
            Task.Run(() =>
            {
                var SAPCharacteristicsTable = objCommon.DbaseQueryReturnSqlList<Library_SAP_Characteristics>("select * from SAP.Library_SAP_Characteristics", Win_Administrator_VM.CommonListConn);
                lstSAPCharacteristics = new ObservableCollection<Library_SAP_Characteristics>(SAPCharacteristicsTable);
            });
            // NotifyPropertyChanged("comonCharacteristics_VM");
            VisibilityCharacteristicsDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityCharacteristicsDashboard_Loader");
        }

        private void CommandClosePopUpExecuteCharacteristics(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpCharacteristics = false;
        }
        private void CommandShowPopUpAddCharacteristicsExecute(object obj)
        {
            ClearPopUpVariableCharacteristics();
            IsEditCharacteristics = true;
            NotifyPropertyChanged("IsEditCharacteristics");
            IsShowPopUpCharacteristics = true;
            PopUpContentAddCharacteristicsVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddCharacteristicsVisibility");
        }

        private void NotifyMeChar(PropertyChangedMessage<Library_SAP_Characteristics> obj)
        {
            SelectedCharacteristics = obj.NewValue;
        }

        public void ClearPopUpVariableCharacteristics()
        {
            CharacteristicsDescription = string.Empty;
            SAP_CharacteristicsCode = string.Empty;
            Verisk_CharacteristicsCode = string.Empty;
            CommentCharacteristics = string.Empty;
            IsActCharacteristics = false;
            IsStandardCharacteristicsC = false;
            NotifyPropertyChanged("CharacteristicsDescription");
            NotifyPropertyChanged("SAP_CharacteristicsCode");
            NotifyPropertyChanged("Verisk_CharacteristicsCode");
            NotifyPropertyChanged("CommentCharacteristics");
            NotifyPropertyChanged("IsActCharacteristics");
            NotifyPropertyChanged("IsStandardCharacteristicsC");
            IsPhraseTypeC = false;
            MultiplePhrasesAllowedC = false;
            MultiPhraseDelimiterC = string.Empty;
            IsFreeTextTypeC = false;
            IsValueTypeC = false;
            TotalValueLengthAllowedC = 0;
            DecimalValueLengthAllowedC = 0;
            IsNegativeValueAllowedC = false;
            IsValue0AllowedC = false;
            IsValue100AllowedC = false;
            IsValueOver100AllowedC = false;
            IsExponentialAllowedC = false;
            UnitsAllowedC = string.Empty;
            NotifyPropertyChanged("IsPhraseTypeC");
            NotifyPropertyChanged("MultiplePhrasesAllowedC");
            NotifyPropertyChanged("MultiPhraseDelimiterC");
            NotifyPropertyChanged("IsFreeTextTypeC");
            NotifyPropertyChanged("IsValueTypeC");
            NotifyPropertyChanged("TotalValueLengthAllowedC");
            NotifyPropertyChanged("DecimalValueLengthAllowedC");
            NotifyPropertyChanged("IsNegativeValueAllowedC");
            NotifyPropertyChanged("IsValue0AllowedC");
            NotifyPropertyChanged("IsValue100AllowedC");
            NotifyPropertyChanged("IsValueOver100AllowedC");
            NotifyPropertyChanged("IsExponentialAllowedC");
            NotifyPropertyChanged("UnitsAllowedC");
        }

        private void CommandButtonExecutedChar(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapHydrate")
            {
            }
            else if (obj.Notification == "AddEditNameChar")
            {
                IsEditCharacteristics = false;
                NotifyPropertyChanged("IsEditCharacteristics");
                SAP_CharacteristicsCode = SelectedCharacteristics.SAP_Characteristic_Code;
                NotifyPropertyChanged("SAP_CharacteristicsCode");
                CharacteristicsDescription = SelectedCharacteristics.Characteristic_Description;
                NotifyPropertyChanged("CharacteristicsDescription");
                Verisk_CharacteristicsCode = SelectedCharacteristics.Verisk_Characteristic_Code;
                NotifyPropertyChanged("Verisk_CharacteristicsCode");
                CommentCharacteristics = SelectedCharacteristics.Comments;
                NotifyPropertyChanged("CommentCharacteristics");
                IsActCharacteristics = SelectedCharacteristics.IsActive;
                NotifyPropertyChanged("IsActCharacteristics");
                IsStandardCharacteristicsC = SelectedCharacteristics.IsStandardCharacteristic;
                NotifyPropertyChanged("IsStandardCharacteristicsC");
                IsPhraseTypeC = SelectedCharacteristics.IsPhraseType;
                MultiplePhrasesAllowedC = SelectedCharacteristics.MultiplePhrasesAllowed;
                MultiPhraseDelimiterC = SelectedCharacteristics.MultiPhraseDelimiter;
                IsFreeTextTypeC = SelectedCharacteristics.IsFreeTextType;
                IsValueTypeC = SelectedCharacteristics.IsValueType;
                TotalValueLengthAllowedC = SelectedCharacteristics.TotalValueLengthAllowed;
                DecimalValueLengthAllowedC = SelectedCharacteristics.DecimalValueLengthAllowed;
                IsNegativeValueAllowedC = SelectedCharacteristics.IsNegativeValueAllowed;
                IsValue0AllowedC = SelectedCharacteristics.IsValue0Allowed;
                IsValue100AllowedC = SelectedCharacteristics.IsValue100Allowed;
                IsValueOver100AllowedC = SelectedCharacteristics.IsValueOver100Allowed;
                IsExponentialAllowedC = SelectedCharacteristics.IsExponentialAllowed;
                UnitsAllowedC = SelectedCharacteristics.UnitsAllowed;
                NotifyPropertyChanged("IsPhraseTypeC");
                NotifyPropertyChanged("MultiplePhrasesAllowedC");
                NotifyPropertyChanged("MultiPhraseDelimiterC");
                NotifyPropertyChanged("IsFreeTextTypeC");
                NotifyPropertyChanged("IsValueTypeC");
                NotifyPropertyChanged("TotalValueLengthAllowedC");
                NotifyPropertyChanged("DecimalValueLengthAllowedC");
                NotifyPropertyChanged("IsNegativeValueAllowedC");
                NotifyPropertyChanged("IsValue0AllowedC");
                NotifyPropertyChanged("IsValue100AllowedC");
                NotifyPropertyChanged("IsValueOver100AllowedC");
                NotifyPropertyChanged("IsExponentialAllowedC");
                NotifyPropertyChanged("UnitsAllowedC");
                IsShowPopUpCharacteristics = true;
                PopUpContentAddCharacteristicsVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddCharacteristicsVisibility");
                ListChemicalAnCharacteristicsVisibility = Visibility.Collapsed;
                ListChemicalCharacteristicsVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemicalAnCharacteristicsVisibility");
                NotifyPropertyChanged("ListChemicalCharacteristicsVisibility");
            }
        }
        private bool _IsShowPopUpCharacteristics { get; set; }
        public bool IsShowPopUpCharacteristics
        {
            get { return _IsShowPopUpCharacteristics; }
            set
            {
                if (_IsShowPopUpCharacteristics != value)
                {
                    _IsShowPopUpCharacteristics = value;
                    NotifyPropertyChanged("IsShowPopUpCharacteristics");
                }
            }
        }
        private void RefreshGridChar(PropertyChangedMessage<string> flag)
        {
            BindCharacteristics();
            if (IsShowPopUpCharacteristics)
            {
                NotifyPropertyChanged("IsShowPopUpCharacteristics");
            }
        }
        public ICommand CommandAddCharacteristics { get; private set; }
        private void CommandAddCharacteristicsExecute(object obj)
        {
            SaveCharacteristics();
            IsShowPopUpCharacteristics = false;
            IsEditCharacteristics = true;
            NotifyPropertyChanged("IsEditCharacteristics");
            ClearPopUpVariableCharacteristics();
            PopUpContentAddCharacteristicsVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddCharacteristicsVisibility");
        }
        private bool CommandAddCharacteristicsCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(SAP_CharacteristicsCode) || string.IsNullOrEmpty(Verisk_CharacteristicsCode))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveCharacteristics()
        {
            Library_SAP_Characteristics objCharacteristics_VM = new Library_SAP_Characteristics();
            using (var context = new CRAModel())
            {
                if (!IsEditCharacteristics)
                {
                    objCharacteristics_VM = context.Library_SAP_Characteristics.Where(x => x.CharacteristicID == SelectedCharacteristics.CharacteristicID).FirstOrDefault();
                }
                else
                {
                    var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('SAP.Library_SAP_Characteristics')", Win_Administrator_VM.CommonListConn));
                    objCharacteristics_VM.CharacteristicID = mx + 1;
                }
                objCharacteristics_VM.SAP_Characteristic_Code = SAP_CharacteristicsCode;
                objCharacteristics_VM.Characteristic_Description = CharacteristicsDescription;
                objCharacteristics_VM.IsActive = IsActCharacteristics;
                objCharacteristics_VM.IsStandardCharacteristic = IsStandardCharacteristicsC;
                objCharacteristics_VM.Verisk_Characteristic_Code = Verisk_CharacteristicsCode;
                objCharacteristics_VM.Comments = CommentCharacteristics;
                objCharacteristics_VM.IsPhraseType = IsPhraseTypeC;
                objCharacteristics_VM.MultiplePhrasesAllowed = MultiplePhrasesAllowedC;
                objCharacteristics_VM.MultiPhraseDelimiter = MultiPhraseDelimiterC;
                objCharacteristics_VM.IsFreeTextType = IsFreeTextTypeC;
                objCharacteristics_VM.IsValueType = IsValueTypeC;
                objCharacteristics_VM.TotalValueLengthAllowed = TotalValueLengthAllowedC;
                objCharacteristics_VM.DecimalValueLengthAllowed = DecimalValueLengthAllowedC;
                objCharacteristics_VM.IsNegativeValueAllowed = IsNegativeValueAllowedC;
                objCharacteristics_VM.IsValue0Allowed = IsValue0AllowedC;
                objCharacteristics_VM.IsValue100Allowed = IsValue100AllowedC;
                objCharacteristics_VM.IsValueOver100Allowed = IsValueOver100AllowedC;
                objCharacteristics_VM.IsExponentialAllowed = IsExponentialAllowedC;
                objCharacteristics_VM.UnitsAllowed = UnitsAllowedC;
                objCharacteristics_VM.TimeStamp = System.DateTime.Now;
                if (!IsEditCharacteristics)
                {
                    context.SaveChanges();
                    _notifier.ShowSuccess("Existing Characteristic Saved Successfully.");
                }
                else
                {
                    var chkAlreadyExist = context.Library_SAP_Characteristics.Count(x => x.SAP_Characteristic_Code == objCharacteristics_VM.SAP_Characteristic_Code);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("Characteristics Name already exists");
                    }
                    else
                    {
                        context.Library_SAP_Characteristics.Add(objCharacteristics_VM);
                        context.SaveChanges();
                        _notifier.ShowSuccess("New Characteristic Saved Successfully.");
                    }
                }
            }
            ClearPopUpVariableCharacteristics();
            BindCharacteristics();

        }
        public ICommand CommandAddMoreCharacteristics { get; private set; }
        private void CommandAddMoreCharacteristicsExecute(object obj)
        {
            SaveCharacteristics();
        }
    }
}
