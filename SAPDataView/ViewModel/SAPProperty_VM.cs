﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;

namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public Visibility VisibilityListDetailDashboard_Loader { get; set; }
        public CommonDataGrid_ViewModel<Library_SAP_Properties> comonDG_VM { get; set; }
        public ICommand CommandClosePopUp { get; private set; }
        public ICommand CommandShowPopUpAddHydrate { get; private set; }
        public Library_SAP_Properties SelectedProperty { get; set; }
        public string PropertyDescription { get; set; }
        public string SAP_PropertyCode { get; set; }
        public string Verisk_PropertyCode { get; set; }
        public string ActionComment { get; set; }
        public bool IsAct { get; set; }
        public bool? IsStandard { get; set; }
        public bool IsEdit { get; set; } = true;
        public Visibility PopUpContentAddHydrateVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalHydrusVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListChemicalAnHydrusVisibility { get; set; } = Visibility.Collapsed;

        private ObservableCollection<Library_SAP_Properties> _lstSAPProperty { get; set; } = new ObservableCollection<Library_SAP_Properties>();
        public ObservableCollection<Library_SAP_Properties> lstSAPProperty
        {
            get { return _lstSAPProperty; }
            set
            {
                if (_lstSAPProperty != value)
                {
                    _lstSAPProperty = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<Library_SAP_Properties>>>(new PropertyChangedMessage<List<Library_SAP_Properties>>(null, _lstSAPProperty.ToList(), "Default List"));
                }
            }
        }
        private void RunSAPProperties()
        {
            BindProperty();
        }
        private List<CommonDataGridColumn> GetListGridColumnSAPProperty()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SAP_Property_Code", ColBindingName = "SAP_Property_Code", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Property_Description", ColBindingName = "Property_Description", ColType = "Textbox", ColumnWidth = "250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Verisk_Property_Code", ColBindingName = "Verisk_Property_Code", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "Comments", ColType = "Textbox", ColumnWidth = "350" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsActive", ColBindingName = "IsActive", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsStandard Property", ColBindingName = "IsStandardProperty", ColType = "CheckboxEnable" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TimeStamp", ColBindingName = "TimeStamp", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "PropertyID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditName" });

            return listColumnQsidDetail;
        }

        private void BindProperty()
        {
            VisibilityListDetailDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
            Task.Run(() =>
            {
                var SAPPropertyTable = objCommon.DbaseQueryReturnSqlList<Library_SAP_Properties>("select * from SAP.Library_SAP_Properties", Win_Administrator_VM.CommonListConn);
                lstSAPProperty = new ObservableCollection<Library_SAP_Properties>(SAPPropertyTable);

            });
            VisibilityListDetailDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityListDetailDashboard_Loader");
        }

        private void CommandClosePopUpExecute(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUp = false;
        }
        private void CommandShowPopUpAddHydrateExecute(object obj)
        {
            ClearPopUpVariable();
            IsEdit = true;
            NotifyPropertyChanged("IsEdit");
            IsShowPopUp = true;
            PopUpContentAddHydrateVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
        }

        private void NotifyMe(PropertyChangedMessage<Library_SAP_Properties> obj)
        {
            SelectedProperty = obj.NewValue;
        }

        public void ClearPopUpVariable()
        {
            PropertyDescription = string.Empty;
            SAP_PropertyCode = string.Empty;
            Verisk_PropertyCode = string.Empty;
            ActionComment = string.Empty;
            IsAct = false;
            IsStandard = false;
            NotifyPropertyChanged("PropertyDescription");
            NotifyPropertyChanged("SAP_PropertyCode");
            NotifyPropertyChanged("Verisk_PropertyCode");
            NotifyPropertyChanged("ActionComment");
            NotifyPropertyChanged("IsAct");
            NotifyPropertyChanged("IsStandard");
        }

        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapHydrate")
            {
                //var result = MessageBox.Show("Are you sure do you want to delete this Hydrates?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                //if (result == MessageBoxResult.Yes)
                //{
                //    bool resultDel = _objGeneric_Service.DeleteMap_Hydrate(SelectedMapHydrate.HydrateID, Engine.CurrentUserSessionID);
                //    if (resultDel)
                //    {
                //        BindListHydrate();
                //        _notifier.ShowSuccess("Selected hydrate deleted successfully");
                //    }
                //    else
                //    {
                //        _notifier.ShowError("Selected hydrate not deleted due to internal server error!");
                //    }
                //}
            }
            else if (obj.Notification == "AddEditName")
            {
                IsEdit = false;
                NotifyPropertyChanged("IsEdit");
                SAP_PropertyCode = SelectedProperty.SAP_Property_Code;
                NotifyPropertyChanged("SAP_PropertyCode");
                PropertyDescription = SelectedProperty.Property_Description;
                NotifyPropertyChanged("PropertyDescription");
                Verisk_PropertyCode = SelectedProperty.Verisk_Property_Code;
                NotifyPropertyChanged("Verisk_PropertyCode");
                ActionComment = SelectedProperty.Comments;
                NotifyPropertyChanged("ActionComment");
                IsAct = SelectedProperty.IsActive;
                NotifyPropertyChanged("IsAct");
                IsStandard = SelectedProperty.IsStandardProperty;
                NotifyPropertyChanged("IsStandard");
                IsShowPopUp = true;
                PopUpContentAddHydrateVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
                ListChemicalAnHydrusVisibility = Visibility.Collapsed;
                ListChemicalHydrusVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListChemicalAnHydrusVisibility");
                NotifyPropertyChanged("ListChemicalHydrusVisibility");
            }
        }
        private bool _IsShowPopUp { get; set; }
        public bool IsShowPopUp
        {
            get { return _IsShowPopUp; }
            set
            {
                if (_IsShowPopUp != value)
                {
                    _IsShowPopUp = value;
                    NotifyPropertyChanged("IsShowPopUp");
                }
            }
        }
        private void RefreshGrid(PropertyChangedMessage<string> flag)
        {
            BindProperty();
            if (IsShowPopUp)
            {
                NotifyPropertyChanged("IsShowPopUp");
            }
        }
        public ICommand CommandAddHydrate { get; private set; }
        private void CommandAddHydrateExecute(object obj)
        {
            SaveHydrate();
            IsShowPopUp = false;
            IsEdit = true;
            NotifyPropertyChanged("IsEdit");
            ClearPopUpVariable();
            PopUpContentAddHydrateVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddHydrateVisibility");
        }
        private bool CommandAddHydrateCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(SAP_PropertyCode) || string.IsNullOrEmpty(Verisk_PropertyCode))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void SaveHydrate()
        {
            Library_SAP_Properties objHydrate_VM = new Library_SAP_Properties();
            using (var context = new CRAModel())
            {
                if (!IsEdit)
                {
                    objHydrate_VM = context.Library_SAP_Properties.Where(x => x.PropertyID == SelectedProperty.PropertyID).FirstOrDefault();
                }
                else
                {
                    var mx = Convert.ToInt32(objCommon.DbaseQueryReturnStringSQL("select IDENT_CURRENT('SAP.Library_SAP_Properties')", Win_Administrator_VM.CommonListConn));
                    objHydrate_VM.PropertyID = mx + 1;
                }
                objHydrate_VM.SAP_Property_Code = SAP_PropertyCode;
                objHydrate_VM.Property_Description = PropertyDescription;
                objHydrate_VM.IsActive = IsAct;
                objHydrate_VM.IsStandardProperty = IsStandard;
                objHydrate_VM.Verisk_Property_Code = Verisk_PropertyCode;
                objHydrate_VM.Comments = ActionComment;
                objHydrate_VM.TimeStamp = System.DateTime.Now;

                if (!IsEdit)
                {
                    context.SaveChanges();
                    _notifier.ShowSuccess("Existing Property Saved Successfully.");
                }
                else
                {
                    var chkAlreadyExist = context.Library_SAP_Properties.Count(x => x.SAP_Property_Code == objHydrate_VM.SAP_Property_Code);
                    if (chkAlreadyExist > 0)
                    {
                        _notifier.ShowError("Property Name already exists");
                    }
                    else
                    {
                        context.Library_SAP_Properties.Add(objHydrate_VM);
                        context.SaveChanges();
                        _notifier.ShowSuccess("New Property Saved Successfully.");
                    }
                }
            }
            ClearPopUpVariable();
            BindProperty();

        }
        public ICommand CommandAddMoreHydrate { get; private set; }
        private void CommandAddMoreHydrateExecute(object obj)
        {
            SaveHydrate();
        }
    }
}
