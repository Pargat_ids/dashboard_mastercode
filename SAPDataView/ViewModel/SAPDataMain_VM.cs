﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstSAPDataMain { get; set; }
        private void RunSAPDataMain()
        {
            using (var context = new CRAModel())
            {
                var SAPDataMainTable = new DataTable();
                int saplistId = 0;
                var sapList = objCommon.DbaseQueryReturnStringSQL("select top 1 SAP_ListID from SAP.Library_SAP_lists where SAP_listName ='" + Qsid + "'", Win_Administrator_VM.CommonListConn);
                if (sapList != "")
                {
                    saplistId = Convert.ToInt32(sapList);
                }

                SAPDataMainTable = rowIdentifier.MakeSAPDataMain(saplistId, false);
                if (SAPDataMainTable.Rows.Count == 0)
                {
                    lstSAPDataMain = null;
                }
                else
                {
                    lstSAPDataMain = new StandardDataGrid_VM("Data", SAPDataMainTable);
                }
                NotifyPropertyChanged("lstSAPDataMain");
            }

        }
    }
}
