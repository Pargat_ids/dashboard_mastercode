﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using SAPDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;


namespace SAPDataView.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstSAPData { get; set; }
        private void RunSAPData()
        {
            using (var context = new CRAModel())
            {
                var SAPDataTable = new DataTable();
                int saplistId = 0;
                var sapList = objCommon.DbaseQueryReturnStringSQL("select top 1 SAP_ListID from SAP.Library_SAP_lists where SAP_listName ='" + Qsid + "'", Win_Administrator_VM.CommonListConn);
                if (sapList != "")
                {
                    saplistId = Convert.ToInt32(sapList);
                }

                SAPDataTable = rowIdentifier.GetListDictionaryDataSAP(SAPDataTable,
                    saplistId, Win_Administrator_VM.CommonListConn);
                if (SAPDataTable.Rows.Count == 0)
                {
                    lstSAPData = null;
                }
                else
                {
                    lstSAPData = new StandardDataGrid_VM("List Dictionary", SAPDataTable);
                }
                NotifyPropertyChanged("lstSAPData");
            }

        }
    }
}



