﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using I4FTab.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using VersionControlSystem.Model.ApplicationEngine;

namespace I4FTab.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public ICommand CommandAttachDocument { get; set; }
        public ICommand CommandDownloadAttachment { get; set; }
        private CommonFunctions objCommon;
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public static int Qsid_ID { get; set; }
        public static string Qsid { get; set; }
        
        public TabItem selectedTabItem { get; set; }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }

        public void SelectedTabItemChanged()
        {
            switch (SelectedTabItem.Header)
            {

                case "List Priorities":
                    RunListPr();
                    break;
                
            }
        }
   
        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }

        public ICommand CommandTabChanged { get; set; }

        public static string CommonListConn;
        public ICommand SearchCommand { get; set; }
        private void SearchCommandExecute(object obj)
        {
            SelectedTabItemChanged();
        }

        public bool IsSelectedTabData { get; set; }
        public List<LibYesNo> listFlavor { get; set; } = new List<LibYesNo>();
        public List<LibYesNo> listFema { get; set; } = new List<LibYesNo>();
        public Win_Administrator_VM()
        {
            objCommon = new CommonFunctions();
            comonListPr_VM = new CommonDataGrid_ViewModel<MapListPr>(GetListGridColumnListPr(), "Property-Characteristics", "");

            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);

            listFlavor.Add(new LibYesNo { Value = "No" });
            listFlavor.Add(new LibYesNo { Value = "Yes" });
            listFema.Add(new LibYesNo { Value = "No" });
            listFema.Add(new LibYesNo { Value = "Yes" });
            
            IsSelectedTabData = true;

            CommandShowPopUpAddListPr = new RelayCommand(CommandShowPopUpAddListPrExecute);
            CommandClosePopUpListPr = new RelayCommand(CommandClosePopUpExecuteListPr);
            CommandAddListPr = new RelayCommand(CommandAddListPrExecute, CommandAddListPrCanExecute);
            CommandAddMoreListPr = new RelayCommand(CommandAddMoreListPrExecute, CommandAddListPrCanExecute);
            CommandAttachDocument = new RelayCommand(CommandAttachmentExecute);
            CommandDownloadAttachment = new RelayCommand(CommandDownloadAttachmentExecute);

            MessengerInstance.Register<PropertyChangedMessage<MapListPr>>(this, NotifyMeListPr);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(MapListPr), CommandButtonExecutedListPr);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(Win_Administrator_VM), RefreshGridListPr);
            RunListPr();
        }

        public void CommandAttachmentExecute(object obj)
        {
            if (obj.GetType() == typeof(System.Windows.Controls.ContextMenu))
            {
                Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
                _objAccessVersion_Service.AttachDocument();
            }
        }
        public void CommandDownloadAttachmentExecute(object obj)
        {
            Engine.CurrentSelectedTabItem = ((System.Windows.Controls.TabItem)((System.Windows.Controls.ContextMenu)obj).PlacementTarget).Header.ToString();
            _objAccessVersion_Service.DownloadAndShowDocument();
        }

    }
}
