﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using I4FTab.Library;
using ToastNotifications.Messages;
using CRA_DataAccess.ViewModel;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;

namespace I4FTab.ViewModel
{
    public class GroupThresholdUC_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<GroupThreshold_ListVM> commonListGroupThreshold_VM { get; set; }

        public CommonDataGrid_ViewModel<GroupThreshold_MemberListVM> commonListGroupThresholdMember_VM { get; set; }
        public ObservableCollection<GenericChemicalName_VM> listAllGT { get; set; } = new ObservableCollection<GenericChemicalName_VM>();
        public GroupThreshold_ListVM SelectedGT { get; set; }
        public GroupThreshold_MemberListVM SelectedGT_MemberCas { get; set; }
        public string GroupThresholdName { get; set; } = string.Empty;
        public string comments { get; set; } = string.Empty;
        public GenericChemicalName_VM _selected_GT_Name { get; set; }
        public GenericChemicalName_VM Selected_GT_Name
        {
            get { return _selected_GT_Name; }
            set
            {
                if (_selected_GT_Name != value)
                {
                    _selected_GT_Name = value;
                    if (_selected_GT_Name != null)
                    {
                        visibilityAllGT = Visibility.Collapsed;
                        BindExistingGTMembers();
                        NotifyPropertyChanged("visibilityAllGT");
                        visibilitySelectedGT_Name_Chip = Visibility.Visible;
                        visibilityGT_Member_List = Visibility.Visible;
                    }
                    else
                    {
                        visibilitySelectedGT_Name_Chip = Visibility.Collapsed;
                        visibilityGT_Member_List = Visibility.Collapsed;
                    }
                    NotifyPropertyChanged("visibilityGT_Member_List");
                    NotifyPropertyChanged("visibilitySelectedGT_Name_Chip");
                    NotifyPropertyChanged("Selected_GT_Name");
                }
            }
        }

        public int? ExistingMap_GT_ID { get; set; }
        public bool IsSelectGTMemberTab { get; set; }
        public bool IsSelectGTAppNameTab { get; set; }

        public GenericChemicalName_VM _selected_GT_AppName { get; set; }
        public GenericChemicalName_VM Selected_GT_AppName
        {
            get { return _selected_GT_AppName; }
            set
            {
                if (_selected_GT_AppName != value)
                {
                    _selected_GT_AppName = value;
                    if (_selected_GT_AppName != null)
                    {
                        visibilityAllGT_AppName = Visibility.Collapsed;
                        visibilitySelectedGT_Name_AppName = Visibility.Visible;
                        BindExistingThresholdAppNameList();
                        NotifyPropertyChanged("visibilityAllGT_AppName");
                        NotifyPropertyChanged("visibilitySelectedGT_Name_AppName");
                        IsEnabled = false;
                        NotifyPropertyChanged("IsEnabled");                        
                    }
                    else
                    {
                        visibilityGT_AppName_List = Visibility.Collapsed;
                        NotifyPropertyChanged("visibilityGT_AppName_List");
                        visibilitySelectedGT_Name_AppName = Visibility.Collapsed;
                        NotifyPropertyChanged("visibilitySelectedGT_Name_AppName");
                    }
                    NotifyPropertyChanged("Selected_GT_AppName");
                }
            }
        }

        public List<EntityClass.Library_Phrases> listAllApplicationName { get; set; }

        public EntityClass.Library_Phrases _filteredApplicationName { get; set; }
        public EntityClass.Library_Phrases FilteredApplicationName
        {
            get { return _filteredApplicationName; }
            set
            {                
                _filteredApplicationName = value;
                FilterApplicationName();
                NotifyPropertyChanged("FilteredApplicationName");
            }
        }

        public List<GroupThreshold_MemberListVM> _listAllSelected_Cas { get; set; } = new List<GroupThreshold_MemberListVM>();
        public List<GroupThreshold_MemberListVM> listAllSelected_Cas
        {
            get
            {
                return _listAllSelected_Cas;
            }
            set
            {
                _listAllSelected_Cas = value;
                MessengerInstance.Send<PropertyChangedMessage<List<GroupThreshold_MemberListVM>>>(new PropertyChangedMessage<List<GroupThreshold_MemberListVM>>(null, _listAllSelected_Cas, "Default List"));
            }
        }
        public ICommand CommandAddCasToList { get; set; }
        public ICommand CommandShowCreateNewGT { get; set; }
        public ICommand CommandCancelGroupThreshold { get; set; }
        public ICommand CommandSaveGroupThreshold { get; set; }
        public ICommand CommandSaveGTMembers { get; set; }
        public ICommand CommandImportMember_Access { get; set; }
        public ICommand CommandImportMember_Excel { get; set; }
        //public ICommand CommandDeleteCas_Member { get; set; }
        public ICommand CommandCancelGTMembers { get; set; }
        public ICommand CommandMapApplicationName { get; set; }
        public ICommand CommandSaveGTAppName { get; set; }
        public ICommand CommandAddNewAppName { get; set; }
        public ICommand CommandDeleteAppName { get; set; }
        public ICommand CommandClearFilterAppName { get; set; }
        public string _searchChemName { get; set; }
        public string searchChemName
        {
            get { return _searchChemName; }
            set
            {
                if (_searchChemName != value)
                {
                    _searchChemName = value;
                    NotifyPropertyChanged("searchChemName");
                    visibilitySelectedGT_Name_Chip = Visibility.Collapsed;
                    NotifyPropertyChanged("visibilitySelectedGT_Name_Chip");
                    FilterChemicalName_Parent(_searchChemName);
                    visibilityAllGT = Visibility.Visible;
                    NotifyPropertyChanged("visibilityAllGT");
                }
            }
        }
        public string _searchGT_AppName { get; set; }
        public string SearchGT_AppName
        {
            get { return _searchGT_AppName; }
            set
            {
                if (_searchGT_AppName != value)
                {
                    _searchGT_AppName = value;
                    NotifyPropertyChanged("_searchGT_AppName");
                    visibilitySelectedGT_Name_AppName = Visibility.Collapsed;
                    NotifyPropertyChanged("visibilitySelectedGT_Name_AppName");
                    FilterChemicalName_Parent(_searchGT_AppName);
                    visibilityAllGT_AppName = Visibility.Visible;
                    NotifyPropertyChanged("visibilityAllGT_AppName");
                }
            }
        }
        private List<GroupThreshold_ListVM> _listGroupThreshold { get; set; } = new List<GroupThreshold_ListVM>();
        public List<GroupThreshold_ListVM> listGroupThreshold
        {
            get { return _listGroupThreshold; }
            set
            {
                _listGroupThreshold = value;
                MessengerInstance.Send<PropertyChangedMessage<List<GroupThreshold_ListVM>>>(new PropertyChangedMessage<List<GroupThreshold_ListVM>>(null, _listGroupThreshold, "Default List"));
            }
        }
        public List<EntityClass.Library_QSIDs> listQsids { get; set; }

        public List<EntityClass.Library_Operators> listMinOperator { get; set; }
        public List<EntityClass.Library_Operators> listMaxOperator { get; set; }
        public ObservableCollection<LibraryUnits_VM> listUnit { get; set; }
        public ObservableCollection<GroupThreshold_ApplicationName_VM> listThreshold_AppName_Backend { get; set; } = new ObservableCollection<GroupThreshold_ApplicationName_VM>();
        public ObservableCollection<GroupThreshold_ApplicationName_VM> listThreshold_AppName { get; set; } = new ObservableCollection<GroupThreshold_ApplicationName_VM>();
        public string AddNewCas { get; set; } = string.Empty;
        public bool IsEnabled { get; set; } = true;
        public bool IsShowPopUp_GroupThreshold { get; set; } = false;
        public Visibility visibilityShowCreateGroupThreshold { get; set; } = Visibility.Collapsed;
        public Visibility visibilitySelectedGT_Name_Chip { get; set; } = Visibility.Collapsed;
        public Visibility visibilitySelectedGT_Name_AppName { get; set; } = Visibility.Collapsed;
        public Visibility visibilityAllGT { get; set; } = Visibility.Collapsed;
        public Visibility visibilityAllGT_AppName { get; set; } = Visibility.Collapsed;
        public Visibility visibilityGT_AppName_List { get; set; } = Visibility.Collapsed;
        public Visibility Visibility_Loader { get; set; } = Visibility.Collapsed;
        public Visibility ListGTLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility ListGTMemberLoaderVisibility { get; set; } = Visibility.Collapsed;
        public Visibility visibilityGT_Member_List { get; set; } = Visibility.Collapsed;

        public GroupThresholdUC_VM()
        {
            commonListGroupThreshold_VM = new CommonDataGrid_ViewModel<GroupThreshold_ListVM>(GetListColumnThreshold(), "", "");
            commonListGroupThresholdMember_VM = new CommonDataGrid_ViewModel<GroupThreshold_MemberListVM>(GetListColumnThresholdMemberList(), "", "");
            BindGroupThresholdList();
            BindAllLibraryQsid();
            BindOperator();
            BindUnits();
            CommandSaveGroupThreshold = new RelayCommand(CommandSaveGroupThresholdExecute, CommandSaveGroupThresholdCanExecute);
            CommandAddCasToList = new RelayCommand(commandAddCasToListExecute, commandAddCasToListCanExecute);
            CommandShowCreateNewGT = new RelayCommand(CommandShowCreateNewGTExecute);
            CommandCancelGroupThreshold = new RelayCommand(CommandCancelGroupThresholdExecute);
            CommandSaveGTMembers = new RelayCommand(CommandSaveGTMembersExecute, CommandSaveGTMembersCanExecute);
            //CommandDeleteCas_Member = new RelayCommand(CommandDeleteCas_MemberExecute);
            CommandImportMember_Access = new RelayCommand(CommandImportMember_AccessExecute);
            CommandImportMember_Excel = new RelayCommand(CommandImportMember_ExcelExecute);
            CommandCancelGTMembers = new RelayCommand(CommandCancelGTMembersExecute);
            CommandMapApplicationName = new RelayCommand(CommandMapApplicationNameExecute, CommandMapApplicationNameCanExecute);
            CommandSaveGTAppName = new RelayCommand(CommandSaveGTAppNameExecute, CommandSaveGTAppNameCanExecute);
            CommandAddNewAppName = new RelayCommand(CommandAddNewAppNameExecute, CommandAddNewAppNameCanExecute);
            CommandDeleteAppName = new RelayCommand(CommandDeleteAppNameExecute);
            CommandClearFilterAppName = new RelayCommand(CommandClearFilterAppNameExecute, CommandClearFilterAppNameCanExecute);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GroupThreshold_ListVM), CommandButtonExecuted);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(GroupThreshold_MemberListVM), CommandButtonExecuted_MemberList);
            MessengerInstance.Register<PropertyChangedMessage<GroupThreshold_ListVM>>(this, NotifyMe);
            MessengerInstance.Register<PropertyChangedMessage<GroupThreshold_MemberListVM>>(this, NotifyMe);
        }

        private bool CommandClearFilterAppNameCanExecute(object obj)
        {
            if (FilteredApplicationName != null)
            {
                return true;
            }
            else {
                return false;
            }
            
        }

        private void CommandClearFilterAppNameExecute(object obj)
        {
            FilteredApplicationName = null;
        }

        private void CommandDeleteAppNameExecute(object obj)
        {
            int seqNum = (int)obj;
            var gtObj = listThreshold_AppName.Where(x => x.seqNumber == seqNum).FirstOrDefault();
            if (gtObj != null)
            {
                listThreshold_AppName.Remove(gtObj);
                listThreshold_AppName_Backend.Remove(gtObj);
            }
            if (listThreshold_AppName_Backend.Count == 0)
            {
                CreateNewThresholdAppName();
            }
            else
            {
                listThreshold_AppName = listThreshold_AppName_Backend;
            }
            NotifyPropertyChanged("listThreshold_AppName");
            BindAllApplicationNameFromList();
        }

        private void CommandButtonExecuted_MemberList(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMemberCas")
            {
                CommandDeleteCas_Member();
            }
            else if (obj.Notification == "GoToSubstanceIdentity")
            {
                MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedGT_MemberCas.Cas, "ActiveSubstanceIdentityWithCas"), typeof(GroupThresholdUC_VM));
                //MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", SelectedGT_MemberCas.Cas, "SearchCasInSubstanceIdentityTab"), typeof(GroupThresholdUC_VM));
            }
        }



        private void NotifyMe(PropertyChangedMessage<GroupThreshold_ListVM> obj)
        {
            SelectedGT = obj.NewValue;
        }
        private void NotifyMe(PropertyChangedMessage<GroupThreshold_MemberListVM> obj)
        {
            SelectedGT_MemberCas = obj.NewValue;
        }

        private void CommandButtonExecuted(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "EditGroupThresholdName")
            {
                CommandShowCreateNewGT.Execute(null);
                GroupThresholdName = SelectedGT.GroupThresholdName;
                comments = SelectedGT.comments;
                ExistingMap_GT_ID = SelectedGT.ChemicalNameID;
                NotifyPropertyChanged("GroupThresholdName");
                NotifyPropertyChanged("comments");
            }
            if (obj.Notification == "AddEditGT_Members")
            {
                IsSelectGTMemberTab = true;
                var objSelect = new GenericChemicalName_VM();
                objSelect.ChemicalName = SelectedGT.GroupThresholdName;
                objSelect.ChemicalNameID = SelectedGT.ChemicalNameID;
                Selected_GT_Name = objSelect;

                NotifyPropertyChanged("IsSelectGTMemberTab");
                NotifyPropertyChanged("Selected_GT_Name");
            }
            if (obj.Notification == "AddEditGT_ApplicationName")
            {
                var objSelect = new GenericChemicalName_VM();
                objSelect.ChemicalName = SelectedGT.GroupThresholdName;
                objSelect.ChemicalNameID = SelectedGT.ChemicalNameID;
                Selected_GT_AppName = objSelect;
                IsSelectGTAppNameTab = true;
                NotifyPropertyChanged("IsSelectGTAppNameTab");
            }
        }

        private bool CommandAddNewAppNameCanExecute(object obj)
        {
            return ThresholdAppNameCanAddNewItem();
        }

        private void CommandAddNewAppNameExecute(object obj)
        {
            CreateNewThresholdAppName();
        }

        private bool CommandSaveGTAppNameCanExecute(object obj)
        {
            return ThresholdAppNameCanAddNewItem();
        }

        private void CommandSaveGTAppNameExecute(object obj)
        {
            var response = _objLibraryFunction_Service.AddEditGroupThresholdAppName(ConvertViewModelToEntityClass(listThreshold_AppName_Backend.ToList()), Selected_GT_AppName.ChemicalNameID, Engine.CurrentUserSessionID);
            if (response.Status)
            {
                BindGroupThresholdList();
                _notifier.ShowSuccess("Group Threshold application name saved successfully.");
            }
            else
            {
                _notifier.ShowError("Group Threshold application name failed due to" + response.Description);
            }

        }

        public List<EntityClass.Map_GroupThreshold_QSID_Application> ConvertViewModelToEntityClass(List<GroupThreshold_ApplicationName_VM> lisAppName)
        {
            return lisAppName.Where(x => x.SelectedApplicationName != null).Select(x => new EntityClass.Map_GroupThreshold_QSID_Application
            {
                ChemicalNameID = x.ChemicalNameID,
                Maxvalue = x.maxValue,
                MaxValueOpID = x.SelectedMaxOperator != null ? x.SelectedMaxOperator.OperatorID : (int?)null,
                Minvalue = x.minValue,
                MinValueOpID = x.SelectedMinOperator != null ? x.SelectedMinOperator.OperatorID : (int?)null,
                PhraseID_ApplicationName = x.SelectedApplicationName.PhraseID,
                QSID_ID = x.SelectedQsid.QSID_ID,
                UnitID = x.SelectedUnit.UnitID,
            }).ToList();
        }
        public List<GroupThreshold_ApplicationName_VM> ConvertEntityClassToVMClass(List<EntityClass.Map_GroupThreshold_QSID_Application> listAppName)
        {
            int counter = 0;
            return listAppName.Select(x => new GroupThreshold_ApplicationName_VM
            {
                seqNumber = counter = (counter + 1),
                SelectedQsid = listQsids.Where(y => y.QSID_ID == x.QSID_ID).FirstOrDefault(),
                ChemicalNameID = x.ChemicalNameID,
                maxValue = x.Maxvalue ?? 0,
                SelectedMaxOperator = x.MaxValueOpID != null ? listMaxOperator.Where(y => y.OperatorID == x.MaxValueOpID).FirstOrDefault() : null,
                minValue = x.Minvalue ?? 0,
                SelectedMinOperator = x.MinValueOpID != null ? listMinOperator.Where(y => y.OperatorID == x.MinValueOpID).FirstOrDefault() : null,
                ApplicationNameID = x.PhraseID_ApplicationName,
                SelectedUnit = listUnit.Where(y => y.UnitID == x.UnitID).FirstOrDefault()

            }).ToList();
        }

        private bool CommandMapApplicationNameCanExecute(object obj)
        {
            if (Selected_GT_AppName != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandMapApplicationNameExecute(object obj)
        {
            IsEnabled = false;
            NotifyPropertyChanged("IsEnabled");
            //BindApplicationName();
            BindExistingThresholdAppNameList();
           
        }
        private bool ThresholdAppNameCanAddNewItem()
        {
            if (!listThreshold_AppName_Backend.Any(x => x.SelectedApplicationName != null && x.SelectedUnit != null && (x.maxValue > 0 || x.minValue > 0)))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public void CreateNewThresholdAppName()
        {
            //listThreshold_AppName.Insert(0, new GroupThreshold_ApplicationName_VM { seqNumber = listThreshold_AppName_Backend.Count + 1, ChemicalNameID = Selected_GT_AppName.ChemicalNameID });
            listThreshold_AppName_Backend.Insert(0, new GroupThreshold_ApplicationName_VM { seqNumber = listThreshold_AppName_Backend.Count + 1, ChemicalNameID = Selected_GT_AppName.ChemicalNameID });
            //listThreshold_AppName = new ObservableCollection<GroupThreshold_ApplicationName_VM>(listThreshold_AppName);
            NotifyPropertyChanged("listThreshold_AppName");
            BindAllApplicationNameFromList();
        }
        public void BindExistingThresholdAppNameList()
        {
            Visibility_Loader = Visibility.Visible;
            NotifyPropertyChanged("Visibility_Loader");
           visibilityGT_AppName_List = Visibility.Collapsed;
            NotifyPropertyChanged("visibilityGT_AppName_List");
            Task.Run(() =>
            {
                var list = _objLibraryFunction_Service.GetExistingThresholdName(Selected_GT_AppName.ChemicalNameID);
                if (list.Count > 0)
                {
                    listThreshold_AppName_Backend = new ObservableCollection<GroupThreshold_ApplicationName_VM>(ConvertEntityClassToVMClass(list));
                    listThreshold_AppName = listThreshold_AppName_Backend;
                }
                else
                {
                    listThreshold_AppName_Backend = new ObservableCollection<GroupThreshold_ApplicationName_VM>();
                    listThreshold_AppName = listThreshold_AppName_Backend;
                    CreateNewThresholdAppName();
                }
                BindAllApplicationNameFromList();
                NotifyPropertyChanged("listThreshold_AppName");
                Visibility_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("Visibility_Loader");
                visibilityGT_AppName_List = Visibility.Visible;
                NotifyPropertyChanged("visibilityGT_AppName_List");
            });
        }


        public void BindOperator()
        {
            var listOperator = _objLibraryFunction_Service.GetAllOperators();
            listMaxOperator = listOperator.Where(x => x.Operator == "<" || x.Operator == "<=").ToList();
            NotifyPropertyChanged("listMaxOperator");
            listMinOperator = listOperator.Where(x => x.Operator == ">" || x.Operator == ">=").ToList();
            NotifyPropertyChanged("listMinOperator");
        }
        public void BindUnits()
        {
            Task.Run(() =>
            {
                listUnit = new ObservableCollection<LibraryUnits_VM>(_objLibraryFunction_Service.GetAllUnit());
                NotifyPropertyChanged("listUnit");
            });
        }
        public void BindAllLibraryQsid()
        {
            Task.Run(() =>
            {
                listQsids = _objLibraryFunction_Service.GetAllQsidListForThreshold();
                NotifyPropertyChanged("listQsids");
            });
        }


        private void CommandImportMember_ExcelExecute(object obj)
        {
            UploadCasMember("Excel");
        }

        private void CommandImportMember_AccessExecute(object obj)
        {
            UploadCasMember("Access");
        }
        private void CommandCancelGTMembersExecute(object obj)
        {
            if ((string)obj == "AppName")
            {
                //SelectedQsid = null;
                SearchGT_AppName = string.Empty;
                Selected_GT_AppName = null;
                listThreshold_AppName = new ObservableCollection<GroupThreshold_ApplicationName_VM>();
                listThreshold_AppName_Backend = new ObservableCollection<GroupThreshold_ApplicationName_VM>();
                IsEnabled = true;

                visibilityGT_AppName_List = Visibility.Collapsed;
                NotifyPropertyChanged("SearchGT_AppName");
                NotifyPropertyChanged("visibilityGT_AppName_List");
                NotifyPropertyChanged("SelectedQsid");
                NotifyPropertyChanged("listThreshold_AppName");
                NotifyPropertyChanged("IsEnabled");
            }
            else
            {
                Selected_GT_Name = null;
                listAllSelected_Cas = new List<GroupThreshold_MemberListVM>();

            }
        }

        //private void CommandDeleteCas_MemberExecute(object obj)
        //{
        //    string cas = (string)obj;
        //    if (listAllSelected_Cas.Any(x => x == cas))
        //    {
        //        listAllSelected_Cas.Remove(cas);
        //    }
        //    
        //}
        private void CommandDeleteCas_Member()
        {
            var itemCas = listAllSelected_Cas.Where(x => x.Cas == SelectedGT_MemberCas.Cas).FirstOrDefault();
            if (itemCas != null)
            {
                listAllSelected_Cas.Remove(itemCas);
                listAllSelected_Cas = new List<GroupThreshold_MemberListVM>(listAllSelected_Cas);
            }


        }
        public void BindExistingGTMembers()
        {
            ListGTMemberLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("ListGTMemberLoaderVisibility");
            Task.Run(() =>
            {
                var listCas = _objLibraryFunction_Service.GetExistingGTMembers(Selected_GT_Name.ChemicalNameID);
                //var listExistiingCas = new List<GroupThreshold_MemberListVM>();
                //foreach (var item in listCas)
                //{
                //    listExistiingCas.Add(AddNewItem_MemberCasList(item));
                //}
                listAllSelected_Cas = listCas;
                ListGTMemberLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListGTMemberLoaderVisibility");
                BindGroupThresholdList();
            });
        }

        private bool CommandSaveGTMembersCanExecute(object obj)
        {
            if (Selected_GT_Name != null && listAllSelected_Cas.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandSaveGTMembersExecute(object obj)
        {
            if (Selected_GT_Name.ChemicalNameID != 0)
            {
                var response = _objLibraryFunction_Service.AddEditGroupThresholdMember(listAllSelected_Cas.Select(x => x.Cas).ToList(), Selected_GT_Name.ChemicalNameID, Engine.CurrentUserSessionID);
                if (response.Status)
                {
                    _notifier.ShowSuccess("Group Threshold Member saved successfully!");
                }
                else
                {
                    _notifier.ShowError("Group Threshold Member saved failed due to " + response.Description);
                }
            }
        }

        public void ResetVisibilityGroupThreshold()
        {
            visibilityShowCreateGroupThreshold = Visibility.Collapsed;
            NotifyPropertyChanged("visibilityShowCreateGroupThreshold");
        }

        private void CommandCancelGroupThresholdExecute(object obj)
        {
            IsShowPopUp_GroupThreshold = false;
            NotifyPropertyChanged("IsShowPopUp_GroupThreshold");
        }

        public void ClearNewGroupThreshold()
        {
            ExistingMap_GT_ID = (int?)null;
            GroupThresholdName = string.Empty;
            comments = string.Empty;
            NotifyPropertyChanged("GroupThresholdName");
            NotifyPropertyChanged("comments");
        }
        private void CommandShowCreateNewGTExecute(object obj)
        {
            ResetVisibilityGroupThreshold();
            ClearNewGroupThreshold();
            IsShowPopUp_GroupThreshold = true;
            visibilityShowCreateGroupThreshold = Visibility.Visible;
            NotifyPropertyChanged("IsShowPopUp_GroupThreshold");
            NotifyPropertyChanged("visibilityShowCreateGroupThreshold");


        }

        private bool commandAddCasToListCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(AddNewCas))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void commandAddCasToListExecute(object obj)
        {
            AddNewCas = AddNewCas.Trim();
            if (!listAllSelected_Cas.Any(x => x.Cas == AddNewCas))
            {

                listAllSelected_Cas.Add(AddNewItem_MemberCasList(AddNewCas));
                listAllSelected_Cas = new List<GroupThreshold_MemberListVM>(listAllSelected_Cas);
                AddNewCas = string.Empty;
                NotifyPropertyChanged("AddNewCas");
            }
            else
            {
                _notifier.ShowWarning("Cas already added!");
            }
        }

        public void BindGroupThresholdList()
        {
            ListGTLoaderVisibility = Visibility.Visible;
            NotifyPropertyChanged("ListGTLoaderVisibility");
            Task.Run(() =>
            {
                listGroupThreshold = _objLibraryFunction_Service.GetListGroupThreshold().Select(x => new GroupThreshold_ListVM { GroupThresholdName = x.GroupThresholdName, ChemicalNameID = x.ChemicalNameID, comments = x.Comment, CountAppName = x.CountAppName, CountMembers = x.CountMembers }).ToList();
                ListGTLoaderVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("ListGTLoaderVisibility");
            });
        }
        public List<CommonDataGridColumn> GetListColumnThreshold()
        {
            List<CommonDataGridColumn> listColTH = new List<CommonDataGridColumn>();
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Group Threshold Name", ColBindingName = "GroupThresholdName", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Comments", ColBindingName = "comments", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Members Count", ColBindingName = "CountMembers", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ApplicationName Count", ColBindingName = "CountAppName", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Edit", ColBindingName = "LibGroupThresholdNameID", ColType = "Button", ColumnWidth = "100", ContentName = "Edit", BackgroundColor = "#07689f", CommandParam = "EditGroupThresholdName" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Members", ColBindingName = "LibGroupThresholdNameID", ColType = "Button", ColumnWidth = "150", ContentName = "Add/Edit Members", BackgroundColor = "#2f5d62", CommandParam = "AddEditGT_Members" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Application Name", ColBindingName = "LibGroupThresholdNameID", ColType = "Button", ColumnWidth = "150", ContentName = "Add/Edit ApplicationName", BackgroundColor = "#310b0b", CommandParam = "AddEditGT_ApplicationName" });
            return listColTH;
        }
        private List<CommonDataGridColumn> GetListColumnThresholdMemberList()
        {
            List<CommonDataGridColumn> listColTH = new List<CommonDataGridColumn>();
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Cas", ColBindingName = "Cas", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Primary Name", ColBindingName = "PrimaryName", ColType = "Textbox" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "EC Number", ColBindingName = "EcNumber", ColType = "Textbox", ColumnWidth = "200" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "E Number", ColBindingName = "ENumber", ColType = "Textbox", ColumnWidth = "200" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "INS Number", ColBindingName = "InsNumber", ColType = "Textbox", ColumnWidth = "200" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Go to Substance Identity", ColBindingName = "Cas", ColType = "Button", ColumnWidth = "200", ContentName = "Go", BackgroundColor = "#007580", CommandParam = "GoToSubstanceIdentity" });
            listColTH.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Delete", ColBindingName = "CasId", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ef4f4f", CommandParam = "DeleteMemberCas" });
            return listColTH;
        }

        private bool CommandSaveGroupThresholdCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(GroupThresholdName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void CommandSaveGroupThresholdExecute(object obj)
        {
            visibilityShowCreateGroupThreshold = Visibility.Collapsed;
            NotifyPropertyChanged("visibilityShowCreateGroupThreshold");

            Task.Run(() =>
            {
                var response = _objLibraryFunction_Service.AddEditGroupThreshold(ExistingMap_GT_ID, GroupThresholdName, comments, Engine.CurrentUserSessionID);
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (response.Status)
                    {
                        BindGroupThresholdList();
                        _notifier.ShowSuccess("Group threshold name successfully added.");
                    }
                    else
                    {
                        _notifier.ShowError("Group threshold not added due to " + response.Description);
                    }
                    IsShowPopUp_GroupThreshold = false;
                    NotifyPropertyChanged("IsShowPopUp_GroupThreshold");
                });
            });
        }

        private void FilterChemicalName_Parent(string chemName)
        {
            if (!string.IsNullOrEmpty(chemName))
            {
                Task.Run(() =>
                {
                    listAllGT = new ObservableCollection<GenericChemicalName_VM>(_objLibraryFunction_Service.FilterGroupThreshold(chemName.ToLower()));
                    NotifyPropertyChanged("listAllGT");

                });
            }
            else
            {
                listAllGT = new ObservableCollection<GenericChemicalName_VM>();
                NotifyPropertyChanged("listAllGT");

            }
        }

        public void UploadCasMember(string type)
        {
            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();
            openFileDlg.Multiselect = false;
            var selectedFiles = openFileDlg.ShowDialog();
            if (selectedFiles == true)
            {
                string[] SelectedPath = openFileDlg.FileNames;
                if (SelectedPath.Count() > 0)
                {
                    if (!IsFileLocked(SelectedPath[0]))
                    {
                        ImportBulkGenericSuggestion(SelectedPath[0], type);
                        // BindGenericSuggestion();
                    }
                    else
                    {
                        _notifier.ShowError("File is already in use, kindly close it.");
                    }
                }
            }
        }
        public void ImportBulkGenericSuggestion(string path, string type)
        {
            DataTable dt = new DataTable();
            if (type == "Excel")
            {
                dt = Common.CommonGeneralFunction.ImportBulkExcel(path);

            }
            if (type == "Access")
            {
                string query = "Select cas from Members";
                dt = _objLibraryFunction_Service.GetListDataTableFromAccess(query, path);
            }
            //Task.Run(() =>
            //{
            //    _objLibraryFunction_Service.InsertBulkDataImportGroupThresholdMember(dt, Engine.CurrentUserSessionID);
            //});
            var list = dt.AsEnumerable().Select(x => x.Field<string>("Cas")).ToList();
            foreach (var item in list)
            {

                if (!listAllSelected_Cas.Any(x => x.Cas == item))
                {
                    listAllSelected_Cas.Add(AddNewItem_MemberCasList(item));
                }

            }
            listAllSelected_Cas = new List<GroupThreshold_MemberListVM>(listAllSelected_Cas);
        }

        public GroupThreshold_MemberListVM AddNewItem_MemberCasList(string cas)
        {
            cas = cas.Replace("-", "");
            string primaryName = _objLibraryFunction_Service.GetMapPreferSubstanceNameByCas(cas);
            List<string> listEcNumber = _objLibraryFunction_Service.ListIdentifierECNumber(cas);
            List<string> listENumber = _objLibraryFunction_Service.ListIdentifierENumber(cas);
            return new GroupThreshold_MemberListVM { PrimaryName = primaryName, Cas = cas, EcNumber = string.Join("; ", listEcNumber), ENumber = string.Join("; ", listENumber) };

        }

        public void FilterApplicationName()
        {
            Task.Run(() =>
            {
                if (FilteredApplicationName != null)
                {
                    listThreshold_AppName = new ObservableCollection<GroupThreshold_ApplicationName_VM>(listThreshold_AppName_Backend.Where(x => x.ApplicationNameID == FilteredApplicationName.PhraseID).ToList());
                }
                else
                {
                    listThreshold_AppName = listThreshold_AppName_Backend;
                }
                NotifyPropertyChanged("listThreshold_AppName");
            });
        }

        public void BindAllApplicationNameFromList()
        {
            listAllApplicationName = listThreshold_AppName_Backend.Where(x=>x.ApplicationNameID!=0).Select(x => new EntityClass.Library_Phrases { PhraseID = x.ApplicationNameID, Phrase = x.SelectedApplicationName.Phrase }).OrderBy(x=>x.Phrase).ToList();
            NotifyPropertyChanged("listAllApplicationName");
        }

    }
}
