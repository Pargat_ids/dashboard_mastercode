﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using I4FTab.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using System.Security.RightsManagement;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;

namespace I4FTab.ViewModel
{
    public partial class Win_Administrator_VM : Base_ViewModel
    {
        public string ListFieldName { get; set; }
        public string ListType { get; set; }
        public CommonDataGrid_ViewModel<MapListPr> comonListPr_VM { get; set; }
        public Visibility VisibilityListPrDashboard_Loader { get; set; }
        public ICommand CommandClosePopUpListPr { get; private set; }
        public ICommand CommandShowPopUpAddListPr { get; private set; }
        public bool IsEditListPr { get; set; } = true;
        public MapListPr SelectedListPr { get; set; }
        public Visibility PopUpContentAddListPrVisibility { get; set; } = Visibility.Collapsed;
        private ObservableCollection<MapListPr> _lstListPr { get; set; } = new ObservableCollection<MapListPr>();
        public ObservableCollection<MapListPr> lstListPr
        {
            get { return _lstListPr; }
            set
            {
                if (_lstListPr != value)
                {
                    _lstListPr = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<MapListPr>>>(new PropertyChangedMessage<List<MapListPr>>(null, _lstListPr.ToList(), "Default List"));
                }
            }
        }
        private void BindListPr()
        {
            VisibilityListPrDashboard_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityListPrDashboard_Loader");
            Task.Run(() =>
            {
                var SAPListPrTable = objCommon.DbaseQueryReturnSqlList<MapListPr>("select distinct a.QSID_ID as QSIDID , b.QSID, " +
                 " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                 " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                 " and z.PhraseCategory = 'List Field Name' and x.QSID_ID = a.QSID_ID) as 'ListFieldName', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'List Type' and x.QSID_ID = a.QSID_ID) as 'ListType', " +
                "  (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'Country' and x.QSID_ID = a.QSID_ID) as 'Country', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'List Phrase' and x.QSID_ID = a.QSID_ID) as 'ListPhrase', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'Short Description' and x.QSID_ID = a.QSID_ID) as 'ShortDescription', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'Short Name' and x.QSID_ID = a.QSID_ID) as 'ShortName', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'Compliance check list priority' and x.QSID_ID = a.QSID_ID) as 'PrioritywithinCountry', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'FEMA list flag' and x.QSID_ID = a.QSID_ID) as 'FEMAlistflag', " +
                " (select phrase from QSxxx_ListDictionary x, Library_Phrases y, Library_PhraseCategories z " +
                " where x.PhraseID = y.PhraseID and x.PhraseCategoryID = z.PhraseCategoryID " +
                " and z.PhraseCategory = 'Flavor list flag' and x.QSID_ID = a.QSID_ID) as 'Flavorlistflag' " +
                "  from QSxxx_ListDictionary a,Library_QSIDs b where a.QSID_ID = b.QSID_ID  and b.qsid like 'QSFF%'", Win_Administrator_VM.CommonListConn);
                lstListPr = new ObservableCollection<MapListPr>(SAPListPrTable);
            });
            NotifyPropertyChanged("comonListPr_VM");
            VisibilityListPrDashboard_Loader = Visibility.Collapsed;
            NotifyPropertyChanged("VisibilityListPrDashboard_Loader");
        }
        private bool _IsShowPopUpListPr { get; set; }
        public bool IsShowPopUpListPr
        {
            get { return _IsShowPopUpListPr; }
            set
            {
                if (_IsShowPopUpListPr != value)
                {
                    _IsShowPopUpListPr = value;
                    NotifyPropertyChanged("IsShowPopUpListPr");
                }
            }
        }
        private void RunListPr()
        {
            BindListPr();

        }



        private void RefreshGridListPr(PropertyChangedMessage<string> flag)
        {
            BindListPr();
            if (IsShowPopUpListPr)
            {
                NotifyPropertyChanged("IsShowPopUpListPr");
            }
        }
        public ICommand CommandAddListPr { get; private set; }
        private void CommandAddListPrExecute(object obj)
        {
            SaveListPr();
            IsShowPopUpListPr = false;
            IsEditListPr = true;
            NotifyPropertyChanged("IsEditListPr");
            ClearPopUpVariableListPr();
            PopUpContentAddListPrVisibility = Visibility.Collapsed;
            NotifyPropertyChanged("PopUpContentAddListPrVisibility");
        }
        private bool CommandAddListPrCanExecute(object obj)
        {
            //if (string.IsNullOrEmpty(PropertyID.ToString()) || string.IsNullOrEmpty(CharacteristicsID.ToString()))
            //{
            //    return false;
            //}
            //else
            //{
            return true;
            //}
        }

        private void addinQsxxxLstDictionary(int PhraseID, int PhraseCategoryID, int currentQsidId, int currentSessId)
        {
            List<Log_QSxxx_ListDictionary> lstFinal = new List<Log_QSxxx_ListDictionary>();
            List<QSxxx_ListDictionary> qsxxxTab = new List<QSxxx_ListDictionary>();
            var qTab = new QSxxx_ListDictionary
            {
                PhraseID = PhraseID,
                PhraseCategoryID = PhraseCategoryID,
                QSID_ID = currentQsidId,
            };
            qsxxxTab.Add(qTab);
            _objLibraryFunction_Service.BulkIns<QSxxx_ListDictionary>(qsxxxTab);
            int maxid = objCommon.GetMaxAutoFieldWithoutIncrement("QSxxx_ListDictionary");
            var libraryListDic = new Log_QSxxx_ListDictionary
            {
                SessionID = currentSessId,
                TimeStamp = objCommon.ESTTime(),
                Status = "A",
                QSxxx_ListDictionary_ID = maxid,
                ListDictionaryID = 1,
                PhraseID = PhraseID,
                PhraseCategoryID = PhraseCategoryID,
                QSID_ID = currentQsidId,
            };
            lstFinal.Add(libraryListDic);
            _objLibraryFunction_Service.BulkIns<Log_QSxxx_ListDictionary>(lstFinal);

        }
        public void SaveListPr()
        {
            QSxxx_ListDictionary objListPr_VM = new QSxxx_ListDictionary();
            using (var context = new CRAModel())
            {
                var lstphrCat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "Compliance check list priority" || x.PhraseCategory == "FEMA list flag" || x.PhraseCategory == "Flavor list flag").Select(y => new { y.PhraseCategoryID, y.PhraseCategory }).OrderBy(z => z.PhraseCategory).ToList();

                if (PrioritywithinCountry != 0 && PrioritywithinCountry != null)
                {
                    var chkExist = objCommon.DbaseQueryReturnTableSql("select top 1 * from QSxxx_ListDictionary where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[0].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                    if (chkExist.Rows.Count == 0 || chkExist == null)
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + PrioritywithinCountry.ToString() + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(PrioritywithinCountry.ToString(), 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + PrioritywithinCountry.ToString() + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        addinQsxxxLstDictionary(Convert.ToInt32(phIds), lstphrCat[0].PhraseCategoryID, SelectedListPr.QSIDID, Engine.CurrentUserSessionID);
                    }
                    else
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + PrioritywithinCountry.ToString() + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(PrioritywithinCountry.ToString(), 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + PrioritywithinCountry.ToString() + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (chkExist.Rows[0]["PhraseId"].ToString() != phIds)
                        {
                            objCommon.DbaseQueryReturnStringSQL("insert into Log_QSxxx_ListDictionary(SessionID,TimeStamp,Status,ListDictionaryID,PhraseID,PhraseCategoryID,QSID_ID,QSxxx_ListDictionary_ID)" +
                          " select " + Engine.CurrentUserSessionID + ",'" + objCommon.ESTTime() + "','C',1," + phIds + ", PhraseCategoryID, Qsid_Id, QSxxx_ListDictionary_ID from Qsxxx_ListDictionary where qsid_id =" + SelectedListPr.QSIDID + " and PhraseCategoryID =" + lstphrCat[0].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                            objCommon.DbaseQueryReturnStringSQL("update QSxxx_ListDictionary set PhraseID =" + phIds + " where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[0].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                        }
                    }
                }

                if (SelectedFema != null && string.IsNullOrEmpty(SelectedFema.Value) == false)
                {
                    var chkExist1 = objCommon.DbaseQueryReturnTableSql("select top 1 * from QSxxx_ListDictionary where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[1].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                    if (chkExist1 == null || chkExist1.Rows.Count == 0)
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFema.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(SelectedFema.Value, 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFema.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        addinQsxxxLstDictionary(Convert.ToInt32(phIds), lstphrCat[1].PhraseCategoryID, SelectedListPr.QSIDID, Engine.CurrentUserSessionID);
                    }
                    else
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFema.Value + "'", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(SelectedFema.Value, 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFema.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (chkExist1.Rows[0]["PhraseId"].ToString() != phIds)
                        {
                            objCommon.DbaseQueryReturnStringSQL("insert into Log_QSxxx_ListDictionary(SessionID,TimeStamp,Status,ListDictionaryID,PhraseID,PhraseCategoryID,QSID_ID,QSxxx_ListDictionary_ID)" +
                          " select " + Engine.CurrentUserSessionID + ",'" + objCommon.ESTTime() + "','C',1," + phIds + ", PhraseCategoryID, Qsid_Id, QSxxx_ListDictionary_ID from Qsxxx_ListDictionary where qsid_id =" + SelectedListPr.QSIDID + " and PhraseCategoryID =" + lstphrCat[1].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                            objCommon.DbaseQueryReturnStringSQL("update QSxxx_ListDictionary set PhraseID =" + phIds + " where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[1].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                        }
                    }
                }

                if (SelectedFlavor != null && string.IsNullOrEmpty(SelectedFlavor.Value) == false)
                {
                    var chkExist2 = objCommon.DbaseQueryReturnTableSql("select top 1 * from QSxxx_ListDictionary where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[2].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                    if (chkExist2 == null || chkExist2.Rows.Count == 0)
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFlavor.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(SelectedFlavor.Value, 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFlavor.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        addinQsxxxLstDictionary(Convert.ToInt32(phIds), lstphrCat[2].PhraseCategoryID, SelectedListPr.QSIDID, Engine.CurrentUserSessionID);
                    }
                    else
                    {
                        var phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFlavor.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (phIds == string.Empty)
                        {
                            _objLibraryFunction_Service.AddDataToPhareLibrary(SelectedFlavor.Value, 1, objCommon.ESTTime());
                        }
                        phIds = objCommon.DbaseQueryReturnStringSQL("select top 1 * from Library_Phrases where phrase ='" + SelectedFlavor.Value + "' COLLATE SQL_Latin1_General_CP1_CS_AS", Win_Administrator_VM.CommonListConn);
                        if (chkExist2.Rows[0]["PhraseId"].ToString() != phIds)
                        {
                            objCommon.DbaseQueryReturnStringSQL("insert into Log_QSxxx_ListDictionary(SessionID,TimeStamp,Status,ListDictionaryID,PhraseID,PhraseCategoryID,QSID_ID,QSxxx_ListDictionary_ID)" +
                          " select " + Engine.CurrentUserSessionID + ",'" + objCommon.ESTTime() + "','C',1," + phIds + ", PhraseCategoryID, Qsid_Id, QSxxx_ListDictionary_ID from Qsxxx_ListDictionary where qsid_id =" + SelectedListPr.QSIDID + " and PhraseCategoryID =" + lstphrCat[2].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                            objCommon.DbaseQueryReturnStringSQL("update QSxxx_ListDictionary set PhraseID =" + phIds + " where QSID_ID =" + SelectedListPr.QSIDID + " and PhraseCategoryID = " + lstphrCat[2].PhraseCategoryID, Win_Administrator_VM.CommonListConn);
                        }
                    }
                }
                _notifier.ShowSuccess("List Priority updated Successfully.");
            }
            ClearPopUpVariableListPr();
            BindListPr();

        }
        public ICommand CommandAddMoreListPr { get; private set; }
        private void CommandAddMoreListPrExecute(object obj)
        {
            SaveListPr();
        }
        private void CommandClosePopUpExecuteListPr(object obj)
        {
            string Currenttab = (string)obj;
            IsShowPopUpListPr = false;

        }
        private void CommandShowPopUpAddListPrExecute(object obj)
        {
            ClearPopUpVariableListPr();
            IsEditListPr = true;
            NotifyPropertyChanged("IsEditListPr");
            IsShowPopUpListPr = true;
            PopUpContentAddListPrVisibility = Visibility.Visible;
            NotifyPropertyChanged("PopUpContentAddListPrVisibility");
        }
        private void NotifyMeListPr(PropertyChangedMessage<MapListPr> obj)
        {
            SelectedListPr = obj.NewValue;
        }

        public void ClearPopUpVariableListPr()
        {
            Qsid_ID = 0;
            Qsid = string.Empty;
            ListFieldName = string.Empty;
            ListType = string.Empty;
            NotifyPropertyChanged("ListFieldName");
            NotifyPropertyChanged("ListType");
            NotifyPropertyChanged("Qsid");
            NotifyPropertyChanged("Qsid_ID");
            PrioritywithinCountry = 0;
            NotifyPropertyChanged("PrioritywithinCountry");
        }
        private void CommandButtonExecutedListPr(NotificationMessageAction<object> obj)
        {
            if (obj.Notification == "DeleteMapListPr")
            {
                var result = System.Windows.MessageBox.Show("Are you sure do you want to delete this List Priority?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    var dt = objCommon.DbaseQueryReturnTableSql("Select PhraseCategoryID from Library_PhraseCategories where phraseCategory in ('Compliance check list priority','Flavor list flag','FEMA list flag')", Win_Administrator_VM.CommonListConn);
                    List<string> lst = new List<string>();
                    dt.AsEnumerable().ToList().ForEach(x =>
                    {
                        lst.Add(x["PhraseCategoryID"].ToString());
                    });
                    var dtList = string.Join(",", lst);
                    objCommon.DbaseQueryReturnStringSQL("insert into Log_QSxxx_ListDictionary(SessionID,TimeStamp,Status,ListDictionaryID,PhraseID,PhraseCategoryID,QSID_ID,QSxxx_ListDictionary_ID)" +
                      " select " + Engine.CurrentUserSessionID + ",'" + objCommon.ESTTime() + "','D',1,PhraseId, PhraseCategoryID, Qsid_Id, QSxxx_ListDictionary_ID from Qsxxx_ListDictionary where qsid_id =" + SelectedListPr.QSIDID + " and PhraseCategoryID in (" + dtList + ")", Win_Administrator_VM.CommonListConn);

                    objCommon.DbaseQueryReturnStringSQL("Delete from Qsxxx_ListDictionary where Qsid_id =" + SelectedListPr.QSIDID + " and PhraseCategoryID in (" + dtList + ")", Win_Administrator_VM.CommonListConn);
                    BindListPr();
                    _notifier.ShowSuccess("Selected  List Priority deleted successfully");

                }
            }
            else if (obj.Notification == "AddEditNameListPr")
            {
                IsEditListPr = false;
                NotifyPropertyChanged("IsEditListPr");
                IsShowPopUpListPr = true;
                PopUpContentAddListPrVisibility = Visibility.Visible;
                NotifyPropertyChanged("PopUpContentAddListPrVisibility");
                PrioritywithinCountry =SelectedListPr.PrioritywithinCountry;
                NotifyPropertyChanged("PrioritywithinCountry");
                SelectedFema = listFema.Where(x => x.Value == SelectedListPr.FEMAlistflag).FirstOrDefault();
                SelectedFlavor = listFlavor.Where(x => x.Value == SelectedListPr.Flavorlistflag).FirstOrDefault();
            }
        }
        public int? PrioritywithinCountry { get; set; }
        private LibYesNo _SelectedFlavor { get; set; }
        public LibYesNo SelectedFlavor
        {
            get => _SelectedFlavor;
            set
            {
                if (Equals(_SelectedFlavor, value))
                {
                    return;
                }

                _SelectedFlavor = value;
                NotifyPropertyChanged("SelectedFlavor");
            }
        }
        private LibYesNo _SelectedFema { get; set; }
        public LibYesNo SelectedFema
        {
            get => _SelectedFema;
            set
            {
                if (Equals(_SelectedFema, value))
                {
                    return;
                }

                _SelectedFema = value;
                NotifyPropertyChanged("SelectedFema");
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnListPr()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSIDID", ColBindingName = "QSIDID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "QSID", ColBindingName = "QSID", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListFieldName", ColBindingName = "ListFieldName", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Country", ColBindingName = "Country", ColType = "Textbox" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListType", ColBindingName = "ListType", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PrioritywithinCountry", ColBindingName = "PrioritywithinCountry", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Flavorlistflag", ColBindingName = "Flavorlistflag", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FEMAlistflag", ColBindingName = "FEMAlistflag", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ShortName", ColBindingName = "ShortName", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ListPhrase", ColBindingName = "ListPhrase", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ShortDescription", ColBindingName = "ShortDescription", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapListPr" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "QSIDID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNameListPr" });

            return listColumnQsidDetail;
        }

    }
    public class MapListPr
    {
        public int QSIDID { get; set; }
        public string QSID { get; set; }
        public string ListFieldName { get; set; }
        public string Country { get; set; }
        public string ListType { get; set; }
        public int? PrioritywithinCountry { get; set; }
        public string Flavorlistflag { get; set; }
        public string FEMAlistflag { get; set; }
        public string ShortName { get; set; }
        public string ListPhrase { get; set; }
        public string ShortDescription { get; set; }
    }

    public class LibYesNo
    {
        public string Value { get; set; }
    }

}
