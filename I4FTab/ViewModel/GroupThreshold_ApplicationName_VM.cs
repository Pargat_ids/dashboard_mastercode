﻿using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRA_DataAccess.ViewModel;

namespace I4FTab.ViewModel
{
    public class GroupThreshold_ApplicationName_VM : Base_ViewModel
    {
        public int seqNumber { get; set; }
        int _chemicalNameID { get; set; }
        public int ChemicalNameID
        {
            get { return _chemicalNameID; }
            set
            {
                _chemicalNameID = value;
                NotifyPropertyChanged("ChemicalNameID");
            }
        }
        int _qsid_ID { get; set; }
        public int Qsid_ID
        {
            get { return _qsid_ID; }
            set
            {
                _qsid_ID = value;
                NotifyPropertyChanged("Qsid_ID");
            }
        }
        public EntityClass.Library_QSIDs _selectedQsid { get; set; }
        public EntityClass.Library_QSIDs SelectedQsid
        {
            get { return _selectedQsid; }
            set
            {
                if (_selectedQsid != value)
                {
                    _selectedQsid = value;
                    BindApplicationName();
                    NotifyPropertyChanged("SelectedQsid");
                }
            }
        }
         int _applicationNameID { get; set; }
        public int ApplicationNameID
        {
            get { return _applicationNameID; }
            set
            {
                _applicationNameID = value;
                if (listApplicationName.Any(y => y.PhraseID == _applicationNameID))
                {
                    this.SelectedApplicationName = listApplicationName.Where(y => y.PhraseID == _applicationNameID).FirstOrDefault();
                }
            }
        }
        Library_Phrases _selectedApplicationName { get; set; }
        public Library_Phrases SelectedApplicationName
        {
            get { return _selectedApplicationName; }
            set
            {
                _selectedApplicationName = value;
                NotifyPropertyChanged("SelectedApplicationName");
            }
        }
        Library_Operators _selectedMinOperator { get; set; }
        public Library_Operators SelectedMinOperator
        {
            get { return _selectedMinOperator; }
            set
            {
                _selectedMinOperator = value;
                NotifyPropertyChanged("SelectedMinOperator");

            }
        }
        double? _minValue { get; set; } = 0;
        public double? minValue
        {
            get { return _minValue; }
            set
            {
                _minValue = value;
                NotifyPropertyChanged("minValue");
            }
        }
        Library_Operators _selectedMaxOperator { get; set; }
        public Library_Operators SelectedMaxOperator
        {
            get { return _selectedMaxOperator; }
            set
            {
                _selectedMaxOperator = value;
                NotifyPropertyChanged("SelectedMaxOperator");

            }
        }
        double? _maxValue { get; set; } = 0;
        public double? maxValue
        {
            get { return _maxValue; }
            set
            {
                _maxValue = value;
                NotifyPropertyChanged("maxValue");
            }
        }
        LibraryUnits_VM _selectedUnit { get; set; }
        public LibraryUnits_VM SelectedUnit
        {
            get { return _selectedUnit; }
            set
            {
                _selectedUnit = value;
                NotifyPropertyChanged("SelectedUnit");
            }
        }
        public ObservableCollection<Library_Phrases> listApplicationName { get; set; } = new ObservableCollection<Library_Phrases>();
        public void BindApplicationName()
        {
            if (SelectedQsid != null)
            {
                listApplicationName = new ObservableCollection<Library_Phrases>(_objLibraryFunction_Service.GetPhraseApplicationName(SelectedQsid.QSID_ID));
                NotifyPropertyChanged("listApplicationName");
            }
        }
    }
}
