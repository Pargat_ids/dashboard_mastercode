﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I4FTab.Common
{
    public static class CommonGeneralFunction
    {
        public static DataTable ImportBulkExcel(string path)
        {
            DataTable dt = new DataTable();

            using (DocumentFormat.OpenXml.Packaging.SpreadsheetDocument spreadSheetDocument = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Open(path, false))
            {

                DocumentFormat.OpenXml.Packaging.WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<DocumentFormat.OpenXml.Spreadsheet.Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>();
                string relationshipId = sheets.First().Id.Value;
                DocumentFormat.OpenXml.Packaging.WorksheetPart worksheetPart = (DocumentFormat.OpenXml.Packaging.WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                DocumentFormat.OpenXml.Spreadsheet.Worksheet workSheet = worksheetPart.Worksheet;
                DocumentFormat.OpenXml.Spreadsheet.SheetData sheetData = workSheet.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                List<string> columnRef = new List<string>();
                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                    columnRef.Add(cell.CellReference.ToString().Substring(0, cell.CellReference.ToString().Length - 1));
                }

                foreach (Row row in rows)
                {
                    DataRow tempRow = dt.NewRow();
                    int i = 0;
                    foreach (Cell cell in row.Descendants<Cell>())
                    {
                        while (columnRef[i] + (dt.Rows.Count + 1) != cell.CellReference)
                        {
                            tempRow[i] = "";
                            i += 1;
                        }

                        tempRow[i] = GetCellValue(spreadSheetDocument, cell);
                        i += 1;
                    }
                    //for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                    //{
                    //    tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                    //}

                    dt.Rows.Add(tempRow);
                }

            }
            dt.Rows.RemoveAt(0);
            return dt;
        }
        public static string GetCellValue(DocumentFormat.OpenXml.Packaging.SpreadsheetDocument document, Cell cell)
        {
            DocumentFormat.OpenXml.Packaging.SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = string.Empty;
            if (cell.CellValue != null)
            {
                value = cell.CellValue.InnerXml;
            }
            else if (!string.IsNullOrEmpty(cell.InnerText))
            {
                value = cell.InnerText;
            }

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

    }
}
