﻿using System.Windows.Controls;

namespace CurrentDataView.CommonControlForGenericType
{
    /// <summary>
    /// Interaction logic for CommonDataGrid_Generic.xaml
    /// </summary>
    public partial class CommonDataGrid_Generic : UserControl
    {
        public CommonDataGrid_Generic()
        {
            InitializeComponent();
        }
    }
}
