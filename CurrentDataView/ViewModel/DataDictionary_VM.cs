﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class DataDictionary_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listDataDictionary { get; set; }
        public StandardDataGrid_VM listDataDictionaryHistory { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderDataDictionary { get; set; }
        public DataDictionary_VM()
        {

            Task.Run(() => RunDataDictionary());
        }
        private void RunDataDictionary()
        {
            VisibleLoderDataDictionary = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderDataDictionary");
            if (Win_Administrator_VM.DataDictionaryTable.Columns.Count == 0)
            {
                Win_Administrator_VM.DataDictionaryTable = rowIdentifier.GetBindDataDictionary(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            if (Win_Administrator_VM.DataDicHistory.Columns.Count == 0)
            {
                Win_Administrator_VM.DataDicHistory = rowIdentifier.GetDataDictionaryHistory(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
           
            listDataDictionary = new StandardDataGrid_VM("Data Dictionary", Win_Administrator_VM.DataDictionaryTable, Visibility.Visible);
            NotifyPropertyChanged("listDataDictionary");
            listDataDictionaryHistory = new StandardDataGrid_VM("Data Dictionary History", Win_Administrator_VM.DataDicHistory, Visibility.Visible);
            NotifyPropertyChanged("listDataDictionaryHistory");
            VisibleLoderDataDictionary = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderDataDictionary");
        }
    }
}
