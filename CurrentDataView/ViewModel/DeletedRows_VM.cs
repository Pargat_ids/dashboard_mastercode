﻿using System.Threading.Tasks;
using System.Windows;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class DeletedRows_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listDeletedRows { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderDeletedRows { get; set; }
        public DeletedRows_VM()
        {
            Task.Run(() => RunDeletedRows());
        }
        private void RunDeletedRows()
        {
            VisibleLoderDeletedRows = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderDeletedRows");
            if (Win_Administrator_VM.DataDeletedRows.Columns.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                Win_Administrator_VM.DataDeletedRows = rowIdentifier.MakeOldDataDeletedRows(Win_Administrator_VM.Qsid_ID, false);
                Win_Administrator_VM.DataDeletedRows = rowIdentifier.AddExtraFieldInCurrentDataDeletedRow(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.DataDeletedRows);
            }
            listDeletedRows = new StandardDataGrid_VM("Deleted Rows", Win_Administrator_VM.DataDeletedRows, Visibility.Visible);
            NotifyPropertyChanged("listDeletedRows");
            VisibleLoderDeletedRows = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleLoderDeletedRows");
        }
    }
}
