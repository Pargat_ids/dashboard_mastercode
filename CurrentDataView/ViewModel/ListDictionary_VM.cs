﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class ListDictionary_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listListDictionary { get; set; }
        public StandardDataGrid_VM listListDictionaryHis { get; set; }
        private RowIdentifier objDataDictionary = new RowIdentifier();
        public Visibility VisibleLoderListDictionary { get; set; }
        public ListDictionary_VM()
        {
            Task.Run(() => RunListDictionary());
        }
        private void RunListDictionary()
        {
            VisibleLoderListDictionary = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderListDictionary");
            if (Win_Administrator_VM.ListDictionaryTable.Columns.Count == 0)
            {
                Win_Administrator_VM.ListDictionaryTable = objDataDictionary.GetListDictionaryData(Win_Administrator_VM.ListDictionaryTable, Win_Administrator_VM.Qsid_ID);
            }
            if (Win_Administrator_VM.ListDicHistory.Columns.Count == 0)
            {
                Win_Administrator_VM.ListDicHistory = objDataDictionary.GetListDictionaryHistory(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            if (Win_Administrator_VM.ListDictionaryTable.Rows.Count > 0)
            {
                listListDictionary = new StandardDataGrid_VM("List Dictionary", Win_Administrator_VM.ListDictionaryTable, Visibility.Visible);
                NotifyPropertyChanged("listListDictionary");
            }
            if (Win_Administrator_VM.ListDicHistory.Rows.Count > 0)
            {
                listListDictionaryHis = new StandardDataGrid_VM("List Dictionary History", Win_Administrator_VM.ListDicHistory, Visibility.Visible);
                NotifyPropertyChanged("listListDictionaryHis");
            }
            VisibleLoderListDictionary = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderListDictionary");
        }
    }
}
