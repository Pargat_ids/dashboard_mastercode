﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class CharacterDetail_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listCharacterDetail { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderCharacterDetail { get; set; }
        public CharacterDetail_VM()
        {
            RunCharacterDetail();
        }
        private void RunCharacterDetail()
        {
            //if (Win_Administrator_VM.CharacterTable.Columns.Count == 0)
            //{
            VisibleLoderCharacterDetail = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderCharacterDetail");
            if (Win_Administrator_VM.CurrentDataTable == null)
            {
                CurrentData_VM.SingleCurrentViewFunction();
            }
            Task.Run(() =>
            {
                if (Win_Administrator_VM.NotesTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.NotesTable = rowIdentifier.GetBindNotes(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                }

                if (Win_Administrator_VM.DataDictionaryTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.DataDictionaryTable = rowIdentifier.GetBindDataDictionary(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                }

                if (Win_Administrator_VM.ListDictionaryTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.ListDictionaryTable = rowIdentifier.GetListDictionaryData(Win_Administrator_VM.ListDictionaryTable, Win_Administrator_VM.Qsid_ID);
                }

                Win_Administrator_VM.CharacterTable = rowIdentifier.GetCharacterDetails(Win_Administrator_VM.CurrentDataTable, Win_Administrator_VM.NotesTable, Win_Administrator_VM.DataDictionaryTable, Win_Administrator_VM.ListDictionaryTable, 4);
               
                listCharacterDetail = new StandardDataGrid_VM("Character Detail", Win_Administrator_VM.CharacterTable, Visibility.Visible);
                NotifyPropertyChanged("listCharacterDetail");
                VisibleLoderCharacterDetail = Visibility.Hidden;
                NotifyPropertyChanged("VisibleLoderCharacterDetail");
            });
            //}
        }
    }
}
