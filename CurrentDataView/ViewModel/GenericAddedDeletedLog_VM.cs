﻿using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using VersionControlSystem.Model.ViewModel;

namespace CurrentDataView.ViewModel
{
    public class GenericAddedDeletedLog_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<IGenericAddedDeleted> comonGenericAddedDeleted { get; set; }
        public Visibility VisibleLoderGenericAddedDeleted { get; set; }
        public GenericAddedDeletedLog_VM()
        {
            comonGenericAddedDeleted = new CommonDataGrid_ViewModel<IGenericAddedDeleted>(GetListGridColumnListPr(), "Disapprove", "Disapprove");
            RunGenericAddedDeleted();
        }
        private ObservableCollection<IGenericAddedDeleted> _lstGenericAddedDeleted { get; set; } = new ObservableCollection<IGenericAddedDeleted>();
        public ObservableCollection<IGenericAddedDeleted> lstGenericAddedDeleted
        {
            get { return _lstGenericAddedDeleted; }
            set
            {
                if (_lstGenericAddedDeleted != value)
                {
                    _lstGenericAddedDeleted = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IGenericAddedDeleted>>>(new PropertyChangedMessage<List<IGenericAddedDeleted>>(null, _lstGenericAddedDeleted.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnListPr()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCAS", ColBindingName = "ParentCAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCAS", ColBindingName = "ChildCAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Status", ColBindingName = "Status", ColType = "Textbox", ColumnWidth = "100" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "UserName", ColBindingName = "UserName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "TimeStamp", ColBindingName = "TimeStamp", ColType = "Textbox", ColumnWidth = "200" });
            return listColumnQsidDetail;
        }
        private void RunGenericAddedDeleted()
        {
            VisibleLoderGenericAddedDeleted = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderGenericAddedDeleted");

            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {

                    var getGenericAddedDeleted = context.Log_Generics_on_List_Adds_Deletes.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID).ToList();
                    var joinParentCas = (from d1 in getGenericAddedDeleted
                                         join d2 in context.Library_CAS.AsNoTracking()
                                         on d1.ParentCASID equals d2.CASID
                                         select new { d1, ParentCas = d2.CAS }).ToList();
                    var joinChildCas = (from d1 in joinParentCas
                                        join d2 in context.Library_CAS.AsNoTracking()
                                         on d1.d1.ChildCASID equals d2.CASID
                                        select new IGenericAddedDeleted { ParentCASID = d1.d1.ParentCASID, ParentCAS = d1.ParentCas, ChildCASID = d1.d1.ChildCASID, ChildCAS = d2.CAS, SessionID = d1.d1.SessionID, Status = d1.d1.Status }).Distinct().ToList();

                    var joinwithSession = (from d1 in joinChildCas
                                        join d2 in context.Log_Sessions.AsNoTracking()
                                         on d1.SessionID equals d2.SessionID
                                        select new IGenericAddedDeleted { ParentCASID = d1.ParentCASID, ParentCAS = d1.ParentCAS, ChildCASID = d1.ChildCASID, ChildCAS = d1.ChildCAS, SessionID = d1.SessionID, Status = d1.Status, UserName = d2.UserName, TimeStamp  = d2.SessionBegin_TimeStamp }).Distinct().ToList();
                    var joinwithUser = (from d1 in joinChildCas
                                           join d2 in context.Library_Users.AsNoTracking()
                                            on d1.UserName equals d2.VeriskID.ToUpper()
                                           select new IGenericAddedDeleted { ParentCASID = d1.ParentCASID, ParentCAS = d1.ParentCAS, ChildCASID = d1.ChildCASID, ChildCAS = d1.ChildCAS, SessionID = d1.SessionID, Status = d1.Status, UserName = d2.FirstName + " " + d2.LastName, TimeStamp = d1.TimeStamp }).Distinct().ToList();

                    lstGenericAddedDeleted = new ObservableCollection<IGenericAddedDeleted>(joinwithUser);
                    NotifyPropertyChanged("comonGenericAddedDeleted");
                    VisibleLoderGenericAddedDeleted = Visibility.Hidden;
                    NotifyPropertyChanged("VisibleLoderGenericAddedDeleted");
                }
            });
            
        }
    }

    public class IGenericAddedDeleted
    {
        public Int32 ParentCASID { get; set; }
        public Int32 ChildCASID { get; set; }
        public string ParentCAS { get; set; }
        public string ChildCAS { get; set; }
        public string Status { get; set; }
        public Int32 SessionID { get; set; }

        public string UserName { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
