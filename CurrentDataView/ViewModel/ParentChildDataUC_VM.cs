﻿using CurrentDataView.CustomUserControl;
using CurrentDataView.Library;
using DataGridFilterLibrary;
using DataGridFilterLibrary.Querying;
using EntityClass;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using ToastNotifications.Messages;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class ParentChildDataUC_VM : Base_ViewModel
    {
        public TabItem selectedTabItem { get; set; }
        private DataTable dtDefault;
        private CommonFunctions objCommon;
        public ICommand CommmandChangeVisiblityUniqueListCol { get; set; }
        public Visibility UniquelistAllColVisible { get; set; } = Visibility.Collapsed;
        public ObservableCollection<iColumnName> listMdbColumns { get; set; }
        private void CommmandChangeVisiblityUniqueListColExecute(object obj)
        {
            if (UniquelistAllColVisible == Visibility.Collapsed)
            {
                UniquelistAllColVisible = Visibility.Visible;
            }
            else
            {
                UniquelistAllColVisible = Visibility.Collapsed;
            }
            NotifyPropertyChanged("UniquelistAllColVisible");
        }
        private iColumnName _SelectedColumn { get; set; }

        public iColumnName SelectedColumn
        {
            get => _SelectedColumn;
            set
            {
                if (Equals(_SelectedColumn, value))
                {
                    return;
                }

                _SelectedColumn = value;
                ShowUniqueValuesExecute();
            }
        }
        private void ShowUniqueValuesExecute()
        {
            if (dtDefault.Rows.Count > 0)
            {
                string delmt = string.Empty;
                using (var context = new CRAModel())
                {
                    var fieldid = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == SelectedColumn.ColumnName).Select(y => y.FieldNameID).FirstOrDefault();
                    var mapDel = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.FieldNameID == fieldid).Select(y => new { y.ChemicalNameDelimiterID, y.MultiValueIdentifierDelimiterID, y.MultiValuePhraseDelimiterID, y.RangeDelimiterID, y.UnitDelimiterID }).FirstOrDefault();
                    if (mapDel != null)
                    {
                        delmt = context.Library_Delimiters.AsNoTracking().Where(x => x.DelimiterID == mapDel.UnitDelimiterID || x.DelimiterID == mapDel.RangeDelimiterID || x.DelimiterID == mapDel.MultiValuePhraseDelimiterID || x.DelimiterID == mapDel.MultiValueIdentifierDelimiterID || x.DelimiterID == mapDel.ChemicalNameDelimiterID).Select(z => z.Delimiter).FirstOrDefault();
                    }
                }
                DataTable columnSelected = new DataTable();
                if (string.IsNullOrEmpty(delmt))
                {
                    columnSelected = dtDefault.DefaultView.ToTable(true, SelectedColumn.ColumnName);
                }
                else
                {
                    columnSelected.Columns.Add(SelectedColumn.ColumnName);
                    var dt = dtDefault.DefaultView.ToTable(true, SelectedColumn.ColumnName);
                    dt.AsEnumerable().ToList().ForEach(x =>
                    {
                        var splitVal = x.ItemArray[0].ToString().Split(Convert.ToChar(delmt));
                        for (int i = 0; i < splitVal.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(splitVal[i].Trim()))
                            {
                                if (columnSelected.AsEnumerable().Count(xx => xx.Field<string>(SelectedColumn.ColumnName) == splitVal[i].Trim()) == 0)
                                {
                                    columnSelected.Rows.Add(splitVal[i].Trim());
                                }
                            }
                        }
                    });
                }
                var objPopup = new ChildPopup_VM(columnSelected);
                ChildPopUp objPopWindow = new ChildPopUp();
                objPopWindow.DataContext = objPopup;
                objPopWindow.ShowDialog();
            }
            else
            {
                _notifier.ShowError("Show Data in Grid First");
            }

        }
        private RowIdentifier objCurrentView = new RowIdentifier();
        private ObservableCollection<DataGridColumn> _columnCollection = new ObservableCollection<DataGridColumn>();
        private List<ListCheckBoxType_ViewModel> listCheckBox = new List<ListCheckBoxType_ViewModel>();
        public IList myList { get; set; }
        public DataColumnCollection defaultDataTableColumns { get; set; }
        public Type listType { get; set; }
        public IList myListTotal { get; set; }
        public IList _objSelectedItem { get; set; }
        public IList myListDataTable { get; set; }
        public Action<DataRow> GenericCallbackAction { get; set; }
        public ObservableCollection<selectedListItemCombo> ListComboxItem { get; set; } = new ObservableCollection<selectedListItemCombo>();
        public Visibility VisibleDataList { get; set; }
        public Visibility VisibleLoder { get; set; }
        public string cas { get; set; }
        public DataGrid_Generic_VM<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM> commonDG_GenericFlat_VM { get; set; }
        public DataGrid_Generic_VM<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM> commonDG_GenericCombined_VM { get; set; }

        public IList objSelectedItem
        {
            get { return _objSelectedItem; }
            set
            {
                if (_objSelectedItem != value)
                {
                    _objSelectedItem = value;
                    //if (GenericCallbackAction != null)
                    //{
                    //    //GenericCallbackAction.Invoke(_objSelectedItem);
                    //}
                }
            }
        }
        private ChildPopup_VM objPopup { get; set; }
        public string PaginationTitle { get; set; }
        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get
            {
                return this._columnCollection;
            }
            set
            {
                _columnCollection = value;
                NotifyPropertyChanged("ColumnCollection");
            }
        }
        public ICommand CommandButton { get; set; }
        public List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM> _listFlatGenerics { get; set; }
        public List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM> ListFlatGenerics
        {
            get { return _listFlatGenerics; }
            set
            {
                if (_listFlatGenerics != value)
                {
                    _listFlatGenerics = value;
                    MessengerInstance.Send<GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>>>(new GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>>(null, _listFlatGenerics, "Default List"));
                }
            }
        }
        public List<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM> _listCombinedGenerics { get; set; }
        public List<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM> ListCombinedGenerics
        {
            get { return _listCombinedGenerics; }
            set
            {
                if (_listCombinedGenerics != value)
                {
                    _listCombinedGenerics = value;
                    MessengerInstance.Send<GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM>>>(new GalaSoft.MvvmLight.Messaging.PropertyChangedMessage<List<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM>>(null, _listCombinedGenerics, "Default List"));
                }
            }
        }
        public ObservableCollection<Library_SubstanceType> listSubstanceType { get; set; }
        private Library_SubstanceType _selectedSubstanceType { get; set; }

        public Library_SubstanceType selectedSubstanceType
        {
            get { return _selectedSubstanceType; }
            set
            {
                _selectedSubstanceType = value;
                NotifyPropertyChanged("selectedSubstanceType");
            }
        }
        public ObservableCollection<Library_SubstanceCategories> listSubstanceCategory { get; set; }
        public Library_SubstanceCategories _selectedSubstanceCategory { get; set; }
        public Library_SubstanceCategories SelectedSubstanceCategory
        {
            get { return _selectedSubstanceCategory; }
            set
            {
                _selectedSubstanceCategory = value;
                NotifyPropertyChanged("SelectedSubstanceCategory");
            }
        }
        public bool IsShowPopUp { get; set; } = false;
        public ICommand CommandUpdateSubstanceType { get; set; }
        public ICommand CommandCopyCellDataToClipboard { get; set; }
        public ICommand CommandHidePopUp { get; set; }
        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public ICommand CommmandChangeVisiblityListColNull { get; set; }
        public int SelectedTabIndex { get; set; }
        public Visibility listAllColVisible { get; set; } = Visibility.Collapsed;
        public bool listAllColVisibleNull { get; set; } = false;
        public ObservableCollection<ColumnSelectionDataGrid> ListAllColumnDGNull { get; set; } = new ObservableCollection<ColumnSelectionDataGrid>();
        public ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM> ListAllColumnDG { get; set; } = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
        private List<string> queryNull { get; set; } = new List<string>();
        private string queryStringForNUll { get; set; }
        public ICommand CommandOnColSelectedChange { get; set; }
        public ICommand CommandOnColSelectedChangeNull { get; set; }
        public ICommand CommandRefreshItemDetailGrid { get; set; }
        public ICommand CommandGoToTreeView { get; set;  }
        public bool IsEnableGotoTreeView { get; set; }
        public PopUp_GenericDetail objPopUp { get; set; }
        
        public ParentChildDataUC_VM(string selectedTestCase, DataTable defaultDataTable)
        {
            //CustomQueryController = new QueryController();
            CustomQueryController.FilteringStarted -= CustomQueryController_FilteringStarted;
            CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;
            // _objLibraryFunction_Service.TestLibraryPhrase("Fruit ices");
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                commonDG_GenericFlat_VM = new DataGrid_Generic_VM<VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM>(GetListGridColumn_Flat(), "", "Flat Generic List:");
                NotifyPropertyChanged("commonDG_GenericFlat_VM");
                commonDG_GenericCombined_VM = new DataGrid_Generic_VM<VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM>(GetListGridColumn_Flat(true), "", "Combined Generic List:");
                NotifyPropertyChanged("commonDG_GenericCombined_VM");
                objPopUp = new PopUp_GenericDetail();
                objPopUp.DataContext = this;
            });
            listCheckBox = new List<ListCheckBoxType_ViewModel>();
            //listCheckBox.Add(new ListCheckBox_ViewModel { Name = (bool?)null });
            listCheckBox.Add(new ListCheckBoxType_ViewModel { ID = 1, Value = true });
            listCheckBox.Add(new ListCheckBoxType_ViewModel { ID = 0, Value = false });
            GenericCallbackAction = null;
            defaultDataTableColumns = defaultDataTable.Columns;
            commandFirst = new RelayCommand(CommandFirstExecute, CommandFirstCanExecute);
            commandPrev = new RelayCommand(CommandPrevExecute, CommandFirstCanExecute);
            commandNext = new RelayCommand(CommandNextExecute, CommandFirstCanExecute);
            commandLast = new RelayCommand(CommandLastExecute, CommandFirstCanExecute);
            ClearAllFilter = new RelayCommand(CommandClearAllFilter, CommandFirstCanExecute);
            ParentChildGenerateAccessFile = new RelayCommand(CommandAccessExportParentChild, CommandFirstCanExecute);
            ParentChildGenerateExcelFile = new RelayCommand(CommandExcelExportParentChild, CommandFirstCanExecute);
            CommmandChangeVisiblityUniqueListCol = new RelayCommand(CommmandChangeVisiblityUniqueListColExecute, CommandFirstCanExecute);
            VisibleLoder = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoder");
            VisibleDataList = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleDataList");
            dtDefault = defaultDataTable;
            Init(selectedTestCase, defaultDataTable);
            PaginationTitle = "selectedTestCase";
            CommandButton = new RelayCommand(CommmandButtonExecute);
            CommandUpdateSubstanceType = new RelayCommand(CommandUpdateSubstanceTypeExecute);
            CommandHidePopUp = new RelayCommand(CommandHidePopUpExecute);
            CommandRefreshItemDetailGrid = new RelayCommand(CommandRefreshItemDetailGridExecute);
            CommandGoToTreeView = new RelayCommand(CommandGoToTreeViewExecute);
            
            BindSubstanceType();
            BindSubstanceCategories();
        }

        private void CommandGoToTreeViewExecute(object obj)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("",cas, "ActiveGenericTab"), typeof(ParentChildDataUC_VM));
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>("", cas, "SearchCasInTreeFromCurrentDataView"), typeof(GenericManagementTool.ViewModels.Generics_VM));
        }

        private void CommandRefreshItemDetailGridExecute(object obj)
        {
            MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>( "RefreshGridParentChildData", "RefreshGridParentChildData", ""), typeof(ParentChildData_VM));
        }

        public void CommandHidePopUpExecute(object obj)
        {
            IsShowPopUp = false;
            NotifyPropertyChanged("IsShowPopUp");
        }
        public void CommandUpdateSubstanceTypeExecute(object obj)
        {
            var library_Cas = _objLibraryFunction_Service.GetLibraryCasByCas(cas);
            if ((string)obj == "Type")
            {               
                if (library_Cas.CASID != 0 && selectedSubstanceType != null)
                {
                    _objLibraryFunction_Service.InsertUpdateSubstanceType(selectedSubstanceType.SubstanceTypeID, library_Cas.CASID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
                    _notifier.ShowSuccess("Update Substance Type Successfully");
                }
            }
            else if ((string)obj == "Cate") {
                if (library_Cas.CASID != 0 && SelectedSubstanceCategory != null)
                {
                    _objLibraryFunction_Service.InsertUpdateSubstanceCategory(SelectedSubstanceCategory.SubstanceCategoryID, library_Cas.CASID, VersionControlSystem.Model.ApplicationEngine.Engine.CurrentUserSessionID);
                    _notifier.ShowSuccess("Update Substance Type Successfully");
                }
            }
        }
        public void BindSubstanceType()
        {
            listSubstanceType = new ObservableCollection<Library_SubstanceType>(_objLibraryFunction_Service.GetAllSubstanceType());
            NotifyPropertyChanged("listSubstanceType");
        }
        public void BindSubstanceCategories()
        {
            listSubstanceCategory = new ObservableCollection<Library_SubstanceCategories>(_objLibraryFunction_Service.GetAllSubstanceCategories());
            NotifyPropertyChanged("listSubstanceCategory");
        }
        public void Init(string selectedTestCase, DataTable defaultDataTable)
        {
            ExecuteClearFilter();
            objCommon = new CommonFunctions();
            BindListAllColumn();            
            var list = _objLibraryFunction_Service.BindListFromTable(defaultDataTable, listType);
            Task.Run(() => ExecuteFunction(list));
            
            CommandCopyCellDataToClipboard = new RelayCommand(CommandCopyCellDataToClipboardExecute);
            CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute);
            CommmandChangeVisiblityListColNull = new RelayCommand(CommmandChangeVisiblityListColNullExecute);
            CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute);
            CommandOnColSelectedChangeNull = new RelayCommand(CommandOnColSelectedChangeNullExecute);
        }
        private List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> GetListGridColumn_Flat(bool IsCombine=false)
        {
            List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn> listColumnFlatGenerics = new List<VersionControlSystem.Model.ViewModel.CommonDataGridColumn>();
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Parent CAS", ColBindingName = "ParentCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Child CAS", ColBindingName = "ChildCas", ColType = "Textbox" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "200" });
            if (IsCombine)
            {
                listColumnFlatGenerics.Add(new VersionControlSystem.Model.ViewModel.CommonDataGridColumn { IsSelected = true, ColName = "Type", ColBindingName = "Type", ColType = "Textbox", ColumnWidth = "150" });
            }
            return listColumnFlatGenerics;
        }
        
        public void CommmandButtonExecute(object obj)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                string flag = (string)obj;
                if (flag == "FlatGenericsChildCount")
                {
                    SelectedTabIndex = 0;
                }
                else if (flag == "CombinedGenericsCount")
                {
                    SelectedTabIndex = 1;
                }
                else if (flag == "SubstanceType")
                {
                    SelectedTabIndex = 2;
                }
                else
                {
                    SelectedTabIndex = 3;
                }
                
                BindFlatGenerics();
                BindCombinedGenerics();
                NotifyPropertyChanged("SelectedTabIndex");                
                objPopUp.Show();
                objPopUp.Focus();              
            });
        }
        public void BindFlatGenerics()
        {
            ListFlatGenerics = _objLibraryFunction_Service.GetAllFlatGenerics(cas, "ParentCasOnly").Select(x => new VersionControlSystem.Model.ViewModel.FlatGeneric_CurrentData_VM
            {
                ParentCas = x.ParentCas,
                ChildCas = x.ChildCas,
                ParentChemicalName = x.ParentChemicalName,
                ChemicalName = x.ChemicalName

            }).ToList();
        }
        public void BindCombinedGenerics()
        {            
            ListCombinedGenerics = _objLibraryFunction_Service.GetAllCombinedGenerics(cas, "OnlyChildCas").Select(x => new VersionControlSystem.Model.ViewModel.CombinedGeneric_CurrentData_VM
            {
                ParentCas = x.ParentCas,
                ChildCas = x.ChildCas,
                ParentChemicalName = x.ParentChemicalName,
                ChemicalName = x.ChemicalName,
                Type=x.Type
            }).ToList();
        }
        private void ExecuteFunction(IList lstData)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                myList = lstData;
                var lst = _objLibraryFunction_Service.BindListFromIQuerable(myList.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                myListTotal = (IList)lst;
                if (myList.Count > 0)
                {
                    BindDataGridCas_ComparedByQSID();
                }

                selectedItemCombo = new selectedListItemCombo() { id = 25, text = "25" };
                NotifyPropertyChanged("selectedItemCombo");
                ListComboxItem = new ObservableCollection<selectedListItemCombo>(AddList());
                NotifyPropertyChanged("ListComboxItem");
                VisibleLoder = Visibility.Collapsed;
                NotifyPropertyChanged("VisibleLoder");
                VisibleDataList = Visibility.Visible;
                NotifyPropertyChanged("VisibleDataList");
                CommandOnColSelectedChange.Execute("CasID");            });
        }
        public void BindDataGridCas_ComparedByQSID()
        {
            foreach (DataColumn dtCol in defaultDataTableColumns)
            {               
                System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(dtCol.ColumnName);
                if (dtCol.ColumnName == "FlatGenericsChildCount" || dtCol.ColumnName == "CombinedGenericsCount" || dtCol.ColumnName == "SubstanceType" || dtCol.ColumnName == "SubstanceCat")
                {

                    string className = string.Empty;
                    switch (dtCol.ColumnName)
                    {
                        case "FlatGenericsChildCount":
                            className = "CustomFlatGenericChild";
                            break;
                        case "CombinedGenericsCount":
                            className = "CustomCombinedGenericChild";
                            break;
                        case "SubstanceType":
                            className = "CustomSubstanceTypeBtn";
                            break;
                        case "SubstanceCat":
                            className = "CustomSubstanceCateBtn";
                            break;
                    }
                    var templatecol = CreateDataGridButtonColumn(dtCol.ColumnName, className);

                    ColumnCollection.Add(templatecol);
                }
                else if (dtCol.DataType.Name != "Boolean")
                {
                    DataGridTextColumn colText = new DataGridTextColumn();
                    colText.Header = dtCol.ColumnName;
                    colText.Binding = bindings;
                    Type typePro = dtCol.DataType;
                    if (typePro == typeof(int) || typePro == typeof(decimal))
                    {
                        DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, false);
                    }
                    if (typePro == typeof(string))
                    {
                        DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, true);
                        DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colText, false);
                    }
                    if (typePro == typeof(DateTime))
                    {
                        DataGridFilterLibrary.DataGridColumnExtensions.SetIsBetweenFilterControl(colText, true);
                    }

                    var style = new Style(typeof(TextBlock));
                    style.Setters.Add(new Setter(TextBlock.TextWrappingProperty, TextWrapping.WrapWithOverflow));
                    style.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
                    colText.ElementStyle = style;
                    ColumnCollection.Add(colText);
                }
                else
                {
                    DataGridCheckBoxColumn colText = new DataGridCheckBoxColumn();
                    colText.Header = dtCol.ColumnName;
                    colText.Binding = bindings;
                    colText.IsReadOnly = false;
                    colText.IsThreeState = false;
                    var style = new Style(typeof(System.Windows.Controls.CheckBox));
                    style.Setters.Add(new Setter(System.Windows.Controls.DataGridCell.IsEditingProperty, true));
                    //style.Setters.Add(new Setter(System.Windows.Controls.DataGridCell.BackgroundProperty, Brushes.White));
                    colText.ElementStyle = style;
                    colText.Width = 80;
                    ColumnCollection.Add(colText);
                }
            }

        }
        public DataGridTemplateColumn CreateDataGridButtonColumn(string columnName, string customClass)
        {

            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(columnName);
            DataGridTemplateColumn colButton = new DataGridTemplateColumn();
            colButton.Header = columnName;

            FrameworkElementFactory elementBtn = new FrameworkElementFactory(typeof(System.Windows.Controls.Button));
            elementBtn.SetValue(System.Windows.Controls.Button.ContentProperty, bindings);
            elementBtn.SetValue(System.Windows.Controls.Button.CommandProperty, CommandButton);
            elementBtn.SetValue(System.Windows.Controls.Button.CommandParameterProperty, columnName);
            

            Style eleStyle = (Style)System.Windows.Application.Current.Resources[customClass];
            elementBtn.SetValue(System.Windows.Controls.Button.StyleProperty, eleStyle);

            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementBtn;
            colButton.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            DataGridFilterLibrary.DataGridColumnExtensions.SetFilterMemberPathProperty(colButton, columnName);
            colButton.SortMemberPath = columnName;
            // DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(colButton, true);

            return colButton;
        }
        private void CommandCopyCellDataToClipboardExecute(object obj)
        {
            try
            {
                if (_currentCellData.Column != null)
                {
                    if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                    {
                        var text = string.Empty;
                        text = ((System.Windows.Controls.TextBlock)_currentCellData.Column.GetCellContent(_currentCellData.Item)).Text.ToString();
                        System.Windows.Clipboard.SetDataObject(text);
                    }
                }
            }
            catch { }
        }
        #region Singlecell Copyin Clipboard
        private DataGridCellInfo _currentCellData { get; set; }
        public DataGridCellInfo CurrentCellData
        {
            get { return _currentCellData; }
            set
            {
                if (value != _currentCellData)
                {
                    _currentCellData = value;
                    if (_currentCellData.Column != null)
                    {
                        cas = value.Item.GetType().GetProperty("CAS").GetValue(value.Item).ToString();
                        NotifyPropertyChanged("cas");
                        var substanceType = string.Empty;
                        var substanceCat = string.Empty;
                        if (value.Item.GetType().GetProperty("SubstanceType") != null && value.Item.GetType().GetProperty("SubstanceType").GetValue(value.Item) != null)
                        {
                             substanceType = value.Item.GetType().GetProperty("SubstanceType").GetValue(value.Item).ToString();
                            selectedSubstanceType = listSubstanceType.Where(x => x.SubstanceTypeName == substanceType).FirstOrDefault();
                        }
                        if (value.Item.GetType().GetProperty("SubstanceCat") != null && value.Item.GetType().GetProperty("SubstanceCat").GetValue(value.Item) != null)
                        {
                             substanceCat = value.Item.GetType().GetProperty("SubstanceCat").GetValue(value.Item).ToString();
                            SelectedSubstanceCategory = listSubstanceCategory.Where(x => x.SubstanceCategoryName == substanceCat).FirstOrDefault();
                        }
                        if (value.Item.GetType().GetProperty("FlatGenericsChildCount") != null && value.Item.GetType().GetProperty("FlatGenericsChildCount").GetValue(value.Item) != null)
                        {
                            int flatGenericCount =Convert.ToInt32( value.Item.GetType().GetProperty("FlatGenericsChildCount").GetValue(value.Item).ToString());
                            if (flatGenericCount > 0 && substanceType == "GROUP") {
                                IsEnableGotoTreeView = true;
                            }
                            else{

                                IsEnableGotoTreeView = false;
                            }
                            NotifyPropertyChanged("IsEnableGotoTreeView");
                        }
                            if (_currentCellData.Column.Header.ToString() == "ChildCount")
                        {

                            var result = Win_Administrator_VM.ChildDataForPopup.Where(x => x.ParentCAS.Replace("-", "") == value.Item.GetType().GetProperty("CAS").GetValue(value.Item).ToString().Replace("-", "")).ToList();
                            var dtResult = objCommon.ConvertToDataTable(result);
                            objPopup = new ChildPopup_VM(dtResult);
                            ChildPopUp objPopWindow = new ChildPopUp();
                            objPopWindow.DataContext = objPopup;
                            objPopWindow.ShowDialog();
                        }
                        //if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                        //{
                        //    string header = ((System.Windows.Data.Binding)((System.Windows.Controls.DataGridBoundColumn)_currentCellData.Column).Binding).Path.Path;
                        //    if (value.Item.GetType().GetProperty(header).GetValue(value.Item) != null)
                        //    {
                        //        System.Windows.Clipboard.SetText(value.Item.GetType().GetProperty(header).GetValue(value.Item).ToString());
                        //    }
                        //}
                    }


                }
            }
        }
        #endregion

        #region CustomQuery and Show in SerachCriteria
        public List<filterList> _listfilter { get; set; } = new List<filterList>();
        public QueryController _queryController { get; set; } = new QueryController();
        public QueryController CustomQueryController
        {
            get { return _queryController; }
            set
            {               
                    _queryController = value;
                NotifyPropertyChanged("CustomQueryController");
            }
        }
        private void CustomQueryController_FilteringStarted(object sender, EventArgs e)
        {
            if (CustomQueryController.ColumnFilterData.Operator != DataGridFilterLibrary.Support.FilterOperator.Undefined)
            {
                if (CustomQueryController.ColumnFilterData != null)
                {
                    string colName = CustomQueryController.ColumnFilterData.ValuePropertyBindingPath;
                    CustomQueryController.ColumnFilterData.QueryString = colName == "CAS" ? CustomQueryController.ColumnFilterData.QueryString.Replace("-", "") : CustomQueryController.ColumnFilterData.QueryString;
                    string valueofCol = CustomQueryController.ColumnFilterData.QueryString;
                    string valueofColTo = CustomQueryController.ColumnFilterData.QueryStringTo;
                    Type valueType = CustomQueryController.ColumnFilterData.ValuePropertyType;
                    string filterOpeartor = CustomQueryController.ColumnFilterData.Operator.ToString();
                    if (CustomQueryController.ColumnFilterData.Operator != DataGridFilterLibrary.Support.FilterOperator.Undefined)
                    {
                        if ((valueofCol != null && valueofCol != "") || (valueofColTo != null && valueofColTo != ""))
                        {
                            if (!_listfilter.Any(x => x.ColName == colName && x.ColValue == valueofCol && x.ColOperator == filterOpeartor && x.ColValueTo == valueofColTo) || colName == "")
                            {
                                if (_listfilter.Any(x => x.ColName == colName))
                                {
                                    var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                                    objFilter.ColValue = valueofCol;
                                    objFilter.ColOperator = filterOpeartor;
                                    objFilter.ColValueTo = valueofColTo;
                                }
                                else
                                {
                                    _listfilter.Add(new filterList { ColName = colName, ColValue = valueofCol, ColType = valueType, ColOperator = filterOpeartor, ColValueTo = valueofColTo });
                                }
                                //SearchCriteria = string.Empty;
                                GetFilterQuery();
                            }
                        }
                        else
                        {

                            var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                            if (objFilter != null)
                            {
                                objFilter.ColValue = valueofCol;
                                objFilter.ColValueTo = valueofColTo;
                                //SearchCriteria = string.Empty;
                                GetFilterQuery();
                            }
                            if (commandFilterExecute == true)
                            {
                                myList = myListTotal;
                                SearchCriteria = string.Empty;
                                Navigate(0);
                            }
                        }
                    }
                }
            }
        }
        public void GetFilterQuery()
        {
            string queryString = string.Empty;
            string searchQueryString = string.Empty;
            List<string> arryOfColValue = new List<string>();
            _listfilter = _listfilter.Where(x => x.ColValue != "").ToList();
            int cnter = 0;
            for (var counter = 0; counter < _listfilter.Count; counter++)
            {
                if (_listfilter[counter].ColType == typeof(Int32) || _listfilter[counter].ColType == typeof(int) || _listfilter[counter].ColType == typeof(Int16))
                {
                    queryString += counter > 0 ? " &&" : "";
                    if (Int32.TryParse(_listfilter[counter].ColValue, out int res))
                    {
                        switch (_listfilter[counter].ColOperator)
                        {
                            case "Equals":
                                queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThan":
                                queryString += " " + _listfilter[counter].ColName + " < " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThan":
                                queryString += " " + _listfilter[counter].ColName + " > " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " <= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " >= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                        }
                        searchQueryString += " " + _listfilter[counter].ColName + " " + _listfilter[counter].ColOperator + " '" + _listfilter[counter].ColValue + "',";
                    }
                }
                else
                {
                    if (_listfilter[counter].ColType.FullName.Contains("DateTime"))
                    {
                        queryString += counter > 0 ? " &&" : "";
                        if ((_listfilter[counter].ColValue != null && _listfilter[counter].ColValue != string.Empty) && (_listfilter[counter].ColValueTo != null && _listfilter[counter].ColValueTo != string.Empty))
                        {
                            var stringDateTime = Convert.ToDateTime(_listfilter[counter].ColValue).Year + ", " + Convert.ToDateTime(_listfilter[counter].ColValue).Month + ", " + Convert.ToDateTime(_listfilter[counter].ColValue).Day;
                            var stringDateTimeTo = Convert.ToDateTime(_listfilter[counter].ColValueTo).Year + ", " + Convert.ToDateTime(_listfilter[counter].ColValueTo).Month + ", " + Convert.ToDateTime(_listfilter[counter].ColValueTo).Day; ;
                            queryString += " " + _listfilter[counter].ColName + " >= DateTime(" + stringDateTime + ") && " + _listfilter[counter].ColName + " <= DateTime(" + stringDateTimeTo + ")";
                            searchQueryString += " " + _listfilter[counter].ColName + " between " + _listfilter[counter].ColValue + " and " + _listfilter[counter].ColValueTo + "',";
                        }
                    }
                    else
                    //if (!_listfilter[counter].ColType.FullName.Contains("DateTime"))
                    {
                        if (_listfilter[counter].ColType == typeof(bool))
                        {
                            queryString += counter > 0 ? " && " : "";
                            queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToBoolean(_listfilter[counter].ColValue);
                            searchQueryString += " " + _listfilter[counter].ColName + " Equal '" + _listfilter[counter].ColValue + "',";
                            //arryOfColValue.Add(_listfilter[counter].ColValue.ToString());
                        }
                        else
                        {
                            queryString += counter > 0 ? " && " : "";
                            if (_listfilter[counter].ColName.Contains("CAS"))
                            {
                                queryString += " " + _listfilter[counter].ColName + ".Replace(\"-\",\"\").ToLower().Contains(@" + cnter + ")";
                                //queryString += " " + _listfilter[counter].ColName + ".ToLower().Contains(@" + counter + ")";
                                searchQueryString += " " + _listfilter[counter].ColName + " Like '" + _listfilter[counter].ColValue.Replace("-", "") + "',";
                                arryOfColValue.Add(_listfilter[counter].ColValue.ToString().ToLower().Replace("-", ""));
                                cnter += 1;
                            }
                            else
                            {
                                queryString += " " + _listfilter[counter].ColName + ".ToLower().Contains(@" + cnter + ")";
                                searchQueryString += " " + _listfilter[counter].ColName + " Like '" + _listfilter[counter].ColValue + "',";
                                arryOfColValue.Add(_listfilter[counter].ColValue.ToString().ToLower());
                                cnter += 1;
                            }

                        }
                    }
                }

            }
            if (queryString != "")
            {
                if (arryOfColValue.Count == 0)
                {
                    var lst = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(queryString.Trim()), listType, defaultDataTableColumns, listCheckBox);
                    myList = (IList)lst;
                }
                else
                {
                    var lst = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(queryString.Trim(), arryOfColValue.ToArray()), listType, defaultDataTableColumns, listCheckBox);
                    myList = (IList)lst;
                    //var splitQuery = queryString.Trim().Split('&').ToList();
                    //var totValue = myListTotal;
                    //for (int i = 0; i < arryOfColValue.Count();i++)
                    //{
                    //    var lst1 = _objLibraryFunction_Service.BindListFromIQuerable(totValue.AsQueryable().Where(splitQuery[i].Replace("&","").Trim(), arryOfColValue[i]), listType, defaultDataTableColumns);
                    //    totValue = (IList)lst1;

                    //}
                    //myList = (IList)totValue;
                }
            }
            else
            {
                myList = myListTotal;
            }
            SearchCriteria = string.Empty;
            SearchCriteria = searchQueryString.TrimEnd(',');
            Navigate(0);
            commandFilterExecute = false;
        }
        #endregion

        #region ClearFilter Button Click
        public bool commandFilterExecute { get; set; }
        public void CommandDataGridClearFilterExecute(object sender)
        {
            commandFilterExecute = true;
            ExecuteClearFilter();
        }
        public DataGridFilterCommand _commandDataGridClearFilter { get; set; }
        public DataGridFilterCommand CommandDataGridClearFilter
        {
            get { return _commandDataGridClearFilter; }
            set
            {
                _commandDataGridClearFilter = new DataGridFilterCommand(new Action<object>(CommandDataGridClearFilterExecute));
            }
        }
        private void ExecuteClearFilter()
        {
            if (CustomQueryController != null && CustomQueryController.ColumnFilterData != null)
            {
                //if (CustomQueryController.ColumnFilterData.QueryString.Length > 0)
                //{
                CustomQueryController.ClearFilter();
                _listfilter.Clear();
                myList = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                SearchCriteria = string.Empty;
                NotifyPropertyChanged("SearchCriteria");
                Navigate(0);
                //}
            }
        }
        #endregion

        #region Excel Export
        public ICommand ParentChildGenerateExcelFile { get; set; }
        private void CommandExcelExportParentChild(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                RowIdentifier.ExportExcelFile(dtDefault, folderDlg.SelectedPath + "\\Test.Xlsx");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        #endregion

        #region Access Export
        public ICommand ParentChildGenerateAccessFile { get; set; }
        public ICommand ClearAllFilter { get; set; }
        private void CommandClearAllFilter(object obj)
        {
            commandFilterExecute = true;
            ExecuteClearFilter();
        }
        private void CommandAccessExportParentChild(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var chekListFieldName = objCommon.DbaseQueryReturnTableSql("select top 1 c.phrase from " +
                " QSxxx_ListDictionary a, Library_PhraseCategories b, Library_Phrases c   " +
                " where a.PhraseCategoryID = b.PhraseCategoryID and b.PhraseCategory = 'List Field Name' " +
                " and a.PhraseID = c.PhraseID and QSID_ID = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                var fileName = chekListFieldName + "_" + Win_Administrator_VM.Qsid + "_ParentChild_data_view_" + System.DateTime.Now.ToString("dd-MMM-yyyy_hh_mm_ss") + ".mdb";

                objCurrentView.ExportAccessFileLocal(folderDlg.SelectedPath + "\\" + fileName, dtDefault, "ParentChild");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        #endregion

        #region Paging
        int pageIndex = 1;
        private int numberOfRecPerPage = 25;
        private enum PagingMode { First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public string lblContent { get; set; }
        public string lblTotRecords { get; set; }
        public selectedListItemCombo _selectedItemCombo { get; set; }
        public selectedListItemCombo selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }
        private void Navigate(int mode)
        {
            lblContent = "";
            lblTotRecords = "";
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("lblContent");
            var result = _objLibraryFunction_Service.NavigateIList(mode, ref pageIndex, numberOfRecPerPage, myList, selectedItemCombo.text, lblContent, lblTotRecords, myListDataTable, listType, defaultDataTableColumns, listCheckBox);
            lblContent = result.Item1;
            lblTotRecords = result.Item2;
            myListDataTable = result.Item3;
            NotifyPropertyChanged("lblTotRecords");
            NotifyPropertyChanged("myListDataTable");
            NotifyPropertyChanged("lblContent");

            //if (myList.Count > 0)
            //{
            //    int count;
            //    switch (mode)
            //    {
            //        case (int)PagingMode.Next:
            //            if (myList.Count >= (pageIndex * numberOfRecPerPage))
            //            {
            //                if (myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count() == 0)
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip((pageIndex * numberOfRecPerPage) - numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = (pageIndex * numberOfRecPerPage) + myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count();
            //                    NotifyPropertyChanged("myListDataTable");
            //                }
            //                else
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = (pageIndex * numberOfRecPerPage) + myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage).Count();
            //                    pageIndex++;
            //                    NotifyPropertyChanged("myListDataTable");
            //                }


            //                var lastCount = (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage);
            //                lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (lastCount > myList.Count ? myList.Count : lastCount) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //        case (int)PagingMode.Previous:
            //            if (pageIndex > 1)
            //            {
            //                pageIndex -= 1;

            //                if (pageIndex == 1)
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = myList.AsQueryable().Take(numberOfRecPerPage).Count();
            //                    lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                    lblTotRecords = "Total Records : " + myList.Count;
            //                    NotifyPropertyChanged("lblTotRecords");
            //                    NotifyPropertyChanged("myListDataTable");
            //                    NotifyPropertyChanged("lblContent");
            //                }
            //                else
            //                {
            //                    var lst = BindListFromIQuerable(myList.AsQueryable().Skip(pageIndex * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                    myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                    count = Math.Min(pageIndex * numberOfRecPerPage, myList.Count);
            //                    lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage) + " of " + myList.Count;
            //                    lblTotRecords = "Total Records : " + myList.Count;
            //                    NotifyPropertyChanged("lblTotRecords");
            //                    NotifyPropertyChanged("myListDataTable");
            //                    NotifyPropertyChanged("lblContent");
            //                }
            //            }
            //            break;

            //        case (int)PagingMode.First:
            //            pageIndex = 2;
            //            Navigate((int)PagingMode.Previous);
            //            break;
            //        case (int)PagingMode.Last:
            //            pageIndex = (myList.Count / numberOfRecPerPage);
            //            Navigate((int)PagingMode.Next);
            //            break;

            //        case (int)PagingMode.PageCountChange:

            //            pageIndex = 1;
            //            if (selectedItemCombo.text == "All")
            //            {
            //                numberOfRecPerPage = myList.Count;
            //                myListDataTable = myList.Count > 0 ? myList : null;
            //                count = myList.Count;
            //                lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("myListDataTable");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            else
            //            {
            //                numberOfRecPerPage = Convert.ToInt32(selectedItemCombo.text);
            //                //System.Collections.IList lst = (System.Collections.IList)Activator.CreateInstance(listType);
            //                var lst = BindListFromIQuerable(myList.AsQueryable().Take(numberOfRecPerPage));
            //                myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                count = myList.AsQueryable().Take(numberOfRecPerPage).Count();
            //                lblContent = 1 + " to " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("myListDataTable");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //        default:
            //            if (myList.Count >= 1)
            //            {
            //                var lst = BindListFromIQuerable(myList.AsQueryable().Skip(0 * numberOfRecPerPage).Take(numberOfRecPerPage));
            //                myListDataTable = myList.Count > 0 ? (IList)lst : null;
            //                count = 25;
            //                NotifyPropertyChanged("myListDataTable");
            //                lblContent = 1 + " of " + (count > myList.Count ? myList.Count : count) + " of " + myList.Count;
            //                lblTotRecords = "Total Records : " + myList.Count;
            //                NotifyPropertyChanged("lblTotRecords");
            //                NotifyPropertyChanged("lblContent");
            //            }
            //            break;
            //    }
            //    // ExecuteClearFilter();
            //}
        }
        private List<selectedListItemCombo> AddList()
        {
            List<selectedListItemCombo> listSelectedCombo =

                new List<selectedListItemCombo>() {
                new selectedListItemCombo() { id = 25, text = "25" },
                new selectedListItemCombo() { id = 50, text = "50" },
                new selectedListItemCombo() { id = 100, text = "100" },
                new selectedListItemCombo() { id = 101, text = "All" },
            };
            return listSelectedCombo;

        }

        #endregion

        #region SearchCriteria  
        public Visibility IsSearchCriteriaVisible { get; set; } = Visibility.Collapsed;
        public string _searchCriteria { get; set; }
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (_searchCriteria != value)
                {
                    _searchCriteria = value;
                    NotifyPropertyChanged("SearchCriteria");
                    if (String.IsNullOrEmpty(_searchCriteria))
                    {
                        IsSearchCriteriaVisible = Visibility.Collapsed;
                    }
                    else
                    {
                        IsSearchCriteriaVisible = Visibility.Visible;
                    }
                    NotifyPropertyChanged("IsSearchCriteriaVisible");
                }
            }
        }
        #endregion
        private void CommmandChangeVisiblityListColNullExecute(object obj)
        {
            listAllColVisibleNull = !listAllColVisibleNull;
            NotifyPropertyChanged("listAllColVisibleNull");
        }
        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            if (listAllColVisible == Visibility.Collapsed)
            {
                listAllColVisible = Visibility.Visible;
            }
            else
            {
                listAllColVisible = Visibility.Collapsed;
            }
            NotifyPropertyChanged("listAllColVisible");
        }
        private void CommandOnColSelectedChangeNullExecute(object obj)
        {
            ExecuteClearFilter();
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDGNull.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            Type Proptype = null;
            if (selectColName == null)
            {
                Proptype = typeof(string);
            }
            else
            {
                foreach (DataColumn dc in defaultDataTableColumns)
                {
                    if (selectColName.ToLower() == dc.ColumnName.ToLower())
                    {
                        Proptype = dc.DataType;
                        break;
                    }
                }
            }
            if (Proptype != null)
            {
                if (selectCol != null)
                {
                    if (Proptype == typeof(string))
                    {
                        if (selectCol.IsNull)
                        {
                            queryNull.Add(" " + selectColName + ".Length = 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectColName + ".Length = 0 ");
                        }
                        if (selectCol.IsNotNull)
                        {
                            queryNull.Add(" " + selectColName + ".Length > 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectColName + ".Length > 0 ");
                        }
                    }
                    else
                    {
                        if (Proptype == typeof(Boolean))
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectColName + ".Value == false ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + ".Value == false ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectColName + ".Value == true ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + ".Value == true ");
                            }
                        }
                        else
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectColName + " = null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + " = null ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectColName + " != null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectColName + " != null ");
                            }
                        }
                    }
                }
                var result = string.Join("&&", queryNull.ToList());
                queryStringForNUll = result;
                SearchCriteria = result;
                NotifyPropertyChanged("SearchCriteria");
                if (result == string.Empty)
                {
                    queryStringForNUll = string.Empty;
                    var list = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(list.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                    myList = myListTotal;
                }
                else
                {
                    var list = _objLibraryFunction_Service.BindListFromTable(dtDefault, listType);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(list.AsQueryable(), listType, defaultDataTableColumns, listCheckBox);
                    myListTotal = _objLibraryFunction_Service.BindListFromIQuerable(myListTotal.AsQueryable().Where(result.Trim()), listType, defaultDataTableColumns, listCheckBox);
                    myList = myListTotal;
                }
                if (myList.Count == 0)
                {
                    myListDataTable = null;
                    lblContent = "";
                    lblTotRecords = "";
                    NotifyPropertyChanged("lblTotRecords");
                    NotifyPropertyChanged("lblContent");
                    NotifyPropertyChanged("myListDataTable");
                }
                else
                {
                    Navigate(0);
                }
            }
        }
        public void BindListAllColumn()
        {

            var list = new List<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
            List<iColumnName> result = new List<iColumnName>();
            foreach (DataColumn dt in defaultDataTableColumns)
            {
                if (dt.ColumnName == "CasID")
                {
                    list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { IsSelected = false, ColumnName = dt.ColumnName });
                }
                else {
                    list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { IsSelected = true, ColumnName = dt.ColumnName });
                    
                }
                result.Add(new iColumnName { ColumnName = dt.ColumnName });
            }
            listMdbColumns = new ObservableCollection<iColumnName>(result);
            NotifyPropertyChanged("listMdbColumns");
            ListAllColumnDG = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>(list);
            NotifyPropertyChanged("ListAllColumnDG");

            var listNull = new List<ColumnSelectionDataGrid>();
            foreach (DataColumn dt in defaultDataTableColumns)
            {
                listNull.Add(new ColumnSelectionDataGrid { ColumnName = dt.ColumnName, IsNull = false, IsNotNull = false });
            }

            ListAllColumnDGNull = new ObservableCollection<ColumnSelectionDataGrid>(listNull);
            NotifyPropertyChanged("ListAllColumnDGNull");
        }

        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDG.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            //selectCol.IsSelected = !selectCol.IsSelected;
            var selectColInGrid = ColumnCollection.Where(x => x.Header.ToString() == selectColName).FirstOrDefault();
            if (selectColInGrid != null)
            {
                if (selectCol.IsSelected)
                {
                    selectColInGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    selectColInGrid.Visibility = Visibility.Collapsed;
                }
            }
            NotifyPropertyChanged("ColumnCollection");
            CustomQueryController = new QueryController();
            CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;

            // NotifyPropertyChanged("listSelectedColumn");
        }

    }
}
