﻿using System;
using System.Collections.Generic;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Windows.Input;
using CurrentDataView.Library;
using GalaSoft.MvvmLight.Messaging;
using System.Linq;
using EntityClass;
using VersionControlSystem.Model.ApplicationEngine;
using ToastNotifications.Messages;

namespace CurrentDataView.ViewModel
{
    public class ProductFields_VM : Base_ViewModel
    {
        private readonly CommonFunctions objCommon = new CommonFunctions();
        public ICommand CommandProductFields { get; set; }
        public StandardDataGrid_VM listProductFields { get; set; }
        public Visibility VisibleLoderProductFields { get; set; }
        public object SelectedRowItem { get; set; }
        public ProductFields_VM()
        {
            CommandProductFields = new RelayCommand(CommandProductFieldsExecute, CommandProductFieldsCanExecute);
            MessengerInstance.Register<PropertyChangedMessage<object>>(this, NotifyMe);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ProductFields_VM), CallbackSaveBtnClick);
            Task.Run(() => RunProductFields());
        }
        private void NotifyMe(PropertyChangedMessage<object> obj)
        {
            if (obj.PropertyName == "ProductFields")
            {
                SelectedRowItem = obj.NewValue;
            }
        }
        private void CallbackSaveBtnClick(NotificationMessageAction<object> obj)
        {
            Int32 fieldNameID = Convert.ToInt32(((dynamic)SelectedRowItem).FieldNameID);
            var qSID_ID = Win_Administrator_VM.Qsid_ID;
            List<string> selectedVal = new List<string>();
            selectedVal.Add(((dynamic)SelectedRowItem).I4C.Value == true ? "I4C" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).I4F.Value == true ? "I4F" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).RD.Value == true ? "RD" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).SAP_Rules.Value == true ? "SAP_Rules" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).Generate.Value == true ? "Generate" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).ECHA.Value == true ? "ECHA" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).S4.Value == true ? "S4" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).ERC.Value == true ? "ERC" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).DF1.Value == true ? "DF1" : string.Empty);
            selectedVal.Add(((dynamic)SelectedRowItem).Prosteward.Value == true ? "Prosteward" : string.Empty);
            selectedVal = selectedVal.Where(x => x != string.Empty).ToList();
            if (selectedVal.Count > 1)
            {
                string msg = "Can't select two values for fieldName" + Convert.ToString(((dynamic)SelectedRowItem).FieldName);
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess(msg);
                });
                return;
            }
            List<Map_Product_Fields> lstList = new List<Map_Product_Fields>();
            List<Log_Map_Product_Fields> lstListLog = new List<Log_Map_Product_Fields>();
            using (var context = new CRAModel())
            {
                if (selectedVal.Count == 1)
                {
                    var selval = selectedVal[0].ToString();
                    var productid = context.Library_Products.AsNoTracking().Where(x => x.ProductInternalCode == selval).Select(y => y.ProductID).FirstOrDefault();
                    var mapProductId = context.Map_Product_Fields.AsNoTracking().Where(x => x.QSID_ID == qSID_ID && x.FieldNameID == fieldNameID).FirstOrDefault();
                    if (mapProductId != null)
                    {
                        var lst = new Map_Product_Fields
                        {
                            FieldNameID = fieldNameID,
                            QSID_ID = qSID_ID,
                            ProductID = productid,
                            MapFieldsProductID = mapProductId.MapFieldsProductID,
                        };
                        var lstLog = new Log_Map_Product_Fields
                        {
                            FieldNameID = fieldNameID,
                            Qsid_ID = qSID_ID,
                            ProductID = productid,
                            SessionID = Engine.CurrentUserSessionID,
                            Status = "C",
                            TimeStamp = objCommon.ESTTime(),
                            MapFieldsProductID = mapProductId.MapFieldsProductID,
                            Log_MapFieldsProductID = 0,
                        };
                        lstList.Add(lst);
                        lstListLog.Add(lstLog);
                        _objLibraryFunction_Service.UpdateMapProductField(lstList);
                        _objLibraryFunction_Service.BulkIns<Log_Map_Product_Fields>(lstListLog);
                    }
                    else
                    {
                        var lst = new Map_Product_Fields
                        {
                            FieldNameID = fieldNameID,
                            QSID_ID = qSID_ID,
                            ProductID = productid,
                        };
                        context.Map_Product_Fields.Add(lst);
                        context.SaveChanges();
                        var mapProdID = lst.MapFieldsProductID;
                        var lstLog = new Log_Map_Product_Fields
                        {
                            FieldNameID = fieldNameID,
                            Qsid_ID = qSID_ID,
                            ProductID = productid,
                            SessionID = Engine.CurrentUserSessionID,
                            Status = "A",
                            TimeStamp = objCommon.ESTTime(),
                            MapFieldsProductID = mapProdID,
                            Log_MapFieldsProductID = 0,
                        };
                        lstListLog.Add(lstLog);
                        _objLibraryFunction_Service.BulkIns(lstListLog);
                    }
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _notifier.ShowSuccess("Updated successfully.");
                    });
                    Win_Administrator_VM.ListProductFields = new DataTable();
                    Task.Run(() => RunProductFields());
                }
                else
                {
                    var mapProductId = context.Map_Product_Fields.AsNoTracking().Where(x => x.QSID_ID == qSID_ID && x.FieldNameID == fieldNameID).FirstOrDefault();
                    objCommon.DbaseQueryReturnStringSQL("Delete from Map_Product_Fields where qsid_id = " + qSID_ID + " and FieldNameID = " + fieldNameID, Win_Administrator_VM.CommonListConn);
                    var lstLog = new Log_Map_Product_Fields
                    {
                        FieldNameID = fieldNameID,
                        Qsid_ID = qSID_ID,
                        ProductID = mapProductId.ProductID,
                        SessionID = Engine.CurrentUserSessionID,
                        Status = "D",
                        TimeStamp = objCommon.ESTTime(),
                        MapFieldsProductID = mapProductId.MapFieldsProductID,
                        Log_MapFieldsProductID = 0,
                    };
                    lstListLog.Add(lstLog);
                    _objLibraryFunction_Service.BulkIns(lstListLog);
                }
            }
        }

        private bool CommandProductFieldsCanExecute(object obj)
        {
            return true;
        }
        private void CommandProductFieldsExecute(object obj)
        {

        }
        private void RunProductFields()
        {
            VisibleLoderProductFields = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderProductFields");
            Win_Administrator_VM.ListProductFields = _objLibraryFunction_Service.GetMapProductFields(Win_Administrator_VM.Qsid_ID);
            
            List<IProductFields> lst = new List<IProductFields>();
            for (int i = 0; i < Win_Administrator_VM.ListProductFields.Rows.Count; i++)
            {
                lst.Add(new IProductFields
                {
                    FieldNameID = Win_Administrator_VM.ListProductFields.Rows[i].Field<int>("FieldNameID"),
                    FieldName = Win_Administrator_VM.ListProductFields.Rows[i].Field<string>("FieldName"),
                    I4C = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("I4C") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("I4C"),
                    I4F = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("I4F") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("I4F"),
                    RD = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("RD") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("RD"),
                    SAP_Rules = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("SAP_Rules") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("SAP_Rules"),
                    Generate = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("Generate") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("Generate"),
                    ECHA = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("ECHA") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("ECHA"),
                    S4 = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("S4") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("S4"),
                    ERC = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("ERC") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("ERC"),
                    DF1 = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("DF1") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("DF1"),
                    Prosteward = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("Prosteward") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("Prosteward"),
                    _3E_Protect = Win_Administrator_VM.ListProductFields.Rows[i].Field<dynamic>("3E Protect") == null ? false : Win_Administrator_VM.ListProductFields.Rows[i].Field<bool>("3E Protect"),
                });
            }
            listProductFields = new StandardDataGrid_VM("ProductFields", Win_Administrator_VM.ListProductFields, Visibility.Visible, true, false, typeof(ProductFields_VM));
            NotifyPropertyChanged("listProductFields");
            VisibleLoderProductFields = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleLoderProductFields");
        }
    }

    public class IProductFields
    {
        public int FieldNameID { get; set; }
        public string FieldName { get; set; }
        public bool I4C { get; set; }
        public bool I4F { get; set; }
        public bool RD { get; set; }
        public bool SAP_Rules { get; set; }
        public bool Generate { get; set; }
        public bool ECHA { get; set; }
        public bool S4 { get; set; }
        public bool ERC { get; set; }
        public bool _3E_Protect { get; set; }
        public bool DF1 { get; set; }
        public bool Prosteward { get; set; }
    }
}
