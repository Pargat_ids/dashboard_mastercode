﻿using System;
using System.Collections.Generic;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Windows.Input;
using CurrentDataView.Library;
using GalaSoft.MvvmLight.Messaging;
using System.Linq;
using EntityClass;
using VersionControlSystem.Model.ApplicationEngine;
using ToastNotifications.Messages;
using System.Collections.ObjectModel;
using System.Windows.Forms;

namespace CurrentDataView.ViewModel
{
    public class ProductionVersionChanges_VM : Base_ViewModel
    {
        private readonly CommonFunctions objCommon = new CommonFunctions();
        List<int?> sessIds = new List<int?>();
        public ObservableCollection<string> listallProductionVersion { get; set; }
        private string _SelectedChooseVersionFirst { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderProductVersion { get; set; } = Visibility.Collapsed;
        public StandardDataGrid_VM lstProductionVersion { get; set; }
        public ICommand ShowProductionVersion { get; set; }
        public string SelectedChooseVersionFirst
        {
            get => _SelectedChooseVersionFirst;
            set
            {
                if (Equals(_SelectedChooseVersionFirst, value))
                {
                    return;
                }

                _SelectedChooseVersionFirst = value;
            }
        }

        private CommonFunctions ObjCommon = new CommonFunctions();
        private string _SelectedChooseVersionSec { get; set; }
        public string SelectedChooseVersionSec
        {
            get => _SelectedChooseVersionSec;
            set
            {
                if (Equals(_SelectedChooseVersionSec, value))
                {
                    return;
                }

                _SelectedChooseVersionSec = value;
            }
        }
        public ProductionVersionChanges_VM()
        {
            using (var context = new CRAModel())
            {
                var allAvailableVersions = context.Log_VersionControl_History.AsNoTracking().Where(y => y.QSID_ID == Win_Administrator_VM.Qsid_ID && y.ProductTimeStamp != null).Select(z => z.Version).OrderByDescending(y => y).ToList();
                listallProductionVersion = new ObservableCollection<string>(allAvailableVersions);
                NotifyPropertyChanged("listallProductionVersion");
                SelectedChooseVersionFirst = allAvailableVersions.Skip(1).Take(1).FirstOrDefault();
                SelectedChooseVersionSec = allAvailableVersions.Take(1).FirstOrDefault(); 
                NotifyPropertyChanged("SelectedChooseVersionFirst");
                NotifyPropertyChanged("SelectedChooseVersionSec");
                ShowProductionVersion = new RelayCommand(CommandShowExecute, CommandFirstCanExecute);
                MessengerInstance.Register<PropertyChangedMessage<object>>(this, NotifyMe);
                MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(ProductionVersionChanges_VM), CallbackSaveBtnClick);

            }
        }
        public object SelectedRowItem { get; set; }
        private void NotifyMe(PropertyChangedMessage<object> obj)
        {
            if (obj.PropertyName == "Row Added Deleted By Version")
            {
                SelectedRowItem = obj.NewValue;
            }
        }
        private void CallbackSaveBtnClick(NotificationMessageAction<object> obj)
        {
            var confirmation = System.Windows.Forms.MessageBox.Show("Do you want to delete the record", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (confirmation == DialogResult.Yes)
            {
                string id = Convert.ToString(((dynamic)SelectedRowItem).ID);
                Int32 rID = Convert.ToInt32(((dynamic)SelectedRowItem).RowID);
                string Typ = Convert.ToString(((dynamic)SelectedRowItem).Type);
                var qSID_ID = Win_Administrator_VM.Qsid_ID;

                switch (Typ)
                {
                    case "CAS":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_Qsxxx_Cas where Log_QSxxx_CAS_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "PREF":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_PREF where Log_QSxxx_PREF_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "PHRASES":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_Phrases where Log_QSxxx_Phrases_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "Identifier":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_Identifiers where Log_QSxxx_Identifiers_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "Notes":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_Notes where Log_QSxxx_NotesID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "Conditional Mapping":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_ConditionalMapping where Log_QSxxx_ConditionalMapping_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "Value Other":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_Values where Log_QSxxx_Values_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                    case "Value":
                        ObjCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_Values where Log_QSxxx_Values_ID in ( " + id + " )", Win_Administrator_VM.CommonListConn);
                        break;
                }
                for (int i = 0; i < id.Split(',').Count(); i++)
                {
                    List<Log_QSxxx_RowHistory_manualDeletes> lstLog = new List<Log_QSxxx_RowHistory_manualDeletes>();
                    var log = new Log_QSxxx_RowHistory_manualDeletes
                    {
                        RowID = rID,
                        QSID_ID = Win_Administrator_VM.Qsid_ID,
                        SessionID = Engine.CurrentUserSessionID,
                        DeleteType = Typ,
                        LogQSxxxRowHistoryManualDeleteID = 0,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(log);
                    _objLibraryFunction_Service.BulkIns(lstLog);
                }
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    _notifier.ShowSuccess("Deleted successfully.");
                });
                //VisibleLoderProductVersion = Visibility.Visible;
                //NotifyPropertyChanged("VisibleLoderProductVersion");
                Task.Run(() => RunProductVersion(sessIds));
            }
        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void CommandShowExecute(object obj)
        {
            using (var context = new CRAModel())
            {
                var prodStampTo = context.Log_VersionControl_History.AsNoTracking().Where(y => y.QSID_ID == Win_Administrator_VM.Qsid_ID && y.Version == _SelectedChooseVersionSec).Select(x => x.ProductTimeStamp).FirstOrDefault();
                var prodStampFrom = context.Log_VersionControl_History.AsNoTracking().Where(y => y.QSID_ID == Win_Administrator_VM.Qsid_ID && y.Version == _SelectedChooseVersionFirst).Select(x => x.ProductTimeStamp).FirstOrDefault();
                sessIds = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.SessionBegin_TimeStamp >= prodStampFrom && x.SessionBegin_TimeStamp <= prodStampTo && (x.SessionTypeID == 1 || x.SessionTypeID == 5)).Select(y => (int?)y.SessionID).ToList();
                //VisibleLoderProductVersion = Visibility.Visible;
                //NotifyPropertyChanged("VisibleLoderProductVersion");
                Task.Run(() => RunProductVersion(sessIds));
            }
        }

        private void RunProductVersion(List<int?> sessIds)
        {
            VisibleLoderProductVersion = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderProductVersion");
            var result = GetRows(sessIds);
            Win_Administrator_VM.dtProductionVersion = result;
            if (result != null)
            {
                lstProductionVersion = new StandardDataGrid_VM("Row Added Deleted By Version", Win_Administrator_VM.dtProductionVersion, Visibility.Visible, false, true, typeof(ProductionVersionChanges_VM));
            }
            NotifyPropertyChanged("lstProductionVersion");
            VisibleLoderProductVersion = Visibility.Collapsed;
            NotifyPropertyChanged("VisibleLoderProductVersion");
        }

        private DataTable GetRows(List<int?> sessIds)
        {
            using (var context = new CRAModel())
            {
                var casIds = (from d1 in context.Log_QSxxx_CAS.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                              join d2 in context.QSxxx_CAS.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                              on d1.QSCASID equals d2.QSCASID
                              select d1).ToList();
                var prefIds = (from d1 in context.Log_QSxxx_PREF.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                               join d2 in context.QSxxx_PREF.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                               on d1.QSPrefID equals d2.QSPrefID
                               select d1).ToList();
                var phraseIds = (from d1 in context.Log_QSxxx_Phrases.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                                 join d2 in context.QSxxx_Phrases.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                                 on d1.QSPhraseID equals d2.QSPhraseID
                                 select d1).ToList();
                var identIds = (from d1 in context.Log_QSxxx_Identifiers.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                                join d2 in context.QSxxx_Identifiers.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                                on d1.QSIdentifiersID equals d2.QSIdentifiersID
                                select d1).ToList();
                var notesIds = (from d1 in context.Log_QSxxx_Notes.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                                join d2 in context.QSxxx_Notes.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                                on d1.NoteCodeID equals d2.NoteCodeID
                                select d1).ToList();
               var cmIds = (from d1 in context.Log_QSxxx_ConditionalMapping.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C")
                             join d2 in context.QSxxx_ConditionalMapping.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID)
                              on d1.QSConditionalMappingID equals d2.QSConditionalMappingID
                             select d1).ToList();
                var valOthIds = (from d1 in context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C" && x.ValueOtherFieldID != null).ToList()
                                 join d2 in context.QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.ValueOtherFieldID != null)
                                on d1.QSValuesID equals d2.QSValuesID
                                 select d1).ToList();
                var valIds = (from d1 in context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && sessIds.Contains(x.SessionID) && x.Status == "C" && x.ValueOtherFieldID == null).ToList()
                              join d2 in context.QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.ValueOtherFieldID == null)
                             on d1.QSValuesID equals d2.QSValuesID
                              select d1).ToList();

                // Now Grouping

                var casGroup = casIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.CASID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var prefGroup = prefIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.ChemicalNameID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var phraseGroup = phraseIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.PhraseID, x.PhraseCategoryID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var noteGroup = notesIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.NoteCodeID, x.PhraseID, x.PhraseCategoryID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var identGroup = identIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.IdentifierValueID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var cmGroup = cmIds.GroupBy(x => new { x.FieldNameID, x.RowID, x.SubfieldID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.FieldNameID }).ToList();
                var valOthGroup = valOthIds.GroupBy(x => new { x.ValueOtherFieldID, x.RowID, x.ValueOther }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.ValueOtherFieldID }).ToList();
                var valGroup = valIds.GroupBy(x => new { x.MaxFieldNameID, x.MinFieldNameID, x.RowID, x.MaxValue, x.MinValue, x.MaxValOpFieldID, x.MinValOpFieldID, x.MaxValueOpID, x.MinValueOpID }).Where(grp => grp.Count() > 1).Select(grp => new { grp.Key.RowID, grp.Key.MaxFieldNameID, grp.Key.MinFieldNameID }).ToList();


                var casF = casGroup.Select(y => y.FieldNameID).ToList();
                var prefF = prefGroup.Select(y => y.FieldNameID).ToList();
                var phraseF = phraseGroup.Select(y => y.FieldNameID).ToList();
                var noteF = noteGroup.Select(y => y.FieldNameID).ToList();
                var identF = identGroup.Select(y => y.FieldNameID).ToList();
                var cmF = cmGroup.Select(y => y.FieldNameID).ToList();
                var valOthF = valOthGroup.Select(y => (int)y.ValueOtherFieldID).ToList();
                var valF = valGroup.Select(y => (int)y.MaxFieldNameID).ToList();
                casF.AddRange(prefF);
                casF.AddRange(phraseF);
                casF.AddRange(noteF);
                casF.AddRange(identF);
                casF.AddRange(cmF);
                casF.AddRange(valOthF);
                casF.AddRange(valF);

                var totFieldNames = context.Library_FieldNames.AsNoTracking().Where(x => casF.Contains(x.FieldNameID)).Select(y => new { y.FieldNameID, y.FieldName }).ToList();

                var casids = casIds.Select(y => y.CASID).Distinct().ToList();
                var libCas = context.Library_CAS.AsNoTracking().Where(x => casids.Contains(x.CASID)).Select(y => new { y.CASID, y.CAS }).ToList();

                var getCASData = (from d1 in casGroup
                                  join d2 in casIds
                                  on new { a = d1.FieldNameID, b = d1.RowID }
                                  equals new { a = d2.FieldNameID, b = d2.RowID }
                                  join d3 in totFieldNames
                                  on d1.FieldNameID equals d3.FieldNameID
                                  join d4 in libCas
                                  on d2.CASID equals d4.CASID
                                  select new IRows
                                  {
                                      ID = d2.Log_QSxxx_CAS_ID,
                                      RowID = d2.RowID,
                                      FieldName = d3.FieldName,
                                      Value = d4.CAS,
                                      Type = "CAS"
                                  }).Distinct().ToList();

                var prefids = prefIds.Select(y => y.ChemicalNameID).Distinct().ToList();
                var libPref = context.Library_ChemicalNames.AsNoTracking().Where(x => prefids.Contains(x.ChemicalNameID)).Select(y => new { y.ChemicalNameID, y.ChemicalName }).ToList();

                var getPrefData = (from d1 in prefGroup
                                   join d2 in prefIds
                                   on new { a = d1.FieldNameID, b = d1.RowID }
                                   equals new { a = d2.FieldNameID, b = d2.RowID }
                                   join d3 in totFieldNames
                                   on d1.FieldNameID equals d3.FieldNameID
                                   join d4 in libPref
                                   on d2.ChemicalNameID equals d4.ChemicalNameID
                                   select new IRows
                                   {
                                       ID = d2.Log_QSxxx_PREF_ID,
                                       RowID = d2.RowID,
                                       FieldName = d3.FieldName,
                                       Value = d4.ChemicalName,
                                       Type = "PREF"
                                   }).Distinct().ToList();

                var phraseids = phraseIds.Select(y => y.PhraseID).Distinct().ToList();
                var libPhrase = context.Library_Phrases.AsNoTracking().Where(x => phraseids.Contains(x.PhraseID)).Select(y => new { y.PhraseID, y.Phrase }).ToList();


                var getPhraseData = (from d1 in phraseGroup
                                     join d2 in phraseIds
                                     on new { a = d1.FieldNameID, b = d1.RowID }
                                     equals new { a = d2.FieldNameID, b = d2.RowID }
                                     join d3 in totFieldNames
                                     on d1.FieldNameID equals d3.FieldNameID
                                     join d4 in libPhrase
                                     on d2.PhraseID equals d4.PhraseID
                                     select new IRows
                                     {
                                         ID = d2.Log_QSxxx_Phrases_ID,
                                         RowID = d2.RowID,
                                         FieldName = d3.FieldName,
                                         Value = d4.Phrase,
                                         Type = "PHRASES"
                                     }).Distinct().ToList();

                var identids = identIds.Select(y => y.IdentifierValueID).Distinct().ToList();
                var libIdent = context.Library_Identifiers.AsNoTracking().Where(x => identids.Contains(x.IdentifierValueID)).Select(y => new { y.IdentifierValueID, y.IdentifierValue }).ToList();

                var getIdentData = (from d1 in identGroup
                                    join d2 in identIds
                                    on new { a = d1.FieldNameID, b = d1.RowID }
                                    equals new { a = d2.FieldNameID, b = d2.RowID }
                                    join d3 in totFieldNames
                                    on d1.FieldNameID equals d3.FieldNameID
                                    join d4 in libIdent
                                    on d2.IdentifierValueID equals d4.IdentifierValueID
                                    select new IRows
                                    {
                                        ID = d2.Log_QSxxx_Identifiers_ID,
                                        RowID = d2.RowID,
                                        FieldName = d3.FieldName,
                                        Value = d4.IdentifierValue,
                                        Type = "Identifier"
                                    }).Distinct().ToList();

                var notesids = notesIds.Select(y => y.NoteCodeID).Distinct().ToList();
                var libnote = context.Library_NoteCodes.AsNoTracking().Where(x => notesids.Contains(x.NoteCodeID)).Select(y => new { y.NoteCodeID, y.NoteCode }).ToList();

                var getNoteData = (from d1 in noteGroup
                                   join d2 in notesIds
                                   on new { a = d1.FieldNameID, b = d1.RowID }
                                   equals new { a = d2.FieldNameID, b = d2.RowID }
                                   join d3 in totFieldNames
                                   on d1.FieldNameID equals d3.FieldNameID
                                   join d4 in libnote
                                   on d2.NoteCodeID equals d4.NoteCodeID
                                   select new IRows
                                   {
                                       ID = d2.Log_QSxxx_NotesID,
                                       RowID = d2.RowID,
                                       FieldName = d3.FieldName,
                                       Value = d4.NoteCode,
                                       Type = "Notes"
                                   }).Distinct().ToList();

                var cmids = cmIds.Select(y => y.SubfieldID).Distinct().ToList();
                var libcm = context.Library_SubfieldCharacters.AsNoTracking().Where(x => cmids.Contains(x.SubfieldID)).Select(y => new { y.SubfieldID, y.Character }).ToList();

                var getCMData = (from d1 in cmGroup
                                 join d2 in cmIds
                                 on new { a = d1.FieldNameID, b = d1.RowID }
                                 equals new { a = d2.FieldNameID, b = d2.RowID }
                                 join d3 in totFieldNames
                                 on d1.FieldNameID equals d3.FieldNameID
                                 join d4 in libcm
                                 on d2.SubfieldID equals d4.SubfieldID
                                 select new IRows
                                 {
                                     ID = d2.Log_QSxxx_ConditionalMapping_ID,
                                     RowID = d2.RowID,
                                     FieldName = d3.FieldName,
                                     Value = d4.Character,
                                     Type = "Conditional Mapping"
                                 }).Distinct().ToList();

                var getValOthData = (from d1 in valOthGroup
                                     join d2 in valOthIds
                                     on new { a = d1.ValueOtherFieldID, b = d1.RowID }
                                     equals new { a = d2.ValueOtherFieldID, b = d2.RowID }
                                     join d3 in totFieldNames
                                     on d1.ValueOtherFieldID equals d3.FieldNameID
                                     select new IRows
                                     {
                                         ID = d2.Log_QSxxx_Values_ID,
                                         RowID = d2.RowID,
                                         FieldName = d3.FieldName,
                                         Value = d2.ValueOther.ToString(),
                                         Type = "Value Other"
                                     }).Distinct().ToList();

                var getvalData = (from d1 in valGroup
                                  join d2 in valIds
                                  on new { a = d1.MaxFieldNameID, b = d1.RowID, c = d1.MinFieldNameID }
                                  equals new { a = d2.MaxFieldNameID, b = d2.RowID, c = d2.MinFieldNameID }
                                  join d3 in totFieldNames
                                  on d1.MaxFieldNameID equals d3.FieldNameID
                                  select new IRows
                                  {
                                      ID = d2.Log_QSxxx_Values_ID,
                                      RowID = d2.RowID,
                                      FieldName = d3.FieldName,
                                      Value = d2.MaxValue.ToString(),
                                      Type = "Value"
                                  }).Distinct().ToList();

                getCASData.AddRange(getPrefData);
                getCASData.AddRange(getPhraseData);
                getCASData.AddRange(getIdentData);
                getCASData.AddRange(getNoteData);
                getCASData.AddRange(getCMData);
                getCASData.AddRange(getValOthData);
                getCASData.AddRange(getvalData);


                var groupAgain = (from d1 in getCASData
                                  group d1.ID by new { d1.FieldName, d1.Value, d1.Type, d1.RowID } into g
                                  select new IRowsNew
                                  {
                                      ID = string.Join(",", g.OrderByDescending(x => x).ToList().Skip(1).ToList()),
                                      RowID = g.Key.RowID,
                                      FieldName = g.Key.FieldName,
                                      Value = g.Key.Value,
                                      Type = g.Key.Type,
                                  }).ToList();

                if (groupAgain.Any())
                {
                    return ObjCommon.ConvertToDataTable(groupAgain);
                }

                return null;
            }






        }
    }

    public class IRows
    {
        public int ID { get; set; }
        public int RowID { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

    }
    public class IRowsNew
    {
        public string ID { get; set; }
        public int RowID { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

    }
}

