﻿using System.Threading.Tasks;
using System.Windows;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using CRA_DataAccess.ViewModel;

namespace CurrentDataView.ViewModel
{
    public class ListedGroupMembers_VM : Base_ViewModel
    {
        public CommonDataGrid_ViewModel<IListedGroupMembers> comonCASGroupMembers { get; set; }
        public Visibility VisibilityGroupMembers_Loader { get; set; } = Visibility.Collapsed;
        private ObservableCollection<IListedGroupMembers> _lstGroupMembers { get; set; } = new ObservableCollection<IListedGroupMembers>();
        public ObservableCollection<IListedGroupMembers> LstGroupMembers
        {
            get { return _lstGroupMembers; }
            set
            {
                if (_lstGroupMembers != value)
                {
                    _lstGroupMembers = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IListedGroupMembers>>>(new PropertyChangedMessage<List<IListedGroupMembers>>(null, _lstGroupMembers.ToList(), "Default List"));
                }
            }
        }
        public ListedGroupMembers_VM()
        {
            comonCASGroupMembers = new CommonDataGrid_ViewModel<IListedGroupMembers>(GetListGridColumnGroupMembers(), "Group Members", "GroupMembers");
            MessengerInstance.Register<PropertyChangedMessage<IListedGroupMembers>>(this, NotifyMeGroupMembers);
            Task.Run(() => RunListedGroupMembers());
        }
        private void RunListedGroupMembers()
        {
            VisibilityGroupMembers_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityGroupMembers_Loader");
            List<IListedGroupMembers> result =_objLibraryFunction_Service.CalculateListedGroupMembers(Win_Administrator_VM.Qsid_ID);
            LstGroupMembers = new ObservableCollection<IListedGroupMembers>(result);
            NotifyPropertyChanged("LstGroupMembers");
            VisibilityGroupMembers_Loader = Visibility.Hidden;
            NotifyPropertyChanged("VisibilityGroupMembers_Loader");
        }
        private List<CommonDataGridColumn> GetListGridColumnGroupMembers()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ParentCAS", ColBindingName = "ParentCAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ChildCAS", ColBindingName = "ChildCAS", ColType = "Textbox", ColumnWidth = "200" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Name in List", ColBindingName = "ChemicalNameinList", ColType = "Textbox", ColumnWidth = "400" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Primary Name", ColBindingName = "ParentPrimaryName", ColType = "Textbox", ColumnWidth = "400" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Primary Name", ColBindingName = "ChildPrimaryName", ColType = "Textbox", ColumnWidth = "400" });
            return listColumnQsidDetail;
        }
        public IListedGroupMembers SelectedPropertyGroupMembers { get; set; }
        private void NotifyMeGroupMembers(PropertyChangedMessage<IListedGroupMembers> obj)
        {
            SelectedPropertyGroupMembers = obj.NewValue;
        }
        
    }

   
}
