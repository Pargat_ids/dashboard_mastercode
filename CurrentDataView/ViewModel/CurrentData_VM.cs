﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class CurrentData_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstCurrentData { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
           public Visibility VisibleLoderCurrentData { get; set; }
        public CurrentData_VM()
        {
            RunCurrentData();
        }

        public void RunCurrentData()
        {
            VisibleLoderCurrentData = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderCurrentData");
            if (Win_Administrator_VM.FilterCas.Any() || Win_Administrator_VM.CurrentDataTable.Columns.Count == 0)
            {
                Win_Administrator_VM.CurrentDataTable = new DataTable();
                SingleCurrentViewFunction();
            }

            lstCurrentData = Win_Administrator_VM.CurrentDataTable.Rows.Count > 0 ? new StandardDataGrid_VM("Data", Win_Administrator_VM.CurrentDataTable, Visibility.Visible) : null;
            NotifyPropertyChanged("lstCurrentData");
            VisibleLoderCurrentData = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderCurrentData");

        }
        public static void SingleCurrentViewFunction()
        {
            RowIdentifier rowIdentifier = new RowIdentifier();
            Win_Administrator_VM.CurrentDataTable = new DataTable();
            var tempRows = rowIdentifier.MakeOldDataListDharmeshForUpstreamComparing(Win_Administrator_VM.Qsid_ID, true, true, Win_Administrator_VM.FilterCas.Any() ? Win_Administrator_VM.FilterCas : null);
           
            if (tempRows.Any())
            {
                Win_Administrator_VM.CurrentDataTableWithChild = CommonFunctions.ConverttoDataTBNew(tempRows).AsEnumerable().OrderBy(x => Convert.ToInt32(x.Field<string>("rowid"))).CopyToDataTable();
                var tempRows1 = rowIdentifier.MakeOldDataListDharmesh(Win_Administrator_VM.Qsid_ID, true, true, Win_Administrator_VM.FilterCas.Any() ? Win_Administrator_VM.FilterCas : null);
                Win_Administrator_VM.CurrentDataTable = rowIdentifier.AddExtraFieldInCurrentData(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CurrentDataTable, tempRows1);
                if (Win_Administrator_VM.CurrentDataTableWithChild.Columns.Contains("ListedChild_CAS"))
                {
                    Win_Administrator_VM.CurrentDataTableWithChild.Columns.Remove("ListedChild_CAS");
                }
                if (Win_Administrator_VM.CurrentDataTableWithChild.Columns.Contains("RN"))
                {
                    Win_Administrator_VM.CurrentDataTableWithChild.Columns["RN"].SetOrdinal(0);
                }
                Win_Administrator_VM.CurrentDataTableWithChild.Columns["RowId"].SetOrdinal(1);
                Win_Administrator_VM.CurrentDataTableWithChild.Columns["CAS"].SetOrdinal(2);
                Win_Administrator_VM.CurrentDataTableWithChild.Columns["PREF"].SetOrdinal(3);
            }
            if (!Win_Administrator_VM.Qsid.ToUpper().Trim().Contains("QSNI") && !Win_Administrator_VM.Qsid.ToUpper().Trim().Contains("QSPC") && !Win_Administrator_VM.Qsid.ToUpper().Trim().Contains("QSTO") && !Win_Administrator_VM.Qsid.ToUpper().Trim().Contains("QSEC"))
            {
                Win_Administrator_VM.ParentChildDataTable = _objLibraryFunction_Service.GetDataTableParentChildGenerics(Win_Administrator_VM.CurrentDataTableWithChild); //_objLibraryFunction_Service.GetDataTableParentChildGenerics(Win_Administrator_VM.CurrentDataTable);
            }
        }

    }
    public class iColumnName
    {
        public string ColumnName { get; set; }
    }
}
