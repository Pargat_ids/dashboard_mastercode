﻿using System.Threading.Tasks;
using System.Windows;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class ParentChildRelationShip_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listParentChildRelationship { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderParentChildRel { get; set; }
        public ParentChildRelationShip_VM()
        {
            Task.Run(() => RunParentChildRelation());
        }
        private void RunParentChildRelation()
        {
            VisibleLoderParentChildRel = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderParentChildRel");
            if (Win_Administrator_VM.ParentChildTable.Columns.Count == 0)
            {
                Win_Administrator_VM.ParentChildTable = rowIdentifier.DataChildRelation(Win_Administrator_VM.ParentChildTable, Win_Administrator_VM.Qsid_ID);
            }
            
            listParentChildRelationship = new StandardDataGrid_VM("Parent Child Relationship", Win_Administrator_VM.ParentChildTable, Visibility.Visible);
            NotifyPropertyChanged("listParentChildRelationship");
            VisibleLoderParentChildRel = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderParentChildRel");
        }
    }
}

