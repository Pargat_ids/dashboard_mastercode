﻿using System;
using System.Collections.Generic;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using EntityClass;
using System.Linq;
using System.Data;

namespace CurrentDataView.ViewModel
{
    public class RowHistory_VM : Base_ViewModel
    {

        public StandardDataGrid_VM listRowHistory { get; set; }
        public Visibility VisibleLoderRowHistory { get; set; } = Visibility.Collapsed;
        public CommonFunctions objCommonFunc = new CommonFunctions();

        public RowHistory_VM()
        {
            Task.Run(() => RunRowHistory());
        }

        private void RunRowHistory()
        {
            VisibleLoderRowHistory = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderRowHistory");
            if (Win_Administrator_VM.ListRowHistory.Columns.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                Win_Administrator_VM.ListRowHistory = GetRowHistoryFromQsid(Win_Administrator_VM.Qsid_ID);
            }
            
            listRowHistory = new StandardDataGrid_VM("RowHistory", Win_Administrator_VM.ListRowHistory, Visibility.Visible);
            NotifyPropertyChanged("listRowHistory");
            VisibleLoderRowHistory = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderRowHistory");
        }
        public DataTable GetRowHistoryFromQsid(int qsid_ID)
        {
            List<Log_ADC_ModelWithSession> lst = new List<Log_ADC_ModelWithSession>();
            DataTable dtRet = new DataTable();
            var dt = objCommonFunc.DbaseQueryReturnTableSql("select distinct RowID,Status, a.TimeStamp, d.FirstName + ' ' + d.LastName as UserName, a.SessionId  " +
            " from Log_QSxxx_RowHistory a, Log_Sessions c, Library_Users d where a.SessionID = c.SessionID and c.UserName = d.VeriskID " +
            " and a.qsid_id = " + qsid_ID, Win_Administrator_VM.CommonListConn);

            using (var context = new CRAModel())
            {
                var allMap = context.Map_QSID_FieldName.AsNoTracking().Where(x => x.QSID_ID == qsid_ID && x.IsInternalOnly == false).ToList();
                var uniqueFields = context.Map_Unique_Criteria.AsNoTracking().Where(x => x.QSID_ID == qsid_ID).Select(y => y.FieldNameID).ToList();
                var listedChild_CAS = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName.ToUpper() == "LISTEDCHILD_CAS").Select(y => y.FieldNameID).FirstOrDefault();
                uniqueFields.Add(listedChild_CAS);
                var listFieldName = context.Library_FieldNames.AsNoTracking().Where(x => uniqueFields.Contains(x.FieldNameID)).ToList();
                var listUniqueRowId = dt.AsEnumerable().GroupBy(x => x.Field<int>("RowID")).Select(x => x.Key).ToList();
                var allDelimiters = context.Library_Delimiters.AsNoTracking().ToList();
                var listOperator = context.Library_Operators.AsNoTracking().ToList();
                var listUnit = context.Library_Units.AsNoTracking().ToList();
                var combineUnique = string.Join(",", uniqueFields);
                
                var logPref = LogDetails("Log_QSxxx_PREF", qsid_ID, combineUnique, listUniqueRowId, "ChemicalNameID", "Library_ChemicalNames", "ChemicalName");
                var logNotes = LogDetails("Log_QSxxx_Notes", qsid_ID, combineUnique, listUniqueRowId, "NoteCodeID", "Library_NoteCodes", "NoteCode");
                var logIdent = LogDetails("Log_QSxxx_Identifiers", qsid_ID, combineUnique, listUniqueRowId, "IdentifierValueID", "Library_Identifiers", "IdentifierValue");
                var logPhrase = LogDetails("Log_QSxxx_Phrases", qsid_ID, combineUnique, listUniqueRowId, "PhraseID", "Library_Phrases", "Phrase");
                var listCas = LogDetails("Log_QSxxx_CAS", qsid_ID, combineUnique, listUniqueRowId, "CASID", "Library_CAS", "CAS");
                var listConditionalMapping = LogDetails("Log_QSxxx_ConditionalMapping", qsid_ID, combineUnique, listUniqueRowId, "SubfieldID", "Library_SubfieldCharacters", "Character");

                var logVal = (from d1 in context.Log_QSxxx_Values.AsNoTracking().Where(y => y.QSID_ID == qsid_ID
                               && (uniqueFields.Contains(y.MaxFieldNameID) || uniqueFields.Contains(y.MinFieldNameID) ||
                               uniqueFields.Contains(y.ValueOtherFieldID))).ToList()
                              join d2 in listUniqueRowId
                              on d1.RowID equals d2
                              select d1).ToList();

                var totRowIds = listCas.Select(y => y.RowID).Distinct().ToList();
                var prefRowIds = logPref.Select(y => y.RowID).Distinct().ToList();
                var notesRowIds = logNotes.Select(y => y.RowID).Distinct().ToList();
                var identRowIds = logIdent.Select(y => y.RowID).Distinct().ToList();
                var phraseRowIds = logPhrase.Select(y => y.RowID).Distinct().ToList();
                var CMRowIds = listConditionalMapping.Select(y => y.RowID).Distinct().ToList();
                var valRowIds = logVal.Select(y => y.RowID).Distinct().ToList();

                totRowIds.AddRange(prefRowIds);
                totRowIds.AddRange(notesRowIds);
                totRowIds.AddRange(identRowIds);
                totRowIds.AddRange(phraseRowIds);
                totRowIds.AddRange(CMRowIds);
                totRowIds.AddRange(valRowIds);

                var result = from d1 in dt.AsEnumerable()
                      join d2 in totRowIds
                      on d1.Field<int>("RowID") equals d2
                      select d1;

                if (result.Any())
                {
                    dt = result.CopyToDataTable();
                }

                foreach (DataRow itemRow in dt.Rows)
                {
                    var RowId = Convert.ToInt32(itemRow.Field<int>("RowID"));
                    var sessId = Convert.ToInt32(itemRow.Field<int>("SessionID"));
                    var uName = itemRow.Field<string>("UserName");
                    var tStamp = itemRow.Field<DateTime>("TimeStamp");
                    var status = itemRow.Field<string>("Status");
                    List<Log_ADC_ModelWithSession> listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_CAS = listCas.Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    foreach (var itemUniqueField in originalList_CAS)
                    {
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField.FieldNameID, RowID = RowId, Value = itemUniqueField.ValueName, SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }
                    lst.AddRange(listADCChanges);
                    listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_CM = listConditionalMapping.Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    foreach (var itemUniqueField in originalList_CM)
                    {
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField.FieldNameID, RowID = RowId, Value = itemUniqueField.ValueName, SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }
                    lst.AddRange(listADCChanges);
                    listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_Pref = logPref.AsEnumerable().Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    var uniqueList_Pref = originalList_Pref.GroupBy(x => x.FieldNameID).Select(x => x.Key).ToList();
                    foreach (var itemUniqueField in uniqueList_Pref)
                    {
                        var delId = allMap.Where(x => x.FieldNameID == itemUniqueField).Select(y => y.ChemicalNameDelimiterID).FirstOrDefault();
                        string del = string.Empty;
                        if (delId != null)
                        {
                            del = allDelimiters.Where(x => x.DelimiterID == delId).Select(y => y.Delimiter).FirstOrDefault();
                        }
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField, RowID = RowId, Value = Get_DataWithSortOrder(originalList_Pref, itemUniqueField, del), SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }

                    lst.AddRange(listADCChanges);
                    listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_Notes = logNotes.Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    var uniqueList_Notes = originalList_Notes.GroupBy(x => x.FieldNameID).Select(x => x.Key).ToList();
                    foreach (var itemUniqueField in uniqueList_Notes)
                    {
                        var delId = allMap.Where(x => x.FieldNameID == itemUniqueField).Select(y => y.MultiValuePhraseDelimiterID).FirstOrDefault();
                        string del = string.Empty;
                        if (delId != null)
                        {
                            del = allDelimiters.Where(x => x.DelimiterID == delId).Select(y => y.Delimiter).FirstOrDefault();
                        }
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField, RowID = RowId, Value = Get_DataWithSortOrder(originalList_Notes, itemUniqueField, del), SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }

                    lst.AddRange(listADCChanges);
                    listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_Ident = logIdent.Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    var uniqueList_Ident = originalList_Ident.GroupBy(x => x.FieldNameID).Select(x => x.Key).ToList();
                    foreach (var itemUniqueField in uniqueList_Ident)
                    {
                        var delId = allMap.Where(x => x.FieldNameID == itemUniqueField).Select(y => y.MultiValueIdentifierDelimiterID).FirstOrDefault();
                        string del = string.Empty;
                        if (delId != null)
                        {
                            del = allDelimiters.Where(x => x.DelimiterID == delId).Select(y => y.Delimiter).FirstOrDefault();
                        }
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField, RowID = RowId, Value = Get_DataWithSortOrder(originalList_Ident, itemUniqueField, del), SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }
                    lst.AddRange(listADCChanges);
                    listADCChanges = new List<Log_ADC_ModelWithSession>();
                    var originalList_Phrase = logPhrase.Where(p => p.RowID == RowId && p.SessionID == sessId).ToList();
                    var uniqueList_Phrase = originalList_Phrase.GroupBy(x => x.FieldNameID).Select(x => x.Key).ToList();
                    foreach (var itemUniqueField in uniqueList_Phrase)
                    {
                        var delId = allMap.Where(x => x.FieldNameID == itemUniqueField).Select(y => y.MultiValuePhraseDelimiterID).FirstOrDefault();
                        string del = string.Empty;
                        if (delId != null)
                        {
                            del = allDelimiters.Where(x => x.DelimiterID == delId).Select(y => y.Delimiter).FirstOrDefault();
                        }
                        listADCChanges.Add(new Log_ADC_ModelWithSession { FieldID = itemUniqueField, RowID = RowId, Value = Get_DataWithSortOrder(originalList_Phrase, itemUniqueField, del), SessionID = sessId, UserName = uName, TimeStamp = tStamp, Status = status });
                    }
                    lst.AddRange(listADCChanges);
                    var listThresMax = GetData_Log_Qsxxx_Value_ADWithoutStatus(logVal, qsid_ID, RowId, sessId, listOperator, allMap, listUnit);
                    lst.AddRange(listThresMax);
                }

                var totFieldName = lst.Select(y => y.FieldID).Distinct().ToList();
                var final1 = (from i in lst
                              group i by new { i.SessionID, i.RowID } into g
                              select new
                              {
                                  keyField = g.Key.RowID + "/" + g.Key.SessionID,
                                  items = g.Select(y => new { y.UserName, y.TimeStamp, y.Status, y.RowID, y.SessionID, y.FieldID, y.Value }).ToList(),

                              }).ToList();
                List<Dictionary<object, object>> lstFinal = new List<Dictionary<object, object>>();
                for (int i = 0; i < final1.Count(); i++)
                {
                    Dictionary<object, object> newLst = new Dictionary<object, object> { { "RowIdSess", final1.ElementAt(i).keyField } };
                    newLst.Add("RowID", Convert.ToInt32(final1.ElementAt(i).items[0].RowID));
                    for (int j = 0; j < totFieldName.Count; j++)
                    {
                        var fieldId = totFieldName[j];
                        var fieldName = listFieldName.Where(x => x.FieldNameID == fieldId).Select(x => x.FieldName).FirstOrDefault();
                        if (!newLst.Keys.Contains(fieldName))
                        {
                            var val = final1.ElementAt(i).items.Where(y => y.FieldID == fieldId).FirstOrDefault();
                            if (val == null)
                            {
                                newLst.Add(fieldName, null);
                            }
                            else
                            {
                                newLst.Add(fieldName, val.Value);
                            }
                        }
                    }
                    newLst.Add("Status", final1.ElementAt(i).items[0].Status);
                    newLst.Add("SessionID", final1.ElementAt(i).items[0].SessionID);
                    newLst.Add("TimeStamp", Convert.ToDateTime(final1.ElementAt(i).items[0].TimeStamp));
                    newLst.Add("UserName", final1.ElementAt(i).items[0].UserName);
                    lstFinal.Add(newLst);
                }
                if (lstFinal.Any())
                {
                    dtRet = objCommonFunc.ConverttoDataTB(lstFinal).AsEnumerable().OrderBy(x => x.Field<string>("RowIdSess")).CopyToDataTable();
                    dtRet.Columns.Remove("RowIdSess");
                }
                DataTable dtCloned = dtRet.Clone();
                if (dtCloned.Columns.Count > 0)
                {
                    dtCloned.Columns["SessionID"].DataType = typeof(Int32);
                    dtCloned.Columns["TimeStamp"].DataType = typeof(DateTime);
                    dtCloned.Columns["RowId"].DataType = typeof(Int32);

                    foreach (DataRow row in dtRet.Rows)
                    {
                        dtCloned.ImportRow(row);
                    }
                    if (dtCloned.Rows.Count > 0)
                    {
                        dtCloned = dtCloned.AsEnumerable().OrderBy(x => x.Field<int>("RowId")).CopyToDataTable();
                    }
                }
                return dtCloned;
            }
        }

        public string Get_DataWithSortOrder(List<ILogDet> list_Log_Pref, int fieldnameId, string del)
        {
            return string.Join(del, list_Log_Pref.Where(p => p.FieldNameID == fieldnameId).OrderBy(x => x.SortOrder).Select(x => x.ValueName).Distinct().ToList());
        }
        public List<Log_ADC_ModelWithSession> GetData_Log_Qsxxx_Value_ADWithoutStatus(List<Log_QSxxx_Values> lst, int qsid_ID, int rowid, int sessId, List<Library_Operators> listOperator, List<Map_QSID_FieldName> lstMap, List<Library_Units> listUnit)
        {
            List<Log_ADC_ModelWithSession> listADC_Threshold = new List<Log_ADC_ModelWithSession>();
            var listValues = lst.Where(p => p.RowID == rowid && p.SessionID == sessId).ToList();
            foreach (var item in listValues)
            {
                string value = string.Empty;
                if (item.ValueOtherFieldID != null)
                {
                    value = item.ValueOther.ToString();
                    listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.ValueOtherFieldID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                }

                if (item.MaxFieldNameID == item.MinFieldNameID)
                {
                    if (item.MaxFieldNameID != null || item.MinFieldNameID != null)
                    {
                        value = string.Empty;
                        if (item.MinValOpFieldID == item.MinFieldNameID)
                        {
                            value = listOperator.Where(x => x.OperatorID == item.MinValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                        }
                        value += item.MinValue;
                        int? rangeOpID = lstMap.Where(x => x.ValueFieldContainsRanges == true).Select(x => x.RangeDelimiterID).FirstOrDefault();
                        value += rangeOpID != null ? listOperator.Where(x => x.OperatorID == rangeOpID).Select(x => x.Operator).FirstOrDefault() : "";
                        if (item.MaxValOpFieldID == item.MaxFieldNameID)
                        {
                            value += listOperator.Where(x => x.OperatorID == item.MaxValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                        }
                        value += item.MaxValue;
                        if (item.MaxFieldNameID == item.UnitsFieldID)
                        {
                            value += listUnit.Where(x => x.UnitID == item.UnitID).FirstOrDefault();
                        }
                        listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.MaxFieldNameID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                    }
                }
                else
                {
                    if (item.MinFieldNameID != null)
                    {
                        value = string.Empty;
                        if (item.MinValOpFieldID == item.MinFieldNameID)
                        {
                            value = listOperator.Where(x => x.OperatorID == item.MinValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                        }
                        value += item.MinValue;
                        if (item.MinFieldNameID == item.UnitsFieldID)
                        {
                            value += listUnit.Where(x => x.UnitID == item.UnitID).FirstOrDefault();
                        }
                        listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.MinFieldNameID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                    }
                    if (item.MaxFieldNameID != null)
                    {
                        value = string.Empty;
                        if (item.MaxValOpFieldID == item.MaxFieldNameID)
                        {
                            value = listOperator.Where(x => x.OperatorID == item.MaxValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                        }
                        value += item.MaxValue;
                        if (item.MaxFieldNameID == item.UnitsFieldID)
                        {
                            value += listUnit.Where(x => x.UnitID == item.UnitID).FirstOrDefault();
                        }
                        listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.MaxFieldNameID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                    }
                    if (item.MinValOpFieldID != null)
                    {
                        value = string.Empty;
                        if (item.MinValOpFieldID != item.MinFieldNameID)
                        {
                            value = listOperator.Where(x => x.OperatorID == item.MinValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                            listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.MinValOpFieldID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                        }
                    }
                    if (item.MaxValOpFieldID != null)
                    {
                        value = string.Empty;
                        if (item.MaxValOpFieldID != item.MaxFieldNameID)
                        {
                            value = listOperator.Where(x => x.OperatorID == item.MaxValOpFieldID).Select(x => x.Operator).FirstOrDefault();
                            listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.MaxValOpFieldID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                        }
                    }
                    if (item.UnitsFieldID != null)
                    {
                        value = string.Empty;
                        if (item.MinFieldNameID != item.UnitsFieldID && item.MaxFieldNameID != item.UnitsFieldID)
                        {
                            value += listUnit.Where(x => x.UnitID == item.UnitID).Select(x => x.Unit).FirstOrDefault();
                            listADC_Threshold.Add(new Log_ADC_ModelWithSession { FieldID = item.UnitsFieldID, RowID = item.RowID, Value = value, SessionID = item.SessionID });
                        }
                    }
                }
            }
            return listADC_Threshold;
        }
        private List<ILogDet> LogDetails(string tableName, int qsid_ID, string combineUnique, List<int> listUniqueRowId, string fieldName, string LibraryTable, string libraryColumn)
        {
            var result = objCommonFunc.DbaseQueryReturnSqlList<ILogDet>("Select a.RowID, a.SessionID,a.FieldNameID, a.SortOrder, a." + fieldName + " as ValueID, b." + libraryColumn + " as ValueName from " + tableName +
                " a , " + LibraryTable + " b where a.QSID_ID =" + qsid_ID + " and a.FieldNameId in (" + combineUnique + ") and a." + fieldName + " = b." + fieldName, Win_Administrator_VM.CommonListConn);
            return (from d1 in result
                    join d2 in listUniqueRowId
                    on d1.RowID equals d2
                    select d1).ToList();
        }
    }
    public class iTypeName
    {
        public string TypeName { get; set; }
    }
    public class Log_ADC_ModelWithSession
    {
        public int RowID { get; set; }
        public int? FieldID { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public int? SessionID { get; set; }
        public DateTime TimeStamp { get; set; }
    }

    public class ILogDet
    {
        public int RowID { get; set; }
        public int? SessionID { get; set; }

        public int FieldNameID { get; set; }
        public int SortOrder { get; set; }
        public int ValueID { get; set; }
        public string ValueName { get; set; }
    }
}
