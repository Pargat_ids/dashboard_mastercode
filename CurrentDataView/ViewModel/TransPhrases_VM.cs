﻿using System;
using System.Collections.Generic;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Linq;

namespace CurrentDataView.ViewModel
{
    public class TransPhrases_VM : Base_ViewModel
    {
        private readonly CommonFunctions objCommon = new CommonFunctions();
        public StandardDataGrid_VM listTransPhrases { get; set; }

        private RowIdentifier objDataDictionary = new RowIdentifier();
        public Visibility VisibleLoderTransPhrases { get; set; }
        public TransPhrases_VM()
        {
            Task.Run(() => RunTransPhrases());
        }
        private void RunTransPhrases()
        {
            VisibleLoderTransPhrases = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderTransPhrases");
            if (Win_Administrator_VM.dtTransPhrases.Columns.Count == 0)
            {
                var dt = objCommon.DbaseQueryReturnTableSql("select distinct b.Phrase, c.LanguageName, a.TranslatedPhraseGroupID as GroupID" +
                    " from Map_TranslatedPhrases a inner join library_phrases b on a.PhraseID_CRA = b.PhraseID " +
                    " inner join Library_Languages c on b.LanguageID = c.LanguageID " +
                    " where TranslatedPhraseGroupID in (select  distinct d.TranslatedPhraseGroupID from " +
                    " QSxxx_Phrases a inner join Map_QSID_FieldName c " +
                    " on a.QSID_ID = c.QSID_ID and a.FieldNameID = c.FieldNameID " +
                    " inner join Map_TranslatedPhrases d on a.PhraseID = d.PhraseID_CRA " +
                    " where a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and c.IsPhraseField = 1 and c.LanguageID = 1) order by GroupID ", Win_Administrator_VM.CommonListConn);

                var totGroup = dt.AsEnumerable().Select(y => y.Field<Int32>("GroupID")).Distinct().ToList();
                var totLang = dt.AsEnumerable().Select(y => y.Field<string>("LanguageName")).Distinct().ToList();

                List <Dictionary<object, object>> temp = objCommon.DataTableToDynamic(dt);
                List<Dictionary<object, object>> lstFinal = new List<Dictionary<object, object>>();
                for (int i = 0; i < totGroup.Count; i++)
                {
                    var grpId = totGroup[i];
                    Dictionary<object, object> newLst = new Dictionary<object, object> { { "GroupID", grpId } };
                    for (int j = 0; j < totLang.Count(); j++)
                    {
                        var val = dt.AsEnumerable().Where(x=> x.Field<Int32>("GroupID") == grpId && x.Field<string>("LanguageName") == totLang[j].ToString()).FirstOrDefault();
                        if (val != null)
                        {
                            newLst.Add(totLang[j].ToString(), val.Field<string>("Phrase"));
                        }
                        else
                        {
                            newLst.Add(totLang[j].ToString(), "");
                        }
                    }

                    lstFinal.Add(newLst);
                }
                Win_Administrator_VM.dtTransPhrases = objCommon.ConverttoDataTB(lstFinal).AsEnumerable().OrderBy(x => Convert.ToInt32(x.Field<string>("GroupID"))).CopyToDataTable();

            }
            
            listTransPhrases = new StandardDataGrid_VM("Translated Phrases", Win_Administrator_VM.dtTransPhrases, Visibility.Visible);
            NotifyPropertyChanged("listTransPhrases");
            VisibleLoderTransPhrases = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderTransPhrases");

        }
    }
}
