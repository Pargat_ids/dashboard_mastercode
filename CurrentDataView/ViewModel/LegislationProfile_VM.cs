﻿using CurrentDataView.Library;
using EntityClass;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using CRA_DataAccess;
using VersionControlSystem.Model.ApplicationEngine;
using DataGridFilterLibrary.Querying;
using System.Data;
using System.Linq.Dynamic;
using System.Windows.Controls;
using CRA_DataAccess.ViewModel;

namespace CurrentDataView.ViewModel
{
    public class LegislationProfile_VM : Base_ViewModel
    {
        private int currentPid { get; set; }
        public ICommand CommandCopyCellData { get; set; }
        public ICommand CommandRefreshItemDetailGrid { get; private set; }
        public ICommand CommandDataGridSorting { get; set; }
        public ICommand CommandDataGridClearSorting { get; set; }
        public ICommand CommandDataGridClearFilter { get; private set; }
        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public ICommand CommmandChangeVisiblityListColNull { get; set; }
        public ICommand CommandOnColSelectedChange { get; set; }
        public ICommand CommandOnColSelectedChangeNull { get; set; }
        public ICommand CommandBeginEdit { get; set; }
        public ICommand btnCheckBox { get; set; }
        public ICommand commandSave { get; set; }
        public ICommand commandDelete { get; set; }
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        public ICommand CommGenerateAccessFile { get; set; }
        public ICommand CommGenerateExcelFile { get; set; }
        public Visibility visibleHeaderSectionText { get; set; } = Visibility.Visible;
        public Visibility visibleHeaderURL { get; set; } = Visibility.Visible;
        public Visibility visibleHeaderDateFirstAdded { get; set; } = Visibility.Visible;
        public Visibility visibleHeaderDateLastUpdated { get; set; } = Visibility.Visible;
        public Visibility VisibleLoderCurrentData { get; set; }
        public Visibility IsSearchCriteriaVisible { get; set; } = Visibility.Collapsed;

        private bool IsSave { get; set; }
        private bool IsNavigationProcess = false;
        public bool listAllColVisible { get; set; } = false;
        public bool listAllColVisibleNull { get; set; } = false;
        private bool _isCheckedFilterCaseSenstive { get; set; } = false;

        private int _defaultColumnBinding { get; set; } = 250;
        private int currpage { get; set; }
        int pageIndex = 1;
        private int numberOfRecPerPage = 25;


        public string SortingExpression { get; set; }
        private string lastSortMemberPath;
        public string defaultSortedBy { get; set; }
        public string lblSaveupdate { get; set; }
        public string lblCurrentCellInfo { get; set; }
        public string lblContent { get; set; }
        public string lblTotRecords { get; set; }
        public string _searchCriteria { get; set; }

        private RowIdentifier objCurrentView = new RowIdentifier();
        CRAModel context = new CRAModel();
        private readonly CommonFunctions objCommon = new CommonFunctions();
        private LegislationProfileTable _SelectedListDataItem { get; set; }
        private enum PagingMode { First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public ObservableCollection<LegislationProfileTable> _ListDataTest { get; set; } = new ObservableCollection<LegislationProfileTable>();
        public ObservableCollection<LegislationProfileTable> ListDataTest
        {
            get { return _ListDataTest; }
            set
            {
                if (_ListDataTest != value)
                {
                    _ListDataTest = value;
                    NotifyPropertyChanged("ListDataTest");
                    if (currentPid != 0)
                    {
                        SelectedListDataItem = ListDataTest.Where(x => x.ID == currentPid).FirstOrDefault();
                        currentPid = 0;
                    }
                }
            }
        }


        //public ObservableCollection<LegislationProfileTable> ListDataTest { get; set; } = new ObservableCollection<LegislationProfileTable>();
        private List<LegislationProfileTable> myList { get; set; } = new List<LegislationProfileTable>();
        private List<LegislationProfileTable> myListPreserve { get; set; } = new List<LegislationProfileTable>();
        private List<LegislationProfileTable> myListTotal { get; set; } = new List<LegislationProfileTable>();
        public ObservableCollection<SectionProfile> SectionNew { get; set; } = new ObservableCollection<SectionProfile>();
        public ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM> ListAllColumnDG { get; set; } = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
        public ObservableCollection<ColumnSelectionDataGrid> ListAllColumnDGNull { get; set; } = new ObservableCollection<ColumnSelectionDataGrid>();
        public List<filterList> _listfilter { get; set; } = new List<filterList>();
        private List<string> listSortingMemberDirection = new List<string>();
        public ObservableCollection<SelectListLegislation> ListLegislation { get; set; }
        public ObservableCollection<selectedListItemCombo> ListComboxItem { get; set; } = new ObservableCollection<selectedListItemCombo>();
        private ListSortDirection? lastSortDirection;
        public DataGridLength DGColumnWidth { get; set; } = new DataGridLength(250);
        public QueryController _queryController { get; set; }
        private List<SectionProfile> lstSectionProfile { get; set; }
        private DataTable dtDefault; // Access Export and Excel Export Variable
        private SelectListLegislation _SelectedListLegislation;
        public selectedListItemCombo _selectedItemCombo { get; set; }

        public SelectListLegislation SelectedListLegislation
        {
            get { return _SelectedListLegislation; }
            set
            {
                _SelectedListLegislation = value;
                NotifyPropertyChanged("SelectedListLegislation");
                lblSaveupdate = string.Empty;
                NotifyPropertyChanged("lblSaveupdate");
                ListDataTest = new ObservableCollection<LegislationProfileTable>();
                BindGridWithSource(_SelectedListLegislation.LG_ID);
            }
        }
        private void CallbackExecute(LegislationProfileTable obj)
        {

        }
        private bool CommandDataGridClearFilterCanExecute(object obj)
        {
            return true;
        }


        private void CommandRefreshItemDetailGridExecute(object obj)
        {
            BindGridWithSource(_SelectedListLegislation.LG_ID);
            //MessengerInstance.Send<NotificationMessageAction<LegislationProfileTable>>(new NotificationMessageAction<LegislationProfileTable>("notification message", CallbackExecute));
        }

        public void CommandDataGridClearFilterExecute(object sender)
        {
            ListAllColumnDGNull.ToList().ForEach(x =>
            {
                x.IsNotNull = false;
                x.IsNull = false;
            });
            NotifyPropertyChanged("ListAllColumnDGNull");
            queryNull = new List<string>();
            CommandOnColSelectedChangeNullExecute(null);
            ExecuteClearFilter();
        }

        private void CommandDataGridSortingExecute(object obj)
        {
            Sorting((DataGridSortingEventArgs)obj);
        }
        public void Sorting(DataGridSortingEventArgs e)
        {
            e.Handled = true;

            if (e.Column.SortMemberPath == lastSortMemberPath)
            {
                if (lastSortDirection == ListSortDirection.Ascending)
                    e.Column.SortDirection = ListSortDirection.Descending;
                else
                    e.Column.SortDirection = ListSortDirection.Ascending;
            }
            else
                e.Column.SortDirection = ListSortDirection.Ascending;

            string dir = (e.Column.SortDirection == ListSortDirection.Ascending) ? "ASC" : "DESC";
            lastSortDirection = e.Column.SortDirection;
            lastSortMemberPath = e.Column.SortMemberPath;
            if (!string.IsNullOrEmpty(lastSortMemberPath))
            {
                var itemRemoved = listSortingMemberDirection.Where(x => x.Contains(lastSortMemberPath)).FirstOrDefault();
                listSortingMemberDirection.Remove(itemRemoved);
            }
            listSortingMemberDirection.Add(lastSortMemberPath + " " + dir);
            SortDefaultList();
            SortingExpression = "Sort By: " + String.Join(", ", listSortingMemberDirection.ToArray());
            NotifyPropertyChanged("SortingExpression");

        }

        private void SortDefaultList()
        {
            try
            {
                var orderByString = String.Join(", ", listSortingMemberDirection.ToArray());
                myList = myList.AsQueryable().OrderBy(orderByString).ToList();
                Navigate(0);
            }
            catch { }
        }
        private bool CommandDataGridClearSortingCanExecute(object obj)
        {
            bool IsRemoveSorting = false;
            if (listSortingMemberDirection.Count > 0)
            {
                IsRemoveSorting = true;
            }
            return IsRemoveSorting;
        }

        private void CommandDataGridClearSortingExecute(object obj)
        {
            listSortingMemberDirection = new List<string>();
            SortingExpression = string.Empty;
            NotifyPropertyChanged("SortingExpression");
            myList = myListTotal.Select(x => x).ToList();
            //Navigate(0);
            if (CustomQueryController != null && CustomQueryController.ColumnFilterData != null)
            {
                GetFilterQuery();
            }
        }
        private void CommandCopyCellDataToClipboardExecute(object obj)
        {
            if (_currentCellData.Column != null)
            {
                if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                {
                    var text = string.Empty;
                    text = ((System.Windows.Controls.TextBlock)_currentCellData.Column.GetCellContent(_currentCellData.Item)).Text.ToString();
                    System.Windows.Clipboard.SetDataObject(text);
                }
            }
        }


        public LegislationProfileTable SelectedListDataItem
        {
            get
            {
                return _SelectedListDataItem;
            }
            set
            {
                if (value != _SelectedListDataItem)
                {
                    _SelectedListDataItem = value;
                    NotifyPropertyChanged("SelectedListDataItem");
                }
            }
        }

        public LegislationProfile_VM()
        {
            Task.Run(() => InitialStep());

            commandSave = new RelayCommand(insertBtn, CommandFirstCanExecute);
            commandDelete = new RelayCommand(deleteBtn, CommandFirstCanExecute);
            btnCheckBox = new RelayCommand(chkboxExecute, CommandFirstCanExecute);

            // paging and export
            commandFirst = new RelayCommand(CommandFirstExecute, CommandFirstCanExecute);
            commandPrev = new RelayCommand(CommandPrevExecute, CommandFirstCanExecute);
            commandNext = new RelayCommand(CommandNextExecute, CommandFirstCanExecute);
            commandLast = new RelayCommand(CommandLastExecute, CommandFirstCanExecute);
            defaultSortedBy = "SectionText";
            CommGenerateAccessFile = new RelayCommand(CommandAccessExport, CommandFirstCanExecute);
            CommGenerateExcelFile = new RelayCommand(CommandExcelExport, CommandFirstCanExecute);
            CommandCopyCellData = new RelayCommand(CommandCopyCellDataToClipboardExecute, CommandDataGridClearFilterCanExecute);
            CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute, CommandFirstCanExecute);
            CommmandChangeVisiblityListColNull = new RelayCommand(CommmandChangeVisiblityListColNullExecute, CommandFirstCanExecute);
            CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute, CommandFirstCanExecute);
            CommandOnColSelectedChangeNull = new RelayCommand(CommandOnColSelectedChangeNullExecute, CommandFirstCanExecute);
            CommandDataGridClearSorting = new RelayCommand(CommandDataGridClearSortingExecute, CommandDataGridClearSortingCanExecute);
            CommandDataGridSorting = new RelayCommand(CommandDataGridSortingExecute, CommandDataGridClearFilterCanExecute);
            CommandRefreshItemDetailGrid = new RelayCommand(CommandRefreshItemDetailGridExecute, CommandDataGridClearFilterCanExecute);
            CommandDataGridClearFilter = new RelayCommand(CommandDataGridClearFilterExecute, CommandDataGridClearFilterCanExecute);
            CommandBeginEdit = new RelayCommand(CommandBeginEditStart, CommandDataGridClearFilterCanExecute);
            CustomQueryController = new QueryController();
            CustomQueryController.FilteringStarted += CustomQueryController_FilteringStarted;

        }
        private void chkboxExecute(object obj)
        {
            if (SelectedListDataItem != null)
            {
                ListDataTest.AsEnumerable().ToList().ForEach(x =>
                {
                    if (x.ID == SelectedListDataItem.ID)
                    {
                        x.IsEdit = x.IsEdit;
                    }
                    else
                    {
                        x.IsEdit = false;
                    }
                });
            }
        }
        public bool IsCheckedFilterCaseSenstive
        {
            get { return _isCheckedFilterCaseSenstive; }
            set
            {
                if (value != _isCheckedFilterCaseSenstive)
                {
                    _isCheckedFilterCaseSenstive = value;
                }
            }
        }
        public void CommandBeginEditStart(object obj)
        {
            var dataGriDBeginEditEventArgs = (DataGridBeginningEditEventArgs)obj;
            if (!SelectedListDataItem.IsEdit && SelectedListDataItem.ID != 0)
            {
                dataGriDBeginEditEventArgs.Cancel = true;
            }
        }
        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            listAllColVisible = !listAllColVisible;
            NotifyPropertyChanged("listAllColVisible");
        }
        private void CommmandChangeVisiblityListColNullExecute(object obj)
        {
            listAllColVisibleNull = !listAllColVisibleNull;
            NotifyPropertyChanged("listAllColVisibleNull");
        }
        public int defaultColumnWidthBinding
        {
            get { return _defaultColumnBinding; }
            set
            {
                if (_defaultColumnBinding != value)

                {
                    _defaultColumnBinding = value;
                    NotifyPropertyChanged("defaultColumnWidthBinding");
                    DGColumnWidth = new DataGridLength(_defaultColumnBinding);
                    NotifyPropertyChanged("DGColumnWidth");

                }
            }
        }

        public void BindListAllColumn()
        {

            var list = new List<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>();
            list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { ColumnName = "SectionText", IsSelected = true });
            list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { ColumnName = "URL", IsSelected = true });
            list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { ColumnName = "DateFirstAdded", IsSelected = true });
            list.Add(new VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM { ColumnName = "DateLastUpdated", IsSelected = true });

            var listNull = new List<ColumnSelectionDataGrid>();
            listNull.Add(new ColumnSelectionDataGrid { ColumnName = "SectionText", IsNull = false, IsNotNull = false });
            listNull.Add(new ColumnSelectionDataGrid { ColumnName = "URL", IsNull = false, IsNotNull = false });
            listNull.Add(new ColumnSelectionDataGrid { ColumnName = "DateFirstAdded", IsNull = false, IsNotNull = false });
            listNull.Add(new ColumnSelectionDataGrid { ColumnName = "DateLastUpdated", IsNull = false, IsNotNull = false });

            ListAllColumnDG = new ObservableCollection<VersionControlSystem.Model.ViewModel.ColumnSelectionDataGrid_VM>(list);
            NotifyPropertyChanged("ListAllColumnDG");

            ListAllColumnDGNull = new ObservableCollection<ColumnSelectionDataGrid>(listNull);
            NotifyPropertyChanged("ListAllColumnDGNull");

        }

        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDG.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            switch (selectColName)
            {
                case "SectionText":
                    visibleHeaderSectionText = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("visibleHeaderSectionText");
                    break;
                case "URL":
                    visibleHeaderURL = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("visibleHeaderURL");
                    break;
                case "DateFirstAdded":
                    visibleHeaderDateFirstAdded = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("visibleHeaderDateFirstAdded");
                    break;
                case "DateLastUpdated":
                    visibleHeaderDateLastUpdated = selectCol.IsSelected ? Visibility.Visible : Visibility.Collapsed;
                    NotifyPropertyChanged("visibleHeaderDateLastUpdated");
                    break;
            }
            CustomQueryController = new QueryController();
        }

        private List<string> queryNull { get; set; } = new List<string>();
        private string queryStringForNUll { get; set; }
        private void CommandOnColSelectedChangeNullExecute(object obj)
        {
            ExecuteClearFilter();
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDGNull.Where(x => x.ColumnName == selectColName).FirstOrDefault();
            switch (selectColName)
            {
                case "SectionText":
                    if (selectCol.IsNull)
                        queryNull.Add(" SectionText.Length = 0 ");
                    else
                        queryNull.Remove(" SectionText.Length = 0 ");
                    if (selectCol.IsNotNull)
                        queryNull.Add(" SectionText.Length > 0 ");
                    else
                        queryNull.Remove(" SectionText.Length > 0 ");
                    break;
                case "URL":
                    if (selectCol.IsNull)
                        queryNull.Add(" URL.Length = 0 ");
                    else
                        queryNull.Remove(" URL.Length = 0 ");
                    if (selectCol.IsNotNull)
                        queryNull.Add(" URL.Length > 0 ");
                    else
                        queryNull.Remove(" URL.Length > 0 ");
                    break;
                case "DateFirstAdded":
                    if (selectCol.IsNull)
                        queryNull.Add(" DateFirstAdded = null ");
                    else
                        queryNull.Remove(" DateFirstAdded = null ");
                    if (selectCol.IsNotNull)
                        queryNull.Add(" DateFirstAdded != null ");
                    else
                        queryNull.Remove(" DateFirstAdded != null ");
                    break;
                case "DateLastUpdated":
                    if (selectCol.IsNull)
                        queryNull.Add(" DateLastUpdated = null ");
                    else
                        queryNull.Remove(" DateLastUpdated = null ");
                    if (selectCol.IsNotNull)
                        queryNull.Add(" DateLastUpdated != null ");
                    else
                        queryNull.Remove(" DateLastUpdated != null ");
                    break;
            }
            var result = string.Join("&&", queryNull.ToList());
            queryStringForNUll = result;
            SearchCriteria = result;
            NotifyPropertyChanged("SearchCriteria");
            if (result == string.Empty)
            {
                queryStringForNUll = string.Empty;
                myListTotal = myListPreserve.Select(x => x).ToList();
                myList = myListTotal.AsQueryable().ToList();
            }
            else
            {
                myListTotal = myListPreserve.Select(x => x).ToList();
                myListTotal = myListPreserve.AsQueryable().Where(result.Trim()).ToList();
                myList = myListTotal;
            }
            if (myList.Count == 0)
            {
                ListDataTest = null;
                lblContent = "";
                lblTotRecords = "";
                NotifyPropertyChanged("lblTotRecords");
                NotifyPropertyChanged("lblContent");
                NotifyPropertyChanged("ListDataTest");
            }
            else
            {
                Navigate(0);
            }
        }
        private void InitialStep()
        {
            //EstTime = objCommon.ESTTime();
            var getShortTitle = context.Library_PhraseCategories.Where(x => x.PhraseCategory == "Legislation Short Title").Select(y => y.PhraseCategoryID).FirstOrDefault();
            var qsxxLeg = context.QSxxx_LegislationDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == getShortTitle).Select(y => new { y.LG_ID, y.PhraseID }).Distinct().ToList();
            var allLeg = (from d1 in context.Library_Legislations.AsNoTracking().ToList()
                          join d2 in qsxxLeg
                          on d1.LG_ID equals d2.LG_ID
                          into tg
                          from tcheck in tg.DefaultIfEmpty()
                          select new { d1.LG_ID, d1.LegislationID, PhraseID = tcheck == null ? 0 : tcheck.PhraseID }).ToList();
            var allLegFinal = (from d1 in allLeg
                               join d2 in context.Library_Phrases.AsNoTracking()
                               on d1.PhraseID equals d2.PhraseID
                               into tg
                               from tcheck in tg.DefaultIfEmpty()
                               select new SelectListLegislation { LG_ID = d1.LG_ID, LegislationID = d1.LegislationID, ShortTitle = tcheck == null ? "" : tcheck.Phrase }).Distinct().ToList();
            ListLegislation = new ObservableCollection<SelectListLegislation>(allLegFinal);
            NotifyPropertyChanged("ListLegislation");
            BindSectionProfile();
            //SelectedListLegislation = allLegFinal.Where(x => x.LG_ID == 1).FirstOrDefault();

            ListComboxItem = new ObservableCollection<selectedListItemCombo>(AddList());
            NotifyPropertyChanged("ListComboxItem");

            VisibleLoderCurrentData = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderCurrentData");
            BindListAllColumn();
        }


        private void insertBtn(object obj)
        {
            var lgID = _SelectedListLegislation.LG_ID;
            var pID = SelectedListDataItem.ID;
            //var pID = ListData[i].ID;
            var secID = SelectedListDataItem.SelectSectionID == null ? "" : SelectedListDataItem.SelectSectionID.SectionID.Trim();
            //var secTExt = objCommon.RemoveWhitespaceCharacter(SelectedListDataItem.SectionText.Trim());
            var stripCharacters = context.Library_StripCharacters.AsNoTracking().Where(x => string.IsNullOrEmpty(x.CharacterUnicode.Trim()) == false).Select(y => new IStripChar { val = "\\u" + y.CharacterUnicode.Trim(), Description = y.Description, Unicode = y.CharacterUnicode.Trim() }).ToList();
            var secTExt = objCommon.RemoveWhitespaceCharacter(SelectedListDataItem.SectionText.Trim(), stripCharacters);
            secTExt = System.Text.RegularExpressions.Regex.Replace(secTExt, @"\s+", " ").Trim();

            var sOrd = SelectedListDataItem.SortOrder;
            var url = string.IsNullOrEmpty(SelectedListDataItem.URL)? null : SelectedListDataItem.URL.Trim();

            var profileId = SelectedListDataItem;
            var status = string.Empty;
            var Id = context.Map_LegislationProfile.Where(x => x.LegislationProfileID == pID).FirstOrDefault();
            if (Id == null)
            {
                if (secID == "" || sOrd == 0 || secTExt == "")
                {
                    System.Windows.Forms.MessageBox.Show("SectionID, SortOrder and SectionText can't be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    status = "A";
                    var maxID = objCommon.GetMaxAutoField("Map_LegislationProfile");
                    pID = maxID;
                    var getMaxFromLastSection = context.Map_LegislationProfile.Where(x => x.LG_ID == lgID && x.SectionID == secID).Select(y => y.SortOrderWithinSection).OrderByDescending(z => z).FirstOrDefault();
                    var chkSortOrderbyUserAlreadyExist = context.Map_LegislationProfile.Count(x => x.LG_ID == lgID && x.SectionID == secID && x.SortOrderWithinSection == sOrd);
                    if (chkSortOrderbyUserAlreadyExist == 0 && sOrd < getMaxFromLastSection)
                    {
                    }
                    else
                    {
                        sOrd = getMaxFromLastSection == 0 ? 1 : getMaxFromLastSection + 1;
                    }
                    _objLibraryFunction_Service.AddMapLegislationProfile(lgID, secID, secTExt, sOrd, url);
                }
            }
            else
            {
                var mapIds = context.Map_LegislationProfile.AsNoTracking().Where(x => x.SectionText == secTExt).ToList();
                var chkupdate = mapIds.Where(x => x.LegislationProfileID == pID
                && x.SectionID == secID && x.SectionText == secTExt && x.SortOrderWithinSection == sOrd
                && x.URL == url).FirstOrDefault();
                if (chkupdate == null)
                {
                    if (secID == "" || sOrd == 0 || secTExt == "")
                    {
                        System.Windows.Forms.MessageBox.Show("SectionID, SortOrder and SectionText can't be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        status = "C";
                        var getMaxFromLastSection = context.Map_LegislationProfile.Where(x => x.LG_ID == lgID && x.SectionID == secID && x.LegislationProfileID != pID).Select(y => y.SortOrderWithinSection).OrderByDescending(z => z).FirstOrDefault();
                        var currentMaxId = context.Map_LegislationProfile.Where(x => x.LG_ID == lgID && x.SectionID == secID && x.LegislationProfileID == pID).Select(y => y.SortOrderWithinSection).OrderByDescending(z => z).FirstOrDefault();
                        var chkSortOrderbyUserAlreadyExist = context.Map_LegislationProfile.Count(x => x.LG_ID == lgID && x.SectionID == secID && x.SortOrderWithinSection == sOrd && x.LegislationProfileID != pID);
                        if (chkSortOrderbyUserAlreadyExist == 0 && (sOrd <= getMaxFromLastSection || sOrd == currentMaxId))
                        {
                            if (sOrd == currentMaxId)
                            {
                                sOrd = currentMaxId;
                            }
                        }
                        else
                        {
                            sOrd = getMaxFromLastSection == 0 ? 1 : getMaxFromLastSection + 1;
                        }
                        var lstFinal = new List<Map_LegislationProfile>();
                        var result = new Map_LegislationProfile
                        {
                            LegislationProfileID = pID,
                            LG_ID = lgID,
                            SectionID = secID,
                            SectionText = secTExt,
                            SortOrderWithinSection = sOrd,
                            URL = url ,
                        };
                        lstFinal.Add(result);
                        _objLibraryFunction_Service.UpdateMapLegislationProfile(lstFinal);
                    }
                }
            }
            if (status != string.Empty)
            {
                _objLibraryFunction_Service.AddLogMapLegislationProfile(lgID, secID, secTExt, sOrd, url, status, pID, objCommon.ESTTime());
                if (context.Log_Legislation_Changes.FirstOrDefault(x => x.LG_ID == lgID && x.Status == "C" && x.SessionID == Engine.CurrentUserSessionID) == null)
                {
                    _objLibraryFunction_Service.AddLogLegislationChanges(lgID, "C", Engine.CurrentUserSessionID, objCommon.ESTTime());
                }
            }
            lblSaveupdate = "Record Saved Successfully";
            NotifyPropertyChanged("lblSaveupdate");
            IsSave = true;
            currentPid = pID;
            BindGridWithSource(lgID);
        }
        private void deleteBtn(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to delete the selected record", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                var profileId = SelectedListDataItem.ID;
                var Id = context.Map_LegislationProfile.Where(x => x.LegislationProfileID == profileId).FirstOrDefault();
                List<int> lstProfileId = new List<int>() { profileId };
                if (Id != null)
                {
                    _objLibraryFunction_Service.AddLogMapLegislationProfile(Id.LG_ID, Id.SectionID, Id.SectionText, Id.SortOrderWithinSection, Id.URL, "D", Id.LegislationProfileID, objCommon.ESTTime());
                    _objLibraryFunction_Service.AddLogLegislationChanges(Id.LG_ID, "C", Engine.CurrentUserSessionID, objCommon.ESTTime());
                    _objLibraryFunction_Service.DeleteMapLegislationProfile(lstProfileId);
                    BindGridWithSource(_SelectedListLegislation.LG_ID);
                    lblSaveupdate = "Record Deleted Successfully";
                    NotifyPropertyChanged("lblSaveupdate");
                }
            }
        }
        private void BindGridWithSource(int lgID)
        {
            VisibleLoderCurrentData = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderCurrentData");
            var result = context.Map_LegislationProfile.AsNoTracking().Where(y => y.LG_ID == lgID).ToList();
            List<LegislationProfileTable> newRec = new List<LegislationProfileTable>();
            List<LegislationProfileImport> accessImport = new List<LegislationProfileImport>();
            var logTableDateAdded = context.Log_Map_LegislationProfile.AsNoTracking().Where(x => x.LG_ID == lgID && x.Status == "A").GroupBy(y => new { y.LegislationProfileID, y.TimeStamp }).OrderBy(xx => new { xx.Key.LegislationProfileID, xx.Key.TimeStamp }).Select(z => z.FirstOrDefault()).ToList();
            var logTableDateChgs = context.Log_Map_LegislationProfile.AsNoTracking().Where(x => x.LG_ID == lgID && x.Status == "C").OrderByDescending(y => new { y.LegislationProfileID, y.TimeStamp }).ToList();

            result.ForEach(x =>
            {
                var dffirstAdded = logTableDateAdded.Where(y => y.LegislationProfileID == x.LegislationProfileID).Select(z => z.TimeStamp).FirstOrDefault();
                var newRecord = new LegislationProfileTable
                {
                    IsEdit = false,
                    ID = x.LegislationProfileID,
                    SectionText = x.SectionText,
                    SortOrder = x.SortOrderWithinSection,
                    URL = string.IsNullOrEmpty(x.URL) ? null : x.URL,
                    SelectSectionID = SectionNew.Where(y => y.SectionID == x.SectionID).FirstOrDefault(),
                    DateFirstAdded = dffirstAdded,
                    DateLastUpdated = logTableDateChgs.Count(y => y.LegislationProfileID == x.LegislationProfileID) == 0 ? dffirstAdded : logTableDateChgs.Where(y => y.LegislationProfileID == x.LegislationProfileID).Select(z => z.TimeStamp).FirstOrDefault(),

                };
                newRec.Add(newRecord);
                var secid = lstSectionProfile.Where(y => y.SectionID == x.SectionID).FirstOrDefault();
                if (secid != null)
                {
                    accessImport.Add(new LegislationProfileImport
                    {
                        ID = x.LegislationProfileID,
                        SectionText = x.SectionText,
                        SortOrder = x.SortOrderWithinSection,
                        URL = string.IsNullOrEmpty(x.URL) ? null : x.URL,
                        SectionID = secid.SectionID,
                        DateFirstAdded = newRecord.DateFirstAdded,
                        DateLastUpdated = newRecord.DateLastUpdated,
                    });
                }
            });

            //newRec.Add(new LegislationProfileTable
            //{
            //    IsEdit = false,
            //    ID = 0,
            //    SectionText = "",
            //    SortOrder = 0,
            //    SelectSectionID = new SectionProfile { SectionID = "SCOPE", SectionTitle = "SCOPE" },
            //    URL = "",
            //    DateFirstAdded = DateTime.Now,
            //    DateLastUpdated = null,
            //});
            myList = newRec;
            currpage = pageIndex;
            myListPreserve = myList.Select(x => x).ToList();
            myListTotal = myList.Select(x => x).ToList();
            dtDefault = objCommon.ConvertToDataTable(accessImport);
            selectedItemCombo = new selectedListItemCombo() { id = 25, text = "25" };
            NotifyPropertyChanged("selectedItemCombo");
            VisibleLoderCurrentData = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderCurrentData");
            if (!IsSave)
            {
                ExecuteClearFilter();
            }
            else
            {
                GetFilterQuery();
                if (currpage == 1)
                {
                }
                else
                {
                    for (int i = 0; i < currpage - 1; i++)
                    {
                        Navigate((int)PagingMode.Next);
                    }
                }
            }
            IsSave = false;
        }
        private void BindSectionProfile()
        {
            lstSectionProfile = context.Library_LegislationProfileSections.AsNoTracking().Select(x => new SectionProfile { SectionID = x.SectionID, SectionTitle = x.SectionTitle }).Distinct().ToList();
            SectionNew = new ObservableCollection<SectionProfile>(lstSectionProfile);
            NotifyPropertyChanged("SectionNew");
        }

        #region Singlecell Copyin Clipboard
        private DataGridCellInfo _currentCellData { get; set; }
        public DataGridCellInfo CurrentCellData
        {
            get { return _currentCellData; }
            set
            {
                if (value != _currentCellData)
                {
                    _currentCellData = value;
                }
            }
        }

        #endregion

        #region CustomQuery and Show in SerachCriteria


        public QueryController CustomQueryController
        {
            get { return _queryController; }
            set
            {
                if (_queryController != value)
                {
                    _queryController = value;
                }
                NotifyPropertyChanged("CustomQueryController");
            }
        }
        private void CustomQueryController_FilteringStarted(object sender, EventArgs e)
        {
            if (IsNavigationProcess != true)
            {
                string colName = CustomQueryController.ColumnFilterData.ValuePropertyBindingPath;
                string valueofCol = CustomQueryController.ColumnFilterData.QueryString;
                Type valueType = CustomQueryController.ColumnFilterData.ValuePropertyType;
                string filterOpeartor = CustomQueryController.ColumnFilterData.Operator.ToString();
                if (valueofCol != null && filterOpeartor != "Undefined")
                {
                    if (!_listfilter.Any(x => x.ColName == colName && x.ColValue == valueofCol && x.ColOperator == filterOpeartor) || colName == "")
                    {
                        if (_listfilter.Any(x => x.ColName == colName))
                        {
                            var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                            objFilter.ColValue = valueofCol;
                            objFilter.ColOperator = filterOpeartor;
                        }
                        else
                        {
                            _listfilter.Add(new filterList { ColName = colName, ColValue = valueofCol, ColType = valueType, ColOperator = filterOpeartor });
                        }
                        SearchCriteria = string.Empty;
                        if (_listfilter.Count() == 1 && valueofCol == "")
                        {
                            _listfilter = new List<filterList>();
                        }
                        GetFilterQuery();
                    }
                }
                //else
                //{
                //    var objFilter = _listfilter.Where(x => x.ColName == colName).FirstOrDefault();
                //    if (objFilter != null )
                //    {
                //        objFilter.ColValue = valueofCol;
                //        SearchCriteria = string.Empty;
                //        //GetFilterQuery();
                //    }
                //}
                // }
            }
        }

        public void GetFilterQuery()
        {
            string queryString = string.Empty;
            string searchQueryString = string.Empty;
            List<string> arryOfColValue = new List<string>();
            _listfilter = _listfilter.Where(x => x.ColValue != "").ToList();
            for (var counter = 0; counter < _listfilter.Count; counter++)
            {
                if (_listfilter[counter].ColType == typeof(Int32))
                {
                    queryString += counter > 0 ? " &&" : "";
                    if (Int32.TryParse(_listfilter[counter].ColValue, out int res))
                    {
                        switch (_listfilter[counter].ColOperator)
                        {
                            case "Equals":
                                queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThan":
                                queryString += " " + _listfilter[counter].ColName + " < " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThan":
                                queryString += " " + _listfilter[counter].ColName + " > " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "LessThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " <= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                            case "GreaterThanOrEqual":
                                queryString += " " + _listfilter[counter].ColName + " >= " + Convert.ToInt32(_listfilter[counter].ColValue);
                                break;
                        }
                        //queryString += " " + _listfilter[counter].ColName + ".Contains('" + _listfilter[counter].ColValue + ")";
                        searchQueryString += " " + _listfilter[counter].ColName + " " + _listfilter[counter].ColOperator + " '" + _listfilter[counter].ColValue + "',";
                    }
                }
                else
                {
                    if (_listfilter[counter].ColType == typeof(bool))
                    {
                        queryString += counter > 0 ? " && " : "";
                        queryString += " " + _listfilter[counter].ColName + " = " + Convert.ToBoolean(_listfilter[counter].ColValue);
                        searchQueryString += " " + _listfilter[counter].ColName + " Equal '" + _listfilter[counter].ColValue + "',";
                        arryOfColValue.Add(_listfilter[counter].ColValue.ToString());
                    }
                    else
                    {
                        queryString += counter > 0 ? " && " : "";
                        if (IsCheckedFilterCaseSenstive)
                        {
                            queryString += " " + _listfilter[counter].ColName + ".Contains(@" + counter + ")";
                            arryOfColValue.Add(_listfilter[counter].ColValue.ToString());
                        }
                        else
                        {
                            queryString += " " + _listfilter[counter].ColName + ".ToLower().Contains(@" + counter + ")";
                            arryOfColValue.Add(_listfilter[counter].ColValue.ToString().ToLower());
                        }
                        searchQueryString += " " + _listfilter[counter].ColName + " Like '" + _listfilter[counter].ColValue + "',";
                    }
                }

            }
            if (queryString != "")
            {
                if (arryOfColValue.Count == 0)
                {
                    myList = myListTotal.AsQueryable().Where(queryString.Trim()).ToList();
                }
                else
                {
                    myList = myListTotal.AsQueryable().Where(queryString.Trim(), arryOfColValue.ToArray()).ToList();
                }
            }
            else
            {
                pageIndex = 1;
                myList = myListTotal.Select(x => x).ToList();
            }
            if (string.IsNullOrEmpty(queryStringForNUll))
            {
                SearchCriteria = "";
                SearchCriteria = searchQueryString.TrimEnd(',').Replace("SelectSectionID.", "");
            }
            else
            {
                SearchCriteria = "";
                SearchCriteria = queryStringForNUll + ", " + searchQueryString.TrimEnd(',').Replace("SelectSectionID.", "");
            }
            //SearchCriteria = string.Empty;

            Navigate(0);
            commandFilterExecute = false;
        }
        #endregion
        #region ClearFilter Button Click
        public bool commandFilterExecute { get; set; }
        private void ExecuteClearFilter()
        {
            if (CustomQueryController != null && CustomQueryController.ColumnFilterData != null)
            {
                CustomQueryController.ClearFilter();
                myList = myListTotal.Select(x => x).ToList();
                if (string.IsNullOrEmpty(queryStringForNUll))
                {
                    SearchCriteria = "";
                }
                else
                {
                    SearchCriteria = queryStringForNUll;
                }
                //SearchCriteria = string.Empty;
                _listfilter = new List<filterList>();
                pageIndex = 1;
                NotifyPropertyChanged("SearchCriteria");
                Navigate(0);
            }
        }
        #endregion
        #region Excel Export
        private void CommandExcelExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                RowIdentifier.ExportExcelFile(dtDefault, folderDlg.SelectedPath + "\\Test.Xlsx");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        #endregion
        #region Access Export
        private void CommandAccessExport(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };

            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                objCurrentView.ExportAccessFileLocal(folderDlg.SelectedPath + "\\LegislationProfile.mdb", dtDefault, "LegislationProfile");
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        #endregion
        #region  Paging
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }
        private void Navigate(int mode)
        {
            IsNavigationProcess = true;
            lblCurrentCellInfo = string.Empty;
            NotifyPropertyChanged("lblCurrentCellInfo");
            Task.Run(() =>
            {
                var result = _objLibraryFunction_Service.NavigateList(mode, ref pageIndex, numberOfRecPerPage, myList, selectedItemCombo.text, lblContent, lblTotRecords, ListDataTest);
                lblContent = result.Item1;
                lblTotRecords = result.Item2;
                ListDataTest = new ObservableCollection<LegislationProfileTable>(result.Item3);
                NotifyPropertyChanged("lblTotRecords");
                NotifyPropertyChanged("lblContent");
                IsNavigationProcess = false;
            });


        }
        public selectedListItemCombo selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    numberOfRecPerPage = Convert.ToInt32(value.text);
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }
        private List<selectedListItemCombo> AddList()
        {
            List<selectedListItemCombo> listSelectedCombo =

                new List<selectedListItemCombo>() {
                new selectedListItemCombo() { id = 25, text = "25" },
                new selectedListItemCombo() { id = 50, text = "50" },
                new selectedListItemCombo() { id = 100, text = "100" },
                new selectedListItemCombo() { id = 101, text = "All" },
            };
            return listSelectedCombo;

        }
        #endregion

        #region Search Criteria
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (_searchCriteria != value)
                {
                    _searchCriteria = value;
                    NotifyPropertyChanged("SearchCriteria");
                    if (String.IsNullOrEmpty(_searchCriteria))
                    {
                        IsSearchCriteriaVisible = Visibility.Collapsed;
                    }
                    else
                    {
                        IsSearchCriteriaVisible = Visibility.Visible;
                    }
                    NotifyPropertyChanged("IsSearchCriteriaVisible");
                }
            }
        }
        #endregion
    }

    public class SelectListLegislation
    {
        public int LG_ID { get; set; }
        public string LegislationID { get; set; }

        public string ShortTitle { get; set; }
    }
    public class LegislationProfileTable : INotifyPropertyChanged
    {
        private bool _IsEdit { get; set; }
        public bool IsEdit
        {
            get { return _IsEdit; }
            set
            {
                _IsEdit = value;
                NotifyPropertyChanged("IsEdit");
            }
        }
        public int ID { get; set; }
        private SectionProfile _SelectSectionID { get; set; }
        public SectionProfile SelectSectionID
        {
            get { return _SelectSectionID; }
            set
            {
                _SelectSectionID = value;
            }
        }
        private string _SectionText { get; set; }

        public string SectionText
        {
            get { return _SectionText; }
            set
            {
                _SectionText = value;
            }
        }

        private int _SortOrder { get; set; }
        public int SortOrder
        {
            get { return _SortOrder; }
            set
            {
                _SortOrder = value;
            }
        }
        public string _URL { get; set; }
        public string URL
        {
            get { return _URL; }
            set
            {
                _URL = value;
            }
        }

        private DateTime? _DateFirstAdded { get; set; }
        public DateTime? DateFirstAdded
        {
            get { return _DateFirstAdded; }
            set
            {
                _DateFirstAdded = value;
            }
        }
        private DateTime? _DateLastUpdated { get; set; }
        public DateTime? DateLastUpdated
        {
            get { return _DateLastUpdated; }
            set
            {
                _DateLastUpdated = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
    public class LegislationProfileImport
    {
        public int ID { get; set; }
        public string SectionID { get; set; }
        public string SectionText { get; set; }
        public int SortOrder { get; set; }
        public string URL { get; set; }
        public DateTime? DateFirstAdded { get; set; }
        public DateTime? DateLastUpdated { get; set; }
    }
    public class SectionProfile
    {
        public string SectionID { get; set; }
        public string SectionTitle { get; set; }
    }

    public class ColumnSelectionDataGrid : INotifyPropertyChanged
    {
        private bool _IsNull { get; set; }
        public bool IsNull
        {
            get { return _IsNull; }
            set
            {
                _IsNull = value;
                if (_IsNull)
                {
                    IsNotNull = false;
                }
                NotifyPropertyChanged("IsNull");
            }
        }

        private bool _IsNotNull { get; set; }
        public bool IsNotNull
        {
            get { return _IsNotNull; }
            set
            {
                _IsNotNull = value;
                if (IsNotNull)
                {
                    IsNull = false;
                }
                NotifyPropertyChanged("IsNotNull");
            }
        }

        public string ColumnName { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
