﻿using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class SubstanceHistory_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listSubstanceHistory { get; set; }
        public Visibility VisibleLoderSubstanceHistory { get; set; }
        public SubstanceHistory_VM()
        {
            Task.Run(() => RunSubstanceHistory());

        }
        private void RunSubstanceHistory()
        {
            VisibleLoderSubstanceHistory = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderSubstanceHistory");
            if (Win_Administrator_VM.ListSubstanceHistory.Columns.Count == 0)
            {
                Win_Administrator_VM.ListSubstanceHistory = _objLibraryFunction_Service.GetSubStanceHistoryFromQsid(Win_Administrator_VM.Qsid_ID);
            }
           
            listSubstanceHistory = new StandardDataGrid_VM("SubstanceHistory", Win_Administrator_VM.ListSubstanceHistory, Visibility.Visible);
            NotifyPropertyChanged("listSubstanceHistory");
            VisibleLoderSubstanceHistory = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderSubstanceHistory");
        }
    }
}
