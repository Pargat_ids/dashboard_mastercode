﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications.Messages;
using EntityClass;
using System.Linq;

namespace CurrentDataView.ViewModel
{
    public class Notes_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listNotes { get; set; }
        private RowIdentifier objLegislation = new RowIdentifier();
        public Visibility VisibleLoderNotes { get; set; }
        public Notes_VM()
        {
            using (var context = new CRAModel())
            {
                var chkExist = context.QSxxx_Notes.AsNoTracking().Count(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID);
                if( chkExist == 0)
                {
                    _notifier.ShowError("No Record Found");
                    listNotes = null;
                    NotifyPropertyChanged("listNotes");
                    VisibleLoderNotes = Visibility.Hidden;
                    NotifyPropertyChanged("VisibleLoderNotes");
                }
                else
                {
                    Task.Run(() => RunNotes());
                    
                }
            }
        }
        private void RunNotes()
        {
            VisibleLoderNotes = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderNotes");
            //if (Win_Administrator_VM.NotesTable.Columns.Count == 0)
            //{
                Win_Administrator_VM.NotesTable = objLegislation.GetBindNotes(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                listNotes = new StandardDataGrid_VM("Notes", Win_Administrator_VM.NotesTable, Visibility.Visible);
                NotifyPropertyChanged("listNotes");
                VisibleLoderNotes = Visibility.Hidden;
                NotifyPropertyChanged("VisibleLoderNotes");
            //}
        }
    }
}
