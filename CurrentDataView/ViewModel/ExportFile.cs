﻿using System.Threading.Tasks;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class ExportFile
    {
        private RowIdentifier rowIdentifier = new RowIdentifier();
        private string SelectedPath;
        public ExportFile(string selectedPath)
        {
            SelectedPath = selectedPath;
            ExportAccess();
        }
        private void ExportAccess()
        {
            //if (Win_Administrator_VM.CharacterTable.Columns.Count == 0)
            //{
            if (Win_Administrator_VM.CurrentDataTable.Columns.Count == 0)
            {
                CurrentData_VM.SingleCurrentViewFunction();
                Win_Administrator_VM.ParentChildDataTable = Win_Administrator_VM.CurrentDataTable.Copy();
            }
            Task.Run(() =>
            {
                if (Win_Administrator_VM.NotesTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.NotesTable = rowIdentifier.GetBindNotes(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                }

                if (Win_Administrator_VM.DataDictionaryTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.DataDictionaryTable = rowIdentifier.GetBindDataDictionary(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                }

                if (Win_Administrator_VM.ListDictionaryTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.ListDictionaryTable = rowIdentifier.GetListDictionaryData(Win_Administrator_VM.ListDictionaryTable, Win_Administrator_VM.Qsid_ID);
                }
                if (Win_Administrator_VM.LegislationTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.LegislationTable = rowIdentifier.GetLegislation(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                }
                if (Win_Administrator_VM.ParentChildTable.Columns.Count == 0)
                {
                    Win_Administrator_VM.ParentChildTable = rowIdentifier.DataChildRelation(Win_Administrator_VM.ParentChildTable, Win_Administrator_VM.Qsid_ID);
                }

                Win_Administrator_VM.CharacterTable = rowIdentifier.GetCharacterDetails(Win_Administrator_VM.CurrentDataTable, Win_Administrator_VM.NotesTable, Win_Administrator_VM.DataDictionaryTable, Win_Administrator_VM.ListDictionaryTable, 4);
                rowIdentifier.ExportAccessFile(SelectedPath, Win_Administrator_VM.CurrentDataTable, Win_Administrator_VM.ListDictionaryTable, Win_Administrator_VM.DataDictionaryTable, Win_Administrator_VM.NotesTable, Win_Administrator_VM.LegislationTable, Win_Administrator_VM.ParentChildTable, Win_Administrator_VM.Qsid, Win_Administrator_VM.Qsid_ID);
            });
            //}

        }
    }
}
