﻿using System.Configuration;
using System.Windows.Input;
using System.Windows.Controls;
using CRA_DataAccess;
using CurrentDataView.Library;
using System.Data;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using static CurrentDataView.ViewModel.ParentChildData_VM;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class Win_Administrator_VM : Base_ViewModel
    {
        private CommonFunctions objCommon;
        public ObservableCollection<string> ListQsidSearchTyp { get; set; }
        public TabItem selectedTabItem { get; set; }
        public ICommand CommandTabChanged { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand ShowCommand { get; set; }
        public string AllCas { get; set; }
        public ICommand BtnCurrentDataViewShow { get; set; }
        public CharacterDetail_VM objCharacterDetail { get; set; }
        public CurrentData_VM objCurrentDataView { get; set; }
        public UniqueValues_VM objUniqueValues { get; set; }
        public AdditionalInfoHistory_VM objAdditionalInfoHistory { get; set; }
        public CommonDataGrid_ViewModel<CurrentPh> CurrentUniquePhrase_VM { get; set; }
        public UniqueCriteria_VM objUniqueCriteriaHistory { get; set; }
        public DataDictionary_VM objDataDictionary { get; set; }
        public Legislation_VM objLegislation { get; set; }
        public MapCASToEC_VM objMapCAStoEC { get; set; }
        public GenericAddedDeletedLog_VM objGenericAddedDeleted { get; set; }
        public ListDictionary_VM objListDictionary { get; set; }
        public ListedGroupMembers_VM objListedGroupMembers { get; set; }
        public ParentChildData_VM objParentChild { get; set; }
        public TransPhrases_VM objTransPhrases { get; set; }
        public Notes_VM objNotes { get; set; }
        public CurrentGenericData_VM _objCurrentGenericData { get; set; }

        public SubstanceHistory_VM objSubstanceHistory { get; set; }
        public RowHistory_VM objRowHistory { get; set; }
        public ProductFields_VM objProductFields { get; set; }
        public CurrentDataViewByVersion_VM objAddDeleteInSameVersion { get; set; }
        public ProductionVersionChanges_VM objProductionVersion { get; set; }
        public ParentChildRelationShip_VM objParentChildRel { get; set; }

        public DeletedRows_VM objDeletedRows { get; set; }

        public string MdbVersion { get; set; }
        public string NormalizedOn { get; set; }

        public static int Qsid_ID { get; set; }
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public bool IsEnabledParentChildTab { get; set; }
        private string _QsidText { get; set; }
        public string QsidText
        {
            get { return _QsidText; }
            set
            {
                if (value != _QsidText)
                {
                    _QsidText = value;
                }
            }
        }

        public static string CommonListConn;

        public CommonDataGrid_ViewModel<CurrentData_ConflictGenerics_VM> ConflictGenerics_VM { get; set; }

        public static DataTable CharacterTable = new DataTable();
        public static DataTable CurrentDataTable = new DataTable();
        public static DataTable CurrentDataTableWithChild = new DataTable();
        public static DataTable DataDictionaryTable = new DataTable();
        public static DataTable LegislationTable = new DataTable();
        public static DataTable ListDictionaryTable = new DataTable();
        public static DataTable NotesTable = new DataTable();
        public static DataTable ParentChildTable = new DataTable();
        public static DataTable ParentChildDataTable = new DataTable();
        public static List<IParentChildD> ChildDataForPopup = new List<IParentChildD>();
        public static DataTable DataDeletedRows = new DataTable();
        public static DataTable AddInfo = new DataTable();
        public static DataTable AddInfoHistory = new DataTable();
        public static DataTable UniqueCriteria = new DataTable();
        public static DataTable UniqueCriteriaHistory = new DataTable();
        public static DataTable DataDicHistory = new DataTable();
        public static DataTable ListDicHistory = new DataTable();
        public static DataTable ListSubstanceHistory = new DataTable();
        public static DataTable ListRowHistory = new DataTable();
        public static DataTable ListProductFields = new DataTable();
        public static DataTable AddedDeletedRows = new DataTable();
        public static DataTable AddedDelBySingle = new DataTable();
        public static DataTable dtProductionVersion = new DataTable();
        public static DataTable dtCurrentGeneric = new DataTable();
        public static DataTable dtTransPhrases = new DataTable();
        public static string SelectType = string.Empty;
        public static List<String> FilterCas = new List<string>();
        public bool IsSelectedTabData { get; set; }
        public bool IsSelectedTabAdditionalInfo { get; set; }
        public ObservableCollection<string> listTypeName { get; set; }

        public string _SelectedType { get; set; }
        public ICommand ShowHistory { get; set; }

        public string SelectedType
        {
            get => _SelectedType;
            set
            {
                if (Equals(_SelectedType, value))
                {
                    return;
                }

                _SelectedType = value;
                if (value != null)
                {
                    SelectType = value;
                }
            }
        }
        private void dataTableMakeEmpty()
        {
            CharacterTable = new DataTable();
            CurrentDataTable = new DataTable();
            DataDictionaryTable = new DataTable();
            LegislationTable = new DataTable();
            ListDictionaryTable = new DataTable();
            NotesTable = new DataTable();
            ParentChildTable = new DataTable();
            ParentChildDataTable = new DataTable();
            ChildDataForPopup = new List<IParentChildD>();
            DataDeletedRows = new DataTable();
            AddInfo = new DataTable();
            UniqueCriteria = new DataTable();
            AddInfoHistory = new DataTable();
            UniqueCriteriaHistory = new DataTable();
            DataDicHistory = new DataTable();
            ListDicHistory = new DataTable();
            ListSubstanceHistory = new DataTable();
            ListRowHistory = new DataTable();
            AddedDeletedRows = new DataTable();
            AddedDelBySingle = new DataTable();
            dtProductionVersion = new DataTable();
            dtCurrentGeneric = new DataTable();
            dtTransPhrases = new DataTable();
            FilterCas = new List<string>();
        }
        public void CallDeletedRows()
        {
            objDeletedRows = new DeletedRows_VM();
            NotifyPropertyChanged("objDeletedRows");
        }
        private ObservableCollection<CurrentData_ConflictGenerics_VM> _listConflictGenerics { get; set; } = new ObservableCollection<CurrentData_ConflictGenerics_VM>();
        public ObservableCollection<CurrentData_ConflictGenerics_VM> listConflictGenerics
        {
            get { return _listConflictGenerics; }
            set
            {
                if (_listConflictGenerics != value)
                {
                    _listConflictGenerics = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<CurrentData_ConflictGenerics_VM>>>(new PropertyChangedMessage<List<CurrentData_ConflictGenerics_VM>>(null, _listConflictGenerics.ToList(), "Default List"));
                }
            }
        }
        public Visibility loderConflictingGenericsVisibility { get; set; } = Visibility.Collapsed;

        private string _SelectedQSIDSrch { get; set; }
        public string SelectedQSIDSearchU { get; set; }
        public string SelectedQSIDSrch
        {
            get { return _SelectedQSIDSrch; }
            set
            {
                if (_SelectedQSIDSrch != value)
                {
                    _SelectedQSIDSrch = value;
                    NotifyPropertyChanged("SelectedQSIDSrch");
                    if (value == "Search By QSID")
                    {
                        SelectedQSIDSearchU = "Enter QSID";
                    }
                    if (value == "Search By QSID_ID")
                    {
                        SelectedQSIDSearchU = "Enter QSID_ID";
                    }
                    if (value == "Search By List_Field_Name")
                    {
                        SelectedQSIDSearchU = "Enter List_Field_Name";
                    }
                    NotifyPropertyChanged("SelectedQSIDSearchU");
                }

            }
        }
        private bool ShowHistoryCanExecute(object obj)
        {
            bool result = true;
            return result;
        }
        public bool IsSelectedTabDataGenericsCount { get; set; } = false;
        public Win_Administrator_VM()
        {
            AllCas = string.Empty;
            NotifyPropertyChanged("AllCas");
            objCommon = new CommonFunctions();
            CommonListConn = ConfigurationManager.AppSettings["CommonListConnection"];
            var listItemCAS = new List<string>();
            listItemCAS.Add("Search By QSID");
            listItemCAS.Add("Search By QSID_ID");
            listItemCAS.Add("Search By List_Field_Name");
            ListQsidSearchTyp = new ObservableCollection<string>(listItemCAS);
            NotifyPropertyChanged("ListQsidSearchTyp");
            SelectedQSIDSrch = ListQsidSearchTyp.Where(x => x == "Search By QSID").FirstOrDefault();
            NotifyPropertyChanged("SelectedQSIDSrch");
            CommandTabChanged = new RelayCommand(CommandTabChangedExecute, CommandTabChangedCanExecute);
            CommandGenerateAccessCasResult = new RelayCommand(CommandAccessExecute, CommandAccessCanExecute);
            SearchCommand = new RelayCommand(SearchCommandExecute, CommandAccessCanExecute);
            ShowCommand = new RelayCommand(ShowCommandExecute, CommandAccessCanExecute);
            var result1 = new List<string>();
            result1.Add("Phrase" );
            result1.Add("Units" );
            result1.Add("Notes" );
            result1.Add("Identifiers");
            listTypeName = new ObservableCollection<string>(result1);
            NotifyPropertyChanged("listTypeName");
            

            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(CurrentData_ConflictGenerics_VM), NotifyRefreshGrid);
            MessengerInstance.Register<NotificationMessageAction<string>>(this,  typeof(Win_Administrator_VM), TabChangedEvent);
            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenCurrentDataViewFromVersionHis)
            {
                IsSelectedTabAdditionalInfo = false;
                IsSelectedTabData = true;
                dataTableMakeEmpty();
                Qsid_ID = VersionControlSystem.Model.ApplicationEngine.Engine.CurrentDataViewQsidID;
                QsidText = VersionControlSystem.Model.ApplicationEngine.Engine.CurrentDataViewQsid;
                Qsid = QsidText;
                VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenCurrentDataViewFromVersionHis = false;
                showLabelofNormalization(Qsid_ID);

            }
            if (VersionControlSystem.Model.ApplicationEngine.Engine.IsOpenAdditionalInfoViewFromVersionHis)
            {
                IsSelectedTabData = false;
                IsSelectedTabAdditionalInfo = true;
                dataTableMakeEmpty();

                Qsid_ID = VersionControlSystem.Model.ApplicationEngine.Engine.CurrentDataViewQsidID;
                QsidText = VersionControlSystem.Model.ApplicationEngine.Engine.CurrentDataViewQsid;
                Qsid = QsidText;
                showLabelofNormalization(Qsid_ID);
            }
            
            CurrentUniquePhrase_VM = new CommonDataGrid_ViewModel<CurrentPh>(GetListGridColumnUniquePhrase(), "Unique-Phrase", "UniquePhrase");
            ConflictGenerics_VM = new CommonDataGrid_ViewModel<CurrentData_ConflictGenerics_VM>(GetColumnListConflictGrid(), "", "ConflictingGenerics");
            ActiveEnabledParentChildTab();
        }
        
        private void TabChangedEvent(NotificationMessageAction<string> obj)
        {
            IsSelectedTabDataGenericsCount = true;
            NotifyPropertyChanged("IsSelectedTabDataGenericsCount");
        }

        private void ShowHistoryExecute()
        {
            objRowHistory = new RowHistory_VM();
            NotifyPropertyChanged("objRowHistory");
        }
        private void NotifyRefreshGrid(PropertyChangedMessage<string> obj)
        {
            BindConflictGenerics();
        }
        public static string Qsid { get; set; }
        private void showLabelofNormalization(int Qsid_ID)
        {
            using (var context = new CRAModel())
            {
                var NormalizeInd = context.Library_SessionTypes.AsNoTracking().Where(x => x.SessionType == "Normalization - Individual list").Select(y => y.SessionTypeID).FirstOrDefault();
                var NormalizeBulk = context.Library_SessionTypes.AsNoTracking().Where(x => x.SessionType == "Normalization - Bulk").Select(y => y.SessionTypeID).FirstOrDefault();
                var currentSessionID = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Qsid_ID && (x.SessionTypeID == NormalizeInd || x.SessionTypeID == NormalizeBulk)).Select(x => new { x.SessionID, x.SessionBegin_TimeStamp, x.QSID_Version }).OrderByDescending(y => y.SessionID).FirstOrDefault();
                if (currentSessionID != null)
                {
                    var checkNormalizedStatus = context.Log_Sessions_Detail.AsNoTracking().Count(x => x.SessionID == currentSessionID.SessionID && x.Description == "Data-Analysis");
                    var chkStatus = checkNormalizedStatus == 0 || checkNormalizedStatus == 2 ? "Complete" : "InComplete";
                    MdbVersion = "Displaying data corresponding to: Mdb Version- " + currentSessionID.QSID_Version.ToString() + "; SessionID- " + currentSessionID.SessionID + "(" + chkStatus + "); Normalized on-" + currentSessionID.SessionBegin_TimeStamp.ToString("dd-MMM-yyyy h:mm:ss") + " EST";
                    NotifyPropertyChanged("MdbVersion");
                }
            }
        }


        private void ShowCommandExecute(object obj)
        {
            CallUniqueValues();
        }
        private void SearchCommandExecute(object obj)
        {
            using (var context = new CRAModel())
            {
                dataTableMakeEmpty();
                if (SelectedQSIDSrch == "Search By QSID")
                {
                    Qsid = QsidText;
                }
                if (SelectedQSIDSrch == "Search By QSID_ID")
                {
                    var qsid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID_ID.ToString() == QsidText).Select(y => y.QSID).FirstOrDefault();
                    Qsid = qsid;
                }
                if (SelectedQSIDSrch == "Search By List_Field_Name")
                {
                    var listFieldCat = context.Library_PhraseCategories.AsNoTracking().Where(x => x.PhraseCategory == "List Field Name").Select(y => y.PhraseCategoryID).FirstOrDefault();
                    var phrasid = context.Library_Phrases.AsNoTracking().Where(x => x.Phrase == QsidText.Trim() && x.LanguageID == 1).Select(y => y.PhraseID).ToList();
                    var qsxxPhraseQsid = context.QSxxx_ListDictionary.AsNoTracking().Where(x => x.PhraseCategoryID == listFieldCat && phrasid.Contains(x.PhraseID)).Select(y => y.QSID_ID).FirstOrDefault();
                    var qsid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID_ID == qsxxPhraseQsid).Select(y => y.QSID).FirstOrDefault();
                    Qsid = qsid;
                }
                if (AllCas.Trim() != string.Empty)
                {
                    FilterCas = AllCas.Split(",").ToList();
                }
                var libraryQsid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID == Qsid).Select(y => y.QSID_ID).FirstOrDefault();
                Qsid_ID = libraryQsid;
                showLabelofNormalization(Qsid_ID);
                SelectedTabItemChanged();
                ActiveEnabledParentChildTab();


            }
        }
        public void ActiveEnabledParentChildTab()
        {
            using (var context = new CRAModel())
            {
                if (!string.IsNullOrEmpty(Qsid))
                {
                    if (!Qsid.ToUpper().Trim().Contains("QSNI") && !Qsid.ToUpper().Trim().Contains("QSPC") && !Qsid.ToUpper().Trim().Contains("QSTO") && !Qsid.ToUpper().Trim().Contains("QSEC"))
                    {
                        IsEnabledParentChildTab = true;

                        CallCurrentView();
                        CallDataWithParentChild();
                        BindCurrentGenerics();
                        BindConflictGenerics();

                    }
                    else
                    {
                        IsEnabledParentChildTab = false;
                        CallCurrentView();
                    }
                    NotifyPropertyChanged("IsEnabledParentChildTab");
                }
                else
                {
                    var qsid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID_ID.ToString() == QsidText).Select(y => y.QSID).FirstOrDefault();
                    if (qsid != null)
                    {
                        Qsid = qsid;
                        CallCurrentView();
                    }
                }
            }
        }

        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }
        private void CommandAccessExecute(object obj)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
            };
            folderDlg.SelectedPath = "C:\\CRAWork\\VersionControl";
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                var objExportFile = new ExportFile(folderDlg.SelectedPath);
                System.Windows.MessageBox.Show("File Created Successfully at " + folderDlg.SelectedPath, "Information");
            }
        }
        private bool CommandTabChangedCanExecute(object obj)
        {
            return true;
        }
        private void CommandTabChangedExecute(object obj)
        {
        }
        public TabItem SelectedTabItem
        {
            get
            {
                return selectedTabItem;
            }
            set
            {
                if (value != selectedTabItem)
                {
                    selectedTabItem = value;
                    SelectedTabItemChanged();
                }
            }
        }
        public void SelectedTabItemChanged()
        {
            if (Qsid != string.Empty && Qsid != null)
            {
                switch (SelectedTabItem.Header)
                {
                    case "Mdb view":
                        //CallCurrentView();
                        break;
                    case "Data + Generics counts":
                        //CallDataWithParentChild();
                        break;
                    case "List Dictionary":
                        CallListDictionary();
                        break;
                    case "Data Dictionary":
                        CallDataDictionary();
                        break;
                    case "Notes":
                        CallNotes();
                        break;
                    case "Legislation Info":
                        CallLegislation();
                        break;
                    //case "List Generics":
                    //    CallParentChild();
                    //    break;
                    case "Character Detail":
                        CallCharacterDetail();
                        break;
                    case "Deleted Rows":
                        CallDeletedRows();
                        break;
                    case "Additional Info":
                        CallAdditionalInfo();
                        break;
                    case "Unique Criteria":
                        CallUniqueCriteria();
                        break;
                    case "Substance History":
                        CallSubstanceHistory();
                        break;
                    case "Product Fields":
                        CallProductField();
                        break;
                    case "Add/Delete in Same Version":
                        CallAddDeleteByVersion();
                        break;
                    case "Deleted then added between prod. versions":
                        CallProductionVersionChanges();
                        break;
                    case "Unique Phrases":
                        CallUniquePhrase();
                        break;
                    case "EC/MF/Syn Mapping":
                        CallMapCASToEC();
                        break;
                    case "Generics Changes":
                        CallGenericAddedDeleted();
                        break;
                    case "Translated Phrases":
                        CallTransPhrases();
                        break;
                    case "Listed Group Members":
                        CallListGroupMembers();
                        break;
                    case "Row History":
                        ShowHistoryExecute();
                        break;
                       //case "Conflicting Generics":
                        //    BindConflictGenerics();
                        //    break;
                        //case "Data And Generics (expanded)":
                        //    BindCurrentGenerics();
                        //    break;
                }
            }
        }
        private ObservableCollection<CurrentPh> _lstCurrentPh { get; set; } = new ObservableCollection<CurrentPh>();
        public ObservableCollection<CurrentPh> lstCurrentPh
        {
            get { return _lstCurrentPh; }
            set
            {
                if (_lstCurrentPh != value)
                {
                    _lstCurrentPh = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<CurrentPh>>>(new PropertyChangedMessage<List<CurrentPh>>(null, _lstCurrentPh.ToList(), "Default List"));
                }
            }
        }
        public Visibility VisibleLodeUniquePh { get; set; }
        public void CallUniquePhrase()
        {
            Task.Run(() =>
            {
                VisibleLodeUniquePh = Visibility.Visible;
                NotifyPropertyChanged("VisibleLodeUniquePh");
                //var dt = objCommon.DbaseQueryReturnSqlList<CurrentPh>("select y.phraseid,  y.PhraseFrequency, y.FieldNames,c.languageName, b.Phrase, b.ARCode, b.PhraseKey, " +
                //                                                " len(b.Phrase) as PhraseLength from " +
                //                                                " (select x.PhraseID, x.PhraseFrequency, stuff(( " +
                //                                                " select distinct ',' + b.FieldName " +
                //                                                " from QSxxx_Phrases u, Library_FieldNames b " +
                //                                                " where u.FieldNameID = b.FieldNameID and u.QSID_ID = x.QSID_ID and u.PhraseID = x.PhraseID " +
                //                                                " for xml path('') " +
                //                                                " ),1,1,'') as FieldNames from " +
                //                                                " (select a.QSID_ID, a.PhraseID, count(*) as PhraseFrequency  from QSxxx_Phrases a " +
                //                                                " where a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " group by a.QSID_ID, a.PhraseID) as x) as y, Library_Phrases b, Library_Languages c where y.PhraseID = b.PhraseID and b.LanguageID = c.LanguageID order by y.PhraseID", Win_Administrator_VM.CommonListConn);

                var dt = objCommon.DbaseQueryReturnSqlList<CurrentPh>("exec SP_GetUniquePhrases_CurrentView " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                int gpNumber = 1;
                var OnlyDup = dt.Where(x => x.IsDuplicate == true).ToList();
                foreach (var ct in OnlyDup)
                {
                    if (ct.IsDuplicate == true && ct.GroupNumber == 0)
                    {
                        dt.Where(x => x.SquashAlphaPhrase == ct.SquashAlphaPhrase).ToList().ForEach(x =>
                         {
                             x.GroupNumber = gpNumber;
                         });
                        gpNumber += 1;
                    }
                }
                lstCurrentPh = new ObservableCollection<CurrentPh>(dt);
                VisibleLodeUniquePh = Visibility.Collapsed;
                NotifyPropertyChanged("VisibleLodeUniquePh");
            });

        }
        public void BindConflictGenerics()
        {
            loderConflictingGenericsVisibility = Visibility.Visible;
            NotifyPropertyChanged("loderConflictingGenericsVisibility");
            Task.Run(() =>
            {
                var listConflict = new List<CurrentData_ConflictGenerics_VM>();
                CurrentData_ConflictGenerics_VM objConflict;
                int conflictGroupNumber = 0;
                foreach (var item in _objLibraryFunction_Service.GetConflictedGenerics(Qsid))
                {
                    string ConflictGroup = string.Empty;
                    if (listConflict.Any(x => x.ChildCas == item.ChildCas))
                    {
                        ConflictGroup = listConflict.Where(x => x.ChildCas == item.ChildCas).Select(x => x.ConflictingGroup).FirstOrDefault();
                    }
                    else
                    {
                        conflictGroupNumber = conflictGroupNumber + 1;
                        ConflictGroup = "G" + conflictGroupNumber;
                    }
                    objConflict = new CurrentData_ConflictGenerics_VM();
                    objConflict.ChildCas = item.ChildCas;
                    objConflict.ChildCasID = item.ChildCasID;
                    objConflict.ParentCas = item.ParentCas;
                    objConflict.ParentCasID = (int)item.ParentCasID;
                    objConflict.ChemicalName = item.ChemicalName;
                    objConflict.ParentChemicalName = item.ParentChemicalName;
                    objConflict.ConflictingGroup = ConflictGroup;
                    listConflict.Add(objConflict);
                }
                listConflictGenerics = new ObservableCollection<CurrentData_ConflictGenerics_VM>(listConflict);
                loderConflictingGenericsVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("loderConflictingGenericsVisibility");
            });
        }
        public void BindCurrentGenerics()
        {
            _objCurrentGenericData = new CurrentGenericData_VM(Qsid);
            NotifyPropertyChanged("_objCurrentGenericData");
        }
        private List<CommonDataGridColumn> GetListGridColumnUniquePhrase()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseID", ColBindingName = "PhraseID", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase", ColBindingName = "Phrase", ColType = "Textbox", ColumnWidth = "500" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Language", ColBindingName = "languageName", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Frequency", ColBindingName = "PhraseFrequency", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "FieldName", ColBindingName = "FieldNames", ColType = "Textbox", ColumnWidth = "250" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "ARCode", ColBindingName = "ARCode", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "PhraseKey", ColBindingName = "PhraseKey", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Phrase Length", ColBindingName = "PhraseLength", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "SquashAlphaPhrase", ColBindingName = "SquashAlphaPhrase", ColType = "Textbox", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "IsDuplicate", ColBindingName = "IsDuplicate", ColType = "CheckBoxEnable", ColumnWidth = "150" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "GroupNumber", ColBindingName = "GroupNumber", ColType = "Textbox", ColumnWidth = "150" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Action", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Delete", BackgroundColor = "#ff414d", CommandParam = "DeleteMapPropChar" });
            //listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Add/Edit Name", ColBindingName = "MapPropertyCharacteristicID", ColType = "Button", ColumnWidth = "100", ContentName = "Update", BackgroundColor = "#07689f", CommandParam = "AddEditNamePropChar" });
            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetColumnListConflictGrid()
        {
            List<CommonDataGridColumn> listColConflictGrid = new List<CommonDataGridColumn>();
            listColConflictGrid.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Cas", ColBindingName = "ParentCas", ColType = "Textbox", ColumnWidth = "150" });
            listColConflictGrid.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Parent Name", ColBindingName = "ParentChemicalName", ColType = "Textbox", ColumnWidth = "150" });
            listColConflictGrid.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Cas", ColBindingName = "ChildCas", ColType = "Textbox", ColumnWidth = "150" });
            listColConflictGrid.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Child Name", ColBindingName = "ChemicalName", ColType = "Textbox", ColumnWidth = "150" });
            listColConflictGrid.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Conflicting Group", ColBindingName = "ConflictingGroup", ColType = "Textbox", ColumnWidth = "150" });
            return listColConflictGrid;
        }

        public void CallAdditionalInfo()
        {
            objAdditionalInfoHistory = new AdditionalInfoHistory_VM();
            NotifyPropertyChanged("objAdditionalInfoHistory");
        }

        public void CallUniqueCriteria()
        {
            objUniqueCriteriaHistory = new UniqueCriteria_VM();
            NotifyPropertyChanged("objUniqueCriteriaHistory");
        }
        
        public void CallUniqueValues()
        {
            objUniqueValues = new UniqueValues_VM();
            NotifyPropertyChanged("objUniqueValues");
        }
        public void CallCurrentView()
        {
            objCurrentDataView = new CurrentData_VM();
            NotifyPropertyChanged("objCurrentDataView");
        }
        public void CallDataWithParentChild()
        {
            objParentChild = new ParentChildData_VM();
            NotifyPropertyChanged("objParentChild");
        }
        public void CallListDictionary()
        {
            objListDictionary = new ListDictionary_VM();
            NotifyPropertyChanged("objListDictionary");
        }
        public void CallListGroupMembers()
        {
            objListedGroupMembers = new ListedGroupMembers_VM();
            NotifyPropertyChanged("objListedGroupMembers");
        }
        public void CallDataDictionary()
        {
            objDataDictionary = new DataDictionary_VM();
            NotifyPropertyChanged("objDataDictionary");
        }
        public void CallNotes()
        {
            objNotes = new Notes_VM();
            NotifyPropertyChanged("objNotes");
        }
        public void CallTransPhrases()
        {
            objTransPhrases = new TransPhrases_VM();
            NotifyPropertyChanged("objTransPhrases");
        }
        public void CallLegislation()
        {
            objLegislation = new Legislation_VM();
            NotifyPropertyChanged("objLegislation");
        }
        public void CallMapCASToEC()
        {
            objMapCAStoEC = new MapCASToEC_VM();
            NotifyPropertyChanged("objMapCAStoEC");
        }
        public void CallGenericAddedDeleted()
        {
            objGenericAddedDeleted = new GenericAddedDeletedLog_VM();
            NotifyPropertyChanged("objGenericAddedDeleted");
        }

        public void CallParentChild()
        {
            objParentChildRel = new ParentChildRelationShip_VM();
            NotifyPropertyChanged("objParentChildRel");
        }
        public void CallCharacterDetail()
        {
            objCharacterDetail = new CharacterDetail_VM();
            NotifyPropertyChanged("objCharacterDetail");
        }
        public void CallSubstanceHistory()
        {
            objSubstanceHistory = new SubstanceHistory_VM();
            NotifyPropertyChanged("objSubstanceHistory");
        }

        public void CallProductField()
        {
            objProductFields = new ProductFields_VM();
            NotifyPropertyChanged("objProductFields");
        }
        public void CallAddDeleteByVersion()
        {
            objAddDeleteInSameVersion = new CurrentDataViewByVersion_VM();
            NotifyPropertyChanged("objAddDeleteInSameVersion");
        }
        public void CallProductionVersionChanges()
        {
            objProductionVersion = new ProductionVersionChanges_VM();
            NotifyPropertyChanged("objProductionVersion");
        }
    }
    public class CurrentPh
    {
        public int PhraseID { get; set; }
        public string Phrase { get; set; }
        public string languageName { get; set; }
        public int PhraseFrequency { get; set; }
        public string FieldNames { get; set; }
        public string ARCode { get; set; }
        public string PhraseKey { get; set; }
        public int PhraseLength { get; set; }
        public string SquashAlphaPhrase { get; set; }
        public bool IsDuplicate { get; set; }
        public int GroupNumber { get; set; }
    }
}
