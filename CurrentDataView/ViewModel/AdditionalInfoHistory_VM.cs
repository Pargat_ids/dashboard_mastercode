﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class AdditionalInfoHistory_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listAddInfo { get; set; }
        public StandardDataGrid_VM listAddInfoHistory { get; set; }
        private RowIdentifier obj = new RowIdentifier();
        public Visibility VisibleLoderAddInfo { get; set; }
        public AdditionalInfoHistory_VM()
        {
            Task.Run(() => RunFunc());
        }
        private void RunFunc()
        {
            VisibleLoderAddInfo = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderAddInfo");
            if (Win_Administrator_VM.AddInfo.Columns.Count == 0)
            {
                Win_Administrator_VM.AddInfo = obj.GetAdditionalInfo(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            if (Win_Administrator_VM.AddInfoHistory.Columns.Count == 0)
            {
                Win_Administrator_VM.AddInfoHistory = obj.GetAdditionalInfoHistory(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }

            listAddInfo = new StandardDataGrid_VM("Additional Info", Win_Administrator_VM.AddInfo,Visibility.Visible);
            NotifyPropertyChanged("listAddInfo");
            listAddInfoHistory = new StandardDataGrid_VM("Additional Info History", Win_Administrator_VM.AddInfoHistory, Visibility.Visible);
            NotifyPropertyChanged("listAddInfoHistory");
            VisibleLoderAddInfo = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderAddInfo");
        }
    }
}
