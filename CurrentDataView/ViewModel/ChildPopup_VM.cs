﻿using System.Windows;
using System.Data;

namespace CurrentDataView.ViewModel
{
    public class ChildPopup_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstpopup { get; set; }

        public ChildPopup_VM(DataTable dt)
        {
            lstpopup = new StandardDataGrid_VM("Child Popup", dt, Visibility.Hidden);
            NotifyPropertyChanged("lstpopup");
        }
    }
}
