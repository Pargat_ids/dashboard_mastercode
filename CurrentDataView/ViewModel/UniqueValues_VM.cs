﻿using System.Data;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using System.Linq;
using System;

namespace CurrentDataView.ViewModel
{
    public class UniqueValues_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstUniqueValuesNew { get; set; }
        public Visibility VisibleLoderUniqueValuesNew { get; set; }
        private CommonFunctions objcommon = new CommonFunctions();
        public string ErrorMsgs { get; set; }
        public UniqueValues_VM()
        {
            Task.Run(() => RunUniqueValues());
        }

        public void RunUniqueValues()
        {
            VisibleLoderUniqueValuesNew = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderUniqueValuesNew");
            var dt = getDetail();
            if (dt.Rows.Count == 0)
            {
                ErrorMsgs = "No Record Found";
                NotifyPropertyChanged("ErrorMsgs");
                lstUniqueValuesNew = null;

            }
            else
            {
                string hiddenColumn = string.Empty;
                switch (Win_Administrator_VM.SelectType)
                {
                    case "Phrase":
                        hiddenColumn = "PhraseID";
                        break;
                    case "Units":
                        hiddenColumn = "UnitID";
                        break;
                    case "Identifiers":
                        hiddenColumn = "IdentifierID";
                        break;
                    case "Notes":
                        hiddenColumn = "NoteCodeID";
                        break;
                }
                lstUniqueValuesNew = new StandardDataGrid_VM("Data", dt, Visibility.Visible, false, false, null, hiddenColumn);
                ErrorMsgs = string.Empty;
                NotifyPropertyChanged("ErrorMsgs");
            }
            NotifyPropertyChanged("lstUniqueValuesNew");
            VisibleLoderUniqueValuesNew = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderUniqueValuesNew");

        }

        private DataTable getDetail()
        {

            DataTable dtFinal = new DataTable();
            DataTable dt = new DataTable();
            DataTable dtMain = new DataTable();
            DataTable dtFieldNames = new DataTable();
            switch (Win_Administrator_VM.SelectType)
            {
                case "Phrase":
                    dtFinal.Columns.Add("PhraseID");
                    dtFinal.Columns.Add("Phrase");

                    dt = objcommon.DbaseQueryReturnTableSql("WITH cte AS (" +
                           " SELECT PhraseID, Status, timestamp, SessionID, " +
                           " ROW_NUMBER() OVER(PARTITION BY PhraseID, Status ORDER BY PhraseID, Status, timestamp) row_num " +
                           " FROM Log_QSxxx_Phrases where qsid_Id =" + Win_Administrator_VM.Qsid_ID + " and status  in ('A', 'D')) " +
                           " select a.PhraseID as ID, Phrase as 'Descr', a.Status, a.timestamp as CurDate, b.timeStamp as FirstLibDate, " +
                           " e.FirstName + ' ' + e.LastName as UName , a.SessionID , a.row_num " +
                           " FROM cte a inner join Library_Phrases b on a.PhraseID = b.PhraseID " +
                           " left join Log_Sessions c on a.SessionID = c.SessionID " +
                           " left join library_Users e on e.veriskID = c.username " +
                           " WHERE row_num = 1", Win_Administrator_VM.CommonListConn);
                    dtMain = objcommon.DbaseQueryReturnTableSql("select distinct PhraseID as Id from QSxxx_Phrases where qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                    dtFieldNames = objcommon.DbaseQueryReturnTableSql("select distinct a.PhraseID as Id, b.FieldName from Log_QSxxx_Phrases a, library_FieldNames b " +
                        " where a.FieldNameID = b.FieldNameID and qsid_id = " + Win_Administrator_VM.Qsid_ID + " and status ='A'", Win_Administrator_VM.CommonListConn);
                    break;
                case "Units":
                    dtFinal.Columns.Add("UnitID");
                    dtFinal.Columns.Add("Unit");
                    dt = objcommon.DbaseQueryReturnTableSql("WITH cte AS (" +
                           " SELECT UnitID, Status, timestamp, SessionID, " +
                           " ROW_NUMBER() OVER(PARTITION BY UnitID, Status ORDER BY UnitID, Status, timestamp) row_num " +
                           " FROM Log_QSxxx_Values where unitid is not null and qsid_Id =" + Win_Administrator_VM.Qsid_ID + " and status  in ('A', 'D')) " +
                           " select a.UnitID as ID, Unit as 'Descr', a.Status, a.timestamp as CurDate, b.timeStamp as FirstLibDate, " +
                           " e.FirstName + ' ' + e.LastName as UName , a.SessionID , a.row_num " +
                           " FROM cte a inner join Library_Units b on a.UnitID = b.UnitID " +
                           " left join Log_Sessions c on a.SessionID = c.SessionID " +
                           " left join library_Users e on e.veriskID = c.username " +
                           " WHERE row_num = 1", Win_Administrator_VM.CommonListConn);
                    dtMain = objcommon.DbaseQueryReturnTableSql("select distinct UnitID  as Id  from QSxxx_Values where UnitId is not null and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                    dtFieldNames = objcommon.DbaseQueryReturnTableSql("select distinct a.UnitID  as Id, b.FieldName from Log_QSxxx_Values a, library_FieldNames b " +
                        " where a.UnitsFieldID = b.FieldNameID and unitid is not null and qsid_id = " + Win_Administrator_VM.Qsid_ID + " and status ='A'", Win_Administrator_VM.CommonListConn);
                    break;
                case "Identifiers":
                    dtFinal.Columns.Add("IdentifierID");
                    dtFinal.Columns.Add("Identifier");
                    dt = objcommon.DbaseQueryReturnTableSql("WITH cte AS (" +
                           " SELECT IdentifierValueID, Status, timestamp, SessionID, " +
                           " ROW_NUMBER() OVER(PARTITION BY IdentifierValueID, Status ORDER BY IdentifierValueID, Status, timestamp) row_num " +
                           " FROM Log_QSxxx_Identifiers where qsid_Id =" + Win_Administrator_VM.Qsid_ID + " and status   in ('A', 'D')) " +
                           " select a.IdentifierValueID as ID, IdentifierValue as 'Descr', a.Status, a.timestamp as CurDate, b.timeStamp as FirstLibDate, " +
                           " e.FirstName + ' ' + e.LastName as UName , a.SessionID , a.row_num " +
                           " FROM cte a inner join Library_Identifiers b on a.IdentifierValueID = b.IdentifierValueID " +
                           " left join Log_Sessions c on a.SessionID = c.SessionID " +
                           " left join library_Users e on e.veriskID = c.username " +
                           " WHERE row_num = 1", Win_Administrator_VM.CommonListConn);
                    dtMain = objcommon.DbaseQueryReturnTableSql("select distinct IdentifierValueID  as Id  from QSxxx_Identifiers where qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                    dtFieldNames = objcommon.DbaseQueryReturnTableSql("select distinct a.IdentifierValueID  as Id, b.FieldName from Log_QSxxx_Identifiers a, library_FieldNames b " +
                        " where a.FieldNameID = b.FieldNameID and qsid_id = " + Win_Administrator_VM.Qsid_ID + " and status ='A'", Win_Administrator_VM.CommonListConn);
                    break;
                case "Notes":
                    dtFinal.Columns.Add("PhraseID");
                    dtFinal.Columns.Add("Phrase");
                    dtFinal.Columns.Add("NoteCodeID");
                    dtFinal.Columns.Add("NoteCode");
                    dt = objcommon.DbaseQueryReturnTableSql("WITH cte AS ( " +
                        " SELECT PhraseID, NoteCodeID, Status, timestamp, SessionID, " +
                        " ROW_NUMBER() OVER(PARTITION BY PhraseID, NoteCodeID, Status ORDER BY PhraseID, NoteCodeID, Status, timestamp) row_num " +
                        " FROM Log_QSxxx_Notes where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and status  in ('A', 'D')) " +
                        " select a.PhraseID as ID, Phrase as 'Descr', a.NoteCodeID, c.NoteCode, a.Status, a.timestamp as CurDate, b.timeStamp as FirstLibDate, " +
                        " e.FirstName + ' ' + e.LastName as UName , a.SessionID , a.row_num " +
                        " FROM cte a inner join Library_Phrases b on a.PhraseID = b.PhraseID " +
                        " inner join Library_NoteCodes c on a.NoteCodeID = c.NoteCodeID " +
                        " left join Log_Sessions d on a.SessionID = d.SessionID  " +
                        " left join library_Users e on e.veriskID = d.username " +
                        " WHERE row_num = 1 ", Win_Administrator_VM.CommonListConn);
                    dtMain = objcommon.DbaseQueryReturnTableSql("select distinct PhraseID as Id, NoteCodeID    from QSxxx_Notes where qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
                    dtFieldNames = objcommon.DbaseQueryReturnTableSql("select distinct a.PhraseID  as Id, NoteCodeID, b.FieldName from Log_QSxxx_Notes a, library_FieldNames b " +
                        " where a.FieldNameID = b.FieldNameID and qsid_id = " + Win_Administrator_VM.Qsid_ID + " and status ='A'", Win_Administrator_VM.CommonListConn);
                    break;
            }



            dtFinal.Columns.Add("Currently_Active", typeof(bool));
            dtFinal.Columns.Add("Date_First_Added_To_List", typeof(DateTime));
            dtFinal.Columns.Add("Date_Deleted", typeof(DateTime));
            dtFinal.Columns.Add("Date_First_Added_To_Library", typeof(DateTime));
            dtFinal.Columns.Add("FieldNames", typeof(string));
            dtFinal.Columns.Add("user_firstAdded'", typeof(string));
            dtFinal.Columns.Add("SessionID_firstAdded2List");
            dtFinal.Columns.Add("user_lastDeleted", typeof(string));
            dtFinal.Columns.Add("SessionID_lastDeleted");

            DataTable dtAdd = new DataTable();
            var onlyAdd = dt.AsEnumerable().Where(x => x.Field<string>("Status") == "A");
            if (onlyAdd.Any())
            {
                dtAdd = onlyAdd.CopyToDataTable();
            }

            DataTable dtDelete = new DataTable();
            var onlyDelete = dt.AsEnumerable().Where(x => x.Field<string>("Status") == "D");
            if (onlyDelete.Any())
            {
                dtDelete = onlyDelete.CopyToDataTable();
            }

            if (Win_Administrator_VM.SelectType != "Notes")
            {
                var dtJoinFieldnames = (from d1 in dtFieldNames.AsEnumerable()
                                        group d1.Field<string>("FieldName") by d1.Field<Int32>("Id")
                                   into tg
                                        select new
                                        {
                                            ID = tg.Key,
                                            FieldName = string.Join(",", tg.ToList())
                                        }).ToList();

                var prepreFinal = (from d1 in dtAdd.AsEnumerable()
                                   join d2 in dtMain.AsEnumerable()
                                   on d1.Field<Int32>("Id") equals d2.Field<Int32>("Id")
                                   into tg
                                   from tcheck in tg.DefaultIfEmpty()
                                   select new
                                   {
                                       ID = d1.Field<Int32>("ID"),
                                       Descr = d1.Field<string>("Descr"),
                                       Status = d1.Field<string>("Status"),
                                       Date_First_Added_To_List = d1.Field<DateTime>("CurDate"),
                                       Date_First_Added_To_Library = (DateTime?)d1.Field<dynamic>("FirstLibDate"),
                                       user_firstAdded = d1.Field<string>("UName"),
                                       SessionID_firstAdded2List = (int?)d1.Field<dynamic>("SessionID"),
                                       Currently_Active = tcheck != null ? true : false,
                                   }).ToList();

                var preFinal = (from d1 in prepreFinal
                                join d2 in dtDelete.AsEnumerable()
                                   on d1.ID equals d2.Field<Int32>("Id")
                                   into tg
                                from tcheck in tg.DefaultIfEmpty()
                                select new
                                {
                                    d1.ID,
                                    d1.Descr,
                                    d1.Status,
                                    d1.Date_First_Added_To_List,
                                    d1.Date_First_Added_To_Library,
                                    d1.user_firstAdded,
                                    d1.SessionID_firstAdded2List,
                                    d1.Currently_Active,
                                    Date_Deleted = tcheck != null && d1.Currently_Active == false ? (DateTime?)tcheck.Field<dynamic>("CurDate") : null,
                                    user_lastDeleted = tcheck != null && d1.Currently_Active == false ? tcheck.Field<string>("Uname") : null,
                                    SessionID_lastDeleted = tcheck != null && d1.Currently_Active == false ? (int?)tcheck.Field<dynamic>("SessionID") : null,
                                }).ToList();
                var Final = (from d1 in preFinal
                             join d2 in dtJoinFieldnames
                                on d1.ID equals d2.ID
                                into tg
                             from tcheck in tg.DefaultIfEmpty()
                             select new
                             {
                                 d1.ID,
                                 d1.Descr,
                                 d1.Status,
                                 d1.Date_First_Added_To_List,
                                 d1.Date_First_Added_To_Library,
                                 d1.user_firstAdded,
                                 d1.SessionID_firstAdded2List,
                                 d1.Currently_Active,
                                 d1.Date_Deleted,
                                 d1.user_lastDeleted,
                                 d1.SessionID_lastDeleted,
                                 FieldNames = tcheck == null ? string.Empty : tcheck.FieldName
                             }).ToList();
                for (int i = 0; i < Final.Count; i++)
                {
                    dtFinal.Rows.Add
                         (
                         Final[i].ID,
                         Final[i].Descr,
                         Final[i].Currently_Active,
                         Final[i].Date_First_Added_To_List,
                         Final[i].Date_Deleted,
                         Final[i].Date_First_Added_To_Library,
                         Final[i].FieldNames,
                         Final[i].user_firstAdded,
                         Final[i].SessionID_firstAdded2List,
                         Final[i].user_lastDeleted,
                         Final[i].SessionID_lastDeleted
                         );
                }

            }
            else
            {
                var dtJoinFieldnames = (from d1 in dtFieldNames.AsEnumerable()
                                        group d1.Field<string>("FieldName") by new
                                        {
                                            Id = d1.Field<Int32>("Id"),
                                            NoteCodeID = d1.Field<Int32>("NoteCodeID")
                                        }
                                        into tg
                                        select new
                                        {
                                            ID = tg.Key.Id,
                                            NoteCodeID = tg.Key.NoteCodeID,
                                            FieldName = string.Join(",", tg.ToList())
                                        }).ToList();
                var prepreFinal = (from d1 in dtAdd.AsEnumerable()
                                   join d2 in dtMain.AsEnumerable()
                                   on new
                                   {
                                       a = d1.Field<dynamic>("Id"),
                                       b = d1.Field<dynamic>("NoteCodeID")
                                   } equals new
                                   {
                                       a = d2.Field<dynamic>("Id"),
                                       b = d2.Field<dynamic>("NoteCodeID")
                                   }
                                   into tg
                                   from tcheck in tg.DefaultIfEmpty()
                                   select new
                                   {
                                       ID = d1.Field<Int32>("ID"),
                                       Descr = d1.Field<string>("Descr"),
                                       NoteCodeID = (int?)d1.Field<Int32>("NoteCodeID"),
                                       NoteCode = d1.Field<string>("NoteCode"),
                                       Status = d1.Field<string>("Status"),
                                       Date_First_Added_To_List = d1.Field<DateTime>("CurDate"),
                                       Date_First_Added_To_Library = (DateTime?)d1.Field<dynamic>("FirstLibDate"),
                                       user_firstAdded = d1.Field<string>("UName"),
                                       SessionID_firstAdded2List = (int?)d1.Field<dynamic>("SessionID"),
                                       Currently_Active = tcheck != null ? true : false,
                                   }).ToList();
                var preFinal = (from d1 in prepreFinal
                                join d2 in dtDelete.AsEnumerable()
                                   on new
                                   {
                                       a = d1.ID,
                                       b = (int)d1.NoteCodeID
                                   } equals new
                                   {
                                       a = d2.Field<Int32>("Id"),
                                       b = d2.Field<Int32>("NoteCodeID")
                                   }
                                   into tg
                                from tcheck in tg.DefaultIfEmpty()
                                select new
                                {
                                    d1.ID,
                                    d1.Descr,
                                    d1.NoteCodeID,
                                    d1.NoteCode,
                                    d1.Status,
                                    d1.Date_First_Added_To_List,
                                    d1.Date_First_Added_To_Library,
                                    d1.user_firstAdded,
                                    d1.SessionID_firstAdded2List,
                                    d1.Currently_Active,
                                    Date_Deleted = tcheck != null && d1.Currently_Active == false ? (DateTime?)tcheck.Field<dynamic>("CurDate") : null,
                                    user_lastDeleted = tcheck != null && d1.Currently_Active == false ? tcheck.Field<string>("Uname") : null,
                                    SessionID_lastDeleted = tcheck != null && d1.Currently_Active == false ? (int?)tcheck.Field<dynamic>("SessionID") : null,
                                }).ToList();
                var Final = (from d1 in preFinal
                             join d2 in dtJoinFieldnames
                                on new
                                {
                                    a = d1.ID,
                                    b = (int)d1.NoteCodeID
                                } equals new
                                {
                                    a = d2.ID,
                                    b = d2.NoteCodeID
                                }
                                into tg
                             from tcheck in tg.DefaultIfEmpty()
                             select new
                             {
                                 d1.ID,
                                 d1.Descr,
                                 d1.NoteCodeID,
                                 d1.NoteCode,
                                 d1.Status,
                                 d1.Date_First_Added_To_List,
                                 d1.Date_First_Added_To_Library,
                                 d1.user_firstAdded,
                                 d1.SessionID_firstAdded2List,
                                 d1.Currently_Active,
                                 d1.Date_Deleted,
                                 d1.user_lastDeleted,
                                 d1.SessionID_lastDeleted,
                                 FieldNames = tcheck == null ? string.Empty : tcheck.FieldName
                             }).ToList();

                for (int i = 0; i < Final.Count; i++)
                {
                    dtFinal.Rows.Add
                        (
                        Final[i].ID,
                        Final[i].Descr,
                        Final[i].NoteCodeID,
                        Final[i].NoteCode,
                        Final[i].Currently_Active,
                        Final[i].Date_First_Added_To_List,
                        Final[i].Date_Deleted,
                        Final[i].Date_First_Added_To_Library,
                        Final[i].FieldNames,
                        Final[i].user_firstAdded,
                        Final[i].SessionID_firstAdded2List,
                        Final[i].user_lastDeleted,
                        Final[i].SessionID_lastDeleted
                        );
                }
            }

            if (dtFinal.Rows.Count > 0)
            {
                dtFinal = dtFinal.AsEnumerable().OrderByDescending(x => x.Field<DateTime>("Date_First_Added_To_List")).CopyToDataTable();
            }
            return dtFinal;
        }

    }
}
