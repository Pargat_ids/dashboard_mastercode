﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public class Legislation_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listLegislation { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderLegislation { get; set; }
        public Legislation_VM()
        {
            Task.Run(() => RunLegislation());
        }
        private void RunLegislation()
        {
            VisibleLoderLegislation = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderLegislation");
            if (Win_Administrator_VM.LegislationTable.Columns.Count == 0)
            {
                Win_Administrator_VM.LegislationTable = rowIdentifier.GetLegislation(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            
            listLegislation = new StandardDataGrid_VM("List Legislation", Win_Administrator_VM.LegislationTable, Visibility.Visible);
            NotifyPropertyChanged("listLegislation");
            VisibleLoderLegislation = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderLegislation");
        }
    }
}
