﻿using System;
using System.Collections.Generic;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using System.Data;
using System.Windows.Input;
using CurrentDataView.Library;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Controls;
using System.Linq;
using EntityClass;
using VersionControlSystem.Model.ApplicationEngine;
using ToastNotifications.Messages;
using System.Collections;

namespace CurrentDataView.ViewModel
{
    public class CurrentDataViewByVersion_VM : Base_ViewModel
    {
        public StandardDataGrid_VM lstRowsByVersion { get; set; }
        public StandardDataGrid_VM lstRowsDelBySingle { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public Visibility VisibleLoderRowsByVersion { get; set; }
        public Visibility VisibleLoderDelBySingle { get; set; }
        public TabItem selectedTabItm { get; set; }

        public ICommand DeleteAllByRow { get; set; }
        public ICommand DeleteAllByField { get; set; }
        public TabItem SelectedTabItm
        {
            get
            {
                return selectedTabItm;
            }
            set
            {
                if (value != selectedTabItm)
                {
                    selectedTabItm = value;
                    SelectedTabItemChanged();
                }
            }
        }
        public void SelectedTabItemChanged()
        {
            if (Win_Administrator_VM.Qsid_ID != 0)
            {
                switch (SelectedTabItm.Header)
                {
                    case "Delete By Row":
                        Task.Run(() => RunDeletedRows());
                        break;
                    case "Delete By FieldName":
                       
                        Task.Run(() => RunDeletedSingleValue());
                        break;
                }
            }
        }
        public object SelectedRowItem { get; set; }
        private CommonFunctions objCommon = new CommonFunctions();

        private string DelRowType = string.Empty;
        List<ISingleValue> lsSingle = new List<ISingleValue>();
        public CurrentDataViewByVersion_VM()
        {
            MessengerInstance.Register<PropertyChangedMessage<object>>(this, NotifyMe);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(IList), CallbackSaveBtnClick);
            DeleteAllByRow = new RelayCommand(CommandDeleteByRowExecute, CommandFirstCanExecute);
            DeleteAllByField = new RelayCommand(CommandDeleteByFieldExecute, CommandFirstCanExecute);

            //Task.Run(() => RunDeletedRows());
        }
        private void CommandDeleteByRowExecute(object obj)
        {
            for (int i = 0; i < Win_Administrator_VM.AddedDeletedRows.Rows.Count; i++)
            {
                DeleteByRow(Win_Administrator_VM.AddedDeletedRows.Rows[i].Field<int>("RowId"));
            }
            Win_Administrator_VM.AddedDeletedRows = new DataTable();
            Task.Run(() => RunDeletedRows());
            string msg = "Deleted Successfully";
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                _notifier.ShowSuccess(msg);
            });
        }
        private void CommandDeleteByFieldExecute(object obj)
        {
            for (int i = 0; i < Win_Administrator_VM.AddedDelBySingle.Rows.Count; i++)
            {
                Int32 rowId = Win_Administrator_VM.AddedDelBySingle.Rows[i].Field<int>("RowId");
                string chType = Win_Administrator_VM.AddedDelBySingle.Rows[i].Field<string>("Type");
                int qId = Win_Administrator_VM.AddedDelBySingle.Rows[i].Field<Int32>("QId");
                int fId = Win_Administrator_VM.AddedDelBySingle.Rows[i].Field<int>("FieldNameId");
                string ssesId = Win_Administrator_VM.AddedDelBySingle.Rows[i].Field<string>("SessionID");
                DeleteByField(rowId, chType, qId, fId, ssesId);
            }
            Win_Administrator_VM.AddedDelBySingle = new DataTable();
            Task.Run(() => RunDeletedSingleValue());
            string msg = "Deleted Successfully";
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                _notifier.ShowSuccess(msg);
            });

        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void RunDeletedRows()
        {
            
            if (Win_Administrator_VM.AddedDeletedRows != null && Win_Administrator_VM.AddedDeletedRows.Columns.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                VisibleLoderRowsByVersion = Visibility.Visible;
                NotifyPropertyChanged("VisibleLoderRowsByVersion");
                using (var context = new CRAModel())
                {
                    var latestVersion = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID).Select(y => y.QSID_Version).OrderByDescending(z => z).FirstOrDefault();
                    List<int?> sessIds = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.QSID_Version == latestVersion &&
                    (x.SessionTypeID == 1 || x.SessionTypeID == 5)).Select(z => (int?)z.SessionID).ToList();
                    var result = rowIdentifier.MakeOldDataByVersion(Win_Administrator_VM.Qsid_ID, false, sessIds);
                    if (result != null)
                    {
                        result.Columns.Remove("Status");
                    }
                    Win_Administrator_VM.AddedDeletedRows = result;
                }
            }
            if (Win_Administrator_VM.AddedDeletedRows != null && Win_Administrator_VM.AddedDeletedRows.Columns.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                lstRowsByVersion = new StandardDataGrid_VM("Row Added Deleted By Version", Win_Administrator_VM.AddedDeletedRows, Visibility.Visible, false, true, typeof(IList));
            }
            NotifyPropertyChanged("lstRowsByVersion");
            VisibleLoderRowsByVersion = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderRowsByVersion");
            
            
        }

        private void RunDeletedSingleValue()
        {
            

            if (Win_Administrator_VM.AddedDeletedRows != null && Win_Administrator_VM.AddedDeletedRows.Columns.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                VisibleLoderDelBySingle = Visibility.Visible;
                NotifyPropertyChanged("VisibleLoderDelBySingle");
                using (var context = new CRAModel())
                {
                    var latestVersion = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID).Select(y => y.QSID_Version).OrderByDescending(z => z).FirstOrDefault();
                    List<int?> sessIds = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.QSID_Version == latestVersion &&
                    (x.SessionTypeID == 1 || x.SessionTypeID == 5)).Select(z => (int?)z.SessionID).ToList();
                    var result = rowIdentifier.MakeOldDataByVersion(Win_Administrator_VM.Qsid_ID, false, sessIds);
                    if (result != null)
                    {
                        result.Columns.Remove("Status");
                    }
                    Win_Administrator_VM.AddedDeletedRows = result;
                }
            }
            if (Win_Administrator_VM.AddedDeletedRows != null  && Win_Administrator_VM.AddedDelBySingle.Rows.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                VisibleLoderDelBySingle = Visibility.Visible;
                NotifyPropertyChanged("VisibleLoderDelBySingle");
                using (var context = new CRAModel())
                {
                    var latestVersion = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID).Select(y => y.QSID_Version).OrderByDescending(z => z).FirstOrDefault();
                    List<int?> sessIds = context.Log_Sessions.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && x.QSID_Version == latestVersion &&
                    (x.SessionTypeID == 1 || x.SessionTypeID == 5)).Select(z => (int?)z.SessionID).ToList();

                    // except full row
                    var fullRowIds = Win_Administrator_VM.AddedDeletedRows.AsEnumerable().Select(y => y.Field<int>("RowId")).ToList();
                    if (fullRowIds.Count == 0)
                    {
                        fullRowIds.Add(0);
                    }
                    var prefA = context.Log_QSxxx_PREF.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A").ToList();
                    var phraseA = context.Log_QSxxx_Phrases.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A").ToList();
                    var cMA = context.Log_QSxxx_ConditionalMapping.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A").ToList();
                    var identA = context.Log_QSxxx_Identifiers.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A").ToList();
                    var notesA = context.Log_QSxxx_Notes.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A").ToList();
                    var valOtherA = context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A" && x.ValueOtherFieldID != null).ToList();
                    var valA = context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "A" && x.ValueOtherFieldID == null).ToList();

                    var prefD = context.Log_QSxxx_PREF.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D").ToList();
                    var phraseD = context.Log_QSxxx_Phrases.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D").ToList();
                    var cMD = context.Log_QSxxx_ConditionalMapping.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D").ToList();
                    var identD = context.Log_QSxxx_Identifiers.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D").ToList();
                    var notesD = context.Log_QSxxx_Notes.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D").ToList();
                    var valOtherD = context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D" && x.ValueOtherFieldID != null).ToList();
                    var valD = context.Log_QSxxx_Values.AsNoTracking().Where(x => x.QSID_ID == Win_Administrator_VM.Qsid_ID && !fullRowIds.Contains(x.RowID) && sessIds.Contains(x.SessionID) && x.Status == "D" && x.ValueOtherFieldID == null).ToList();

                    var final = GetDeletedRows(prefA, prefD, "Pref");
                    final.AddRange(GetDeletedRows(phraseA, phraseD, "Phrases"));
                    final.AddRange(GetDeletedRows(cMA, cMD, "ConditonalMapping"));
                    final.AddRange(GetDeletedRows(identA, identD, "Identifier"));
                    final.AddRange(GetDeletedRows(notesA, notesD, "Notes"));
                    final.AddRange(GetDeletedRows(valOtherA, valOtherD, "ValueOther"));
                    final.AddRange(GetDeletedRows(valA, valD, "Values"));

                    Win_Administrator_VM.AddedDelBySingle = objCommon.ConvertToDataTable(final);

                }
            }
            if (Win_Administrator_VM.AddedDeletedRows != null && Win_Administrator_VM.AddedDelBySingle.Rows.Count == 0 && Win_Administrator_VM.Qsid_ID != 0)
            {
                lstRowsDelBySingle = new StandardDataGrid_VM("Row Added Deleted By SingleValue", Win_Administrator_VM.AddedDelBySingle, Visibility.Visible, false, true, typeof(IList));
            }
            NotifyPropertyChanged("lstRowsDelBySingle");
            VisibleLoderDelBySingle = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderDelBySingle");
            
        }
        private void NotifyMe(PropertyChangedMessage<object> obj)
        {
            if (obj.PropertyName == "Row Added Deleted By Version")
            {
                SelectedRowItem = obj.NewValue;
                DelRowType = "Row Added Deleted By Version";
            }
            if (obj.PropertyName == "Row Added Deleted By SingleValue")
            {
                SelectedRowItem = obj.NewValue;
                DelRowType = "Row Added Deleted By SingleValue";
            }
        }
        private void CallbackSaveBtnClick(NotificationMessageAction<object> obj)
        {
            if (DelRowType == "Row Added Deleted By Version")
            {
                Int32 rowId = Convert.ToInt32(((dynamic)SelectedRowItem).RowId);
                DeleteByRow(rowId);
                Win_Administrator_VM.AddedDeletedRows = new DataTable();
                Task.Run(() => RunDeletedRows());
            }
            if (DelRowType == "Row Added Deleted By SingleValue")
            {
                Int32 rowId = Convert.ToInt32(((dynamic)SelectedRowItem).RowId);
                string chType = Convert.ToString(((dynamic)SelectedRowItem).Type);
                Int32 qId = Convert.ToInt32(((dynamic)SelectedRowItem).QId);
                Int32 fId = Convert.ToInt32(((dynamic)SelectedRowItem).FieldNameId);
                string ssesId = Convert.ToString(((dynamic)SelectedRowItem).SessionId);
                DeleteByField(rowId, chType, qId, fId, ssesId);
                Win_Administrator_VM.AddedDelBySingle = new DataTable();
                Task.Run(() => RunDeletedSingleValue());
            }
            string msg = "Deleted Successfully";
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                _notifier.ShowSuccess(msg);
            });
        }

        private void DeleteByField(Int32 rowId, string chType, Int32 qId, Int32 fId, string ssesId)
        {
            switch (chType)
            {
                case "Pref":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_qsxxx_pref where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and FieldNameId ="
                    + fId + " and QSPrefID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
                case "Phrases":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_qsxxx_phrases where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and FieldNameId ="
                    + fId + " and QSPhraseID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
                case "Notes":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Notes where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and FieldNameId ="
                    + fId + " and QSNotesID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
                case "ConditonalMapping":
                    objCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_ConditionalMapping where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and FieldNameId ="
                    + fId + " and QSConditionalMappingID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
                case "Identifer":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Identifiers where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and FieldNameId ="
                    + fId + " and QSIdentifiersID =" + qId, Win_Administrator_VM.CommonListConn);
                    break;
                case "ValueOther":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Values where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and valueOtherFieldId ="
                    + fId + " and QSValuesID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
                case "Values":
                    objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Values where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and (MaxFieldNameID ="
                    + fId + " or MinFieldNameID = " + fId + ") and QSValuesID =" + qId + " and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
                    break;
            }
            objCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_RowHistory where qsid_id = " + Win_Administrator_VM.Qsid_ID + " and rowid =" + rowId + " and sessionId ="
                    + fId + " and Status ='C' and SessionID in (" + ssesId + ")", Win_Administrator_VM.CommonListConn);
            List<Log_QSxxx_RowHistory_manualDeletes> lstLog = new List<Log_QSxxx_RowHistory_manualDeletes>();
            var log = new Log_QSxxx_RowHistory_manualDeletes
            {
                RowID = rowId,
                QSID_ID = Win_Administrator_VM.Qsid_ID,
                SessionID = Engine.CurrentUserSessionID,
                DeleteType = chType,
                LogQSxxxRowHistoryManualDeleteID = 0,
                TimeStamp = objCommon.ESTTime(),
            };
            lstLog.Add(log);
            _objLibraryFunction_Service.BulkIns(lstLog);
        }

        private void DeleteByRow(int rowId)
        {
            objCommon.DbaseQueryReturnStringSQL("Delete from log_qsxxx_cas where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from log_qsxxx_pref where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from log_qsxxx_phrases where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_ConditionalMapping where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Identifiers where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Notes where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from log_QSxxx_Values where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            objCommon.DbaseQueryReturnStringSQL("Delete from Log_QSxxx_RowHistory where rowid = " + rowId + " and qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            List<Log_QSxxx_RowHistory_manualDeletes> lstLog = new List<Log_QSxxx_RowHistory_manualDeletes>();
            var log = new Log_QSxxx_RowHistory_manualDeletes
            {
                RowID = rowId,
                QSID_ID = Win_Administrator_VM.Qsid_ID,
                SessionID = Engine.CurrentUserSessionID,
                DeleteType = "Full Row",
                LogQSxxxRowHistoryManualDeleteID = 0,
                TimeStamp = objCommon.ESTTime(),
            };
            lstLog.Add(log);
            _objLibraryFunction_Service.BulkIns(lstLog);
        }

        public List<ISingleValueFinal> GetDeletedRows<T>(List<T> lstA, List<T> lstD, string typ)
        {
            string qsId = string.Empty;
            string fName = string.Empty;
            string val = string.Empty;
            lsSingle = new List<ISingleValue>();
            List<ISingleValueFinal> result = new List<ISingleValueFinal>();
            switch (typ)
            {
                case "Pref":
                    qsId = "QSPrefID";
                    fName = "FieldNameID";
                    val = "ChemicalNameID";
                    break;
                case "Phrases":
                    qsId = "QSPhraseID";
                    fName = "FieldNameID";
                    val = "PhraseID";
                    break;
                case "Notes":
                    qsId = "QSNotesID";
                    fName = "FieldNameID";
                    val = "NoteCodeID";
                    break;
                case "ConditonalMapping":
                    qsId = "QSConditionalMappingID";
                    fName = "FieldNameID";
                    val = "SubfieldID";
                    break;
                case "Identifer":
                    qsId = "QSIdentifiersID";
                    fName = "FieldNameID";
                    val = "IdentifierValueID";
                    break;
                case "ValueOther":
                    qsId = "QSValuesID";
                    fName = "ValueOtherFieldID";
                    val = "ValueOther";
                    break;
                case "Values":
                    qsId = "QSValuesID";
                    fName = "MaxFieldNameID";
                    val = "MaxValue";
                    break;
            }

            for (int i = 0; i < lstA.Count(); i++)
            {
                var qId = Convert.ToInt32(lstA[i].GetType().GetProperty(qsId).GetValue(lstA[i], null));
                var rId = Convert.ToInt32(lstA[i].GetType().GetProperty("RowID").GetValue(lstA[i], null));
                var fId = Convert.ToInt32(lstA[i].GetType().GetProperty(fName).GetValue(lstA[i], null));
                var valu = Convert.ToString(lstA[i].GetType().GetProperty(val).GetValue(lstA[i], null));
                if (valu == "" & typ == "Values")
                {
                    fId = Convert.ToInt32(lstA[i].GetType().GetProperty("MinFieldNameID").GetValue(lstA[i], null));
                    valu = Convert.ToString(lstA[i].GetType().GetProperty("MinValue").GetValue(lstA[i], null));
                }
                var type = typ;
                var chkExt = lstD.Where(x => Convert.ToInt32(x.GetType().GetProperty(qsId).GetValue(x, null)) == qId).ToList();
                if (chkExt.Any())
                {
                    var sId = Convert.ToString(lstA[i].GetType().GetProperty("SessionID").GetValue(lstA[i], null)) + "," + Convert.ToString(chkExt[0].GetType().GetProperty("SessionID").GetValue(lstA[i], null));
                    lsSingle.Add(new ISingleValue { QId = qId, FieldNameId = fId, RowId = rId, Status = "A,D", Type = typ, Value = valu, SessionId = sId });
                    //lsSingle.Add(new ISingleValue { QId = qId, FieldNameId = fId, RowId = rId, Status = "D", Type = typ, Value = valu });
                }
            }
            if (lsSingle.Count() > 0)
            {
                List<int> allFld = lsSingle.Where(y=> y.FieldNameId != 0).Select(x => x.FieldNameId).Distinct().ToList();
                using (var context = new CRAModel())
                {
                    switch (typ)
                    {
                        case "Pref":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x=> allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      join d3 in context.Library_ChemicalNames.AsNoTracking()
                                      on Convert.ToInt32(d1.Value) equals d3.ChemicalNameID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d3.ChemicalName,
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();
                            break;
                        case "Phrases":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      join d3 in context.Library_Phrases.AsNoTracking()
                                      on Convert.ToInt32(d1.Value) equals d3.PhraseID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d3.Phrase,
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();
                            break;
                        case "Notes":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      join d3 in context.Library_NoteCodes.AsNoTracking()
                                      on Convert.ToInt32(d1.Value) equals d3.NoteCodeID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d3.NoteCode,
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();

                            break;
                        case "ConditonalMapping":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      join d3 in context.Library_SubfieldCharacters.AsNoTracking()
                                      on Convert.ToInt32(d1.Value) equals d3.SubfieldID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d3.Character,
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();

                            break;
                        case "Identifer":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      join d3 in context.Library_Identifiers.AsNoTracking()
                                      on Convert.ToInt32(d1.Value) equals d3.IdentifierValueID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d3.IdentifierValue,
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();

                            break;
                        case "ValueOther":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d1.Value.ToString(),
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();

                            break;
                        case "Values":
                            result = (from d1 in lsSingle
                                      join d2 in context.Library_FieldNames.AsNoTracking().Where(x => allFld.Contains(x.FieldNameID))
                                      on d1.FieldNameId equals d2.FieldNameID
                                      select new ISingleValueFinal
                                      {
                                          QId = d1.QId,
                                          RowId = d1.RowId,
                                          FieldNameId = d2.FieldNameID,
                                          FieldName = d2.FieldName,
                                          Status = d1.Status,
                                          Value = d1.Value.ToString(),
                                          Type = d1.Type,
                                          SessionId = d1.SessionId,
                                      }).ToList();
                            break;
                    }
                }
            }
            return result;
        }
    }
    public class ISingleValue
    {
        public int QId { get; set; }
        public int RowId { get; set; }
        public int FieldNameId { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }

        public string SessionId { get; set; }
    }

    public class ISingleValueFinal
    {
        public int QId { get; set; }
        public int RowId { get; set; }
        public int FieldNameId { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string SessionId { get; set; }
    }
}
