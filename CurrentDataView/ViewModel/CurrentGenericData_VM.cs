﻿using CurrentDataView.Library;
using GalaSoft.MvvmLight.Messaging;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CRA_DataAccess;

namespace CurrentDataView.ViewModel
{
    public class CurrentGenericData_VM : Base_ViewModel
    {
        private RowIdentifier rowIdentifier = new RowIdentifier();
        public StandardDataGrid_VM listCurrentGeneric { get; set; }
        public string Qsid { get; set; }
        public Visibility loderCurrentGenericsVisibility { get; set; }
        public int CountGenericHit { get; set; }
        public int CountHydrate { get; set; }
        public int CountAlternateCas { get; set; }
        public int CountDirectHit { get; set; }
        public int CountDirectListed { get; set; }
        public ICommand CommandFilterDirectHit { get; set; }
        public ICommand CommandFilterDirectListed { get; set; }
        public ICommand CommandFilterGenericHit { get; set; }
        public ICommand CommandFilterHydrate { get; set; }
        public ICommand CommandFilterAlternateCas { get; set; }
        public ICommand CommandClearFilter { get; set; }
        public Visibility ClearBtnVisisbility = Visibility.Collapsed;
        //public ObservableCollection<iColumnName> listMdbColumnsGD { get; set; }
        //public ICommand ShowUniqueValuesGD { get; set; }
        //private iColumnName _SelectedColumnGD { get; set; }

        //public iColumnName SelectedColumnGD
        //{
        //    get => _SelectedColumnGD;
        //    set
        //    {
        //        if (Equals(_SelectedColumnGD, value))
        //        {
        //            return;
        //        }

        //        _SelectedColumnGD = value;
        //    }
        //}
        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }

        public CurrentGenericData_VM(string _qsid)
        {
            //ShowUniqueValuesGD = new RelayCommand(ShowUniqueValuesGDExecute, CommandAccessCanExecute);
            Qsid = _qsid;
            BindCurrentGenerics();
            CommandFilterGenericHit = new RelayCommand(CommandFilterGenericHitExcute, CommandFilterGenericHitCanExecute);
            CommandFilterDirectHit = new RelayCommand(CommandFilterDirectHitExcute, CommandFilterDirectHitCanExecute);
            CommandFilterDirectListed = new RelayCommand(CommandFilterDirectListedExcute, CommandFilterDirectListedCanExecute);
            CommandFilterHydrate = new RelayCommand(CommandFilterHydrateExecute, CommandFilterHydrateCanExecute);
            CommandFilterAlternateCas = new RelayCommand(CommandFilterAlternateCasExecute, CommandFilterAlternateCasCanExecute);
            CommandClearFilter = new RelayCommand(CommandClearFilterExecute);
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(CurrentGenericData_VM), NotifyRefreshGrid);
        }

        private void NotifyRefreshGrid(PropertyChangedMessage<string> obj)
        {
            if (obj.PropertyName == "CurrentDataView.ViewModel.CurrentGenericData_VM")
            {
                BindCurrentGenerics();
            }
        }

        private bool CommandFilterAlternateCasCanExecute(object obj)
        {
            if (CountAlternateCas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CommandFilterHydrateCanExecute(object obj)
        {
            if (CountHydrate > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CommandFilterHydrateExecute(object obj)
        {
            DataTable filterDB = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Hydrates'").AsQueryable().CopyToDataTable();
            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", filterDB, Visibility.Visible, false, false, typeof(CurrentGenericData_VM));
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Visible;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }

        private void CommandFilterAlternateCasExecute(object obj)
        {
            DataTable filterDB = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Alternate CAS'").AsQueryable().CopyToDataTable();
            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", filterDB, Visibility.Visible);
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Visible;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }

        private void CommandClearFilterExecute(object obj)
        {

            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", Win_Administrator_VM.dtCurrentGeneric, Visibility.Visible);
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Collapsed;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }

        private void CommandFilterDirectHitExcute(object obj)
        {
            DataTable filterDB = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Direct'").AsQueryable().CopyToDataTable();
            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", filterDB, Visibility.Visible);
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Visible;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }
        private void CommandFilterDirectListedExcute(object obj)
        {
            DataTable filterDB = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Direct-Listed Children'").AsQueryable().CopyToDataTable();
            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", filterDB, Visibility.Visible);
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Visible;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }
        private void CommandFilterGenericHitExcute(object obj)
        {
            DataTable filterDB = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Generics'").AsQueryable().CopyToDataTable();
            listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", filterDB, Visibility.Visible);
            NotifyPropertyChanged("listCurrentGeneric");
            ClearBtnVisisbility = Visibility.Visible;
            NotifyPropertyChanged("ClearBtnVisisbility");
        }
        private bool CommandFilterDirectHitCanExecute(object obj)
        {
            if (CountDirectHit > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool CommandFilterDirectListedCanExecute(object obj)
        {
            if (CountDirectListed > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CommandFilterGenericHitCanExecute(object obj)
        {
            if (CountGenericHit > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public void BindCurrentGenerics()
        {
            loderCurrentGenericsVisibility = Visibility.Visible;
            NotifyPropertyChanged("loderCurrentGenericsVisibility");

            if (Win_Administrator_VM.CurrentDataTableWithChild.Columns.Count == 0)
            {
                CurrentData_VM.SingleCurrentViewFunction();
            }
            Task.Run(() =>
            {
                Win_Administrator_VM.dtCurrentGeneric = _objLibraryFunction_Service.GetDataTableCombineWithGenerics(Win_Administrator_VM.CurrentDataTableWithChild, Win_Administrator_VM.Qsid_ID);
                if (Win_Administrator_VM.dtCurrentGeneric.Columns.Count > 6)
                {
                    Win_Administrator_VM.dtCurrentGeneric.Columns["Type"].SetOrdinal(4);
                    Win_Administrator_VM.dtCurrentGeneric.Columns["ParentPrimaryCas"].SetOrdinal(5);
                }
                
                CountGenericHit = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Generics'").Count();
                CountDirectHit = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Direct'").Count();
                CountDirectListed = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Direct-Listed Children'").Count();
                CountHydrate = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Hydrates'").Count();
                CountAlternateCas = Win_Administrator_VM.dtCurrentGeneric.Select("Type='Alternate CAS'").Count();

                if (Win_Administrator_VM.dtCurrentGeneric.Rows.Count > 0)
                {
                    listCurrentGeneric = new StandardDataGrid_VM("Current Generic View", Win_Administrator_VM.dtCurrentGeneric, Visibility.Visible, false, false, typeof(CurrentGenericData_VM));
                    NotifyPropertyChanged("listCurrentGeneric");
                }
                NotifyPropertyChanged("CountDirectListed");
                NotifyPropertyChanged("CountGenericHit");
                NotifyPropertyChanged("CountDirectHit");
                NotifyPropertyChanged("CountHydrate");
                NotifyPropertyChanged("CountAlternateCas");
                loderCurrentGenericsVisibility = Visibility.Collapsed;
                NotifyPropertyChanged("loderCurrentGenericsVisibility");
            });
        }

    }
}
