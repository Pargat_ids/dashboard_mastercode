﻿using System.Windows.Input;
using CRA_DataAccess;
using CurrentDataView.Library;
using System.Data;
using System.Collections.Generic;
using EntityClass;
using System.Linq;
using VersionControlSystem.Model.ViewModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using EntityFramework.Utilities;
using System.Windows.Forms;
using CommonGenericUIView.ViewModel;

namespace CurrentDataView.ViewModel
{
    public class MapCASToEC_VM : Base_ViewModel
    {

        public ObservableCollection<string> ListECType { get; set; } = new ObservableCollection<string>();
        private List<string> AddECType()
        {
            List<string> listSelectedCombo = new List<string>() { "EC Number", "Molecular Formula", "SubstanceName Synonyms" };
            return listSelectedCombo;
        }
        public string _selectedECItemCombo { get; set; }
        public string selectedECItemCombo
        {
            get { return _selectedECItemCombo; }
            set
            {
                if (_selectedECItemCombo != value)
                {
                    _selectedECItemCombo = value;
                    ShowSelectedType();
                }
            }
        }

        public ICommand TabPendingApproveAll { get; set; }
        public ICommand TabPendingDisapproveAll { get; set; }

        private CommonFunctions objCommon;
        public CommonDataGrid_ViewModel<IMapCasToECDisApprove> comonCASECDisapprove { get; set; }
        public CommonDataGrid_ViewModel<IMapCasToECApprove> comonCASECApprove { get; set; }
        public CommonDataGrid_ViewModel<IMapCasToECPending> comonCASECPending { get; set; }
        public Visibility VisibilityEcDisapprove_Loader { get; set; } = Visibility.Collapsed;
        public Visibility VisibilityEcApprove_Loader { get; set; } = Visibility.Collapsed;
        public Visibility VisibilityEcPending_Loader { get; set; } = Visibility.Collapsed;

        public CommentsUC_VM objCommentDC_Identifier { get; set; } = new CommentsUC_VM(new GenericComment_VM());
        public MapCASToEC_VM()
        {
            objCommon = new CommonFunctions();
            ListECType = new ObservableCollection<string>(AddECType());
            NotifyPropertyChanged("ListECType");
            TabPendingApproveAll = new RelayCommand(TabPendingApproveAllExecute, CommandCanExecute);
            TabPendingDisapproveAll = new RelayCommand(TabPendingDisapproveAllExecute, CommandCanExecute);
            comonCASECDisapprove = new CommonDataGrid_ViewModel<IMapCasToECDisApprove>(GetListGridColumnListPr(), "Disapprove", "Disapprove");
            comonCASECApprove = new CommonDataGrid_ViewModel<IMapCasToECApprove>(GetListGridColumnECApprove(), "Approved", "Approved");
            comonCASECPending = new CommonDataGrid_ViewModel<IMapCasToECPending>(GetListGridColumnECPending(), "Review Needed", "ReviewNeeded");
            MessengerInstance.Register<PropertyChangedMessage<IMapCasToECPending>>(this, NotifyMeECPending);
            MessengerInstance.Register<PropertyChangedMessage<IMapCasToECApprove>>(this, NotifyMeECApprove);
            MessengerInstance.Register<PropertyChangedMessage<IMapCasToECDisApprove>>(this, NotifyMeECDisapprove);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(IMapCasToECPending), CommandButtonExecutedECPending);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(IMapCasToECApprove), CommandButtonExecutedECApprove);
            MessengerInstance.Register<NotificationMessageAction<object>>(this, typeof(IMapCasToECDisApprove), CommandButtonExecutedECDisapprove);
        }
        private void TabPendingApproveAllExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to Approve All records", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                switch (selectedECItemCombo)
                {
                    case "EC Number":
                        var lstLog = new List<Log_Map_CAS_EC>();
                        var lst = new List<Map_CAS_ECNumber>();
                        var maxID = objCommon.GetMaxAutoField("Map_CAS_ECNumber");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDis = new Map_CAS_ECNumber()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID
                            };
                            lst.Add(lgMainDis);
                            var lgLog = new Log_Map_CAS_EC
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                CASECNumberID = maxID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lgLog);
                            maxID += 1;
                        }
                        _objLibraryFunction_Service.BulkIns<Map_CAS_ECNumber>(lst);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);
                        BindECNumberPending();
                        BindECNumberDisapproved();
                        BindECNumberApproved();
                        break;
                    case "Molecular Formula":
                        var lstLogM = new List<Log_Map_CAS_MolecularFormula>();
                        var lstM = new List<Map_CAS_MolecularFormula>();
                        var maxIDM = objCommon.GetMaxAutoField("Map_CAS_MolecularFormula");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDisM = new Map_CAS_MolecularFormula()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID
                            };
                            lstM.Add(lgMainDisM);

                            var lgLogM = new Log_Map_CAS_MolecularFormula
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                CASMolecularFormulaID = maxIDM,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLogM.Add(lgLogM);
                        }
                        _objLibraryFunction_Service.BulkIns<Map_CAS_MolecularFormula>(lstM);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLogM);
                        BindMolecularPending();
                        BindMolecularDisapproved();
                        BindMolecularApproved();
                        break;
                    case "SubstanceName Synonyms":
                        var lstLogS = new List<Log_Map_SubstanceNameSynonyms>();
                        var lstS = new List<Map_SubstanceNameSynonyms>();
                        var maxIDS = objCommon.GetMaxAutoField("Map_SubstanceNameSynonyms");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDis = new Map_SubstanceNameSynonyms()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                ChemicalNameID = lstCASToECPending[i].ID
                            };
                            lstS.Add(lgMainDis);
                            var lgLog = new Log_Map_SubstanceNameSynonyms
                            {
                                CASID = lstCASToECPending[i].CASID,
                                ChemicalNameID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                SynonymID = maxIDS,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLogS.Add(lgLog);
                        }
                        _objLibraryFunction_Service.BulkIns<Map_SubstanceNameSynonyms>(lstS);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLogS);
                        BindSubstanceNamePending();
                        BindSubstanceNameDisApproved();
                        BindSubstanceNameApproved();
                        break;
                }
                _notifier.ShowSuccess("Selected Value approved successfully");
            }
        }
        private void TabPendingDisapproveAllExecute(object obj)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to Disapprove All records", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                switch (selectedECItemCombo)
                {
                    case "EC Number":
                        var lstLog = new List<Log_Map_CAS_EC_Disapproved>();
                        var lst = new List<Map_CAS_ECNumber_Disapproved>();
                        var maxID = objCommon.GetMaxAutoField("Map_CAS_ECNumber_Disapproved");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDis = new Map_CAS_ECNumber_Disapproved()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID
                            };
                            lst.Add(lgMainDis);

                            var lgLog = new Log_Map_CAS_EC_Disapproved
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                CASECNumberID_Disapproved = maxID,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLog.Add(lgLog);
                            maxID += 1;
                        }
                        _objLibraryFunction_Service.BulkIns<Map_CAS_ECNumber_Disapproved>(lst);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLog);
                        BindECNumberPending();
                        BindECNumberDisapproved();
                        BindECNumberApproved();
                        break;
                    case "Molecular Formula":
                        var lstLogM = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
                        var lstM = new List<Map_CAS_MolecularFormula_Disapproved>();
                        var maxIDM = objCommon.GetMaxAutoField("Map_CAS_MolecularFormula_Disapproved");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDisM = new Map_CAS_MolecularFormula_Disapproved()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID
                            };
                            lstM.Add(lgMainDisM);

                            var lgLogM = new Log_Map_CAS_MolecularFormula_Disapproved
                            {
                                CASID = lstCASToECPending[i].CASID,
                                IdentifierValueID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                CASMolecularFormulaID_Disapprove = maxIDM,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLogM.Add(lgLogM);
                            maxIDM += 1;
                        }
                        _objLibraryFunction_Service.BulkIns<Map_CAS_MolecularFormula_Disapproved>(lstM);
                        _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLogM);
                        BindMolecularPending();
                        BindMolecularDisapproved();
                        BindMolecularApproved();
                        break;
                    case "SubstanceName Synonyms":
                        var lstLogS = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
                        var lstS = new List<Map_SubstanceNameSynonyms_Disapproved>();
                        var maxIDS = objCommon.GetMaxAutoField("Map_SubstanceNameSynonyms_Disapproved");
                        for (int i = 0; i < lstCASToECPending.Count(); i++)
                        {
                            var lgMainDis = new Map_SubstanceNameSynonyms_Disapproved()
                            {
                                CASID = lstCASToECPending[i].CASID,
                                ChemicalNameID = lstCASToECPending[i].ID
                            };
                            lstS.Add(lgMainDis);
                            var lgLog = new Log_Map_SubstanceNameSynonyms_Disapproved
                            {
                                CASID = lstCASToECPending[i].CASID,
                                ChemicalNameID = lstCASToECPending[i].ID,
                                Status = "A",
                                SessionID = Engine.CurrentUserSessionID,
                                SynonymID_Disapprove = maxIDS,
                                TimeStamp = objCommon.ESTTime(),
                            };
                            lstLogS.Add(lgLog);
                        }
                        _objLibraryFunction_Service.BulkIns<Map_SubstanceNameSynonyms_Disapproved>(lstS);
                        _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLogS);
                        BindSubstanceNamePending();
                        BindSubstanceNameDisApproved();
                        BindSubstanceNameApproved();
                        break;
                }
                _notifier.ShowSuccess("Selected Values Disapproved successfully");
            }
        }
        private bool CommandCanExecute(object obj)
        {
            return true;
        }
        private void ShowSelectedType()
        {
            switch (selectedECItemCombo)
            {
                case "EC Number":
                    Task.Run(() =>
                    {
                        BindECNumberDisapproved();
                        BindECNumberApproved();
                        BindECNumberPending();
                    });
                    break;
                case "Molecular Formula":
                    Task.Run(() =>
                    {
                        BindMolecularDisapproved();
                        BindMolecularApproved();
                        BindMolecularPending();
                    });
                    break;
                case "SubstanceName Synonyms":
                    Task.Run(() =>
                    {
                        BindSubstanceNameApproved();
                        BindSubstanceNameDisApproved();
                        BindSubstanceNamePending();
                    });
                    break;
            }
        }
        private void BindECNumberDisapproved()
        {
            VisibilityEcDisapprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
            Task.Run(() =>
            {
                //var SAPTPhrasesTable1 = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>(
                //" select distinct x.CASID,a.CAS,x.IdentifierValueID as ID, b.IdentifierValue  as Value" +
                //" from (select distinct a.CASID, c.IdentifierValueID from qsxxx_cas a " +
                //" inner join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
                //" and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
                //" inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
                //" inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
                //" and e.IdentifierTypeName = 'EC Number') x " +
                //" inner join Map_CAS_ECNumber_Disapproved y " +
                //" on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
                //"  inner join library_cas a on x.casid = a.casid " +
                //"  inner join library_identifiers b on x.IdentifierValueID = b.IdentifierValueID ",
                //Win_Administrator_VM.CommonListConn);
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>(
                    " select distinct a.CASID, d.CAS, b.IdentifierValueID as ID, c.IdentifierValue as Value  from qsxxx_cas a," +
                    " Map_CAS_ECNumber_Disapproved b, Library_Identifiers c, Library_CAS d" +
                    " where a.CASID = b.CASID and  b.IdentifierValueID = c.IdentifierValueID" +
                    " and a.CASID = d.CASID and  a.qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);

                lstCASToECDisapprove = new ObservableCollection<IMapCasToECDisApprove>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECDisapprove");
                VisibilityEcDisapprove_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
            });

        }
        private void BindECNumberApproved()
        {
            VisibilityEcApprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcApprove_Loader");
            Task.Run(() =>
            {
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECApprove>(
               " select distinct x.CASID,a.CAS,x.IdentifierValueID as ID, b.IdentifierValue as Value " +
               " from (select distinct a.CASID, c.IdentifierValueID from qsxxx_cas a " +
               " inner join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
               " and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
               " inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
               " inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
               " and e.IdentifierTypeName = 'EC Number') x " +
               " inner join Map_CAS_ECNumber y " +
               " on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
               " inner join library_cas a on x.casid = a.casid " +
               " inner join library_identifiers b on x.IdentifierValueID = b.IdentifierValueID ",
               Win_Administrator_VM.CommonListConn);
                lstCASToECApprove = new ObservableCollection<IMapCasToECApprove>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECApprove");
                VisibilityEcApprove_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcApprove_Loader");
            });

        }
        private void BindECNumberPending()
        {
            VisibilityEcPending_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcPending_Loader");
            Task.Run(() =>
            {
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECPending>(
                " select distinct fnl.CASID, lc.CAS, fnl.IdentifierValueID as ID, li.IdentifierValue as Value from  " +
                " (select z.CASID, z.IdentifierValueID from " +
                " (select x.CASID, x.IdentifierValueID from " +
                " (select a.CASID, b.IdentifierValueID from qsxxx_cas a " +
                " inner " +
                " join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
                " and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
                " inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
                " inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
                " and e.IdentifierTypeName = 'EC Number') x " +
                " left join Map_CAS_ECNumber y " +
                " on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
                " where y.IdentifierValueID is null) z " +
                " left join Map_CAS_ECNumber_Disapproved zz " +
                " on z.CASID = zz.CASID and z.IdentifierValueID = zz.IdentifierValueID " +
                " where zz.IdentifierValueID is null) fnl " +
                " inner join library_cas lc on fnl.CASID = lc.CASID " +
                " inner join Library_Identifiers li on fnl.IdentifierValueID = li.IdentifierValueID ",
                Win_Administrator_VM.CommonListConn);
                lstCASToECPending = new ObservableCollection<IMapCasToECPending>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECPending");
                VisibilityEcPending_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcPending_Loader");
            });

        }

        private void BindMolecularDisapproved()
        {
            VisibilityEcDisapprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
            Task.Run(() =>
            {

                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>(
                    " select distinct a.CASID, d.CAS, b.IdentifierValueID as ID, c.IdentifierValue as Value  from qsxxx_cas a," +
                    " Map_CAS_MolecularFormula_Disapproved b, Library_Identifiers c, Library_CAS d" +
                    " where a.CASID = b.CASID and  b.IdentifierValueID = c.IdentifierValueID" +
                    " and a.CASID = d.CASID and  a.qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);


                //var SAPTPhrasesTable1 = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>(
                //" select distinct x.CASID,a.CAS,x.IdentifierValueID as ID, b.IdentifierValue  as Value" +
                //" from (select distinct a.CASID, c.IdentifierValueID from qsxxx_cas a " +
                //" inner join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
                //" and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
                //" inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
                //" inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
                //" and e.IdentifierTypeName = 'Molecular Formula') x " +
                //" inner join Map_CAS_MolecularFormula_Disapproved y " +
                //" on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
                //"  inner join library_cas a on x.casid = a.casid " +
                //"  inner join library_identifiers b on x.IdentifierValueID = b.IdentifierValueID ",
                //Win_Administrator_VM.CommonListConn);
                lstCASToECDisapprove = new ObservableCollection<IMapCasToECDisApprove>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECDisapprove");
                VisibilityEcDisapprove_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
            });

        }
        private void BindMolecularApproved()
        {
            VisibilityEcApprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcApprove_Loader");
            Task.Run(() =>
            {
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECApprove>(
               " select distinct x.CASID,a.CAS,x.IdentifierValueID as ID, b.IdentifierValue  as Value" +
               " from (select distinct a.CASID, c.IdentifierValueID from qsxxx_cas a " +
               " inner join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
               " and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
               " inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
               " inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
               " and e.IdentifierTypeName = 'Molecular Formula') x " +
               " inner join Map_CAS_MolecularFormula y " +
               " on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
               " inner join library_cas a on x.casid = a.casid " +
               " inner join library_identifiers b on x.IdentifierValueID = b.IdentifierValueID ",
               Win_Administrator_VM.CommonListConn);
                lstCASToECApprove = new ObservableCollection<IMapCasToECApprove>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECApprove");
                VisibilityEcApprove_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcApprove_Loader");
            });

        }
        private void BindMolecularPending()
        {
            VisibilityEcPending_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcPending_Loader");
            Task.Run(() =>
            {
                var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECPending>(
                " select distinct fnl.CASID, lc.CAS, fnl.IdentifierValueID as ID, li.IdentifierValue as Value from  " +
                " (select z.CASID, z.IdentifierValueID from " +
                " (select x.CASID, x.IdentifierValueID from " +
                " (select a.CASID, b.IdentifierValueID from qsxxx_cas a " +
                " inner " +
                " join QSxxx_Identifiers b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID " +
                " and a.qsid_id = " + Win_Administrator_VM.Qsid_ID +
                " inner join  Library_Identifiers c on b.IdentifierValueID = c.IdentifierValueID " +
                " inner join  Library_IdentifierCategories e  on c.IdentifierTypeID = e.IdentifierTypeID " +
                " and e.IdentifierTypeName = 'Molecular Formula') x " +
                " left join Map_CAS_MolecularFormula y " +
                " on x.CASID = y.CASID and x.IdentifierValueID = y.IdentifierValueID " +
                " where y.IdentifierValueID is null) z " +
                " left join Map_CAS_MolecularFormula_Disapproved zz " +
                " on z.CASID = zz.CASID and z.IdentifierValueID = zz.IdentifierValueID " +
                " where zz.IdentifierValueID is null) fnl " +
                " inner join library_cas lc on fnl.CASID = lc.CASID " +
                " inner join Library_Identifiers li on fnl.IdentifierValueID = li.IdentifierValueID ",
                Win_Administrator_VM.CommonListConn);
                lstCASToECPending = new ObservableCollection<IMapCasToECPending>(SAPTPhrasesTable);
                NotifyPropertyChanged("comonCASECPending");
                VisibilityEcPending_Loader = Visibility.Collapsed;
                NotifyPropertyChanged("VisibilityEcPending_Loader");
            });

        }

        private void BindSubstanceNamePending()
        {
            VisibilityEcPending_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcPending_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var PrefSourceFieldId = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == "PREF_SOURCE").Select(z => z.FieldNameID).FirstOrDefault();
                    var PendingSyn = objCommon.DbaseQueryReturnSqlList<IMapCasToECPending>("Select x.CASID, y.CAS, x.ChemicalNameID as ID, " +
                        " z.ChemicalName as Value, xx.LanguageName from (select distinct a.CASID, b.ChemicalNameID from " +
                        " qsxxx_cas a inner join qsxxx_pref b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID and " +
                        " b.FieldNameID != " + PrefSourceFieldId + " and a.casid in (select CASID from qsxxx_cas where " +
                        " QSID_ID = " + Win_Administrator_VM.Qsid_ID + ")" +
                        " except select distinct a.CASID, a.ChemicalNameID from Map_SubstanceNameSynonyms a " +
                        " inner join QSxxx_CAS b on a.CASID = b.CASID and b.QSID_ID =" + Win_Administrator_VM.Qsid_ID + ") as x " +
                        " inner join Library_cas y on x.CASId = y.CASID inner join Library_ChemicalNames z on x.ChemicalNameID = z.ChemicalNameID  inner join Library_Languages xx on z.languageId = xx.LanguageId", Win_Administrator_VM.CommonListConn);

                    var pendingSynNotinMap = (from d1 in PendingSyn
                                              join d2 in context.Map_SubstanceNameSynonyms_Disapproved.AsNoTracking()
                                              on new { a = d1.CASID, b = d1.ID }
                                              equals new { a = d2.CASID, b = d2.ChemicalNameID }
                                              into tg
                                              from tcheck in tg.DefaultIfEmpty()
                                              where tcheck == null
                                              select d1).ToList();

                    lstCASToECPending = new ObservableCollection<IMapCasToECPending>(pendingSynNotinMap);
                    NotifyPropertyChanged("comonCASECPending");
                    VisibilityEcPending_Loader = Visibility.Collapsed;
                    NotifyPropertyChanged("VisibilityEcPending_Loader");
                }
            });
        }
        private void BindSubstanceNameApproved()
        {
            VisibilityEcApprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcApprove_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    var PrefSourceFieldId = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == "PREF_SOURCE").Select(z => z.FieldNameID).FirstOrDefault();
                    var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECApprove>("Select distinct xy.CASID, y.CAS, " +
                        " xy.ChemicalNameID as ID , z.ChemicalName as Value, xx.LanguageName from (select distinct a.CASID, b.ChemicalNameID from " +
                        " qsxxx_cas a inner join qsxxx_pref b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID and " +
                        " b.FieldNameID != " + PrefSourceFieldId + " and a.casid in (select CASID from qsxxx_cas where QSID_ID = " + Win_Administrator_VM.Qsid_ID + ") " +
                        " ) as xy " +
                        " inner join Map_SubstanceNameSynonyms aa on aa.casID = xy.CASID and aa.ChemicalNameID = xy.ChemicalNameID " +
                        " inner join Library_cas y on xy.CASId = y.CASID inner join Library_ChemicalNames z on " +
                        " xy.ChemicalNameID = z.ChemicalNameID inner join Library_Languages xx on z.languageId = xx.LanguageId ", Win_Administrator_VM.CommonListConn);
                    lstCASToECApprove = new ObservableCollection<IMapCasToECApprove>(SAPTPhrasesTable);
                    NotifyPropertyChanged("comonCASECApprove");
                    VisibilityEcApprove_Loader = Visibility.Collapsed;
                    NotifyPropertyChanged("VisibilityEcApprove_Loader");
                }

            });
        }

        private void BindSubstanceNameDisApproved()
        {
            VisibilityEcDisapprove_Loader = Visibility.Visible;
            NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
            Task.Run(() =>
            {
                using (var context = new CRAModel())
                {
                    //var PrefSourceFieldId = context.Library_FieldNames.AsNoTracking().Where(x => x.FieldName == "PREF_SOURCE").Select(z => z.FieldNameID).FirstOrDefault();
                    //var SAPTPhrasesTable1 = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>("Select distinct xy.CASID, y.CAS, " +
                    //   " xy.ChemicalNameID as ID, z.ChemicalName as Value, xx.LanguageName from (select distinct a.CASID, b.ChemicalNameID from " +
                    //   " qsxxx_cas a inner join qsxxx_pref b on a.QSID_ID = b.QSID_ID  and a.RowID = b.RowID and " +
                    //   " b.FieldNameID != " + PrefSourceFieldId + " and a.casid in (select CASID from qsxxx_cas where QSID_ID = " + Win_Administrator_VM.Qsid_ID + ") " +
                    //   " ) as xy " +
                    //   " inner join Map_SubstanceNameSynonyms_Disapproved aa on aa.casID = xy.CASID and aa.ChemicalNameID = xy.ChemicalNameID " +
                    //   " inner join Library_cas y on xy.CASId = y.CASID inner join Library_ChemicalNames z on " +
                    //   " xy.ChemicalNameID = z.ChemicalNameID inner join Library_Languages xx on z.languageId = xx.LanguageId", Win_Administrator_VM.CommonListConn);

                    var SAPTPhrasesTable = objCommon.DbaseQueryReturnSqlList<IMapCasToECDisApprove>(
                    "  select distinct a.CASID, d.CAS, b.ChemicalNameID as ID, c.ChemicalName as Value,e.LanguageName  from qsxxx_cas a," +
                    " Map_SubstanceNameSynonyms_Disapproved b, Library_ChemicalNames c, Library_CAS d, Library_Languages e " +
                    " where a.CASID = b.CASID and  b.ChemicalNameID = c.ChemicalNameID and c.LanguageID = e.LanguageID " +
                    " and a.CASID = d.CASID and  a.qsid_id = " + Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);

                    lstCASToECDisapprove = new ObservableCollection<IMapCasToECDisApprove>(SAPTPhrasesTable);
                    NotifyPropertyChanged("comonCASECDisapprove");
                    VisibilityEcDisapprove_Loader = Visibility.Collapsed;
                    NotifyPropertyChanged("VisibilityEcDisapprove_Loader");
                }
            });
        }
        public IMapCasToECPending SelectedPropertyECPending { get; set; }
        public IMapCasToECApprove SelectedPropertyECApprove { get; set; }
        public IMapCasToECDisApprove SelectedPropertyECDisapprove { get; set; }
        private ObservableCollection<IMapCasToECDisApprove> _lstCASToECDisapprove { get; set; } = new ObservableCollection<IMapCasToECDisApprove>();
        private ObservableCollection<IMapCasToECPending> _lstCASToECPending { get; set; } = new ObservableCollection<IMapCasToECPending>();

        private void NotifyMeECPending(PropertyChangedMessage<IMapCasToECPending> obj)
        {
            SelectedPropertyECPending = obj.NewValue;
            if (SelectedPropertyECPending != null)
            {
                if (selectedECItemCombo == "SubstanceName Synonyms")
                {
                    ShowCommentsPopUp(SelectedPropertyECPending.CAS, SelectedPropertyECPending.Value, SelectedPropertyECPending.CASID, SelectedPropertyECPending.ID, null, true);
                }
                else
                {
                    var identTypeId = GetIdentifierTypeID(selectedECItemCombo);
                    ShowCommentsPopUp(SelectedPropertyECPending.CAS, SelectedPropertyECPending.Value, SelectedPropertyECPending.CASID, SelectedPropertyECPending.ID, identTypeId);
                }
            }
        }
        private void NotifyMeECApprove(PropertyChangedMessage<IMapCasToECApprove> obj)
        {
            SelectedPropertyECApprove = obj.NewValue;
            if (SelectedPropertyECApprove != null)
            {
                if (selectedECItemCombo == "SubstanceName Synonyms")
                {
                    ShowCommentsPopUp(SelectedPropertyECApprove.CAS, SelectedPropertyECApprove.Value, SelectedPropertyECApprove.CASID, SelectedPropertyECApprove.ID, null, true);
                }
                else
                {
                    var identTypeId = GetIdentifierTypeID(selectedECItemCombo);
                    ShowCommentsPopUp(SelectedPropertyECApprove.CAS, SelectedPropertyECApprove.Value, SelectedPropertyECApprove.CASID, SelectedPropertyECApprove.ID, identTypeId);
                }
            }

        }
        public int GetIdentifierTypeID(string name)
        {
            using (var context = new CRAModel())
            {
                return context.Library_IdentifierCategories.Where(x => x.IdentifierTypeName == name).Select(x => x.IdentifierTypeID).FirstOrDefault();

            }
        }
        public void ShowCommentsPopUp(string cas, string identifierValue, int? casId, int? indentifierValueID, int? indentifierTypeID, bool isChemicalView = false)
        {
            if (isChemicalView)
            {
                objCommentDC_Identifier = new CommonGenericUIView.ViewModel.CommentsUC_VM(new GenericComment_VM(cas, "", casId, 0, 550, false, "", true, identifierValue, indentifierValueID, false, 0));
            }
            else
            {
                objCommentDC_Identifier = new CommonGenericUIView.ViewModel.CommentsUC_VM(new GenericComment_VM(cas, identifierValue, casId, indentifierValueID, 550, false, "", false, "", 0, true, indentifierTypeID));
            }
            NotifyPropertyChanged("objCommentDC_Identifier");
        }
        private void NotifyMeECDisapprove(PropertyChangedMessage<IMapCasToECDisApprove> obj)
        {
            SelectedPropertyECDisapprove = obj.NewValue;
            if (SelectedPropertyECDisapprove != null)
            {
                if (selectedECItemCombo == "SubstanceName Synonyms")
                {
                    ShowCommentsPopUp(SelectedPropertyECDisapprove.CAS, SelectedPropertyECDisapprove.Value, SelectedPropertyECDisapprove.CASID, SelectedPropertyECDisapprove.ID, null, true);
                }
                else
                {
                    var identTypeId = GetIdentifierTypeID(selectedECItemCombo);
                    ShowCommentsPopUp(SelectedPropertyECDisapprove.CAS, SelectedPropertyECDisapprove.Value, SelectedPropertyECDisapprove.CASID, SelectedPropertyECDisapprove.ID, identTypeId);
                }
            }


        }
        public ObservableCollection<IMapCasToECDisApprove> lstCASToECDisapprove
        {
            get { return _lstCASToECDisapprove; }
            set
            {
                if (_lstCASToECDisapprove != value)
                {
                    _lstCASToECDisapprove = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IMapCasToECDisApprove>>>(new PropertyChangedMessage<List<IMapCasToECDisApprove>>(null, _lstCASToECDisapprove.ToList(), "Default List"));
                }
            }
        }
        private ObservableCollection<IMapCasToECApprove> _lstCASToECApprove { get; set; } = new ObservableCollection<IMapCasToECApprove>();
        public ObservableCollection<IMapCasToECApprove> lstCASToECApprove
        {
            get { return _lstCASToECApprove; }
            set
            {
                if (_lstCASToECApprove != value)
                {
                    _lstCASToECApprove = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IMapCasToECApprove>>>(new PropertyChangedMessage<List<IMapCasToECApprove>>(null, _lstCASToECApprove.ToList(), "Default List"));
                }
            }
        }

        public ObservableCollection<IMapCasToECPending> lstCASToECPending
        {
            get { return _lstCASToECPending; }
            set
            {
                if (_lstCASToECPending != value)
                {
                    _lstCASToECPending = value;
                    MessengerInstance.Send<PropertyChangedMessage<List<IMapCasToECPending>>>(new PropertyChangedMessage<List<IMapCasToECPending>>(null, _lstCASToECPending.ToList(), "Default List"));
                }
            }
        }
        private List<CommonDataGridColumn> GetListGridColumnListPr()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Value", ColBindingName = "Value", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approve", ColBindingName = "CASID", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#07689f", CommandParam = "ECApprove" });

            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetListGridColumnECApprove()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Value", ColBindingName = "Value", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Disapprove", ColBindingName = "CASID", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "ECDisapprove" });

            return listColumnQsidDetail;
        }
        private List<CommonDataGridColumn> GetListGridColumnECPending()
        {
            List<CommonDataGridColumn> listColumnQsidDetail = new List<CommonDataGridColumn>();
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "CAS", ColBindingName = "CAS", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Value", ColBindingName = "Value", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "LanguageName", ColBindingName = "LanguageName", ColType = "Textbox", ColumnWidth = "300" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Approve", ColBindingName = "CASID", ColType = "Button", ColumnWidth = "100", ContentName = "Approve", BackgroundColor = "#07689f", CommandParam = "ECApprove" });
            listColumnQsidDetail.Add(new CommonDataGridColumn { IsSelected = true, ColName = "Disapprove", ColBindingName = "CASID", ColType = "Button", ColumnWidth = "100", ContentName = "Disapprove", BackgroundColor = "#ff414d", CommandParam = "ECDisapprove" });

            return listColumnQsidDetail;
        }

        private void CommandButtonExecutedECPending(NotificationMessageAction<object> obj)
        {
            switch (selectedECItemCombo)
            {
                case "EC Number":
                    ECPending(obj);
                    break;
                case "Molecular Formula":
                    MolecularPending(obj);
                    break;
                case "SubstanceName Synonyms":
                    SubstanceNamePending(obj);
                    break;
            }
        }
        private void CommandButtonExecutedECApprove(NotificationMessageAction<object> obj)
        {
            var result = System.Windows.MessageBox.Show("Are you sure do you want to Disapprove this CAS/Identifier ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                switch (selectedECItemCombo)
                {
                    case "EC Number":
                        ECApproved();
                        break;
                    case "Molecular Formula":
                        MolecularApproved();
                        break;
                    case "SubstanceName Synonyms":
                        SubstanceNameApproved();
                        break;
                }
            }
        }
        private void CommandButtonExecutedECDisapprove(NotificationMessageAction<object> obj)
        {
            var result = System.Windows.MessageBox.Show("Are you sure do you want to Approve this CAS/Identifier ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                switch (selectedECItemCombo)
                {
                    case "EC Number":
                        ECDisApproved();
                        break;
                    case "Molecular Formula":
                        MolecularDisApproved();
                        break;
                    case "SubstanceName Synonyms":
                        SubstanceNameDisApproved();
                        break;
                }
            }
        }

        private void ECPending(NotificationMessageAction<object> obj)
        {
            using (var context = new CRAModel())
            {
                if (obj.Notification == "ECDisapprove")
                {
                    var lgMainDis = new Map_CAS_ECNumber_Disapproved();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.IdentifierValueID = SelectedPropertyECPending.ID;
                    context.Map_CAS_ECNumber_Disapproved.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_CAS_EC_Disapproved>();
                    var lgLog = new Log_Map_CAS_EC_Disapproved
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        IdentifierValueID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASECNumberID_Disapproved = lgMainDis.CASECNumberID_Disapproved,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLog);
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");
                }
                if (obj.Notification == "ECApprove")
                {
                    var lgMainDis = new Map_CAS_ECNumber();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.IdentifierValueID = SelectedPropertyECPending.ID;
                    context.Map_CAS_ECNumber.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_CAS_EC>();
                    var lgLog = new Log_Map_CAS_EC
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        IdentifierValueID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASECNumberID = lgMainDis.CASECNumberID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);
                    _notifier.ShowSuccess("Selected Value approved successfully");
                }
            }
            BindECNumberPending();
            BindECNumberDisapproved();
            BindECNumberApproved();
        }
        private void ECApproved()
        {
            List<Log_Map_CAS_EC_Disapproved> lstLog = new List<Log_Map_CAS_EC_Disapproved>();
            List<Log_Map_CAS_EC> lstLogDis = new List<Log_Map_CAS_EC>();
            using (var context = new CRAModel())
            {
                var casid = SelectedPropertyECApprove.CASID;
                var idntval = SelectedPropertyECApprove.ID;
                var itemtoRemove = context.Map_CAS_ECNumber.AsNoTracking().Where(x => x.CASID == casid && x.IdentifierValueID == idntval).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_CAS_ECNumber_Disapproved();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                    context.Map_CAS_ECNumber_Disapproved.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_EC_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASECNumberID_Disapproved = lgMainDis.CASECNumberID_Disapproved,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLog);


                    var lgLogDis = new Log_Map_CAS_EC
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        CASECNumberID = itemtoRemove.CASECNumberID,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_CAS_ECNumber).Where(x => x.CASECNumberID == itemtoRemove.CASECNumberID).Delete();
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");

                }
            }
            BindECNumberDisapproved();
            BindECNumberApproved();
        }
        private void ECDisApproved()
        {
            List<Log_Map_CAS_EC> lstLog = new List<Log_Map_CAS_EC>();
            List<Log_Map_CAS_EC_Disapproved> lstLogDis = new List<Log_Map_CAS_EC_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_CAS_ECNumber_Disapproved.AsNoTracking().Where(x => x.CASID == SelectedPropertyECDisapprove.CASID && x.IdentifierValueID == SelectedPropertyECDisapprove.ID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_CAS_ECNumber();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                    context.Map_CAS_ECNumber.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_EC
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASECNumberID = lgMainDis.CASECNumberID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC>(lstLog);


                    var lgLogDis = new Log_Map_CAS_EC_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        CASECNumberID_Disapproved = itemtoRemove.CASECNumberID_Disapproved,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_EC_Disapproved>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_CAS_ECNumber_Disapproved).Where(x => x.CASECNumberID_Disapproved == itemtoRemove.CASECNumberID_Disapproved).Delete();
                    _notifier.ShowSuccess("Selected Value approved successfully");

                }
            }
            BindECNumberDisapproved();
            BindECNumberApproved();
        }

        private void MolecularDisApproved()
        {
            List<Log_Map_CAS_MolecularFormula> lstLog = new List<Log_Map_CAS_MolecularFormula>();
            List<Log_Map_CAS_MolecularFormula_Disapproved> lstLogDis = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_CAS_MolecularFormula_Disapproved.AsNoTracking().Where(x => x.CASID == SelectedPropertyECDisapprove.CASID && x.IdentifierValueID == SelectedPropertyECDisapprove.ID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_CAS_MolecularFormula();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                    context.Map_CAS_MolecularFormula.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_MolecularFormula
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID = lgMainDis.CASMolecularFormulaID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);


                    var lgLogDis = new Log_Map_CAS_MolecularFormula_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        CASMolecularFormulaID_Disapprove = itemtoRemove.CASMolecularFormulaID_Disapprove,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_CAS_MolecularFormula_Disapproved).Where(x => x.CASMolecularFormulaID_Disapprove == itemtoRemove.CASMolecularFormulaID_Disapprove).Delete();
                    _notifier.ShowSuccess("Selected Value approved successfully");

                }
            }
            BindMolecularDisapproved();
            BindMolecularApproved();
        }
        private void MolecularApproved()
        {
            List<Log_Map_CAS_MolecularFormula_Disapproved> lstLog = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
            List<Log_Map_CAS_MolecularFormula> lstLogDis = new List<Log_Map_CAS_MolecularFormula>();
            using (var context = new CRAModel())
            {
                var casid = SelectedPropertyECApprove.CASID;
                var idntval = SelectedPropertyECApprove.ID;
                var itemtoRemove = context.Map_CAS_MolecularFormula.AsNoTracking().Where(x => x.CASID == casid && x.IdentifierValueID == idntval).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_CAS_MolecularFormula_Disapproved();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.IdentifierValueID = itemtoRemove.IdentifierValueID;
                    context.Map_CAS_MolecularFormula_Disapproved.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_CAS_MolecularFormula_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID_Disapprove = lgMainDis.CASMolecularFormulaID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLog);


                    var lgLogDis = new Log_Map_CAS_MolecularFormula
                    {
                        CASID = itemtoRemove.CASID,
                        IdentifierValueID = itemtoRemove.IdentifierValueID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        CASMolecularFormulaID = itemtoRemove.CASMolecularFormulaID,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_CAS_MolecularFormula).Where(x => x.CASMolecularFormulaID == itemtoRemove.CASMolecularFormulaID).Delete();
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");

                }
            }
            BindMolecularDisapproved();
            BindMolecularApproved();
        }
        private void MolecularPending(NotificationMessageAction<object> obj)
        {
            using (var context = new CRAModel())
            {
                if (obj.Notification == "ECDisapprove")
                {
                    var lgMainDis = new Map_CAS_MolecularFormula_Disapproved();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.IdentifierValueID = SelectedPropertyECPending.ID;
                    context.Map_CAS_MolecularFormula_Disapproved.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_CAS_MolecularFormula_Disapproved>();
                    var lgLog = new Log_Map_CAS_MolecularFormula_Disapproved
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        IdentifierValueID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID_Disapprove = lgMainDis.CASMolecularFormulaID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula_Disapproved>(lstLog);
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");
                }
                if (obj.Notification == "ECApprove")
                {
                    var lgMainDis = new Map_CAS_MolecularFormula();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.IdentifierValueID = SelectedPropertyECPending.ID;
                    context.Map_CAS_MolecularFormula.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_CAS_MolecularFormula>();
                    var lgLog = new Log_Map_CAS_MolecularFormula
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        IdentifierValueID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        CASMolecularFormulaID = lgMainDis.CASMolecularFormulaID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_CAS_MolecularFormula>(lstLog);
                    _notifier.ShowSuccess("Selected Value approved successfully");
                }
            }
            BindMolecularPending();
            BindMolecularDisapproved();
            BindMolecularApproved();
        }

        private void SubstanceNameDisApproved()
        {
            List<Log_Map_SubstanceNameSynonyms> lstLog = new List<Log_Map_SubstanceNameSynonyms>();
            List<Log_Map_SubstanceNameSynonyms_Disapproved> lstLogDis = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
            using (var context = new CRAModel())
            {
                var itemtoRemove = context.Map_SubstanceNameSynonyms_Disapproved.AsNoTracking().Where(x => x.CASID == SelectedPropertyECDisapprove.CASID && x.ChemicalNameID == SelectedPropertyECDisapprove.ID).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_SubstanceNameSynonyms();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.ChemicalNameID = itemtoRemove.ChemicalNameID;
                    lgMainDis.IsIUPACName = false;
                    lgMainDis.SynonymID = objCommon.GetMaxAutoField("Map_SubstanceNameSynonyms");
                    context.Map_SubstanceNameSynonyms.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = itemtoRemove.CASID,
                        ChemicalNameID = itemtoRemove.ChemicalNameID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        Log_SynonymID = lgMainDis.SynonymID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);


                    var lgLogDis = new Log_Map_SubstanceNameSynonyms_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        ChemicalNameID = itemtoRemove.ChemicalNameID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        Log_SynonymID_Disapprove = itemtoRemove.SynonymID_Disapprove,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms_Disapproved).Where(x => x.SynonymID_Disapprove == itemtoRemove.SynonymID_Disapprove).Delete();
                    _notifier.ShowSuccess("Selected Value approved successfully");

                }
            }
            BindSubstanceNameDisApproved();
            BindSubstanceNameApproved();
        }
        private void SubstanceNameApproved()
        {
            List<Log_Map_SubstanceNameSynonyms_Disapproved> lstLog = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
            List<Log_Map_SubstanceNameSynonyms> lstLogDis = new List<Log_Map_SubstanceNameSynonyms>();
            using (var context = new CRAModel())
            {
                var casid = SelectedPropertyECApprove.CASID;
                var idntval = SelectedPropertyECApprove.ID;
                var itemtoRemove = context.Map_SubstanceNameSynonyms.AsNoTracking().Where(x => x.CASID == casid && x.ChemicalNameID == idntval).FirstOrDefault();
                if (itemtoRemove != null)
                {
                    var lgMainDis = new Map_SubstanceNameSynonyms_Disapproved();
                    lgMainDis.CASID = itemtoRemove.CASID;
                    lgMainDis.ChemicalNameID = itemtoRemove.ChemicalNameID;
                    context.Map_SubstanceNameSynonyms_Disapproved.Add(lgMainDis);
                    context.SaveChanges();

                    var lgLog = new Log_Map_SubstanceNameSynonyms_Disapproved
                    {
                        CASID = itemtoRemove.CASID,
                        ChemicalNameID = itemtoRemove.ChemicalNameID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID_Disapprove = lgMainDis.SynonymID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLog);


                    var lgLogDis = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = itemtoRemove.CASID,
                        ChemicalNameID = itemtoRemove.ChemicalNameID,
                        Status = "D",
                        SessionID = Engine.CurrentUserSessionID,
                        TimeStamp = objCommon.ESTTime(),
                        SynonymID = itemtoRemove.SynonymID,
                    };
                    lstLogDis.Add(lgLogDis);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLogDis);
                    EFBatchOperation.For(context, context.Map_SubstanceNameSynonyms).Where(x => x.SynonymID == itemtoRemove.SynonymID).Delete();
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");

                }
            }
            BindSubstanceNameDisApproved();
            BindSubstanceNameApproved();
        }
        private void SubstanceNamePending(NotificationMessageAction<object> obj)
        {
            using (var context = new CRAModel())
            {
                if (obj.Notification == "ECDisapprove")
                {
                    var lgMainDis = new Map_SubstanceNameSynonyms_Disapproved();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.ChemicalNameID = SelectedPropertyECPending.ID;
                    context.Map_SubstanceNameSynonyms_Disapproved.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_SubstanceNameSynonyms_Disapproved>();
                    var lgLog = new Log_Map_SubstanceNameSynonyms_Disapproved
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        ChemicalNameID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID_Disapprove = lgMainDis.SynonymID_Disapprove,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms_Disapproved>(lstLog);
                    _notifier.ShowSuccess("Selected Value Disapproved successfully");
                }
                if (obj.Notification == "ECApprove")
                {
                    var lgMainDis = new Map_SubstanceNameSynonyms();
                    lgMainDis.CASID = SelectedPropertyECPending.CASID;
                    lgMainDis.ChemicalNameID = SelectedPropertyECPending.ID;
                    context.Map_SubstanceNameSynonyms.Add(lgMainDis);
                    context.SaveChanges();
                    var lstLog = new List<Log_Map_SubstanceNameSynonyms>();
                    var lgLog = new Log_Map_SubstanceNameSynonyms
                    {
                        CASID = SelectedPropertyECPending.CASID,
                        ChemicalNameID = SelectedPropertyECPending.ID,
                        Status = "A",
                        SessionID = Engine.CurrentUserSessionID,
                        SynonymID = lgMainDis.SynonymID,
                        TimeStamp = objCommon.ESTTime(),
                    };
                    lstLog.Add(lgLog);
                    _objLibraryFunction_Service.BulkIns<Log_Map_SubstanceNameSynonyms>(lstLog);
                    _notifier.ShowSuccess("Selected Value approved successfully");
                }
            }
            BindSubstanceNamePending();
            BindSubstanceNameDisApproved();
            BindSubstanceNameApproved();
        }


    }
    public class IMapCasToECApprove
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ID { get; set; }
        public string LanguageName { get; set; }
        public string Value { get; set; }
    }
    public class IMapCasToECDisApprove
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ID { get; set; }

        public string LanguageName { get; set; }

        public string Value { get; set; }
    }
    public class IMapCasToECPending
    {
        public int CASID { get; set; }
        public string CAS { get; set; }
        public int ID { get; set; }

        public string LanguageName { get; set; }

        public string Value { get; set; }
    }
}
