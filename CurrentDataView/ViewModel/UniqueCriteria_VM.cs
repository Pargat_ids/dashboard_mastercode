﻿using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;

namespace CurrentDataView.ViewModel
{
    public  class UniqueCriteria_VM : Base_ViewModel
    {
        public StandardDataGrid_VM listUniqueCriteria { get; set; }
        public StandardDataGrid_VM listUniqueCriteriaHistory { get; set; }
        private RowIdentifier obj = new RowIdentifier();
        public Visibility VisibleLoderUniqueCriteria { get; set; }
        public UniqueCriteria_VM()
        {
            Task.Run(() => RunFunc());
        }
        private void RunFunc()
        {
            VisibleLoderUniqueCriteria = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderUniqueCriteria");
            if (Win_Administrator_VM.UniqueCriteria.Columns.Count == 0)
            {
                Win_Administrator_VM.UniqueCriteria = obj.GetUniqueCriteria(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            if (Win_Administrator_VM.UniqueCriteriaHistory.Columns.Count == 0)
            {
                Win_Administrator_VM.UniqueCriteriaHistory = obj.GetUniqueCriteriaHistory(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.CommonListConn);
            }
            listUniqueCriteria = new StandardDataGrid_VM("Unique Criteria", Win_Administrator_VM.UniqueCriteria, Visibility.Visible);
            NotifyPropertyChanged("listUniqueCriteria");
            listUniqueCriteriaHistory = new StandardDataGrid_VM("Unique Criteria History", Win_Administrator_VM.UniqueCriteriaHistory, Visibility.Visible);
            NotifyPropertyChanged("listUniqueCriteriaHistory");
            VisibleLoderUniqueCriteria = Visibility.Hidden;
            NotifyPropertyChanged("VisibleLoderUniqueCriteria");
        }
    }
}
