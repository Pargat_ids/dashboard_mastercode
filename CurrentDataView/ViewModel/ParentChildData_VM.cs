﻿using System.Data;
using System.Linq;
using CRA_DataAccess;
using System.Threading.Tasks;
using System.Windows;
using EntityClass;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;

namespace CurrentDataView.ViewModel
{
    public class ParentChildData_VM : Base_ViewModel
    {
        public ParentChildDataUC_VM listParentChildData { get; set; }
        private RowIdentifier rowIdentifier = new RowIdentifier();
        private readonly CommonFunctions objCommon = new CommonFunctions();

        public Visibility VisibleLoderParentChildData { get; set; }
        private bool CommandAccessCanExecute(object obj)
        {
            return true;
        }

        public ParentChildData_VM()
        {
            RunParentChildData();
            MessengerInstance.Register<PropertyChangedMessage<string>>(this, typeof(ParentChildData_VM), NotifyRefreshGrid);
        }

        private void NotifyRefreshGrid(PropertyChangedMessage<string> obj)
        {
            RunParentChildData();
        }

        private void RunParentChildData()
        {
            VisibleLoderParentChildData = Visibility.Visible;
            NotifyPropertyChanged("VisibleLoderParentChildData");
            if (Win_Administrator_VM.CurrentDataTableWithChild.Columns.Count == 0)
            {
                CurrentData_VM.SingleCurrentViewFunction();
            }
            Task.Run(() =>
            {
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("CasID"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "CasID",
                        DataType = typeof(int),
                        DefaultValue = 0,
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("FlatGenericsChildCount"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "FlatGenericsChildCount",
                        DataType = typeof(int),
                        DefaultValue = 0,
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("IsNeedMoreResearch"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "IsNeedMoreResearch",
                        DataType = typeof(string),
                        DefaultValue = "",
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("CombinedGenericsCount"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "CombinedGenericsCount",
                        DataType = typeof(int),
                        DefaultValue = 0,
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("SubstanceType"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "SubstanceType",
                        DataType = typeof(string),
                        DefaultValue = "",
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                if (!Win_Administrator_VM.ParentChildDataTable.Columns.Contains("SubstanceCat"))
                {
                    var newCol = new DataColumn
                    {
                        ColumnName = "SubstanceCat",
                        DataType = typeof(string),
                        DefaultValue = "",
                    };
                    Win_Administrator_VM.ParentChildDataTable.Columns.Add(newCol);
                }
                var lstCombine = GetParentChildDataWPF(Win_Administrator_VM.Qsid_ID, Win_Administrator_VM.ParentChildDataTable);
                Win_Administrator_VM.ChildDataForPopup = lstCombine;

                using (var context = new CRAModel())
                {
                    var substanceTypes = objCommon.DbaseQueryReturnTableSql("select a.CASID, SubstanceTypeName, IsValid from qsxxx_cas a inner join library_cas b on a.CASID = b.CASID and a.QSID_ID =  " + Win_Administrator_VM.Qsid_ID +
                    " left join Map_SubstanceType c on a.CASID = c.CASID " +
                    " left join Library_SubstanceType d on c.SubstanceTypeID = d.SubstanceTypeID", Win_Administrator_VM.CommonListConn);

                    var substanceCat = objCommon.DbaseQueryReturnTableSql("select a.CASID, SubstanceCategoryName, IsValid from qsxxx_cas a inner join library_cas b on a.CASID = b.CASID and a.QSID_ID =  " + Win_Administrator_VM.Qsid_ID +
                    " left join Map_SubstanceCategory c on a.CASID = c.CASID " +
                    " left join Library_SubstanceCategories d on c.SubstanceCategoryID = d.SubstanceCategoryID", Win_Administrator_VM.CommonListConn);

                    string queryGetFlatGenericCount = "Select count(1) as ChildCount  from {0} f where f.ParentCasID ='{1}' ";
                    string queryIsNeedMoreRes= "Select (case when exists(Select 1 from {0} where Additional_Generics_research_flag = 1 and CasID = {1}) then 'Yes' else '' end) IsNeedMoreRes";

                    Win_Administrator_VM.ParentChildDataTable.AsEnumerable().ToList().ForEach(x =>
                    {
                        if (string.IsNullOrEmpty(x.Field<string>("CAS"))==false && x.Field<string>("CAS").Contains("-"))
                        {
                            x.SetField("CAS", x.Field<string>("CAS").Replace("-", ""));
                        }
                    });

                    var joinWithCasid = (from d1 in Win_Administrator_VM.ParentChildDataTable.AsEnumerable()
                                         join d2 in context.Library_CAS.AsNoTracking()
                                         on d1.Field<string>("CAS") equals d2.CAS
                                         select new { d1, d2.CASID }).ToList();

                    joinWithCasid.ForEach(x =>
                    {
                        x.d1.SetField("CasID", x.CASID);
                    });

                    var joinWithSubType = (from d1 in Win_Administrator_VM.ParentChildDataTable.AsEnumerable()
                                           join d2 in substanceTypes.AsEnumerable()
                                           on d1.Field<int>("CASID") equals d2.Field<int>("CASID")
                                           select new
                                           {
                                               d1,
                                               SubstanceTypeName = d2.Field<string>("SubstanceTypeName"),
                                           }).ToList();

                    joinWithSubType.ForEach(x =>
                    {
                        x.d1.SetField("SubstanceType", "");
                        x.d1.SetField("SubstanceType", x.SubstanceTypeName);
                    });

                    var joinWithSubCat = (from d1 in Win_Administrator_VM.ParentChildDataTable.AsEnumerable()
                                          join d2 in substanceCat.AsEnumerable()
                                          on d1.Field<int>("CASID") equals d2.Field<int>("CASID")
                                          select new
                                          {
                                              d1,
                                              SubstanceCategoryName = d2.Field<string>("SubstanceCategoryName"),
                                          }).ToList();

                    joinWithSubCat.ForEach(x =>
                    {
                        x.d1.SetField("SubstanceCat", "");
                        x.d1.SetField("SubstanceCat", x.SubstanceCategoryName);
                    });

                    Win_Administrator_VM.ParentChildDataTable.AsEnumerable().ToList().ForEach(x =>
                 {
                     var casDes = x.Field<int>("CASID");

                     x.SetField("FlatGenericsChildCount", objCommon.DbaseQueryReturnStringSQL(string.Format(queryGetFlatGenericCount, "Map_Generics_Flat", casDes), Win_Administrator_VM.CommonListConn));

                     x.SetField("CombinedGenericsCount", objCommon.DbaseQueryReturnStringSQL(string.Format(queryGetFlatGenericCount, "Map_Generics_Combined_Hydrates_AlternateCAS_etc", casDes), Win_Administrator_VM.CommonListConn));
                     x.SetField("IsNeedMoreResearch", objCommon.DbaseQueryReturnStringSQL(string.Format(queryIsNeedMoreRes, "Map_Substance_Comments", casDes), Win_Administrator_VM.CommonListConn));
                 });

                }

                if (Win_Administrator_VM.ParentChildDataTable.Rows.Count > 0)
                {
                    Win_Administrator_VM.ParentChildDataTable.Columns["CasID"].SetOrdinal(1);
                    Win_Administrator_VM.ParentChildDataTable.Columns["CAS"].SetOrdinal(2);
                    Win_Administrator_VM.ParentChildDataTable.Columns["FlatGenericsChildCount"].SetOrdinal(3);
                    Win_Administrator_VM.ParentChildDataTable.Columns["CombinedGenericsCount"].SetOrdinal(4);
                    Win_Administrator_VM.ParentChildDataTable.Columns["SubstanceType"].SetOrdinal(5);
                    Win_Administrator_VM.ParentChildDataTable.Columns["SubstanceCat"].SetOrdinal(6);
                    Win_Administrator_VM.ParentChildDataTable.Columns["IsNeedMoreResearch"].SetOrdinal(7);
                }

                if (Win_Administrator_VM.ParentChildDataTable.Rows.Count > 0)
                {
                    listParentChildData = new ParentChildDataUC_VM("Parent/Child Data", Win_Administrator_VM.ParentChildDataTable);
                    NotifyPropertyChanged("listParentChildData");
                }
                VisibleLoderParentChildData = Visibility.Hidden;
                NotifyPropertyChanged("VisibleLoderParentChildData");
            });

        }
        public List<IParentChildD> GetParentChildDataWPF(int qsidID, DataTable dtFinal1)
        {
            int dtfinalCount = dtFinal1.Rows.Count;
            using (CRAModel context = new CRAModel())
            {
                var currentQsid = context.Library_QSIDs.AsNoTracking().Where(x => x.QSID_ID == qsidID).Select(y => y.QSID).FirstOrDefault();
                if (currentQsid != null)
                {
                    //List<int> onlyCas = context.QSxxx_CAS.AsNoTracking().Where(x => x.QSID_ID == qsidID).Select(d2 => d2.CASID).Distinct().ToList();
                    DataTable genericData = new DataTable();
                    DataTable combineParents = new DataTable();
                    DataTable combineChilds = new DataTable();
                    if (currentQsid.ToUpper().Contains("QSFF"))
                    {
                        genericData = objCommon.DbaseQueryReturnTableSql("select distinct ParentCASID,ChildCASID from QSxxx_CAS a, " +
                                                    " Generics_ParentChild b where " +
                                                    " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIF] = 1", Win_Administrator_VM.CommonListConn);
                        combineParents = objCommon.DbaseQueryReturnTableSql("select distinct CASID,CAS,IsValid from (select distinct ParentCASID from QSxxx_CAS a, " +
                            " Generics_ParentChild b where " +
                            " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIF] = 1) x " +
                            " inner join Library_CAS y on x.ParentCASID = y.CASID", Win_Administrator_VM.CommonListConn);
                        combineChilds = objCommon.DbaseQueryReturnTableSql("select distinct CASID,CAS,IsValid from (select distinct ChildCASID from QSxxx_CAS a, " +
                           " Generics_ParentChild b where " +
                           " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIF] = 1) x " +
                           " inner join Library_CAS y on x.ChildCASID = y.CASID", Win_Administrator_VM.CommonListConn);
                    }
                    else
                    {
                        genericData = objCommon.DbaseQueryReturnTableSql("select distinct ParentCASID,ChildCASID from QSxxx_CAS a, " +
                                                    " Generics_ParentChild b where " +
                                                    " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIC] = 1", Win_Administrator_VM.CommonListConn);
                        combineParents = objCommon.DbaseQueryReturnTableSql("select distinct CASID,CAS,IsValid from (select distinct ParentCASID from QSxxx_CAS a, " +
                            " Generics_ParentChild b where " +
                            " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIC] = 1) x " +
                            " inner join Library_CAS y on x.ParentCASID = y.CASID", Win_Administrator_VM.CommonListConn);

                        combineChilds = objCommon.DbaseQueryReturnTableSql("select distinct CASID,CAS,IsValid from (select distinct ChildCASID from QSxxx_CAS a, " +
                            " Generics_ParentChild b where " +
                            " a.CASID = b.ParentCASID and a.QSID_ID = " + Win_Administrator_VM.Qsid_ID + " and b.[3EIC] = 1) x " +
                            " inner join Library_CAS y on x.ChildCASID = y.CASID", Win_Administrator_VM.CommonListConn);
                    }

                    var lstCombine = (from d1 in genericData.AsEnumerable()
                                      join d2 in combineParents.AsEnumerable()
                                      on d1.Field<int>("ParentCASID") equals d2.Field<int>("CASID")
                                      join d3 in combineChilds.AsEnumerable()
                                      on d1.Field<int>("ChildCASID") equals d3.Field<int>("CASID")
                                      select new IParentChildD
                                      {
                                          ParentCAS = d2.Field<bool>("IsValid") ? objCommon.FormatLibraryCasWithDash(d2.Field<string>("CAS")) : d2.Field<string>("CAS"),
                                          ChildCAS = d3.Field<bool>("IsValid") ? objCommon.FormatLibraryCasWithDash(d3.Field<string>("CAS")) : d3.Field<string>("CAS"),
                                      }).Distinct().ToList();
                    return lstCombine;
                }
                else
                {
                    return null;
                }

            }
        }
        public class IParentChildD
        {
            public string ParentCAS { get; set; }

            public string ChildCAS { get; set; }
        }
    }
}
