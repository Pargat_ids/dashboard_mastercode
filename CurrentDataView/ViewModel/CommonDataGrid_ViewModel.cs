﻿using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using ToastNotifications.Messages;
using VersionControlSystem.Model.ApplicationEngine;
using VersionControlSystem.Model.ViewModel;
using DataGridFilterLibrary.Querying;
using DataGridFilterLibrary.Support;
using CurrentDataView.Library;
using System;
using System.Collections;
using VersionControlSystem.Business.IBusinessService;
using System.Diagnostics;

namespace CurrentDataView.ViewModel
{
    public class CommonDataGrid_ViewModel<T> : Base_ViewModel where T : class
    {
        #region Data Grid Property Member
        public ICommand CommandGenerateAccessCasResult { get; set; }
        public ICommand CommandGenerateExcelCasResult { get; set; }
        private ObservableCollection<T> _itemSource { get; set; }
        public ObservableCollection<T> ItemSource
        {
            get { return _itemSource; }
            set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    NotifyPropertyChanged("ItemSource");
                }
            }
        }
        public List<T> defaultList { get; set; }
        public List<T> listDataGridItemSource { get; set; }
        private T _selectedQsidVM { get; set; }
        public T SelectedQsidVM
        {
            get { return _selectedQsidVM; }
            set
            {
                if (_selectedQsidVM != value)
                {
                    _selectedQsidVM = value;

                    NotifyPropertyChanged("SelectedQsidVM");

                    MessengerInstance.Send<PropertyChangedMessage<T>>(new PropertyChangedMessage<T>(SelectedQsidVM, SelectedQsidVM, "Selected Value"));
                }
            }
        }

        private string _searchCriteria { get; set; }
        public string SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                if (value != _searchCriteria)
                {
                    _searchCriteria = value;
                    MessengerInstance.Send<PropertyChangedMessage<string>>(new PropertyChangedMessage<string>(_searchCriteria, _searchCriteria, "SearchCriteria_" + typeof(T).Name));
                }
            }
        }
        private ObservableCollection<DataGridColumn> _columnCollection { get; set; }
        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get
            {
                return this._columnCollection;
            }
            set
            {
                _columnCollection = value;
                NotifyPropertyChanged("ColumnCollection");
            }
        }
        public ObservableCollection<CommonDataGridColumn> defaultListColumnGrid { get; set; }
        private DataGridCellInfo _currentCellData { get; set; }
        public DataGridCellInfo CurrentCellData
        {
            get { return _currentCellData; }
            set
            {
                if (value != _currentCellData)
                {
                    _currentCellData = value;
                }
                MessengerInstance.Send<PropertyChangedMessage<DataGridCellInfo>>(new PropertyChangedMessage<DataGridCellInfo>(typeof(T).Name, _currentCellData, _currentCellData, "Current Cell Value"));
            }
        }
        private bool IsNavigateProgress { get; set; }
        public string defaultSortedBy { get; set; }
        public string defaultTitleContent { get; set; }
        public bool IsSynchronizedWithCurrentItem { get; set; }
        private bool defaultColWidthChange { get; set; } = false;
        private int _defaultColumnBinding { get; set; } = 250;
        public int defaultColumnWidthBinding
        {
            get { return _defaultColumnBinding; }
            set
            {
                if (_defaultColumnBinding != value)
                {
                    _defaultColumnBinding = value;
                    NotifyPropertyChanged("defaultColumnWidthBinding");
                    if (value > 100)
                    {
                        defaultColWidthChange = true;
                        BindDataGridColumnCollection();
                        defaultColWidthChange = false;
                    }
                }
            }
        }
        private List<string> listSortingMemberDirection = new List<string>();
        private ListSortDirection? lastSortDirection;

        private string lastSortMemberPath;
        public string SortingExpression { get; set; }
        private bool _isLoaderShow { get; set; } = false;
        public bool IsLoaderShow
        {
            get { return _isLoaderShow; }
            set
            {
                if (_isLoaderShow != value)
                {
                    _isLoaderShow = value;
                    if (_isLoaderShow)
                    {
                        Visibility_Loader = Visibility.Visible;
                    }
                    else
                    {
                        Visibility_Loader = Visibility.Collapsed;
                    }
                    NotifyPropertyChanged("Visibility_Loader");
                }
            }
        }
        public Visibility Visibility_Loader { get; set; } = Visibility.Collapsed;
        public Visibility dgVisibility { get; set; } = Visibility.Collapsed;
        public Visibility visibilityAddNoteButton { get; set; } = Visibility.Collapsed;
        public Visibility visibilityParentQsidButton { get; set; } = Visibility.Collapsed;
        #endregion

        #region Data Grid Icommand And Action
        public ICommand CommmandChangeVisiblityListCol { get; set; }
        public ICommand CommandDataGridSorting { get; set; }
        public ICommand CommandDataGridClearSorting { get; set; }
        public ICommand CommandRefreshItemDetailGrid { get; private set; }
        public ICommand CommandCopyCellDataToClipboard { get; set; }

        public ICommand CommandDataGridLoaded { get; private set; }
        public ICommand CommandDataGridSourceUpdated { get; private set; }
        public ICommand CommandAddNote { get; private set; }
        public ICommand CommandParentQsid { get; private set; }

        public ICommand CommandHyperlinkClick { get; private set; }
        public ICommand CommandButton { get; private set; }

        private bool CommandCopyCellDataToClipboardCanExecute(object obj)
        {
            return true;
        }

        private void CommandCopyCellDataToClipboardExecute(object obj)
        {
            try
            {
                var text = string.Empty;
                if (_currentCellData.Column != null)
                {
                    if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTextColumn))
                    {
                        text = ((System.Windows.Controls.TextBlock)_currentCellData.Column.GetCellContent(_currentCellData.Item)).Text.ToString();
                    }
                    else if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridComboBoxColumn))
                    {
                        var objSelected = ((System.Windows.Controls.ComboBox)_currentCellData.Column.GetCellContent(_currentCellData.Item)).SelectedItem;
                        if (objSelected != null)
                        {
                            if (objSelected.GetType() == typeof(SelectListItem))
                            {
                                text = ((SelectListItem)objSelected).Text;
                            }
                            else if (objSelected.GetType() == typeof(ListCountries_ViewModel))
                            {
                                text = ((ListCountries_ViewModel)objSelected).Name;
                            }
                            else if (objSelected.GetType() == typeof(ListTopic_ViewModel))
                            {
                                text = ((ListTopic_ViewModel)objSelected).Name;
                            }
                            else if (objSelected.GetType() == typeof(ListModule_ViewModel))
                            {
                                text = ((ListModule_ViewModel)objSelected).Name;
                            }
                            else if (objSelected.GetType() == typeof(VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel))
                            {
                                text = ((VersionControlSystem.Model.ViewModel.ListCheckBoxType_ViewModel)objSelected).Content;
                            }
                            else if (objSelected.GetType() == typeof(ListCommonComboBox_ViewModel))
                            {
                                text = ((ListCommonComboBox_ViewModel)objSelected).Name;
                            }
                        }
                        else
                        {
                            _notifier.ShowError("Cell text empty or unable to copy!");
                        }
                    }
                    else if (_currentCellData.Column.GetType() == typeof(System.Windows.Controls.DataGridTemplateColumn))
                    {
                        var propertName = ((System.Windows.PropertyPath)_currentCellData.Column.ClipboardContentBinding.GetType().GetProperty("Path").GetValue(_currentCellData.Column.ClipboardContentBinding)).Path;
                        var property = _currentCellData.Item.GetType().GetProperty(propertName);
                        var listData = property.GetValue(_currentCellData.Item);
                        if (listData.GetType() == typeof(List<HyperLinkDataValue>))
                        {
                            text = String.Join(", ", ((List<HyperLinkDataValue>)listData).Select(x => x.DisplayValue).ToArray());
                        }
                        else
                        {
                            _notifier.ShowWarning("Unable to find column type list in instruction of copy functionality. Connect with Adminstrator!");
                        }
                    }
                    else
                    {
                        _notifier.ShowWarning("Unable to find column type list in instruction of copy functionality. Connect with Adminstrator!");
                    }
                }
                else
                {
                    _notifier.ShowWarning("Unable to focus the selected column Kindly Refocus and copy again!");
                }
                Clipboard.SetDataObject(text);
            }
            catch (Exception ex)
            {
                _notifier.ShowError("Error: while copy the text! " + ex.Message);
            }
        }

        private bool CommandRefreshItemDetailGridCanExecute(object obj)
        {
            return true;
        }

        private void CommandRefreshItemDetailGridExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<T>>(new NotificationMessageAction<T>("notification message", CallbackExecute));

        }
        private void CallbackExecute(string obj)
        {

        }
        private void CallbackExecute(object obj)
        {

        }
        private void CallbackExecute(T obj)
        {

        }
        private bool CommandDataGridClearSortingCanExecute(object obj)
        {
            bool IsRemoveSorting = false;
            if (listSortingMemberDirection.Count > 0)
            {
                IsRemoveSorting = true;
            }
            return IsRemoveSorting;
        }

        private void CommandDataGridClearSortingExecute(object obj)
        {
            listSortingMemberDirection = new List<string>();
            SortingExpression = string.Empty;
            NotifyPropertyChanged("SortingExpression");
            listDataGridItemSource = listDataGridItemSource.AsQueryable().OrderBy(defaultSortedBy).ToList();
            Navigate(0);
        }

        private bool CommandDataGridSortingCanExecute(object obj)
        {
            return true;
        }

        private void CommandDataGridSortingExecute(object obj)
        {
            Sorting((DataGridSortingEventArgs)obj);
        }
        public void Sorting(DataGridSortingEventArgs e)
        {
            e.Handled = true;

            if (e.Column.SortMemberPath == lastSortMemberPath)
            {
                if (lastSortDirection == ListSortDirection.Ascending)
                    e.Column.SortDirection = ListSortDirection.Descending;
                else
                    e.Column.SortDirection = ListSortDirection.Ascending;
            }
            else
                e.Column.SortDirection = ListSortDirection.Ascending;

            string dir = (e.Column.SortDirection == ListSortDirection.Ascending) ? "ASC" : "DESC";
            lastSortDirection = e.Column.SortDirection;
            lastSortMemberPath = e.Column.SortMemberPath;
            if (!string.IsNullOrEmpty(lastSortMemberPath))
            {
                var itemRemoved = listSortingMemberDirection.Where(x => x.Contains(lastSortMemberPath)).FirstOrDefault();
                listSortingMemberDirection.Remove(itemRemoved);
            }
            listSortingMemberDirection.Add(lastSortMemberPath + " " + dir);
            SortDefaultList();
            SortingExpression = "Sort By: " + String.Join(", ", listSortingMemberDirection.ToArray());
            NotifyPropertyChanged("SortingExpression");

        }

        private void SortDefaultList()
        {
            var orderByString = String.Join(", ", listSortingMemberDirection.ToArray());
            listDataGridItemSource = listDataGridItemSource.AsQueryable().OrderBy(orderByString).ToList();

            Navigate(0);
        }

        private bool CommandDataGridLoadedCanExecute(object obj)
        {
            return true;
        }

        private void CommandDataGridLoadedExecute(object obj)
        {
            var dg = (DataGrid)((RoutedEventArgs)obj).Source;
            foreach (var col in dg.Columns)
            {

                //double colActualWidthFirst = col.Width.DisplayValue;
                //col.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                double colActualWidth = col.Width.DisplayValue;
                if (colActualWidth < 30)
                {
                    col.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToHeader);
                    // col.MaxWidth = defaultColumnWidthBinding;
                }
                if (colActualWidth > 30 && colActualWidth < defaultColumnWidthBinding)
                {
                    if (col.Width != null && col.Width.Value != 0 && col.Width.Value > 150)
                    {
                        col.Width = new DataGridLength(col.Width.Value);
                    }
                    else
                    {
                        col.Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
                    }
                }
                else if (colActualWidth > defaultColumnWidthBinding)
                {
                    col.Width = defaultColumnWidthBinding;
                }

            }
        }
        private bool CommandDataGridSourceUpdatedCanExecute(object obj)
        {
            return true;
        }

        public void RefreshDataGrid()
        {
            Dispatcher.CurrentDispatcher.InvokeAsync(() => { System.Windows.Input.Mouse.OverrideCursor = null; }, DispatcherPriority.ApplicationIdle);
        }

        private void CommandDataGridSourceUpdatedExecute(object obj)
        {
            var dg = (DataGrid)((System.Windows.SizeChangedEventArgs)obj).Source;
            foreach (var col in dg.Columns)
            {
                col.MaxWidth = Double.PositiveInfinity;
            }
        }
        private void CommandAddNoteExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("AddNote", CallbackExecute));
        }
        private void CommandParentQsidExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<string>>(new NotificationMessageAction<string>("AddParentChildQsid", CallbackExecute));
        }
        public void NotifyMe(string callAction)
        {

        }
        private bool CommandAddNoteCanExecute(object obj)
        {
            bool result = true;
            return result;
        }

        public bool CommandHyperlinkClickCanExecute(object obj)
        {
            return true;
        }
        public void CommandHyperlinkClickExecuted(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<object>>(new NotificationMessageAction<object>(obj.ToString(), CallbackExecute), typeof(T));
        }

        public void CommmandButtonExecute(object obj)
        {
            MessengerInstance.Send<NotificationMessageAction<object>>(new NotificationMessageAction<object>(obj.ToString(), CallbackExecute), typeof(T));
        }

        #endregion

        #region Filter Query Property Member
        private QueryController _overrideQueryController { get; set; }
        public QueryController OverrideQueryController
        {
            get { return _overrideQueryController; }
            set
            {
                _overrideQueryController = value;
                NotifyPropertyChanged("OverrideQueryController");
                //dgColFilter.query_FilteringStarted += DataGridFilteringStarted;
            }
        }
        DataGridFilterLibrary.DataGridColumnFilter dgColFilter { get; set; }
        private Dictionary<string, FilterData> filtersForColumns;
        private Query query;
        public FilterData FilterCurrentData
        {
            get { return _filterCurrentData; }
            set
            {
                _filterCurrentData = value;
                FilterCurrentData.FilterChangedEvent -= new EventHandler<EventArgs>(filterCurrentData_FilterChangedEvent);
                FilterCurrentData.FilterChangedEvent += new EventHandler<EventArgs>(filterCurrentData_FilterChangedEvent);
                NotifyPropertyChanged("FilterCurrentData");
            }
        }

        private DataGridFilterLibrary.Support.FilterData _filterCurrentData { get; set; }

        #endregion
        #region Filter ICommand And Action
        public ICommand CommandDataGridClearFilter { get; private set; }

        private bool CommandDataGridClearFilterCanExecute(object obj)
        {
            return true;
        }

        public void CommandDataGridClearFilterExecute(object sender)
        {
            SearchCriteria = string.Empty;
            if (ListAllColumnDGNull.Any(x => x.IsNotNull || x.IsNull))
            {
                ListAllColumnDGNull.ToList().ForEach(x =>
                {
                    x.IsNotNull = false;
                    x.IsNull = false;
                });
                NotifyPropertyChanged("ListAllColumnDGNull");
                queryNull = new List<string>();
                CommandOnColSelectedChangeNullExecute(null);
            }

            OverrideQueryController.ClearFilter();

        }
        public void RefreshFilterComponent()
        {
            OverrideQueryController = new QueryController();

            query = new Query();
            filtersForColumns = new Dictionary<string, FilterData>();
            OverrideQueryController.FilteringStarted -= DataGridFilteringStarted;
            OverrideQueryController.FilteringStarted += DataGridFilteringStarted;
        }
        private void DataGridFilteringError(object sender, FilteringEventArgs e)
        {
            _notifier.ShowError("Error in Filter Grid: " + e.Error.Message);
        }
        void filterCurrentData_FilterChangedEvent(object sender, EventArgs e)
        {
            addFilterStateHandlers(OverrideQueryController);
        }
        private void addFilterStateHandlers(QueryController query)
        {
            query.FilteringStarted -= new EventHandler<EventArgs>(query_FilteringStarted);
            query.FilteringFinished -= new EventHandler<EventArgs>(query_FilteringFinished);

            query.FilteringStarted += new EventHandler<EventArgs>(query_FilteringStarted);
            query.FilteringFinished += new EventHandler<EventArgs>(query_FilteringFinished);
        }
        private void query_FilteringFinished(object sender, EventArgs e)
        {
            if (FilterCurrentData != null && FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
            {
                if (FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
                {
                    // FilterCurrentData.IsFilteringInProgress = false;
                }
            }
        }
        private void query_FilteringStarted(object sender, EventArgs e)
        {
            if (FilterCurrentData != null && FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
            {
                if (FilterCurrentData.Equals((sender as QueryController).ColumnFilterData))
                {
                    //data.IsFilteringInProgress = true;
                }
            }
        }
        private void DataGridFilteringStarted(object sender, EventArgs e)
        {
            if (!IsNavigateProgress)
            {
                pageIndex = 1;
                FilterCurrentData = OverrideQueryController.ColumnFilterData;
                if (OverrideQueryController.ColumnFilterData.Operator == FilterOperator.Undefined)
                {
                    filtersForColumns = new Dictionary<string, FilterData>();
                    OverrideQueryController.ClearFilter();
                }

                if (OverrideQueryController.ColumnFilterData != null && OverrideQueryController.ColumnFilterData.Operator != FilterOperator.Undefined)
                {
                    if (!filtersForColumns.ContainsKey(OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath))
                    {
                        filtersForColumns.Add(OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath, OverrideQueryController.ColumnFilterData);
                    }
                    else
                    {
                        filtersForColumns[OverrideQueryController.ColumnFilterData.ValuePropertyBindingPath] = OverrideQueryController.ColumnFilterData;
                    }
                }
                var customizeFilter = filtersForColumns.Where(x => x.Key == "AlternateCAS" || x.Key == "PrimaryCAS" || x.Key == "Anhydrous_CAS" || x.Key == "Hydrous_CAS" || x.Key == "Cas.CasOrignal" || x.Key == "ParentCas" || x.Key == "ChildCas").FirstOrDefault();
                if (!string.IsNullOrEmpty(customizeFilter.Key))
                {
                    customizeFilter.Value.QueryString = customizeFilter.Value.QueryString.Replace("-", "");
                }

                QueryCreator queryCreator = new QueryCreator(filtersForColumns);
                queryCreator.CreateFilter(ref query);

                if (query.FilterString != String.Empty)
                {
                    listDataGridItemSource = defaultList.AsQueryable().Where(query.FilterString, query.QueryParameters.ToArray<object>()).ToList();
                    Navigate(0);
                    CreateFilterQuery();
                }
                else
                {
                    if (string.IsNullOrEmpty(queryStringForNUll))
                    {
                        listDataGridItemSource = defaultList.ToList();
                        Navigate(0);
                        SearchCriteria = "";
                    }
                    else
                    {
                        var result = string.Join("&&", queryNull.ToList());
                        if (result != string.Empty)
                        {
                            listDataGridItemSource = defaultList.AsQueryable().Where(result.Trim()).ToList();
                        }
                        else
                        {
                            listDataGridItemSource = defaultList.ToList();
                        }
                        ItemSource = new ObservableCollection<T>(listDataGridItemSource);
                        SearchCriteria = queryStringForNUll;
                        Navigate(0);
                    }
                }
            }
        }

        public void CreateFilterQuery()
        {
            string queryString = string.Empty;
            foreach (var item in filtersForColumns)
            {
                if (item.Key != "" && item.Value.QueryString != "")
                    queryString += " " + item.Key + " Like '" + item.Value.QueryString + (!string.IsNullOrEmpty(item.Value.QueryStringTo) ? " And " + item.Value.QueryStringTo : "") + "',";
            }
            if (string.IsNullOrEmpty(queryStringForNUll))
            {
                SearchCriteria = queryString;
            }
            else
            {
                SearchCriteria = queryStringForNUll + ", " + queryString;
            }
        }

        public void BindDataGridColumnCollection()
        {
            ColumnCollection = new ObservableCollection<DataGridColumn>();
            var currentModelProp = typeof(T).GetProperties();
            foreach (var itemCurrentCol in defaultListColumnGrid)
            {

                switch (itemCurrentCol.ColType)
                {
                    case "Combobox":
                        ColumnCollection.Add(CreateDataGridComboBoxColumn(itemCurrentCol, currentModelProp));
                        break;
                    case "Checkbox":
                        ColumnCollection.Add(CreateDataGridCheckboxColumn(itemCurrentCol, currentModelProp));
                        break;
                    case "CheckboxEnable":
                        ColumnCollection.Add(CreateDataGridCheckboxEnableColumn(itemCurrentCol, currentModelProp));
                        break;
                    case "Image":
                        ColumnCollection.Add(CreateDataGridImageColumn(itemCurrentCol, currentModelProp));
                        break;
                    case "Hyperlink":
                        ColumnCollection.Add(CreateDataGridHyperlinkColumn(itemCurrentCol, currentModelProp));
                        break;
                    case "MultipleHyperLinkCol":
                        ColumnCollection.Add(CreateMultipleHyperLink(itemCurrentCol, currentModelProp));
                        break;
                    case "Button":
                        ColumnCollection.Add(CreateDataGridButtonColumn(itemCurrentCol, currentModelProp));
                        break;
                    default:
                        ColumnCollection.Add(CreateDataGridTextColumn(itemCurrentCol, currentModelProp));
                        break;
                }
            }
            NotifyPropertyChanged("ColumnCollection");
        }
        public DataGridComboBoxColumn CreateDataGridComboBoxColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridComboBoxColumn comboBoxCol = new DataGridComboBoxColumn();
            comboBoxCol.Header = objCurrentCol.ColName;
            comboBoxCol.SelectedItemBinding = bindings;
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetUserCanEnterText(comboBoxCol, true);
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetIsTextFilter(comboBoxCol, false);
            comboBoxCol.DisplayMemberPath = "Name";
            comboBoxCol.SelectedValuePath = "Id";
            comboBoxCol.ClipboardContentBinding = bindings;
            if (defaultColWidthChange)
            {
                comboBoxCol.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    comboBoxCol.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    comboBoxCol.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            comboBoxCol.SortMemberPath = objCurrentCol.ColBindingName + ".Name";
            if (!objCurrentCol.IsSelected)
            {
                comboBoxCol.Visibility = Visibility.Collapsed;
            }
            switch (objCurrentCol.ColBindingName)
            {
                case "Country":
                    comboBoxCol.ItemsSource = Engine._listCountry;
                    break;
                case "Module":
                    comboBoxCol.ItemsSource = Engine._listModule;
                    break;
                case "Topic":
                    comboBoxCol.ItemsSource = Engine._listTopic;
                    break;
                case "ProductName":
                    comboBoxCol.ItemsSource = Engine._listProducts;
                    break;
                case "ListType":
                    comboBoxCol.ItemsSource = Engine._listTypeCategory;
                    break;
                case "ListObligationType":
                    comboBoxCol.ItemsSource = Engine._listObligationType;
                    break;
                case "SuggestedBy":
                    comboBoxCol.ItemsSource = Engine._listCheckedOutUser;
                    break;
                case "UserName":
                    comboBoxCol.ItemsSource = Engine._listCheckedOutUser;
                    break;
                case "ListVersionChangeType":
                    comboBoxCol.ItemsSource = Engine._listVersionChangeType;
                    break;
                default:
                    comboBoxCol.ItemsSource = null;
                    break;
            }
            return comboBoxCol;
        }

        public DataGridTextColumn CreateDataGridTextColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {

            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            // bindings.Path = new PropertyPath(objCurrentCol.ColBindingName);
            DataGridTextColumn colText = new DataGridTextColumn();
            colText.Header = objCurrentCol.ColName;
            colText.Binding = bindings;
            colText.SortMemberPath = objCurrentCol.ColBindingName;
            colText.ClipboardContentBinding = bindings;
            if (defaultColWidthChange)
            {
                colText.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colText.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colText.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            Type typePro = modelProperties.Where(x => x.Name.ToLower() == objCurrentCol.ColBindingName.ToLower()).Select(x => x.PropertyType).FirstOrDefault();
            if (typePro == typeof(int))
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, false);
            }
            else if (typePro == typeof(DateTime) || typePro == typeof(DateTime?))
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetIsBetweenFilterControl(colText, true);
            }
            else
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colText, true);
                DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colText, IsCheckedFilterCaseSenstive);
            }
            if (!string.IsNullOrEmpty(objCurrentCol.ColFilterMemberPath))
            {
                DataGridFilterLibrary.DataGridColumnExtensions.SetFilterMemberPathProperty(colText, objCurrentCol.ColFilterMemberPath);
            }

            if (!objCurrentCol.IsSelected)
            {
                colText.Visibility = Visibility.Collapsed;
            }
            var style = new Style(typeof(TextBlock));
            style.Setters.Add(new Setter(TextBlock.ToolTipProperty, bindings));
            style.Setters.Add(new Setter(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis));
            style.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
            colText.ElementStyle = style;
            return colText;
        }

        public DataGridComboBoxColumn CreateDataGridCheckboxColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridComboBoxColumn comboBoxCol = new DataGridComboBoxColumn();
            comboBoxCol.Header = objCurrentCol.ColName;
            comboBoxCol.SelectedItemBinding = bindings;
            DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(comboBoxCol, true);
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetUserCanEnterText(comboBoxCol, true);
            DataGridFilterLibrary.DataGridComboBoxExtensions.SetIsTextFilter(comboBoxCol, false);
            if (defaultColWidthChange)
            {
                comboBoxCol.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    comboBoxCol.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    comboBoxCol.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            comboBoxCol.DisplayMemberPath = "Content";
            comboBoxCol.SelectedValuePath = "Value";
            comboBoxCol.SortMemberPath = objCurrentCol.ColBindingName + ".Value";
            comboBoxCol.ClipboardContentBinding = bindings;
            comboBoxCol.ItemsSource = Engine._listCheckboxType;
            if (!objCurrentCol.IsSelected)
            {
                comboBoxCol.Visibility = Visibility.Collapsed;
            }
            Style myStyle = (Style)Application.Current.Resources["ListCheckBoxCombo"] as Style;
            comboBoxCol.ElementStyle = myStyle;
            return comboBoxCol;
        }
        public DataGridTemplateColumn CreateDataGridCheckboxEnableColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridTemplateColumn colCheckBox = new DataGridTemplateColumn();
            colCheckBox.ClipboardContentBinding = bindings;
            if (defaultColWidthChange)
            {
                colCheckBox.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colCheckBox.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colCheckBox.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            colCheckBox.Header = objCurrentCol.ColName;
            DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(colCheckBox, true);
            FrameworkElementFactory elementImage = new FrameworkElementFactory(typeof(CheckBox));
            bindings.Mode = System.Windows.Data.BindingMode.TwoWay;
            bindings.UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged;
            elementImage.SetValue(CheckBox.IsCheckedProperty, bindings);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementImage;
            colCheckBox.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            return colCheckBox;
        }
        public DataGridTemplateColumn CreateDataGridImageColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridTemplateColumn colImage = new DataGridTemplateColumn();
            colImage.Header = objCurrentCol.ColName;
            colImage.ClipboardContentBinding = bindings;
            DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(colImage, true);
            FrameworkElementFactory elementImage = new FrameworkElementFactory(typeof(Image));
            //bindings.Mode = System.Windows.Data.BindingMode.TwoWay;
            var imageProp = Image.SourceProperty;
            if (defaultColWidthChange)
            {
                colImage.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colImage.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colImage.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            elementImage.SetValue(Image.SourceProperty, bindings);
            elementImage.SetValue(Image.WidthProperty, 20.0);
            elementImage.SetValue(Image.HeightProperty, 25.0);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementImage;

            colImage.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            return colImage;
        }
        public DataGridHyperlinkColumn CreateDataGridHyperlinkColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridHyperlinkColumn colHyperlink = new DataGridHyperlinkColumn();
            colHyperlink.Header = objCurrentCol.ColName;
            colHyperlink.ClipboardContentBinding = bindings;
            DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colHyperlink, true);
            DataGridFilterLibrary.DataGridColumnExtensions.SetFilterMemberPathProperty(colHyperlink, objCurrentCol.ColBindingName);
            colHyperlink.Binding = bindings;

            if (defaultColWidthChange)
            {
                colHyperlink.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colHyperlink.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colHyperlink.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            var hyperlinkStyle = new Style(typeof(TextBlock));

            hyperlinkStyle.Setters.Add(new EventSetter(TextBlock.PreviewMouseLeftButtonDownEvent, (MouseButtonEventHandler)OnCellHyperlinkClick));

            colHyperlink.ElementStyle = hyperlinkStyle;

            return colHyperlink;
        }

        public DataGridTemplateColumn CreateMultipleHyperLink(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {
            System.Windows.Data.Binding filterbindings = new System.Windows.Data.Binding(objCurrentCol.ColFilterMemberPath);
            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridTemplateColumn colHyperLink = new DataGridTemplateColumn();
            colHyperLink.Header = objCurrentCol.ColName;
            colHyperLink.ClipboardContentBinding = bindings;

            if (defaultColWidthChange)
            {
                colHyperLink.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colHyperLink.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colHyperLink.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            Style scrollStyle = (Style)Application.Current.Resources["MultipleHyperlinkScrollViewer"] as Style;
            FrameworkElementFactory elementScrollView = new FrameworkElementFactory(typeof(ScrollViewer));
            elementScrollView.SetValue(ItemsControl.StyleProperty, scrollStyle);
            FrameworkElementFactory elementItemsControl = new FrameworkElementFactory(typeof(ItemsControl));
            Style myStyle = (Style)Application.Current.Resources["MultipleHyperlinkCol"] as Style;
            elementItemsControl.SetBinding(ItemsControl.ItemsSourceProperty, bindings);
            elementItemsControl.SetValue(ItemsControl.StyleProperty, myStyle);
            elementScrollView.AppendChild(elementItemsControl);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementScrollView;

            colHyperLink.SetValue(FrameworkElement.TagProperty, filterbindings);
            colHyperLink.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            DataGridFilterLibrary.DataGridColumnExtensions.SetContainsSearchProperty(colHyperLink, true);
            DataGridFilterLibrary.DataGridColumnExtensions.SetIsCaseSensitiveSearch(colHyperLink, false);
            DataGridFilterLibrary.DataGridColumnExtensions.SetFilterMemberPathProperty(colHyperLink, objCurrentCol.ColFilterMemberPath);
            return colHyperLink;
        }

        public DataGridTemplateColumn CreateDataGridButtonColumn(CommonDataGridColumn objCurrentCol, System.Reflection.PropertyInfo[] modelProperties)
        {

            System.Windows.Data.Binding bindings = new System.Windows.Data.Binding(objCurrentCol.ColBindingName);
            DataGridTemplateColumn colButton = new DataGridTemplateColumn();
            colButton.Header = objCurrentCol.ColName;
            colButton.ClipboardContentBinding = bindings;
            if (defaultColWidthChange)
            {
                colButton.Width = defaultColumnWidthBinding;
            }
            else
            {
                if (string.IsNullOrEmpty(objCurrentCol.ColumnWidth))
                {
                    colButton.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                }
                else
                {
                    colButton.Width = new DataGridLength(Convert.ToDouble(objCurrentCol.ColumnWidth));
                }
            }
            FrameworkElementFactory elementBtn = new FrameworkElementFactory(typeof(System.Windows.Controls.Button));
            elementBtn.SetValue(System.Windows.Controls.Button.ContentProperty, objCurrentCol.ContentName);
            elementBtn.SetValue(System.Windows.Controls.Button.CommandProperty, CommandButton);
            elementBtn.SetValue(System.Windows.Controls.Button.CommandParameterProperty, objCurrentCol.CommandParam);
            elementBtn.SetValue(System.Windows.Controls.Button.BackgroundProperty, new BrushConverter().ConvertFromString(objCurrentCol.BackgroundColor) as SolidColorBrush);
            elementBtn.SetValue(System.Windows.Controls.Button.BorderBrushProperty, new BrushConverter().ConvertFromString(objCurrentCol.BackgroundColor) as SolidColorBrush);
            if (!string.IsNullOrEmpty(objCurrentCol.CustomStyleClass))
            {
                Style eleStyle = (Style)Application.Current.Resources[objCurrentCol.CustomStyleClass];
                elementBtn.SetValue(System.Windows.Controls.Button.StyleProperty, eleStyle);
            }
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = elementBtn;
            colButton.CellTemplate = cellTemplate;
            cellTemplate.Seal();
            DataGridFilterLibrary.DataGridColumnExtensions.SetFilterMemberPathProperty(colButton, objCurrentCol.ColBindingName);
            DataGridFilterLibrary.DataGridColumnExtensions.SetDoNotGenerateFilterControl(colButton, true);

            return colButton;
        }
        private void OnCellHyperlinkClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (e.OriginalSource != null)
            {
                string url = ((System.Windows.FrameworkElement)e.OriginalSource).DataContext.ToString();
                if (url != "VersionControlSystem.Model.ViewModel.Library_QSID_Information_VM")
                {
                   // System.Diagnostics.Process.Start(url);
                    var p = new Process();
                    p.StartInfo = new ProcessStartInfo(url)
                    {
                        UseShellExecute = true
                    };
                    p.Start();
                }
            }

        }

        #endregion

        #region Filter Setting User Property Member
        private bool _isCheckedFilterCaseSenstive { get; set; } = false;
        public bool IsCheckedFilterCaseSenstive
        {
            get { return _isCheckedFilterCaseSenstive; }
            set
            {
                if (value != _isCheckedFilterCaseSenstive)
                {
                    _isCheckedFilterCaseSenstive = value;
                    BindDataGridColumnCollection();
                    RefreshFilterComponent();
                }
            }
        }
        public bool listAllColVisible { get; set; } = false;
        #endregion
        #region Setting pop up Icommand And Action
        private void CommmandChangeVisiblityListColExecute(object obj)
        {
            listAllColVisible = !listAllColVisible;
            NotifyPropertyChanged("listAllColVisible");
        }
        private void CommandOnColSelectedChangeExecute(object obj)
        {
            string selectColName = (string)obj;
            var selectCol = defaultListColumnGrid.Where(x => x.ColName == selectColName).FirstOrDefault();
            var selectColInGrid = ColumnCollection.Where(x => x.Header.ToString() == selectColName).FirstOrDefault();
            if (selectCol.IsSelected)
            {
                selectColInGrid.Visibility = Visibility.Visible;
            }
            else
            {
                selectColInGrid.Visibility = Visibility.Collapsed;
            }
            NotifyPropertyChanged("ColumnCollection");
            RefreshFilterComponent();
        }
        #endregion

        #region Paging Property Member
        private int pageIndex { get; set; } = 1;
        private int numberOfRecPerPage { get; set; } = 25;
        private enum PagingMode { Default = 0, First = 1, Next = 2, Previous = 3, Last = 4, PageCountChange = 5 };
        public string lblContent { get; set; }
        public string lblTotRecords { get; set; }
        private ObservableCollection<ListCommonComboBox_ViewModel> _listComboxItem { get; set; } = new ObservableCollection<ListCommonComboBox_ViewModel>();
        public ObservableCollection<ListCommonComboBox_ViewModel> ListComboxItem
        {
            get { return _listComboxItem; }
            set
            {
                if (_listComboxItem != value)
                {
                    _listComboxItem = value;
                    NotifyPropertyChanged("ListComboxItem");
                }
            }
        }
        private ListCommonComboBox_ViewModel _selectedItemCombo { get; set; }
        public ListCommonComboBox_ViewModel selectedItemCombo
        {
            get { return _selectedItemCombo; }
            set
            {
                if (_selectedItemCombo != value)
                {
                    _selectedItemCombo = value;
                    Navigate((int)PagingMode.PageCountChange);
                }
            }
        }
        #endregion

        #region Paging Icommand and Action
        public ICommand commandFirst { get; set; }
        public ICommand commandPrev { get; set; }
        public ICommand commandNext { get; set; }
        public ICommand commandLast { get; set; }
        public ICommand CommandOnColSelectedChange { get; set; }
        private bool CommandPrevCanExecute(object obj)
        {
            bool isResultPrev = false;
            if (pageIndex > 1)
            {
                isResultPrev = true;
            }
            return isResultPrev;
        }

        private bool CommandNextCanExecute(object obj)
        {
            bool isNext = false;
            if (listDataGridItemSource != null && listDataGridItemSource.Count > 0)
            {
                if (listDataGridItemSource.Count >= (pageIndex * numberOfRecPerPage))
                {
                    isNext = true;
                }
            }
            return isNext;
        }
        private void CommandLastExecute(object obj)
        {
            Navigate((int)PagingMode.Last);
        }
        private void CommandNextExecute(object obj)
        {
            Navigate((int)PagingMode.Next);
        }
        private void CommandPrevExecute(object obj)
        {
            Navigate((int)PagingMode.Previous);
        }
        private bool CommandFirstCanExecute(object obj)
        {
            return true;
        }
        private void CommandFirstExecute(object obj)
        {
            Navigate((int)PagingMode.First);
        }

        private void Navigate(int mode)
        {
            if (listDataGridItemSource != null)
            {
                IsNavigateProgress = true;
                lblTotRecords = "Total Records : " + listDataGridItemSource.Count;
                NotifyPropertyChanged("lblTotRecords");
                lblContent = "";
                NotifyPropertyChanged("lblContent");
                if (listDataGridItemSource.Count > 0)
                {
                    int count;
                    switch (mode)
                    {
                        case (int)PagingMode.Next:
                            pageIndex += 1;
                            var lstNextRec = listDataGridItemSource.Skip((pageIndex * numberOfRecPerPage) - numberOfRecPerPage).Take(numberOfRecPerPage);
                            ItemSource = new ObservableCollection<T>(lstNextRec);
                            var lastCount = (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage);
                            lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (lastCount > listDataGridItemSource.Count ? listDataGridItemSource.Count : lastCount) + " of " + listDataGridItemSource.Count;

                            NotifyPropertyChanged("lblContent");

                            break;
                        case (int)PagingMode.Previous:
                            if (pageIndex > 1)
                            {
                                pageIndex -= 1;

                                var lstPrevRec = listDataGridItemSource.Skip((pageIndex - 1) * numberOfRecPerPage).Take(numberOfRecPerPage);
                                ItemSource = new ObservableCollection<T>(lstPrevRec);
                                count = Math.Min(pageIndex * numberOfRecPerPage, listDataGridItemSource.Count);
                                lblContent = ((pageIndex - 1) * numberOfRecPerPage + 1) + " to " + (((pageIndex - 1) * numberOfRecPerPage) + numberOfRecPerPage) + " of " + listDataGridItemSource.Count;
                                //lblTotRecords = "Total Records : " + listDataGridItemSource.Count;
                                //NotifyPropertyChanged("lblTotRecords");
                                NotifyPropertyChanged("myListDataTable");
                                NotifyPropertyChanged("lblContent");

                            }
                            break;

                        case (int)PagingMode.First:
                            pageIndex = 2;
                            Navigate((int)PagingMode.Previous);
                            break;
                        case (int)PagingMode.Last:
                            pageIndex = (listDataGridItemSource.Count / numberOfRecPerPage);
                            Navigate((int)PagingMode.Next);
                            break;

                        case (int)PagingMode.PageCountChange:

                            pageIndex = 1;
                            if (selectedItemCombo.Name == "All")
                            {
                                numberOfRecPerPage = listDataGridItemSource.Count;
                                ItemSource = new ObservableCollection<T>(listDataGridItemSource.Count > 0 ? listDataGridItemSource : null);
                                //  lblTotRecords = "Total Records : " + listDataGridItemSource.Count;
                                lblContent = 1 + " to " + (listDataGridItemSource.Count) + " of " + listDataGridItemSource.Count;
                                // NotifyPropertyChanged("lblTotRecords");
                                NotifyPropertyChanged("lblContent");
                            }
                            else
                            {
                                numberOfRecPerPage = Convert.ToInt32(selectedItemCombo.Name);
                                var lst = listDataGridItemSource.AsQueryable().Take(numberOfRecPerPage).ToList();
                                ItemSource = new ObservableCollection<T>(lst);
                                count = listDataGridItemSource.AsQueryable().Take(numberOfRecPerPage).Count();
                                lblContent = 1 + " to " + (count > listDataGridItemSource.Count ? listDataGridItemSource.Count : count) + " of " + listDataGridItemSource.Count;
                                //lblTotRecords = "Total Records : " + listDataGridItemSource.Count;
                                //NotifyPropertyChanged("lblTotRecords");
                                NotifyPropertyChanged("myListDataTable");
                                NotifyPropertyChanged("lblContent");
                            }
                            break;
                        default:
                            if (listDataGridItemSource.Count >= 1)
                            {
                                var lst = listDataGridItemSource.Skip(0 * numberOfRecPerPage).Take(numberOfRecPerPage);
                                ItemSource = new ObservableCollection<T>(lst);
                                count = 25;
                                NotifyPropertyChanged("myListDataTable");
                                lblContent = 1 + " of " + (count > listDataGridItemSource.Count ? listDataGridItemSource.Count : count) + " of " + listDataGridItemSource.Count;
                                //lblTotRecords = "Total Records : " + listDataGridItemSource.Count;
                                //NotifyPropertyChanged("lblTotRecords");
                                NotifyPropertyChanged("lblContent");
                            }
                            break;
                    }

                }
                else
                {
                    ItemSource = new ObservableCollection<T>(listDataGridItemSource);
                }
                IsNavigateProgress = false;
                IsLoaderShow = false;
            }
        }
        private List<ListCommonComboBox_ViewModel> AddList()
        {
            List<ListCommonComboBox_ViewModel> listSelectedCombo =

                new List<ListCommonComboBox_ViewModel>() {
                new ListCommonComboBox_ViewModel() { Id = 25, Name= "25" },
                new ListCommonComboBox_ViewModel() { Id = 50, Name= "50" },
                new ListCommonComboBox_ViewModel() { Id = 100,Name = "100" },
                new ListCommonComboBox_ViewModel() { Id = 101,Name = "All" },
            };
            return listSelectedCombo;

        }
        #endregion


        #region Constructure
        public CommonDataGrid_ViewModel(List<CommonDataGridColumn> _defaultListColumnGrid, string _defaultSortedBy, string _defaultTitleContent, bool _isSynchronizedWithCurrentItem = false)
        {
            OverrideQueryController = new QueryController();
            query = new Query();
            filtersForColumns = new Dictionary<string, FilterData>();
            OverrideQueryController.FilteringStarted -= DataGridFilteringStarted;
            OverrideQueryController.FilteringStarted += DataGridFilteringStarted;
            MessengerInstance.Register<PropertyChangedMessage<List<T>>>(this, NotifyDefualtListChangedInParentViewModel);
            MessengerInstance.Register<PropertyChangedMessage<bool>>(this, NotifyLoderShowInParentViewModel);
            defaultListColumnGrid = new ObservableCollection<CommonDataGridColumn>(_defaultListColumnGrid);
            NotifyPropertyChanged("defaultListColumnGrid");
            defaultSortedBy = _defaultSortedBy;
            defaultTitleContent = _defaultTitleContent;
            IsSynchronizedWithCurrentItem = _isSynchronizedWithCurrentItem;

            ListComboxItem = new ObservableCollection<ListCommonComboBox_ViewModel>(AddList());
            selectedItemCombo = ListComboxItem.Where(x => x.Id == 25).FirstOrDefault();
            NotifyPropertyChanged("selectedItemCombo");
            commandFirst = new RelayCommand(CommandFirstExecute, CommandPrevCanExecute);
            commandPrev = new RelayCommand(CommandPrevExecute, CommandPrevCanExecute);
            commandNext = new RelayCommand(CommandNextExecute, CommandNextCanExecute);
            commandLast = new RelayCommand(CommandLastExecute, CommandNextCanExecute);
            CommmandChangeVisiblityListCol = new RelayCommand(CommmandChangeVisiblityListColExecute, CommandFirstCanExecute);
            CommandDataGridSorting = new RelayCommand(CommandDataGridSortingExecute, CommandDataGridSortingCanExecute);
            CommandOnColSelectedChange = new RelayCommand(CommandOnColSelectedChangeExecute, CommandFirstCanExecute);
            CommandDataGridClearFilter = new RelayCommand(CommandDataGridClearFilterExecute, CommandDataGridClearFilterCanExecute);
            CommandDataGridClearSorting = new RelayCommand(CommandDataGridClearSortingExecute, CommandDataGridClearSortingCanExecute);
            CommandGenerateExcelCasResult = new RelayCommand(CommandGenerateExcelListDetailExecuted, CommandGenerateAccessListDetailCanExecute);
            CommandGenerateAccessCasResult = new RelayCommand(CommandGenerateAccessListDetailExecuted, CommandGenerateAccessListDetailCanExecute);
            CommandRefreshItemDetailGrid = new RelayCommand(CommandRefreshItemDetailGridExecute, CommandRefreshItemDetailGridCanExecute);
            CommandCopyCellDataToClipboard = new RelayCommand(CommandCopyCellDataToClipboardExecute, CommandCopyCellDataToClipboardCanExecute);
            CommandDataGridLoaded = new RelayCommand(CommandDataGridLoadedExecute, CommandDataGridLoadedCanExecute);
            CommandDataGridSourceUpdated = new RelayCommand(CommandDataGridSourceUpdatedExecute, CommandDataGridSourceUpdatedCanExecute);
            CommmandChangeVisiblityListColNull = new RelayCommand(CommmandChangeVisiblityListColNullExecute, CommandFirstCanExecute);
            CommandOnColSelectedChangeNull = new RelayCommand(CommandOnColSelectedChangeNullExecute, CommandFirstCanExecute);
            CommandOnColSelectedFilterPercent = new RelayCommand(CommandOnColSelectedFilterPercentExecute);
            CommandAddNote = new RelayCommand(CommandAddNoteExecute, CommandAddNoteCanExecute);
            CommandParentQsid = new RelayCommand(CommandParentQsidExecute, CommandAddNoteCanExecute);
            CommandHyperlinkClick = new RelayCommand(CommandHyperlinkClickExecuted, CommandHyperlinkClickCanExecute);
            CommandButton = new RelayCommand(CommmandButtonExecute);
            BindDataGridColumnCollection();
            BindDataGridColumn();
            if (typeof(T).Name == "Library_QSID_Information_VM")
            {
                visibilityAddNoteButton = Visibility.Visible;
                visibilityParentQsidButton = Visibility.Visible;
            }
        }


        #endregion
        #region Update Data Source By Parent
        public void NotifyDefualtListChangedInParentViewModel(PropertyChangedMessage<List<T>> objUpdateList)
        {
            Task.Run(() =>
            {
                defaultList = objUpdateList.NewValue;
                listDataGridItemSource = objUpdateList.NewValue;
                Navigate(0);
            });
        }
        public void NotifyLoderShowInParentViewModel(PropertyChangedMessage<bool> objLoaderStatus)
        {
            IsLoaderShow = objLoaderStatus.NewValue;
        }

        #endregion
        #region Export Excel & Access File Action
        private bool CommandGenerateAccessListDetailCanExecute(object obj)
        {
            if (listDataGridItemSource != null && listDataGridItemSource.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void CommandGenerateExcelListDetailExecuted(object obj)
        {
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".Xlsx";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listDataGridItemSource);
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportExcelFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Excel File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Excel File Not Exported due to : " + resultMessage);
            }
        }
        public void CommandGenerateAccessListDetailExecuted(object obj)
        {
            string fileName = defaultTitleContent.Replace(":", "") + "_" + DateTime.Now.ToString("yyyy_MM_ddTHH_mm_ss") + ".accdb";
            DataTable dtListQsidDetail = ConvertDataTableFromListDetail(listDataGridItemSource);
            dtListQsidDetail.TableName = defaultTitleContent.Replace(":", "");// "List Qsids";
            var resultMessage = ExportAccessFile(dtListQsidDetail, Engine.CommonAccessExportFolderPath + fileName, _objAccessVersion_Service);
            if (resultMessage == "OK")
            {
                Task.Run(() => { OpenFileInAccessAsync(Engine.CommonAccessExportFolderPath + fileName); });
                _notifier.ShowSuccess("Access File Successfully Exported on below path:" + System.Environment.NewLine + Engine.CommonAccessExportFolderPath + fileName);
            }
            else
            {
                _notifier.ShowError("Access File Not Exported due to : " + resultMessage);
            }

        }
        public async Task OpenFileInAccessAsync(string resultCopyPath)
        {
            try
            {
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(resultCopyPath)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            catch (Exception ex)
            {
                _notifier.ShowError("System not open the file in access due to below error:" + Environment.NewLine + ex.Message);
            }
        }
        public DataTable ConvertDataTableFromListDetail(List<T> listDetails)
        {
            DataTable table = new DataTable();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));


            foreach (PropertyDescriptor prop in properties)
            {
                CommonDataGridColumn selectCol = new CommonDataGridColumn();
                selectCol = defaultListColumnGrid.Where(x => x.ColBindingName.IndexOf(".") > 0 ? x.ColBindingName.Split('.').FirstOrDefault().Contains(prop.Name) : x.ColBindingName == prop.Name).FirstOrDefault();
                if (selectCol != null)
                {
                    if (selectCol.ColType == "Checkbox")
                    {
                        table.Columns.Add(prop.Name, typeof(bool));
                    }
                    else
                    {
                        table.Columns.Add(prop.Name, typeof(string));
                    }
                }

            }
            foreach (T item in listDetails)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    CommonDataGridColumn selectCol = new CommonDataGridColumn();
                    selectCol = defaultListColumnGrid.Where(x => x.ColBindingName.IndexOf(".") > 0 ? x.ColBindingName.Split('.').FirstOrDefault().Contains(prop.Name) : x.ColBindingName == prop.Name).FirstOrDefault();
                    if (selectCol != null)
                    {
                        if (selectCol.ColType == "Checkbox")
                        {
                            var obj = prop.GetValue(item);
                            row[prop.Name] = obj != null ? (obj.GetType().GetProperty("Value").GetValue(obj).ToString()) : "";
                        }
                        else if (selectCol.ColType == "Combobox")
                        {
                            var obj = prop.GetValue(item);
                            row[prop.Name] = obj != null ? (obj.GetType().GetProperty("Name").GetValue(obj) != null ? obj.GetType().GetProperty("Name").GetValue(obj).ToString() : "") : "";
                        }
                        else if (selectCol.ColType == "MultipleHyperLinkCol")
                        {
                            List<HyperLinkDataValue> obj = (List<HyperLinkDataValue>)prop.GetValue(item);
                            row[prop.Name] = obj != null ? string.Join(", ", obj.Select(y => y.DisplayValue).ToArray()) : "";
                        }
                        else
                        {
                            if (prop.PropertyType.FullName == "CRA_DataAccess.ViewModel.CasType")
                            {
                                var obj = prop.GetValue(item);
                                row[prop.Name] = obj != null ? (obj.GetType().GetProperty("CasFormatted").GetValue(obj) != null ? obj.GetType().GetProperty("CasFormatted").GetValue(obj).ToString() : "") : "";
                            }
                            else
                            {
                                row[prop.Name] = prop.GetValue(item) != null ? prop.GetValue(item).ToString() : "";
                            }
                        }
                    }
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public string ExportExcelFile(DataTable table, string destination)
        {
            try
            {
                var workbook = DocumentFormat.OpenXml.Packaging.SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook);
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();
                    //foreach (System.Data.DataTable table in ds.Tables)
                    //{
                    var sheetPart = workbook.WorkbookPart.AddNewPart<DocumentFormat.OpenXml.Packaging.WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }
                    //}
                }
                workbook.WorkbookPart.Workbook.Save();
                workbook.Close();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExportAccessFile(DataTable _objDataTable, string fileName, IAccess_Version_BL _objAccessVersion_Service)
        {
            try
            {
                List<string> columnList = new List<string>();
                var propertifyInfo = _objDataTable.Columns;
                List<string> listColParam = new List<string>();
                ADOX.Catalog cat = new ADOX.Catalog();
                ADOX.Table table = new ADOX.Table();

                table.Name = _objDataTable.TableName;
                for (int i = 0; i < propertifyInfo.Count; i++)
                {
                    table.Columns.Append(propertifyInfo[i].ColumnName, ADOX.DataTypeEnum.adLongVarWChar);
                    listColParam.Add("@" + propertifyInfo[i].ColumnName);
                    columnList.Add(propertifyInfo[i].ColumnName);
                }

                cat.Create("Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5");
                cat.Tables.Append(table);

                ADODB.Connection con = cat.ActiveConnection as ADODB.Connection;
                if (con != null)
                {
                    con.Close();
                }
                //IAccess_Version_BL _objAccessVersion_Service = new Access_Version_BL();
                _objAccessVersion_Service.ExportAccessData(_objDataTable,  fileName, "[" + _objDataTable.TableName + "]", listColParam.ToArray(), columnList.ToArray());
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        #region NUll Filter
        public bool listAllColVisibleNull { get; set; } = false;
        public ICommand CommmandChangeVisiblityListColNull { get; set; }
        public ObservableCollection<ColumnSelectionDataGridForVM> ListAllColumnDGNull { get; set; } = new ObservableCollection<ColumnSelectionDataGridForVM>();
        public ICommand CommandOnColSelectedChangeNull { get; set; }
        public ICommand CommandOnColSelectedFilterPercent { get; set; }

        private List<string> queryNull { get; set; } = new List<string>();
        private void CommmandChangeVisiblityListColNullExecute(object obj)
        {
            listAllColVisibleNull = !listAllColVisibleNull;
            NotifyPropertyChanged("listAllColVisibleNull");
        }

        private string queryStringForNUll { get; set; }
        private void CommandOnColSelectedFilterPercentExecute(object obj)
        {
            if (OverrideQueryController.ColumnFilterData != null)
            {
                OverrideQueryController.ClearFilter();
            }
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDGNull.Where(x => x.ColumnName == selectColName).Select(y => new { y.ActualColName, y.IsFilterPercent }).FirstOrDefault();
            string Proptype = "";
            if (typeof(T) == typeof(object))
            {
                Proptype = "string";
            }
            else
            {
                if (selectCol != null)
                {
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    foreach (PropertyDescriptor prop in properties)
                    {
                        //var chkexts = defaultListColumnGrid.Where(x => x.ColBindingName == selectColName).FirstOrDefault();
                        if (prop.Name == selectCol.ActualColName)
                        {
                            Proptype = prop.PropertyType.Name;
                            break;
                        }
                    }
                }
            }
            if (Proptype != null && Proptype != "")
            {
                if (selectCol != null)
                {
                    if (Proptype.ToUpper() == "STRING")
                    {
                        if (selectCol.IsFilterPercent)
                        {
                            queryNull.Add(" " + selectCol.ActualColName + " != null and " + selectCol.ActualColName + ".Contains(@0)");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectCol.ActualColName + " != null and " + selectCol.ActualColName + ".Contains(@0)");
                        }

                    }

                }

                var result = string.Join("&&", queryNull.ToList());
                queryStringForNUll = result;
                SearchCriteria = result;
                if (result == string.Empty)
                {
                    queryStringForNUll = string.Empty;
                    listDataGridItemSource = defaultList;
                }
                else
                {
                    if (typeof(T) == typeof(object))
                    {
                        try
                        {
                            var lstNew = BindListFromIQuerable(defaultList.AsQueryable());

                            var res = lstNew.AsQueryable().Where(result.Trim());
                            List<T> lstnew = new List<T>();
                            foreach (var ct in res)
                            {
                                lstnew.Add((T)ct);
                            }
                            listDataGridItemSource = lstnew;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        listDataGridItemSource = defaultList.AsQueryable().Where(result.Trim(), "%").ToList();
                    }
                }
            }
            else
            {
                queryStringForNUll = string.Empty;
                listDataGridItemSource = defaultList;
            }
            ItemSource = new ObservableCollection<T>(listDataGridItemSource);
            if (ItemSource.Count == 0)
            {
                ItemSource = null;
                lblContent = "";
                lblTotRecords = "";
                NotifyPropertyChanged("lblTotRecords");
                NotifyPropertyChanged("lblContent");
                NotifyPropertyChanged("ItemSource");
            }
            else
            {
                Navigate(0);
            }

        }
        private void CommandOnColSelectedChangeNullExecute(object obj)
        {
            if (OverrideQueryController.ColumnFilterData != null)
            {
                OverrideQueryController.ClearFilter();
            }
            string selectColName = (string)obj;
            var selectCol = ListAllColumnDGNull.Where(x => x.ColumnName == selectColName).Select(y => new { y.ActualColName, y.IsNull, y.IsNotNull }).FirstOrDefault();
            string Proptype = "";
            if (typeof(T) == typeof(object))
            {
                Proptype = "string";
            }
            else
            {
                if (selectCol != null)
                {
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    foreach (PropertyDescriptor prop in properties)
                    {
                        //var chkexts = defaultListColumnGrid.Where(x => x.ColBindingName == selectColName).FirstOrDefault();
                        if (prop.Name == selectCol.ActualColName)
                        {
                            Proptype = prop.PropertyType.Name;
                            break;
                        }
                    }
                }
            }
            if (Proptype != null && Proptype != "")
            {
                if (selectCol != null)
                {
                    if (Proptype.ToUpper() == "STRING")
                    {
                        if (selectCol.IsNull)
                        {
                            queryNull.Add(" " + selectCol.ActualColName + " = null || " + selectCol.ActualColName + ".Length = 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectCol.ActualColName + " = null || " + selectCol.ActualColName + ".Length = 0 ");
                        }
                        if (selectCol.IsNotNull)
                        {
                            queryNull.Add(" " + selectCol.ActualColName + " != null && " + selectCol.ActualColName + ".Length > 0 ");
                        }
                        else
                        {
                            queryNull.Remove(" " + selectCol.ActualColName + " != null && " + selectCol.ActualColName + ".Length > 0 ");
                        }
                    }
                    else
                    {
                        if (Proptype.Contains("CheckBox"))
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectCol.ActualColName + ".Value == false ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectCol.ActualColName + ".Value == false ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectCol.ActualColName + ".Value == true ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectCol.ActualColName + ".Value == true ");
                            }
                        }
                        else
                        {
                            if (selectCol.IsNull)
                            {
                                queryNull.Add(" " + selectCol.ActualColName + " = null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectCol.ActualColName + " = null ");
                            }
                            if (selectCol.IsNotNull)
                            {
                                queryNull.Add(" " + selectCol.ActualColName + " != null ");
                            }
                            else
                            {
                                queryNull.Remove(" " + selectCol.ActualColName + " != null ");
                            }
                        }
                    }
                }

                var result = string.Join("&&", queryNull.ToList());
                queryStringForNUll = result;
                SearchCriteria = result;
                if (result == string.Empty)
                {
                    queryStringForNUll = string.Empty;
                    listDataGridItemSource = defaultList;
                }
                else
                {
                    if (typeof(T) == typeof(object))
                    {
                        try
                        {
                            var lstNew = BindListFromIQuerable(defaultList.AsQueryable());

                            var res = lstNew.AsQueryable().Where(result.Trim());
                            List<T> lstnew = new List<T>();
                            foreach (var ct in res)
                            {
                                lstnew.Add((T)ct);
                            }
                            listDataGridItemSource = lstnew;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        listDataGridItemSource = defaultList.AsQueryable().Where(result.Trim()).ToList();
                    }
                }
            }
            else
            {
                queryStringForNUll = string.Empty;
                listDataGridItemSource = defaultList;
            }
            ItemSource = new ObservableCollection<T>(listDataGridItemSource);
            if (ItemSource.Count == 0)
            {
                ItemSource = null;
                lblContent = "";
                lblTotRecords = "";
                NotifyPropertyChanged("lblTotRecords");
                NotifyPropertyChanged("lblContent");
                NotifyPropertyChanged("ItemSource");
            }
            else
            {
                Navigate(0);
            }

        }
        DataColumnCollection defaultDataTableColumns { get; set; }
        public IList BindListFromIQuerable(IQueryable listQuerable)
        {
            DataTable dt = new DataTable();
            foreach (var ct in defaultListColumnGrid)
            {
                dt.Columns.Add(ct.ColBindingName);
            }

            defaultDataTableColumns = dt.Columns;
            var obj = MyTypeBuilder.CreateNewObject(defaultDataTableColumns);
            var listType = typeof(List<>).MakeGenericType(new[] { obj });
            IList lst = (IList)Activator.CreateInstance(listType);
            var fields = obj.GetProperties();
            foreach (var dr in listQuerable)
            {
                var ob = Activator.CreateInstance(obj);

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in defaultDataTableColumns)
                    {
                        if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
                        {
                            var Proptype = dr.GetType().GetProperty(fieldInfo.Name).PropertyType;
                            object value = (dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null) == DBNull.Value) ? null : dr.GetType().GetProperty(fieldInfo.Name).GetValue(dr, null);
                            if (value != null)
                            {
                                var converter = TypeDescriptor.GetConverter(fieldInfo.PropertyType);
                                var result = converter.ConvertFrom(value.ToString());
                                fieldInfo.SetValue(ob, result);
                            }
                            else
                            {
                                if (Proptype == typeof(string))
                                {
                                    fieldInfo.SetValue(ob, "");
                                }
                                else
                                {
                                    fieldInfo.SetValue(ob, value);
                                }
                            }
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }
        private void BindDataGridColumn()
        {
            var listNull = new List<ColumnSelectionDataGridForVM>();
            foreach (var dt in defaultListColumnGrid)
            {
                listNull.Add(new ColumnSelectionDataGridForVM { ColumnName = dt.ColName, ActualColName = dt.ColBindingName, IsNull = false, IsNotNull = false });
            }

            ListAllColumnDGNull = new ObservableCollection<ColumnSelectionDataGridForVM>(listNull);
            NotifyPropertyChanged("ListAllColumnDGNull");
        }

        public class ColumnSelectionDataGridForVM : INotifyPropertyChanged
        {
            private bool _IsNull { get; set; }
            public bool IsNull
            {
                get { return _IsNull; }
                set
                {
                    _IsNull = value;
                    if (_IsNull)
                    {
                        IsNotNull = false;
                    }
                    NotifyPropertyChanged("IsNull");
                }
            }

            private bool _IsNotNull { get; set; }
            public bool IsNotNull
            {
                get { return _IsNotNull; }
                set
                {
                    _IsNotNull = value;
                    if (IsNotNull)
                    {
                        IsNull = false;
                    }
                    NotifyPropertyChanged("IsNotNull");
                }
            }
            private bool _IsFilterPercent { get; set; }
            public bool IsFilterPercent
            {
                get { return _IsFilterPercent; }
                set
                {
                    _IsFilterPercent = value;
                    NotifyPropertyChanged("IsFilterPercent");
                }
            }
            public string ColumnName { get; set; }
            public string ActualColName { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;
            public void NotifyPropertyChanged(String info)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(info));
                }
            }

        }
        #endregion 
    }
}
