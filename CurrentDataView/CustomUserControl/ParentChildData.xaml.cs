﻿using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for ParentChildData.xaml
    /// </summary>
    public partial class ParentChildData : UserControl
    {
        public ParentChildData()
        {
            InitializeComponent();
        }
    }
}
