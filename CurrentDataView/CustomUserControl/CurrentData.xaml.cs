﻿using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for CurrentData.xaml
    /// </summary>
    public partial class CurrentData : UserControl
    {
        public CurrentData()
        {
            InitializeComponent();
        }
    }
}
