﻿using System;
using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for LegislationProfile.xaml
    /// </summary>
    public partial class LegislationProfile : UserControl
    {
        //private int rowIndexSel = 5000;
        public LegislationProfile()
        {
            InitializeComponent();
            //myDataGrid.BeginningEdit += grid_BeginningEdit;

        }

        //void grid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        //{
        //    if (e.Row.GetIndex() != rowIndexSel)
        //    {
        //      e.Cancel = true;
        //    }
        //}
        private void dataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            try
            {
                var dg = sender as DataGrid;
                if (dg != null && e.AddedCells != null && e.AddedCells.Count > 0)
                {
                    var cell = e.AddedCells[0];
                    if (!cell.IsValid)
                        return;
                    var generator = dg.ItemContainerGenerator;
                    //int columnIndex = cell.Column.DisplayIndex; //OK as long user can't reorder
                    int rowIndex = generator.IndexFromContainer(generator.ContainerFromItem(cell.Item));
                    lblCurrentRow.Content = "Current Row - " + (Convert.ToInt32(rowIndex) + 1);
                }
            }
            catch { }
        }
        //private void btnEdit_Click(object sender, RoutedEventArgs e)
        //{
        //    rowIndexSel = myDataGrid.Items.IndexOf(myDataGrid.CurrentItem);
        //    int i = 0;
        //    foreach (var item in myDataGrid.Items.SourceCollection)
        //    {
        //        if (i != rowIndexSel)
        //        {
        //            ((CurrentDataView.ViewModel.LegislationProfileTable)item).IsEdit = false;
        //        }
        //        else
        //        {
        //            if (((CurrentDataView.ViewModel.LegislationProfileTable)item).IsEdit == false)
        //            {
        //                rowIndexSel = 5000;
        //            }
        //        }
        //        i++;
        //    }
        //}
    }
}
