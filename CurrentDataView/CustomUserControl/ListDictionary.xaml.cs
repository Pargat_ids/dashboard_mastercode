﻿using CRA_DataAccess;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VersionControlSystem.Business.BusinessService;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Model.ApplicationEngine;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for ListDictionary.xaml
    /// </summary>
    public partial class ListDictionary : UserControl
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public ListDictionary()
        {
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in Tab1.Items)
            {
                string headerName = ((TabItem)ct).Header.ToString();
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
                if (!Engine.listAllPermissionDashboard.Any(x => x.PermissionName == headerName.ToLower()))
                {
                    ((TabItem)ct).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((TabItem)ct).Visibility = Visibility.Visible;
                }
            }
        }
    }
}
