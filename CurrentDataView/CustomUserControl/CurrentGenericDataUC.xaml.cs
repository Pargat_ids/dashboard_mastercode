﻿using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for CurrentGenericDataUC.xaml
    /// </summary>
    public partial class CurrentGenericDataUC : UserControl
    {
        public CurrentGenericDataUC()
        {
            InitializeComponent();
        }
    }
}
