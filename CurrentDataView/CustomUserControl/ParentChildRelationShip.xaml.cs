﻿using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for ParentChildRelationShip.xaml
    /// </summary>
    public partial class ParentChildRelationShip : UserControl
    {
        public ParentChildRelationShip()
        {
            InitializeComponent();
        }
    }
}
