﻿using System.Windows;
using System.Windows.Controls;
using CRA_DataAccess;
using VersionControlSystem.Business.IBusinessService;
using VersionControlSystem.Business.BusinessService;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for MainControl_legislationUpd.xaml
    /// </summary>
    public partial class MainControl_legislationUpd : UserControl
    {
        public ILibraryFunction _objLibraryFunction_Service { get; private set; }
        public IAccess_Version_BL obj { get; private set; }
        public MainControl_legislationUpd()
        {
            InitializeComponent();
            if (_objLibraryFunction_Service == null)
            {
                _objLibraryFunction_Service = LibraryFunction.GetInstance;
            }
            if (obj == null)
            {
                obj = new Access_Version_BL(_objLibraryFunction_Service);
            }
            foreach (var ct in tabSectionProfile.Items)
            {
                if (obj.IsAttachmentAvaliable(((TabItem)ct).Header.ToString()))
                {
                    ((TabItem)ct).Style = (Style)FindResource("DocumentedTab");
                }
            }
        }

    }
}
