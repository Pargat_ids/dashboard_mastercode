﻿using System.Windows.Controls;

namespace CurrentDataView.CustomUserControl
{
    /// <summary>
    /// Interaction logic for GenericAddedDeletedLog.xaml
    /// </summary>
    public partial class GenericAddedDeletedLog : UserControl
    {
        public GenericAddedDeletedLog()
        {
            InitializeComponent();
        }
    }
}
