﻿using System;
using System.Data;
using System.Reflection;
using System.Reflection.Emit;

namespace CurrentDataView
{
    public static class MyTypeBuilder
    {
        public static Type CreateNewObject(DataColumnCollection dtColumns)
        {
            var myType = CompileResultType(dtColumns);
            var myObject = Activator.CreateInstance(myType);
            return myType;
        }
        //public static DataTable GetEmployeeDataTable()
        //{
        //    DataTable _dataTable = new DataTable();
        //    _dataTable.Columns.Add("EmpID", typeof(int));
        //    _dataTable.Columns.Add("EmpName", typeof(string));
        //    _dataTable.Columns.Add("Dept", typeof(string));
        //    _dataTable.Columns.Add("AccountInfo", typeof(int));
        //    _dataTable.Columns.Add("JoingDate", typeof(DateTime));

        //    _dataTable.Rows.Add(1, "pargat", "dot net", 213213, DateTime.Now.AddDays(-2));
        //    _dataTable.Rows.Add(2, "pargat2", "dot net", 43458, DateTime.Now.AddDays(-1));
        //    _dataTable.Rows.Add(3, "pargat3", "dot net", 4567824, DateTime.Now.AddDays(-4));
        //    _dataTable.Rows.Add(4, "pargat4", "dot net", 12753453, DateTime.Now.AddDays(-5));
        //    _dataTable.Rows.Add(5, "pargat5", "dot net", 14528, DateTime.Now.AddDays(-6));
        //    _dataTable.Rows.Add(6, "dharmesh1", "dot net", 45645, DateTime.Now.AddDays(-10));
        //    _dataTable.Rows.Add(7, "abc1", "dot net", 45645, DateTime.Now.AddDays(-8));
        //    _dataTable.Rows.Add(8, "p2dshjdf", "dot net", 21364, DateTime.Now.AddDays(-9));
        //    _dataTable.Rows.Add(9, "manesh", "dot net", 4768, DateTime.Now.AddDays(-7));
        //    _dataTable.Rows.Add(10, "ramskd", "dot net", 865544, DateTime.Now.AddDays(-11));
        //    _dataTable.Rows.Add(11, "mnjtsgsa", "dot net", 569964, DateTime.Now.AddDays(-7));
        //    _dataTable.Rows.Add(12, "sahdyw", "dot net", 9885, DateTime.Now.AddDays(-90));
        //    return _dataTable;
        //}
        public static Type CompileResultType(DataColumnCollection dtColumns)
        {
            TypeBuilder tb = GetTypeBuilder();
            ConstructorBuilder constructor = tb.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);
            // DataTable yourListOfFields = GetEmployeeDataTable();
            // NOTE: assuming your list contains Field objects with fields FieldName(string) and FieldType(Type)
            foreach (DataColumn field in dtColumns)
            {
                var typ = field.DataType;
                if(typ.Name == "DateTime")
                {
                    typ = typeof(DateTime?);
                }
                CreateProperty(tb, field.ColumnName, typ);
            }

            Type objectType = tb.CreateType();
            return objectType;
        }

        private static TypeBuilder GetTypeBuilder()
        {
            var typeSignature = "MyDynamicType";
            var an = new AssemblyName(typeSignature);
            AssemblyBuilder assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
            TypeBuilder tb = moduleBuilder.DefineType(typeSignature,
                    TypeAttributes.Public |
                    TypeAttributes.Class |
                    TypeAttributes.AutoClass |
                    TypeAttributes.AnsiClass |
                    TypeAttributes.BeforeFieldInit |
                    TypeAttributes.AutoLayout,
                    null);
            return tb;
        }

        private static void CreateProperty(TypeBuilder tb, string propertyName, Type propertyType)
        {
            FieldBuilder fieldBuilder = tb.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

            PropertyBuilder propertyBuilder = tb.DefineProperty(propertyName, System.Reflection.PropertyAttributes.HasDefault, propertyType, null);
            MethodBuilder getPropMthdBldr = tb.DefineMethod("get_" + propertyName, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, propertyType, Type.EmptyTypes);
            ILGenerator getIl = getPropMthdBldr.GetILGenerator();

            getIl.Emit(OpCodes.Ldarg_0);
            getIl.Emit(OpCodes.Ldfld, fieldBuilder);
            getIl.Emit(OpCodes.Ret);

            MethodBuilder setPropMthdBldr =
                tb.DefineMethod("set_" + propertyName,
                  MethodAttributes.Public |
                  MethodAttributes.SpecialName |
                  MethodAttributes.HideBySig,
                  null, new[] { propertyType });

            ILGenerator setIl = setPropMthdBldr.GetILGenerator();
            System.Reflection.Emit.Label modifyProperty = setIl.DefineLabel();
            System.Reflection.Emit.Label exitSet = setIl.DefineLabel();

            setIl.MarkLabel(modifyProperty);
            setIl.Emit(OpCodes.Ldarg_0);
            setIl.Emit(OpCodes.Ldarg_1);
            setIl.Emit(OpCodes.Stfld, fieldBuilder);

            setIl.Emit(OpCodes.Nop);
            setIl.MarkLabel(exitSet);
            setIl.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getPropMthdBldr);
            propertyBuilder.SetSetMethod(setPropMthdBldr);
        }

    }
}
