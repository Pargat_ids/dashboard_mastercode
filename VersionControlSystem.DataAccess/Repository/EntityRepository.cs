﻿using EntityFramework.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using VersionControlSystem.Model.DataModel;

namespace VersionControlSystem.DataAccess.Repository
{
    public class EntityRepository<TEntity> where TEntity : class
    {

        internal CRA_DevEntities context;
        internal DbSet<TEntity> dbSet;

        public EntityRepository(CRA_DevEntities _context)
        {
            this.context = _context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual List<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }
        public virtual List<TEntity> GetAllList()
        {
            return dbSet.ToList();
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
        }
        public virtual void InsertBulk(List<TEntity> entity)
        {
            EFBatchOperation.For(context, dbSet).InsertAll(entity);
        }

        public virtual void Delete(int id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == System.Data.Entity.EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            context.SaveChanges();
        }
        public virtual void DeleteBulk(List<TEntity> entityToDelete)
        {
            dbSet.RemoveRange(entityToDelete);
            context.SaveChanges();
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public virtual void UpdateBulk(List<TEntity> entity, params Expression<Func<TEntity, object>>[] properties)
        {
            EFBatchOperation.For(context, dbSet).UpdateAll(entity, x => x.ColumnsToUpdate(properties));
        }





        public virtual int GetTotalCount()
        {

            return dbSet.Count();
        }
    }
}
