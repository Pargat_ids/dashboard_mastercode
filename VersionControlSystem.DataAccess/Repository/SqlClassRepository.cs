﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace VersionControlSystem.DataAccess.Repository
{
    public class SqlClassRepository
    {
        public static List<T> DbaseQueryReturnList<T>(string query, string path)
        {

            List<T> lst = new List<T>();
            var dt = new DataTable();

            using (var dBconn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path))
            {
                var adp = new OleDbDataAdapter(query, dBconn);
                adp.Fill(dt);
                lst = BindListFromTable<T>(dt);
            }

            return lst;
        }
        public static T DbaseQueryReturnObject<T>(string query, string path)
        {

            var ob = Activator.CreateInstance<T>();
            var dt = new DataTable();

            using (var dBconn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path))
            {
                var adp = new OleDbDataAdapter(query, dBconn);
                adp.Fill(dt);
                ob = BindObjectModelFromTable<T>(dt);
            }
            return ob;
        }
        public static DataTable DbaseQueryReturnTable(string query, string path)
        {
            var dt = new DataTable();

            using (var dBconn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path))
            {
                var adp = new OleDbDataAdapter(query, dBconn);
                adp.Fill(dt);

            }


            return dt;
        }

        public static string DbaseQueryReturnString(string query, string path)
        {
            try
            {
                using (var dBconn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path))
                {
                    dBconn.Open();
                    var getListField = new OleDbCommand(query, dBconn);
                    var result = getListField.ExecuteScalar();
                    return result.ToString();
                }
            }
            catch
            {
                return "";
            }
        }
        public static T BindObjectModelFromTable<T>(DataTable dt)
        {
            var fields = typeof(T).GetProperties();

            var ob = Activator.CreateInstance<T>();

            foreach (var fieldInfo in fields)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
                    {
                        object value = (dt.Rows[0][dc.ColumnName] == DBNull.Value) ? null : dt.Rows[0][dc.ColumnName];
                        fieldInfo.SetValue(ob, value);
                        break;
                    }
                }
            }

            return ob;
        }
        public static List<T> BindListFromTable<T>(DataTable dt)
        {
            var fields = typeof(T).GetProperties();

            List<T> lst = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                // Create the object of T
                var ob = Activator.CreateInstance<T>();

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // Matching the columns with fields
                        if (fieldInfo.Name.ToLower() == dc.ColumnName.ToLower())
                        {
                            //try
                            //{
                            // Get the value from the datatable cell
                            object value = (dr[dc.ColumnName] == DBNull.Value) ? null : dr[dc.ColumnName];

                            // Set the value into the object
                            fieldInfo.SetValue(ob, value);
                            break;
                            //}
                            //catch (Exception ex)
                            //{
                            //}
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }
        private static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable();
            if (typeof(T).Name == "Object")
            {
                var properties = data[0].GetType().GetProperties();
                foreach (var prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                foreach (var item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (var prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
            }
            else
            {

                PropertyDescriptorCollection properties =
                   TypeDescriptor.GetProperties(typeof(T));


                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
            }
            return table;

        }

        public static void InserttoDatabase<T>(string[] parameterName, string[] valuePar, string[] datatableFields, string tableName, List<T> ListInput, string path)
        {
            DataTable dtFromList = ConvertToDataTable<T>(ListInput);
            var dtDefault = dtFromList.DefaultView.ToTable(false, datatableFields);
            var dtResult = dtDefault.DefaultView.ToTable(false, datatableFields);
            var valueFields = string.Join(",", parameterName);
            var valueParameter = string.Join(",", valuePar);
            var sql = "SELECT * FROM " + tableName;
            var insertQuery = "INSERT INTO " + tableName + " ( " + valueParameter + " ) VALUES (" + valueFields + " )";
            AddParametersInsertData(parameterName, datatableFields, sql, insertQuery, dtResult, path);
        }
        public static void InserttoDatabase(string[] parameterName, string[] valuePar, string[] datatableFields, string tableName, DataTable dtFromList, string path)
        {
            //DataTable dtFromList = ConvertToDataTable<T>(ListInput);
            var dtDefault = dtFromList.DefaultView.ToTable(false, datatableFields);
            var dtResult = dtDefault.DefaultView.ToTable(false, datatableFields);
            var valueFields = string.Join(", ", parameterName);
            var valueParameter = string.Join(", ", valuePar.Select(x => "[" + x + "]").ToList());
            var sql = "SELECT * FROM " + tableName;
            var insertQuery = "INSERT INTO " + tableName + " ( " + valueParameter + " ) VALUES (" + valueFields + " )";
            AddParametersInsertData(parameterName, datatableFields, sql, insertQuery, dtResult, path);
        }
        private static void AddParametersInsertData(string[] parameterName, string[] datatableFields, string sql, string insertQuery, DataTable dtNew, string path)
        {
            var oleConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path);
            var oleAdp = new OleDbDataAdapter(sql, oleConn) { InsertCommand = new OleDbCommand(insertQuery) };
            for (var i = 0; i <= parameterName.GetUpperBound(0); i++)
            {
                oleAdp.InsertCommand.Parameters.Add(parameterName[i], OleDbType.LongVarChar, 3000, datatableFields[i]);
            }
            oleAdp.InsertCommand.Connection = oleConn;
            oleAdp.InsertCommand.Connection.Open();
            oleAdp.Update(dtNew);
            oleAdp.InsertCommand.Connection.Close();
        }
    }
}
