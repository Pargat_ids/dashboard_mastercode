﻿using VersionControlSystem.DataAccess.IDataService;
using VersionControlSystem.DataAccess.Repository;
using VersionControlSystem.Model.DataModel;

namespace VersionControlSystem.DataAccess.DataService
{
    public class GenericDataService : IGenericDataService
    {
        CRA_DevEntities _context;
        public GenericDataService()
        {
            this._context = new CRA_DevEntities();
        }
        public EntityRepository<TEntity> AttachInstance<TEntity>() where TEntity : class
        {
            return new EntityRepository<TEntity>(this._context);

        }
        public void TruncateTable(string tableName)
        {
            _context.Database.ExecuteSqlCommand("TRUNCATE TABLE " + tableName);
        }
        public EntityRepository<TEntity> AttachInstance_ReloadContext<TEntity>() where TEntity : class
        {
            ReloadContext();
            return new EntityRepository<TEntity>(this._context);

        }

        public void ReloadContext()
        {
            this._context = new CRA_DevEntities();
        }
    }
}
