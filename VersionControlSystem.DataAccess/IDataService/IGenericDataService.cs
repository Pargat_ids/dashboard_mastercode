﻿using VersionControlSystem.DataAccess.Repository;

namespace VersionControlSystem.DataAccess.IDataService
{
    public interface IGenericDataService
    {
        EntityRepository<TEntity> AttachInstance<TEntity>() where TEntity : class;
        void TruncateTable(string tableName);
        EntityRepository<TEntity> AttachInstance_ReloadContext<TEntity>() where TEntity : class;
    }
}
